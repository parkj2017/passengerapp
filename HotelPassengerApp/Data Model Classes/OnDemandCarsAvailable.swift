//
//  OnDemandCarsAvailable.swift
//  PassangerApp
//
//  Created by Netquall on 12/16/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import UIKit

class OnDemandCarsAvailable: NSObject {
   
    lazy var car_Description = String()
   lazy var distance: Double = Double()
    lazy var driver_id : String? = String()
   lazy var fleet_id: String? = String()
   lazy var vehicle_type_id: String = String()
   lazy var driver_ProfilePic: String = String()
   lazy var latitude: String = String()
   lazy var longitude: String = String()
   lazy var car_Image: String = String()
   lazy var car_Distance: String = String()
   lazy var vehicalType_title: String = String()
    ///get cars basic prices ///
   lazy var base_fare: String = String()
   lazy var minumum_fare: String = String()
  lazy  var fare_per_minute: String = String()
  lazy  var base_per_km: String = String()
   lazy var max_seat_available: String = String()
     lazy var strArrivalTime: String = String()
}
