//
//  CardDetails.swift
//  PassangerApp
//
//  Created by Netquall on 1/12/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import Foundation
import UIKit
class AddressDetails: NSObject {
    
    var airportInstructions = [Any]()
    var strDateTime = ""
    var airportDateTime = ""
    var strAirport = ""
    var isEditingMode = false
    var dicDetails = [String:Any]()
    var strPickupType = pickupType.Address.rawValue
    var strDropOffType = DropOffType.Address.rawValue
    var strPickupDate =  ""
    var currentSection = 2
    var viewFrom:String?
    var strTypeOfAddress = "Pickup Address"
    var isPickUpAddress = true
    var strAirportReservationType = ""
    var dateSelected = Date()
    var serviceType = ""
    var PickupAddress = "N/A"
    var noOfPax = ""
}
