//
//  CardDetails.swift
//  PassangerApp
//
//  Created by Netquall on 1/12/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import Foundation
import UIKit
class VehicleData: NSObject {
    
    var farebreakdown = [String:Any]()
    var farebreakdown_arr = [[String:Any]]()
    
    
    var fleet_id = ""
    var id = ""
    var fleetName = "N/A"
    var vehicle_id = ""
    var vehicle_specific_passenger_capacity = "1"
    var vehicle_specific_luggage_capacity = "1"
    var active_icon = ""
    var inactive_icon = ""
    var map_icon =  ""
    var web_icon = ""
    var online_icon = ""
    var totalamount = "N/A"
     func setupValueWithDataSource(dicParam:[String:Any]) -> VehicleData {
        self.active_icon = String(describing: dicParam["active_icon"]!)
        self.fleet_id = String(describing: dicParam["id"]!)
        self.fleetName = String(describing: dicParam["vehicle_type_title"]!)
      //  self.vehicle_id = String(describing: dicParam["vehicle_id"]!)
        self.id = String(describing: dicParam["id"]!)
        self.vehicle_specific_passenger_capacity = String(describing: dicParam["vehicle_specific_passenger_capacity"]!)
        self.vehicle_specific_luggage_capacity = String(describing: dicParam["vehicle_specific_luggage_capacity"]!)
        self.inactive_icon = String(describing: dicParam["inactive_icon"]!)
        self.map_icon = String(describing: dicParam["map_icon"]!)
        self.web_icon = String(describing: dicParam["web_icon"]!)
        self.online_icon = String(describing: dicParam["online_icon"]!)
        self.totalamount = String(describing: dicParam["totalamount"]!)
        if let fare = dicParam["farebreakdown"] as? [String:Any] {
            self.farebreakdown = fare
        }
        if let fare = dicParam["farebreakdown_arr"] as? [[String:Any]] {
            self.farebreakdown_arr = fare
        }
        return self
    }
  

}
