//
//  DriverDetails.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 19/01/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON
class DriverDetails: NSObject {
    
    var jobStatus : String = ""
    var fare_Quote : String!
    var pickup_time : String!
    var pick_date : String!
    var fleet_Name : String!
    var vehicle_type_title : String!
    var cellular_phone : String!
    var driver_ID : String!
    var firstName : String!
    var lastName : String!
    var response_Status : String!
    var driver_image : String!
    var image_url : String!
    var licence_number : String!
    var fleet_image : String!
    var base_fare : String!
    var mile_rate : String!
    var km_rate : String!
    
    var min_fare : String!
    var min_rate : String!
    var driverLat : Double! = 0.0
    var driverLong : Double! = 0.0
    var booking_Id : String = ""
    var reservation_id : String = ""

    
    //CustomerInfo
    
    var customer_id : String!
    var customer_fName : String!
    var customer_lName : String!
    var customer_Mobile : String!
    
    var pickUp_Loc : String!
    var dropOff_Loc : String!
    
    var pickup_lat : Double! = 0.0
    var pickup_Long : Double! = 0.0
    var dropOff_lat : Double! = 0.0
    var dropOff_Long : Double! = 0.0
    
    var vehicleType_id : String!
    var acceptRejectJobFlag : String!
    var reservation_Source : String!
   
    var dicAppliedRates:[String:AnyObject] = [String:AnyObject]()
    var dicAppliedRates_arr:[[String:AnyObject]] = [[String:AnyObject]]()

    var currency:String = ""
    var totalFare:String = "" //for ride later jobs


    func updatePickupLoc(_ location:String){
        
        if location.characters.count > 0 {
            let arrDropOff = location.components(separatedBy: "place_id")
            if arrDropOff.count > 0{
                self.pickUp_Loc = arrDropOff[0]
            }
            if location.characters.count > 0  {
                let arrDropOff = location.components(separatedBy: "place_id")
                
                if arrDropOff.count > 1{
                    self.pickUp_Loc = arrDropOff[0]
                    
                    GoogleMapAPIServiceManager.sharedInstance.retrieveJSONDetailsAboutWithPlaceID(arrDropOff[1], withComplitionHandler: {[weak self] (location) -> () in
                        
                        self?.pickup_lat = location.coordinate.latitude
                        self?.pickup_Long = location.coordinate.longitude
                        self?.updateGlobalJobDetails()
                        
                        
                        }, failureBlock: { (error) -> () in
                            print("failureBlock")
                            
                    })
                }
                else{
                    self.pickUp_Loc = arrDropOff[0]
                    
                    let dropoffLocation:CLLocation = self.getAddressLatLongiFromAddressString(self.dropOff_Loc, isPickup: false)
                    
                    self.pickup_lat = dropoffLocation.coordinate.latitude
                    self.pickup_Long = dropoffLocation.coordinate.longitude
                    self.updateGlobalJobDetails()
                    
                }
                
            }
            
        }
    }
    func updateDropoffLoc(_ location:String){
        
        if location.characters.count > 0  {
            let arrDropOff = location.components(separatedBy: "place_id")
            
            if arrDropOff.count > 1{
                self.dropOff_Loc = arrDropOff[0]
                
                GoogleMapAPIServiceManager.sharedInstance.retrieveJSONDetailsAboutWithPlaceID(arrDropOff[1], withComplitionHandler: {[weak self] (location) -> () in
                    
                    self?.dropOff_lat = location.coordinate.latitude
                    self?.dropOff_Long = location.coordinate.longitude
                    self?.updateGlobalJobDetails()
                    
                    
                    }, failureBlock: { (error) -> () in
                        print("failureBlock")
                        
                })
            }
            else{
                self.dropOff_Loc = location
                
                let dropoffLocation:CLLocation = self.getAddressLatLongiFromAddressString(location, isPickup: false)
                
                self.dropOff_lat = dropoffLocation.coordinate.latitude
                self.dropOff_Long = dropoffLocation.coordinate.longitude
                self.updateGlobalJobDetails()
                
            }
            
        }
    }
    
    func getAddressLatLongiFromAddressString(_ strAddress: String, isPickup:Bool) -> CLLocation {
        
        var latitude:Double = 0
        var longitude:Double = 0
        
        let esc_addr: String = strAddress.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let req = Globals.sharedInstance.getFullApiAddressForAddressString(esc_addr)
        var result : String!
        do {
            result = try String(contentsOf: URL(string: req)!, encoding: String.Encoding.utf8)
        }catch {
            print("json Error-::\(error)")
        }
        if result  != "" && result  != nil{
            let scanner: Scanner = Scanner(string: result)
            
            if scanner.scanUpTo("\"lat\" :", into: nil) && scanner.scanString("\"lat\" :", into: nil) {
                scanner.scanDouble(&latitude)
                if scanner.scanUpTo("\"lng\" :", into: nil) && scanner.scanString("\"lng\" :", into: nil) {
                    scanner.scanDouble(&longitude)
                }
            }
        }
        
        let location = CLLocation(latitude: latitude, longitude: longitude)
        return location
    }
    
    func setCurrentJobValues(_ dictJobDetails:[String:Any]!){
        print("setCurrentJobValues---->\n"+"\(dictJobDetails!)")
        
        //        if let strJobStaus = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.currentJobStaus){
        //            self.jobStatus =  strJobStaus as! String
        //        }
        //        else{
        
        //}
        if let status = dictJobDetails["availablestatus"] {
            self.jobStatus = "\(status)"
        }
        self.fleet_Name = dictJobDetails["fleet_name"] as! String
        self.vehicle_type_title = dictJobDetails["vehicle_type_title"] as! String
        self.cellular_phone = dictJobDetails["cellular_phone"] as! String
        self.driver_ID = dictJobDetails["driver_id"] as! String
        self.firstName = dictJobDetails["first_name"] as! String
        self.lastName = dictJobDetails["last_name"] as! String
        self.driver_image = dictJobDetails["image_url"] as! String
        
        self.image_url = dictJobDetails["image_url"] as! String
        
        self.licence_number = dictJobDetails["licenceno"] as! String
        self.fleet_image = dictJobDetails["fleet_image"] as! String
        self.vehicleType_id = String(describing: dictJobDetails["vehicle_type_id"]!)
        
        if let curr = dictJobDetails["currency"] as? String{
            self.currency = curr
        }
        else {
            self.currency = appDelegate.currency
        }
        
        if dictJobDetails["totalfare"] != nil{
            
            if let totalFare = dictJobDetails["totalfare"] as? String{
                self.totalFare = totalFare
            }
            
        }

        
        
        if let dicRates = dictJobDetails["rate_arr"] as? [[String:AnyObject]] {
            dicAppliedRates_arr = dicRates
        }

        if let dicRates = dictJobDetails["rate"] as? [String:AnyObject] {
            
            dicAppliedRates = dicRates
            
            if let base_fare = dicRates["base_fare"] as? String{
                self.base_fare = base_fare
            }
            if let mile_rate = dicRates["mile_rate"] as? String{
                self.mile_rate =  mile_rate
            }
            if let min_fare = dicRates["min_fare"] as? String{
                self.min_fare =  min_fare
            }
            if let km_rate = dicRates["km_rate"] as? String{
                self.km_rate = km_rate
            }
            if let min_rate = dicRates["min_rate"] as? String{
                self.min_rate = min_rate
            }
        }
        else{
            self.base_fare = "0.0"
            self.mile_rate = "0.0"
            self.km_rate = "0.0"
            self.min_fare = "0.0"
            self.min_rate = "0.0"
        }
        
        if let dicCustom = dictJobDetails["custom"] as? [String:Any]{
            if let b_id = dicCustom["booking_id"] as? String{
                self.booking_Id = b_id
            }
            if let c_id = dicCustom["customer_id"] as? String{
                self.customer_id = c_id
            }
            
            if let r_id = dicCustom["reservation_id"] as? String{
                self.reservation_id = r_id
            }
            
            self.customer_fName = String(describing: dicCustom["first_name"]!)
            self.customer_lName = String(describing: dicCustom["last_name"]!)
            
            if let source = dicCustom["reservation_source"] as? String{
                self.reservation_Source = source
            }
            else
            {
                self.reservation_Source = ""
            }
            
            if let dropOff = dicCustom["dropoff_location"] as? String{
                self.dropOff_Loc = dropOff
            }
            else {
                self.dropOff_Loc = ""
            }
            if let d_long = dicCustom["dropoff_longitude"]  as? String{
                self.dropOff_Long = (d_long == "") ? 0.0 : Double(d_long)
            }
            else {
                self.dropOff_Long = 0.0
            }
            if let d_lat = dicCustom["dropoff_latitude"]  as? String{
                self.dropOff_lat = (d_lat == "") ? 0.0 : Double(d_lat)
            }
            else {
                self.dropOff_lat = 0.0
            }
            
            
            if let pickup_loc = dicCustom["pickup_location"]  as? String{
                self.pickUp_Loc = pickup_loc
            }
            else {
                self.pickUp_Loc = ""
            }
            if let p_long = dicCustom["pickup_longitude"]  as? String{
                self.pickup_Long = (p_long == "") ? 0.0 : Double(p_long)
            }
            else {
                self.pickup_Long = 0.0
            }
            if let p_lat = dicCustom["pickup_latitude"]  as? String{
                self.pickup_lat = (p_lat == "") ? 0.0 : Double(p_lat)
            }
            else {
                self.pickup_lat = 0.0
            }
            
        }
        
        self.acceptRejectJobFlag = dictJobDetails["availablestatus"] as! String
        updateGlobalJobDetails()
    }
    
    func updateGlobalJobDetails()
    {
        Globals.sharedInstance.activeDriver = self
    }
    
}
