//
//  cellVehicleTypeList.swift
//  HotelPassengerApp
//
//  Created by Netquall on 26/04/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit

class cellVehicleTypeList: UITableViewCell {

    @IBOutlet var vehicleName: UILabel!
    @IBOutlet var totalFare: UILabel!
    @IBOutlet var tapForBreakDownButton: UIButton!
    @IBOutlet var prizeNote: UILabel!
    @IBOutlet var selectVehicleButton: UIButton!
    @IBOutlet var lagguageCount: UIButton!
    @IBOutlet var adultCount: UIButton!
    @IBOutlet var vehicleImage: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        Globals.layoutViewFor(self.tapForBreakDownButton, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 3)
         Globals.layoutViewFor(self.selectVehicleButton, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 3)
         Globals.layoutViewFor(self.callNowButton, color: UIColor.appRideLater_GrayColor(), width: 1.0, cornerRadius: 3)
        self.vehicleName.font = Globals.defaultAppFontWithBold(14)
        self.totalFare.font = Globals.defaultAppFontWithBold(14)
        self.totalFare.numberOfLines = 5
        self.totalFare.lineBreakMode = .byWordWrapping
        self.adultCount.titleLabel?.font = Globals.defaultAppFont(14)
        self.lagguageCount.titleLabel?.font = Globals.defaultAppFont(14)
        self.tapForBreakDownButton.titleLabel?.font = Globals.defaultAppFont(12)
        self.tapForBreakDownButton.titleLabel?.textAlignment = .center
        self.callNowButton.titleLabel?.font = Globals.defaultAppFontWithBold(14)
        self.selectVehicleButton.titleLabel?.font = Globals.defaultAppFontWithBold(14)
        self.prizeNote.font = Globals.defaultAppFontItalic(10)
        self.prizeNote.numberOfLines = 5
        self.prizeNote.lineBreakMode = .byWordWrapping
        self.tapForBreakDownButton.titleLabel?.numberOfLines = 10
        self.tapForBreakDownButton.titleLabel?.lineBreakMode = .byWordWrapping
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
     
        // Configure the view for the selected state
    }
    @IBOutlet var callNowButton: UIButton!

}
