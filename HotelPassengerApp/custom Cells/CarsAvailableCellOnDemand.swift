//
//  CarsAvailableCellOnDemand.swift
//  PassangerApp
//
//  Created by Netquall on 1/4/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader

class CarsAvailableCellOnDemand: UITableViewCell {

    
    @IBOutlet  var imageCarIcon: DLImageView!
    
    @IBOutlet weak var viewCarNamePrize: UIView!
    @IBOutlet weak var imageTimeClock: UIImageView!
    @IBOutlet weak var lblCarPrice: UILabel!
    @IBOutlet weak var lblMaxSeats: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblCarName: UILabel!
    
    @IBOutlet weak var lblArivalTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      //  self.lblMaxSeats.layer.borderColor = UIColor.lightGray.cgColor
      //  self.lblCarPrice.layer.borderColor = UIColor.lightGray.cgColor
        
     //   self.lblMaxSeats.layer.borderWidth = 0.5
      //  self.lblCarPrice.layer.borderWidth = 0.5
        self.imageCarIcon.contentMode = .scaleAspectFit
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

func vehicleImageForType(_ dicVehicleInfo:[String:String] ,activeIcon:Bool)-> Data{
        var vehicleImage_Data : Data? = nil
        var iconTypeKey : String! = ""
        iconTypeKey = activeIcon ? "active_icon" : "inactive_icon"
        let keyForImage = dicVehicleInfo["id"]! + iconTypeKey
        vehicleImage_Data = Globals.sharedInstance.getValueFromUserDefaultsForKey(keyForImage) as? Data
        if vehicleImage_Data == nil {
                let cars_Icon: URL = URL(string: dicVehicleInfo[iconTypeKey]!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!
                
                vehicleImage_Data = try! Data(contentsOf: cars_Icon)
            
                Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(vehicleImage_Data! as Any?, key: keyForImage)
            }
 
        return vehicleImage_Data!
    }
func SetFontColorOfCellSubViews(_ cell:CarsAvailableCellOnDemand, color:UIColor){
    cell.lblCarName.textColor = color
   // cell.lblArivalTime.textColor = color
    cell.lblCarPrice.textColor = color
    cell.lblMaxSeats.textColor = color
    }

}
