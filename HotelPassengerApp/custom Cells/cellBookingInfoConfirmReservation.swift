//
//  cellBookingInfoConfirmReservation.swift
//  HotelPassengerApp
//
//  Created by Netquall on 09/05/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit

class cellBookingInfoConfirmReservation: UITableViewCell {
  
    @IBOutlet var heightOfStops: NSLayoutConstraint!
    @IBOutlet var payment: PALabel!
    @IBOutlet var vehicleName: PALabel!
    @IBOutlet var stops: PALabel!
    @IBOutlet var dropOffAdress: UIButton!
    @IBOutlet var pickupAddress: UIButton!
    @IBOutlet var serviceType: PALabel!
    @IBOutlet var pickupDateTime: PALabel!
     var viewFontSize : Float = 14
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        payment.font = Globals.defaultAppFont(viewFontSize)
        vehicleName.font = Globals.defaultAppFont(viewFontSize)
        stops.font = Globals.defaultAppFont(viewFontSize)
        pickupDateTime.font = Globals.defaultAppFont(viewFontSize)
        dropOffAdress.titleLabel?.font = Globals.defaultAppFont(viewFontSize)
        pickupAddress.titleLabel?.font = Globals.defaultAppFont(viewFontSize)
        serviceType.font = Globals.defaultAppFont(viewFontSize)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
