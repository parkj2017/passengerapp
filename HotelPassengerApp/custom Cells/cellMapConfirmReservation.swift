//
//  cellMapConfirmReservation.swift
//  HotelPassengerApp
//
//  Created by Netquall on 09/05/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit
import GoogleMaps
class cellMapConfirmReservation: UITableViewCell {
    
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var messageStatus: UILabel!
    @IBOutlet var confirmationNumber: UILabel!
    @IBOutlet var jobDuration: UILabel!
    @IBOutlet var distance: UILabel!
    
    @IBOutlet var fare: UILabel!
    var viewFontSize : Float = 14
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let viewMapDetails = self.viewWithTag(123456)
        if let jobInfo = viewMapDetails?.viewWithTag(66) as? UILabel {
            jobInfo.font = Globals.defaultAppFont(viewFontSize - 2)
        }
        if let distance = viewMapDetails?.viewWithTag(77) as? UILabel {
            distance.font = Globals.defaultAppFont(viewFontSize - 2)
        }
        if let fare = viewMapDetails?.viewWithTag(88) as? UILabel {
            fare.font = Globals.defaultAppFont(viewFontSize - 2)
        }
       jobDuration.font = Globals.defaultAppFont(viewFontSize)
       distance.font = Globals.defaultAppFont(viewFontSize)
       confirmationNumber.font = Globals.defaultAppFont(viewFontSize)
       messageStatus.font = Globals.defaultAppFont(viewFontSize - 2)
        messageStatus.textColor = UIColor.appRideLater_GreenColor()
        confirmationNumber.textColor = UIColor.appRideLater_GrayColor()
        jobDuration.textColor = UIColor.appRideLater_RedColor()
        fare.textColor = UIColor.appRideLater_RedColor()
        distance.textColor = UIColor.appRideLater_RedColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateSourceDataOfMap(dicReservation:[String:Any])  {
        
        let pickupLoc = CLLocation(latitude: Double(String(describing:dicReservation["pickup_latitude"]! ))!, longitude:  Double(String(describing:dicReservation["pickup_longitude"]!))! )
        
        if let lat = dicReservation["dropoff_latitude"], let long = dicReservation["dropoff_longitude"]{
        
            if "\(lat)".isEmpty || "\(long)".isEmpty{
                
            }else{
                let dropOffLoc = CLLocation(latitude: Double(String(describing:"\(lat)"))!  , longitude: Double(String(describing:"\(long)"))! )
            
           
            self.addOverlayToMapView(pickupLoc: pickupLoc, dropOff: dropOffLoc)
            self.showPick_DropOffLocationOnMap(pickupLoc.coordinate, withVehicalTitle: "Pick-Up Address", image: "pickupPin")

            self.showPick_DropOffLocationOnMap(dropOffLoc.coordinate, withVehicalTitle: "Drop-Off Address", image: "dropOffPin")
            self.setupMapBoundsWithLocation(pickupLoc, destiLOC: dropOffLoc)
            }
        }
        else {

        self.showPick_DropOffLocationOnMap(pickupLoc.coordinate, withVehicalTitle: "Pick-Up Address", image: "pickupPin")
        self.setupMapBoundsWithLocation(pickupLoc, destiLOC: nil)
        }
    }
    
}
extension cellMapConfirmReservation {
    // MARK: - ======== Pickup DropOff Markers  ========
    func showPick_DropOffLocationOnMap (_ location:CLLocationCoordinate2D, withVehicalTitle:String, image:String){
        let carMarker = GMSMarker(position: location)
        carMarker.position = location
        carMarker.userData = withVehicalTitle
        carMarker.icon = UIImage(named: image)
        carMarker.map = self.mapView
    }
    func addPolyLineWithEncodedStringInMap(_ encodedString: String) {
        let path = GMSMutablePath(fromEncodedPath: encodedString)
        let polyLine = GMSPolyline(path: path)
        polyLine.strokeWidth = 1.5
        polyLine.geodesic = true
        polyLine.strokeColor = UIColor.appRideLater_GreenColor()
        polyLine.isTappable = true
        polyLine.map = self.mapView
        
    }
    func addOverlayToMapView(pickupLoc origin:CLLocation, dropOff destination:CLLocation){
        let request = OCDirectionsRequest(originLocation: origin, andDestinationLocation: destination)
        request?.transitMode = .bus
        request?.trafficModel = .default
        
        request?.alternatives = true
        request?.travelMode = OCDirectionsRequestTravelMode.driving
        request?.waypointsOptimise = true
        
        let direction = OCDirectionsAPIClient(noKeyUseHttps: true)
        direction?.directions(request, response: { (response, error) in
            //print("response ---\(response?.dictionary)")
            if ((error) != nil) {
                return;
            }
            if (response?.status != OCDirectionsResponseStatus.OK) {
                return
            }
            DispatchQueue.main.async(execute: { [weak self] in
                self?.addPolyLineWithEncodedStringInMap(response?.route().overviewPolyline.dictionary["points"] as! String)
            })
            
            if let legs = response?.route().legs.first as? OCDirectionsLeg {
               // print("duration ---\(legs.duration.text)")
            }
            
        })
        
    }
    func setupMapBoundsWithLocation(_ originLOC:CLLocation?, destiLOC: CLLocation?){
        
        var mapBounds:GMSCoordinateBounds!
        if let _ = originLOC {
            mapBounds = GMSCoordinateBounds(coordinate: originLOC!.coordinate, coordinate: originLOC!.coordinate)
        }
        
        if let _ = destiLOC {
            if destiLOC!.coordinate.latitude != 0.0 ||  destiLOC!.coordinate.longitude != 0.0{
                let carBounds = GMSCoordinateBounds(coordinate: destiLOC!.coordinate, coordinate: destiLOC!.coordinate)
                if let _ = mapBounds {
                    mapBounds = mapBounds.includingBounds(carBounds)
                }else {
                    mapBounds = GMSCoordinateBounds(coordinate: destiLOC!.coordinate, coordinate: destiLOC!.coordinate)
                }
            }
        }
        if mapBounds != nil {
            let camera = self.mapView.camera(for: mapBounds, insets: UIEdgeInsetsMake(50.0,50.0, 70, 50.0))
            self.mapView.animate(to: camera!)
            DispatchQueue.main.async {[weak self] () -> Void in
                CATransaction.begin()
                CATransaction.setAnimationDuration(1.0)
                
                self?.mapView.animate(to: (self?.mapView.camera(for: (mapBounds)!, insets: UIEdgeInsetsMake(50.0,50.0, 70, 50.0)))!)
                CATransaction.commit()
            }
        }
        
    }
}
