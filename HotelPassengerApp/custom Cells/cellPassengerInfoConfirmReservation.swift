//
//  cellPassengerInfoConfirmReservation.swift
//  HotelPassengerApp
//
//  Created by Netquall on 09/05/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit

class cellPassengerInfoConfirmReservation: UITableViewCell {

    @IBOutlet var childSeat: PALabel!
    @IBOutlet var adultCount: PALabel!
    @IBOutlet var laggageOfPax: PALabel!
    @IBOutlet var numberOfPax: PALabel!
    @IBOutlet var nameOfPax: PALabel!
  var viewFontSize : Float = 14
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        childSeat.font = Globals.defaultAppFont(viewFontSize)
        adultCount.font = Globals.defaultAppFont(viewFontSize)
        laggageOfPax.font = Globals.defaultAppFont(viewFontSize)
        numberOfPax.font = Globals.defaultAppFont(viewFontSize)
        nameOfPax.font = Globals.defaultAppFont(viewFontSize)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
