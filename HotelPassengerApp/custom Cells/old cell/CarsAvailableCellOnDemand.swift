//
//  CarsAvailableCellOnDemand.swift
//  PassangerApp
//
//  Created by Netquall on 1/4/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader

class CarsAvailableCellOnDemand: UITableViewCell {

    
    @IBOutlet  var imageCarIcon: DLImageView!
    
    @IBOutlet weak var viewCarNamePrize: UIView!
    @IBOutlet weak var imageTimeClock: UIImageView!
    @IBOutlet weak var lblCarPrice: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblCarName: UILabel!
    
    @IBOutlet weak var lblArivalTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
      
      
        // Configure the view for the selected state
    }

func vehicleImageForType(dicVehicleInfo:[String:String] ,activeIcon:Bool)-> NSData{
        var vehicleImage_Data : NSData? = nil
        
       
        var iconTypeKey : String! = ""
        iconTypeKey = activeIcon ? "active_icon" : "inactive_icon"
        let keyForImage = dicVehicleInfo["id"]! + iconTypeKey
        vehicleImage_Data = Globals.sharedInstance.getValueFromUserDefaultsForKey(keyForImage) as? NSData
        if vehicleImage_Data == nil {
                let cars_Icon: NSURL = NSURL(string: dicVehicleInfo[iconTypeKey]!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)!
                
                vehicleImage_Data = NSData(contentsOfURL: cars_Icon)!
            
                Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(vehicleImage_Data!, key: keyForImage)
            }
 
        return vehicleImage_Data!
    }
func SetFontColorOfCellSubViews(cell:CarsAvailableCellOnDemand, color:UIColor){
    cell.lblCarName.textColor = color
    cell.lblArivalTime.textColor = color
    cell.lblCarPrice.textColor = color
    
    }

}
