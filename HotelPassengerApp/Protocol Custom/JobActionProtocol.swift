//
//  BackButtonProtocol.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 18/01/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import Foundation
import UIKit
@objc  protocol JobActionProtocol
{
   @objc optional func jobAccepted(_ dicNotification:[String:Any])
   @objc optional  func jobRejected(_ dicNotification:[String:Any])
   @objc optional func JobReceiptRecieved(_ dicNotification:[String:Any])
   @objc optional func jobStatusChanged(_ strStatus:String, dicNotification:[String:Any])
   @objc optional func getupdatedAddress(_ dicNotification:[String:Any])
    @objc optional func dropOffDoneByDriver(_ dicNotification:[String:Any])
   @objc optional func  jobCancelled(_ dicNotification:[String:Any]?)
}
