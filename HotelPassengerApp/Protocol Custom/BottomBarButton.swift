//
//  BottomBarButton.swift
//  MindMe
//
//  Created by Wei Zhang on 3/27/15.
//  Copyright (c) 2015 MindMe Mobile. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class BottomBarButton : UIButton {
    @IBInspectable
    var borderColor : UIColor = UIColor(red: 94.0/255.0, green: 193.0/255.0, blue: 56.0/255.0, alpha: 1.0) {
        didSet {
            self.setNeedsDisplay();
        }
    }
    @IBInspectable
    var highlightedBorderColor : UIColor = UIColor.gray {
        didSet {
            self.setNeedsDisplay();
        }
    }
    @IBInspectable
    var innerColor : UIColor = UIColor.white {
        didSet {
            self.setNeedsDisplay();
        }
    }
    @IBInspectable
    var highlightedInnerColor : UIColor = UIColor(red: 94.0/255.0, green: 193.0/255.0, blue: 56.0/255.0, alpha: 1.0) {
        didSet {
            self.setNeedsDisplay();
        }
    }
    @IBInspectable
    var cornerRadius : CGFloat = 5 {
        didSet {
            self.setNeedsDisplay();
        }
    }
    @IBInspectable
    var borderWidth : CGFloat = 2 {
        didSet {
            self.setNeedsDisplay();
        }
    }
    
    @IBInspectable
    var touchSelected : Bool = false {
        didSet {
            self.setNeedsDisplay();
        }
    }
    
    
    //TODO: add highlighted Colors
    
    override
    init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required
    init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override
    var isHighlighted: Bool {
        didSet {
            self.setNeedsDisplay();
        }
    }
    
    override
    func draw(_ rect: CGRect) {
       // let context = UIGraphicsGetCurrentContext();
        let borderOffset = borderWidth / 2;
        
        let borderRect = CGRect(x: rect.origin.x + borderOffset, y: rect.origin.y + borderOffset, width: rect.size.width - borderWidth, height: rect.size.height - borderWidth);
        let roundedRectanglePath = UIBezierPath(roundedRect: borderRect, cornerRadius: cornerRadius)
        roundedRectanglePath.lineWidth = borderWidth
        
        if (self.isHighlighted) {
            highlightedBorderColor.setStroke()
            highlightedInnerColor.setFill()
        } else {
            borderColor.setStroke()
            innerColor.setFill()
        }
        roundedRectanglePath.stroke()
        roundedRectanglePath.fill()
    }
    
    
    
}
