//
//  FlightDataParser.h
//  HotelPassengerApp
//
//  Created by netquall on 31/August/2016.
//  Copyright © 2016 Netquall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlightDataParser : NSObject
-(void)retrieveFlightDataWithAirportCode:(NSString*)airportCode airlineCode:(NSString*)AICode  flightID:(NSString*)flightID;
@end
