//
//  Constants.swift
//  PassangerApp
//
//  Created by Netquall on 11/30/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import Foundation
import UIKit
public enum iconImageType : String {
    case  map_icon
    case active_icon
    case inactive_icon
}

class Constants {
    enum iconImageType : String {
        case  map_icon
        case active_icon
        case inactive_icon
    }
    enum CreditCardUrlType: Int {
        case getCards
        case deleteCard
        case updateCard
        case addUserCard
        case searchCard
        case addPreferredCard
    }
    enum ViewSelected: Int {
        case firstView = 0
        case secondView
        case thirdView
    }
    /*
     VIP Global (149)---
     ETSAMS (166)
     Global Chauffeur(http://globalchauffeur.co.uk/) (248)---
     Blackbird (26) google key "AIzaSyCqttR69eErMzHM8F5Xi87tcFlKNTPagHU"
     Luxury Ride (117)
     CTG (240)
     Hallsley Limo (128), As per discussion with Kalpana company name is (JAMES LIMOUSINE SERVICE)
     Dansk Limousine Service (165)
     city trans ---118
     park limo 103
     NUBE - 342
     www.ClassiqueLimo.com - 146
     PRestige - 275
     //Wynne = 174
     */
    //jaitechme - 70
    //ground wedget 382
    
    //blackbird
    //static let placeApiKey = "AIzaSyCqttR69eErMzHM8F5Xi87tcFlKNTPagHU"
  //  static let placeApiKey = "AIzaSyCYm7bquFMfpUiDp2g1bcqiK1pGFtALz9U" //meridian limo
    static let placeApiKey = "AIzaSyDsimGbbg1evgHgu1886haO0jSSV7veHFk" //ocean limo
    static let appDelegate = UIApplication.shared.delegate! as! AppDelegate
    static let DeviceToken = "DeviceToken"
    static let  CompanyID =  "102"//"382" //157 miles//482- ride // 501 - LG // 246- A1Limo //460 apollo ATS // 146 - Classique Limo 437 - Ocian limo
    static let RoyalLimo = "355"
    static let needForHotel = "0" //"1"
    static let GC_THEME = "0" //"1"
    static let partyBus = "Party BUS" //for company specific case for Royal limo
    static let countyApp = "0" //"0"
    
    
    static let ondemandCollectionView = "1" //"1"

    //company id - 122 , googleAPI key - AIzaSyBks4d8-HJGoDXAKnyBpfDGYuTHQnEsOTs
    //company id - 281 , googleAPI key - "AIzaSyCYm7bquFMfpUiDp2g1bcqiK1pGFtALz9U"
    //LetDrive company id - 522 , gooleAPI key - AIzaSyB45_jRQ2X1QP2N4vSgRBF7mSXuPCQQ_EA
    //RIDE company ID - 482 Google key: AIzaSyCB3yiEHkeAeVBi8cyLkYDCGrrGXBAJIT0
    //Masterslimo company ID - 408, Google key:AIzaSyA_9B6EVR7or6VfKscie6_NQJtGWth69js
    //GOOLGE KEYS
    /*
     Matrix: AIzaSyDuyjohrmoUmyJOJnixC1S48723iY2h2e8
     */
    
    static var reservationUpdated:Bool! = false
    static var isBookerLogin:Bool! = false
    
    // MARK:- ====webservice static url ===========
    
    // static let baseUrl = "https://gmate.loginla.com/webservice_rates/"
    // static let baseUrl = "https://staging.loginla.com/webservice/"
    // static let baseUrl = "https://deploy.loginla.com/webservice/"
    //static let baseUrl = "https://api.loginla.com/webservice/"
    static let baseUrl = "https://app.loginla.com/webservice/"
    // static let baseUrl = "https://goldami.loginla.com/webservice/"
    // static let baseUrl = "https://appdeploy.loginla.com/webservice/"
    
    static let login = "login"
    static let signUp = "signup"
    static let signUp_step3 = "signup_step2"
    static let cancel_registraion = "delete_user"
    static let getBillingTypeInfo = "get_billing_type"
    static let getUserCreditCards = "get_user_credit_cards"
    static let deleteUserCreditCard = "delete_user_credit_card"
    static let getUserPreferredCards = "get_user_credit_cards_information"
    static let updateCreditCard = "update_credit_card"
    static let addUserCard = "add_user_credit_card"
    static let getNearestCars = "get_nearest_cars"
    static let getNearestCarsNew = "get_nearest_carsDataNew"
    static let bookaCar_passenger = "book_car_passenger1"
    static let cancel_Booking = "cancel_booking1"
    static let cancel_ongoing_Reservation = "cancel_booking"
    
    static let verifycompany = "verifycompany"
    static let signup_step2 = "signup_step2"
    static let forgetPassword = "forgot_password_send_email"
    // MARK:- ====Notifications  ===========
    
    static let kUpdateJobListNotification = "updatedJobListView"
    
    static let kUpdateAddressSearchView = "kUpdateAddressSearchView"
    
    // Mark: - BOOKER SECTION
    
    static let getAccoutListing = "get_passenger_by_bookerid"
    static let removeAccount = "delete_booker_passenger"
    static let accoutPaymentHistory = "booker_payment_history"
    // MARK:- ====User Deafult Keys for active info  ===========
    
    static let allRequestedDrivers = "requestedDriverIds"
    static let getAllFavDrivers = "favouriteList"
    static let ActiveUserInfo = "ActiveUserInfo"
    static let GetCurrentLocation = "GetCurrentLocation"
    static let ActiveCardInfo = "ActiveCardInfo"
    static let DriverInfo = "DriverInfo"
    static let BookACar = "book_car"
    static let LoginID: String = "LoginID"
    static let Password: String = "Password"
    
    static let KeepMeLogin = "KeepMeLogin"
    static let SignUpEmail: String = "SignUpEmail"
    static let SignUpmobile: String = "SignUpmobile"
    static let updateNewAddress:String = "get_route_path"
    static let updateDropoffAddress = "save_path"
    
    static let getDriverUpdates:String = "previous_job_status"
    static let getFareQuote = "fare_receipt"
    static let getPreviousJobStaus = "getactivejobdata"
    static let updateProfileImage:String = "updateProfileImage"
    
    static let shareTripDetails = "shareJob"
    static let SignUpProcessData = "signUpData"
    
    static let aliasCompanyID = "aliasID"
    // MARK:- ==== User Deafult Keys for Images for cars ===========
    static let SedanImage: String = "SedanImage"
    static let SedanPfremiumImage: String = "SedanPfremiumImage"
    static let SuvImage: String = "SuvImage"
    static let ImageData: String = "ImageData"
    static let DriverId: String = "DriverId"
    static let CancelReservationId: String = "CancelledReservationId"
    
    // MARK:- ==== User Deafult Keys for Notifications ===========
    static let activeJobNotification = "activeJobNotification"
    static let currentJobStaus = "currentJobStaus"
    
    static let strNoCars = "No Cars"
    static let carsActive = "carsActive"
    
    // MARK:-==== search places user deafult keys
    static let HomeAddress = "HomeAddress"
    static let WorkAddress = "WorkAddress"
    
    static let TotalCardsForPayment = "TotalCardsForPayment"
    static let screenHeight = UIScreen.main.bounds.size.height
    
    static var iPadScreen:Bool! = false
    // MARK:- ==== Notifications   ===========
    static let DriverAcceptedJobNotification = "DriverAcceptedJobNotification"
    
    // MARK:-===== Keys for Driver Profilee or rate Card
    static let DriverName = "driverName"
    static let DriverLicence = "DriverLicence"
    static let VehicleName = "VehicleName"
    static let DriverPhone = "DriverPhone"
    static let DriverImage = "DriverImage"
    
    static let MinFare = "MinFare"
    static let PerMinRate = "PerMinRate"
    static let PerMileRate = "PerMileRate"
    static let PerKMRate = "PerKMRate"
    static let BaseFare = "BaseFare"
    
    // MARK:- ===ride later section keys ====
    
    static let createReservation = "create_reservation"
    static let getVehicleTypes = "get_vechile_type"
    
    static let Service = "Service Type"
    static let PickUpDate = "PickUpDate"
    static let PassangerName = "Passenger Name"
    static let PassangerNumber = "Passenger Contact Number"
    static let PickType = "PickType"
    static let DropType = "DropType"
    static let Vehicle = "Vehicle"
    static let Hourly = "Add Hours"
    static let Payment = "Payment"
    static let AdultLagguge = "AdultLagguge"
    static let NoOfPax = "NoOfPassengers"
    static let EstimateFare = "EstimateFare"
    static let Stops = "Stops"
    static let PickAddress = "PickAddress"
    static let PickNotes = "PickNotes"
    static let DropAddress = "DropAddress"
    static let DropNotes = "DropNotes"
    static let ChildSeatType = "ChildSeatType"
    static let ChildCount = "ChildCount"
 
  
}
