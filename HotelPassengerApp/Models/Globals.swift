//
//  Globals.swift
//  PassangerApp
//
//  Created by Netquall on 12/4/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import CoreLocation
import EventKit
import EventKitUI
import Toaster

let appDelegate = UIApplication.shared.delegate! as! AppDelegate
@objc class Globals: NSObject {
    
    var delegateBack : BackButtonActionProtocol!
    // MARK:============== Singleton Object=======
    static let sharedInstance: Globals = {
        let instance = Globals()
        // setup code
        return instance
    }()
    // MARK:==============Property Declaration ======
    var activeDriver : DriverDetails!
    
    
    // MARK:============== User Defaulst value Get and Set=======
    func saveValuetoUserDefaultsWithKeyandValue(_ value:Any?, key: String){
        UserDefaults.standard.setValue(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    func removeValuetoUserDefaultsWithKey( _ key: String){
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func getValueFromUserDefaultsForKey(_ key: String) -> Any!{
        if let value = UserDefaults.standard.value(forKey: key){
            return value as Any!
        }else{
            return nil
        }
    }
    func getValueFromUserDefaultsForKey_Path(_ keyPath:String) -> Any! {
        if let value = UserDefaults.standard.value(forKeyPath: keyPath)
        {
            return value as Any!
        }else {
            return nil
        }
        
    }
    func updateCustomAddressValuestoUserDefaultsForKey(_ key:String, value:String, forDefaultKey:String, forKeyPath:Bool) {
        
        if var savedObj = Globals.sharedInstance.getValueFromUserDefaultsForKey(forDefaultKey) as? [String:Any]
        {
            if var dict = savedObj["custom"] as? [String:Any]{
                dict[key] = value as Any?
                
                savedObj["custom"] = dict as Any?
                Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(savedObj as Any?, key: forDefaultKey)
            }
        }
    }
    func updateValuetoUserDefaultsForKey(_ key:String, value:String, forDefaultKey:String) {
        
        if var savedObj = Globals.sharedInstance.getValueFromUserDefaultsForKey(forDefaultKey) as? [String:Any]
        {
            if let _ = savedObj[key] {
                savedObj[key] = value as Any?
            }
            else {
                savedObj[key] = value as Any?
            }
            Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(savedObj as Any?, key: forDefaultKey)
        }
    }
    // MARK:- ==== map api address====
    func getFullApiAddressForDistanceToDriverDistance(_ sourceLoc:CLLocation?, destinationLoc : CLLocation ) -> String{
        
        if sourceLoc == nil {
            return ""
        }
        // let locationObject = LocationManager.sharedInstance
        let strKey = "VF7U0akPlqBg4KQqyCpJCxoY4Ow="
        let url = "/maps/api/directions/json?origin=\(sourceLoc!.coordinate.latitude),\(sourceLoc!.coordinate.longitude)&destination=\(destinationLoc.coordinate.latitude),\(destinationLoc.coordinate.longitude)&sensor=false&client=gme-netqualltechnologies1"
        let binarySignData = url.hmacsha1(strKey)
        let strFinalUrl = "http://maps.googleapis.com\(url)&signature=\(binarySignData)"
        return strFinalUrl
        
    }
    func getFullApiAddressForAddressString(_ addressString:String?) -> String{
        
        if addressString == nil {
            return ""
        }
        // let locationObject = LocationManager.sharedInstance
        let strKey = "VF7U0akPlqBg4KQqyCpJCxoY4Ow="
        let urlString = "/maps/api/geocode/json?sensor=false&address=\(addressString!)&client=gme-netqualltechnologies1"
        //let url = "/maps/api/directions/json?origin=\(sourceLoc!.coordinate.latitude),\(sourceLoc!.coordinate.longitude)&destination=\(destinationLoc.coordinate.latitude),\(destinationLoc.coordinate.longitude)&sensor=false&client=gme-netqualltechnologies"
        let binarySignData = urlString.hmacsha1(strKey)
        let strFinalUrl = "http://maps.googleapis.com\(urlString)&signature=\(binarySignData)"
        
        print(strFinalUrl)
        return strFinalUrl
    }
    
    
    // MARK:-============= Left Bar Button =========
    func leftBackButtonWithCustom(_ title:String, imageName : String) -> UIBarButtonItem{
        let navLeftBtn: UIButton = UIButton(type: .custom)
        navLeftBtn.addTarget(self, action: #selector(Globals.leftBarButtonClicked(_:)), for: .touchUpInside)
        navLeftBtn.setTitle(title, for: UIControlState())
        navLeftBtn.setTitleColor(UIColor.darkBlack(), for: UIControlState())
        navLeftBtn.setImage(UIImage(named: imageName), for: UIControlState())
        navLeftBtn.contentMode = .scaleAspectFit
        navLeftBtn.titleLabel?.font = Globals.defaultAppFontWithBold(16)
        navLeftBtn.contentHorizontalAlignment = .left
        if Constants.iPadScreen == true {
       //     navLeftBtn.titleLabel!.font = UIFont(name: "Oxygen-bold", size: 20)
            navLeftBtn.frame = CGRect(x: 5, y: 0,width: 300, height: 40)
        }else {
        //    navLeftBtn.titleLabel!.font = UIFont(name: "Oxygen-bold", size: 16)
            navLeftBtn.frame = CGRect(x: 5, y: 0,width: 230, height: 40)
        }
        navLeftBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -2, 0, 0)
        navLeftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -8, 0, 0)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: navLeftBtn)
        return leftBarButton
    }
    func leftBarButtonClicked(_ sender : UIButton){
        if self.delegateBack != nil {
            self.delegateBack.backButtonActionMethod(sender)
        }
    }
     // MARK:-============= date Formator Methods =========
    func dateFromTimestamp(_ timeStamp:Double) -> String{
        let date:Date = Date(timeIntervalSinceNow: timeStamp)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy/MM/dd"
        return dateFormatter.string(from: date)
    }
    
    // MARK :- get Minutes for user Wait for Car Arival
    func getCurrentDate() -> String {
        let today: Date = Date()
        let dateFormat: DateFormatter = DateFormatter()
        dateFormat.timeZone = TimeZone.current

        dateFormat.dateFormat = "yyyy-MM-dd HH:mm"
        let dateString: String = dateFormat.string(from: today)
        return dateString
    }
    
    // MARK :- get Minutes for user Wait for Car Arival
    func getCurrentDateforOndemandCars() -> String {
        let today: Date = Date()
        let dateFormat: DateFormatter = DateFormatter()
        dateFormat.timeZone = TimeZone.current

        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString: String = dateFormat.string(from: today)
        return dateString
    }
    func getDateFormatForDateString(_ date:String) -> String{
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm a"
        if let current_Date: Date = dateFormatter.date(from: date){
            let dateFormatter2: DateFormatter = DateFormatter()
            dateFormatter2.timeZone = TimeZone.current

            dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm"
            let dateText: String = dateFormatter2.string(from: current_Date)
        return dateText
        }
        else {
            return date
        }
    }
    
    // MARK :- Get Address Components from map
    func formatedAddressCompnentsSwift(_ dicAddress:[String:Any]) -> [String:Any]{
        var dictAddressComp = [String:Any]()
        dictAddressComp["address"] =  dicAddress ["formatted_address"]
        let dicGeometry = (dicAddress["geometry"]! as! [String:Any])["location"]! as! [String:Any]
        dictAddressComp["placeID"] = dicAddress["place_id"]!
        dictAddressComp["latitude"] = dicGeometry["lat"]
        dictAddressComp["longitude"] = dicGeometry["lng"]
        var address: [String] = [String]()
        let arrAddresComponents = (dicAddress["address_components"] as? [[String : Any]])
        for var add_comp in arrAddresComponents!{
            if let _ = add_comp["types"] {
                let types =  (add_comp["types"] as! [String]).first!
                if types.characters.count > 0 {
                    if types.compare("street_number")  {
                        dictAddressComp["street_number"] = add_comp["long_name"]
                        if address.contains(add_comp["long_name"] as! String) {
                            address.append(add_comp["long_name"] as! String)
                        }
                    }else  if types.compare("route")  {
                        dictAddressComp["route"] = add_comp["long_name"]
                        if address.contains(add_comp["long_name"] as! String) {
                            address.append(add_comp["long_name"] as! String)
                        }
                    }else  if types.compare("neighborhood")  {
                        dictAddressComp["neighborhood"] = add_comp["long_name"]
                        if address.contains(add_comp["long_name"] as! String) {
                            address.append(add_comp["long_name"] as! String)
                        }
                    }else  if types.compare("sublocality_level_1")  {
                        dictAddressComp["sublocality_level_1"] = add_comp["long_name"]
                        if address.contains(add_comp["long_name"] as! String) {
                            address.append(add_comp["long_name"] as! String)
                        }
                    }else  if types.compare("sublocality_level_2")  {
                        dictAddressComp["sublocality_level_2"] = add_comp["long_name"]
                        if address.contains(add_comp["long_name"] as! String) {
                            address.append(add_comp["long_name"] as! String)
                        }
                    }else  if types.compare("sublocality_level_3")  {
                        dictAddressComp["sublocality_level_3"] = add_comp["long_name"]
                        if address.contains(add_comp["long_name"] as! String) {
                            address.append(add_comp["long_name"] as! String)
                        }
                    }else  if types.compare("locality")  {
                        dictAddressComp["locality"] = add_comp["long_name"]
                        if address.contains(add_comp["long_name"] as! String) {
                            address.append(add_comp["long_name"] as! String)
                        }
                    }else  if types.compare("administrative_area_level_1")  {
                        dictAddressComp["administrative_area_level_1"] = add_comp["long_name"]
                        if address.contains(add_comp["long_name"] as! String) {
                            address.append(add_comp["long_name"] as! String)
                        }
                    }else  if types.compare("country")  {
                        dictAddressComp["country"] = add_comp["long_name"]
                        if address.contains(add_comp["long_name"] as! String) {
                            address.append(add_comp["long_name"] as! String)
                        }
                    }else  if types.compare("postal_code")  {
                        dictAddressComp["postal_code"] = add_comp["long_name"]
                        if address.contains(add_comp["long_name"] as! String) {
                            address.append(add_comp["long_name"] as! String)
                        }
                    }
                }
            }
        }
        return dictAddressComp
    }
    func formatArrivalTimeStringWith(_ strTimeArrival:String)-> String{
        var strTimeArrival = strTimeArrival
        if strTimeArrival.contains("hours") || strTimeArrival.contains("hour") || strTimeArrival.contains("mins") || strTimeArrival.contains("min"){
        if strTimeArrival.contains("hours") {
            strTimeArrival = strTimeArrival.replacingOccurrences(of: "hours", with: "h")
            strTimeArrival = strTimeArrival.replacingOccurrences(of: "mins", with: "m")
            strTimeArrival = strTimeArrival.replacingOccurrences(of: "min", with: "m")
             return strTimeArrival
        }
        if strTimeArrival.contains("hour") {
            strTimeArrival = strTimeArrival.replacingOccurrences(of: "hour", with: "h")
            strTimeArrival = strTimeArrival.replacingOccurrences(of: "mins", with: "m")
            strTimeArrival = strTimeArrival.replacingOccurrences(of: "min", with: "m")
             return strTimeArrival
        }
        if strTimeArrival.contains("mins") {
            return strTimeArrival //= strTimeArrival.stringByReplacingOccurrencesOfString("mins", withString: "m")
        }
        if strTimeArrival.contains("min") {
            return strTimeArrival //= strTimeArrival.stringByReplacingOccurrencesOfString("min", withString: "m")
        }
        }
       else {
            if strTimeArrival == "N/A" || strTimeArrival == ""{
                return "Loading"
            }
             if strTimeArrival == "0"{
                return "0 m"
            }
            return "\(strTimeArrival) "
        }
        return strTimeArrival
    }
    func getdateComponentsforDate(_ date: String) -> [String] {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current

        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if dateFormatter.date(from: date) != nil{
        let current_Date: Date = dateFormatter.date(from: date)!
        // var activeUserInfo: [NSObject : Any] = NSUserDefaults.standardUserDefaults()(valueForKey: kActiveUserInfo)
        let strDateFormat: String = "dd-MMM-HH:mm"
        //        if (activeUserInfo["time_format"] == "H:M24") {
        //            strDateFormat = strDateFormat.stringByAppendingString("-HH:mm")
        //        }
        //        else {
        //            strDateFormat = strDateFormat.stringByAppendingString("-hh:mm a")
        //        }
        let dateFormatter2: DateFormatter = DateFormatter()
            dateFormatter2.timeZone = TimeZone.current

        let locale: Locale = Locale(identifier: "en_US_POSIX")
        dateFormatter2.locale = locale
        dateFormatter2.dateFormat = strDateFormat
        let dateText: String = dateFormatter2.string(from: current_Date)
        return dateText.components(separatedBy: "-")
        }
        return "00-000-00:00".components(separatedBy: "-")
    }
    func ToastAlertInstance(_ strMessage:String){
        
        //Toast(text: strMessage).show()

        if let rootView  = appDelegate.window?.rootViewController {
            if let  visible = (rootView as! UINavigationController).visibleViewController {
            visible.showBannerWithMessage(strMessage)
            }
        }
    }
    func ToastAlertForInfo(_ strMessage:String){
        
        //Toast(text: strMessage).show()
        
        if let rootView  = appDelegate.window?.rootViewController {
            if let  visible = (rootView as! UINavigationController).visibleViewController {
                visible.showInfoBannerWithMessage(strMessage)
            }
        }
    }
    
    // MARK:-===Toast Alert Methods=====
    // MARK:-  ===class methods=====
    // MARK:-======== Set Corner and Border Witdh  ======
    
    public class func layoutViewFor(_ view:UIView?, color:UIColor?, width:CGFloat?, cornerRadius:CGFloat)
    {
        if view != nil {
            if color != nil && width != nil {
                view!.layer.borderColor = color!.cgColor
                view!.layer.borderWidth = width!
            }
            view!.layer.cornerRadius = cornerRadius
            view!.layer.masksToBounds = true
            view!.clipsToBounds = true
        }
    }
    class func layoutViewForBorder(_ view:UIView?)
    {
        if view != nil {
            view!.layer.borderColor = UIColor.white.cgColor
            view!.layer.borderWidth = 1
            view!.clipsToBounds = true
        }
        
    }
    // MARK:-  ===class methods=====
     // MARK:-===Log Out User=====
    class func logout(){
        Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.ActiveUserInfo)
        Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.ActiveCardInfo)
        Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.TotalCardsForPayment)
        Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.activeJobNotification)
        Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.currentJobStaus)
        Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(false as Any? , key: Constants.KeepMeLogin)
    }
    // MARK:-===Font Methods for iPAd=====
class func defaultAppFont(_ size:Float) -> UIFont {
    if Constants.iPadScreen == true {
            return UIFont(name: "Oxygen", size: CGFloat(size * 1.5))!
        }
        return UIFont(name: "Oxygen", size: CGFloat(size))!
    }
class func defaultAppFontWithBold(_ size:Float) -> UIFont {
    if Constants.iPadScreen == true {
            return UIFont(name: "Oxygen", size: CGFloat(size * 1.5))!
        }
        return UIFont(name: "Oxygen-bold", size: CGFloat(size))!
    }

class func defaultAppFontItalic(_ size:Float) -> UIFont {
    if Constants.iPadScreen == true {
        return UIFont(name: "Oxygen-italic", size: CGFloat(size * 1.5))!
    }
    return UIFont(name: "Oxygen-italic", size: CGFloat(size))!
    }

    // MARK:-===Toast Alert Methods=====
    class func ToastAlertForNetworkConnection(){
        if Constants.CompanyID == Constants.RoyalLimo {  // Rashpinder Changes Royal Limo
            let alertView = UIAlertView(title: "Internet Error", message: "Please try back later or contact 1866-690-2200 for customer support.", delegate:nil, cancelButtonTitle: "Ok")
            alertView.show()
            return
        }
        if let _ = appDelegate.window?.rootViewController {
            Toast(text: "Please check your network connection").show()
        }
        
    }
    
    
    class func ToastAlertWithString(_ strMessage:String){
        if strMessage.compare("Netowrk Error") { // Rashpinder Changes Royal Limo
            if Constants.CompanyID == Constants.RoyalLimo {
                let alertView = UIAlertView(title: "Internet Error", message: "Please try back later or contact 1866-690-2200 for customer support.", delegate:nil, cancelButtonTitle: "Ok")
                alertView.show()
                return
            }
        }
        Toast(text: strMessage).show()
    }
    
    
    //authorizationForCalendar
    lazy var eventStore: EKEventStore! = {
        let eventStore = EKEventStore()
        return eventStore
    }()
    
  
    //MARK: ---- Create Property list for storing calendar events -------
    func addEventwithEventID(_ eventID:String, reservation_id:String) {
        let fileManager = FileManager.default
        
        let doc_Directory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = doc_Directory + "/Events.plist"
        
        if(!fileManager.fileExists(atPath: path)){
            let data:[String: String] = [reservation_id: eventID]
            let someData = NSMutableDictionary(dictionary: data)
            let isWritten = someData.write(toFile: path, atomically: true)
            
        }else{
            let existingData:NSMutableDictionary = NSMutableDictionary(contentsOfFile: path)!
            existingData.setValue(eventID, forKey: reservation_id)
            let someData = NSDictionary(dictionary: existingData)
            let isWritten = someData.write(toFile: path, atomically: true)
        }
    }
    func deleteEventWithReservationID(_ reservation_id:String) {
        
    }
    func getEventWithReservationID(_ reservation_id:String) -> String {
        let fileManager = FileManager.default
        
        let doc_Directory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = doc_Directory + "/Events.plist"
        
        if(!fileManager.fileExists(atPath: path)){
            print("file exists")
            return ""
        }else{
            print("file exists")
            let existingData:NSMutableDictionary = NSMutableDictionary(contentsOfFile: path)!
            let someData = NSDictionary(dictionary: existingData)
            return "\(someData.value(forKey: reservation_id)!)"
        }
        return ""
    }
//MARK: ============= Change date format to same as portal ==========
    class func getFormattedDateString(_ date: String) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current

        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if dateFormatter.date(from: date) != nil{
            let current_Date: Date = dateFormatter.date(from: date)!
            let dicActiveUser = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
            var strDateFormat: String = "\(dicActiveUser["date_format"]!)".lowercased()

            if strDateFormat.range(of: "m") != nil {
                strDateFormat = strDateFormat.replacingOccurrences(of: "m", with: "M")
            }
            if dicActiveUser["time_format"] as! String == "H:M24" {
                strDateFormat = strDateFormat + " HH:mm"
            }
            else {
                strDateFormat = strDateFormat + " hh:mm a"
            }
            let dateFormatter2: DateFormatter = DateFormatter()
            let locale: Locale = Locale(identifier: "en_US_POSIX")
            dateFormatter2.timeZone = TimeZone.current

            dateFormatter2.locale = locale
            dateFormatter2.dateFormat = strDateFormat
            let dateText: String = dateFormatter2.string(from: current_Date)

            return dateText
        }
        return "0000:00:00 00:00"
    }
    class func getDateFormatForDateString(_ date: String) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if dateFormatter.date(from: date) != nil{
        let current_Date: Date = dateFormatter.date(from: date)!
        let dicActiveUser = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        var strDateFormat: String = "\(dicActiveUser["date_format"]!)".lowercased()
        if strDateFormat.range(of: "m") != nil {
            strDateFormat = strDateFormat.replacingOccurrences(of: "m", with: "M")
        }
        if dicActiveUser["time_format"] as! String == "H:M24" {
            strDateFormat = strDateFormat + "&HH:mm"
        }
        else {
            strDateFormat = strDateFormat + "&hh:mm a"
        }
        let dateFormatter2: DateFormatter = DateFormatter()
        let locale: Locale = Locale(identifier: "en_US_POSIX")
        dateFormatter2.locale = locale
        dateFormatter2.dateFormat = strDateFormat
        let dateText: String = dateFormatter2.string(from: current_Date)
            return dateText
        }
        return "0000:00:00 00:00"
    }
    
}// class brackets
