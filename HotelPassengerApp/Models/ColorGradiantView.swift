//
//  ColorGradiantView.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 19/01/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import Foundation
class Colors {
    let colorleft = UIColor(red: 37.0/255.0, green: 37.0/255.0, blue: 37.0/255.0, alpha: 1.0).cgColor
     let gl: CAGradientLayer
    
    init() {
        gl = CAGradientLayer()
        gl.startPoint = CGPoint(x: 0, y: 0.5);
        gl.endPoint = CGPoint(x: 1.0, y: 0.5);
        gl.colors = [ colorleft]
        gl.locations = [ 0.0,0.40,0.60, 1.0]
    }
}
