//
//  LocationManager.swift
//  PassangerApp
//
//  Created by Netquall on 12/4/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import CoreLocation

class LocationManager: NSObject,CLLocationManagerDelegate {
    
 //   typealias didUpdateLocation = (_ location: [Any], _ locationManager: CLLocationManager) -> ()
  //  typealias didFailLocationUpdate = (_ error:NSError, _ locationManager: CLLocationManager) -> ()
    typealias CompitaionBlock = (_ location: [Any], _ locationManager: CLLocationManager) -> ()
    typealias FailureBlock = (_ error:NSError, _ locationManager: CLLocationManager) -> ()
    //  var _didUpdateLocation: didUpdateLocation?
    //  var _didFailLocationUpdate: didFailLocationUpdate?
    lazy var currentLocation : CLLocation? = CLLocation()
    lazy var pickUpLocation : CLLocation? = CLLocation()
    lazy var carNearestLocation : CLLocation? = CLLocation()
    var destinationLocation : CLLocation?
    lazy var dicDestinationLocation : [String:Any]? = [String:Any]()
    lazy var dicSourceCustomLocation : [String:Any]? = [String:Any]()
    lazy var strCurrentLocation = String()
    lazy var getCurrentLocation = Bool()
    var strVehicleType:String! = ""

    lazy var sharedInstanceCLLocation: CLLocationManager! = {
        let newLocationmanager = CLLocationManager()
        newLocationmanager.delegate = self
        newLocationmanager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        newLocationmanager.distanceFilter = 10
        return newLocationmanager
    }()
    
    static let sharedInstance: LocationManager = {
        let instance = LocationManager()
        // setup code
        return instance
    }()
    
    func startUpdatingLocation() {
        self.sharedInstanceCLLocation.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled(){
            
            self.sharedInstanceCLLocation.startUpdatingLocation()
            self.sharedInstanceCLLocation.startMonitoringSignificantLocationChanges()
//            if #available(iOS 9.0, *) {
//                self.sharedInstanceCLLocation.allowsBackgroundLocationUpdates = true
//            } else {
//                // Fallback on earlier versions
//            }
            self.sharedInstanceCLLocation.pausesLocationUpdatesAutomatically = false
            //        self.sharedInstanceCLLocat
        }
        
    }
    
    // MARK:- ============Stop Update Location ===========
    func stopUpdatingLocation() {
        self.sharedInstanceCLLocation.stopUpdatingLocation()
    }
    
    func getLocationUpdateWithCompletionHandler(_ completionHandler:@escaping (_ location: [Any], _ locationManager: CLLocationManager) -> () , failureHandler:@escaping (_ error:NSError, _ locationManager: CLLocationManager) -> () , withDesiredAccuracy desiredAccuracy: CLLocationAccuracy, distanceFilter: Double) {
        // _didUpdateLocation = completionHandler
        // _didFailLocationUpdate = failureHandler
        self.sharedInstanceCLLocation.delegate = self
        self.sharedInstanceCLLocation.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.sharedInstanceCLLocation.distanceFilter = 10
        
        let code = CLLocationManager.authorizationStatus()
        if (code == .notDetermined) {
            // choose one request according to your business.
            
            if (Bundle.main.object(forInfoDictionaryKey: "NSLocationAlwaysUsageDescription") != nil) {
                sharedInstanceCLLocation.requestAlwaysAuthorization()
            }
            else {
                
                if (Bundle.main.object(forInfoDictionaryKey: "NSLocationWhenInUseUsageDescription") != nil) {
                    sharedInstanceCLLocation.requestWhenInUseAuthorization()
                }
                else {
                    #if DEBUG
                        NSLog("Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription")
                    #endif
                }
            }
        }
        if CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            self.sharedInstanceCLLocation.startUpdatingLocation()
        }
        else {
            if CLLocationManager.authorizationStatus() == .restricted {
                let alert: UIAlertView = UIAlertView(title: "Alert!", message: "Location Services is disabled for this application. You can not use location services.", delegate: nil, cancelButtonTitle: "OK", otherButtonTitles:"")
                alert.show()
                //  let error = NSError(domain:"LocationManagerError", code:2 , userInfo: ["NSLocalizedDescriptionKey":"Location Services is disabled for this application You can not use location services"])
                //  _didFailLocationUpdate!(error, sharedInstanceCLLocation)
            }
            else {
                if CLLocationManager.authorizationStatus() == .denied {
                    let alert: UIAlertView = UIAlertView(title: "Alert!", message: "You have turned off location services for this program . Please enable it in the Device Settings", delegate: nil, cancelButtonTitle: "OK", otherButtonTitles:"")
                    alert.show()
                    //  let error: NSError = NSError(domain: "LocationManagerError", code:3, userInfo:["NSLocalizedDescriptionKey":"Location Services is disabled for this application You can not use location services"])
                    //  _didFailLocationUpdate!(error, sharedInstanceCLLocation)
                }
                else {
                    self.startUpdatingLocation()
                }
            }
        }
    }
    // MARK:- ============get Address of Current Location  ===========
    func MapStopScrollingAndGetAddressOfCurrentLocation(_ newLocation:CLLocation){
    
        GoogleMapAPIServiceManager.sharedInstance.getLocationAddressUsingLatitude(newLocation.coordinate.latitude, longitude: newLocation.coordinate.longitude, withComplitionHandler: { [weak self](dicRecievedJSON) -> () in
            if dicRecievedJSON["status"] as! String == "OK"{
                DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async(execute: { () -> Void in
                    self?.strCurrentLocation = (( dicRecievedJSON["results"]! as! [[String:Any]]).first! )["formatted_address"] as! String
                })
            }
        })
        { (error) -> () in
            
        }
        
        
    }
    
    
    
    // MARK:-=============CLLocationManager Delegates===========
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let newLocation = locations.last{
            //  self.currentLocation! = newLocation
            let locationAge = -(newLocation.timestamp.timeIntervalSinceNow)
            if locationAge > 15.0 {
                return
            }
            if newLocation.horizontalAccuracy < 0 {
                return
            }
            if self.currentLocation == nil || self.currentLocation!.horizontalAccuracy > newLocation.horizontalAccuracy{
                self.currentLocation! = newLocation
                if newLocation.horizontalAccuracy <= sharedInstanceCLLocation.desiredAccuracy{
                    NSObject.cancelPreviousPerformRequests(withTarget: self)
                }
            }
            // _didUpdateLocation!(location: locations,locationManager: self.sharedInstanceCLLocation)
            if Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.GetCurrentLocation) as! String != "yes"{
                weak var weakSelf = self
                Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue("yes" as Any?, key: Constants.GetCurrentLocation)
                DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: { () -> Void in
                    weakSelf?.MapStopScrollingAndGetAddressOfCurrentLocation(locations.last!)
                    weakSelf?.currentLocation = locations.last!
                })
            }
        }
        else {
//            let alert: UIAlertView = UIAlertView(title: "Alert!", message: "Not able to get your locaion. Please check your GPS settings.", delegate: nil, cancelButtonTitle: "OK", otherButtonTitles:"")
//            alert.show()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            self.sharedInstanceCLLocation.perform(#selector(LocationManager.startUpdatingLocation), with: nil, afterDelay: 0.0)
        }
        else {
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied  {
                
                let alert: UIAlertView = UIAlertView(title: "Location Services", message: "Location Services is disabled for this application. Please enable location form iPhone Settings > Select App > Location.", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
            }
            else {
                self.startUpdatingLocation()
            }
        }
    }
}
