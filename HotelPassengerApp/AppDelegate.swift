//
//  AppDelegate.swift
//  PassangerApp
//
//  Created by Netquall on 11/30/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import Fabric
import Crashlytics
import GooglePlaces


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var requestedDriverIDs:[String]! = [String]()
    
    var window: UIWindow?
    var networkAvialable : Bool! = false
    var reachability : Reachability!
    var PrgressView : UIView!
    var lblProgressText: UILabel!
    var thread : Thread!
    var activityindicatorView: UIActivityIndicatorView!
    var delegateJobStatusAction : JobActionProtocol!
    // distance
    var distanceType : String! // km or mile
    var distanceUnit : String! // km or mile
    var distanceMultiplier : Float! // Km -- 0.001 Miles -- 0.000621371
    
    var ondemandRadius:String!
    var currency : String!
    
    var showCompanyNameToPassanger : String!
    var time_format : String!
    var rideLater_storybaord = UIStoryboard(name:"Main", bundle: nil);

    // active reservation ID for all screens
    var activeReservationID:String = ""
    

    var storybaord : UIStoryboard!
    var activeUserDetailsForBooker:[String:Any] = [String:Any]()

    // MARK: - ===========application launch Methods==========================
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if let _ = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.DeviceToken) as? String {
            NSLog("%@",  Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.DeviceToken) as! String)
        }
        
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
        
        Fabric.with([Crashlytics.self])
     
        
        GMSServices.provideAPIKey(Constants.placeApiKey)
        GMSPlacesClient.provideAPIKey(Constants.placeApiKey)
        
        Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue("no", key: Constants.GetCurrentLocation)
        LocationManager.sharedInstance.startUpdatingLocation()
        // =====push notification Code ===
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        UIApplication.shared.registerForRemoteNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
        // ====reachability of Network ========
       
        if  let flag = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.KeepMeLogin) as? Bool
        {
            if flag == true{
                if let userInfo = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as? [String:Any] {
                    //print("active user Exit--\(userInfo)")
                    self.distanceType = userInfo["radiusunit"] as! String
                    self.distanceUnit = (userInfo["radiusunit"] as! String) == "mile" ? "Mile(s)": "Km(s)"
                    self.distanceMultiplier = self.distanceType == "mile" ? 0.000621371 : 0.001
                    self.currency = "\(userInfo["currency"] as! String)"
                    self.ondemandRadius = "\(userInfo["on_demand_radius"] as! String)"
                }
            }
        }
        
        if ( UIDevice.current.userInterfaceIdiom == .pad)//userInterfaceIdiom == .pad)
        {
            storybaord = UIStoryboard(name:"iPad_Main", bundle:nil);
            Constants.iPadScreen = true
        }
        else
        {
            Constants.iPadScreen = false
            storybaord = UIStoryboard(name:"Main", bundle: nil);
        }

        if Constants.countyApp == "1"{
            /*
             {
             "ac_status" = ACTIVE;
             "acc_comp_id" = 6332;
             "acc_comp_status" = active;
             "account_farmout_type" = 0;
             "account_type" = 2;
             "alias_id" = 0;
             appPermitCancellations = Y;
             "appli_access" = ALLOW;
             "company_id" = 40;
             currency = "$";
             "date_format" = "MM-DD-YYYY";
             email = "jatinder@netquall.com";
             "first_name" = park;
             id = 15777;
             "image_url" = "";
             "last_name" = Jw;
             mobile = "+1";
             "on_demand_radius" = 20;
             "profile_image" = "1474440731.png";
             "prohibitReservation_before" = 0;
             radiusunit = mile;
             session = 59789e9255bbf;
             showCompanyNameToPassanger = Y;
             "time_format" = "H.M";
             }
             */
     let userDict:[String:Any] = ["alias_id":"0",
                "appPermitCancellations":"Y",
                "company_id":"122",
                "currency":"$",
                "date_format":"MM-DD-YYYY",
                "email":"",
                "first_name":"",
                "id":"148456",
                "image_url":"",
                "last_name":"",
                "mobile":"+1",
                "on_demand_radius":"20",
                "profile_image":"",
                "prohibitReservation_before":"0",
                "radiusunit":"mile",
                "session":"abcd",
                "time_format":"H.M"]
            
//            let userDict:[String:Any] = ["alias_id":"0",
//                                         "appPermitCancellations":"Y",
//                                         "company_id":"40",
//                                         "currency":"$",
//                                         "date_format":"MM-DD-YYYY",
//                                         "email":"",
//                                         "first_name":"",
//                                         "id":"15777",
//                                         "image_url":"",
//                                         "last_name":"",
//                                         "mobile":"+1",
//                                         "on_demand_radius":"20",
//                                         "profile_image":"",
//                                         "prohibitReservation_before":"0",
//                                         "radiusunit":"mile",
//                                         "session":"59b934a2cbaaa",
//                                         "time_format":"H.M"]
            
            self.distanceType = "mile"
            self.distanceUnit = "Mile(s)"
            self.distanceMultiplier = 0.000621371
            
            self.currency = "$"
            
            Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(userDict, key: Constants.ActiveUserInfo)
            
            Constants.iPadScreen = true
              let storybaord_county_app = UIStoryboard(name:"RideLater", bundle: nil);
            

            let navigation = storybaord_county_app.instantiateInitialViewController() as! UINavigationController
            //self.window = UIWindow()
            self.window?.rootViewController? = navigation
        }else {
            let navigation = self.storybaord!.instantiateInitialViewController() as! UINavigationController
            //self.window = UIWindow()
            self.window?.rootViewController? = navigation
        }
        
        
        self.window?.makeKeyAndVisible()
        self.window?.tintColor = UIColor.appThemeColor()
        self.SetUpProgressViewCustom()

        // Override point for customization after application launch.
        reachability = Reachability.forInternetConnection()
        NotificationCenter.default.addObserver(self, selector:#selector(AppDelegate.reachabilityChanged(_:)), name: NSNotification.Name.reachabilityChanged, object: nil)
        reachability.startNotifier()
        if let status : NetworkStatus = reachability.currentReachabilityStatus(){
            if status == NotReachable{
                NSLog("%@", "not coonected ")
                self.networkAvialable = false
                let detailsVc = self.storybaord?.instantiateViewController(withIdentifier: "InternetErrorViewController") as! InternetErrorViewController
                detailsVc.modalPresentationStyle = .overFullScreen
                detailsVc.providesPresentationContextTransitionStyle = true
                detailsVc.definesPresentationContext = true
                self.window?.rootViewController!.providesPresentationContextTransitionStyle = true
                self.window?.rootViewController!.definesPresentationContext = true
                self.window?.rootViewController!.modalTransitionStyle = .crossDissolve
                self.window?.rootViewController!.modalPresentationStyle = .overFullScreen
                self.window?.rootViewController!.present(detailsVc, animated: true) {
                }
            }
            else {
                self.networkAvialable = true
                NSLog("%@", "internet connection found")
            }
        }
        if let dicNotification = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.activeJobNotification)
        {
            let driverObject = DriverDetails()
            driverObject.setCurrentJobValues(dicNotification as! [String : Any])
        }
        
        if let arrDriverIds = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.allRequestedDrivers)
        {
            self.requestedDriverIDs =  arrDriverIds as! [String]
        }
        
        var remoteNotification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [String : Any]
        if remoteNotification != nil {
            remoteNotification = remoteNotification!.formatDictionaryForNullValues(remoteNotification!)
            // NSLog("launch option notification %@", remoteNotification!)
            // self.handlePushNotificaionsWithDictionary(remoteNotification!)
        }
        
        return true
    }
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail("test")
        Crashlytics.sharedInstance().setUserIdentifier("12345")
        Crashlytics.sharedInstance().setUserName("test")
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        
        
        self.saveContext()
    }
    // MARK: - ===========reachability for network==========================
    func updateInterfaceWithReachability(_ hostReach:Reachability)
    {
        let status : NetworkStatus = hostReach.currentReachabilityStatus()
        if status == NotReachable{
            self.networkAvialable = false
            appDelegate.hideProgress()
            let detailsVc = self.storybaord?.instantiateViewController(withIdentifier: "InternetErrorViewController") as! InternetErrorViewController
            detailsVc.modalPresentationStyle = .overFullScreen
            detailsVc.providesPresentationContextTransitionStyle = true
            detailsVc.definesPresentationContext = true
            self.window?.rootViewController!.providesPresentationContextTransitionStyle = true
            self.window?.rootViewController!.definesPresentationContext = true
            self.window?.rootViewController!.modalTransitionStyle = .crossDissolve
            self.window?.rootViewController!.modalPresentationStyle = .overFullScreen
            self.window?.rootViewController!.present(detailsVc, animated: true) {
            }
            
        }else{
            self.networkAvialable = true
            self.window?.rootViewController?.dismiss(animated: true, completion: nil)
        }
        
        if !self.networkAvialable {
            Globals.sharedInstance.ToastAlertInstance("Please check your network connection")
        }
    }
    func reachabilityChanged(_ notification : Notification){
        let currntreach : Reachability = notification.object as! Reachability
        updateInterfaceWithReachability(currntreach)
    }
    
    
    func backToMainMenuView(){
        DispatchQueue.main.async { [weak self]() -> Void in
            if self == nil {
                return
            }
            
           // print("view class ----\((self?.window?.rootViewController as! UINavigationController).topViewController)")
            if self?.window?.rootViewController != nil {
                for vc: UIViewController in ((self?.window?.rootViewController as! UINavigationController).viewControllers) {
                   // print("view class ----\(vc.className)")
                    if (vc.isKind(of: SWRevealViewController.self)) {
                        (self?.window?.rootViewController as! UINavigationController).popToViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    // MARK: - ==========Progress View Custom ==========================
    func SetUpProgressViewCustom() {
        // progress View
        self.PrgressView = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height:  ScreenSize.SCREEN_HEIGHT))
        self.PrgressView.backgroundColor = UIColor.clear
        self.PrgressView.alpha = 1.0
        let view = UIView(frame: self.PrgressView.frame)
        view.backgroundColor = UIColor.white
        view.alpha = 0.60
        self.PrgressView.addSubview(view)
        self.activityindicatorView = UIActivityIndicatorView()
        self.activityindicatorView.center = CGPoint(x: PrgressView.frame.size.width / 2, y: PrgressView.frame.size.height / 2)
        self.activityindicatorView.activityIndicatorViewStyle = .whiteLarge
        self.activityindicatorView.color = UIColor.appThemeColor()
        self.activityindicatorView.startAnimating()
        self.PrgressView.addSubview(activityindicatorView)
        self.PrgressView.isHidden = true
        //progress.addSubview(activityindicatorView)
        self.lblProgressText = UILabel(frame: CGRect(x: 0, y: 0, width: self.PrgressView!.frame.width, height: 30))
        self.lblProgressText.center = CGPoint(x: self.PrgressView!.frame.size.width / 2, y: self.PrgressView!.frame.size.height / 2 - 40)
        self.lblProgressText.backgroundColor = UIColor.clear
        self.lblProgressText.font =  Globals.defaultAppFont(14)
        self.lblProgressText.textAlignment = .center
        self.lblProgressText.numberOfLines = 5
        self.lblProgressText.textColor = UIColor.appThemeColor()
        self.lblProgressText.contentMode = .center
        self.lblProgressText.lineBreakMode = .byWordWrapping
        self.PrgressView.addSubview(self.lblProgressText)
        self.PrgressView.sendSubview(toBack: view)
        self.window!.addSubview(self.PrgressView)
    }
    
       func SetUpProgressViewCustom1() {
     
     // progress View
     self.PrgressView = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height:  ScreenSize.SCREEN_HEIGHT))
     self.PrgressView.backgroundColor = UIColor.clear
     self.PrgressView.alpha = 1.0
     self.PrgressView.isHidden = true
     
     let viewMain = UIView(frame: self.PrgressView.frame)
     viewMain.backgroundColor = UIColor.black
     viewMain.alpha = 0.50
     self.PrgressView.addSubview(viewMain)
     
     var whiteLblFrame = CGRect(x: 0, y: 0, width: 250, height:  120)
     
     if DeviceType.IS_IPAD{
     whiteLblFrame = CGRect(x: 0, y: 0, width: 350, height:  120)
     }
     let viewWhiteLbl = UIView(frame:whiteLblFrame)
     viewWhiteLbl.center =  self.PrgressView.center
     viewWhiteLbl.center.y += 60.0
     viewWhiteLbl.backgroundColor = UIColor.white
     Globals.layoutViewFor(viewWhiteLbl, color: UIColor.appThemeColor(), width: 2.0, cornerRadius: 8.0)
     
     viewWhiteLbl.layer.masksToBounds = true
     self.PrgressView.addSubview(viewWhiteLbl)
     
     let lineColorView = UIView(frame: CGRect(x: 0, y: 41, width: 250, height:  2))
     lineColorView.backgroundColor = UIColor(red: 24.0/255.0, green: 157.0/255.0, blue: 219.0/255.0, alpha: 1.0)
     
     
     
     var lblProgressFrame = CGRect(x: 0, y: 50, width: 250, height:  70)
     
     if DeviceType.IS_IPAD{
     lblProgressFrame = CGRect(x: 0, y: 50, width: 350, height:  70)
     }
     self.lblProgressText = UILabel(frame: lblProgressFrame)
     self.lblProgressText.backgroundColor = UIColor.clear
     self.lblProgressText.font = Globals.defaultAppFont(16)
     self.lblProgressText.textAlignment = .center
     self.lblProgressText.numberOfLines = 2
     self.lblProgressText.textColor = UIColor.white
     self.lblProgressText.contentMode = .center
     viewWhiteLbl.addSubview(self.lblProgressText)
     
     
     self.activityindicatorView = UIActivityIndicatorView()
     self.activityindicatorView.frame.origin = CGPoint(x:self.lblProgressText.center.x - 20, y:10)
     self.activityindicatorView.frame.size = CGSize(width: 40, height: 40)
     
     self.activityindicatorView.activityIndicatorViewStyle = .whiteLarge
     self.activityindicatorView.startAnimating()
     viewWhiteLbl.addSubview(activityindicatorView)
     
     self.PrgressView.sendSubview(toBack: viewMain)
     self.PrgressView.bringSubview(toFront: viewWhiteLbl)
     self.window!.addSubview(self.PrgressView)
     
     
     }
    
    func showProgressWithText(_ text: String) {
        
        self.lblProgressText.text = text
        self.window!.bringSubview(toFront: self.PrgressView)
        if Thread.current.isCancelled {
            return
        }
        // Cell! Stop what you were doing!
        //[_thread release];
        self.thread = nil
        self.thread = Thread(target: self, selector: #selector(AppDelegate.setVisible(_:)), object: "no")
        self.thread.start()
        
    }
    func hideProgress() {
        if Thread.current.isCancelled {
            return
        }
        // Cell! Stop what you were doing!
        //[_thread release];
        self.lblProgressText.text = ""
        self.thread = nil
        self.thread = Thread(target: self, selector: #selector(AppDelegate.setVisible(_:)), object: "yes")
        self.thread.start()
        
    }
    func setVisible(_ visibility: String) {
        let x = visibility.toBool()
         DispatchQueue.main.async { [weak self]() -> Void in
            self?.PrgressView.isHidden = x!
        }
    }
    
    // MARK:- ========== Push Notification Methods ================
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        NSLog("did Recieve notification ===%@", userInfo)
        
        if application.applicationState == UIApplicationState.inactive || application.applicationState == UIApplicationState.background {
            UIApplication.shared.applicationIconBadgeNumber += 1
        }else {
        }
        var dicNotification : [String:Any]! = userInfo as! [String:Any]
        dicNotification = dicNotification.formatDictionaryForNullValues(dicNotification)
        if self.window?.rootViewController != nil{
            NSLog("rootViewControlle ===")
            self.handlePushNotificaionsWithDictionary(dicNotification)
        }
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func handlePushNotificaionsWithDictionary(_ dicNotification : [String:Any])
    {
        if let acceptJobStaus = dicNotification["availablestatus"]{
            if acceptJobStaus as! String == "Accept"{
                
                let navi = self.window?.rootViewController as? UINavigationController
                if navi != nil && navi!.visibleViewController!.isKind(of: PAActiveJobViewController.self) {
                    let driverObject = DriverDetails()
                    driverObject.setCurrentJobValues(dicNotification)
                }
                if self.delegateJobStatusAction != nil {
                    self.delegateJobStatusAction.jobAccepted!(dicNotification)
                }
               /* if Constants.needForHotel == "1"{
                    if Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.activeJobNotification) != nil
                    {
                    }
                    else{
                        print(dicNotification)
                        if let cancelReservationId = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.CancelReservationId) as? String{
                            if cancelReservationId == String (describing: dicNotification["reservation_id"]!) {
                                return
                            }
                        }
                        Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(dicNotification, key:Constants.activeJobNotification)
                        Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue("Accept", key:Constants.currentJobStaus)
                        let driverObject = DriverDetails()
                        driverObject.setCurrentJobValues(dicNotification)
                        if self.delegateJobStatusAction != nil {
                            self.delegateJobStatusAction.jobAccepted!(dicNotification)
                        }
                    }
                }
                else{
                    let navi = self.window?.rootViewController as? UINavigationController
                    if navi != nil && navi!.visibleViewController!.isKind(of: PAActiveJobViewController.self) {
                        let driverObject = DriverDetails()
                        driverObject.setCurrentJobValues(dicNotification)
                    }
                    if self.delegateJobStatusAction != nil {
                        self.delegateJobStatusAction.jobAccepted!(dicNotification)
                    }
                }*/
            }
        }
        
        
        //  print(dicNotification)
        // ==========if job Rejected=============
        if (dicNotification["status"] != nil){
            //            if (dicNotification["status"] as! String == "Job Rejected") {
            //                let navigation =   UIApplication.topViewController()
            //                if navigation!.isKindOfClass(PAActiveJobViewController){
            //                    if self.delegateJobStatusAction != nil {
            //                        self.delegateJobStatusAction.jobRejected!(dicNotification)
            //                    }
            //                }
            //            }
            if (dicNotification["status"] as! String == "Job Rejected") {
                
               // print(self.requestedDriverIDs, dicNotification["driver_id"] as! String)
                if self.requestedDriverIDs.count == 1{
                    //let navigation =   UIApplication.topViewController()
                    
                    /// if navigation != nil && navigation!.isKindOfClass(PAActiveJobViewController){
                    if self.delegateJobStatusAction != nil {
                        self.delegateJobStatusAction.jobRejected!(dicNotification)
                    }
                    // }
                    self.requestedDriverIDs.remove(dicNotification["driver_id"] as! String)
                    Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(appDelegate.requestedDriverIDs!, key: Constants.allRequestedDrivers)
                }
                else {
                    self.requestedDriverIDs.remove(dicNotification["driver_id"] as! String)
                    Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(appDelegate.requestedDriverIDs!, key: Constants.allRequestedDrivers)
                }
            }
            //  ========== if driver update location ================
            
            if (dicNotification["status"] as! String == "getpassenger") {
                // let navigation =   UIApplication.topViewController()
                // if navigation!.isKindOfClass(PAActiveJobViewController){
                if self.delegateJobStatusAction != nil {
                    self.delegateJobStatusAction.getupdatedAddress!(dicNotification)
                }
                //}
            }
            // ========== if driver change job Status ================
            
            if (dicNotification["status"] as! String == "job_status") {
                NSLog("----------I'm here------------job_status")
            
                Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(dicNotification["message"] as! String, key:Constants.currentJobStaus)
                
                if let messge = dicNotification["message"]{
                    
                    var Conf_Id = ""
                    
                    if let conf_id = dicNotification["conf_id"]{
                        Conf_Id = "Job with booking ID: " + "\(conf_id) is ".replacingOccurrences(of: "-", with: "") + " "
                    }
                        Globals.sharedInstance.ToastAlertForInfo("\(Conf_Id)\(messge)")
                    
                        if self.delegateJobStatusAction != nil {
                            self.delegateJobStatusAction.jobStatusChanged!("\(messge)", dicNotification: dicNotification)
                        }
                    }
 
            }

            // Job done and witing for receipt
            if let status = dicNotification["status"]
            {
                if "\(status)" == "fare"{
                if self.delegateJobStatusAction != nil
                {
                    self.delegateJobStatusAction.dropOffDoneByDriver!(dicNotification)
                }
                }
            }
 
            //  ========== if Preference changes in portal ================
            if (dicNotification["status"] as! String == "units updated") {
                
                appDelegate.distanceType = dicNotification["unit"] as! String
                appDelegate.distanceUnit = (dicNotification["unit"] as! String).uppercased()
                appDelegate.distanceMultiplier = appDelegate.distanceType == "mile" ? 0.000621371 : 0.001
                appDelegate.currency = dicNotification["currency"] as! String
                
                Globals.sharedInstance.updateValuetoUserDefaultsForKey("prohibitReservation_before", value: "\(dicNotification["prohibitReservation_before"]!)", forDefaultKey: Constants.ActiveUserInfo)
                
                Globals.sharedInstance.updateValuetoUserDefaultsForKey("time_format", value: dicNotification["timeformat"] as! String, forDefaultKey: Constants.ActiveUserInfo)
                Globals.sharedInstance.updateValuetoUserDefaultsForKey("showCompanyNameToPassanger", value: dicNotification["showCompanyNameToPassanger"] as! String, forDefaultKey: Constants.ActiveUserInfo)
                
                Globals.sharedInstance.updateValuetoUserDefaultsForKey("date_format", value: dicNotification["dateformat"] as! String, forDefaultKey: Constants.ActiveUserInfo)
                
            }
        }
            //  ========== if Reservation changes notification ================
            
        else  if (dicNotification["record"] != nil) {
            if (dicNotification["record"]! as! [String:Any])["status"] as! String == "driver_assign"{
                
                let alert = UIAlertView(title: "Driver Assigned", message: ( (dicNotification["record"]! as! [String:Any])["message"]!) as? String, delegate: nil, cancelButtonTitle: "Dismiss")
                alert.show()
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.kUpdateJobListNotification), object: nil)
                /*
                 var dic = userInfo.formatDictionaryForNullValues(userInfo as! [String:Any])
                 if let _ = dic!["record"] as?  [String:Any] {
                 let driverAssignVC =
                 self.storybaord.instantiateViewControllerWithIdentifier("PAUpcomingReservationDetailsVC") as! PAUpcomingReservationDetailsVC
                 driverAssignVC.dicDataSource = dic!["record"] as! [String:Any]
                 driverAssignVC.isPresented = true
                 driverAssignVC.strJobTitle = "Driver Assigned"
                 let navigation = UINavigationController(rootViewController: driverAssignVC)
                 let navi = self.window?.rootViewController as? UINavigationController
                 navi?.presentViewController(navigation, animated: true, completion:nil )
                 }*/
            }
            else if (dicNotification["record"]! as! [String:Any])["status"] as! String == "driver_cancel"{
                
                // let navigation =   UIApplication.topViewController()
                // if (navigation != nil && navigation!.isKindOfClass(PAActiveJobViewController)) ||  (navigation != nil && navigation!.isKindOfClass(PAMainMenuViewController)) {
                if self.delegateJobStatusAction != nil {
                    self.delegateJobStatusAction.jobCancelled!(dicNotification)
                }
                    // }
                else {
                    Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.activeJobNotification)
                    Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.currentJobStaus)
                }
                
                let conf_id = "\((dicNotification["record"]! as! [String:Any])["conf_id"] as! String)".replacingOccurrences(of: "-", with: "")
                let alert = UIAlertView(title: "Booking No: \(conf_id)", message: "Driver has cancelled your reservation.", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
                
            }
        }
        
    }
    
    func saveDriverDetailsWhenJobAccepted(_ dicJobDetails:[String:Any])
    {
    }
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        NSLog("performFetchWithCompletionHandler")

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64( UInt64(20) * NSEC_PER_SEC )) / Double(NSEC_PER_SEC)) { () -> Void in
            completionHandler(UIBackgroundFetchResult.newData)
            
        }
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var deviceTokenString = ""
        for i in 0..<deviceToken.count {
            deviceTokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        if deviceTokenString.characters.count != 0 {
            Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(deviceTokenString, key: Constants.DeviceToken)
        }
          NSLog("deviceTokenString ===%@", deviceTokenString)
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error){
        print("error notification register --\(error)")
    }
    
    
    // MARK: - =====Core Data stack========
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "netquall.PassangerApp" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "PassangerApp", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: Any]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as Any?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as Any?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
}

