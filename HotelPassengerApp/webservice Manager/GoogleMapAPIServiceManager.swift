//
//  GoogleMapAPIServiceManager.swift
//  PassangerApp
//
//  Created by Netquall on 12/3/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit


class GoogleMapAPIServiceManager: NSObject {
  
    typealias findLocation = (_ location : CLLocation) -> ()
    
    typealias SuccessBlock = (_ dicRecievedJSON : [String : Any]) -> ()
    typealias FailureBlock = (_ error:Error?) -> ()
    static let sharedInstance: GoogleMapAPIServiceManager = {
        let instance = GoogleMapAPIServiceManager()
        
        // setup code
        
        return instance
    }()
  
   
func getLocationAddressUsingLatitude(_ lattitude:Double, longitude:Double, withComplitionHandler:@escaping SuccessBlock, failureBlock:@escaping FailureBlock){
        if lattitude == 0.0 && longitude == 0.0 {
            return
        }
        let url = URL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&sensor=false")
        // let URL = NSURL(string:url)!
        let mutableURLRequest = NSMutableURLRequest(url: url!)
        mutableURLRequest.httpMethod = "GET"
        mutableURLRequest.timeoutInterval = 30
        // let parameters = ["foo": "bar"]
        
        mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //Alamofire.request(mutableURLRequest).responseJSON { (Response) -> Void in
     Alamofire.request(url!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
    if response.result.isSuccess{
                if let  jsonDataRecieved = response.data{
                    let jsonDicRecieved = JSON(data:jsonDataRecieved )
                    // successBlock(dicData: jsonDicRecieved.dictionary)
                    withComplitionHandler(jsonDicRecieved.dictionaryObject!)
                }
            }else{
                failureBlock(response.result.error)
            }
        }
    }
    
func retrieveJSONDetailsAboutWithPlaceID(_ place: String, withComplitionHandler:@escaping findLocation, failureBlock:@escaping FailureBlock){
        
        let urlString: String = "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(place)&key=\(Constants.placeApiKey)"
        let url: URL = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!
    
        let mutableURLRequest = NSMutableURLRequest(url: url)
        mutableURLRequest.httpMethod = "GET"
        mutableURLRequest.timeoutInterval = 30
        
        mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
       // Alamofire.request(mutableURLRequest).responseJSON { (Response) -> Void in
    
   Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (Response) in
     if Response.result.isSuccess{
                if let  jsonDataRecieved = Response.data{
                    let jsonDicRecieved = JSON(data:jsonDataRecieved )
                    // successBlock(dicData: jsonDicRecieved.dictionary)
                    
                    if let dicRecievedJSON = jsonDicRecieved.dictionaryObject{
                        if dicRecievedJSON["status"] as! String == "OK"{
                            if let arrPlaces = ((dicRecievedJSON["result"]! as! [String:Any])["geometry"]! as! [String:Any] )["location"] as? [String:Any]{
                                
                                var latitude:Double! = 0.0
                                var longitude:Double! = 0.0
                                
                                latitude = arrPlaces["lat"] as! Double
                                longitude = arrPlaces["lng"] as! Double
                                
                                withComplitionHandler(CLLocation(latitude:latitude
                                    , longitude:longitude))
                            }
                        }
                    }
                }
                
            }
            else{
                failureBlock(Response.result.error)
            }
        }
    }
    
    func getAddressLatLongiFromAddressString(_ strAddress: String, isPickup:Bool) -> CLLocation {
        
        var latitude:Double = 0
        var longitude:Double = 0
        
        let esc_addr: String = strAddress.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let req = Globals.sharedInstance.getFullApiAddressForAddressString(esc_addr)
        var result : String!
        do {
            result = try String(contentsOf: URL(string: req)!, encoding: String.Encoding.utf8)
        }catch {
            print("json Error-::\(error)")
            
        }
        
        if result  != "" && result  != nil{
            let scanner: Scanner = Scanner(string: result)
            
            if scanner.scanUpTo("\"lat\" :", into: nil) && scanner.scanString("\"lat\" :", into: nil) {
                scanner.scanDouble(&latitude)
                if scanner.scanUpTo("\"lng\" :", into: nil) && scanner.scanString("\"lng\" :", into: nil) {
                    scanner.scanDouble(&longitude)
                }
            }
        }
        
        let location = CLLocation(latitude: latitude, longitude: longitude)
        return location
    }
    
}// class brackets









