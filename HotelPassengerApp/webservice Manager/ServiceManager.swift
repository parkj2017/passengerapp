//
//  ServiceManager.swift
//  PassangerApp
//
//  Created by Netquall on 11/30/15.
//  Copyright © 2015 Netquall. All rights reserved.
//




import UIKit
import Alamofire
import SwiftyJSON
class ServiceManager: NSObject {
    
    typealias SuccessBlock = (_ dicData : [String : Any]) -> ()
    typealias FailureBlock = (_ error:NSError?) -> ()
    
    static let sharedInstance: ServiceManager = {
        let instance = ServiceManager()
        
        // setup code
        
        return instance
    }()

    
    func driverStatusTrackRequestPost(_ urlPath:String!,dicParameters: [String : Any], successBlock:@escaping (_ dicData : [String : Any]) -> Void,failureBlock: @escaping (_ error:NSError?) -> ())
    {
        
        let jsonString : String!
        var dicNewParameters = dicParameters
        dicNewParameters["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?

        if Globals.sharedInstance.getValueFromUserDefaultsForKey("loginWeb") as! Bool == false  {
            // let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id") as! String
            // let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session") as! String
            //        dicNewParameters["session"] = session
            //        dicNewParameters["user_id"] = user_ID
        }
        
        //var error : NSError!
        var jsonData : Data! = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dicNewParameters, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            print(" print data dic of webservice hit--- \(jsonString)")
        }catch{
            print("Error--- \(error)")
            return
        }
        let url = Constants.baseUrl + urlPath
        print(url)
        
          Alamofire.request(url, method: .post, parameters: dicParameters, encoding:JSONEncoding.default, headers: nil).validate().responseData { (Response) in

   //     Alamofire.request(.POST, url, parameters: dicParameters, encoding: .JSON, headers: .None).responseJSON             { (Response) -> Void in
                
                if Response.result.isSuccess{
                    if let  jsonDataRecieved = Response.data{
                        let jsonDicRecieved = JSON(data:jsonDataRecieved )
                        if let dic = jsonDicRecieved.dictionaryObject {
                            if dic["status"] as! String == "session_expired"{
                                
                                appDelegate.hideProgress()
                                Globals.ToastAlertWithString(dic["message"] as! String)
                                self.loginVC()
                            }else {
                                successBlock( jsonDicRecieved.dictionaryObject!)
                            }
                        }
                    }
                }else{
                    failureBlock( Response.result.error as NSError?)
                }
                
                
        }
        
    }
    
func dataTaskWithPostRequest(_ urlPath:String!,dicParameters: [String : Any], successBlock:@escaping (_ dicData : [String : Any]) -> Void,failureBlock: @escaping (_ error:NSError?) -> ())
    {
        
        var dicParameters = dicParameters;
        dicParameters["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?

        let jsonString : String!
        var jsonData : Data! = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dicParameters, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            print(" print data dic of webservice hit--- \(jsonString)")
        }catch{
            print("Error--- \(error)")
            return
        }
        let url = Constants.baseUrl + urlPath
        NSLog("%@", url)
        NSLog("%@", jsonString)
        let URL = Foundation.URL(string:url)!
        let mutableURLRequest = NSMutableURLRequest(url: URL)
        mutableURLRequest.httpMethod = "POST"
        NSLog("\n%@", jsonString)

        // let parameters = ["foo": "bar"]
        
        do {
            mutableURLRequest.httpBody = try JSONSerialization.data(withJSONObject: dicParameters, options: JSONSerialization.WritingOptions())
            mutableURLRequest.timeoutInterval = 45
            
        } catch {
            // No-op
        }
        
        mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        Alamofire.request(URL, method: .post, parameters: dicParameters, encoding:JSONEncoding.default, headers: nil).responseData { (Response) in
            
            NSLog("\n%@", jsonString)

       // Alamofire.request(mutableURLRequest).responseJSON { (Response) -> Void in
                if Response.result.isSuccess{
                    if let  jsonDataRecieved = Response.data{
                        let jsonDicRecieved = JSON(data:jsonDataRecieved )
                        if let dic = jsonDicRecieved.dictionaryObject {
                            if dic["status"] as! String == "session_expired"{
                                appDelegate.hideProgress()

                                Globals.ToastAlertWithString(dic["message"] as! String)
                                self.loginVC()
                            }else {
                                successBlock(jsonDicRecieved.dictionaryObject!)
                            }
                        }
                        else {
                             failureBlock( Response.result.error as NSError?)
                        }
                    }
                    else {
                        failureBlock( Response.result.error as NSError?)
                    }
                }else{
                    failureBlock( Response.result.error as NSError?)
                }
        }
        
}
func dataTaskWithGetRequest(_ urlPath:String!, successBlock:@escaping (_ dicData : [String : Any]) -> Void,failureBlock: @escaping (_ error:NSError?) -> ())
{
        // var urlrequest = NSMutableURLRequest(URL: NSURL(string: urlPath)!)
        //urlrequest.
    //    Alamofire.request(.GET, urlPath).responseJSON { (Response) -> Void in Response
    
    
     Alamofire.request(urlPath, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (Response) in
            if Response.result.isSuccess{
                if let  jsonDataRecieved = Response.data{
                    let jsonDicRecieved = JSON(data:jsonDataRecieved )
                    if let dic = jsonDicRecieved.dictionaryObject {
                        if dic["status"] as! String == "session_expired"{
                            appDelegate.hideProgress()
                            Globals.ToastAlertWithString(dic["message"] as! String)
                            self.loginVC()
                        }else {
                            successBlock(jsonDicRecieved.dictionaryObject! )
                        }
                    }
                }
            }else{
                failureBlock(Response.result.error as NSError?)
            }
        }
}
func loginVC()
{
        let navigation = appDelegate.window?.rootViewController as? UINavigationController
        if let _ = navigation {
            navigation!.popToRootViewController(animated: true)
        }
        appDelegate.hideProgress()
        Globals.logout()
}
}// claas brackets
