//
//  SideMenuViewController.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 14/06/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit



class SideMenuViewController: UIViewController {

    @IBOutlet var lblUserName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden =  true

        let userDetails = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        
        let userName = (userDetails["first_name"] as! String).capitalized + " " +  (userDetails["last_name"] as! String).capitalized
        
        let welcomeText = NSMutableAttributedString(string: "Welcome\n ", attributes: [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName: Globals.defaultAppFontWithBold(18)])
        let strStatus = NSAttributedString(string: userName, attributes: [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName: Globals.defaultAppFont(15)])
        welcomeText.append(strStatus)
        lblUserName.attributedText = welcomeText
            // Do any additional setup after loading the view.
    }
    @IBAction func btnLogOutAction(_ sender: UIButton) {
        
        if let _ = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.activeJobNotification)
        {
            let alert = UIAlertController(title: "Please wait", message: "There is one ongoing job.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            
            let alert = UIAlertController(title: "Confirmation", message: "Do you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
                self.logoutUser()
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
 
    }

    func logoutUser() {
        if (appDelegate.networkAvialable == true) {
            appDelegate.showProgressWithText("Please wait...")
            var dicParameters : [String:Any]! = Dictionary()
            
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            
            dicParameters["user_id"] = user_ID
            dicParameters["session"] = session
            
            weak var weakSelf = self
            ServiceManager.sharedInstance.dataTaskWithPostRequest("logout", dicParameters: dicParameters, successBlock:
                { (dicRecievedJSON) -> Void in
                    if weakSelf == nil {
                        appDelegate.hideProgress()
                        return
                    }
                    appDelegate.hideProgress()

                    if dicRecievedJSON != nil
                    {
                        if let status = dicRecievedJSON["status"] as? String{
                            if status.compare("success") == .orderedSame
                            {
                            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.ActiveUserInfo)
                            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.ActiveCardInfo)
                            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.TotalCardsForPayment)
                            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.activeJobNotification)
                            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.currentJobStaus)
                            
                            Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(false , key: Constants.KeepMeLogin)
                            DispatchQueue.main.async(execute: {
                                let navigation = appDelegate.window?.rootViewController as? UINavigationController
                                navigation!.popToRootViewController(animated: false)
                            })
                                }
                        }else {
                          //  JLToast.makeText(dicRecievedJSON["message"] as! String, duration: 5.0).show()
                            appDelegate.hideProgress()
                        }
                    }
            }) { (error) -> () in
                (UIApplication.shared.delegate! as! AppDelegate).hideProgress()
                Globals.ToastAlertWithString("Please try again")
                print(error)
            }
            
        }else {
         //   JLToast.makeText("Please check your network connection", duration: JLToastDelay.ShortDelay).show()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnShareClicked(_ sender: Any) {
        let textToShare = "Swift is awesome!  Check out this website about it!"
        
        if let myWebsite = URL(string: "http://www.google.com/") {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            //New Excluded Activities Code
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            //
            activityVC.popoverPresentationController?.sourceView = sender as! UIView
            self.present(activityVC, animated: true, completion: nil)
        }

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toPaymentVC" { // Rashpinder
            let paymentVc = segue.destination as! PASelectPaymentVC
            paymentVc.profilePayment = true
            paymentVc.pushToRequestView = { (dicCardDetails:[String:Any]?, cardID:String?) -> () in
           
            }
            
        }
    }
    

}
