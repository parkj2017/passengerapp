//
//  PAMainMenuViewController.swift
//  PassangerApp
//
//  Created by Netquall on 1/27/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit

class PAMainMenuViewController: BaseViewController , JobActionProtocol{

    @IBOutlet var btnRideNow: UIButton!
    
    var isBookerApp:Bool = false
    // MARK:- =====View Life Cycle ========
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.backItem?.hidesBackButton = true
        self.navigationController!.interactivePopGestureRecognizer!.isEnabled = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        appDelegate.delegateJobStatusAction = nil
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "HOME"
        self.navigationController?.navigationBar.isHidden =  false
        self.navigationItem.hidesBackButton = true
        appDelegate.delegateJobStatusAction = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     // MARK:- =====Button Methods  ========
    @IBAction func btnRideLaterClicked(_ sender: UIButton) {
        let detailsVc = UIStoryboard(name: "RideLater", bundle: nil).instantiateViewController(withIdentifier: "PAReservationsRideLaterVC") as! PAReservationsRideLaterVC
        detailsVc.isEditingMode = false
        self.navigationController?.pushViewController(detailsVc, animated: true)
    }
    @IBAction func btnPaymentClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toPaymentVC", sender: nil)
    }
    @IBAction func btnRideNowClicked(_ sender: UIButton) {
        
        if(appDelegate.networkAvialable == true){
            
            self.performSegue(withIdentifier: "goToOndemandView", sender: self)
        }
        else {
       //     JLToast.makeText("Please check your network connection", duration: JLToastDelay.ShortDelay).show()
        }
        
    }
    @IBAction func btnLogOutAction(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Confirmation", message: "Do you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.ActiveUserInfo)
            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.ActiveCardInfo)
            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.TotalCardsForPayment)
            
            let navigation = appDelegate.window?.rootViewController as? UINavigationController
            if let _ = navigation {
                navigation!.popToRootViewController(animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
            
        }))
        self.present(alert, animated: true, completion: nil)
        //}
        
    }
    // MARK:- =====Job Delegate methods ========
    func jobRejected(_ dicNotification: [String : AnyObject]) {
    }
    func JobReceiptRecieved(_ dicNotification: [String : AnyObject]) {
    }
    func jobStatusChanged(_ strStatus: String, dicNotification: [String : AnyObject]) {
        if strStatus.compare("Unassigned"){
            
                let alert = UIAlertView(title: "Booking Info", message:"Driver has been unassigned from this job. You can re-request a car from BOOK FOR NOW section.", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
        }
        if strStatus.compare("bail out"){
                let alert = UIAlertView(title: "Driver Requested Bail Out", message:"Request has already been sent to all nearby chauffeurs. Please hang on, you will be notified once the job is accepted.", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
        }
    }
    func dropOffDoneByDriver(_ dicNotification: [String : AnyObject]) {
    }
    func jobCancelled(){
        DispatchQueue.main.async { () -> Void in
            self.btnRideNow .setImage(UIImage(named: "ridenow"), for: UIControlState())
            self.btnRideNow.backgroundColor = UIColor(colorLiteralRed: 32.0/255.0, green: 177.0/255.0, blue: 3.0/255.0, alpha: 1);
        }
        
    }
    func getupdatedAddress(_ dicNotification: [String : AnyObject]) {
    }
  
    
    
    func jobAccepted(_ dicNotification: [String : AnyObject]) {

        var driverName = ""
        var conf_id = ""
        
        if let dicCustom = dicNotification["custom"]{
            if let b_id = dicCustom["booking_id"] as? String{
                conf_id = b_id
            }
        if let f_name = dicNotification["first_name"], let l_name = dicNotification["last_name"]{
            driverName = "\(f_name) \(l_name)"
        }
        
        let alertView = UIAlertView(title:"Job Accepted" , message: "Job with booking No #\(conf_id) accepted by \(driverName). \n Now you can track this job in \"ON-GOING RESERVATION\" section.", delegate: nil, cancelButtonTitle: "Okay")
        alertView.show()
        }

    }
    
    /*
     
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    // MARK:- =====Some other Methods  ========
    
    func getActiveCardsFromServer(){
        if appDelegate.networkAvialable == true {
            var dicParameters = [String:Any]()
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            dicParameters["session"] = session
            dicParameters["user_id"] = user_ID
            ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.getUserCreditCards, dicParameters: dicParameters, successBlock: { (dicData) -> Void in
                appDelegate.hideProgress()
                if dicData["status"] as! String == "success" {
                    Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(dicData, key: Constants.ActiveCardInfo)
                }
                }, failureBlock: { (error) -> () in
                    appDelegate.hideProgress()
                    let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
                    alert.show()
            })
        }
    }
    
    // MARK:- ====navigation ========
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToActiveJobVC"{
              return
        }else if segue.identifier == "toPaymentVC" { // Rashpinder
            let paymentVc = segue.destination as! PASelectPaymentVC
            paymentVc.profilePayment = true
            paymentVc.pushToRequestView = { (dicCardDetails:[String:Any]?, cardID:String?) -> () in
                
            }
        }
    }
}
