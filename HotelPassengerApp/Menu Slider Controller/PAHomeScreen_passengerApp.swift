//
//  PAHomeScreen_passengerApp.swift
//  PassangerApp
//
//  Created by Netquall on 1/27/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
class CollectionItem {
    
    var itemImage:String
    var itemTitle:String
    
    init(title:String, image:String) {
        itemTitle = title
        itemImage = image
    }
}


class CenterCellCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        if let cv = self.collectionView {
            
            let cvBounds = cv.bounds
            let halfWidth = cvBounds.size.width * 0.5;
            let proposedContentOffsetCenterX = proposedContentOffset.x + halfWidth;
            
            if let attributesForVisibleCells = self.layoutAttributesForElements(in: cvBounds) {
                
                var candidateAttributes : UICollectionViewLayoutAttributes?
                for attributes in attributesForVisibleCells {
                    
                    // == Skip comparison with non-cell items (headers and footers) == //
                    if attributes.representedElementCategory != UICollectionElementCategory.cell {
                        continue
                    }
                    
                    if let candAttrs = candidateAttributes {
                        
                        let a = attributes.center.x - proposedContentOffsetCenterX
                        let b = candAttrs.center.x - proposedContentOffsetCenterX
                        
                        if fabsf(Float(a)) < fabsf(Float(b)) {
                            candidateAttributes = attributes;
                        }
                        
                    }
                    else { // == First time in the loop == //
                        
                        candidateAttributes = attributes;
                        continue;
                    }
                    
                    
                }
                
                return CGPoint(x: round(candidateAttributes!.center.x - halfWidth), y: proposedContentOffset.y)
                
            }
            
        }
        
        // Fallback
        return super.targetContentOffset(forProposedContentOffset: proposedContentOffset)
    }
    
}

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    func setGalleryItem(_ item:CollectionItem)  {
        self.imageView.image = UIImage(named: item.itemImage)
        self.lblTitle.text = item.itemTitle
    }
}

class PAHomeScreen_passengerApp: BaseViewController , JobActionProtocol, UICollectionViewDataSource, UICollectionViewDelegate{
    @IBOutlet weak var menuButton: UIButton!
    var dataSource: [CollectionItem] = []
    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet var welcomeView: UIView!
    @IBOutlet var collection_MenuView: UICollectionView!
    @IBOutlet var topLogoImage: UIImageView!
    
    // MARK:- =====View Life Cycle ========
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let userDetails = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        
        let userName = (userDetails["first_name"] as! String).capitalized + " " +  (userDetails["last_name"] as! String).capitalized
        if let lblName = self.welcomeView.viewWithTag(1000) as? UILabel {
            lblName.text = userName
        }

        self.collection_MenuView.dataSource = self
        self.collection_MenuView.delegate = self
        self.navigationController?.navigationBar.backItem?.hidesBackButton = true
        self.navigationController!.interactivePopGestureRecognizer!.isEnabled = false
        
        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)//addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), forents.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        
                if let _ = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.activeJobNotification)
                {
                    let mainMenuVC = self.storyboard?.instantiateViewController(withIdentifier: "PAActiveJobViewController") as! PAActiveJobViewController
                    self.navigationController?.pushViewController(mainMenuVC, animated: false)
                     return
                }
        
    
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        appDelegate.delegateJobStatusAction = nil
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "User Menu"
        initGalleryItems()
        collection_MenuView.reloadData()
        self.navigationController?.navigationBar.isHidden =  true
        self.navigationItem.hidesBackButton = true
        appDelegate.delegateJobStatusAction = self
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if DeviceType.IS_IPAD{
            self.collection_MenuView.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT - 355 * 1.5,  width: ScreenSize.SCREEN_WIDTH, height: 300*1.5)
            self.topLogoImage.frame = CGRect(x: 0, y: 75*1.5,   width: ScreenSize.SCREEN_WIDTH, height: 134*1.5)
            self.welcomeView.frame = CGRect(x: 0, y: 240*1.5,   width: ScreenSize.SCREEN_WIDTH, height: 46*1.5)
            
            self.view.updateConstraintsIfNeeded()
            
        }else{
            if DeviceType.IS_IPHONE_5  {
                self.collection_MenuView.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT - 300,  width: ScreenSize.SCREEN_WIDTH, height: 260)
                self.topLogoImage.frame = CGRect(x: 0, y: 44,   width: ScreenSize.SCREEN_WIDTH, height: 125)
                self.welcomeView.frame = CGRect(x: 0, y: 190,   width: ScreenSize.SCREEN_WIDTH, height: 46)
                self.view.updateConstraintsIfNeeded()
                
            }
            else if DeviceType.IS_IPHONE_4_OR_LESS{
                self.collection_MenuView.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT - 290,  width: ScreenSize.SCREEN_WIDTH, height: 260)
                self.topLogoImage.frame = CGRect(x: 0, y: 25,   width: ScreenSize.SCREEN_WIDTH, height: 100)
                self.welcomeView.frame = CGRect(x: 0, y: 135,   width: ScreenSize.SCREEN_WIDTH, height: 46)
                
                self.view.updateConstraintsIfNeeded()
            }
            else {
                self.collection_MenuView.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT - 355,  width: ScreenSize.SCREEN_WIDTH, height: 300)
                self.topLogoImage.frame = CGRect(x: 0, y: 75,   width: ScreenSize.SCREEN_WIDTH, height: 134)
                self.welcomeView.frame = CGRect(x: 0, y: 240,   width: ScreenSize.SCREEN_WIDTH, height: 46)
                
                self.view.updateConstraintsIfNeeded()
                
            }
        }
        
        var insets = self.collection_MenuView.contentInset
        let value = (self.view.frame.size.width - (self.collection_MenuView.collectionViewLayout as! UICollectionViewFlowLayout).itemSize.width) * 0.5
        insets.left = value
        insets.right = value
        self.collection_MenuView.contentInset = insets
        self.collection_MenuView.decelerationRate = UIScrollViewDecelerationRateFast;
    }

    fileprivate func initGalleryItems() {
        
        var items = [CollectionItem]()
        var item_1:CollectionItem!
                if let _ = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.activeJobNotification) {
                    item_1 = CollectionItem(title: "", image: "goingJob")
                }else {
        item_1 = CollectionItem(title: "", image: "car")
        }
        let item_2:CollectionItem = CollectionItem(title: "", image: "book")
        let item_3:CollectionItem = CollectionItem(title: "", image: "goingJob")
        let item_4:CollectionItem = CollectionItem(title: "", image: "upcoming")
        let item_5:CollectionItem = CollectionItem(title: "", image: "past")
        
        items.append(item_1)
        items.append(item_2)
        items.append(item_3)
        items.append(item_4)
        items.append(item_5)
        self.dataSource = items
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellIdentifier", for: indexPath) as! CollectionViewCell
       cell.setGalleryItem(dataSource[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if indexPath.row == 0{
            
            if(appDelegate.networkAvialable == true){
                                if let dicNotification = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.activeJobNotification)
                                {
                                    let mainMenuVC = self.storyboard?.instantiateViewController(withIdentifier: "PAActiveJobViewController") as! PAActiveJobViewController
                                    self.navigationController?.pushViewController(mainMenuVC, animated: true)
                                }
                                else {
                  self.performSegue(withIdentifier: "goToOndemandView", sender: self)
//                let mainMenuVC = self.storyboard?.instantiateViewController(withIdentifier: "OnDemandViewController") as! OnDemandViewController
//                self.navigationController?.pushViewController(mainMenuVC, animated: true)
                 }
            }
            else {
                //JLToast.makeText("Please check your network connection", duration: JLToastDelay.ShortDelay).show()
            }
        }
        else if indexPath.row == 1{
            self.performSegue(withIdentifier: "goToRideLaterView", sender: self)
        }
        else if indexPath.row == 2{
            self.performSegue(withIdentifier: "showOngoingReservations", sender: self)
        }
        else if indexPath.row == 3{
            self.performSegue(withIdentifier: "showUpcomingReservations", sender: self)
        } else if indexPath.row == 4{
            let pastVC  = self.storyboard?.instantiateViewController(withIdentifier: "PastReservationsViewController") as! PastReservationsViewController
            self.navigationController?.show(pastVC, sender: nil)
         //   self.performSegue(withIdentifier: "showPastReservations", sender: self)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        if DeviceType.IS_IPAD{
            return CGSize(width: 180*1.5 , height: 300*1.5)

        }
        else {
        if DeviceType.IS_IPHONE_5  {
            return CGSize(width: 160 , height: 260)
        }
        else if DeviceType.IS_IPHONE_4_OR_LESS{
            return CGSize(width: 160 , height: 250)
        }
        else {
            return CGSize(width: 180 , height: 300)
        }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        if DeviceType.IS_IPAD{
            pageControll.currentPage = Int((scrollView.contentOffset.x / 180*1.5) + 1)
        }
        else {
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS{
            pageControll.currentPage = Int((scrollView.contentOffset.x / 160) + 1)
        }
        else {
            pageControll.currentPage = Int((scrollView.contentOffset.x / 180) + 1)
        }
        }
        
    }
    
    // MARK:- =====Button Methods  ========
    
    @IBAction func btnPaymentClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toPaymentVC", sender: nil)
    }
    @IBAction func btnRideNowClicked(_ sender: UIButton) {
        
        if(appDelegate.networkAvialable == true){
            
            self.performSegue(withIdentifier: "goToOndemandView", sender: self)

            if let dicNotification = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.activeJobNotification)
            {
                let mainMenuVC = self.storyboard?.instantiateViewController(withIdentifier: "PAActiveJobViewController") as! PAActiveJobViewController
                //let nvc: UINavigationController = UINavigationController(rootViewController: mainMenuVC)
                self.navigationController?.pushViewController(mainMenuVC, animated: true)
            }
            else {
                self.performSegue(withIdentifier: "goToOndemandView", sender: self)
            }
        }
        else {
      //      JLToast.makeText("Please check your network connection", duration: JLToastDelay.ShortDelay).show()
        }
        
    }
    @IBAction func btnLogOutAction(_ sender: UIButton) {
        
        //        if let _ = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.activeJobNotification)
        //        {
        //            let alert = UIAlertController(title: "Please wait", message: "There is one ongoing job.", preferredStyle: UIAlertControllerStyle.Alert)
        //            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler:{ (ACTION :UIAlertAction!)in
        //
        //            }))
        //            self.presentViewController(alert, animated: true, completion: nil)
        //        }
        //        else{
        
        let alert = UIAlertController(title: "Confirmation", message: "Do you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.ActiveUserInfo)
            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.ActiveCardInfo)
            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.TotalCardsForPayment)
            //Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.activeJobNotification)
           // Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.currentJobStaus)
            
            let navigation = appDelegate.window?.rootViewController as? UINavigationController
            if let _ = navigation {
                navigation!.popToRootViewController(animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
            
        }))
        self.present(alert, animated: true, completion: nil)
        //}
        
    }
    // MARK:- =====Job Delegate methods ========
    func jobRejected(_ dicNotification: [String : AnyObject]) {
    }
    func JobReceiptRecieved(_ dicNotification: [String : AnyObject]) {
    }
    func jobStatusChanged(_ strStatus: String, dicNotification: [String : AnyObject]) {
        if strStatus.compare("Unassigned"){

                let alert = UIAlertView(title: "Booking Info ", message:"Driver has been unassigned from this job. You can re-request a car from BOOK FOR NOW section.", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
        }
        if strStatus.compare("bail out"){

                let alert = UIAlertView(title: "Driver Requested Bail Out", message:"Request has already been sent to all nearby chauffeurs. Please hang on, you will be notified once the job is accepted.", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
                
        }
    }
    func dropOffDoneByDriver(_ dicNotification: [String : AnyObject]) {
    }
    func jobCancelled(){
        DispatchQueue.main.async { () -> Void in
          //  self.btnRideNow .setImage(UIImage(named: "ridenow"), forState: .Normal)
          //  self.btnRideNow.backgroundColor = UIColor(colorLiteralRed: 32.0/255.0, green: 177.0/255.0, blue: 3.0/255.0, alpha: 1);
        }
      //  Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.activeJobNotification)
      //  Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.currentJobStaus)
        
    }
    func getupdatedAddress(_ dicNotification: [String : AnyObject]) {
    }
    
    
    func jobAccepted(_ dicNotification: [String : AnyObject]) {

        var driverName = ""
        var conf_id = ""
        
        if let dicCustom = dicNotification["custom"]{
            if let b_id = dicCustom["booking_id"] as? String{
                conf_id = b_id
            }
        if let f_name = dicNotification["first_name"], let l_name = dicNotification["last_name"]{
            driverName = "\(f_name) \(l_name)"
        }
        
        let alertView = UIAlertView(title:"Job Accepted" , message: "Job with booking No #\(conf_id) accepted by \(driverName). \n Now you can track this job in \"ON-GOING RESERVATION\" section.", delegate: nil, cancelButtonTitle: "Okay")
        alertView.show()
        }

    }
    
    /*
     
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    // MARK:- =====Some other Methods  ========
    
    func getActiveCardsFromServer(){
        if appDelegate.networkAvialable == true {
            var dicParameters = [String:Any]()
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            dicParameters["session"] = session
            dicParameters["user_id"] = user_ID
            ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.getUserCreditCards, dicParameters: dicParameters, successBlock: { (dicData) -> Void in
                appDelegate.hideProgress()
                if dicData["status"] as! String == "success" {
                    Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(dicData, key: Constants.ActiveCardInfo)
                }
                }, failureBlock: { (error) -> () in
                    appDelegate.hideProgress()
                    let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
                    alert.show()
            })
        }
    }
    
    // MARK:- ====navigation ========
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToActiveJobVC"{
             return
        }else
        if segue.identifier == "toPaymentVC" { // Rashpinder
            let paymentVc = segue.destination as! PASelectPaymentVC
            paymentVc.profilePayment = true
            paymentVc.pushToRequestView = { (dicCardDetails:[String:Any]?, cardID:String?) -> () in
                
            }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
       }
    }
}
