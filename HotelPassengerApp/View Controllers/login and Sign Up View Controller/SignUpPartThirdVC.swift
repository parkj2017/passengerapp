//
//  SignUpPartThirdVC.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 19/01/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import SwiftyJSON
class SignUpPartThirdVC: BaseStatusBarViewController, CardIOPaymentViewControllerDelegate, UIActionSheetDelegate , UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate{
    // MARK:- ======property declaration =======
    @IBOutlet weak var btnUserImage: UIButton!
    @IBOutlet weak var btnSelectPayment: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var accessaryViewPhone: UIView!
    @IBOutlet weak var contentViewScroll: UIView!
    @IBOutlet weak var imageMainLogo: UIImageView!
    lazy var dicCardDetails = [String:Any]()
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtCompanyName: UITextField!
    @IBOutlet weak var btnCompanyVerify: UIButton!

    var picker = UIImagePickerController()
    var activeTextField : UITextField?
    var activeButton : UIButton!
    var userId = ""
    var strImageBase64 = ""
    var cardType:String = ""
    var dicParameter = [String:Any]()
    var acc_comp_id = ""
    var alias_id = ""
    // MARK:-  ========view life cycle====
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        self.imageMainLogo.layer.masksToBounds = true
        self.imageMainLogo.contentMode = .scaleAspectFit
        self.imageMainLogo.backgroundColor = UIColor.appLogo_bgColor()
       // self.imageMainLogo.alpha = 0.7
        
        picker.delegate = self
        self.btnSelectPayment.layer.borderColor = UIColor.lightGray.cgColor
        self.btnSelectPayment.layer.borderWidth = 1.0
        // Do any additional setup after loading the view.
        
        btnCompanyVerify.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 5);
        btnCompanyVerify.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        
        self.btnUserImage.layer.masksToBounds = true
        
        Globals.layoutViewFor(self.btnUserImage, color: UIColor.appThemeColor(), width: 1.5, cornerRadius: self.btnUserImage.frame.size.height/2)
       // print("hi - 1")
       // self.flagPermissionForCameraAccess = self.permission()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK:- =====text field delegate ====
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        UIView.animateKeyframes(withDuration: 0.20, delay: 0.0, options: .overrideInheritedOptions, animations: { [weak self]() -> Void in
            self?.view.frame.origin.y = 0
            }, completion: nil)
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var keyboardHeight:CGFloat = 0.0
        if textField == txtFirstName{
            if DeviceType.IS_IPHONE_4_OR_LESS{
                keyboardHeight = -90
            }
            else if DeviceType.IS_IPHONE_5{
                keyboardHeight = -60
            }
        }
        
        if textField == txtLastName{
            if DeviceType.IS_IPHONE_4_OR_LESS{
                keyboardHeight = -130
            }
            else if DeviceType.IS_IPHONE_5{
                keyboardHeight = -100
            }
        }
        if textField == txtCompanyName{
            if DeviceType.IS_IPHONE_4_OR_LESS{
                keyboardHeight = -170
            }
            else if DeviceType.IS_IPHONE_5{
                keyboardHeight = -140
            }
        }
        UIView.animateKeyframes(withDuration: 0.20, delay: 0.0, options: .overrideInheritedOptions, animations: { [weak self]() -> Void in
            self?.view.frame.origin.y = keyboardHeight
            }, completion: nil)
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    // MARK:- ====Custom Methods action ===
    
    @IBAction func btnSelectPaymentTypeAction(_ sender: UIButton) {
        self.view.endEditing(true)
        var dicUser = [String:Any]()
        dicUser["id"] = self.userId as Any?
        dicUser["uniquecode"] = self.txtCompanyName.text! as Any?

        //Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(dicUser as Any?, key: Constants.SignUpProcessData)
         let actionSheet = UIActionSheet(title: "Choose card type", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Business","Personal")
        actionSheet.tag = 1000
        actionSheet.show(in: self.view)
        
    }
    func scanCard() {
    }
    func showCardIOCardIOPaymentView()
    {
        let scanViewController: CardIOPaymentViewController = CardIOPaymentViewController(paymentDelegate: self)
        scanViewController.modalPresentationStyle = .formSheet
        scanViewController.collectCardholderName = true

        DispatchQueue.main.async { [weak self]() -> Void in
            if self == nil {
                return
            }
            self?.present(scanViewController, animated: true, completion: nil)
        }
    }
    // MARK: ========CardIOPaymentViewControllerDelegate=====
    func userDidProvide(_ info: CardIOCreditCardInfo, in paymentViewController: CardIOPaymentViewController) {
        NSLog("Scan succeeded with info: %@", info)
        // Do whatever needs to be done to deliver the purchased items.
        self.dismiss(animated: true, completion: nil)
//        strCardNumber = info.cardNumber
//        strCvvNumberCard = info.cvv
//        strExpDateCard =  String(format: "%lu/%lu", info.expiryMonth, info.expiryYear)
        dicParameter["card_number"] = info.cardNumber as Any?
        dicParameter["cvv_number"] = info.cvv as Any?
        dicParameter["cc_name"] = "\(info.cardholderName!)"

      //  print("info.expiryMonth", info.expiryMonth)
        
       // let strExpiryMonth = "\(info.expiryMonth)".characters.count == 1 ? "0" + "\(info.expiryMonth)" : "\(info.expiryMonth)"
        dicParameter["expiry_date"] = String(format: "%02lu/%lu", info.expiryMonth, info.expiryYear)//String(format: "%@/%lu", strExpiryMonth, info.expiryYear) as Any?
        self.btnSelectPayment.setTitle(info.redactedCardNumber , for: UIControlState())
    }
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController) {
        NSLog("User cancelled scan")
        self.dismiss(animated: true, completion: nil)
    }
    // MARK:- ==Web Call get , add, delete user cards from server  =====
func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int){
        if actionSheet.tag == 1000{
            switch buttonIndex{
            case 1 :
                self.cardType = "Business"
                self.permission()
            case 2:
                self.cardType  = "Personal"
                self.permission()
            default : break
            }
        }
        else
        {
                picker.allowsEditing = false //2
                switch buttonIndex{
                case 1 :
                    self.openCameraWithType(sourceType: .photoLibrary)
                case 2:
                    self.openCameraWithType(sourceType: .camera)
                default : break
                }
               }
    }
    
    func openCameraWithType(sourceType: UIImagePickerControllerSourceType) {
     
        switch sourceType {
        case .photoLibrary:
            picker.sourceType = sourceType
            picker.modalPresentationStyle = .fullScreen
            DispatchQueue.main.async(execute: {[weak self] () -> Void in
                self!.present((self?.picker)!, animated: true, completion: nil)
            })

        case .camera:
            picker.sourceType = sourceType
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            DispatchQueue.main.async(execute: {[weak self] () -> Void in
                self!.present((self?.picker)!, animated: true, completion: nil)
            })

        default:
            break
        }
    }
    @IBAction func btnImageUploadAction(_ sender: UIButton) {
        self.activeButton = sender
        let actionSheet = UIActionSheet(title: "Select", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Photo Gallery","Camera")
        actionSheet.show(in: self.view)
    }
    // MARK:- ====button submit action ===
    @IBAction func btnContinueAndSubmitAction(_ sender: UIButton) {
        if self.txtFirstName.text == "" {
            Globals.ToastAlertWithString("Please enter first name")
            return
        }
        else if self.txtLastName.text == "" {
            Globals.ToastAlertWithString("Please enter last name")
            return
        }
        else   if self.btnCompanyVerify.isSelected == false && self.txtCompanyName.text != ""{
            Globals.ToastAlertWithString("Please verify company to proceed")
            return
        }
        else if dicParameter["card_number"] == nil {
            Globals.ToastAlertWithString("Please select payment type")
            return
        }
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }
        appDelegate.showProgressWithText("Please Wait...")
        dicParameter["first_name"] = self.txtFirstName.text! as Any?
        dicParameter["last_name"] = self.txtLastName.text! as Any?
        dicParameter["zip_code"] = "" as Any?
        dicParameter["user_id"] = self.userId as Any?
        dicParameter["card_type"] = self.cardType as Any?
        dicParameter["profile_image"] = self.strImageBase64 as Any?
        dicParameter["billing_type_id"] = "2" as Any?//String(self.dicCardDetails["payment_method"]!)
        dicParameter["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?
        dicParameter["uniquecode"] = self.txtCompanyName.text!
        dicParameter["acc_comp_id"] = self.acc_comp_id
        dicParameter["company_id"] = Constants.CompanyID
        dicParameter["alias_id"] = self.alias_id
   
        
        ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.signup_step2, dicParameters: dicParameter, successBlock: {[weak self] (dicData) in
                 appDelegate.hideProgress()
                if dicData["status"] as! String == "session_expired"{
                    Globals.ToastAlertWithString(dicData["message"] as! String)
                }else {
                  //  print("Sign Up --\(dicData)")
                    if dicData["status"] as! String == "success" {
                        Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.SignUpProcessData)
                        Globals.ToastAlertWithString(dicData["message"] as! String)
                        self?.navigationController?.popToRootViewController(animated: true)
                    }
                    else {
                         Globals.ToastAlertWithString(dicData["message"] as! String)
                    }
                }
            
        }) { (error:NSError?) in
                  appDelegate.hideProgress()
        }
    }
    // MARK:- ====action sheet delegate ===
    func permission() {
        let authStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if authStatus == .authorized {
            self.showCardIOCardIOPaymentView()
        }
        else if authStatus == .notDetermined {
            NSLog("%@", "Camera access not determined. Ask for permission.")
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(granted: Bool) -> Void in
                if granted {
                    NSLog("Granted access to %@", AVMediaTypeVideo)
                    self.showCardIOCardIOPaymentView()
                }
                else {
                    NSLog("Not granted access to %@", AVMediaTypeVideo)
                    let alert = UIAlertView(title: "Camera Access Denied", message: "Please go to Settings -> Privacy -> Enable camera access for app.", delegate: nil, cancelButtonTitle: "Cancel")
                    alert.show()
                }
            })
        }
        else  {
            // My own Helper class is used here to pop a dialog in one simple line.
            let alert = UIAlertView(title: "Camera Access Denied", message: "Please go to Settings -> Privacy -> Enable camera access for app.", delegate: nil, cancelButtonTitle: "Cancel")
            alert.show()
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        let dataImage = UIImageJPEGRepresentation(resizeImage(image), 0.1)
        self.strImageBase64 = dataImage!.base64EncodedString(options: .lineLength64Characters)
        self.btnUserImage.setImage(image, for: UIControlState())
        dismiss(animated: true, completion: nil) //5
    }

    func resizeImage(_ image: UIImage) -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 150, height: 150))
        image.draw(in: CGRect(x: 0, y: 0, width: 150, height: 150))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
/*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func btnCompanyVerificationClicked(_ sender: UIButton) {
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }
        //COMP8921
        
        if sender.isSelected == true{
            self.btnCompanyVerify.isSelected = false
        }
        else{
            
            if self.txtCompanyName.text!.characters.count == 0{
                return
            }
            
        appDelegate.showProgressWithText("Verifying company details...")
        var param_Dictionary = [String:Any]()
        param_Dictionary["code"] = "\(String(describing: self.txtCompanyName.text!))"
        ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.verifycompany, dicParameters: param_Dictionary, successBlock: { [weak self](dicData) -> Void in
           // print("dictionary recived --\(dicData)")
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success" {
                DispatchQueue.main.async(execute: {
                    Globals.ToastAlertWithString("Company Verified")
                    self?.btnCompanyVerify.isSelected = true
                    //["acc_comp_id": 89, "status": success, "exist": 1, "company_id": 40, "alias_id": 0]
                    if let acc_id = dicData["acc_comp_id"]{
                        self?.acc_comp_id = "\(acc_id)"
                    }
                    if let al_ID = dicData["alias_id"]{
                        self?.alias_id = "\(al_ID)"
                    }
                })
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
                self?.btnCompanyVerify.isSelected = false
                self?.txtCompanyName.text = ""

            }
        }) { (error) -> () in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString("Please try again.")
        }
        }
    }
    
    
    func deleteAddedAccount(){
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }
        appDelegate.showProgressWithText("Please wait...")
        var param_Dictionary = [String:Any]()
        param_Dictionary["user_id"] = self.userId as Any?
        ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.cancel_registraion, dicParameters: param_Dictionary, successBlock: { [weak self](dicData) -> Void in
           // print("dictionary recived --\(dicData)")
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success" {
                DispatchQueue.main.async(execute: {
                    self?.navigationController?.popToRootViewController(animated: true)
                    //remove any unfinished registration data
                    Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.SignUpProcessData)
                })
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) -> () in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString("Please try again.")
        }
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "Confirmation", message: "Do you want to cancel registration process?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:{[weak self] (ACTION :UIAlertAction!)in
            self?.deleteAddedAccount()
            }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
