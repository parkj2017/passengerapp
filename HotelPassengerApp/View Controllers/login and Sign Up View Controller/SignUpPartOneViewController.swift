//
//  SignUpPartOneViewController.swift
//  PassangerApp
//
//  Created by Netquall on 11/30/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import UIKit
import CoreTelephony
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

class SignUpPartOneViewController: BaseStatusBarViewController, UITextFieldDelegate {
    
    // MARK:- ======property declaration =======
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var accessaryViewPhone: UIView!
    @IBOutlet weak var contentViewScroll: UIView!
    
    @IBOutlet weak var viewLogoMain: UIView!
    @IBOutlet weak var imageMainLogo: UIImageView!
    //@IBOutlet weak var btnImageUploadOutlet: UIButton!
    @IBOutlet weak var btnContinueOutlet: UIButton!
    // first View Properties
    
    @IBOutlet weak var viewFirstInput: UIView!
    @IBOutlet var txtUserName: UITextField!
    
    @IBOutlet weak var txtEmailID: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtVerifyPassowrd: UITextField!
    @IBOutlet weak var txtSelectCountry: UITextField!
    
    var activeTextField : UITextField?
    var dicParameter = [String:String]()
    
    var userId:String = ""
    var lblCountryPhoneCode = UITextField()

    // MARK:-   =====view life cycle====
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // get current locala country
        //let currentLocal = NSLocale.currentLocale()
        //let countryCode = currentLocal.objectForKey(NSLocaleCountryCode) as! String
       // let USlocal = NSLocale(localeIdentifier: "en_US")
//        let country = currentLocal.displayNameForKey(NSLocaleCountryCode, value: countryCode)
//        self.txtSelectCountry.text! = country!
        
        self.txtSelectCountry.placeholder = "Country"
        self.txtPhoneNumber.inputAccessoryView = self.accessaryViewPhone;
        self.txtPhoneNumber.keyboardType = .numberPad
        UIColor.appThemeColor()
        self.imageMainLogo.layer.masksToBounds = true
        self.imageMainLogo.contentMode = .scaleAspectFit
        self.imageMainLogo.backgroundColor = UIColor.appLogo_bgColor()
      //  self.imageMainLogo.alpha = 0.7
        
        
//        let networkInfo = CTTelephonyNetworkInfo()
//        if let carrier = networkInfo.subscriberCellularProvider{
//            print ("\(carrier.isoCountryCode)")
//            self.txtSelectCountry.text! = country! + " (+\(carrier.mobileCountryCode!))"
//        }
//
     
        self.lblCountryPhoneCode.textAlignment = .center
        self.lblCountryPhoneCode.placeholder = "Code"
        self.lblCountryPhoneCode.isUserInteractionEnabled = false
        self.lblCountryPhoneCode.textColor = UIColor.gray
        self.lblCountryPhoneCode.font = Globals.defaultAppFont(16)
        self.txtEmailID.autocorrectionType = .no
        self.lblCountryPhoneCode.frame = CGRect(x: 0, y: 0, width: (DeviceType.IS_IPAD ? 100 : 65), height: self.txtPhoneNumber.frame.height)
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: (DeviceType.IS_IPAD ? 100 : 65), height: self.txtPhoneNumber.frame.height))
        leftView.addSubview(self.lblCountryPhoneCode)
        self.txtPhoneNumber.leftView = leftView
        self.txtPhoneNumber.leftViewMode = .always
        let rightBorder = CALayer()
        rightBorder.borderColor = UIColor.gray.cgColor
        rightBorder.borderWidth = 1
        rightBorder.frame = CGRect(x: self.lblCountryPhoneCode.frame.width - 2, y: 0,  width: 1, height: self.lblCountryPhoneCode.frame.height)
        self.lblCountryPhoneCode.layer.addSublayer(rightBorder)

        if let userdata = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.SignUpProcessData) as? [String:String]{
            
            dicParameter = userdata
            
            self.txtEmailID.text = String(dicParameter["email"]!)
            self.txtUserName.text = String(dicParameter["username"]!)
            self.txtPhoneNumber.text = String(dicParameter["mobile"]!)
            self.txtSelectCountry.text = String(dicParameter["country"]!)
            self.lblCountryPhoneCode.text = String(dicParameter["code"]!)
           }

    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.SetNavigationBarThemeColor()
        registerForKeybaordNotification()
        
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        unRegisterForKeboardNotification()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle
    {
        return UIStatusBarStyle.lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //country and country codes
    func getCountryCallingCode(_ countryRegionCode:String) -> String {
        let prefix =  ["AF": ["Afghanistan","93"],
                       "AX": ["Aland Islands","358"],
                       "AL": ["Albania","355"],
                       "DZ": ["Algeria","213"],
                       "AS": ["American Samoa","1"],
                       "AD": ["Andorra","376"],
                       "AO": ["Angola","244"],
                       "AI": ["Anguilla","1"],
                       "AQ": ["Antarctica","672"],
                       "AG": ["Antigua and Barbuda","1"],
                       "AR": ["Argentina","54"],
                       "AM": ["Armenia","374"],
                       "AW": ["Aruba","297"],
                       "AU": ["Australia","61"],
                       "AT": ["Austria","43"],
                       "AZ": ["Azerbaijan","994"],
                       "BS": ["Bahamas","1"],
                       "BH": ["Bahrain","973"],
                       "BD": ["Bangladesh","880"],
                       "BB": ["Barbados","1"],
                       "BY": ["Belarus","375"],
                       "BE": ["Belgium","32"],
                       "BZ": ["Belize","501"],
                       "BJ": ["Benin","229"],
                       "BM": ["Bermuda","1"],
                       "BT": ["Bhutan","975"],
                       "BO": ["Bolivia","591"],
                       "BA": ["Bosnia and Herzegovina","387"],
                       "BW": ["Botswana","267"],
                       "BV": ["Bouvet Island","47"],
                       "BQ": ["BQ","599"],
                       "BR": ["Brazil","55"],
                       "IO": ["British Indian Ocean Territory","246"],
                       "VG": ["British Virgin Islands","1"],
                       "BN": ["Brunei Darussalam","673"],
                       "BG": ["Bulgaria","359"],
                       "BF": ["Burkina Faso","226"],
                       "BI": ["Burundi","257"],
                       "KH": ["Cambodia","855"],
                       "CM": ["Cameroon","237"],
                       "CA": ["Canada","1"],
                       "CV": ["Cape Verde","238"],
                       "KY": ["Cayman Islands","345"],
                       "CF": ["Central African Republic","236"],
                       "TD": ["Chad","235"],
                       "CL": ["Chile","56"],
                       "CN": ["China","86"],
                       "CX": ["Christmas Island","61"],
                       "CC": ["Cocos (Keeling) Islands","61"],
                       "CO": ["Colombia","57"],
                       "KM": ["Comoros","269"],
                       "CG": ["Congo (Brazzaville)","242"],
                       "CD": ["Congo, Democratic Republic of the","243"],
                       "CK": ["Cook Islands","682"],
                       "CR": ["Costa Rica","506"],
                       "CI": ["Côte d'Ivoire","225"],
                       "HR": ["Croatia","385"],
                       "CU": ["Cuba","53"],
                       "CW": ["Curacao","599"],
                       "CY": ["Cyprus","537"],
                       "CZ": ["Czech Republic","420"],
                       "DK": ["Denmark","45"],
                       "DJ": ["Djibouti","253"],
                       "DM": ["Dominica","1"],
                       "DO": ["Dominican Republic","1"],
                       "EC": ["Ecuador","593"],
                       "EG": ["Egypt","20"],
                       "SV": ["El Salvador","503"],
                       "GQ": ["Equatorial Guinea","240"],
                       "ER": ["Eritrea","291"],
                       "EE": ["Estonia","372"],
                       "ET": ["Ethiopia","251"],
                       "FK": ["Falkland Islands (Malvinas)","500"],
                       "FO": ["Faroe Islands","298"],
                       "FJ": ["Fiji","679"],
                       "FI": ["Finland","358"],
                       "FR": ["France","33"],
                       "GF": ["French Guiana","594"],
                       "PF": ["French Polynesia","689"],
                       "TF": ["French Southern Territories","689"],
                       "GA": ["Gabon","241"],
                       "GM": ["Gambia","220"],
                       "GE": ["Georgia","995"],
                       "DE": ["Germany","49"],
                       "GH": ["Ghana","233"],
                       "GI": ["Gibraltar","350"],
                       "GR": ["Greece","30"],
                       "GL": ["Greenland","299"],
                       "GD": ["Grenada","1"],
                       "GP": ["Guadeloupe","590"],
                       "GU": ["Guam","1"],
                       "GT": ["Guatemala","502"],
                       "GG": ["Guernsey","44"],
                       "GN": ["Guinea","224"],
                       "GW": ["Guinea-Bissau","245"],
                       "GY": ["Guyana","595"],
                       "HT": ["Haiti","509"],
                       "VA": ["Holy See (Vatican City State)","379"],
                       "HN": ["Honduras","504"],
                       "HK": ["Hong Kong, Special Administrative Region of China","852"],
                       "HU": ["Hungary","36"],
                       "IS": ["Iceland","354"],
                       "IN": ["India","91"],
                       "ID": ["Indonesia","62"],
                       "IR": ["Iran, Islamic Republic of","98"],
                       "IQ": ["Iraq","964"],
                       "IE": ["Ireland","353"],
                       "IM": ["Isle of Man","44"],
                       "IL": ["Israel","972"],
                       "IT": ["Italy","39"],
                       "JM": ["Jamaica","1"],
                       "JP": ["Japan","81"],
                       "JE": ["Jersey","44"],
                       "JO": ["Jordan","962"],
                       "KZ": ["Kazakhstan","77"],
                       "KE": ["Kenya","254"],
                       "KI": ["Kiribati","686"],
                       "KP": ["Korea, Democratic People's Republic of","850"],
                       "KR": ["Korea, Republic of","82"],
                       "KW": ["Kuwait","965"],
                       "KG": ["Kyrgyzstan","996"],
                       "LA": ["Lao PDR","856"],
                       "LV": ["Latvia","371"],
                       "LB": ["Lebanon","961"],
                       "LS": ["Lesotho","266"],
                       "LR": ["Liberia","231"],
                       "LY": ["Libya","218"],
                       "LI": ["Liechtenstein","423"],
                       "LT": ["Lithuania","370"],
                       "LU": ["Luxembourg","352"],
                       "MO": ["Macao, Special Administrative Region of China","853"],
                       "MK": ["Macedonia, Republic of","389"],
                       "MG": ["Madagascar","261"],
                       "MW": ["Malawi","265"],
                       "MY": ["Malaysia","60"],
                       "MV": ["Maldives","960"],
                       "ML": ["Mali","223"],
                       "MT": ["Malta","356"],
                       "MH": ["Marshall Islands","692"],
                       "MQ": ["Martinique","596"],
                       "MR": ["Mauritania","222"],
                       "MU": ["Mauritius","230"],
                       "YT": ["Mayotte","262"],
                       "MX": ["Mexico","52"],
                       "FM": ["Micronesia, Federated States of","691"],
                       "MD": ["Moldova","373"],
                       "MC": ["Monaco","377"],
                       "MN": ["Mongolia","976"],
                       "ME": ["Montenegro","382"],
                       "MS": ["Montserrat","1"],
                       "MA": ["Morocco","212"],
                       "MZ": ["Mozambique","258"],
                       "MM": ["Myanmar","95"],
                       "NA": ["Namibia","264"],
                       "NR": ["Nauru","674"],
                       "NP": ["Nepal","977"],
                       "NL": ["Netherlands","31"],
                       "AN": ["Netherlands Antilles","599"],
                       "NC": ["New Caledonia","687"],
                       "NZ": ["New Zealand","64"],
                       "NI": ["Nicaragua","505"],
                       "NE": ["Niger","227"],
                       "NG": ["Nigeria","234"],
                       "NU": ["Niue","683"],
                       "NF": ["Norfolk Island","672"],
                       "MP": ["Northern Mariana Islands","1"],
                       "NO": ["Norway","47"],
                       "OM": ["Oman","968"],
                       "PK": ["Pakistan","92"],
                       "PW": ["Palau","680"],
                       "PS": ["Palestinian Territory, Occupied","970"],
                       "PA": ["Panama","507"],
                       "PG": ["Papua New Guinea","675"],
                       "PY": ["Paraguay","595"],
                       "PE": ["Peru","51"],
                       "PH": ["Philippines","63"],
                       "PN": ["Pitcairn","872"],
                       "PL": ["Poland","48"],
                       "PT": ["Portugal","351"],
                       "PR": ["Puerto Rico","1"],
                       "QA": ["Qatar","974"],
                       "RE": ["Réunion","262"],
                       "RO": ["Romania","40"],
                       "RU": ["Russian Federation","7"],
                       "RW": ["Rwanda","250"],
                       "SH": ["Saint Helena","290"],
                       "KN": ["Saint Kitts and Nevis","1"],
                       "LC": ["Saint Lucia","1"],
                       "PM": ["Saint Pierre and Miquelon","508"],
                       "VC": ["Saint Vincent and Grenadines","1"],
                       "BL": ["Saint-Barthélemy","590"],
                       "MF": ["Saint-Martin (French part)","590"],
                       "WS": ["Samoa","685"],
                       "SM": ["San Marino","378"],
                       "ST": ["Sao Tome and Principe","239"],
                       "SA": ["Saudi Arabia","966"],
                       "SN": ["Senegal","221"],
                       "RS": ["Serbia","381"],
                       "SC": ["Seychelles","248"],
                       "SL": ["Sierra Leone","232"],
                       "SG": ["Singapore","65"],
                       "SX": ["Sint Maarten","1"],
                       "SK": ["Slovakia","421"],
                       "SI": ["Slovenia","386"],
                       "SB": ["Solomon Islands","677"],
                       "SO": ["Somalia","252"],
                       "ZA": ["South Africa","27"],
                       "GS": ["South Georgia and the South Sandwich Islands","500"],
                       "SS​": ["South Sudan","211"],
                       "ES": ["Spain","34"],
                       "LK": ["Sri Lanka","94"],
                       "SD": ["Sudan","249"],
                       "SR": ["Suriname","597"],
                       "SJ": ["Svalbard and Jan Mayen Islands","47"],
                       "SZ": ["Swaziland","268"],
                       "SE": ["Sweden","46"],
                       "CH": ["Switzerland","41"],
                       "SY": ["Syrian Arab Republic (Syria)","963"],
                       "TW": ["Taiwan, Republic of China","886"],
                       "TJ": ["Tajikistan","992"],
                       "TZ": ["Tanzania, United Republic of","255"],
                       "TH": ["Thailand","66"],
                       "TL": ["Timor-Leste","670"],
                       "TG": ["Togo","228"],
                       "TK": ["Tokelau","690"],
                       "TO": ["Tonga","676"],
                       "TT": ["Trinidad and Tobago","1"],
                       "TN": ["Tunisia","216"],
                       "TR": ["Turkey","90"],
                       "TM": ["Turkmenistan","993"],
                       "TC": ["Turks and Caicos Islands","1"],
                       "TV": ["Tuvalu","688"],
                       "UG": ["Uganda","256"],
                       "UA": ["Ukraine","380"],
                       "AE": ["United Arab Emirates","971"],
                       "GB": ["United Kingdom","44"],
                       "US": ["USA","1"],
                       "UY": ["Uruguay","598"],
                       "UZ": ["Uzbekistan","998"],
                       "VU": ["Vanuatu","678"],
                       "VE": ["Venezuela (Bolivarian Republic of)","58"],
                       "VN": ["Viet Nam","84"],
                       "VI": ["Virgin Islands, US","1"],
                       "WF": ["Wallis and Futuna Islands","681"],
                       "EH": ["Western Sahara","212"],
                       "YE": ["Yemen","967"],
                       "ZM": ["Zambia","260"],
                       "ZW": ["Zimbabwe","263"]]
        
        if let countryDialingCode = prefix[countryRegionCode] {
            return countryDialingCode[1]
        }
        return "0 "
    }

    // MARK:- =====text field delegate ====
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.activeTextField?.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        self.activeTextField = textField
        self.txtPassword.layer.borderColor = UIColor.clear.cgColor
        self.txtVerifyPassowrd.layer.borderWidth = 1.0
        self.txtVerifyPassowrd.layer.borderColor = UIColor.clear.cgColor
        if textField == self.txtSelectCountry {
            self.txtSelectCountry.resignFirstResponder()
            self.performSegue(withIdentifier: "toSelectCountry", sender: nil)
            return false
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.txtVerifyPassowrd {
            if self.txtPassword.text != self.txtVerifyPassowrd.text {
                self.txtPassword.layer.borderWidth = 1.0
                self.txtPassword.layer.borderColor = UIColor.appBlackColor().cgColor
                self.txtVerifyPassowrd.layer.borderWidth = 1.0
                self.txtVerifyPassowrd.layer.borderColor = UIColor.appBlackColor().cgColor
            }
        }
        else if textField == self.txtEmailID{
            if self.isValidEmail(textField.text!) == true{
                
            }
            else {
                let alert = UIAlertView(title: "Alert !", message: "Please enter valid email id.", delegate: nil, cancelButtonTitle: "Okay")
                alert.show()
                
            }
        }
        
    }
    func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    // MARK:- ====key will Shown and Hide Notification ===
    func registerForKeybaordNotification()
    {
        NotificationCenter.default.addObserver(self, selector:#selector(SignUpPartOneViewController.KeyboardWillShown(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpPartOneViewController.KeyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    func unRegisterForKeboardNotification()
    {
        NotificationCenter.default.removeObserver(self)
    }
    func KeyboardWillShown(_ notification: Notification)
    {
        let userInfo = notification.userInfo!
        let keyboardSize = ((userInfo[UIKeyboardFrameEndUserInfoKey]! as Any) as AnyObject).cgRectValue.size
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        var rectView = self.view.frame
        rectView.size.height -= keyboardSize.height //- 15.0
        if self.activeTextField != nil && !rectView.contains(self.activeTextField!.frame.origin)
        {
             self.scrollView.scrollRectToVisible(self.activeTextField!.frame, animated: true)
        }
        
    }
    func KeyboardWillHide(_ notification: Notification)
    {
        
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    func validateForm() -> Bool
    {
        if (self.txtUserName.text == "")
        {
            Globals.ToastAlertWithString("Please enter username")
            return false
        }
        else if (self.txtEmailID.text == "")
        {
            Globals.ToastAlertWithString("Please enter email id")
            return false
        }
        else if self.isValidEmail(txtEmailID.text!) == false{
            Globals.ToastAlertWithString("Please enter valid email id")
            return false
        }
        else if (self.txtPassword.text == "")
        {
            Globals.ToastAlertWithString("Please choose password")
            return false
        }
        else if (self.txtSelectCountry.text == "")
        {
            Globals.ToastAlertWithString("Please choose your country")
            return false
        }
        
        return true
    }
    
    @IBAction func btnDonePhoneAction(_ sender: UIBarButtonItem)
    {
        if sender.tag == 19
        {
            self.txtPhoneNumber.resignFirstResponder()
        }
    }
    
    // MARK:- ====Custom Methods action ===
    
    
    @IBAction func btnContinueAndSubmitAction(_ sender: UIButton) {
        

        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }
        if !(self.validateForm()) {
              return
        }
        else if  (self.txtPassword.text != self.txtVerifyPassowrd.text)
            {
                Globals.ToastAlertWithString("Password mismatch")
                return
        }
        appDelegate.showProgressWithText("Please Wait...")
        
        dicParameter["email"] = self.txtEmailID.text!
        dicParameter["username"] = self.txtUserName.text!
        
        
//        let networkInfo = CTTelephonyNetworkInfo()
//        if let carrier = networkInfo.subscriberCellularProvider{
//            print ("\(carrier.isoCountryCode)")
//            dicParameter["mobile"] = "(+\(carrier.mobileCountryCode!))\(self.txtPhoneNumber.text!)"
//        }
//        else{
        dicParameter["mobile"] = self.lblCountryPhoneCode.text! + self.txtPhoneNumber.text!
       // }
        
        dicParameter["password"] = self.txtPassword.text!
        dicParameter["company_id"] = Constants.CompanyID
        dicParameter["country"] = self.txtSelectCountry.text!
        dicParameter["code"] = self.lblCountryPhoneCode.text!
        
        ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.signUp, dicParameters: dicParameter, successBlock: { [weak self](dicData) -> Void in
            if dicData["status"] as! String == "success"{
                appDelegate.hideProgress()
           //     print("sign Up 1st --\(dicData)")
                self?.dicParameter["user_id"] = String(describing: dicData["user_id"]!)
                self?.userId = String(describing: dicData["user_id"]!)
                self?.dicParameter["complete"] = "step1"
                Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(self?.dicParameter, key: Constants.SignUpProcessData)

                let signUp2ndVC = self?.storyboard?.instantiateViewController(withIdentifier: "SignUpPartSecondVC") as! SignUpPartSecondVC
                signUp2ndVC.userId =  String(describing: dicData["user_id"]!)
                self?.navigationController?.pushViewController(signUp2ndVC, animated: true)
                
            }
            else {
                appDelegate.hideProgress()
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
            }, failureBlock: { (error) -> () in
                appDelegate.hideProgress()
                let alert = UIAlertView(title: "Alert !", message: "Please check your inputs and try again.", delegate: nil, cancelButtonTitle: "Okay")
                alert.show()
                
        })
    }
    // MARK: - =========Navigation========
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSelectCountry"{
            let selectCountryVC = segue.destination as! SelectCountryViewController
            selectCountryVC.pushAssignCountryBlock = { [weak self] (strCountry, strMobileCode) -> () in
                self?.txtSelectCountry.text! = strCountry
                self?.lblCountryPhoneCode.text = "+" + self!.getCountryCallingCode(strMobileCode)
                self?.txtSelectCountry.resignFirstResponder()
            }
            
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    // MARK:- ====Custom Methods action ===
    func deleteAddedAccount(){
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }
        
        appDelegate.showProgressWithText("Please wait...")
        var param_Dictionary = [String:Any]()
        param_Dictionary["user_id"] = self.userId as Any?
        ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.cancel_registraion, dicParameters: param_Dictionary, successBlock: { [weak self](dicData) -> Void in
           // print("dictionary recived --\(dicData)")
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success" {
                DispatchQueue.main.async(execute: {
                    self?.navigationController?.popViewController(animated: true)
                    
                    //remove any unfinished registration data
                    Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.SignUpProcessData)
                    
                    
                })
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) -> () in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString("Please try again.")
        }
    }
    

    @IBAction func btnBackAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "Confirmation", message: "Do you want to cancel registration process?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:{[weak self] (ACTION :UIAlertAction!)in
            if self?.userId.characters.count > 0
            {
                self?.deleteAddedAccount()
            }
            else {
                self?.navigationController?.popViewController(animated: true)
            }
            }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}// class brackets
