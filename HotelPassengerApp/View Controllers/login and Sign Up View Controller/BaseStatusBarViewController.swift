//
//  BaseStatusBarViewController.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 19/01/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
class BaseStatusBarViewController: UIViewController {
     var StatusView : UIView!
    // MARK:- =====View Life Cycle=== ===
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
 override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
        //self.navigationController?.navigationBar.SetNavigationBarThemeColor()
        self.StatusView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 20.0))
        self.StatusView.backgroundColor = UIColor(red: 37.0/255.0, green: 37.0/255.0, blue: 37.0/255.0, alpha: 1.0)
        self.view.addSubview(self.StatusView)
        self.view.bringSubview(toFront: self.StatusView)
}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
