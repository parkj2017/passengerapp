 //
//  SelectCountryViewController.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 20/01/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import CoreTelephony
class SelectCountryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CustomSearchControllerDelegate {
    // MARK:- ======property declaration =======
    // @IBOutlet weak var searchDisplay: UISearchDisplayController!
    var listOfCountries : [[String:String]]!
    var filteredTableData = [[String:String]]()
    var resultSearchController = UISearchController()
    var shouldShowSearchResults = false
    var pushAssignCountryBlock : ((_ strCountry:String, _ strPhoneCode: String) -> ())!
    var customSearchController: CustomSearchController!
    var carrier: CTCarrier!
    @IBOutlet weak var tableViewCountry: UITableView!
    // MARK:-   =====view life cycle====
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Select Country"
        self.navigationController!.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
        self.listOfCountries = [[String:String]]()
        ///get country list ////
        var countries = [[String:String]]()
        for countryCode: String in Locale.isoRegionCodes {
            let identifier: String = Locale.identifier(fromComponents: [
                NSLocale.Key.countryCode.rawValue : countryCode
                ] )
            let country: String = (Locale.current as NSLocale).displayName(forKey: NSLocale.Key.identifier, value: identifier)!
            var dicCounrty = [String:String]()
            dicCounrty[country] = countryCode
            countries.append(dicCounrty)
        }
        let sortedCountry :[[String:String]] = countries.sorted { (S1, S2) -> Bool in
            S1.keys.first!.localizedCompare(S2.keys.first!) == ComparisonResult.orderedAscending
        }
        //let sortedCountries: [String] = countries.sort { $0.localizedCaseInsensitiveCompare($1 as String) == NSComparisonResult.OrderedAscending }        //countries.sortedArrayUsingSelector("localizedCaseInsensitiveCompare:")
        NSLog("country %@", sortedCountry)
        self.listOfCountries = sortedCountry
        //    let network_Info: CTTelephonyNetworkInfo = CTTelephonyNetworkInfo()
        //      self.carrier = network_Info.subscriberCellularProvider!
        //    NSLog("country code is: %@", (carrier.mobileNetworkCode)!)
        self.tableViewCountry.dataSource = self
        self.tableViewCountry.delegate = self
        self.configureCustomSearchController()
    }
    func configureCustomSearchController() {
        self.customSearchController = CustomSearchController(searchResultsController: self, searchBarFrame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: 50.0), searchBarFont: UIFont(name: "Oxygen", size: 18.0)!, searchBarTextColor: UIColor.appBlackColor(), searchBarTintColor: UIColor.white)
        self.customSearchController.customSearchBar.placeholder = "Search Country"
        self.tableViewCountry.tableHeaderView = customSearchController.customSearchBar
        customSearchController.customDelegate = self
    }
    // MARK: - ======== table view  data source and  delegate ========
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        if (shouldShowSearchResults) {
            return self.filteredTableData.count
        }
        else {
            return self.listOfCountries.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier: String = "CellSelectCountry"
        var cell: UITableViewCell? = self.tableViewCountry.dequeueReusableCell(withIdentifier: CellIdentifier)!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CellIdentifier)
        }
        cell!.textLabel!.font = UIFont(name: "Oxygen", size: 18)
        let dicCountry :[String:String]!
        if (shouldShowSearchResults) {
            dicCountry = self.filteredTableData[indexPath.row]
        }
        else {
            dicCountry = self.listOfCountries[indexPath.row]
        }
        cell!.textLabel!.text = dicCountry.keys.first!
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicCountry :[String:String]!
        if (shouldShowSearchResults) {
            dicCountry = self.filteredTableData[indexPath.row]
        }
        else {
            dicCountry = self.listOfCountries[indexPath.row]
        }
        self.pushAssignCountryBlock(dicCountry.keys.first!, dicCountry.values.first!)
        //self.pushAssignCountryBlock(strCountry: selected_Country,strPhoneCode: self.carrier.mobileCountryCode!)
       // self.pushAssignCountryBlock(strCountry: dicCountry.keys.first!,strPhoneCode:  dicCountry.values.first!)
        //        NSUserDefaults.standardUserDefaults().setObject(selected_Country, forKey: "CountryName")
        //        NSUserDefaults.standardUserDefaults().synchronize()
        self.navigationController?.popViewController(animated: true)
    }
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if (shouldShowSearchResults) {
            return nil
         }
        return ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    }
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        let newRow: Int!
        if (shouldShowSearchResults) {
            if self.filteredTableData.count == 0 {
                return 0
            }
            newRow = self.indexForFirstChar(title, inArray: self.filteredTableData)
        }
        else {
            if self.listOfCountries.count == 0 {
                return 0
            }
            newRow = self.indexForFirstChar(title, inArray: self.listOfCountries)
        }
        let newIndexPath: IndexPath = IndexPath(row: newRow, section: 0)
        tableView.scrollToRow(at: newIndexPath, at: .top, animated: false)
        return index
    }
    // Return the index for the location of the first item in an array that begins with a certain character
    
    func indexForFirstChar(_ character: String, inArray array: [[String:String]]) -> Int {
        var count: Int = 0
        for dicCountry in array {
            if dicCountry.keys.first!.hasPrefix(character) {
                return count
            }
            count += 1
        }
        return 0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    // MARK: CustomSearchControllerDelegate functions
    func didStartSearching() {
        shouldShowSearchResults = true
        self.tableViewCountry.reloadData()
    }
    func didTapOnSearchButton() {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            self.tableViewCountry.reloadData()
        }
    }
    func didTapOnCancelButton() {
        shouldShowSearchResults = false
        self.tableViewCountry.reloadData()
    }
    func didChangeSearchText(_ searchText: String) {
        // Filter the data array and get only those countries that match the search text.
        filteredTableData.removeAll(keepingCapacity: false)
        //  let searchPredicate = NSPredicate(format: "SELF beginswith[c]%@",searchText)
        let array = self.listOfCountries.filter{
            let string = $0.keys.first!
            return string.hasPrefix(searchText)
        }  // (self.listOfCountries as NSArray).filteredArrayUsingPredicate(searchPredicate)
        self.filteredTableData = array
        self.tableViewCountry.reloadData()
    }
    //func updateSearchResultsForSearchController(searchController: UISearchController)
    //{
    //         filteredTableData.removeAll(keepCapacity: false)
    //        
    //        let searchPredicate = NSPredicate(format: "SELF beginswith[c]%@", searchController.searchBar.text!)
    //        let array = (self.listOfCountries as NSArray).filteredArrayUsingPredicate(searchPredicate)
    //        filteredTableData = array as! [String]
    //        self.tableViewCountry.reloadData()
    //    }
}// class brackets
