//
//  SignUpPartSecondVC.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 19/01/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
enum verifyType : Int{
    case email = 0
    case phone = 1
    case none
}
class SignUpPartSecondVC: BaseStatusBarViewController, UITextFieldDelegate {

// MARK:- ============ Property decalaration ==========

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var accessaryViewPhone: UIView!
    @IBOutlet weak var contentViewScroll: UIView!
    
    var typeVerifySelect : verifyType!
    @IBOutlet weak var imageMainLogo: UIImageView!
     var activeTextField : UITextField?
    var userId : String!
    var isVarifiedEmail = false
     var isVarifiedPhone = false
    @IBOutlet weak var txtVerificationEmailCode: UITextField!
    @IBOutlet weak var txtVerificationPhoneCode: UITextField!
    
    @IBOutlet weak var lblVerifyEmailID: UILabel!
    @IBOutlet weak var lblVerifyPhoneNo: UILabel!

    var txtActiveField:UITextField!
// MARK: - ======== View LIfe Cycyle   ========
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
       
        self.imageMainLogo.layer.masksToBounds = true
        self.imageMainLogo.contentMode = .scaleAspectFit
        self.imageMainLogo.backgroundColor = UIColor.appLogo_bgColor()
       // self.imageMainLogo.alpha = 0.7

        self.txtVerificationEmailCode.delegate = self
        self.txtVerificationPhoneCode.delegate = self
 
        lblVerifyEmailID.isHidden = true
        lblVerifyPhoneNo.isHidden = true
        
        self.txtVerificationEmailCode.keyboardType = .numberPad
        self.txtVerificationPhoneCode.keyboardType = .numberPad

        self.txtVerificationEmailCode.inputAccessoryView = self.accessaryViewPhone;
        self.txtVerificationPhoneCode.inputAccessoryView = self.accessaryViewPhone;

        // Do any additional setup after loading the view.

    }

    @IBAction func btnDoneClicked(_ sender: UIBarButtonItem) {
        self.txtActiveField.resignFirstResponder()
        if self.txtActiveField.text == "" {
            return
        }
        if self.txtActiveField == self.txtVerificationEmailCode {
            self.MakeRequestVerifyEmailandPhoneOfUser(verifyType.email)
        }else if self.txtActiveField == self.txtVerificationPhoneCode {
            self.MakeRequestVerifyEmailandPhoneOfUser(verifyType.phone)
        }
        UIView.animateKeyframes(withDuration: 0.20, delay: 0.0, options: .overrideInheritedOptions, animations: { [weak self]() -> Void in
            self?.view.frame.origin.y = 0
            }, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK:- =====text field delegate ====
   
func textFieldShouldReturn(_ textField: UITextField) -> Bool
{
        textField.resignFirstResponder()
     UIView.animateKeyframes(withDuration: 0.20, delay: 0.0, options: .overrideInheritedOptions, animations: { [weak self]() -> Void in
        self?.view.frame.origin.y = 0
        }, completion: nil)

        return true
}
func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
{
    var keyboardHeight:CGFloat = 0.0
    if textField == self.txtVerificationEmailCode {
        if DeviceType.IS_IPHONE_4_OR_LESS{
            keyboardHeight = -90
        }
        else {
        keyboardHeight = -60
        }
        self.txtActiveField = self.txtVerificationEmailCode
    }else if textField == self.txtVerificationPhoneCode {
        if DeviceType.IS_IPHONE_4_OR_LESS{
            keyboardHeight = -130
        }
        else {
        keyboardHeight = -100
        }
        self.txtActiveField = self.txtVerificationPhoneCode
    }
    UIView.animateKeyframes(withDuration: 0.20, delay: 0.0, options: .overrideInheritedOptions, animations: { [weak self]() -> Void in
         self?.view.frame.origin.y = keyboardHeight
        }, completion: nil)
     return true
  }
func textFieldDidEndEditing(_ textField: UITextField) {

}
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if string.characters.count == 0 {
            if textField == self.txtVerificationEmailCode {
                self.lblVerifyEmailID.isHidden = true
                self.lblVerifyEmailID.textColor = UIColor.darkGray
            }
            else if textField == self.txtVerificationPhoneCode {
                self.lblVerifyPhoneNo.isHidden = true
                self.lblVerifyPhoneNo.textColor = UIColor.darkGray
                
            }
        }
        return true
    }
    // MARK:- ====webcall to verify email and phone ===
 
    
func MakeRequestVerifyEmailandPhoneOfUser(_ verifyTypeLocal:verifyType){
        
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }
    
        var param_Dictionary = [String:Any]()
        var  urlString = ""
        param_Dictionary["user_id"] = self.userId! as Any?
       // param_Dictionary["session"] = session
        if verifyTypeLocal.rawValue ==  verifyType.email.rawValue {
            urlString = "user_email_verification"
            param_Dictionary["email_verification_code"] = self.txtVerificationEmailCode.text as Any?
            urlString = "user_email_verification"
            lblVerifyEmailID.isHidden = false
            lblVerifyEmailID.text = "wait..."
        }else if verifyTypeLocal.rawValue ==  verifyType.phone.rawValue  {
            urlString = "user_mobile_verification"
            param_Dictionary["mobile_verification_code"] = self.txtVerificationPhoneCode.text as Any?
            urlString = "user_mobile_verification"
            lblVerifyPhoneNo.isHidden = false
            lblVerifyPhoneNo.text = "wait..."
        }
    
        ServiceManager.sharedInstance.dataTaskWithPostRequest(urlString, dicParameters: param_Dictionary, successBlock: { [weak self](dicData) -> Void in
             // print("dictionary recived --\(dicData)")
              appDelegate.hideProgress()
            if dicData["status"] as! String == "success" {
                Globals.ToastAlertWithString(dicData["message"] as! String)
                if dicData["message"] as! String == "User mobile verification completed successfully." {
                   self?.isVarifiedPhone = true
                    self?.lblVerifyPhoneNo.textColor = UIColor.green
                    self?.lblVerifyPhoneNo.text = "Verified"
                }else {
                    self?.isVarifiedEmail = true
                    self?.lblVerifyEmailID.textColor = UIColor.green
                    self?.lblVerifyEmailID.text = "Verified"
                }
                
            }else {
                DispatchQueue.main.async(execute: {
                    if verifyTypeLocal.rawValue ==  verifyType.email.rawValue {
                        self?.lblVerifyEmailID.isHidden = true
                        self?.lblVerifyEmailID.textColor = UIColor.darkGray
                    }else if verifyTypeLocal.rawValue ==  verifyType.phone.rawValue  {
                        self?.lblVerifyPhoneNo.isHidden = true
                        self?.lblVerifyPhoneNo.textColor = UIColor.darkGray
                    }
                    Globals.ToastAlertWithString(dicData["message"] as! String)
                })
            }
            }) {[weak self] (error) -> () in
                
                DispatchQueue.main.async(execute: { 
                    if verifyTypeLocal.rawValue ==  verifyType.email.rawValue {
                        self?.lblVerifyEmailID.isHidden = true
                        self?.lblVerifyEmailID.textColor = UIColor.darkGray

                    }else if verifyTypeLocal.rawValue ==  verifyType.phone.rawValue  {
                        self?.lblVerifyPhoneNo.isHidden = true
                        self?.lblVerifyPhoneNo.textColor = UIColor.darkGray
                    }
                    appDelegate.hideProgress()
                    Globals.ToastAlertWithString("Please try again.")
                })
           }
    }
    // MARK:- ====Custom Methods action ===
    func deleteAddedAccount(){
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }

        appDelegate.showProgressWithText("Please wait...")
        var param_Dictionary = [String:Any]()
        param_Dictionary["user_id"] = self.userId! as Any?
        ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.cancel_registraion, dicParameters: param_Dictionary, successBlock: { [weak self](dicData) -> Void in
          //  print("dictionary recived --\(dicData)")
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success" {
                DispatchQueue.main.async(execute: { 
                    self?.navigationController?.popToRootViewController(animated: true)
                    
                    //remove any unfinished registration data
                    Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.SignUpProcessData)
                    

                })
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) -> () in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString("Please try again.")
        }
    }
    
    
@IBAction func btnContinueAndSubmitAction(_ sender: UIButton) {

    if self.isVarifiedEmail == false {
        Globals.ToastAlertWithString("Please enter email verificaiton code.")
        return
    }
    if self.isVarifiedPhone == false {
        Globals.ToastAlertWithString("Please enter phone verificaiton code.")
        return
    }
    
    var dicParameter = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.SignUpProcessData) as! [String:String]
    dicParameter["user_id"] = String(self.userId)
    dicParameter["complete"] = "step2"
    Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(dicParameter as Any?, key: Constants.SignUpProcessData)

    self.performSegue(withIdentifier: "toFinelSignUp", sender: self)
    
}
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let signThird = segue.destination as! SignUpPartThirdVC
        signThird.userId = self.userId
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "Confirmation", message: "Do you want to cancel registration process?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:{[weak self] (ACTION :UIAlertAction!)in
                 self?.deleteAddedAccount()
            }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
