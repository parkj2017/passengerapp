//
//  LoginViewController.swift
//  PassangerApp
//
//  Created by Netquall on 11/30/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import UIKit
import Crashlytics
import Fabric

class LoginViewController: BaseStatusBarViewController, UITextFieldDelegate, UIAlertViewDelegate {
 // MARK: -===== Properties Declaration=============
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var txtEmailOrMobile: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnKeepMeLoggedIn: UIButton!
    
    @IBOutlet weak var imageMainLogo: UIImageView!

    //var StatusView : UIView!
    /*
    WCTSG  id 269
    APLUXURYLIMOSERVICE id  119
    ABCALS id 173
    GLLService id 278
    */
    
    /*
    VIP Global (149)---
    ETSAMS (166)
    Global Chauffeur(http://globalchauffeur.co.uk/) (248)---
    Blackbird (26)
    Luxury Ride (117)
    CTG (240)
    Hallsley Limo (128), As per discussion with Kalpana company name is (JAMES LIMOUSINE SERVICE)
    Dansk Limousine Service (165)
    city trans ---118
    parc limo 103
    limo stop 131
    www.ClassiqueLimo.com - 146
     Modicus: 326
     VIPLS: 325
     

NYG Limousine, ID: 330).
Friendly Ride Limousine, ID: 331).
 US Limo System, ID: 333).
 DC Livery, ID: 332).
 Robinson Transportation Group LLC, ID: 334).

     

    */
// MARK: - ======== View Life Cycyle   ========

override func viewDidLoad() {
        super.viewDidLoad()
    
    self.imageMainLogo.layer.masksToBounds = true
    self.imageMainLogo.contentMode = .scaleAspectFit
    self.imageMainLogo.backgroundColor =  UIColor.appLogo_bgColor()
 //   self.imageMainLogo.alpha = 0.7

    let showPassButton = UIButton(type: .custom)
    showPassButton.frame = CGRect(x: 0, y: 0, width: 55, height: self.txtPassword.frame.size.height) //(0, 0, 55, self.txtPassword.frame.size.height)
    
    showPassButton.backgroundColor = UIColor.clear
    showPassButton.setImage(UIImage(named:"_showPassword"), for: .normal)
    showPassButton.setImage(UIImage(named:"showPassword"), for: .selected)
    self.txtPassword.rightView = showPassButton
    showPassButton.addTarget(self, action: #selector(LoginViewController.showEnteredPassword(_:)), for: UIControlEvents.touchUpInside)

    self.txtPassword.rightViewMode = .always

    self.txtEmailOrMobile.autocorrectionType = .no
    self.txtPassword.autocorrectionType = .no
    
    if let scrollView = self.view.viewWithTag(6677) {
        if let cotentView = scrollView.viewWithTag(6677) {
            if let smallView = cotentView.viewWithTag(6677) {
                if let buttonSignUp = smallView.viewWithTag(7766) as? UIButton {
                    let attrsString = NSMutableAttributedString(string: "New User? ", attributes: [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: Globals.defaultAppFont(15)])
                    attrsString.append(NSAttributedString(string: "Sign Up", attributes: [NSForegroundColorAttributeName: UIColor.appThemeColor(), NSFontAttributeName: Globals.defaultAppFont(15)]))
                    buttonSignUp.setAttributedTitle(attrsString, for: .normal)
                }
            }
        }
    }
    self.txtEmailOrMobile.placeholder = "Username"
    //if user intiated signup process but didn't complete
        if let dicParameter = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.SignUpProcessData) as? [String:String]{
            

            if dicParameter["complete"] != nil{
             //   print("Step2 complete", dicParameter)

               // print(String(describing: dicParameter["complete"]), dicParameter)
                
              //  print("Step2 complete")

                if String(dicParameter["complete"]!) == "step1"{
                    if dicParameter["user_id"] != nil{
                    let signUp2ndVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpPartSecondVC") as! SignUpPartSecondVC
                    signUp2ndVC.userId =  String(dicParameter["user_id"]!)
                    self.navigationController?.pushViewController(signUp2ndVC, animated: true)
                    }
                    else {
                    self.performSegue(withIdentifier: "goToSignUP", sender: self)
                    }
                }
                else if String(dicParameter["complete"]!) == "step2"{
                    if dicParameter["user_id"] != nil{
                        let signUp2ndVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpPartThirdVC") as! SignUpPartThirdVC
                        signUp2ndVC.userId =  String(dicParameter["user_id"]!)
                        self.navigationController?.pushViewController(signUp2ndVC, animated: true)
                    }
                    else {
                        self.performSegue(withIdentifier: "goToSignUP", sender: self)
                    }
                }
            }
    }
    if  let flag = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.KeepMeLogin) as? Bool
    {
        self.btnKeepMeLoggedIn.isSelected = flag
    }
    if  (Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) != nil)
    {
    if let boolVal = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.KeepMeLogin) as? Bool{
        if boolVal == true{
            if  let userType = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).user_type") as? String{
                self.navigateToHomeScreen("\(String(describing: userType))")
            }
        }
      }
    }
}
    
    func showEnteredPassword(_ sender:UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true{
            self.txtPassword.isSecureTextEntry = false
        }
        else {
            self.txtPassword.isSecureTextEntry = true
        }
    }
    func navigateToHomeScreen(_ forUserType:String) {
        
        if forUserType == "booker"{
            let story_board = UIStoryboard(name: "Booker", bundle: nil)
            let accoutListingVC = story_board.instantiateViewController(withIdentifier: "PAAccoutListingViewController") as! PAAccoutListingViewController
            self.navigationController?.pushViewController(accoutListingVC, animated: false)
            Constants.isBookerLogin = true
        }else {
            Constants.isBookerLogin = false

        if Constants.GC_THEME == "1"{
            let mainMenuVC = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            self.navigationController?.pushViewController(mainMenuVC, animated: false)
        }
        else {
            let mainMenuVC = self.storyboard?.instantiateViewController(withIdentifier: "PAMainMenuViewController") as! PAMainMenuViewController
            self.navigationController?.pushViewController(mainMenuVC, animated: false)
        }
        }
      
    }
override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeybaordNotification()
    }
override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.unRegisterForKeboardNotification()
}
override var preferredStatusBarStyle : UIStatusBarStyle
    {
        return UIStatusBarStyle.lightContent
    }
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnHelpClicked(_ sender: UIButton) {
        let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
        detailsVc.view.backgroundColor = UIColor.clear
        detailsVc.modalPresentationStyle = .overFullScreen
        detailsVc.providesPresentationContextTransitionStyle = true
        detailsVc.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overFullScreen
        self.present(detailsVc, animated: true) {
        }
    }
// MARK: - ======== text field delegate  ========
func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true
    }
    // MARK:- ====key will Shown and Hide Notification ===
func registerForKeybaordNotification()
    {
        NotificationCenter.default.addObserver(self, selector:#selector(LoginViewController.KeyboardWillShown(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.KeyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
}
func unRegisterForKeboardNotification()
{
        NotificationCenter.default.removeObserver(self)
}
func KeyboardWillShown(_ notification: Notification)
{
            //let userInfo = notification.userInfo!
            //let keyboardSize = userInfo[UIKeyboardFrameEndUserInfoKey]!.CGRectValue.size
            //let pointInTable:CGPoint!
       //let contentInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0)
    if self.view.frame.origin.y != -70 {
    UIView.animateKeyframes(withDuration: 0.10, delay: 0.10, options: .overrideInheritedOptions, animations: { [weak self]() -> Void in
         self!.view.frame.origin.y -= 70.0
        }, completion: nil)
    }
}
func KeyboardWillHide(_ notification: Notification)
{
        UIView.animateKeyframes(withDuration: 0.10, delay: 0.10, options:.overrideInheritedOptions , animations: {[weak self] () -> Void in
            self!.view.frame.origin.y = 0.0
            }, completion: nil)
}

    // MARK: - ======---Navigation--========
    // In a storyboard-based application, you will often want to do a little preparation before navigation
override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//    if segue.identifier == "toMainMenu" {
//        let mainMenuVC = segue.destinationViewController as! PAMainMenuViewController
//        
//    }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
    if buttonIndex == 1{
    }
    }
// MARK: - ======== button outlet action ========
@IBAction func btnRemeberMeAction(_ sender: UIButton) {
    sender.isSelected = !sender.isSelected
}
@IBAction func btnLoginActionClicked(_ sender: UIButton) {
     Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue( self.btnKeepMeLoggedIn.isSelected as Any? , key: Constants.KeepMeLogin)
    if (self.txtEmailOrMobile.text == ""){
        Globals.ToastAlertWithString("Please enter username")
    }
    else if self.txtPassword.text == "" {
        Globals.ToastAlertWithString("Please enter password")
    }
    else{
        if (appDelegate.networkAvialable == true) {
        Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(true as Any?, key: "loginWeb")
        appDelegate.showProgressWithText("Please wait...")
      //UIApplication.sharedApplication().delegate as! AppDelegate.showProgressWithText("Updating Profile...")
        var dicParameters : [String:Any]! = Dictionary()
        dicParameters["email"] = self.txtEmailOrMobile.text as Any?
        dicParameters["password"] = self.txtPassword.text as Any?
        dicParameters["platform"] = "iphone" as Any?
            dicParameters["app_type"] = (Constants.needForHotel == "1") ? "hotel" as Any? : "Passenger"
        if let _ = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.DeviceToken) as? String {
              dicParameters["device_token"] = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.DeviceToken)
        }else {
            dicParameters["device_token"] = "80921n 908109 129 19231" as Any?
        }
        dicParameters["company_id"] = Constants.CompanyID
            
            
            
        weak var weakSelf = self
        ServiceManager.sharedInstance.dataTaskWithPostRequest("login", dicParameters: dicParameters, successBlock:
            { (dicRecievedJSON) -> Void in
                if weakSelf == nil {
                     appDelegate.hideProgress()
                    return 
                }
           // print("Responser 1----- \(dicRecievedJSON)")
            if dicRecievedJSON != nil
            {
              if "\(dicRecievedJSON["status"]!)" == "success"
              {
                Crashlytics.sharedInstance().setUserEmail("\(self.txtEmailOrMobile.text!)")
                Crashlytics.sharedInstance().setUserIdentifier("companyID - " + Constants.CompanyID)
                Crashlytics.sharedInstance().setUserName("\(self.txtEmailOrMobile.text!)")
                
                let dicRecievedJSON = dicRecievedJSON.formatDictionaryForNullValues(dicRecievedJSON)
//Crashlytics.sharedInstance().crash()
                
                Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.SignUpProcessData)
                Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(dicRecievedJSON?["record"] as! [String:Any] as Any?, key: Constants.ActiveUserInfo)
                Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(false as Any? , key: "loginWeb")
                
                appDelegate.distanceType = (dicRecievedJSON?["record"] as! [String:Any])["radiusunit"] as! String
                appDelegate.distanceUnit = ((dicRecievedJSON?["record"] as! [String:Any])["radiusunit"] as! String).uppercased()
                 appDelegate.distanceMultiplier = appDelegate.distanceType == "mile" ? 0.000621371 : 0.001
                 appDelegate.currency = (dicRecievedJSON?["record"] as! [String:Any])["currency"] as! String
                appDelegate.ondemandRadius = (dicRecievedJSON?["record"] as! [String:Any])["on_demand_radius"] as! String
                self.navigateToHomeScreen((dicRecievedJSON?["record"] as! [String:Any])["user_type"] as! String)
                 appDelegate.hideProgress()
              }else {
                Globals.ToastAlertWithString(dicRecievedJSON["message"] as! String)
                appDelegate.hideProgress()
                }
            }
        }) { (error) -> () in
                 (UIApplication.shared.delegate! as! AppDelegate).hideProgress()
                Globals.ToastAlertWithString("Please try again")
                print(error)
        }
        }else {
         //   JLToast.makeText("Please check your network connection", duration: JLToastDelay.ShortDelay).show()
        }
    }
}
    @IBAction func signupButtonClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "goToSignUP", sender: self)
    }
}// class bracket
