//
//  ForgetPasswordViewController.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 10/06/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {
    @IBOutlet var txtEmailIDForgetPass: UITextField!

    @IBOutlet var txtUserName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ForgetPasswordViewController.tapGestureAction(_:)))
        self.view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeybaordNotification()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.unRegisterForKeboardNotification()
    }
    // MARK:- ====key will Shown and Hide Notification ===
    func registerForKeybaordNotification()
    {
        NotificationCenter.default.addObserver(self, selector:#selector(ForgetPasswordViewController.KeyboardWillShown(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ForgetPasswordViewController.KeyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    func unRegisterForKeboardNotification()
    {
        NotificationCenter.default.removeObserver(self)
    }
    func KeyboardWillShown(_ notification: Notification)
    {
        let userInfo = notification.userInfo!
        _ = ((userInfo[UIKeyboardFrameEndUserInfoKey]! as Any) as AnyObject).cgRectValue.size
        //let pointInTable:CGPoint!
        //let contentInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0)
        if self.view.frame.origin.y != -70 {
            UIView.animateKeyframes(withDuration: 0.10, delay: 0.10, options: .overrideInheritedOptions, animations: { [weak self]() -> Void in
                self?.view.frame.origin.y -= 70.0
                }, completion: nil)
        }
        
    }
    func KeyboardWillHide(_ notification: Notification)
    {
        UIView.animateKeyframes(withDuration: 0.10, delay: 0.10, options:.overrideInheritedOptions , animations: {[weak self] () -> Void in
            self?.view.frame.origin.y = 0.0
            }, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tapGestureAction(_ tapGesture:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnResetPasswordClicked(_ sender: UIButton) {
        
        var emailOrUserID = ""
        if self.txtEmailIDForgetPass.text != ""{
            emailOrUserID = self.txtEmailIDForgetPass.text!
        }
        else {
            emailOrUserID = self.txtUserName.text!
        }

        if emailOrUserID == "" {
            let alert = self.alertView("Forgot Password", message:"Please provide your email or username", delegate:nil, cancelButton:"Ok", otherButons:nil)
            alert?.show()
            
        }else{
            if (appDelegate.networkAvialable == true) {
                
                appDelegate.showProgressWithText("Please wait...")
                self.view.endEditing(true)
                
                var dicParameters : [String:Any]! = Dictionary()

                if self.txtEmailIDForgetPass.text! != ""{
                dicParameters["email"] = self.txtEmailIDForgetPass.text! as Any?
                }
                else
                {
                dicParameters["username"] = self.txtUserName.text! as Any?
                }

                    dicParameters["company_id"] = Constants.CompanyID as Any?
                
                ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.forgetPassword, dicParameters: dicParameters, successBlock:
                    { [weak self](dicRecievedJSON) -> Void in
                        appDelegate.hideProgress()

                        if dicRecievedJSON != nil
                        {
                            if let status = dicRecievedJSON["status"]
                            {
                                if "\(status)".compare("success") == true{
                                self?.dismiss(animated: true, completion: nil)
                                if let msg = dicRecievedJSON["message"]{
                                    Globals.ToastAlertWithString("\(msg)")
                                }
                                }
                                else {
                                    if let msg = dicRecievedJSON["message"]{
                                        Globals.ToastAlertWithString("\(msg)")
                                    }
                                }
                            }
                        }
                }) { (error) -> () in
                    appDelegate.hideProgress()
                    Globals.ToastAlertWithString("Please try again")
                    print(error)
                }
                
            }else {
                let alert = self.alertView("Alert", message:"Please Check Network Connection", delegate:nil, cancelButton:"Ok", otherButons:nil)
                alert?.show()
            }
        }
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if textField == self.txtEmailIDForgetPass{
            if !(self.txtUserName.text?.isEmpty)!{
                txtUserName.text = ""
            }
        }
        else if textField == self.txtUserName{
            if !(self.txtEmailIDForgetPass.text?.isEmpty)!{
                self.txtEmailIDForgetPass.text = ""
            }
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
