//
//  PADriverFeedbackPopupVC.swift
//  PassangerApp
//
//  Created by Netquall on 6/23/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
enum viewTagsName :Int {
    case lblDate = 20
    case lblTime = 21
    case lblVehicleType = 22
    case lblVehicleName = 23
    case imageDriver = 50
    case ratingView = 60
}
class PADriverFeedbackPopupVC: BaseViewController {

    // MARK: - ===property ===
    @IBOutlet var viewTimeDateVehicleDetails: UIView!
    @IBOutlet var viewDriverImage: UIView!
    
    @IBOutlet var viewRating: UIView!
    @IBOutlet weak var tblViewDriverFeedback: UITableView!
    
    var dicJobDeatils:[String:AnyObject]!
    var dicDriverInfo : [String:AnyObject]!
    // MARK: - ===view life cycle ===
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDriverInfomation()
        let tapGesture = UITapGestureRecognizer(target: self, action: "tapGestureAction:")
        self.view.viewWithTag(96)!.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tapGestureAction(tapGesture:UITapGestureRecognizer){
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func reloadViewAgain()  {
        let imageDriver = self.viewDriverImage.viewWithTag(viewTagsName.imageDriver.rawValue) as! UIImageView
        imageDriver.image = UIImage(named: "driver_img")
        weak var weakImageDriver = imageDriver
        if let str = self.dicJobDeatils["image"] as? String{
            DLImageLoader.sharedInstance.imageFromUrl(str) { (error, image) -> () in
                if image != nil
                {
                    dispatch_async(dispatch_get_main_queue(), {
                        weakImageDriver!.image = image
                    })
                }
            }
        }
        if Constants.iPadScreen == true {
            imageDriver.layer.cornerRadius = CGRectGetHeight(imageDriver.frame) / 2.0
        }else {
            imageDriver.layer.cornerRadius = 50.0
        }
        imageDriver.layer.masksToBounds = true
        dispatch_async(dispatch_get_main_queue()) { [weak self] in
            self?.tblViewDriverFeedback.reloadData()
        }
    }
    // MARK: - ============table View Data Source  ==========
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if self.dicJobDeatils == nil {
            return 0
        }
       return 4
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 2 {
            if let arrComment = self.dicJobDeatils["comments"] as? [AnyObject] {
                return arrComment.count
            }
        }
         return 0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        var cell : UITableViewCell?
        
        let  cellIndetifier = "cellFeedbackDriverComments"
        //cellCarInfo
        cell  = self.tblViewDriverFeedback.dequeueReusableCellWithIdentifier(cellIndetifier, forIndexPath: indexPath)
        var lblPaxName : UILabel!
        var txtViewComments : UITextView!
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: cellIndetifier)
            lblPaxName = cell!.viewWithTag(62) as! UILabel
            txtViewComments = cell!.viewWithTag(63)! as! UITextView
            
        }else {
            lblPaxName = cell!.viewWithTag(62) as! UILabel
            txtViewComments = cell!.viewWithTag(63)! as! UITextView
            
        }
        if let arrComment = self.dicJobDeatils["comments"] as? [[String:AnyObject]] {
           let dicComments = arrComment[indexPath.row]
            let strPaxname = dicComments["passenger_name"] as? String
            lblPaxName.text = strPaxname == "" ? "N/A" : strPaxname!
            txtViewComments.text = dicComments["passenger_comment"] as? String
        }
        lblPaxName.numberOfLines = 1
        lblPaxName.font = Globals.sharedInstance.defaultAppFontWithBold(16)
        lblPaxName.textColor = UIColor.blackColor()
        txtViewComments.font = Globals.sharedInstance.defaultAppFontItalic(13)
        txtViewComments.textColor = UIColor.DefaultLightBlackColor()
        return cell!
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if let arrComment = self.dicJobDeatils["comments"] as? [[String:AnyObject]] {
            let constraint: CGSize = CGSizeMake(CGRectGetWidth(tblViewDriverFeedback.frame), 20000.0)
            let dicComments = arrComment[indexPath.row]
           let strComments = dicComments["passenger_comment"] as? String
            var size: CGSize
            let boundingBox: CGSize = strComments!.boundingRectWithSize(constraint, options: .UsesLineFragmentOrigin, attributes: [NSFontAttributeName:Globals.sharedInstance.defaultAppFontItalic(13)], context: nil).size
            size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height))
            return size.height > 70 ? size.height + 30 : 70
        }
        return 70.0
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch (section){
        case 0:
            if Constants.iPadScreen == true {
                return CGRectGetHeight(self.view.frame) - 350
            }
            return 120
        case 1:
            if Constants.iPadScreen == true {
                return CGRectGetHeight(self.view.frame) - 350
            }
            return 155
        case 2:
            return 0
        case 3:
            return 70
        default :
            return 70
        }
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch(section){
        case 0:
            self.viewDriverImage.translatesAutoresizingMaskIntoConstraints = true
            //        var heightView : CGFloat!
            //        if Constants.iPadScreen == true {
            //            heightView =  CGRectGetHeight(self.view.frame) - 350
            //        }else {
            //            heightView = 192.0
            //        }
            self.viewDriverImage.frame = CGRectMake(0,0,CGRectGetWidth(self.view.frame),120)
            return self.viewDriverImage
        case 1:
            
             let lblDate = self.viewTimeDateVehicleDetails.viewWithTag(viewTagsName.lblDate.rawValue) as! UILabel
             lblDate.text = (self.dicJobDeatils["pu_date"] as! String)
             let lblTime = self.viewTimeDateVehicleDetails.viewWithTag(viewTagsName.lblTime.rawValue) as! UILabel
             lblTime.text = (self.dicJobDeatils["pu_time"]! as! String)
             
             let lblVehicleType = self.viewTimeDateVehicleDetails.viewWithTag(viewTagsName.lblVehicleType.rawValue) as! UILabel
             lblVehicleType.text = (self.dicJobDeatils["vehicle_type"]! as! String)
             let lblVehicleName = self.viewTimeDateVehicleDetails.viewWithTag(viewTagsName.lblVehicleName.rawValue) as! UILabel
             lblVehicleName.text = (self.dicJobDeatils["fleet"]! as! String)
             self.viewTimeDateVehicleDetails.translatesAutoresizingMaskIntoConstraints = true
             self.viewTimeDateVehicleDetails.frame = CGRectMake(0,0,CGRectGetWidth(self.view.frame),155)
             return  self.viewTimeDateVehicleDetails
            
        case 2:
            return  nil
        case 3:
            var ratingStr = self.dicJobDeatils["rating"] as? String
            if ratingStr == nil {
                ratingStr = ""
            }
            let ratinFloat = ratingStr == "" ? 0.0 : ratingStr!.toDouble()
            let floatingRatesStar = self.viewRating.viewWithTag(viewTagsName.ratingView.rawValue) as! CosmosView
            floatingRatesStar.rating = ratinFloat!
            floatingRatesStar.totalStars = 5
            floatingRatesStar.userInteractionEnabled = false
            self.viewRating.translatesAutoresizingMaskIntoConstraints = true
            self.viewRating.frame = CGRectMake(0,0,CGRectGetWidth(self.view.frame),70)
            return self.viewRating
            
        default :
            break
        }
        return nil
    }
    func getDriverInfomation()  {
        if appDelegate.networkAvialable == false   {
            return
        }
        appDelegate.showProgressWithText("Please Wait...")
        let userInfo = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:AnyObject]
        var dicParameters = [String:AnyObject]()
        dicParameters["driver_id"] = self.dicDriverInfo["primary_driver"]!
        dicParameters["user_id"] = userInfo["id"]!
        dicParameters["session"] = userInfo["session"]!
        dicParameters["reservation_id"] = self.dicDriverInfo["id"]!
        ServiceManager.sharedInstance.dataTaskWithPostRequest("getDriverAllInfomation", dicParameters: dicParameters, successBlock: { [weak self](dicData:[String:AnyObject]!) in
            appDelegate.hideProgress()
            print(dicData)
            if dicData["status"] as! String == "success"{
                self?.dicJobDeatils = dicData["record"] as! [String:AnyObject]
                self?.reloadViewAgain()
            }else {
              Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString(error.description)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
