//
//  PAPaymentHistoryViewController.swift
//  HotelPassengerApp
//
//  Created by Netquall on 22/08/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit

enum K_PAYMENT_HISTORY_TYPE {
    case K_PAID_RESERVATIONS
    case K_UNPAID_RESERVATIONS
    case K_SEARCH_RESULTS
}
enum K_SELECTED_DATE_TYPE {
    case K_FROM_DATE
    case K_TO_DATE
    case K_KEY_NAME
    case K_KEYWORD
}


class searchFiltersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txtFromDate: UITextField!
    @IBOutlet weak var txtToDate: UITextField!
    @IBOutlet weak var txtSearchIn: UITextField!
    @IBOutlet weak var txtSearchFor: UITextField!
    @IBOutlet weak var buttonGo: UIButton!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //self.configureCell()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func configureCell() {
        
        Globals.layoutViewFor(self.txtFromDate, color: UIColor.gray, width: 1, cornerRadius: 3)
        Globals.layoutViewFor(self.txtToDate, color: UIColor.gray, width: 1, cornerRadius: 3)
        Globals.layoutViewFor(self.txtSearchIn, color: UIColor.gray, width: 1, cornerRadius: 3)
        Globals.layoutViewFor(self.txtSearchFor, color: UIColor.gray, width: 1, cornerRadius: 3)
        Globals.layoutViewFor(self.buttonGo, color: UIColor.gray, width: 1, cornerRadius: 3)
        
        let d_image = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
        d_image.contentMode = .center
        d_image.image = UIImage(named: "calendar")
        self.txtFromDate.leftView = d_image
        self.txtFromDate.leftViewMode = .always
        
        let d1_image = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
        d1_image.contentMode = .center
        d1_image.image = UIImage(named: "calendar")
        self.txtToDate.leftView = d1_image
        self.txtToDate.leftViewMode = .always
        
         let imageDropDown = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
        imageDropDown.contentMode = .center
        imageDropDown.image = UIImage(named: "dropdown")
        self.txtSearchIn.rightView = imageDropDown
        self.txtSearchIn.rightViewMode = .always
        
        let view_1 = UIImageView(frame: CGRect(x: 0, y: 0, width: 5, height: 50))
        self.txtSearchIn.leftView = view_1
        self.txtSearchIn.leftViewMode = .always
        
        let view_2 = UIImageView(frame: CGRect(x: 0, y: 0, width: 5, height: 50))
        self.txtSearchFor.leftView = view_2
        self.txtSearchFor.leftViewMode = .always
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
class PAPaymentHistoryViewController: UIViewController, passengerListingaActionDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, BackButtonActionProtocol {
    @IBOutlet weak var tableAccoutPaymentHistory: UITableView!
    
    let selectionLine:UIView = UIView()
    var pageNumber:Int = 1
    var loading = false
    var totalJobRowsForSearch:Int = 0
    
    var arrPaidReservationData = [[String:Any]]()
    var arrUnpaidReservationData = [[String:Any]]()
    var arrSearchResultsData = [[String:Any]]()
    
    var reservationType:K_PAYMENT_HISTORY_TYPE = .K_PAID_RESERVATIONS
    @IBOutlet weak var txtEmailID: UITextField!
    
    @IBOutlet weak var viewSendEmail: UIView!
    
    @IBOutlet var viewDatePicker: UIView!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var searchKeysPicker: UIPickerView!
    
    var selectedDateType:K_SELECTED_DATE_TYPE = .K_FROM_DATE
    var showSearchFilterView = false
    var dict_searchItems = [["Search Term":""], ["Amount":"amount"], ["Name":"first_name"], ["Booking No":"conf_id"]]
    var searchFilterDictionary = ["from":"", "to":"", "for":"", "keyword":""]
    
    
    var flagApplySearch = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectionLine.backgroundColor = UIColor.appThemeColor()
       // self.view.addSubview(self.selectionLine)
        self.selectionLine.frame = CGRect(x: 0, y: 41, width:  self.view.frame.width/2, height: 3)
        self.getPaymentHistory(forType: self.reservationType)
        
        self.viewSendEmail.frame = CGRect(x: 20, y: 60, width:  self.view.frame.width - 40, height: 250)
        self.searchKeysPicker.isHidden = true
        
        self.view.updateConstraints()

        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Payment History", imageName: "backArrow")
        Globals.sharedInstance.delegateBack = self
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Back nav button delegate
    
    func backButtonActionMethod(_ sender: UIButton) {
        Globals.sharedInstance.delegateBack = nil
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Send email view
    
    @IBAction func sendEmailViewCancelButtonClicked(_ sender: Any) {
    }
    @IBAction func sendEmailViewSendButtonClicked(_ sender: UIButton) {
    }
    @IBAction func toggleTopBarButtons(_ sender: UIButton) {
        
        if sender.tag == 101{
            reservationType = .K_PAID_RESERVATIONS
            self.selectionLine.frame = CGRect(x: 0, y: 41, width:  sender.frame.width, height: 3)
        }
        else {
            reservationType = .K_UNPAID_RESERVATIONS
            self.selectionLine.frame = CGRect(x: sender.frame.width, y: 41, width:  sender.frame.width, height: 3)
        }
        self.pageNumber = 0
        self.showSearchFilterView = !self.showSearchFilterView
        self.searchFilterDictionary = ["from":"", "to":"", "for":"", "keyword":""]
        self.flagApplySearch = false
        self.getPaymentHistory(forType: self.reservationType)
        
    }
    func getPaymentHistory(forType:K_PAYMENT_HISTORY_TYPE)
    {
        if (appDelegate.networkAvialable == true) {
            appDelegate.showProgressWithText("Fetching Reservations...")
            var dicParameters : [String:Any]! = Dictionary()
            
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let booker_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).booker_id")
            
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            
            dicParameters["booker_id"] = booker_ID
            dicParameters["user_id"] = user_ID
            dicParameters["session"] = session
            dicParameters["page"] = "\(self.pageNumber)"
            dicParameters["company_id"] = Constants.CompanyID as Any?
            dicParameters["ptype"] = forType == .K_PAID_RESERVATIONS ? "PAID" : "UNPAID"
            
            dicParameters["start_date"] = self.flagApplySearch == true ? self.searchFilterDictionary["from"] : ""
            dicParameters["end_date"] = self.flagApplySearch == true ? self.searchFilterDictionary["to"] : ""
            dicParameters["term"] = self.flagApplySearch == true ? self.searchFilterDictionary["keyword"] : ""
            
            if self.flagApplySearch == true {
                for (key, value) in self.dict_searchItems.enumerated(){
                    print(key, value)
                    if value.keys.first == "\(self.searchFilterDictionary["for"]!)"{
                         dicParameters["search_term"] = value.values.first
                        break
                    }
                }
            }
            else{
                dicParameters["search_term"] = ""
            }
            
            dicParameters["acc_id"] = "\(appDelegate.activeUserDetailsForBooker["acc_id"]!)"
            dicParameters["is_booker"] = "yes"
            
            ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.accoutPaymentHistory, dicParameters: dicParameters, successBlock:
                { [weak self](dicRecievedJSON) -> Void in
                    appDelegate.hideProgress()
                    
                    if dicRecievedJSON != nil
                    {
                        var dict_Response:[String:Any]!
                        if "\(dicRecievedJSON["status"]!)" == "success" {
                            dict_Response = dicRecievedJSON.formatDictionaryForNullValues(dicRecievedJSON)
                            if let total_rows = dict_Response["total"] as? Int {
                                if total_rows > 0{
                                    self?.totalJobRowsForSearch = total_rows
                                    
                                    if self?.flagApplySearch == true{
                                        self?.arrSearchResultsData.removeAll()
                                    }
                                    if self?.pageNumber == 0{
                                        self?.arrPaidReservationData.removeAll()
                                        self?.arrUnpaidReservationData.removeAll()
                                       
                                    }
                                    if let records = dict_Response["record"] as? [[String:Any]]{
                                        
                                        if self?.flagApplySearch == true{
                                            self?.arrSearchResultsData = records
                                        }else{
                                            if self?.reservationType == .K_PAID_RESERVATIONS{
                                            self?.arrPaidReservationData = records
                                        }else if self?.reservationType == .K_UNPAID_RESERVATIONS{
                                            self?.arrUnpaidReservationData = records
                                        }
                                        }
                                        self?.tableAccoutPaymentHistory.reloadData()
                                    }
                                }
                                else {
                                    Globals.ToastAlertWithString("No record found")
                                    
                                }
                            }
                        }
                    }
            }){[weak self] (error) -> ()  in
                appDelegate.hideProgress()
                Globals.ToastAlertWithString("")
                self?.loading = false
            }
            
        }else {
            self.loading = false
            let alert = self.alertView("Sorry", message:"Please check your network connection", delegate:nil, cancelButton:"Ok", otherButons:nil)
            alert?.show()
        }
    }
    
    func toggleShowHideAction(_ sender:UIButton) {
        self.showSearchFilterView = !self.showSearchFilterView
        self.searchFilterDictionary = ["from":"", "to":"", "for":"", "keyword":""]
        //self.showSearchFilterView = false
        
        if self.showSearchFilterView == false{
            self.flagApplySearch = false
            self.pageNumber = 0
            if self.arrSearchResultsData.count > 0{
                self.arrSearchResultsData.removeAll()
                self.getPaymentHistory(forType: .K_PAID_RESERVATIONS)
            }
        }
        self.tableAccoutPaymentHistory.reloadData()
    }
    
    // MARK: - UITableViewDelegates
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        if section == 0{
            let headerView = self.tableAccoutPaymentHistory.dequeueReusableCell(withIdentifier: "searchHeaderCell")
            if let btnShowHide = headerView?.viewWithTag(1000) as? UIButton{
                btnShowHide.addTarget(self, action: #selector(PAPaymentHistoryViewController.toggleShowHideAction(_:)), for: .touchUpInside)
                btnShowHide.isSelected = showSearchFilterView
            }
            
            
            return headerView
        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if section == 0{
            return 50.0
        }
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if indexPath.section == 0{
            return 160.0
        }else {
            return self.getHeightForCellAtIndexPath(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if self.reservationType == .K_PAID_RESERVATIONS{
        }else if self.reservationType == .K_UNPAID_RESERVATIONS{
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0{
            return (showSearchFilterView == true) ? 1 : 0
        }else {
            if self.flagApplySearch == true{
                return self.arrSearchResultsData.count
            }else {
            if self.reservationType == .K_PAID_RESERVATIONS{
                return self.arrPaidReservationData.count
            }else if self.reservationType == .K_UNPAID_RESERVATIONS{
                return self.arrUnpaidReservationData.count
            }
            }
        }
        return 0
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.section == 0{
            if let cell = self.tableAccoutPaymentHistory.dequeueReusableCell(withIdentifier: "searchOptionsCell", for: indexPath) as? searchFiltersTableViewCell{
                cell.configureCell()
                
                cell.txtToDate.delegate = self
                cell.txtFromDate.delegate = self
                cell.txtSearchIn.delegate = self
                cell.txtSearchFor.delegate = self
                
                cell.txtFromDate.text = self.searchFilterDictionary["from"]
                cell.txtToDate.text = self.searchFilterDictionary["to"]
                cell.txtSearchIn.text = self.searchFilterDictionary["for"]
                cell.txtSearchFor.text = self.searchFilterDictionary["keyword"]
                
                cell.txtFromDate.clearButtonMode = UITextFieldViewMode.always
                cell.txtFromDate.clearsOnBeginEditing = true
                
                cell.txtToDate.clearButtonMode = UITextFieldViewMode.always
                cell.txtToDate.clearsOnBeginEditing = true
                
                cell.buttonGo.addTarget(self, action: #selector(PAPaymentHistoryViewController.btnApplyFilterClicked), for: UIControlEvents.touchUpInside)
                
                cell.txtFromDate.inputView = viewDatePicker
                cell.txtToDate.inputView = viewDatePicker
                cell.txtSearchIn.inputView = viewDatePicker
                
                return cell
            }
        }
        if indexPath.section == 1{
            
            if let cell = self.tableAccoutPaymentHistory.dequeueReusableCell(withIdentifier: "paymentHistoryCell", for: indexPath) as? PassengerListingTableViewCell{
                var dataDict = [String:Any]()
                cell.k_listing_type = .K_TYPE_PAID_UNPAID_LIST
                if self.flagApplySearch == true{
                     dataDict = self.arrSearchResultsData[indexPath.row]
                }else {
                if self.reservationType == .K_PAID_RESERVATIONS{
                    dataDict = self.arrPaidReservationData[indexPath.row]
                }else if self.reservationType == .K_UNPAID_RESERVATIONS{
                    dataDict = self.arrUnpaidReservationData[indexPath.row]
                }
                }
                let arrData:[[String:Any]] = [["Booking No": "\(dataDict["conf_id"]!)".replacingOccurrences(of: "-", with: "")], ["Pick-up Date/Time": "\(dataDict["pdate"]!)"], ["Passenger Name": "\(dataDict["pname"]!)"], ["Payment Method": "\(dataDict["payment_method"]!)"], ["Total Amount": "\(dataDict["currency_symbol"]!)" + "\(dataDict["total_payment"]!)"]]//["Account Name": "\(dataDict["mobile"]!)"]
                cell.setContentForTextView(value: arrData)
                cell.AccountListingDelegate = self
                cell.btn_option1.tag = indexPath.row
                cell.btn_option2.tag = indexPath.row
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func btnApplyFilterClicked() {
        

        if (self.searchFilterDictionary["from"]?.isEmpty)! && (self.searchFilterDictionary["to"]?.isEmpty)! && (self.searchFilterDictionary["for"]?.isEmpty)! && (self.searchFilterDictionary["keyword"]?.isEmpty)!
        {
            return
        }
        else  if self.searchFilterDictionary["for"]?.isEmpty == false && self.searchFilterDictionary["keyword"]?.isEmpty == true{
            Globals.ToastAlertWithString("Please input search term")
        }
        self.view.endEditing(true)
        self.flagApplySearch = true
        self.pageNumber = 0
        if self.arrSearchResultsData.count > 0{
            self.arrSearchResultsData.removeAll()
        }
        self.tableAccoutPaymentHistory.reloadData()

        self.getPaymentHistory(forType: self.reservationType)
    }
    // MARK: - get height for cell
    
    func getHeightForCellAtIndexPath(_ indexPath:IndexPath) -> CGFloat
    {
        
        let textView = UITextView()
        textView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width - 30, height: 100)
        textView.font = Globals.defaultAppFont(16)
        
        var dataDict = [String:Any]()
        if self.flagApplySearch == true{
            dataDict = self.arrSearchResultsData[indexPath.row]
        }else{
        if self.reservationType == .K_PAID_RESERVATIONS{
            dataDict = self.arrPaidReservationData[indexPath.row]
        }else if self.reservationType == .K_UNPAID_RESERVATIONS{
            dataDict = self.arrUnpaidReservationData[indexPath.row]
        }
        }
        
        let arrData:[[String:Any]] = [["Booking No": "\(dataDict["conf_id"]!)".replacingOccurrences(of: "-", with: "")], ["Pick-up Date/Time": "\(dataDict["pdate"]!)"], ["Passenger Name": "\(dataDict["pname"]!)"], ["Payment Method": "\(dataDict["payment_method"]!)"], ["Total Amount": "\(dataDict["currency_symbol"]!)" + "\(dataDict["total_payment"]!)"]]//["Account Name": "\(dataDict["mobile"]!)"]
        let str_combine_attibuted: NSMutableAttributedString = NSMutableAttributedString()
        for  (index, val) in arrData.enumerated() {
            
            var key_star: String = ""
            var val_str: String = ""
            var fontColor:UIColor?
            
            if index == 0 {
                key_star = "\(val.keys.first!): ".trimmingCharacters(in:NSCharacterSet.whitespaces)
                val_str = " \(val.values.first!)\n".trimmingCharacters(in:NSCharacterSet.whitespaces)
                fontColor = UIColor.appThemeColor()
            }
            else {
                key_star = "\(val.keys.first!): ".trimmingCharacters(in:NSCharacterSet.whitespaces)
                val_str = " \(val.values.first!)\n".trimmingCharacters(in:NSCharacterSet.whitespaces)
                fontColor = UIColor.darkGray
            }
            
            let title_attri = [NSFontAttributeName: Globals.defaultAppFont(16), NSForegroundColorAttributeName: UIColor.black]
            
            let str_title: NSMutableAttributedString = NSMutableAttributedString(string: key_star, attributes: title_attri)
            str_combine_attibuted.append(str_title)
            
            
            let values_Attri = [NSFontAttributeName: Globals.defaultAppFont(16), NSForegroundColorAttributeName: fontColor!] as [String : Any]
            let str_value: NSMutableAttributedString = NSMutableAttributedString(string: val_str, attributes: values_Attri)
            
            str_combine_attibuted.append(str_value)
        }
        
        textView.attributedText = str_combine_attibuted
        
        return textView.contentSize.height + 40
    }
    // MARK: - Picker Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return dict_searchItems.count
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat{
        return 40
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return dict_searchItems[row].keys.first
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
    }
    
    // MARK: - passengerListingaActionDelegate
    func btnDeleteActionPerformed(_ sender: UIButton){
    }
    func btnDashboardActionPerformed(_ sender: UIButton){
    }
    func btnViewActionPerformed(_ sender: UIButton){
        
    }
    func btnEmailActionPerformed(_ sender: UIButton){ // Share
        
        var dataDict = [String:Any]()
        if self.flagApplySearch == true{
            dataDict = self.arrSearchResultsData[sender.tag]
        }else{
        if self.reservationType == .K_PAID_RESERVATIONS{
            dataDict = self.arrPaidReservationData[sender.tag]
        }else if self.reservationType == .K_UNPAID_RESERVATIONS{
            dataDict = self.arrUnpaidReservationData[sender.tag]
        }
        }
        let detailsVc = appDelegate.storybaord?.instantiateViewController(withIdentifier: "ShareTripDetailsViewController") as! ShareTripDetailsViewController
        detailsVc.strShareType = "invoice"
        detailsVc.reservation_id = "\(dataDict["id"]!)"
        detailsVc.emailBillling = "\(dataDict["billing_email"]!)"
        detailsVc.view.backgroundColor = UIColor.clear
        detailsVc.modalPresentationStyle = .overFullScreen
        detailsVc.providesPresentationContextTransitionStyle = true
        detailsVc.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overFullScreen
        self.present(detailsVc, animated: true) { () -> Void in
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:-======= Picker view button actions ==========
    
    @IBAction func pickerCancelButtonClicked(_ sender: UIBarButtonItem) {
       self.view.endEditing(true)
     }
    @IBAction func pickerDoneButtonClicked(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        if self.selectedDateType == .K_FROM_DATE{
            searchFilterDictionary["from"] = "\(self.getDateFromSelectedPickerDate())"
        }else if self.selectedDateType == .K_TO_DATE{
            searchFilterDictionary["to"] = "\(self.getDateFromSelectedPickerDate())"
        }else if self.selectedDateType == .K_KEY_NAME{
            let index = self.searchKeysPicker.selectedRow(inComponent: 0)
            if index > 0{
                searchFilterDictionary["for"] = "\(dict_searchItems[index].keys.first!)"
            }
        }
        
        let indexPath = IndexPath(item: 0, section: 0)
        self.tableAccoutPaymentHistory.reloadRows(at: [indexPath], with: .fade)
        
    }
    func getDateFromSelectedPickerDate() -> String{
        let dateFormatter2: DateFormatter = DateFormatter()
        let locale: Locale = Locale(identifier: "en_US_POSIX")
        dateFormatter2.locale = locale
        dateFormatter2.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter2.string(from: self.datePicker.date)
    }
    // MARK:-======= Pagination of table view rows ==========
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if !self.loading {
            let endScrolling:CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height
            if endScrolling >= scrollView.contentSize.height {
                self.loading = true
                self.loadDataDelayed()
            }
        }
    }
    
    func loadDataDelayed() {
        if self.reservationType == .K_PAID_RESERVATIONS{
            if totalJobRowsForSearch > arrPaidReservationData.count {
                let page: Double = ceil(Double(arrPaidReservationData.count)/8.0)
                pageNumber = Int(page) + 1
            }
        }else if self.reservationType == .K_UNPAID_RESERVATIONS{
            if totalJobRowsForSearch > arrUnpaidReservationData.count {
                let page: Double = ceil(Double(arrUnpaidReservationData.count)/8.0)
                pageNumber = Int(page) + 1
            }
        }
        self.getPaymentHistory(forType: self.reservationType)
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if textField.tag == 100 || textField.tag == 101{
            if textField.tag == 100{
                self.selectedDateType = .K_FROM_DATE
                searchFilterDictionary["from"] = ""
            }else if textField.tag == 101{
                self.selectedDateType = .K_TO_DATE
                searchFilterDictionary["to"] = ""
            }
            self.datePicker.date = Date()
            self.datePicker.isHidden = false
            self.searchKeysPicker.isHidden = true
            self.view.bringSubview(toFront: self.viewDatePicker)
            return true
        }else if textField.tag == 102{
            self.selectedDateType = .K_KEY_NAME
            self.datePicker.isHidden = true
            self.searchKeysPicker.isHidden = false
            self.view.bringSubview(toFront: self.viewDatePicker)
            return true
            
        }else if textField.tag == 103 {
            self.selectedDateType = .K_KEYWORD
            searchFilterDictionary["keyword"] = "\(String(describing: textField.text!))"
            return true
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField){
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        if textField.tag == 103 {
            searchFilterDictionary["keyword"] = "\(String(describing: textField.text!))"
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        if textField.tag == 103 {
            searchFilterDictionary["keyword"] = "\(String(describing: textField.text!))"
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    // MARK: - Pickerview show hide
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
