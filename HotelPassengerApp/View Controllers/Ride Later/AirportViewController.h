//
//  AirportViewController.h
//  RideApplication
//
//  Created by Gulraj Grewal on 8/27/15.
//  Copyright (c) 2015 BlackBirdWorldWide. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AirportViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnBaggage;
@property (weak, nonatomic) IBOutlet UIButton *btnCurbside;
- (IBAction)btnAirPortInstructionPressed:(UIButton*)sender;

@property (weak, nonatomic) IBOutlet UITextField *txtAirPortName;
@property (weak, nonatomic) IBOutlet UITextField *txtAirPortCode;
@property (weak, nonatomic) IBOutlet UITextField *txtFlightNo;
@property (weak, nonatomic) IBOutlet UITextField *txtArrDep;
@property (weak, nonatomic) IBOutlet UITextField *txtTerminalGate;
@property (weak, nonatomic) IBOutlet UITextField *txtETA;
@property (weak, nonatomic) IBOutlet UITextField *txtAirportInstructions;

@property (weak, nonatomic) IBOutlet UITextField *txtAirlineName;
@property (weak, nonatomic) IBOutlet UITextField *txtAirlineCode;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *allViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerAirportInstructions;
@property (strong, nonatomic) IBOutlet UIView *airportInstructionPickerView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceBottomView;

@property (weak, nonatomic) IBOutlet UITableView *tblResults;
@property(nonatomic) NSString *strReservationType;
@property(nonatomic) NSString *strDateTime;
@property (strong, nonatomic)  NSDictionary *dicDetails;
@property(nonatomic) NSString *strPickupDate;
@property (nonatomic)  BOOL editReservation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *doneBtnTopVerticalSpacing;


@property(nonatomic) NSString *strNoOfPax;
@property(nonatomic) NSString *strNewDateTime;
@property(nonatomic) NSString *strPickAddress;



@property (weak, nonatomic) IBOutlet UIView *viewAirportInstructions;

@property (weak, nonatomic) IBOutlet UIButton *btnVerify;
@property (weak, nonatomic) IBOutlet UIView *contentViewScroll;
@property (assign) NSInteger tagBtnLast;
@property (nonatomic, copy) void (^finishAirportSelection)(NSDictionary *dicAirPortDetails, NSInteger tag, BOOL isDone);

@property (strong, nonatomic)  NSMutableArray *airportInstructions;

@end
