//
//  AirportViewController.m
//  RideApplication
//
//  Created by Gulraj Grewal on 8/27/15.
//  Copyright (c) 2015 BlackBirdWorldWide. All rights reserved.
//

#import "AirportViewController.h"

//#import <JLToast/JLToast.h>

#import "XMLReader.h"
#import "GTMStringEncoding.h"
#import "GTMDefines.h"
#import "GMUrlSigner.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>
#import "HotelPassengerApp-Swift.h"

typedef enum : NSUInteger {
    kSearchTypeAirport,
    kSearchTypeAirportCode,
    kSearchTypeAirline,
    kSearchTypeAirlineCode
} kSearchType;

@interface AirportViewController ()< NSXMLParserDelegate, UIGestureRecognizerDelegate, BackButtonActionProtocol>
{
    int url_Called;
    NSMutableArray *arrSearchResults;
    //   UIImage *imgCheckBox, *imgCheckedBox;
    
    
    //XML Parsing
    NSXMLParser *rssParser;
    NSMutableArray *articles;
    NSMutableDictionary *item;
    NSString *currentElement;
    NSMutableString *ElementValue;
    BOOL errorParsing;
    
    NSString *airline_id, *airPort_id;
    CGRect rect ;
     AddressDetails   *modalAdress;
    
}
@property NSTimer *autoCompleteTimer;
@property kSearchType searchType;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeight;
@property (nonatomic) NSString *strAirportInstructionType;
@property ( nonatomic) IBOutlet PABottomViewAddress *bottomViewAddress;
@property (nonatomic) NSDictionary *flightStaus;
@property (nonatomic) NSDictionary *departureDetails;
@property (nonatomic) NSDictionary *arrivalDetails;
@property (nonatomic) NSString *flight_Number, *airline_code, *airline_name;
@end

@implementation AirportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.'
    if ([self.view viewWithTag:737373] != nil ) {
        UIButton *btnDone = (UIButton*)[self.view viewWithTag:737373];
        btnDone.titleLabel.font = [Globals defaultAppFontWithBold:16];
        btnDone.layer.cornerRadius = 5.0 ;
        btnDone.layer.masksToBounds = true ;
    }
 
    float scrollHeight = 700.0f;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        scrollHeight = CGRectGetHeight(self.view.frame);
    }
    [_scrollView setContentSize:CGSizeMake(CGRectGetWidth(self.view.frame), scrollHeight)];
    
    NSLog(@"_airportInstructions %@", _airportInstructions);
    
    _tblResults.hidden = YES;
    NSString *strTypeAddress;
    bool isPickupAddress = true ;
    if ([self.strReservationType isEqualToString:@"toAirPort"]) {
        strTypeAddress = @"Dropoff Airport";
        isPickupAddress = false;
    }else {
        strTypeAddress = @"Pickup Airport";
        isPickupAddress = true;
    }
    self.bottomSpaceBottomView.constant = 65;
    self.bottomViewHeight.constant = 100;
    self.allViewHeightConstraint.constant = 50.0;
    if ([UIDevice currentDevice].userInterfaceIdiom ==  UIUserInterfaceIdiomPad) {
           self.allViewHeightConstraint.constant = 75.0;
    
    }
    
    self.bottomViewAddress.dateTime.text = [NSString stringWithFormat:@"Date & Time: %@", _strNewDateTime]; //@"" +
    self.bottomViewAddress.pickupAddress.text =  [NSString stringWithFormat:@"Pick-Up Address:  %@", _strPickAddress];
    self.bottomViewAddress.serviceType.text =  [NSString stringWithFormat:@"No of Passenger(s):  %@", _strNoOfPax];
    self.bottomViewAddress.pickupAddress.numberOfLines = 3 ;
    self.bottomViewAddress.serviceType.font = [Globals defaultAppFont:14];
    self.bottomViewAddress.pickupAddress.font = [Globals defaultAppFont:14];
    self.bottomViewAddress.dateTime.font = [Globals defaultAppFont:14];
    if (isPickupAddress) {
        self.bottomViewHeight.constant = 70;
        self.bottomSpaceBottomView.constant = 55;
        self.bottomViewAddress.pickupAddress.alpha = 0.0 ;
        self.bottomViewAddress.pickupAddress.text = @"" ;
        [self.bottomViewAddress.pickupAddress setHidden:true];
        
    }
   
    [self.view layoutIfNeeded];
    [self.view updateConstraintsIfNeeded];
    self.navigationItem.leftBarButtonItem = [[Globals sharedInstance] leftBackButtonWithCustom:strTypeAddress imageName:@"backArrow"];
    arrSearchResults = [[NSMutableArray alloc]init];
    
    _tblResults.layer.cornerRadius = 5.0f;
    _tblResults.layer.borderColor = [UIColor grayColor].CGColor;
    _tblResults.layer.borderWidth = 1.0f;
    
    _txtAirPortName.returnKeyType = UIReturnKeyDefault;
    _txtAirPortCode.returnKeyType = UIReturnKeyDefault;
    _txtAirlineName.returnKeyType = UIReturnKeyDefault;
    _txtAirlineCode.returnKeyType = UIReturnKeyDefault;
    
    _txtAirPortName.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtAirPortCode.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtAirlineName.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtAirlineCode.autocorrectionType = UITextAutocorrectionTypeNo;
    
    _txtFlightNo.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    _txtAirPortCode.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    _txtAirlineCode.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    
    _txtAirlineName.font = [Globals defaultAppFont:14];
    _txtETA.font = [Globals defaultAppFont:14];
    _txtArrDep.font = [Globals defaultAppFont:14];
    _txtFlightNo.font = [Globals defaultAppFont:14];
    _txtAirlineCode.font = [Globals defaultAppFont:14];
    _txtAirPortCode.font = [Globals defaultAppFont:14];
    _txtAirportInstructions.font = [Globals defaultAppFont:14];
    _txtAirPortName.font = [Globals defaultAppFont:14];
    _txtTerminalGate.font = [Globals defaultAppFont:14];
    
    
    //_strAirportInstructionType = @"Baggage";
    [_btnBaggage setImage:[UIImage imageNamed:@"radiouncheck"] forState:UIControlStateNormal];
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    singleTap.delegate = self;
    [self.view addGestureRecognizer:singleTap];
    
    
    _airportInstructionPickerView.frame = CGRectMake(0, CGRectGetHeight(self.view.frame) - 216, CGRectGetWidth(self.view.frame) , 260);
    _airportInstructionPickerView.hidden = YES;
    
    if (_airportInstructions.count>0){
        _txtAirportInstructions.inputView = _airportInstructionPickerView;
        _txtAirportInstructions.text = @"Select Airport Instruction(s)";
    }
    else
    {
        _txtAirportInstructions.placeholder = @"No Airport Instruction(s)";
    }
    
    for (UIView *view in self.contentViewScroll.subviews) {
        if ([view isKindOfClass:[UIView class]]) {
            UIView *viewSub = (UIView*)view;
            viewSub.layer.borderColor = [UIColor lightGrayColor].CGColor;
            viewSub.layer.borderWidth = 1.0;
        }
    }
    //self.btnVerify.layer.borderWidth = 1.0;
    //self.btnVerify.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.view updateConstraints];
    if (_dicDetails != nil && _editReservation == true  )  {
        _txtAirPortCode.text = [_dicDetails valueForKey:@"iata"];
        _txtAirPortName.text = [_dicDetails valueForKey:@"airport_name"];
        self.airline_code = [_dicDetails valueForKey:@"airline_code"];
        self.airline_name = [_dicDetails valueForKey:@"airline_name"];
        _txtAirlineCode.text = [_dicDetails valueForKey:@"airline_code"];
        _txtAirlineName.text = [_dicDetails valueForKey:@"airline_name"];

        _txtFlightNo.text = [_dicDetails valueForKey:@"flight"];
        _txtArrDep.text = [_dicDetails valueForKey:@"arr_ap"];
        _txtETA.text = [_dicDetails valueForKey:@"eta"];
        _txtTerminalGate.text = [_dicDetails valueForKey:@"terminal"];
        airline_id = [_dicDetails valueForKey:@"airline_id"];
        airPort_id = [_dicDetails valueForKey:@"airport_id"];
        //if (![[_dicDetails valueForKey:@"flight_status"] isEqualToString:@""]) {
          //  self.btnVerify.selected = true;
        //}
        if ([_dicDetails valueForKey:@"airport_inst"]) {
            _txtAirportInstructions.text = [_dicDetails valueForKey:@"airport_inst"];
        }
        
    }else if (_dicDetails != nil) {
        self.airline_code = [_dicDetails valueForKey:@"airline_code"];
        self.airline_name = [_dicDetails valueForKey:@"airline_name"];
        
        _txtAirlineCode.text = self.airline_code;
        _txtAirlineName.text = self.airline_name;
        _txtAirPortCode.text = [_dicDetails valueForKey:@"airport_code"];

        _txtAirPortCode.text = [_dicDetails valueForKey:@"iata"];
        _txtAirPortName.text = [_dicDetails valueForKey:@"airport_name"];
        _txtAirlineCode.text = [_dicDetails valueForKey:@"airline_code"];
        _txtAirlineName.text = [_dicDetails valueForKey:@"airline_name"];
        _txtFlightNo.text = [_dicDetails valueForKey:@"flight"];
        _txtArrDep.text = [_dicDetails valueForKey:@"arr_ap"];
        _txtETA.text = [_dicDetails valueForKey:@"eta"];
        _txtTerminalGate.text = [_dicDetails valueForKey:@"terminal"];
        airline_id = [_dicDetails valueForKey:@"airline_id"];
        airPort_id = [_dicDetails valueForKey:@"airport_id"];
        
       // if ([[_dicDetails valueForKey:@"flight_status"] isEqualToString:@"InAir"]) {
            //self.btnVerify.selected = true;
       // }
        if ([_dicDetails valueForKey:@"airport_inst"]) {
            _txtAirportInstructions.text = [_dicDetails valueForKey:@"airport_inst"];
        }
     }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [Globals sharedInstance].delegateBack = self;
}


- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    if (!_tblResults.hidden) {
        [_tblResults setHidden:YES];
        _scrollView.userInteractionEnabled = true;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_tblResults]) {
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)backButtonActionMethod:(UIButton *)sender{
    NSDictionary *dic = [NSDictionary new];
    self.finishAirportSelection(dic,self.tagBtnLast, false);
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)toggleFlightVerifyPressed:(UIButton*)sender
{
    if (_dicDetails != nil){
        _dicDetails = nil;
    }

    if (sender.isSelected) {
            sender.selected = false ;
    }else {
        sender.selected = true;
            
        [self getFlightStatus];
    }
    
}
-(void)showActivityIndicator{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    switch (_searchType) {
        case kSearchTypeAirport:
            [app showProgressWithText:@"Searching airports..."];
            break;
        case kSearchTypeAirportCode:
            [app showProgressWithText:@"Searching airports..."];
            break;
        case kSearchTypeAirline:
            [app showProgressWithText:@"Searching airlines..."];
            break;
        case kSearchTypeAirlineCode:
            [app showProgressWithText:@"Searching airlines..."];
            break;
        default:
            [app showProgressWithText:@"Please Wait..."];
            break;
    }
}
-(void)hideActivityIndicator{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [app hideProgress];
    
}
-(void)getFlightStatus
{
    if (_txtAirPortCode.text.length == 0) {
        [Globals ToastAlertWithString:@"Please enter valid airport code."];
    }if (_txtAirlineCode.text.length == 0) {
        [Globals ToastAlertWithString:@"Please enter valid airline code."];
    }
    else{
        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [app showProgressWithText:@"Please wait..."];
        [self retrieveFlightDataWithAirportCode:_txtAirPortCode.text airlineCode:_txtAirlineCode.text flightID:_txtFlightNo.text];
    }
}


-(void)retrieveFlightDataWithAirportCode:(NSString*)airportCode airlineCode:(NSString*)AICode  flightID:(NSString*)flightID {
    //http://xml.flightview.com/fvDemoConsOOOI/fvxml.exe?a=fvxmldemoOOOIe&b=fsd$thr3KW99afg&acid=SY244&depap=JFK&depdate=20150831
    NSString *urlString = nil;
    if ([_strReservationType isEqualToString:@"toAirPort"]) {
        urlString = [NSString stringWithFormat:@"http://xml.flightview.com/fvDemoConsOOOI/fvxml.exe?a=fvxmldemoOOOIe&b=fsd$thr3KW99afg&acid=%@%@&depap=%@&depdate=%@",AICode, flightID, airportCode, _strDateTime];
        
    }
    else
    {
        urlString = [NSString stringWithFormat:@"http://xml.flightview.com/fvDemoConsOOOI/fvxml.exe?a=fvxmldemoOOOIe&b=fsd$thr3KW99afg&acid=%@%@&arrap=%@&arrdate=%@",AICode, flightID, airportCode, _strDateTime];
    }
    
    NSLog(@"%@", urlString);
    
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    __block typeof(self) _self = self;
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler: ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        [_self hideActivityIndicator];
        if (data==nil)//prakash changes JUNE 29
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[_self.btnVerify setSelected:false];
            });
            return;
        }
        NSString *responseString  = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSError *parserError = NULL;
        NSDictionary *xmlDict = [XMLReader dictionaryForXMLString:responseString error:&parserError];
        // NSLog(@"%@", xmlDict);
        
        if (xmlDict) {
            
            NSString *strResultCode = [[[[[xmlDict valueForKey:@"FlightViewResults"] valueForKey:@"QueryProcessingStamp"] valueForKey:@"Result"] valueForKey:@"ResultCode"] valueForKey:@"text"];
            if (![strResultCode isEqualToString:@"0"]) {
                if ([[[[xmlDict valueForKey:@"FlightViewResults"] valueForKey:@"QueryProcessingStamp"] valueForKey:@"Result"] valueForKey:@"ResultMessage"]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[[[UIAlertView alloc]initWithTitle:@"" message:[[[[[xmlDict valueForKey:@"FlightViewResults"] valueForKey:@"QueryProcessingStamp"] valueForKey:@"Result"] valueForKey:@"ResultMessage"] valueForKey:@"text"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                        //[_self.btnVerify setSelected:false];

                    });
                }
                _self.airline_code = _txtAirlineCode.text;
                _self.airline_name = _txtAirlineName.text;
                _self.flight_Number = _txtFlightNo.text;
                _self.flightStaus = [NSDictionary new];
                _self.departureDetails = [NSDictionary new];
                _self.arrivalDetails = [NSDictionary new];
                [self finishSelectingAirport];

            }
            
            if ([[xmlDict valueForKey:@"FlightViewResults"] valueForKey:@"Flight"]) {
                
                _self.flightStaus = [[[xmlDict valueForKey:@"FlightViewResults"] valueForKey:@"Flight"] valueForKey:@"FlightStatus"];
                
                if ([[[xmlDict valueForKey:@"FlightViewResults"] valueForKey:@"Flight"] valueForKey:@"FlightId"])
                {
                    if ([[[xmlDict valueForKey:@"FlightViewResults"] valueForKey:@"Flight"] valueForKey:@"FlightId"]){
                        NSArray *arrflight = [[[xmlDict valueForKey:@"FlightViewResults"] valueForKey:@"Flight"] valueForKey:@"FlightId"];
                        
                        NSDictionary *airlineDetails = [arrflight objectAtIndex:1];
                        if (airlineDetails != nil){
                            _self.airline_code = [[[[airlineDetails valueForKey:@"CommercialAirline"] valueForKey:@"AirlineId"] valueForKey:@"AirlineCode"] valueForKey:@"text"];
                            _self.airline_name = [[[airlineDetails valueForKey:@"CommercialAirline"] valueForKey:@"AirlineName"] valueForKey:@"text"];
                            _self.flight_Number = [NSString stringWithFormat:@"%@", [[airlineDetails valueForKey:@"FlightNumber"] valueForKey:@"text"]];
                        }
                        else {
                            
                        }
                    }
                }
                
                _self.departureDetails = [[[xmlDict valueForKey:@"FlightViewResults"] valueForKey:@"Flight"] valueForKey:@"Departure"];
                _self.arrivalDetails = [[[xmlDict valueForKey:@"FlightViewResults"] valueForKey:@"Flight"] valueForKey:@"Arrival"];
                
                if ([_strReservationType isEqualToString:@"toAirPort"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[[UIAlertView alloc]initWithTitle:@"" message:[[[[[xmlDict valueForKey:@"FlightViewResults"] valueForKey:@"QueryProcessingStamp"] valueForKey:@"Result"] valueForKey:@"ResultMessage"] valueForKey:@"text"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                        //[_self.btnVerify setSelected:true];
                        
                    });
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        _self.txtArrDep.text = [[[[_self.arrivalDetails valueForKey:@"Airport"] valueForKey:@"AirportId"] valueForKey:@"AirportCode"] valueForKey:@"text"];
                        _self.txtTerminalGate.text = [[[_self.departureDetails valueForKey:@"Airport"] valueForKey:@"Terminal"]  valueForKey:@"text"];
                        
                        
                        id dateTimedetails = [_self.departureDetails valueForKey:@"DateTime"];
                        if([dateTimedetails isKindOfClass:[NSDictionary class]])
                        {
                            NSDictionary *dic = [_self.departureDetails valueForKey:@"DateTime"];
                            if([dic valueForKey:@"Time"])
                                _self.txtETA.text = [[dic valueForKey:@"Time"]  valueForKey:@"text"];
                        }
                        else
                        {
                            NSMutableArray *arrDepDateTime = [NSMutableArray new];
                            for (NSDictionary *dic in (NSDictionary*)[_self.departureDetails valueForKey:@"DateTime"]) {
                                [arrDepDateTime addObject:dic];
                            }
                            if (arrDepDateTime.count>0) {
                                NSDictionary *dicDateTime = [arrDepDateTime lastObject];
                                if([dicDateTime valueForKey:@"Time"])
                                    _self.txtETA.text = [[dicDateTime valueForKey:@"Time"]  valueForKey:@"text"];
                            }
                        }
                    });
                    
                }
                else
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        _self.txtArrDep.text = [[[[_self.departureDetails valueForKey:@"Airport"] valueForKey:@"AirportId"] valueForKey:@"AirportCode"] valueForKey:@"text"];
                        _self.txtTerminalGate.text = [[[_self.arrivalDetails valueForKey:@"Airport"] valueForKey:@"Terminal"]  valueForKey:@"text"];
                        
                        id dateTimedetails = [_self.arrivalDetails valueForKey:@"DateTime"];
                        
                        
                        if([dateTimedetails isKindOfClass:[NSDictionary class]])
                        {
                            NSDictionary *dic = [_self.arrivalDetails valueForKey:@"DateTime"];
                            if([dic valueForKey:@"Time"])
                                _self.txtETA.text = [[[dic valueForKey:@"Time"]  valueForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                            
                            if (![_self.txtETA.text isEqualToString:_self.strPickupDate])
                            {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [[[UIAlertView alloc]initWithTitle:@"Info" message:@"Flight Verified, but there is a mismatch between flight arrival time & reservation pickup time." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                                    
                                });
                                
                            }
                        }
                        else
                        {
                            NSMutableArray *arrArrivalDateTime = [NSMutableArray new];
                            for (NSDictionary *dic in (NSDictionary*)[_self.arrivalDetails valueForKey:@"DateTime"]) {
                                [arrArrivalDateTime addObject:dic];
                            }
                            if (arrArrivalDateTime.count>0) {
                                NSDictionary *dicDateTime = [arrArrivalDateTime lastObject];
                                if([dicDateTime valueForKey:@"Time"])
                                    _self.txtETA.text = [[[dicDateTime valueForKey:@"Time"]  valueForKey:@"text"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                
                                if (![_self.txtETA.text isEqualToString:_self.strPickupDate])
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        [[[UIAlertView alloc]initWithTitle:@"Info" message:@"Flight Verified, but there is a mismatch between flight arrival time & pickup time." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                                        
                                    });
                                    
                                }
                            }
                        }
                    });
                    
                }
                
                [self finishSelectingAirport];

            }
            
            
//            if (_btnVerify.tag == 99999) {
//                _btnVerify.selected = true ;
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [_self btnDonePress:_btnVerify];
//                });
//            }
        }
        else{
            _btnVerify.selected = NO;
            [[[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"No results found for flight no: %@",_txtFlightNo.text] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        }
        [_self hideActivityIndicator];
        
    }];
    
    
}
- (void)parseXMLFileAtURL:(NSString *)URL
{
    
    NSURL *url = [NSURL URLWithString:URL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    if (!responseData) {
        [self hideActivityIndicator];
        
        return;
    }
    NSString *responseString  = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSError *parserError = NULL;
    NSDictionary *xmlDict = [XMLReader dictionaryForXMLString:responseString error:&parserError];
    NSLog(@"%@", xmlDict);
    
    
    
}

-(void)finishSelectingAirport
{
    NSMutableDictionary *param_Dictionary  = [[NSMutableDictionary alloc] init];
    
    if (_dicDetails != nil)
    {
        NSMutableDictionary *dicDetails = [_dicDetails mutableCopy];
        [dicDetails setValue:_txtAirportInstructions.text forKey:@"airport_inst"];
        
        self.finishAirportSelection(_dicDetails,self.tagBtnLast,true);
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    //getting drop off lat long
    NSLog(@"_flightStaus%@", _flightStaus);
    NSLog(@"_departureDetails%@", _departureDetails);
    NSLog(@"_arrivalDetails%@", _arrivalDetails);
   
    //  if (_flightStaus == nil || _departureDetails == nil || _arrivalDetails == nil )
    //   {
    //       [Globals ToastAlertWithString:@"Please verify flight data"];
    //       return;
    //   }
    [param_Dictionary setValue:airline_id forKey:@"airline_id"];
    [param_Dictionary setValue:self.flight_Number forKey:@"flight"];
    [param_Dictionary setValue:airPort_id forKey:@"airport_id"];
    
    //    if ([_strReservationType isEqualToString:@"toAirPort"])
    //        [param_Dictionary setValue:@"" forKey:@"airport_inst"];
    //    else
    [param_Dictionary setValue:_strAirportInstructionType forKey:@"airport_inst"];
    
    if (_flightStaus.count > 0){
        NSArray *arrFlightStaus = [_flightStaus allKeys];
        [param_Dictionary setValue:[arrFlightStaus objectAtIndex:0]  forKey:@"flight_status"];
    }
    else {
        [param_Dictionary setValue:@""  forKey:@"flight_status"];
    }
    
   
    if ([_strReservationType isEqualToString:@"toAirPort"]) {
        
         [param_Dictionary setValue:[[[[_arrivalDetails valueForKey:@"Airport"] valueForKey:@"AirportId"] valueForKey:@"AirportCode"] valueForKey:@"text"] forKey:@"arr_ap"];
        //[[_departureDetails valueForKey:@"Airport"] valueForKey:@"AirportCode"]
        [param_Dictionary setValue:[NSString stringWithFormat:@"%@, %@, %@", [[[_departureDetails valueForKey:@"Airport"] valueForKey:@"AirportName"] valueForKey:@"text"], [[[[_departureDetails valueForKey:@"Airport"] valueForKey:@"AirportLocation"] valueForKey:@"CityName"] valueForKey:@"text"], [[[[_departureDetails valueForKey:@"Airport"] valueForKey:@"AirportLocation"] valueForKey:@"CountryId"] valueForKey:@"text"]] forKey:@"airport_name"];
        
        [param_Dictionary setValue:[[[_departureDetails valueForKey:@"Airport"] valueForKey:@"Terminal"] valueForKey:@"text"] forKey:@"terminal"];
        
        
        id dateTimedetails = [_departureDetails valueForKey:@"DateTime"];
        if([dateTimedetails isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dictFlightTime = [_departureDetails valueForKey:@"DateTime"];
            
            [param_Dictionary setValue:[[dictFlightTime valueForKey:@"Time"] valueForKey:@"text"] forKey:@"eta"];
            
            [param_Dictionary setValue:[NSString stringWithFormat:@"%@ %@",[[dictFlightTime valueForKey:@"Date"] valueForKey:@"text"], [[dictFlightTime valueForKey:@"Time"] valueForKey:@"text"]] forKey:@"orignal_datetime"];
            
            [param_Dictionary setValue:[NSString stringWithFormat:@"%@ %@",[[dictFlightTime valueForKey:@"Date"] valueForKey:@"text"], [[dictFlightTime valueForKey:@"Time"]  valueForKey:@"text"]] forKey:@"departure_datetime"];
        }
        else
        {
            NSMutableArray *arrFlightTime = [NSMutableArray new];
            for (NSDictionary *dic in [_departureDetails valueForKey:@"DateTime"]) {
                [arrFlightTime addObject:dic];
            }
            if (arrFlightTime ) {
                if (arrFlightTime.count>0)
                {
                    NSDictionary *dictFlightTime = [arrFlightTime objectAtIndex:0];
                    [param_Dictionary setValue:[[dictFlightTime valueForKey:@"Time"] valueForKey:@"text"] forKey:@"eta"];
                    [param_Dictionary setValue:[NSString stringWithFormat:@"%@ %@",[[dictFlightTime valueForKey:@"Date"] valueForKey:@"text"], [[dictFlightTime valueForKey:@"Time"] valueForKey:@"text"]] forKey:@"orignal_datetime"];
                    
                    NSDictionary *dictDepDateTime = [arrFlightTime lastObject];
                    [param_Dictionary setValue:[NSString stringWithFormat:@"%@ %@",[[dictDepDateTime valueForKey:@"Date"] valueForKey:@"text"], [[dictDepDateTime valueForKey:@"Time"]  valueForKey:@"text"]] forKey:@"departure_datetime"];
                }
            }
        }
        [param_Dictionary setValue:@"DO" forKey:@"routing_type"]; //PU- form airPort DO- to airport
        
    }
    else
    {

        [param_Dictionary setValue:[[[[_departureDetails valueForKey:@"Airport"] valueForKey:@"AirportId"] valueForKey:@"AirportCode"] valueForKey:@"text"] forKey:@"arr_ap"];
        
        [param_Dictionary setValue:[NSString stringWithFormat:@"%@, %@, %@", [[[_arrivalDetails valueForKey:@"Airport"] valueForKey:@"AirportName"] valueForKey:@"text"], [[[[_arrivalDetails valueForKey:@"Airport"] valueForKey:@"AirportLocation"] valueForKey:@"CityName"] valueForKey:@"text"], [[[[_arrivalDetails valueForKey:@"Airport"] valueForKey:@"AirportLocation"] valueForKey:@"CountryId"] valueForKey:@"text"]] forKey:@"airport_name"];
        
        [param_Dictionary setValue:[[[_arrivalDetails valueForKey:@"Airport"] valueForKey:@"Terminal"] valueForKey:@"text"] forKey:@"terminal"];
        
        id dateTimedetails = [_arrivalDetails valueForKey:@"DateTime"];
        if([dateTimedetails isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dictFlightTime = [_arrivalDetails valueForKey:@"DateTime"];
            
            [param_Dictionary setValue:[[dictFlightTime valueForKey:@"Time"] valueForKey:@"text"] forKey:@"eta"];
            
            [param_Dictionary setValue:[NSString stringWithFormat:@"%@ %@",[[dictFlightTime valueForKey:@"Date"] valueForKey:@"text"], [[dictFlightTime valueForKey:@"Time"] valueForKey:@"text"]] forKey:@"orignal_datetime"];
            
            [param_Dictionary setValue:[NSString stringWithFormat:@"%@ %@",[[dictFlightTime valueForKey:@"Date"] valueForKey:@"text"], [[dictFlightTime valueForKey:@"Time"]  valueForKey:@"text"]] forKey:@"arrival_datetime"];
        }
        else
        {
            NSMutableArray *arrFlightTime = [NSMutableArray new];
            for (NSDictionary *dic in [_arrivalDetails valueForKey:@"DateTime"]) {
                [arrFlightTime addObject:dic];
            }
            
            if (arrFlightTime ) {
                if (arrFlightTime.count>0) {
                    NSDictionary *dictFlightTime = [arrFlightTime objectAtIndex:0];
                    [param_Dictionary setValue:[[dictFlightTime valueForKey:@"Time"] valueForKey:@"text"] forKey:@"eta"];
                    
                    [param_Dictionary setValue:[NSString stringWithFormat:@"%@ %@",[[dictFlightTime valueForKey:@"Date"] valueForKey:@"text"], [[dictFlightTime valueForKey:@"Time"] valueForKey:@"text"]] forKey:@"orignal_datetime"];
                    
                    NSDictionary *dictDepDateTime = [arrFlightTime lastObject];
                    [param_Dictionary setValue:[NSString stringWithFormat:@"%@ %@",[[dictDepDateTime valueForKey:@"Date"] valueForKey:@"text"], [[dictDepDateTime valueForKey:@"Time"]  valueForKey:@"text"]] forKey:@"arrival_datetime"];
                }
            }
        }
        [param_Dictionary setValue:@"PU" forKey:@"routing_type"]; //PU- form airPort DO- to airport
        
    }
    
    double pickupLat = 0, pickupLong = 0;
    
    NSString *dropoffEsc_addr =  [[param_Dictionary valueForKey:@"airport_name"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //if dropoffEsc_addr.
    if ([dropoffEsc_addr containsString:@"null"]){
        dropoffEsc_addr = [_txtAirPortName.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    NSString *pickupReq =  [self getFullAPIAddress:dropoffEsc_addr];
    
    NSString *pickupResult = [NSString stringWithContentsOfURL:[NSURL URLWithString:pickupReq] encoding:NSUTF8StringEncoding error:NULL];
    if (pickupResult) {
        NSScanner *scanner = [NSScanner scannerWithString:pickupResult];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&pickupLat];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&pickupLong];
            }
        }
    }
    
    [param_Dictionary setValue:_txtAirPortName.text forKey:@"airport_name"];
    [param_Dictionary setValue:_txtAirPortCode.text forKey:@"airport_code"];
    [param_Dictionary setValue:_txtAirPortCode.text forKey:@"airport_code"];

    [param_Dictionary setValue:_txtAirPortCode.text forKey:@"iata"];
    
    [param_Dictionary setValue:_txtAirlineCode.text forKey:@"airline_code"];
    [param_Dictionary setValue:_txtAirlineName.text forKey:@"airline_name"];
    
    [param_Dictionary setValue:[NSString stringWithFormat:@"%f", pickupLat] forKey:@"airport_lat"];
    [param_Dictionary setValue:[NSString stringWithFormat:@"%f", pickupLong] forKey:@"airport_lng"];
    
    
    self.finishAirportSelection(param_Dictionary,self.tagBtnLast, true);
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnDonePress:(UIButton*)sender {
    
    [self getFlightStatus];
    

}

- (NSString *)getFullAPIAddress :(NSString *)addressString{
    
    NSString *key = @"VF7U0akPlqBg4KQqyCpJCxoY4Ow=";
    
    NSString *url =  [NSString stringWithFormat:@"/maps/api/geocode/json?sensor=false&address=%@&client=gme-netqualltechnologies1",addressString];
    
    // Stores the url in a NSData.
    NSData *urlData = [url dataUsingEncoding: NSASCIIStringEncoding];
    
    // URL-safe Base64 coder/decoder.
    GTMStringEncoding *encoding = [GTMStringEncoding rfc4648Base64WebsafeStringEncoding];
    
    // Decodes the URL-safe Base64 key to binary.
    NSData *binaryKey = [encoding decode:key];
    
    // Signs the URL.
    unsigned char result[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1,
           [binaryKey bytes], [binaryKey length],
           [urlData bytes], [urlData length],
           &result);
    NSData *binarySignature =
    [NSData dataWithBytes:&result length:CC_SHA1_DIGEST_LENGTH];
    
    // Encodes the signature to URL-safe Base64.
    NSString *signature = [encoding encode:binarySignature];
    NSString *final_String = [NSString stringWithFormat:@"http://maps.googleapis.com%@&signature=%@",url,signature];
    NSLog(@"The final URL is: http://maps.googleapis.com%@&signature=%@", url,
          signature);
    
    return final_String;
}

#pragma mark- Request Methods



//////************ MAKE REQUEST  ***********///////
- (void)makeRequest:(kSearchType)searchType {
    [self.autoCompleteTimer invalidate];
    
    NSString *urlString;
    NSMutableDictionary *param_Dictionary;
    switch (searchType) {
        case kSearchTypeAirport:
            urlString =  @"getairports";
            param_Dictionary = [[NSMutableDictionary alloc] init];
            [param_Dictionary setObject:_txtAirPortName.text forKey:@"airport"];
            break;
        case kSearchTypeAirportCode:
            urlString = @"getairportcode";
            param_Dictionary = [[NSMutableDictionary alloc] init];
            [param_Dictionary setObject:_txtAirPortCode.text forKey:@"airportcode"];
            break;
        case kSearchTypeAirline:
            urlString = @"getairlines";
            param_Dictionary = [[NSMutableDictionary alloc] init];
            [param_Dictionary setObject:_txtAirlineName.text forKey:@"airline"];
            break;
        case kSearchTypeAirlineCode:
            urlString = @"getairlinecode";
            param_Dictionary = [[NSMutableDictionary alloc] init];
            [param_Dictionary setObject:_txtAirlineCode.text forKey:@"airlinecode"];
            break;
            
        default:
            break;
    }
    // dicParameters["user_id"] = user_ID
    // dicParameters["session"] = session
    [self showActivityIndicator];
    [param_Dictionary setValue:[[Globals sharedInstance] getValueFromUserDefaultsForKey_Path:@"ActiveUserInfo.id"] forKey:@"user_id"];
    [param_Dictionary setValue:[[Globals sharedInstance] getValueFromUserDefaultsForKey_Path:@"ActiveUserInfo.session"] forKey:@"session"];
    __block typeof(self) weakSelf = self;
    ServiceManager *ObjtServcManager = [ServiceManager sharedInstance];
    [ObjtServcManager dataTaskWithPostRequest:urlString dicParameters:param_Dictionary successBlock:^(NSDictionary<NSString *,id> * dicReponse) {
        [weakSelf hideActivityIndicator];
        if (searchType == kSearchTypeAirport || kSearchTypeAirportCode) {
            if ([[dicReponse objectForKey:@"status"] isEqualToString:@"success"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    weakSelf.tblResults.translatesAutoresizingMaskIntoConstraints = true;
                    [weakSelf.tblResults setFrame:rect];
                    weakSelf.tblResults.hidden = NO;
                    weakSelf.scrollView.userInteractionEnabled = false;
                    
                    [weakSelf.view bringSubviewToFront:weakSelf.tblResults];
                    arrSearchResults = [[dicReponse valueForKey:@"records"] mutableCopy];
                    NSLog(@"url --%@  reponse ====%@",urlString,dicReponse );
                    [weakSelf.view endEditing:YES];
                    [weakSelf.tblResults reloadData];
                    [weakSelf hideActivityIndicator];
                });
            }else {
              //  [[JLToast makeText:[NSString stringWithFormat:@"%@",[dicReponse valueForKey:@"message"]] delay:0.0 duration:3.0] show];
            }
            
        }
    } failureBlock:^(NSError *error) {
        [weakSelf hideActivityIndicator];
       // [[JLToast makeText:@"Network Error " delay:0.0 duration:3.0] show];
    } ];
    /*
     NSError *error;
     NSString *jsonString;
     
     NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param_Dictionary
     options:NSJSONWritingPrettyPrinted //Pass 0 if you don't care about the readability of the generated string
     error:&error];
     
     if (!jsonData) {
     NSLog(@"Got an error: %@", error);
     }
     else {
     jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
     NSLog(@"response data %@", jsonString);
     }
     
     //////************* make request to server *********/////////////
    // WebServiceClass *cls_Obj = [[WebServiceClass alloc]init];
    //cls_Obj.delegate = self;
    //
    //[cls_Obj makeRequestAndGetDataFromServer:urlString ForParameters:jsonString withData:nil withTag:searchType]; */
}

/// GET RESPONSE FROM WEB SERVICE ////
////////////*********** PROTOCOL METHOD IMPLEMENTATION *********////////////////
- (void)responseStringValue:(NSDictionary *)dict_Value withData:(NSData *)responseData withTag:(NSInteger)tag
{
    
    NSLog(@" -- %@", dict_Value);
    
    if (tag == kSearchTypeAirport || kSearchTypeAirportCode) {
        NSString *status_String = [dict_Value objectForKey:@"status"];
        if ([status_String isEqualToString:@"success"]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _tblResults.hidden = NO;
                [self.view bringSubviewToFront:_tblResults];
                arrSearchResults = [dict_Value valueForKey:@"records"];
                [self.view endEditing:YES];
                [_tblResults reloadData];
            });
        }
    }
}

#pragma mark - UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AirportCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    cell.textLabel.font = [Globals defaultAppFont:14];
    cell.detailTextLabel.font = [Globals defaultAppFont:13];
    
    NSDictionary *dicRecord = nil;
    
    switch (_searchType) {
        case kSearchTypeAirport:
            dicRecord = [arrSearchResults objectAtIndex:indexPath.row];
            
            cell.textLabel.text = [dicRecord valueForKey:@"description"];
            cell.detailTextLabel.text = [dicRecord valueForKey:@"iata"];
            break;
        case kSearchTypeAirportCode:
            dicRecord = [arrSearchResults objectAtIndex:indexPath.row];
            
            cell.textLabel.text = [dicRecord valueForKey:@"description"];
            cell.detailTextLabel.text = [dicRecord valueForKey:@"iata"];
            break;
        case kSearchTypeAirline:
            dicRecord = [arrSearchResults objectAtIndex:indexPath.row];
            
            cell.textLabel.text = [dicRecord valueForKey:@"name"];
            cell.detailTextLabel.text = [dicRecord valueForKey:@"iata"];
            break;
        case kSearchTypeAirlineCode:
            dicRecord = [arrSearchResults objectAtIndex:indexPath.row];
            
            cell.textLabel.text = [dicRecord valueForKey:@"name"];
            cell.detailTextLabel.text = [dicRecord valueForKey:@"iata"];
            break;
            
        default:
            break;
    }
    
    
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSearchResults.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _scrollView.userInteractionEnabled = true;
    
    switch (_searchType) {
        case kSearchTypeAirport:
        {
            NSDictionary *dicRecord = [arrSearchResults objectAtIndex:indexPath.row];
            _txtAirPortCode.text = [dicRecord valueForKey:@"iata"];
            _txtAirPortName.text = [dicRecord valueForKey:@"description"];
            airPort_id = [dicRecord valueForKey:@"id"];
        }
            break;
        case kSearchTypeAirportCode:
        {
            NSDictionary *dicRecord = [arrSearchResults objectAtIndex:indexPath.row];
            _txtAirPortCode.text = [dicRecord valueForKey:@"iata"];
            _txtAirPortName.text = [dicRecord valueForKey:@"description"];
            airPort_id = [dicRecord valueForKey:@"id"];
            
            if (_txtFlightNo.text.length > 0){
                _txtArrDep.text = @"";
                _txtETA.text = @"";
                _txtTerminalGate.text = @"";
            }
            
        }
            break;
        case kSearchTypeAirline:
        {
            NSDictionary *dicRecord = [arrSearchResults objectAtIndex:indexPath.row];
            _txtAirlineName.text = [dicRecord valueForKey:@"name"];
            _txtAirlineCode.text =[dicRecord valueForKey:@"iata"];
            airline_id = [dicRecord valueForKey:@"id"];
        }
            break;
        case kSearchTypeAirlineCode:
        {
            NSDictionary *dicRecord = [arrSearchResults objectAtIndex:indexPath.row];
            _txtAirlineName.text = [dicRecord valueForKey:@"name"];
            _txtAirlineCode.text =[dicRecord valueForKey:@"iata"];
            airline_id = [dicRecord valueForKey:@"id"];
        }
            break;
            
        default:
            break;
            
    }
    [_tblResults setHidden:YES];
    [arrSearchResults removeAllObjects];
    
}

#pragma mark - UITextFieldDelegate
- (BOOL) textFieldShouldClear:(UITextField *)textField{
    
    textField.text = @"";
    // [self.localSearchQueries removeAllObjects];
    [_tblResults reloadData];
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == _txtETA || textField == _txtTerminalGate || textField == _txtArrDep){
       // [[JLToast makeText:@"Please tap on verify to get the required information" delay:0.0 duration:3.0] show];
        return NO;
    }
    else if (textField == _txtAirportInstructions){
        if (_airportInstructions.count>0){
            [self.pickerAirportInstructions reloadAllComponents];
            self.airportInstructionPickerView.frame = CGRectMake(0, CGRectGetHeight(self.view.frame) - 216, CGRectGetWidth(self.view.frame) , 260);
            self.airportInstructionPickerView.hidden = NO;
            [self.view bringSubviewToFront:self.airportInstructionPickerView];
        }
        else {
           // [[JLToast makeText:@"Airport instruction(s) not found." delay:0.0 duration:3.0] show];
            return NO;
        }
    }
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _txtAirPortCode || textField == _txtAirPortName || textField == _txtAirlineCode || textField == _txtAirlineName){
        NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (currentString.length != 0) {
            [self runScript];
        }
        else {
            //NSLog(@"The searcTextField is empty.");
        }
    }
    return YES;
}

- (void)runScript{
    
    [self.autoCompleteTimer invalidate];
    self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.65f
                                                              target:self
                                                            selector:@selector(searchAutocompleteLocationsWithSubstring:)
                                                            userInfo:nil
                                                             repeats:NO];
}

- (void)searchAutocompleteLocationsWithSubstring:(NSString *)substring
{
    [self.tblResults reloadData];
    [self makeRequest:_searchType];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    rect = [_scrollView frameForAlignmentRect:textField.superview.frame];
    rect.size.height = 100;
    rect.origin.y += textField.frame.size.height + 20;
    if (textField == _txtAirPortName)
        _searchType = kSearchTypeAirport;
    else if (textField == _txtAirPortCode)
        _searchType = kSearchTypeAirportCode;
    else if (textField == _txtAirlineCode)
        _searchType = kSearchTypeAirlineCode;
    else if (textField == _txtAirlineName)
        _searchType = kSearchTypeAirline;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Picker view delegates and dataSource

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return _airportInstructions.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSDictionary *dic = [_airportInstructions objectAtIndex:row];
    return [dic valueForKey:@"instruction"];
}


- (IBAction)pickerSelectionDone:(id)sender {
    [self.view endEditing:YES];
    NSDictionary *dic = [_airportInstructions objectAtIndex:[_pickerAirportInstructions selectedRowInComponent:0]];
    _txtAirportInstructions.text = [dic valueForKey:@"instruction"];
    _strAirportInstructionType = [NSString stringWithFormat:@"%@",[dic valueForKey:@"instruction"]];
}

- (IBAction)pickerSelectionCancel:(id)sender {
    [self.view endEditing:YES];
    _airportInstructionPickerView.hidden = YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
