//
//  PAReservationsRideLaterVC.swift
//  PassangerApp
//
//  Created by Netquall on 2/1/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import GoogleMaps
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}



enum btnTagAddress : Int {
    
    case btnAddress = 40
    case btnAirport = 41
    case btnFBO = 42
    case btnSeaport = 43
}
enum pickupTypeBtn : String {
    case Address
    case Airport
    case FBO
    case Seaport
    case POI
    case None
}
enum DropOffTypeBtn : String {
    case Address
    case Airport
    case FBO
    case Seaport
    case POI
    case None
}
class PAReservationsHourlyVC: BaseViewController, UITextViewDelegate, UITextFieldDelegate, BackButtonActionProtocol
{
   fileprivate enum IndexSection : Int {
        
        case pickUpDate = 0
        case passangerName = 1
        case passangerNumber = 2
        case pickType = 3
        case dropType = 4
        case stops = 5
       case serviceType = 6
        case vehicle = 7
        case payment = 8
        case adultLagguge = 9
        case childSeatType = 10
        case childSeatCount = 11
    
         case hourly = 999
        case none = 17777
    }
    // MARK:-============ Property decalaration ==========
    
    @IBOutlet weak var tableViewReservations: UITableView!
    
    // @IBOutlet weak var tblCarInfo: UITableView!
    var arrDataSourceHourly  : [[String:Any]]! = [[Constants.PickUpDate:"Date" as Any],[Constants.PassangerName:"Passenger Name" as Any],[Constants.PassangerNumber:"Passenger Contact Number" as Any],[Constants.PickType:"PICKUP TYPE" as Any],[Constants.DropType:"DROP OFF TYPE" as Any],[Constants.Stops:"Stops" as Any],[Constants.Hourly:"Duration" as Any],[Constants.Service:"Service Type"],[Constants.Vehicle:"Vehicle"],[Constants.Payment:"Payment"],[Constants.AdultLagguge:"01"],[Constants.ChildSeatType:"Child Seat Type"],[Constants.ChildCount:"Child Seat Count"]]
    lazy var dicOnlyAirport = [String:Any]()
    lazy var dicCardDetail = [String:Any]()
    lazy var arrDataSourceCarInfo = [Any]()
    lazy var arrDataServiceType = [Any]()
    var arrKeys : [String]!
    lazy  var arrStops = [[String:Any]]()
    
    var arrChildSeatType = ["Booster","Toddler","Infant"]
    var arrChildSeatTypeForAPI = ["Booster Seat","Forward facing (Toddler)","Rear facing (Infant)"]
    var arrChildCount = ["01","02","03","04","05","06","07","08","09","10"]
    var arrAdults = [String]()
    var arrLuggage = [String]()
    var arrHoursCount = ["02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"]
    lazy  var dicDisAndTime = [String:Any]()
    var strAirport : String!
    lazy var dicWebServiceParam = [String:Any]()
    var activeTextField: UITextField!
    var activeTextView: UITextView!
    var activeButton : UIButton!
    var currentSection = IndexSection.none.rawValue
    var dateSelected : Date!
    var strDateTime : String!
    var dicCheckCallDistance = [String:Bool]()
    var dictPickAddess = [String:Any]()
    var dictDropOffAddess = [String:Any]()
    var arrAddedChildSeats = [[String:Any]]()
    var btnPickupTypeTag = 0
    var btnPickupTypeLastOne = 0
    var btnDroppOffTypeTag = 0
    var btnDroppOffTypeLastOne = 0
    var isDisable = false
    var isTxtAdultPicker = false
    var strAdult = "0"
    var strLagguge = "0"
    var strEstimateFare = ""
    var strDisability = "0"
    var strVehicle = ""
    var strServiceType = ""
    var strPickType = pickupType.None.rawValue
    var strDropOffType = DropOffType.None.rawValue
    var tagTextView : Int!
    var strPayment = "Payment"
    var pickUpLocation : CLLocation?
    var dropOffLocation : CLLocation?
    var minimumJobHours:String! = ""
    var adultLuaggeTag = Int()
    @IBOutlet var viewDatePicker: UIView!
    
    @IBOutlet var viewAdultLuggageTbl: UIView!
    @IBOutlet weak var pickerViewCarName: UIPickerView!
    @IBOutlet var viewPickerView: UIView!
    @IBOutlet var accessoryViewTextView: UIView!
    
    @IBOutlet weak var txtLagguge: UITextField!
    @IBOutlet weak var txtAdult: UITextField!
    
    @IBOutlet var datePicker: UIDatePicker!
    var isEditingMode:Bool = false
    var reservationDetails:[String:Any]!
     var strReservationId = "0"
    
    var arrAirportInstuctions:[[String:String]] = [[String:String]]()

    // MARK: -======== View LIfe cycle   ========
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Hourly Reservation", imageName: "backArrow")
        datePicker.minimumDate = Date()
        if self.isEditingMode == true{
            self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Edit Reservation", imageName: "backArrow")
            
            if let id = self.reservationDetails["id"] as? String{
                self.strReservationId = id
            }
            var passengerName = ""
            if let name = self.reservationDetails["passanger_name"] as? String{
                passengerName = passengerName + name
            }
            if !passengerName.isEmpty
            {
                self.arrDataSourceHourly[IndexSection.passangerName.rawValue][Constants.PassangerName] = passengerName as Any?
            }
            if let phone = self.reservationDetails["pass_phone"] {
                self.arrDataSourceHourly[IndexSection.passangerNumber.rawValue][Constants.PassangerNumber] = String(describing: phone)
            }
            
            if let lagguge = self.reservationDetails["luggage"]{
                self.txtLagguge.text = String(describing: lagguge)
            }
            if let noPax = self.reservationDetails["no_of_pax"] {
                self.txtAdult.text = String(describing: noPax)
            }
            if let phone = self.reservationDetails["pass_phone"] as? String{
                self.arrDataSourceHourly[IndexSection.passangerNumber.rawValue][Constants.PassangerNumber] = phone as Any?
            }
            var dateTime = ""
            if let date = self.reservationDetails["pu_date"] as? String{
                dateTime = dateTime + date
            }
            if let time = self.reservationDetails["pu_time"] as? String{
                dateTime += " "
                dateTime = dateTime + time
            }
            if !dateTime.isEmpty
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = dateFormatter.date(from: dateTime)
                self.dateSelected = date!
                dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm a"
                self.arrDataSourceHourly[IndexSection.pickUpDate.rawValue][Constants.PickUpDate] = dateFormatter.string(from: self.dateSelected) as Any?
                self.strDateTime  = Globals.sharedInstance.dateFromTimestamp(self.dateSelected.timeIntervalSinceNow)
            }
          //  var pickuptype = ""
            if let ptype = self.reservationDetails["pickup_type"] as? String{
                if ptype.compare( "address") ||  ptype.compare( "poi"){
                    self.strPickType = ptype.compare( "address") == true ? pickupType.Address.rawValue : pickupType.POI.rawValue
                    self.btnPickupTypeTag = btnTag.btnAddress.rawValue
                    self.dicWebServiceParam["pickup_type"] = "address" as Any?
                    if self.dictPickAddess.count != 0  {
                        self.dicWebServiceParam["pickup"] = self.dictPickAddess["address"]
                        self.dicWebServiceParam["pickup_latitude"] = self.dictPickAddess["latitude"]
                        self.dicWebServiceParam["pickup_longitude"] = self.dictPickAddess["longitude"]
                        self.dicWebServiceParam["zip_from"] = self.dictPickAddess["postal_code"]
                        self.dicWebServiceParam["pickup_info"] = self.dictPickAddess as Any?
                    }
                    
                }
                else if ptype.compare("fbo"){
                    self.strPickType = pickupType.FBO.rawValue
                    self.btnPickupTypeTag = btnTag.btnFBO.rawValue
                }
                else if ptype.compare("airport"){
                    self.strPickType = pickupType.Airport.rawValue
                    self.btnPickupTypeTag = btnTag.btnAirport.rawValue
                }
                else if ptype.compare("seaport") {
                    self.strPickType = pickupType.Seaport.rawValue
                    self.btnPickupTypeTag = btnTag.btnSeaport.rawValue
                }
            }
//            if !pickuptype.isEmpty
//            {
//                self.arrDataSourceHourly[IndexSection.PickType.rawValue][Constants.PickUpDate] = pickuptype
//            }
            
            if let dtype = self.reservationDetails["dropoff_type"] as? String{
                if dtype.compare("address") || dtype.compare( "poi"){
                    self.strDropOffType = dtype.compare("address") == true ? DropOffType.Address.rawValue : DropOffType.POI.rawValue
                    self.btnDroppOffTypeTag = btnTag.btnAddress.rawValue
                    self.dicWebServiceParam["drop_type"] = "address" as Any?
                    if self.dictDropOffAddess.count != 0 {
                        self.dicWebServiceParam["dropoff"] = self.dictDropOffAddess["address"]
                        self.dicWebServiceParam["dropoff_latitude"] = self.dictDropOffAddess["latitude"]
                        self.dicWebServiceParam["dropoff_longitude"] = self.dictDropOffAddess["longitude"]
                        self.dicWebServiceParam["zip_to"] = self.dictDropOffAddess["zip"]
                        self.dicWebServiceParam["dropoff_info"] = self.dictDropOffAddess as Any?
                    }
                }
                    else if dtype.compare("fbo"){
                        self.strDropOffType = DropOffType.FBO.rawValue
                        self.btnDroppOffTypeTag = btnTag.btnFBO.rawValue
                    }
                    else if dtype.compare("airport"){
                        self.strDropOffType = DropOffType.Airport.rawValue
                        self.btnDroppOffTypeTag = btnTag.btnAirport.rawValue
                    }
                    else if dtype.compare("seaport"){
                        self.strDropOffType = DropOffType.Seaport.rawValue
                        self.btnDroppOffTypeTag = btnTag.btnSeaport.rawValue
                    }
                
            }
            if let pickup_info = self.reservationDetails["pickup_info"] as? [String:Any]{
                if self.strPickType == pickupType.Address.rawValue ||  self.strPickType == pickupType.POI.rawValue{
                    self.dictPickAddess = pickup_info
                }
                else if self.strPickType == pickupType.Airport.rawValue {
                    self.dicOnlyAirport["pickup"] = pickup_info as Any?
                }
                else if self.strPickType == pickupType.Seaport.rawValue {
                    self.dictPickAddess["seaport_address"] = pickup_info as Any?
                    self.dictPickAddess["seaport_details"] = pickup_info as Any?
                }
                else if self.strPickType == pickupType.FBO.rawValue {
                    self.dictPickAddess["fbo_details"] = pickup_info as Any?
                    self.dictPickAddess["fbo_address"] = pickup_info as Any?
                }
                self.arrDataSourceHourly[IndexSection.pickType.rawValue][Constants.PickType] = pickup_info as Any?
            }
            if let dropoff_info = self.reservationDetails["dropoff_info"] as? [String:Any]{
                if self.strDropOffType == DropOffType.Address.rawValue  ||  self.strDropOffType ==  DropOffType.POI.rawValue{
                    self.dictDropOffAddess = dropoff_info
                }
                else if self.strDropOffType == DropOffType.Airport.rawValue {
                    self.dicOnlyAirport["dropoff"] = dropoff_info as Any?
                }
                else if self.strDropOffType == DropOffType.Seaport.rawValue {
                    self.dictDropOffAddess["seaport_address"] = dropoff_info as Any?
                    self.dictDropOffAddess["seaport_details"] = dropoff_info as Any?
                }
                else if self.strDropOffType == DropOffType.FBO.rawValue {
                    self.dictDropOffAddess["fbo_details"] = dropoff_info as Any?
                    self.dictDropOffAddess["fbo_address"] = dropoff_info as Any?
                }
                self.arrDataSourceHourly[IndexSection.dropType.rawValue][Constants.DropType] = dropoff_info as Any?
                
            }
                self.dicWebServiceParam["pickup"] = self.reservationDetails["address"]
            self.dicWebServiceParam["pickup_latitude"] = self.reservationDetails["latitude"]
            self.dicWebServiceParam["pickup_longitude"] = self.reservationDetails["longitude"]
            self.dicWebServiceParam["dropoff"] = self.reservationDetails["address"]
            self.dicWebServiceParam["dropoff_latitude"] = self.reservationDetails["latitude"]
            self.dicWebServiceParam["dropoff_longitude"] = self.reservationDetails["longitude"]
            if let stopArr = self.reservationDetails["stops"] as? [[String:Any]]{
                if stopArr.count > 0{
                    self.arrDataSourceHourly[IndexSection.stops.rawValue][Constants.Stops] = stopArr as Any?
                    self.arrStops = stopArr
                     self.tableViewReservations.setEditing(true, animated: true)
                }
            }
            
            
            let dNote = self.reservationDetails["dropoff_notes"] as? String
            if dNote != nil  && dNote?.characters.count > 0{
                self.dictDropOffAddess[Constants.DropNotes] = dNote! as Any?
            }else {
                self.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
            }
            let pNote = self.reservationDetails["pickup_notes"] as? String
            if pNote != nil  && pNote?.characters.count > 0 {
                self.dictPickAddess[Constants.PickNotes] = pNote! as Any?
            } else {
                self.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
            }

            
            var vehicleType = "Vehicle"
            if let vType = self.reservationDetails["vehicle_type_title"] as? String{
                vehicleType = vType
            }
            if !vehicleType.isEmpty
            {
                self.arrDataSourceHourly[IndexSection.vehicle.rawValue][Constants.Vehicle] = vehicleType as Any?
                self.strVehicle = vehicleType
            }
            var serviceType = Constants.Service
            if let sType = self.reservationDetails["service_type"] as? String{
                serviceType = sType
                
            }
            if !serviceType.isEmpty
            {
                self.arrDataSourceHourly[IndexSection.serviceType.rawValue][Constants.Service] = serviceType as Any?
                self.strServiceType = serviceType
            }
//            var childSeatCount = 0
//            if let childCount = self.reservationDetails["child_seat_count"] as? Int{
//                childSeatCount = childCount
//            }
//            if childSeatCount != 0
//            {
//                self.arrDataSourceHourly[IndexSection.ChildSeatCount.rawValue][Constants.ChildCount] = String(childSeatCount)
//            }
            
            if let childSeatArray = self.reservationDetails["child_seat_data"] as? [[String:Any]]{
                self.arrAddedChildSeats = childSeatArray
            }
            
            if self.arrAddedChildSeats.count != 0 {
                self.dicWebServiceParam["child_seat_count"] = self.arrAddedChildSeats as Any?
            }else {
                self.dicWebServiceParam["child_seat_count"] = "0" as Any?
            }
            if let vType = self.reservationDetails["luggage"] as? String
            {
                 self.arrDataSourceHourly[IndexSection.adultLagguge.rawValue][Constants.AdultLagguge] = String(vType) as Any?
            }
            
           
            // Paymment
            if let PayId = self.reservationDetails["payment_id"]{
                self.dicCardDetail["id"] = String(describing: PayId)
            } else {
                self.dicCardDetail["id"] =  "" as Any?
            }
            if let methodPayId = self.reservationDetails["payment_method_id"]{
                self.dicCardDetail["payment_method"] = String(describing: methodPayId)
            } else {
                self.dicCardDetail["payment_method"] =  "" as Any?
            }
            if let name = self.reservationDetails["payment_method"]{
                self.dicCardDetail["name"] = String(describing: name)
                self.arrDataSourceHourly[IndexSection.payment.rawValue][Constants.Payment] = name
            } else {
                self.dicCardDetail["name"] =  "Payment" as Any?
                self.arrDataSourceHourly[IndexSection.payment.rawValue][Constants.Payment] = "Payment" as Any?
            }
            self.strPayment =  self.dicCardDetail["name"] as! String
            let strHandicap = self.reservationDetails["is_handicap"]
            if strHandicap != nil   {
                self.strDisability = String(describing: strHandicap!)
                
            }else {
                self.strDisability = "0"
            }
            self.isDisable = self.strDisability.characters.count>0 ? self.strDisability.toBool()! : false
            if let btnView = self.viewAdultLuggageTbl.viewWithTag(55) {
              (btnView as! UIButton).isSelected = self.isDisable
            }
            
            //hourly_time
             if let hourly_time = self.reservationDetails["hourly_time"]{
            }
        }else {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm a"
        self.arrDataSourceHourly[0][Constants.PickUpDate] = dateFormatter.string(from: Date()) as Any?
        self.strDateTime  = Globals.sharedInstance.dateFromTimestamp(Date().timeIntervalSinceNow)
        var name = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).first_name") as! String
        let lastName =  Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).last_name") as! String
        if name == "" && lastName == "" {
            name = "Full Name"
        }
        self.arrDataSourceHourly[1][Constants.PassangerName] = "\(name.capitalized) \(lastName.capitalized)"
        let phoneNumber =  Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).mobile") as! String
        self.arrDataSourceHourly[2][Constants.PassangerNumber] = phoneNumber
        self.dicCheckCallDistance["Pick"] = false
        self.dicCheckCallDistance["Dropp"] = false //"Dropp":false]
        }
        self.viewDatePicker.frame = CGRect(x: 0, y: self.view.frame.height - 216, width: self.view.frame.width , height: 216)
        //  self.viewPickerView.frame = CGRectMake(0, CGRectGetHeight(self.view.frame) - 216, CGRectGetWidth(self.view.frame) , 0)
        self.viewPickerView.isHidden = true
        self.tableViewReservations.contentInset = UIEdgeInsetsMake(5, 0, 0, 0)
        
        //cellTextField.txtFieldCell.inputView = self.viewPickerView
        self.viewPickerView.frame = CGRect(x: 0, y: self.view.frame.height - 216, width: self.view.frame.width , height: 260)
        
        self.viewPickerView.isHidden = true
        self.getingVehlciTypesAvailable()
        self.txtAdult.inputView = self.viewPickerView
        self.txtLagguge.inputView = self.viewPickerView
        //self.view.addSubview(self.viewPickerView)
        // self.view.bringSubviewToFront(self.viewPickerView)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
        self.registerForKeybaordNotification()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        Globals.sharedInstance.delegateBack = nil
        self.unRegisterForKeboardNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    class func baseUrl()-> String{
        return Constants.baseUrl
    }
    func reloadTableViewOnMainQueue(){
        DispatchQueue.main.async { [weak self]() -> Void in
            self?.tableViewReservations.reloadData()
        }
    }
    func reloadTableViewSectionOnMainQueue(_ section:Int){
        DispatchQueue.main.async { [weak self]() -> Void in
            self?.tableViewReservations.reloadSections(IndexSet(integer: section), with: UITableViewRowAnimation.automatic)
        }
    }
    // MARK:- ============ get vehicle available ==========
    func getingVehlciTypesAvailable()
    {
        
        if let _ = self.activeTextField {
            self.activeTextField.resignFirstResponder()
        }
        self.view.endEditing(true)
        if self.arrDataSourceCarInfo.count == 0 {
            if appDelegate.networkAvialable == true {
                appDelegate.showProgressWithText("Getting available vehicle...")
                var param_Dictionary = [String : Any]()
                let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
                let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
                let  urlString = "get_vechile_type"
                param_Dictionary["user_id"] = user_ID
                param_Dictionary["session"] = session
                param_Dictionary["company_id"] = Constants.CompanyID as Any?
                
                ServiceManager.sharedInstance.dataTaskWithPostRequest(urlString, dicParameters: param_Dictionary, successBlock: { [weak self] (dicData) -> Void in
                    appDelegate.hideProgress()
                    if dicData["status"] as! String == "success" {
                        
                        
                        if self?.isEditingMode == true{
                            if let minimumJobHours = dicData["first_range"]{
                                if let strHours = self?.arrDataSourceHourly[IndexSection.hourly.rawValue][Constants.Hourly] as? String{
                                    if String(strHours) == "0" {
                                    self?.arrDataSourceHourly[IndexSection.hourly.rawValue][Constants.Hourly] = String("Minimum (\(minimumJobHours) hour(s) job")
                                    }
                                }
                                self?.minimumJobHours = String(describing: minimumJobHours)
                            }
                        }
                        else {
                        if let minimumJobHours = dicData["first_range"]{
                            if String(describing: minimumJobHours) == "0"
                            {
                                self?.arrDataSourceHourly[IndexSection.hourly.rawValue][Constants.Hourly] = "Duration"
                            }else {
                                self?.arrDataSourceHourly[IndexSection.hourly.rawValue][Constants.Hourly] = String("Minimum (\(minimumJobHours) hour(s) job")
                            }
                            self?.minimumJobHours = String(describing: minimumJobHours)
                           }
                        }

                        if let arrSevice = dicData["service_type"] as? [[String:Any]]{
                            self?.arrDataServiceType = arrSevice
                        }
                        if let arrSevice = dicData["airport_instructions"] as? [[String:String]]{
                            self?.arrAirportInstuctions = arrSevice
                        }
                        if let arrVeicleInfo = dicData["records"] as? [[String:Any]]{
                            self?.arrDataSourceCarInfo = arrVeicleInfo
                            self?.arrDataSourceHourly[IndexSection.vehicle.rawValue][Constants.Vehicle] = self?.arrDataSourceCarInfo[0] as! [String:Any]
                            let dicCars = self?.arrDataSourceCarInfo[0] as! [String:Any]
                            self?.strVehicle =  (dicCars["vehicle_type_title"] as? String)!
                            if let adut_cap = dicCars["vehicle_specific_passenger_capacity"]{
                                self?.txtAdult.text = String(describing: adut_cap).characters.count > 0 ?  String(describing: adut_cap) : "01"
                                self!.arrAdults.removeAll(keepingCapacity: true)
                                let loop = Int(self!.txtAdult.text!)!
                                for  i  in  1...loop {
                                    self?.arrAdults.append(String(i))
                                }
//                                for var i = 1 ; i <= Int(self!.txtAdult.text!)!; i += 1 {
//                                    self?.arrAdults.append(String(i))
//                                }
                            }
                            if let adut_cap = dicCars["vehicle_specific_luggage_capacity"]{
                                self?.txtLagguge.text =  String(describing: adut_cap).characters.count > 0 ?  String(describing: adut_cap) : "01"
                                self?.arrLuggage.removeAll(keepingCapacity: true)
                                let loop = Int(self!.txtLagguge.text!)!
                                for  i  in  1...loop {
                                    self?.arrLuggage.append(String(i))
                                }
//                                for var i = 1 ; i <= Int(self!.txtLagguge.text!); i += 1 {
//                                    self?.arrLuggage.append(String(i))
//                                }
                            }
                        }
                        // self?.setupPickerViewAgain()
                        if self?.isEditingMode == true {
                              if self?.arrDataServiceType != nil &&  self?.arrDataServiceType.count != 0 {
                            for dicVehicle in self!.arrDataSourceCarInfo as! [[String:Any]] {
                                if String(describing: dicVehicle["id"]!).compare(String(describing: self!.reservationDetails["vehicle_type"]!)) {
                                    self?.arrDataSourceHourly[IndexSection.vehicle.rawValue][Constants.Vehicle] = dicVehicle
                                    self!.strVehicle = String(describing: self!.reservationDetails["vehicle_type_title"]!)
                                    break
                                }
                            }
                           }
                            if self?.arrDataServiceType != nil &&  self?.arrDataServiceType.count != 0 {
                                for dicVehicle in self!.arrDataServiceType as! [[String:Any]] {
                                    if String(describing: dicVehicle["id"]!).compare(String(describing: self!.reservationDetails["service_type_id"]!)) {
                                        self?.arrDataSourceHourly[IndexSection.serviceType.rawValue][Constants.Service] = dicVehicle
                                        self!.strServiceType = String(describing: self!.reservationDetails["service_type"]!)
                                        break
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.async(execute: { 
                            self?.tableViewReservations.reloadData()
                        })
                }else {
            if self?.activeTextField != nil  {
                self?.activeTextField.resignFirstResponder()
                }
                self?.viewPickerView.isHidden = true
                appDelegate.hideProgress()
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
            }, failureBlock: { [weak self](error) -> () in
                if self?.activeTextField != nil  {
                    self?.activeTextField.resignFirstResponder()
                }
                self?.viewPickerView.isHidden = true
                appDelegate.hideProgress()
                Globals.ToastAlertWithString("Network Error!..")
            })
            }else {
                Globals.ToastAlertForNetworkConnection()
            }
        }else {
            //self.setupPickerViewAgain()
        }
    }
    // MARK: -======== text Field Delegate  ========
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        if self.activeTextField  != nil {
            self.activeTextField.resignFirstResponder()
        }
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        self.isTxtAdultPicker = false
        self.activeTextField = textField
        self.currentSection = textField.tag
        self.viewDatePicker.isHidden = false
        self.pickerViewCarName.selectRow(0, inComponent: 0, animated: true)
        if textField.tag == 55 {
            self.currentSection = IndexSection.childSeatCount.rawValue
            self.pickerViewCarName.reloadAllComponents()
            self.viewPickerView.isHidden = false
        }else  if textField.tag == IndexSection.childSeatType.rawValue {
            self.currentSection = IndexSection.childSeatType.rawValue
            self.pickerViewCarName.reloadAllComponents()
            self.viewPickerView.isHidden = false
        }else if textField.tag == 80 || textField.tag == 81  {
            self.adultLuaggeTag = textField.tag
            self.isTxtAdultPicker = true
            if  self.adultLuaggeTag == 80 {
                if self.arrAdults.count == 0 {
                    Globals.sharedInstance.ToastAlertInstance("Please select preferred vehicle to get adult count.")
                    return false
                }
            }else if self.adultLuaggeTag == 81 {
                if self.arrLuggage.count == 0 {
                    Globals.sharedInstance.ToastAlertInstance("Please select preferred vehicle to get luggage count.")
                    return  false
                }
            }
            self.currentSection = IndexSection.adultLagguge.rawValue
            self.pickerViewCarName.reloadAllComponents()
            self.viewPickerView.isHidden = false
        }else  if textField.tag == IndexSection.vehicle.rawValue {
            self.currentSection = IndexSection.vehicle.rawValue
            if self.arrDataSourceCarInfo.count != 0 {
                self.pickerViewCarName.reloadAllComponents()
                self.viewPickerView.isHidden = false
            }else {
                textField.resignFirstResponder()
                self.getingVehlciTypesAvailable()
                return false
            }
        }else  if textField.tag == IndexSection.serviceType.rawValue {
            self.currentSection = IndexSection.serviceType.rawValue
            if self.arrDataServiceType.count != 0 {
                self.pickerViewCarName.reloadAllComponents()
                self.viewPickerView.isHidden = false
            }else {
                textField.resignFirstResponder()
                self.getingVehlciTypesAvailable()
                return false
            }
        }else  if textField.tag == IndexSection.hourly.rawValue {
            textField.text = ""
            self.currentSection = IndexSection.hourly.rawValue
        }
       return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        let arrDicKeys = (self.arrDataSourceHourly[ self.currentSection]).keys
        let strKey = arrDicKeys.first!
        if  self.currentSection == IndexSection.adultLagguge.rawValue
        {
            self.arrDataSourceHourly[self.currentSection][strKey] = textField.text as Any?
        }else if self.currentSection == IndexSection.passangerName.rawValue {
            self.arrDataSourceHourly[self.currentSection][strKey] =  textField.text as Any?
        }else if   self.currentSection == IndexSection.passangerNumber.rawValue {
            self.arrDataSourceHourly[self.currentSection][strKey] =  textField.text as Any?
        }else  if textField.tag == IndexSection.hourly.rawValue {
            self.arrDataSourceHourly[IndexSection.hourly.rawValue][Constants.Hourly] = self.arrHoursCount[pickerViewCarName.selectedRow(inComponent: 0)] as Any?
            // self.tableViewReservations.reloadData()
            self.activeTextField.text = self.arrHoursCount[pickerViewCarName.selectedRow(inComponent: 0)]
            self.view.endEditing(true)
        }
  }
    @IBAction func datePickerValueChangedAction(_ sender: UIDatePicker) {
        self.dateSelected = sender.date
    }
    
    @IBAction func btnDonePickUpDateAction(_ sender: UIBarButtonItem) {
        
        if self.dateSelected == nil {
            self.dateSelected = Date()
        }
        
        let strHours = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).prohibitReservation_before")
        if strHours != nil && String(describing: strHours).compare("0") == false {
            let date = Date()
            let dayHourMinuteSecond: NSCalendar.Unit = [.day, .hour, .minute, .second]
            let components:DateComponents = (Calendar.current as NSCalendar).components(dayHourMinuteSecond, from: date, to: self.dateSelected, options: [])
            
            
            if components.day == 0 {
                if components.hour!+1 <= Int(String(describing: strHours!))! {
                    let alert = UIAlertView(title: "Alert", message:  "Reservations can only be accepted after \(strHours!) hours of current time", delegate: nil, cancelButtonTitle: "Dismiss")
                    alert.show()
                    return
                }
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm a"
        //    if self.dateSelected != nil {
        self.strDateTime  = Globals.sharedInstance.dateFromTimestamp(self.dateSelected.timeIntervalSinceNow)
        self.arrDataSourceHourly[0][Constants.PickUpDate] = dateFormatter.string(from: self.dateSelected) as Any?
        self.activeTextField.text = dateFormatter.string(from: self.dateSelected)
        self.activeTextField.resignFirstResponder()
        //  self.tableViewReservations.reloadData()
    }
    @IBAction func btnDonePickerViewAction(_ sender: UIBarButtonItem) {
        if self.isTxtAdultPicker == false {
            if self.currentSection == IndexSection.serviceType.rawValue
            {
                self.arrDataSourceHourly[IndexSection.serviceType.rawValue][Constants.Service] = self.arrDataServiceType[pickerViewCarName.selectedRow(inComponent: 0)] as! [String:Any] as Any?
                self.strServiceType =  (( self.arrDataServiceType[pickerViewCarName.selectedRow(inComponent: 0)] as! [String:Any])["service_type"] as? String!)!
                self.activeTextField.text = self.strServiceType
                //self.calculateEstimateFareHoury()
            }else
                if self.currentSection == IndexSection.vehicle.rawValue
                {
                    self.arrDataSourceHourly[self.currentSection][Constants.Vehicle] = self.arrDataSourceCarInfo[pickerViewCarName.selectedRow(inComponent: 0)] as! [String:Any] as Any?
                    if let dicValues =  self.arrDataSourceCarInfo[pickerViewCarName.selectedRow(inComponent: 0)] as? [String:Any]{
                        self.strVehicle =  dicValues["vehicle_type_title"] as! String
                        self.activeTextField.text = self.strVehicle
                        
                        if let adut_cap = dicValues["vehicle_specific_passenger_capacity"]{
                            txtAdult.text = String(describing: adut_cap).characters.count > 0 ?  String(describing: adut_cap) : "01"
                            self.arrAdults.removeAll(keepingCapacity: true)
                            let loop = Int(self.txtAdult.text!)!
                            for  i  in  1...loop {
                                self.arrAdults.append(String(i))
                            }
//                            for var i = 1 ; i <= Int(txtAdult.text!); i += 1 {
//                                self.arrAdults.append(String(i))
//                            }
                        }
                        
                        if let adut_cap = dicValues["vehicle_specific_luggage_capacity"]{
                            txtLagguge.text =  String(describing: adut_cap).characters.count > 0 ?  String(describing: adut_cap) : "01"
                            self.arrLuggage.removeAll(keepingCapacity: true)
                            let loop = Int(self.txtLagguge.text!)!
                            for  i  in  1...loop {
                                self.arrLuggage.append(String(i))
                            }
//                            for var i = 1 ; i <= Int(txtLagguge.text!); i += 1 {
//                                self.arrLuggage.append(String(i))
//                            }
                        }
                        //self.calculateEstimateFareHoury()
                    }
                }else if self.currentSection == IndexSection.childSeatType.rawValue
                {
                    self.arrDataSourceHourly[self.currentSection][Constants.ChildSeatType] = self.arrChildSeatType[pickerViewCarName.selectedRow(inComponent: 0)] as Any?
                    self.activeTextField.text = self.arrChildSeatType[pickerViewCarName.selectedRow(inComponent: 0)]
                }else if self.currentSection == IndexSection.childSeatCount.rawValue
                {
                    self.arrDataSourceHourly[self.currentSection][Constants.ChildCount] = self.arrChildCount[pickerViewCarName.selectedRow(inComponent: 0)] as Any?
                    self.activeTextField.text = self.arrChildCount[pickerViewCarName.selectedRow(inComponent: 0)]
                    
                }else  if self.currentSection == IndexSection.hourly.rawValue {
                    
                    self.arrDataSourceHourly[IndexSection.hourly.rawValue][Constants.Hourly] = self.arrHoursCount[pickerViewCarName.selectedRow(inComponent: 0)] as Any?
                    // self.tableViewReservations.reloadData()
                    self.activeTextField.text = self.arrHoursCount[pickerViewCarName.selectedRow(inComponent: 0)]
                    self.view.endEditing(true)
            }
        } else {
            self.isTxtAdultPicker = false
            if self.activeTextField.tag == 81 {
                self.activeTextField.text = self.arrLuggage[pickerViewCarName.selectedRow(inComponent: 0)]
                strLagguge = self.arrLuggage[pickerViewCarName.selectedRow(inComponent: 0)]
            }else if self.activeTextField.tag == 80 {
                self.activeTextField.text = self.arrAdults[pickerViewCarName.selectedRow(inComponent: 0)]
                strAdult = self.arrAdults[pickerViewCarName.selectedRow(inComponent: 0)]
            }
        }
        self.view.endEditing(true)
        self.activeTextField.resignFirstResponder()
    }
    
    func setupPickerViewAgain(){
        appDelegate.hideProgress()
        UIView.animate(withDuration: 0.20, animations: { [weak self]() -> Void in
            self?.pickerViewCarName.reloadAllComponents()
            self?.viewPickerView.frame = CGRect(x: 0, y: (self?.view.frame)!.height - 216, width: (self?.view.frame)!.width , height: 260)
            self?.viewPickerView.isHidden = false
            self?.view.addSubview((self?.viewPickerView)!)
            self?.view!.bringSubview(toFront: (self?.viewPickerView)!)
        }) 
    }
    // MARK:- ====Text View delegate  ===
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.activeTextView = textView
        if self.activeTextView.text == "Pickup Notes" || self.activeTextView.text == "Drop Off Notes"{
            self.activeTextView.text = "" //changes Prakash
        }
        self.tagTextView = textView.superview?.tag
        self.currentSection  = (textView.superview?.tag)!
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView){
        
        // self.arrDataSource[10]["Pickup Notes"] = textView.text
        if self.activeTextView.text != nil {
            self.activeTextView.resignFirstResponder()
            if  self.tagTextView == IndexSection.pickType.rawValue{
                self.dictPickAddess[Constants.PickNotes] = self.activeTextView.text as Any?
            }else if  self.tagTextView == IndexSection.dropType.rawValue{
                self.dictDropOffAddess[Constants.DropNotes] = self.activeTextView.text as Any?
            }
            // self.reloadTableViewOnMainQueue()//changes Prakash
        }
        
    }
    @IBAction func btnDoneTextViewNotesAction(_ sender: UIBarButtonItem) {
        if self.activeTextField != nil {
        if self.currentSection == IndexSection.hourly.rawValue {
            self.arrDataSourceHourly[IndexSection.hourly.rawValue][Constants.Hourly] =  self.activeTextField.text as Any?
            self.activeTextField.resignFirstResponder()
           }
        }
        if self.activeTextView != nil {
            self.activeTextView.resignFirstResponder()
            if  self.tagTextView == IndexSection.pickType.rawValue{
                self.dictPickAddess[Constants.PickNotes] = self.activeTextView.text as Any?
            }else if  self.tagTextView == IndexSection.dropType.rawValue{
                self.dictDropOffAddess[Constants.DropNotes] = self.activeTextView.text as Any?
            }
            self.activeTextView.resignFirstResponder ()
            // self.reloadTableViewOnMainQueue()//changes Prakash
        }
    }
    // MARK:- ====key will Shown and Hide Notification ===
    func registerForKeybaordNotification()
    {
        NotificationCenter.default.addObserver(self, selector:#selector(PAReservationsHourlyVC.KeyboardWillShown(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PAReservationsHourlyVC.KeyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
    }
    func unRegisterForKeboardNotification()
    {
        NotificationCenter.default.removeObserver(self)
    }
    func KeyboardWillShown(_ notification: Notification)
    {
        if self.currentSection == IndexSection.pickUpDate.rawValue || self.currentSection == IndexSection.vehicle.rawValue || self.currentSection == IndexSection.serviceType.rawValue || self.currentSection == IndexSection.adultLagguge.rawValue  || self.currentSection == IndexSection.none.rawValue {
        }else{
            if self.activeTextView == nil || self.activeTextField == nil  {
                return
            }
            
            let userInfo = notification.userInfo!
            _ = ((userInfo[UIKeyboardFrameEndUserInfoKey]! as Any) as AnyObject).cgRectValue.size
            let pointInTable:CGPoint!
            if self.currentSection == IndexSection.pickType.rawValue || self.currentSection == IndexSection.dropType.rawValue {
                
                pointInTable = self.activeTextView.superview!.convert(self.activeTextView.frame.origin, to:self.tableViewReservations)
            }else {
                
                pointInTable = self.activeTextField.convert(self.activeTextField.frame.origin, to:self.tableViewReservations)
            }
            
            var contentOffset:CGPoint = self.tableViewReservations.contentOffset
            contentOffset.y  = pointInTable.y - 50
            if let accessoryView = self.accessoryViewTextView {
                contentOffset.y -= accessoryView.frame.size.height
            }
            self.tableViewReservations.contentOffset = contentOffset
        }
    }
    func KeyboardWillHide(_ notification: Notification)
    {
        if self.currentSection != 0  {
            self.tableViewReservations.scrollsToTop = true
            self.tableViewReservations.scrollToNearestSelectedRow(at: .middle, animated: true)
            self.tableViewReservations.scrollIndicatorInsets = UIEdgeInsets.zero
            //            if self.currentSection == IndexSection.ChildSeatCount.rawValue  || self.currentSection == IndexSection.ChildSeatType.rawValue {
            //                var contentOffset:CGPoint = self.tableViewReservations.contentOffset
            //                contentOffset.y  = +350
            //                self.tableViewReservations.contentOffset = contentOffset
            //            }
        }
    }
   
    
    // MARK: - ======== cell button Custom Methodsss========
    func btnDeleteChildSeatCount(_ sender:UIButton){
        self.view.endEditing(true)

        let tag = sender.superview!.superview!.tag
        self.arrAddedChildSeats.remove(at: tag);
        self.tableViewReservations.reloadData()
    }
    func btnAddChildSeatCountAction(_ sender:UIButton){
        
        self.view.endEditing(true)
        
        let name = self.arrDataSourceHourly[IndexSection.childSeatType.rawValue][Constants.ChildSeatType] as! String
        let count = self.arrDataSourceHourly[IndexSection.childSeatCount.rawValue][Constants.ChildCount] as! String
        
        if name == "Child Seat Type"  {
            Globals.ToastAlertWithString("Please select seat type")
            return
        }
        if  count == "Child Seat Count" {
            Globals.ToastAlertWithString("Please select seat count")
            return
        }
        var match = false
        var dicChild = [String:Any]()
        if self.arrAddedChildSeats.count != 0 {
            for (index, var dic) in self.arrAddedChildSeats.enumerated() {
                if dic["name"] as! String == name {
                    let CountInt = count.toInteger()
                    
                    let lastCount = (dic["count"] as! String).toInteger()
                    if  CountInt + lastCount > 10 {
                        dic["count"] = "10" as Any?
                        Globals.ToastAlertWithString("Maximum seat are 10")
                    }else {
                        dic["count"] = String(CountInt + lastCount) as Any?
                    }
                    match = true
                    dicChild = dic
                    self.arrAddedChildSeats.remove(at: index)
                    self.arrAddedChildSeats.insert(dicChild, at: index)
                    break
                }
            }
        }
        if match == false {
            dicChild["name"] = name as Any?
            dicChild["count"] = count as Any?
            self.arrAddedChildSeats.append(dicChild)
        }
        self.tableViewReservations.reloadData()
    }
    
    @IBAction func btnDisabilityAddRemoveAction(_ sender: UIButton) {
        if sender.isSelected  == true {
            self.isDisable = false
            self.strDisability = "0"
            sender.isSelected = false
        }else {
            self.isDisable = true
            self.strDisability = "1"
            sender.isSelected = true
        }
    }
    @IBAction func btnAddAdultLaggugeAction(_ sender: UIButton) {
        self.isTxtAdultPicker = true
        self.pickerViewCarName.reloadAllComponents()
        self.currentSection = IndexSection.adultLagguge.rawValue
        self.activeButton = sender
        // self.setupPickerViewAgain()
    }
    // MARK:- ====add Pickup and Drop of Address =====
    @IBAction func btnCellPickupAddressTypeAction(_ sender: UIButton) {
        self.strPickType = self.isEditingMode == true ? self.strPickType : pickupType.None.rawValue
        //self.strPickType = self.isEditingMode == false ? self.strPickType : pickupType.None.rawValue
        
        self.view.endEditing(true)
        let ContentView = sender.superview
        for buttons in ContentView!.subviews {
            if buttons.isKind(of: UIButton.self){
                let btn = buttons as! UIButton
                if sender.tag == btn.tag {
                    sender.isSelected = true
                    self.btnPickupTypeTag = sender.tag
                }else {
                    btn.isSelected = false
                }
            }
        }
        if self.btnPickupTypeLastOne == 0 {
            self.btnPickupTypeLastOne = self.btnPickupTypeTag
        }
        self.currentSection = IndexSection.pickType.rawValue
        switch sender.tag {
        case btnTag.btnAddress.rawValue :
            // self.strPickType = pickupType.Address.rawValue
            self.pushToSelectAddressVC(sender)
            
        case btnTag.btnAirport.rawValue :
            //self.strPickType = pickupType.Airport.rawValue
            self.pushToAirportVC(sender)
            
            break
        case btnTag.btnFBO.rawValue :
            //self.strPickType = pickupType.FBO.rawValue
            self.showFBOAlertController(sender)
            
            break
        case btnTag.btnSeaport.rawValue :
            //self.strPickType = pickupType.Seaport.rawValue
            self.showSeaportAlertController(sender)
            
            break
        default :
            break
        }
    }
    func setAddressTypeForButton(_ isPickup:Bool){
        if isPickup == true {
            switch self.btnPickupTypeTag {
            case btnTag.btnAddress.rawValue :
                self.strPickType = pickupType.Address.rawValue
            case btnTag.btnAirport.rawValue :
                self.strPickType = pickupType.Airport.rawValue
                break
            case btnTag.btnFBO.rawValue :
                self.strPickType = pickupType.FBO.rawValue
                break
            case btnTag.btnSeaport.rawValue :
                self.strPickType = pickupType.Seaport.rawValue
                break
            case btnTag.btnSeaport.rawValue :
                self.strPickType = pickupType.Seaport.rawValue
                break
            default :
                self.strPickType = pickupType.None.rawValue
                break
            }
        }else {
            switch self.btnDroppOffTypeTag {
            case btnTag.btnAddress.rawValue :
                self.strDropOffType = DropOffType.Address.rawValue
            case btnTag.btnAirport.rawValue :
                self.strDropOffType = DropOffType.Airport.rawValue
                break
            case btnTag.btnFBO.rawValue :
                self.strDropOffType = DropOffType.FBO.rawValue
                
                break
            case btnTag.btnSeaport.rawValue :
                self.strDropOffType = DropOffType.Seaport.rawValue
                break
            default :
                self.strDropOffType = DropOffType.None.rawValue
                
                break
            }
        }
        
    }
    
    @IBAction func btnCellDropOffAddressTypeAction(_ sender:UIButton){
         self.strDropOffType = self.isEditingMode == true ? self.strDropOffType : DropOffType.None.rawValue
        self.view.endEditing(true)
        let ContentView = sender.superview
        for buttons in ContentView!.subviews {
            if buttons.isKind(of: UIButton.self){
                let btn = buttons as! UIButton
                if sender.tag == btn.tag {
                    sender.isSelected = true
                    self.btnDroppOffTypeTag = sender.tag
                }else {
                    btn.isSelected = false
                }
            }
        }
        self.currentSection = IndexSection.dropType.rawValue
        if self.btnDroppOffTypeLastOne == 0 {
            self.btnDroppOffTypeLastOne = self.btnDroppOffTypeTag
        }
        switch sender.tag {
        case btnTag.btnAddress.rawValue :
            // self.strDropOffType = DropOffType.Address.rawValue
            self.pushToSelectAddressVC(sender)
        case btnTag.btnAirport.rawValue :
            // self.strDropOffType = DropOffType.Airport.rawValue
            self.pushToAirportVC(sender)
            break
        case btnTag.btnFBO.rawValue :
            // self.strDropOffType = DropOffType.FBO.rawValue
            self.showFBOAlertController(sender)
            break
        case btnTag.btnSeaport.rawValue :
            // self.strDropOffType = DropOffType.Seaport.rawValue
            self.showSeaportAlertController(sender)
            
            break
        default :
            break
        }
        
    }
    func btnCellStopsAndPaymentAction(_ sender:UIButton){
        self.view.endEditing(true)
        if sender.tag == IndexSection.stops.rawValue {
            self.currentSection = IndexSection.stops.rawValue
            self.pushToSelectAddressVC(sender)
        }else if sender.tag == IndexSection.payment.rawValue {
            self.currentSection = IndexSection.payment.rawValue
            self.showPaymentAlertController(sender)
        }
        
        
    }
    func btnCellOptionAction(_ sender:UIButton){
        
        //    let arrDicKeys = (self.arrDataSource[sender.tag]).keys
        //
        //    let strKey = arrDicKeys.first!
        if sender.tag == 1 || sender.tag == 3 {
            if self.strAirport == "toAirPort"  {
                if sender.tag == 3 {
                    self.pushToAirportVC(sender)
                }else {
                    self.pushToSelectAddressVC(sender)
                }
            }else if self.strAirport == "From Airport" {
                if sender.tag == 1 {
                    self.pushToAirportVC(sender)
                }else {
                    self.pushToSelectAddressVC(sender)
                }
            }else {
                self.pushToSelectAddressVC(sender)
            }
        }else  if sender.tag ==  2 {
            self.pushToSelectAddressVC(sender)
        }else  if sender.tag ==  7 {
            self.activeButton = sender
            self.getingVehlciTypesAvailable()
        }else  if sender.tag ==  9 {
            self.activeButton = sender
            self.showPaymentAlertController(sender)
        }
        
    }
    func GetAddressOfCurrentLocationFromAirportView( _ dicAirport:[String:Any], withTag:Int)
    {
        if (dicAirport["airport_lat"]! as! String).toDouble()! == 0.0 {
            return
        }
        if appDelegate.networkAvialable == true {
            appDelegate.showProgressWithText("Loading Address....")
            var  dicReturn = [String:Any]()
            
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async { () -> Void in
                weak var weakSelf = self
                GoogleMapAPIServiceManager.sharedInstance.getLocationAddressUsingLatitude((dicAirport["airport_lat"]! as! String).toDouble()! , longitude: (dicAirport["airport_lng"]! as! String).toDouble()!, withComplitionHandler: { (dicRecievedJSON) -> () in
                    
                    if dicRecievedJSON["status"] as! String == "OK"{
                        dicReturn = Globals.sharedInstance.formatedAddressCompnentsSwift( (dicRecievedJSON ["results"] as! [Any]).first as! [String:Any])
                        print(dicReturn)
                        appDelegate.hideProgress()
                        if withTag == IndexSection.pickType.rawValue {
                            weakSelf?.arrDataSourceHourly[weakSelf!.currentSection][Constants.PickType] = dicReturn
                            weakSelf?.dictPickAddess = dicReturn
                            weakSelf?.dictPickAddess[Constants.PickNotes] = "Pickup Notes"
                            weakSelf?.getDistanceFromPickUpToDropOff()
                            weakSelf?.reloadTableViewSectionOnMainQueue(IndexSection.pickType.rawValue)
                        } else if withTag == IndexSection.dropType.rawValue{
                            weakSelf?.arrDataSourceHourly[weakSelf!.currentSection][Constants.DropType] = dicReturn
                            weakSelf?.dictDropOffAddess = dicReturn
                            weakSelf?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes"
                            weakSelf?.getDistanceFromPickUpToDropOff()
                            weakSelf?.reloadTableViewSectionOnMainQueue(IndexSection.dropType.rawValue)
                        }
                    }else {
                        appDelegate.hideProgress()
                        Globals.ToastAlertForNetworkConnection()
                    }
                    })
                { (error) -> () in
                    appDelegate.hideProgress()
                    Globals.ToastAlertForNetworkConnection()
                }
            }
        }
        else {
            Globals.ToastAlertForNetworkConnection()
        }
    }
    // MARK: - ======== Push to  view Contrtollers ========
    func pushToAirportVC(_ sender:UIButton){
        
        let arrDicKeys = (self.arrDataSourceHourly[self.currentSection]).keys
        
        let strKey = arrDicKeys.first!
        let strDate = self.arrDataSourceHourly[0][Constants.PickUpDate] as? String
        if  strDate == ""   {
        //    JLToast.makeText("Please Select Pick-Up-Date", duration: 3.0).show()
            return
        }
        
        let airportVC = self.storyboard?.instantiateViewController(withIdentifier: "AirportViewController") as! AirportViewController
        
        if self.currentSection == IndexSection.pickType.rawValue{
            self.strAirport = "From Airport"
            airportVC.tagBtnLast = self.btnPickupTypeLastOne
            let date1  = Globals.sharedInstance.getDateFormatForDateString(strDate!)
            let arrtime = date1.components(separatedBy: " ")
            airportVC.strPickupDate = arrtime[1]
            if self.isEditingMode == true && self.strPickType == pickupType.Airport.rawValue{
                if let dic = self.reservationDetails["pickup_info"] as? [AnyHashable: Any] {
                    airportVC.dicDetails = dic
                    airportVC.editReservation = true
                }
            }else if self.dicOnlyAirport.count > 0 && btnTag.btnAirport.rawValue == self.btnPickupTypeLastOne{
                if let pickDic = self.dicOnlyAirport["pickup"] as? [String:Any]{
                    airportVC.dicDetails = pickDic
                    airportVC.editReservation = false
                }
            }
            

        }else if self.currentSection == IndexSection.dropType.rawValue {
            airportVC.tagBtnLast = self.btnDroppOffTypeLastOne
            self.strAirport = "toAirPort"
            if self.isEditingMode == true && self.strDropOffType == DropOffType.Airport.rawValue {
                if let dic = self.reservationDetails["dropoff_info"] as? [AnyHashable: Any] {
                    airportVC.dicDetails = dic
                    airportVC.editReservation = true
                }
            }else if self.dicOnlyAirport.count > 0 && btnTag.btnAirport.rawValue == self.btnDroppOffTypeLastOne{
                if let pickDic = self.dicOnlyAirport["dropoff"] as? [String:Any]{
                    airportVC.dicDetails = pickDic
                    airportVC.editReservation = false
                }
            }

        }
        
        airportVC.airportInstructions = NSMutableArray(array: self.arrAirportInstuctions)

        airportVC.strReservationType = self.strAirport
        airportVC.strDateTime = self.strDateTime.replacingOccurrences(of: "/", with: "")
        airportVC.finishAirportSelection = { [weak self](dicAirdicAirPortDetails :[AnyHashable: Any]?, tag:NSInteger, isDone:Bool)-> () in
             let dicAirdicAirPortDetails = dicAirdicAirPortDetails! as! [AnyHashable: Any]
            if dicAirdicAirPortDetails.count == 0 {
                if self?.currentSection == IndexSection.pickType.rawValue{
                    if self?.dictPickAddess.count != 0 {
                        self?.btnPickupTypeTag = tag
                        self?.setAddressTypeForButton(true)
                    }else {
                        self?.btnPickupTypeTag = 0
                        
                        self?.strPickType = pickupType.None.rawValue
                    }
                }else if self?.currentSection == IndexSection.dropType.rawValue {
                    if self?.dictDropOffAddess.count != 0 {
                        self?.btnDroppOffTypeTag = tag
                        self?.setAddressTypeForButton(false)
                    }else {
                        self?.btnDroppOffTypeTag = 0
                        self?.strDropOffType = pickupType.None.rawValue
                    }
                }
                self?.reloadTableViewOnMainQueue()
                self?.strAirport = ""
                return
            }
            if self?.currentSection == IndexSection.pickType.rawValue{
                self?.dicOnlyAirport["pickup"] = dicAirdicAirPortDetails as! [String:Any]
                self?.pickUpLocation = CLLocation(latitude:(dicAirdicAirPortDetails["airport_lat"] as! String) .toDouble()!, longitude: (dicAirdicAirPortDetails["airport_lng"] as! String) .toDouble()!)
                self?.dictPickAddess[Constants.PickNotes] = "Pickup Notes"
                //  self?.btnPickupTypeTag = tag
                self?.setAddressTypeForButton(true)
                self?.GetAddressOfCurrentLocationFromAirportView(dicAirdicAirPortDetails as! [String:Any], withTag: IndexSection.pickType.rawValue)
                self?.btnPickupTypeLastOne = (self?.btnPickupTypeTag)!
                self?.arrDataSourceHourly[(self?.currentSection)!][strKey] = dicAirdicAirPortDetails
                //   self?.reloadTableViewSectionOnMainQueue(IndexSection.PickType.rawValue)
            }else if self?.currentSection == IndexSection.dropType.rawValue {
                self?.dicOnlyAirport["dropoff"] = dicAirdicAirPortDetails as! [String:Any]
                self?.dropOffLocation = CLLocation(latitude:(dicAirdicAirPortDetails["airport_lat"] as! String) .toDouble()!, longitude: (dicAirdicAirPortDetails["airport_lng"] as! String) .toDouble()!)
                self?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes"
                // self?.btnDroppOffTypeTag = tag
                self?.setAddressTypeForButton(false)
                self?.GetAddressOfCurrentLocationFromAirportView(dicAirdicAirPortDetails as! [String:Any], withTag: IndexSection.dropType.rawValue)
                self?.btnDroppOffTypeLastOne = (self?.btnDroppOffTypeTag)!
                self?.arrDataSourceHourly[(self?.currentSection)!][strKey] = dicAirdicAirPortDetails
                // self?.reloadTableViewSectionOnMainQueue(IndexSection.PickType.rawValue)
            }
            self?.reloadTableViewOnMainQueue()
            
        }
        
        self.navigationController?.pushViewController(airportVC, animated: true)
        
    }
    
    func pushToSelectAddressVC(_ sender:UIButton){
        //let arrDicKeys = (self.arrDataSourceHourly[self.currentSection]).keys
        
        //let strKey = arrDicKeys.first!
        let selectAddrsVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesAddessViewController") as! SearchPlacesAddessViewController
        selectAddrsVC.viewFrom = "rideLater"
        
     
        if self.currentSection == IndexSection.pickType.rawValue{
            self.strPickType = pickupType.Address.rawValue
            selectAddrsVC.tagBtnAddressLast = self.btnPickupTypeLastOne
            selectAddrsVC.isPickUpAddress = true
            selectAddrsVC.strTypeOfAddress = "Pickup Address"
            if self.isEditingMode == true && self.strPickType == pickupType.Address.rawValue ||  self.strPickType == pickupType.POI.rawValue {
                if let dic = self.reservationDetails["pickup_info"] as? [String:Any] {
                    selectAddrsVC.dicDetails = dic
                }
            }else if self.dictPickAddess.count > 0 && btnTag.btnAddress.rawValue == self.btnPickupTypeLastOne{
                selectAddrsVC.dicDetails =  self.dictPickAddess
            }
        }else if self.currentSection == IndexSection.dropType.rawValue {
               self.strDropOffType = DropOffType.Address.rawValue
            selectAddrsVC.tagBtnAddressLast = self.btnDroppOffTypeLastOne
            selectAddrsVC.isPickUpAddress = false
            selectAddrsVC.strTypeOfAddress = "Dropoff Address"
            if self.isEditingMode == true && self.strDropOffType == DropOffType.Address.rawValue || self.strDropOffType ==  DropOffType.POI.rawValue {
                if let dic = self.reservationDetails["dropoff_info"] as? [String:Any] {
                    selectAddrsVC.dicDetails = dic
                }
            }else if self.dictDropOffAddess.count > 0 && btnTag.btnAddress.rawValue == self.btnDroppOffTypeLastOne{
                selectAddrsVC.dicDetails =  self.dictDropOffAddess
            }
            
        }else {
            selectAddrsVC.isPickUpAddress = false
            selectAddrsVC.strTypeOfAddress = "Stops Address"
        }
        selectAddrsVC.finishSelectingAddress = { [weak self](dicDropOff : [String:Any], tag:Int, isdone:Bool) -> () in
            
            if  self?.currentSection == IndexSection.pickType.rawValue {
                if dicDropOff.count == 0  {
                    if self?.dictPickAddess.count != 0 {
                        self?.btnPickupTypeTag = tag
                        self?.setAddressTypeForButton(true)
                    }else {
                        self?.btnPickupTypeTag = 0
                        self?.strPickType = pickupType.None.rawValue
                    }
                    self?.reloadTableViewOnMainQueue()
                    return
                }
                self?.arrDataSourceHourly[(self?.currentSection)!][Constants.PickType] = dicDropOff as Any?
                self?.dictPickAddess.removeAll()
                self?.dictPickAddess = dicDropOff
                self?.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                self?.btnPickupTypeLastOne = (self?.btnPickupTypeTag)!
                self?.setAddressTypeForButton(true)
                self?.reloadTableViewOnMainQueue()
                self?.pickUpLocation = CLLocation(latitude: dicDropOff["latitude"] as! Double , longitude: dicDropOff["longitude"] as! Double)
                self?.getDistanceFromPickUpToDropOff()
                
            }else if self?.currentSection == IndexSection.stops.rawValue {
                
                if dicDropOff.count == 0 {
                    return
                }
                var dicStops = [String:Any]()
                dicStops["stop_name"] = dicDropOff["address"]
                dicStops["latitude"] = dicDropOff["latitude"]
                dicStops["longitude"] = dicDropOff["longitude"]
                dicStops["location"] = dicDropOff["address"]
                dicStops["city"] = dicDropOff["locality"]
                dicStops["state"] = dicDropOff["administrative_area_level_1"]
                dicStops["country"] = dicDropOff["country"]
                dicStops["zip"] = dicDropOff["postal_code"]
                self?.arrStops.append(dicStops)
                
                self?.arrDataSourceHourly[IndexSection.stops.rawValue][Constants.Stops] = self?.arrStops as Any?
                
                self?.tableViewReservations.setEditing(true, animated: true)
                self?.reloadTableViewOnMainQueue()
            }else if self?.currentSection == IndexSection.dropType.rawValue {
                if dicDropOff.count == 0  {
                    if self?.dictDropOffAddess.count != 0 {
                        self?.btnDroppOffTypeTag = tag
                        self?.setAddressTypeForButton(false)
                    }else {
                        self?.btnDroppOffTypeTag = 0
                        self?.strDropOffType = pickupType.None.rawValue
                    }
                    self?.reloadTableViewOnMainQueue()
                    return
                }
                self?.arrDataSourceHourly[IndexSection.dropType.rawValue][Constants.DropType] = dicDropOff as Any?
                self?.dictDropOffAddess.removeAll()
                self?.dictDropOffAddess = dicDropOff
                self?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                self?.setAddressTypeForButton(false)
                self?.btnDroppOffTypeLastOne = (self?.btnDroppOffTypeTag)!
                self?.reloadTableViewOnMainQueue()
                self?.dropOffLocation = CLLocation(latitude: dicDropOff["latitude"] as! Double , longitude: dicDropOff["longitude"] as! Double)
                self?.getDistanceFromPickUpToDropOff()
            }
            
        }
          self.navigationController?.pushViewController(selectAddrsVC, animated: true)
    }
    
    
    func showPaymentAlertController(_ sender:UIButton){
        let payment = self.storyboard?.instantiateViewController(withIdentifier: "PASelectPaymentVC") as! PASelectPaymentVC
        
        payment.pushToRequestView = { [weak self] (dicCardDetails:[String:Any]?, cardID:String?) -> () in
            if let _ = dicCardDetails {
                self?.arrDataSourceHourly[(self?.currentSection)!][Constants.Payment] = cardID as Any?
                self?.dicCardDetail = dicCardDetails!
                self?.strPayment = cardID!
            }
            //   self?.reloadTableViewOnMainQueue()
            self?.reloadTableViewSectionOnMainQueue(IndexSection.payment.rawValue)
        }
        self.navigationController?.pushViewController(payment, animated: true)
    }
    func showSeaportAlertController(_ sender:UIButton){
        
        let arrDicKeys = (self.arrDataSourceHourly[self.currentSection]).keys
        
        let strKey = arrDicKeys.first!
        let SeaportVC = self.storyboard?.instantiateViewController(withIdentifier: "PASeaportAddressVC") as! PASeaportAddressVC
        var strTypeOfAddress = ""
        if self.currentSection == IndexSection.pickType.rawValue{
            SeaportVC.tagBtnLast = self.btnPickupTypeLastOne
            SeaportVC.isPickUpAddress = true
            strTypeOfAddress = "Pickup Seaport"
            if self.isEditingMode == true && self.strPickType == pickupType.Seaport.rawValue{
                if let dic = self.reservationDetails["pickup_info"] as? [String:Any] {
                    SeaportVC.dicDetails = dic
                }
            }else if self.dictPickAddess.count > 0 && btnTag.btnSeaport.rawValue == self.btnPickupTypeLastOne{
                SeaportVC.dicDetails =  self.dictPickAddess["seaport_details"] as? [String:Any]
            }
        }else if self.currentSection == IndexSection.dropType.rawValue {
            SeaportVC.tagBtnLast = self.btnDroppOffTypeLastOne
            SeaportVC.isPickUpAddress = false
            strTypeOfAddress = "Dropoff Seaport"
            if self.isEditingMode == true && self.strDropOffType == DropOffType.Seaport.rawValue {
                if let dic = self.reservationDetails["dropoff_info"] as? [String:Any]
                {
                    SeaportVC.dicDetails = dic
                }
            }else if self.dictDropOffAddess.count > 0 && btnTag.btnSeaport.rawValue == self.btnDroppOffTypeLastOne{
                SeaportVC.dicDetails =  self.dictDropOffAddess["seaport_details"] as? [String:Any]
            }
        }
        
        SeaportVC.finishSelectingSeaportAddress = { [weak self](dicSeaportDetails :[String:Any]!, tag:NSInteger, isdone:Bool)-> () in
            if dicSeaportDetails.count == 0 {
                if self?.currentSection == IndexSection.pickType.rawValue{
                    if self?.dictPickAddess.count != 0 {
                        self?.btnPickupTypeTag = tag
                        self?.setAddressTypeForButton(true)
                    }else {
                        self?.btnPickupTypeTag = 0
                        
                        self?.strPickType = pickupType.None.rawValue
                    }
                    self?.reloadTableViewOnMainQueue()
                    // self?.reloadTableViewSectionOnMainQueue(IndexSection.PickType.rawValue)
                }else if self?.currentSection == IndexSection.dropType.rawValue {
                    if self?.dictDropOffAddess.count != 0 {
                        self?.btnDroppOffTypeTag = tag
                        self?.setAddressTypeForButton(false)
                    }else {
                        
                        self?.btnDroppOffTypeTag = 0
                        self?.strDropOffType = pickupType.None.rawValue
                    }
                    self?.reloadTableViewOnMainQueue()
                    // self?.reloadTableViewSectionOnMainQueue(IndexSection.DropType.rawValue)
                    
                }
                return
            }
            self?.arrDataSourceHourly[(self?.currentSection)!][strKey] = dicSeaportDetails as Any?
            if self?.currentSection == IndexSection.pickType.rawValue{
                self?.btnPickupTypeLastOne = (self?.btnPickupTypeTag)!
                self?.dictPickAddess.removeAll()
                self?.dictPickAddess = dicSeaportDetails
                self?.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                if let addressDetails = self?.dictPickAddess["seaport_address"] as? [String:Any] {
                    let lati : Double = CDouble(addressDetails["latitude"] as! Double)
                    let longi : Double = CDouble(addressDetails["longitude"] as! Double)
                    self?.pickUpLocation = CLLocation(latitude: lati, longitude: longi)
                }
                //self?.btnPickupTypeTag = tag
                self?.setAddressTypeForButton(true)
                self?.reloadTableViewOnMainQueue()
                //   self?.reloadTableViewSectionOnMainQueue(IndexSection.PickType.rawValue)
            }else if self?.currentSection == IndexSection.dropType.rawValue {
                self?.btnDroppOffTypeLastOne = (self?.btnDroppOffTypeTag)!
                self?.dictDropOffAddess.removeAll()
                self?.dictDropOffAddess = dicSeaportDetails
                self?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                if let addressDetails = self?.dictPickAddess["seaport_address"] as? [String:Any] {
                    let lati : Double = CDouble(addressDetails["latitude"] as! Double)
                    let longi : Double = CDouble(addressDetails["longitude"] as! Double)
                    self?.dropOffLocation = CLLocation(latitude: lati, longitude: longi)
                }
                //self?.btnDroppOffTypeTag = tag
                self?.setAddressTypeForButton(false)
                // self?.reloadTableViewSectionOnMainQueue(IndexSection.DropType.rawValue)
                self?.reloadTableViewOnMainQueue()
            }
            //  self?.reloadTableViewOnMainQueue()
            self?.getDistanceFromPickUpToDropOff()
        }
        SeaportVC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(strTypeOfAddress, imageName: "backArrow")
        self.navigationController?.pushViewController(SeaportVC, animated: true)
    }
    func showFBOAlertController(_ sender:UIButton){
        let arrDicKeys = (self.arrDataSourceHourly[self.currentSection]).keys
        let strKey = arrDicKeys.first!
        let strDate = self.arrDataSourceHourly[0][Constants.PickUpDate] as? String
        if  strDate == ""   {
            //JLToast.makeText("Please Select Pick-Up-Date", duration: 3.0).show()
            return
        }
        let FBO_VC = self.storyboard?.instantiateViewController(withIdentifier: "PAFBOAddressVC") as! PAFBOAddressVC
        var strTypeOfAddress = ""
        FBO_VC.strDateTime = self.strDateTime.replacingOccurrences(of: "/", with: "")
        if self.currentSection == IndexSection.pickType.rawValue{
            FBO_VC.strReservationType = "From Airport"
            FBO_VC.tagBtnLast = self.btnPickupTypeLastOne
            FBO_VC.isPickUpAddress = true
            strTypeOfAddress = "Pickup FBO"
            if self.isEditingMode == true && self.strPickType == pickupType.FBO.rawValue{
                if let dic = self.reservationDetails["pickup_info"] as? [String:Any] {
                    FBO_VC.dicDetails = dic
                }
            }else if self.dictPickAddess.count > 0 && btnTag.btnFBO.rawValue == self.btnPickupTypeLastOne{
                FBO_VC.dicDetails =  self.dictPickAddess["fbo_details"] as? [String:Any]
            }
        }else if self.currentSection == IndexSection.dropType.rawValue {
            FBO_VC.tagBtnLast = self.btnDroppOffTypeLastOne
            FBO_VC.strReservationType = "toAirPort"
            FBO_VC.isPickUpAddress = false
            strTypeOfAddress = "Dropoff FBO"
            if self.isEditingMode == true && self.strDropOffType == DropOffType.FBO.rawValue {
                if let dic = self.reservationDetails["dropoff_info"] as? [String:Any]{
                    FBO_VC.dicDetails = dic
                }
            }else if self.dictDropOffAddess.count > 0 && btnTag.btnFBO.rawValue == self.btnDroppOffTypeLastOne{
                FBO_VC.dicDetails =  self.dictDropOffAddess["fbo_details"] as? [String:Any]
            }
        }
        FBO_VC.finishSelectingFBOAddress = { [weak self](dicFBODetails :[String:Any]!, tag:NSInteger, isdone:Bool)-> () in
            if dicFBODetails.count == 0 {
                if self?.currentSection == IndexSection.pickType.rawValue{
                    if self?.dictPickAddess.count != 0 {
                        self?.btnPickupTypeTag = tag
                        self?.setAddressTypeForButton(true)
                    }else {
                        self?.btnPickupTypeTag = 0
                        self?.strPickType = pickupType.None.rawValue
                    }
                    self?.reloadTableViewOnMainQueue()
                    // self?.reloadTableViewSectionOnMainQueue(IndexSection.PickType.rawValue)
                }else if self?.currentSection == IndexSection.dropType.rawValue {
                    if self?.dictDropOffAddess.count != 0 {
                        self?.btnDroppOffTypeTag = tag
                        self?.setAddressTypeForButton(false)
                    }else {
                        self?.btnDroppOffTypeTag = 0
                        self?.strDropOffType = pickupType.None.rawValue
                    }
                    self?.reloadTableViewOnMainQueue()
                    //s//elf?.reloadTableViewSectionOnMainQueue(IndexSection.DropType.rawValue)
                }
                // self?.reloadTableViewOnMainQueue()
                return
            }
            self?.arrDataSourceHourly[(self?.currentSection)!][strKey] = dicFBODetails as Any?
            if self?.currentSection == IndexSection.pickType.rawValue{
                self?.btnPickupTypeLastOne = (self?.btnPickupTypeTag)!
                self?.dictPickAddess.removeAll()
                self?.dictPickAddess = dicFBODetails
                self?.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                if let addressDetails = self?.dictPickAddess["address"] as? [String:Any] {
                    let lati : Double = CDouble(addressDetails["latitude"] as! Double)
                    let longi : Double = CDouble(addressDetails["longitude"] as! Double)
                    self?.pickUpLocation = CLLocation(latitude:lati , longitude:longi )
                    
                }
                //  self?.btnPickupTypeTag = tag
                self?.setAddressTypeForButton(true)
                //  self?.reloadTableViewSectionOnMainQueue(IndexSection.PickType.rawValue)
                
            }else if self?.currentSection == IndexSection.dropType.rawValue {
                self?.btnDroppOffTypeLastOne = (self?.btnDroppOffTypeTag)!
                self?.dictDropOffAddess.removeAll()
                self?.dictDropOffAddess = dicFBODetails
                self?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                if let addressDetails = self?.dictPickAddess["address"] as? [String:Any] {
                    let lati : Double = CDouble(addressDetails["latitude"] as! Double)
                    let longi : Double = CDouble(addressDetails["longitude"] as! Double)
                    self?.dropOffLocation = CLLocation(latitude: lati, longitude: longi)
                }
                // self?.btnDroppOffTypeTag = tag
                self?.setAddressTypeForButton(false)
                //self?.reloadTableViewSectionOnMainQueue(IndexSection.DropType.rawValue)
                
            }
            self?.reloadTableViewOnMainQueue()
        self?.getDistanceFromPickUpToDropOff()
        }
        FBO_VC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(strTypeOfAddress, imageName: "backArrow")
        self.navigationController?.pushViewController(FBO_VC, animated: true)
    }
    
    // MARK: - ====calculate distance between pickp and drop off =====
    func getDistanceFromPickUpToDropOff(){
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async { [weak self]() -> Void in
            if  self?.pickUpLocation == nil  ||  self?.dropOffLocation == nil  {
                return
            }
            let urlStr = Globals.sharedInstance.getFullApiAddressForDistanceToDriverDistance(self?.pickUpLocation, destinationLoc: (self?.dropOffLocation)!)
            
            if urlStr == "" {
                return
            }
            ServiceManager.sharedInstance.dataTaskWithGetRequest(urlStr, successBlock: { (dicData) -> Void in
                let arrRoutes = dicData["routes"] as! [[String : Any]]
                if arrRoutes.count > 0 {
                    let dicMainRoutes = arrRoutes.first!
                    let arrLegs = dicMainRoutes["legs"] as! [[String:Any]]
                    self?.dicDisAndTime["distance"] = arrLegs.first!["distance"]!
                    self?.dicDisAndTime["duration"] = arrLegs.first!["duration"]!
                    //self?.calculateEstimateFareHoury()
                }
                }, failureBlock: { (error) -> () in
                    appDelegate.hideProgress()
                    print("Error \(error?.description)")
            })
        }
        
    }
    // MARK: - ====calculate Estimate fare between pickp and drop off =====
    
    func calculateEstimateFareHoury() {
        
        // let estFare = estDistance * Float(CarObject.base_per_km)!
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }
        if  self.strVehicle == "" || self.arrDataSourceHourly[IndexSection.dropType.rawValue][Constants.DropType] is String || self.arrDataSourceHourly[IndexSection.pickType.rawValue][Constants.PickType] is String  {
            return
        }
        var Time : Double!
        var distance : Float!
        if let arrKeeys : [String]? = [String](self.dicDisAndTime.keys)
        {
            if arrKeeys!.contains("duration"){
                let DicTime = self.dicDisAndTime["duration"]! as! [String:Any]
                Time = (DicTime["value"] as! Double) / 60
                let Dicdistance = self.dicDisAndTime["distance"]! as! [String:Any]
                distance = (Dicdistance["value"]  as! Float) * appDelegate.distanceMultiplier
                //let estFare = estDistance * Float(CarObject.base_per_km)!
            }
        }else {
            return
        }
        
        
        
        var param_Dictionary: [String : Any] = [String : Any]()
        let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
        let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
        
        param_Dictionary["user_id"] = user_ID
        param_Dictionary["session"] = session
        
        
        if   !(self.arrDataSourceHourly[IndexSection.vehicle.rawValue][Constants.Vehicle] is String){
            let dic = self.arrDataSourceHourly[IndexSection.vehicle.rawValue][Constants.Vehicle] as! [String:Any]
            param_Dictionary["vechile"] = dic["id"]
        }
        if !(self.arrDataSourceHourly[IndexSection.serviceType.rawValue][Constants.Service] is String){
            let dic = self.arrDataSourceHourly[IndexSection.serviceType.rawValue][Constants.Service] as! [String:Any]
            param_Dictionary["service_type"] = dic["id"]
        }
        param_Dictionary["distance"] = distance as Any?
        param_Dictionary["time"] = Time as Any?
        param_Dictionary["distance_type"] = appDelegate.distanceType
          param_Dictionary["template_type"] = "hourly" as Any?
        
        if self.strPickType == pickupType.Address.rawValue ||  self.strPickType == pickupType.POI.rawValue
        {
            // for pickup
            if let dicPickupAddress = self.arrDataSourceHourly[IndexSection.pickType.rawValue][Constants.PickType] as? [String:Any] {
                param_Dictionary["pickup_location"] = dicPickupAddress["address"]
                param_Dictionary["zip_from"] = dicPickupAddress["postal_code"]
            }
            
        }
        else if self.strPickType == pickupType.Airport.rawValue
        {
            self.dicWebServiceParam["pickup_type"] = "airport" as Any?
            if let _ = self.dicOnlyAirport["pickup"] as? [String:Any]
            {
                if let dicDropOffAddress = self.arrDataSourceHourly[IndexSection.pickType.rawValue][Constants.PickType] as? [String:Any] {
                    param_Dictionary["pickup_location"] = dicDropOffAddress["address"]
                    param_Dictionary["zip_from"] = dicDropOffAddress["postal_code"]
                }
            }
        }
        else if self.strPickType == pickupType.Seaport.rawValue
        {
            if self.dictPickAddess.count != 0 {
                if let addressDetails = self.dictPickAddess["seaport_address"] as? [String:Any]{
                    param_Dictionary["pickup_location"] = addressDetails["address"]
                    param_Dictionary["zip_from"] = addressDetails["postal_code"]
                }
                
            }
        } else if self.strPickType == pickupType.FBO.rawValue {
            
            if self.dictPickAddess.count != 0 {
                let addressDetails = self.dictPickAddess["fbo_details"] as! [String:Any]
                param_Dictionary["pickup_location"] = addressDetails["address"]
                param_Dictionary["zip_from"] = addressDetails["postal_code"]
                
            }
        }
        
        
        if self.strDropOffType == DropOffType.Address.rawValue ||  self.strDropOffType ==  DropOffType.POI.rawValue
        {
            
            // for pickup
            if let dicPickupAddress = self.arrDataSourceHourly[IndexSection.dropType.rawValue][Constants.DropType] as? [String:Any] {
                param_Dictionary["dropoff_location"] = dicPickupAddress["address"]
                param_Dictionary["zip_to"] = dicPickupAddress["postal_code"]
            }
        }
        else if self.strDropOffType == DropOffType.Airport.rawValue
        {
            
            
            if let dicDropOffAddress = self.arrDataSourceHourly[IndexSection.dropType.rawValue][Constants.DropType] as? [String:Any] {
                param_Dictionary["dropoff_location"] = dicDropOffAddress["address"]
                param_Dictionary["zip_to"] = dicDropOffAddress["postal_code"]
            }
            
        }
        else if self.strDropOffType == DropOffType.Seaport.rawValue
        {
            if self.dictDropOffAddess.count != 0 {
                if let addressDetails = self.dictDropOffAddess["seaport_address"] as? [String:Any]{
                    param_Dictionary["dropoff_location"] = addressDetails["address"]
                    param_Dictionary["zip_to"] = addressDetails["postal_code"]
                }
                
            }
        } else if self.strDropOffType == DropOffType.FBO.rawValue
        {
            if self.dictDropOffAddess.count != 0 {
                
                let addressDetails = self.dictDropOffAddess["fbo_details"] as! [String:Any]
                param_Dictionary["dropoff_location"] = addressDetails["address"]
                param_Dictionary["zip_to"] = addressDetails["postal_code"]
                
            }
        }
        self.strEstimateFare = ""
        self.tableViewReservations.reloadSections(IndexSet(integer: IndexSection.vehicle.rawValue), with: UITableViewRowAnimation.automatic)
        appDelegate.showProgressWithText("Calculating Estimated Fare")
        ServiceManager.sharedInstance.dataTaskWithPostRequest("get_fare_quote", dicParameters: param_Dictionary, successBlock: {[weak self] (dicData) -> Void in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success"{
                if let totalAmt = dicData["totalamount"]
                {
                    if String(describing: totalAmt)  == "0"
                    {
                        self?.strEstimateFare = "N/A"
                        let alert = self?.alertView("Alert", message:"No rates are available for this request, please contact your customer support.", delegate:nil, cancelButton:"Ok", otherButons:nil)
                        alert!.show()
                    }
                    else {
                        self?.strEstimateFare = "\(appDelegate.currency) \(totalAmt)"
                    }
                }
                self?.reloadTableViewOnMainQueue()
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
            }, failureBlock: { (error) -> () in
                appDelegate.hideProgress()
                print(error)
                Globals.ToastAlertWithString("Netowrk Error")
        })
    }
    func verifiy() -> Bool{
        
        var flag:Bool = false
        for dict in self.arrDataSourceHourly {
            for (key , value) in dict as [String: Any]{
                if value is String && value as! String == ""{
                    if key == Constants.Stops || key == Constants.DropType || key == Constants.ChildCount || key == Constants.ChildSeatType {
                        continue
                    }
                    else {
                        flag = true
                        break
                    }
                    
                }
            }
            if flag == true{
                break
            }
        }
        return flag
    }
    // MARK: - ======== Webservice call Methods========
    
    func addParamterForPickupAddressWithType() -> Bool
    {
        
        if self.strPickType == pickupType.Address.rawValue ||  self.strPickType == pickupType.POI.rawValue
        {
            self.dicWebServiceParam["pickup_type"] = self.strPickType.lowercased() as Any?
            // for pickup
            if self.dictPickAddess.count != 0  {
                self.dicWebServiceParam["pickup"] = self.dictPickAddess["address"]
                self.dicWebServiceParam["pickup_latitude"] = self.dictPickAddess["latitude"]
                self.dicWebServiceParam["pickup_longitude"] = self.dictPickAddess["longitude"]
                var dicPickup = [String:Any]()
                dicPickup["location"] = self.dictPickAddess["address"]
                dicPickup["address"] = self.dictPickAddess["address"]
                dicPickup["city"] = self.dictPickAddess["locality"] != nil ? self.dictPickAddess["locality"] : (self.dictPickAddess["city"] != nil ? self.dictPickAddess["city"]  : "")

                dicPickup["state"] =  self.dictPickAddess["administrative_area_level_1"] != nil ?  self.dictPickAddess["administrative_area_level_1"] : ( self.dictPickAddess["state"] != nil ?  self.dictPickAddess["state"]  : "")

                dicPickup["country"] = self.dictPickAddess["country"]
                
                dicPickup["zip_from"] = self.dictPickAddess["postal_code"] != nil ? self.dictPickAddess["postal_code"] : (self.dictPickAddess["zip_from"] != nil ? self.dictPickAddess["zip_from"]  : "")

                
                self.dicWebServiceParam["zip_from"] = self.dictPickAddess["postal_code"] != nil ? self.dictPickAddess["postal_code"] : (self.dictPickAddess["zip_from"] != nil ? self.dictPickAddess["zip_from"]  : "")
                
                self.dicWebServiceParam["pickup_info"] = dicPickup as Any?

                
                return true
            }
            else {
                return false
            }
            
        }
        else if self.strPickType == pickupType.Airport.rawValue
        {
            self.dicWebServiceParam["pickup_type"] = "airport" as Any?
            if let dicAirportPickup = self.dicOnlyAirport["pickup"] as? [String:Any]
            {
                self.dicWebServiceParam["pickup_info"] = dicAirportPickup as Any?
                self.dicWebServiceParam["pickup"] =  dicAirportPickup["airport_name"]
                self.dicWebServiceParam["pickup_latitude"] =  dicAirportPickup["airport_lat"]
                self.dicWebServiceParam["pickup_longitude"] = dicAirportPickup["airport_lng"]
                return true
            }else {
                return false
            }
            
        }
        else if self.strPickType == pickupType.Seaport.rawValue
        {
            if self.dictPickAddess.count != 0 {
                self.dicWebServiceParam["pickup_type"] = "seaport" as Any?
                var dicPickup = [String:Any]()
                if let addressDetails = self.dictPickAddess["seaport_address"] as? [String:Any] {
                    self.dicWebServiceParam["pickup"] = addressDetails["address"]
                    self.dicWebServiceParam["pickup_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["pickup_longitude"] = addressDetails["longitude"]
                    dicPickup["arriving_from"] = addressDetails["address"]
                    dicPickup["city"] = addressDetails["locality"]
                    dicPickup["state"] = addressDetails["administrative_area_level_1"]
                    dicPickup["country"] = addressDetails["country"]
                    dicPickup["zip_from"] = addressDetails["postal_code"]
                    self.dicWebServiceParam["zip_from"] = addressDetails["postal_code"]
                }else  if let addressDetails = self.dictPickAddess["seaport_details"] as? [String:Any]{
                    self.dicWebServiceParam["pickup"] = addressDetails["address"]
                    self.dicWebServiceParam["pickup_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["pickup_longitude"] = addressDetails["longitude"]
                    dicPickup["arriving_from"] = "" as Any?
                    dicPickup["city"] = "" as Any?
                    dicPickup["state"] = "" as Any?
                    dicPickup["country"] = "" as Any?
                    dicPickup["zip_from"] = "" as Any?
                    self.dicWebServiceParam["zip_from"] = "" as Any?
                }
                if let addressDetails = self.dictPickAddess["seaport_details"] as? [String:Any]{
                    dicPickup["seaport_id"] = addressDetails["seaport_id"]
                    dicPickup["cruise_id"] = addressDetails["cruise_id"]
                    dicPickup["seaport_inst"] = addressDetails["seport_inst"]
                }

                self.dicWebServiceParam["pickup_info"] = dicPickup as Any?
                return true
            }else {
                return false
            }
        } else if self.strPickType == pickupType.FBO.rawValue {
            
            if self.dictPickAddess.count != 0 {
                
                self.dicWebServiceParam["pickup_type"] = "fbo" as Any?
                 var dicPickup = [String:Any]()
                if let addressDetails = self.dictPickAddess["fbo_address"] as? [String:Any]{
                    self.dicWebServiceParam["pickup"] = addressDetails["address"]
                    self.dicWebServiceParam["pickup_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["pickup_longitude"] = addressDetails["longitude"]
                    dicPickup["address"] = addressDetails["address"]
                    dicPickup["city"] = (addressDetails["city"] == nil) ? "" : addressDetails["city"]
                    dicPickup["state"] = (addressDetails["state"] == nil) ? "" : addressDetails["state"]
                    dicPickup["country"] = (addressDetails["country"] == nil) ? "" : addressDetails["country"]
                    self.dicWebServiceParam["zip_from"] = (addressDetails["zip"] == nil) ? "" : addressDetails["zip"]

                }else if let addressDetails = self.dictPickAddess["fbo_details"] as? [String:Any]{
                    self.dicWebServiceParam["pickup"] = addressDetails["address"]
                    self.dicWebServiceParam["pickup_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["pickup_longitude"] = addressDetails["longitude"]
                }
                if let addressDetails = self.dictPickAddess["fbo_details"] as? [String:Any]{
                    for (keys,value) in addressDetails {
                        dicPickup[keys] = value
                    }
                }
                self.dicWebServiceParam["pickup_info"] = dicPickup as Any?
                return true
            }else {
                return false
            }
        }
        return false
    }
    
    
    //// drop offf
    func dropOffParametersAddressWithType() -> Bool
    {
        
        if self.strDropOffType == DropOffType.Address.rawValue ||  self.strDropOffType ==  DropOffType.POI.rawValue
        {
            self.dicWebServiceParam["drop_type"] = self.strDropOffType.lowercased() as Any?
            // for pickup
            if let dickDropOff = self.arrDataSourceHourly[IndexSection.dropType.rawValue][Constants.DropType] as? [String:Any] {
                self.dicWebServiceParam["dropoff"] = dickDropOff["address"]
                self.dicWebServiceParam["dropoff_latitude"] = dickDropOff["latitude"]
                self.dicWebServiceParam["dropoff_longitude"] = dickDropOff["longitude"]
                var dicPickup = [String:Any]()
                dicPickup["location"] = dickDropOff["address"]
                dicPickup["address"] = dickDropOff["address"]
                dicPickup["city"] = dickDropOff["locality"] != nil ? dickDropOff["locality"] : (dickDropOff["city"] != nil ? dickDropOff["city"]  : "")
                
                dicPickup["state"] = dickDropOff["administrative_area_level_1"] != nil ? dickDropOff["administrative_area_level_1"] : (dickDropOff["state"] != nil ? dickDropOff["state"]  : "")
                
                dicPickup["country"] = dickDropOff["country"]
                dicPickup["zip_to"] = dickDropOff["postal_code"] != nil ? dickDropOff["postal_code"] : (dickDropOff["zip_to"] != nil ? dickDropOff["zip_to"]  : "")
                
                self.dicWebServiceParam["zip_to"] = dickDropOff["postal_code"] != nil ? dickDropOff["postal_code"] : (dickDropOff["zip_to"] != nil ? dickDropOff["zip_to"]  : "")
                
                self.dicWebServiceParam["dropoff_info"] = dicPickup as Any?
                
                return true
            }else {
                return false
            }
            
        }
        else if self.strDropOffType == DropOffType.Airport.rawValue
        {
            self.dicWebServiceParam["drop_type"] = "airport" as Any?
            if let dicAirportPickup = self.dicOnlyAirport["dropoff"] as? [String:Any]
            {
                self.dicWebServiceParam["dropoff_info"] = dicAirportPickup as Any?
                self.dicWebServiceParam["dropoff"] =  dicAirportPickup["airport_name"]
                self.dicWebServiceParam["dropoff_latitude"] =  dicAirportPickup["airport_lat"]
                self.dicWebServiceParam["dropoff_longitude"] = dicAirportPickup["airport_lng"]
                return true
            }else {
                return false
            }
            
        }
        else if self.strDropOffType == DropOffType.Seaport.rawValue
        {
            if self.dictDropOffAddess.count != 0 {
                self.dicWebServiceParam["drop_type"] = "seaport" as Any?
                var dicPickup = [String:Any]()
                if let addressDetails = self.dictDropOffAddess["seaport_address"] as? [String:Any] {
                    self.dicWebServiceParam["dropoff"] = addressDetails["address"]
                    self.dicWebServiceParam["dropoff_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["dropoff_longitude"] = addressDetails["longitude"]
                    dicPickup["arriving_from"] = addressDetails["address"]
                    dicPickup["city"] = addressDetails["locality"]
                    dicPickup["state"] = addressDetails["administrative_area_level_1"]
                    dicPickup["country"] = addressDetails["country"]
                    dicPickup["zip_to"] = addressDetails["postal_code"]
                    self.dicWebServiceParam["zip_to"] = addressDetails["postal_code"]
                }else if let addressDetails = self.dictDropOffAddess["seaport_details"] as? [String:Any]  {
                    self.dicWebServiceParam["dropoff"] = addressDetails["address"]
                    self.dicWebServiceParam["dropoff_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["dropoff_longitude"] = addressDetails["longitude"]
                    dicPickup["arriving_from"] = "" as Any?
                    dicPickup["city"] = "" as Any?
                    dicPickup["state"] = "" as Any?
                    dicPickup["country"] = "" as Any?
                    dicPickup["zip_to"] = "" as Any?
                    self.dicWebServiceParam["zip_to"] = "" as Any?
                }
                if let addressDetails = self.dictDropOffAddess["seaport_details"] as? [String:Any]  {
                    dicPickup["seaport_id"] = addressDetails["seaport_id"]
                    dicPickup["cruise_id"] = addressDetails["cruise_id"]
                    dicPickup["seaport_inst"] = addressDetails["seport_inst"]
                }

                self.dicWebServiceParam["dropoff_info"] = dicPickup as Any?
                return true
            }else {
                return false
            }
        } else if self.strDropOffType == DropOffType.FBO.rawValue
        {
            if self.dictDropOffAddess.count != 0 {
                self.dicWebServiceParam["drop_type"] = "fbo" as Any?
                  var dicPickup = [String:Any]()
                if let addressDetails = self.dictDropOffAddess["fbo_address"] as? [String:Any]
                {
                    self.dicWebServiceParam["dropoff"] = addressDetails["address"]
                    self.dicWebServiceParam["dropoff_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["dropoff_longitude"] = addressDetails["longitude"]
                      self.dicWebServiceParam["zip_from"] = (addressDetails["zip"] == nil) ? "" : addressDetails["zip"]
                    dicPickup["address"] = addressDetails["address"]
                    dicPickup["city"] = (addressDetails["city"] == nil) ? "" : addressDetails["city"]
                    dicPickup["state"] = (addressDetails["state"] == nil) ? "" : addressDetails["state"]
                    dicPickup["country"] = (addressDetails["country"] == nil) ? "" : addressDetails["country"]
                }else if let addressDetails = self.dictDropOffAddess["fbo_details"] as? [String:Any]{
                    self.dicWebServiceParam["dropoff"] = addressDetails["address"]
                    self.dicWebServiceParam["dropoff_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["dropoff_longitude"] = addressDetails["longitude"]
                   
                }
                if let addressDetails = self.dictDropOffAddess["fbo_details"] as? [String:Any]{
                    for (keys,value) in addressDetails {
                        dicPickup[keys] = value
                    }
                }
                self.dicWebServiceParam["dropoff_info"] = dicPickup as Any?
                
                return true
            }else {
                return false
            }
        }
        return false
    }
    // MARK: -======== Submitt button action ========
    
    @IBAction func btnSubmitAction(_ sender: UIButton)
    {
        var strMessage = ""
       
        let pickUp = self.addParamterForPickupAddressWithType()
        self.dropOffParametersAddressWithType()
        if pickUp == false  {
            strMessage += "Pickup Address\n"
        }
        if self.arrDataSourceHourly.count != 0 {
            if appDelegate.networkAvialable == false  {
                Globals.ToastAlertForNetworkConnection()
                return
            }
            //   var param_Dictionary: [String: Any] = [String: Any]()
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            let  urlString = "create_reservation"
            self.dicWebServiceParam["user_id"] = user_ID
            self.dicWebServiceParam["session"] = session
            //self.dicWebServiceParam["status"] = "UNA"
            self.dicWebServiceParam["job_type"] = "INH" as Any?
            // for pickup date and time
            let strDate = self.arrDataSourceHourly[IndexSection.pickUpDate.rawValue][Constants.PickUpDate] as! String
            if strDate.isEmpty == true{
                strMessage += "Date \n"
            }
            else {
                let  dateStrFormated = Globals.sharedInstance.getDateFormatForDateString(strDate)
                let arrDateTime = dateStrFormated.components(separatedBy: CharacterSet.whitespaces)
                self.dicWebServiceParam["pu_date"] = arrDateTime[0]
                self.dicWebServiceParam["pu_time"] = arrDateTime[1]
            }
            if self.dateSelected == nil {
                self.dateSelected = Date()
            }
            let strHours = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).prohibitReservation_before")
            if strHours != nil && String(describing: strHours).compare("0") == false {
                let date = Date()
                let dayHourMinuteSecond: NSCalendar.Unit = [.day, .hour, .minute, .second]
                let components:DateComponents = (Calendar.current as NSCalendar).components(dayHourMinuteSecond, from: date, to: self.dateSelected, options: [])
                if components.day == 0 {
                    if components.hour!+1 <= Int(String(describing: strHours!))! {
                        let alert = UIAlertView(title: "Alert", message:  "Reservations can only be accepted after \(strHours!) hours of current time", delegate: nil, cancelButtonTitle: "Dismiss")
                        alert.show()
                        return
                    }
                }
            }
            ////Stops ===
            if let arrStop = self.arrDataSourceHourly[IndexSection.stops.rawValue][Constants.Stops] as? [Any]{
                self.dicWebServiceParam["stops"] = arrStop as Any?
            }
            /// hours
            if self.arrDataSourceHourly[IndexSection.hourly.rawValue][Constants.Hourly] as! String == "" {
                strMessage += "Add Hours \n"
            }
            // for Service Type
            if let dicCar = self.arrDataSourceHourly[IndexSection.serviceType.rawValue][Constants.Service] as? [String:Any] {
                self.dicWebServiceParam["service_type"] = dicCar["id"]
            }else {
               strMessage += "Service Type \n"
            }
            // for car Vehicle
            if let dicCar = self.arrDataSourceHourly[IndexSection.vehicle.rawValue][Constants.Vehicle] as? [String:Any] {
                self.dicWebServiceParam["primary_car"] = dicCar["id"]
            }else {
                strMessage += "Preferred Vehicle \n"
            }
            // pickup and dropp notes
            
            if let pickupNotes = self.dictPickAddess[Constants.PickNotes]  as? String
            {
                if pickupNotes == "Pickup Notes"{
                    self.dicWebServiceParam["pickup_notes"] = "" as Any?
                }
                else {
                    self.dicWebServiceParam["pickup_notes"] = pickupNotes as Any?
                }
            }
            if let dropoffNotes = self.dictDropOffAddess[Constants.DropNotes]  as? String
            {
                if dropoffNotes == "Drop Off Notes"{
                    self.dicWebServiceParam["dropoff_notes"] = "" as Any?
                }
                else {
                    self.dicWebServiceParam["dropoff_notes"] = dropoffNotes as Any?
                }
            }
            if strPayment.compare("Payment"){
                 strMessage += "Payment Option \n"
            }
            if strMessage.characters.count > 0 {
                let alert = UIAlertView(title: "Please provide following details", message: strMessage, delegate: nil, cancelButtonTitle: "Dismiss")
                alert.show()
                return
            }
            if let val = self.arrDataSourceHourly[IndexSection.hourly.rawValue][Constants.Hourly]{
                let minHr = self.minimumJobHours.toDouble()
                let inputHours = String(describing: val).toDouble()
                if minHr > inputHours{
                    Globals.ToastAlertWithString("Selected hour(s) should be greater or equal to minimum hour(s)")
                    return
                }
                self.arrDataSourceHourly[IndexSection.hourly.rawValue][Constants.Hourly] = val
                self.dicWebServiceParam["hourly_time"] = self.arrDataSourceHourly[IndexSection.hourly.rawValue][Constants.Hourly]
            }
            // for card payment
            var paymentNil = true
            if self.dicCardDetail.count != 0  {
                if let _ = self.dicCardDetail["id"]
                {
                    if  self.dicCardDetail["id"] as! String != ""{
                        self.dicWebServiceParam["credit_card_id"] = self.dicCardDetail["id"]
                        self.dicWebServiceParam["payment_method"] = self.dicCardDetail["payment_method"]
                        paymentNil = false
                    }
                }
            }
            if paymentNil == true {
                self.dicWebServiceParam["credit_card_id"] = "" as Any?
                self.dicWebServiceParam["payment_method"] = self.dicCardDetail["payment_method"]
            }
            ////distancek time
            ////distancek time
            let arrKeeys : [String]? = [String](self.dicDisAndTime.keys)
            if arrKeeys!.contains("duration"){
                let DicTime = self.dicDisAndTime["duration"]! as! [String:Any]
                let Time = (DicTime["text"] as! String)
                self.dicWebServiceParam["time"] = Time as Any?;
                let Dicdistance = self.dicDisAndTime["distance"]! as! [String:Any]
                let distance = (Dicdistance["value"]  as! Float) * appDelegate.distanceMultiplier
                self.dicWebServiceParam["distance"] = distance
                self.dicWebServiceParam["distance_type"] = appDelegate.distanceType
            }
            if self.arrAddedChildSeats.count != 0 {
                self.dicWebServiceParam["child_seat_count"] = self.arrAddedChildSeats as Any?
            }else {
                self.dicWebServiceParam["child_seat_count"] = "0" as Any?
            }
            self.dicWebServiceParam["company_id"] = Constants.CompanyID as Any?
            self.dicWebServiceParam["luggage"] = self.txtLagguge.text as Any?
            self.dicWebServiceParam["is_handicap"] = self.strDisability as Any?
            self.dicWebServiceParam["adult_seat_count"] = self.txtAdult.text as Any?
            self.dicWebServiceParam["reservation_source"] = "mobile" as Any?
            self.dicWebServiceParam["reservation_id"] = strReservationId as Any?
            self.dicWebServiceParam["template_type"] = "hourly" as Any?
            if let passName = self.arrDataSourceHourly[IndexSection.passangerName.rawValue][Constants.PassangerName] as? String{
                self.dicWebServiceParam["passanger_name"] = passName as Any?
            }
            if let passNO = self.arrDataSourceHourly[IndexSection.passangerNumber.rawValue][Constants.PassangerNumber] as? String{
                self.dicWebServiceParam["phone_no"] = passNO as Any?
            }
            appDelegate.showProgressWithText("Please Wait...")
            
            ServiceManager.sharedInstance.dataTaskWithPostRequest(urlString, dicParameters: self.dicWebServiceParam, successBlock: { [weak self] (dicData) -> Void in
                appDelegate.hideProgress()
                if dicData["status"] as! String == "success"{
                    let alert = UIAlertView(title: "Message", message: dicData["message"] as? String, delegate: nil, cancelButtonTitle: "Ok")
                    alert.show()
                     Constants.reservationUpdated = self?.isEditingMode
                    self?.navigationController?.popViewController(animated: true)
                }else {
                    Globals.ToastAlertWithString(dicData["message"] as! String)
                }
                }, failureBlock: { (error) -> () in
                    appDelegate.hideProgress()
                    print(error)
                    Globals.ToastAlertWithString("Netowrk Error")
            })
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    // MARK: -======== Outlet button actions ========
    func backButtonActionMethod(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension PAReservationsHourlyVC : UIPickerViewDelegate, UIPickerViewDataSource {
    // MARK: - =======picker View Data Source and DSelegate ======
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if self.isTxtAdultPicker == false {
            if self.currentSection == IndexSection.serviceType.rawValue {
                return self.arrDataServiceType.count
            }else
                if self.currentSection == IndexSection.vehicle.rawValue {
                    return self.arrDataSourceCarInfo.count
                }else if self.currentSection == IndexSection.childSeatType.rawValue
                {
                    return self.arrChildSeatType.count
                }else if self.currentSection == IndexSection.childSeatCount.rawValue
                {
                    return self.arrChildCount.count
                }else
                    if self.currentSection == IndexSection.hourly.rawValue {
                        return self.arrHoursCount.count
                }
        }else {
            if self.adultLuaggeTag == 80 {
                return self.arrAdults.count
            }else
                if self.adultLuaggeTag == 81 {
                    return self.arrLuggage.count
            }
            return self.arrChildCount.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let viewReturn = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width, height: 80))
        let lblTitle = UILabel(frame: CGRect(x: 0,y: 0,width: self.view.frame.width, height: 80))
        lblTitle.textAlignment = .center
        lblTitle.numberOfLines = 3
        lblTitle.textColor = UIColor.darkGray
        lblTitle.font = Globals.defaultAppFontWithBold(22)
        viewReturn.addSubview(lblTitle)
        if self.isTxtAdultPicker == false {
            if self.currentSection == IndexSection.serviceType.rawValue {
                let dicStops = self.arrDataServiceType[row] as! [String:Any]
                lblTitle.text = dicStops["service_type"] as? String
            }else
                if self.currentSection == IndexSection.vehicle.rawValue {
                    let dicStops = self.arrDataSourceCarInfo[row] as! [String:Any]
                    let title = dicStops["vehicle_type_title"] as! String
                    let adult = dicStops["vehicle_specific_passenger_capacity"] as! String
                    let luggage = dicStops["vehicle_specific_luggage_capacity"] as! String
                    let returnStr = "\(title) \n Adult:\(adult) Luggage:\(luggage)"
                    let attrStr = returnStr.setAttributedStringWithColorSize(title, secondStr: "\n Adult: \(adult) Luggage: \(luggage)", firstColor: UIColor.black, secondColor: UIColor.darkGray, font1: Globals.defaultAppFontWithBold(22), font2: Globals.defaultAppFont(18))
                    lblTitle.attributedText = attrStr
                }else if self.currentSection == IndexSection.childSeatType.rawValue
                {
                    lblTitle.text =   self.arrChildSeatTypeForAPI[row]
                }
                else if self.currentSection == IndexSection.childSeatCount.rawValue
                {
                    lblTitle.text = self.arrChildCount[row]
                }else  if self.currentSection == IndexSection.hourly.rawValue {
                    lblTitle.text = "\(self.arrHoursCount[row]) hours"
            }
        }else {
            if self.activeTextField.tag == 81 {
                strLagguge =  self.arrLuggage[row]
                lblTitle.text = strLagguge
            }else if self.activeTextField.tag == 80 {
                strAdult =  self.arrAdults[row]
                lblTitle.text = strAdult
            }
        }
        return viewReturn
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat
    {
        if self.currentSection == IndexSection.vehicle.rawValue {
            return 80
        }
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.isTxtAdultPicker == false {
            if self.currentSection == IndexSection.serviceType.rawValue {
                self.arrDataSourceHourly[IndexSection.serviceType.rawValue][Constants.Service] = self.arrDataServiceType[row] as! [String:Any] as Any?
                let dicCars = self.arrDataServiceType[row] as! [String:Any]
                self.strServiceType =  (dicCars["service_type"] as? String)!
                self.activeTextField.text = strServiceType
            }else
                if self.currentSection == IndexSection.vehicle.rawValue {
                    self.arrDataSourceHourly[IndexSection.vehicle.rawValue][Constants.Vehicle] = self.arrDataSourceCarInfo[row] as! [String:Any] as Any?
                    let dicCars = self.arrDataSourceCarInfo[row] as! [String:Any]
                    self.activeTextField.text = dicCars["vehicle_type_title"] as? String
                    self.strVehicle =  (dicCars["vehicle_type_title"] as? String)!
                    self.activeTextField.text = dicCars["vehicle_type_title"] as? String
                }else if self.currentSection == IndexSection.childSeatType.rawValue
                {
                    self.activeTextField.text = self.arrChildSeatType[row]
                    self.arrDataSourceHourly[IndexSection.childSeatType.rawValue][Constants.ChildSeatType] = self.arrChildSeatType[row] as Any?
                }else if self.currentSection == IndexSection.childSeatCount.rawValue
                {
                    self.activeTextField.text = self.arrChildCount[row]
                    self.arrDataSourceHourly[IndexSection.childSeatCount.rawValue][Constants.ChildCount] = self.arrChildCount[row] as Any?
                }else  if self.currentSection == IndexSection.hourly.rawValue {
                    self.activeTextField.text = self.arrHoursCount[row]
                    self.arrDataSourceHourly[IndexSection.hourly.rawValue][Constants.Hourly] = self.arrHoursCount[row] as Any?
            }
        }else {
            if self.activeTextField.tag == 81 {
                strLagguge =  self.arrLuggage[row]
                self.activeTextField.text = strLagguge
            }else if self.activeTextField.tag == 80 {
                strAdult =  self.arrAdults[row]
                self.activeTextField.text = strAdult
            }
        }
        
    }
    
}
extension PAReservationsHourlyVC : UITableViewDataSource, UITableViewDelegate {
    // MARK: - ======== table View data Source  ========
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return self.arrDataSourceHourly.count
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        switch section {
        case IndexSection.pickType.rawValue :
            if strPickType != pickupType.None.rawValue {
                return 2
            }
        case IndexSection.dropType.rawValue:
            if strDropOffType != DropOffType.None.rawValue {
                return 2
            }
        case IndexSection.stops.rawValue :
            return self.arrStops.count
            
        case IndexSection.childSeatCount.rawValue :
            return self.arrAddedChildSeats.count
            
        case IndexSection.vehicle.rawValue :
            if strEstimateFare != "" {
                return 1
            }
        default :
            return 0
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : UITableViewCell?
        
        if indexPath.section == IndexSection.pickType.rawValue  || indexPath.section == IndexSection.dropType.rawValue
        {
            if indexPath.row == 0
            {
                let cellIndetifier = "cellRideLaterWithButton"
                let  cellButton = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier) as! cellReservationForButton
                // cellButton.btnCellOptions.layer.borderColor = UIColor.lightGrayColor().CGColor
                //cellButton.btnCellOptions.layer.borderWidth = 1.0
                cellButton.btnCellOptions.titleLabel?.numberOfLines = 4
                
                //cellButton.btnCellOptions.addTarget(self, action: "btnCellOptionAction:", forControlEvents: UIControlEvents.TouchUpInside)
                cellButton.btnCellOptions.tag = indexPath.row
                cellButton.btnCellOptions.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
                cellButton.btnCellOptions.titleEdgeInsets = UIEdgeInsetsMake(0, 13, 0,0)
                var text = ""
                if indexPath.section == IndexSection.pickType.rawValue {
                    switch self.strPickType {
                    case pickupType.Airport.rawValue :
                        if let dic = self.dicOnlyAirport["pickup"] as? [String:Any]{
                            if let strAdres = dic["airport_name"] as? String{
                                text = strAdres
                            }
                        }
                    case pickupType.Address.rawValue , pickupType.POI.rawValue:
                        if let strAdres = self.dictPickAddess["address"] as? String{
                            text = strAdres
                        }
                    case pickupType.FBO.rawValue :
                        if let dic = self.dictPickAddess["fbo_details"] as? [String:Any]{
                            if let strAdres = dic["address"] as? String{
                                text = strAdres
                            } else {
                                if let dic = self.dictPickAddess["fbo_address"] as? [String:Any] {
                                    if let strAdres = dic["address"] as? String{
                                        text = strAdres
                                    }
                                }
                            }
                        }
                    case pickupType.Seaport.rawValue :
                        if let dic = self.dictPickAddess["seaport_address"] as? [String:Any]{
                            if let strAdres = dic["address"] as? String{
                                text = strAdres
                            }
                        }
                    default :
                        break
                    }
                    cellButton.btnCellOptions.setImage(UIImage(named: "pickup"), for: UIControlState())
                }else if indexPath.section == IndexSection.dropType.rawValue {
                    switch self.strDropOffType {
                    case DropOffType.Airport.rawValue :
                        if let dic = self.dicOnlyAirport["dropoff"] as? [String:Any]{
                            if let strAdres = dic["airport_name"] as? String{
                                text = strAdres
                            }
                        }
                    // text = self.dicOnlyAirport["dropoff"]!["airport_name"] as! String
                    case DropOffType.Address.rawValue , DropOffType.POI.rawValue:
                        if let strAdres = self.dictDropOffAddess["address"] as? String{
                            text = strAdres
                        }
                    //text = self.dictDropOffAddess["address"] as! String
                    case DropOffType.FBO.rawValue :
                        if let dic = self.dictDropOffAddess["fbo_details"] as? [String:Any]{
                            if let strAdres = dic["address"] as? String{
                                text = strAdres
                            }else {
                                if let dic = self.dictDropOffAddess["fbo_address"] as? [String:Any] {
                                    if let strAdres = dic["address"] as? String{
                                        text = strAdres
                                    }
                                }
                            }
                        }
                    // text = self.dictDropOffAddess["fbo_details"]!["address"] as! String
                    case DropOffType.Seaport.rawValue :
                        if let dic = self.dictDropOffAddess["seaport_address"] as? [String:Any]{
                            if let strAdres = dic["address"] as? String{
                                text = strAdres
                            }
                        }
                    // text = self.dictDropOffAddess["seaport_address"]!["address"] as! String
                    default :
                        break
                    }
                    cellButton.btnCellOptions.setImage(UIImage(named: "dropoff"), for: UIControlState())
                }
                if text == "" {
                    text = "N/A"
                }
                cellButton.btnCellOptions.setTitle(text, for: UIControlState())
                
                return cellButton
            }else if indexPath.row == 1
            {
                let cellIndetifier = "cellRideLaterWithTextView"
                let cell : UITableViewCell  = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier)! as UITableViewCell
                let textViewNotes = cell.viewWithTag(41) as! UITextView
                cell.contentView.tag = indexPath.section
                textViewNotes.layer.borderColor = UIColor.lightGray.cgColor
                textViewNotes.layer.borderWidth = 1.0
                textViewNotes.delegate = self
                textViewNotes.inputAccessoryView = self.accessoryViewTextView
                
                var text :String! = ""
                
                if indexPath.section == IndexSection.pickType.rawValue {
                    if let text1 = self.dictPickAddess[Constants.PickNotes] {
                        text = String(describing: text1)
                    }
                }else if indexPath.section == IndexSection.dropType.rawValue {
                    if let text2 = self.dictDropOffAddess[Constants.DropNotes] as? String{
                        text = String(text2)
                    }
                }
                textViewNotes.text = text
                return cell
            }
        } else if indexPath.section == IndexSection.stops.rawValue || indexPath.section == IndexSection.vehicle.rawValue
        {
            let  cellIndetifier = "cellRideLaterWithStops"
            //cellCarInfo
            cell   = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier)!
            var lblText : UILabel!
            var viewOuter : UIView!
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
                lblText = cell!.viewWithTag(63) as! UILabel
                viewOuter = cell!.viewWithTag(62)! as UIView
                
            }else {
                lblText = cell!.viewWithTag(63) as! UILabel
                viewOuter = cell!.viewWithTag(62)! as UIView
                
            }
            viewOuter.layer.borderColor = UIColor.lightGray.cgColor
            viewOuter.layer.borderWidth = 1.0
            if indexPath.section == IndexSection.vehicle.rawValue {
                lblText.text = strEstimateFare
                lblText.numberOfLines = 1
                lblText.font = Globals.defaultAppFont(12)
                lblText.textAlignment = .left
            }else {
                let dicStops = self.arrStops[indexPath.row]
                var strStopAddress = ""
                if let strStop = dicStops["location"] as? String {
                    strStopAddress = strStop
                }
                if let strStop = dicStops["stop_name"] as? String {
                    strStopAddress = strStop
                }
                lblText.text = strStopAddress
                lblText.numberOfLines = 4
                lblText.font = Globals.defaultAppFont(12)
                lblText.textAlignment = .center
            }
            
            return cell!
        }else if indexPath.section == IndexSection.childSeatCount.rawValue {
            
            let  cellIndetifier = "cellRideLaterAddedChildSeatCount"
            //cellCarInfo
            cell   = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier)!
            var lblText : UILabel!
            var viewOuter : UIView!
            var btnDelete : UIButton!
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
                
                viewOuter = cell!.viewWithTag(100)! as UIView
                lblText = viewOuter!.viewWithTag(20) as! UILabel
                btnDelete = viewOuter!.viewWithTag(21) as! UIButton
            }else {
                viewOuter = cell!.viewWithTag(100)! as UIView
                lblText = viewOuter!.viewWithTag(20) as! UILabel
                btnDelete = viewOuter!.viewWithTag(21) as! UIButton
            }
            cell!.contentView.tag = indexPath.row
            viewOuter.layer.borderColor = UIColor.lightGray.cgColor
            viewOuter.layer.borderWidth = 1.0
            btnDelete.addTarget(self, action: #selector(PAReservationsHourlyVC.btnDeleteChildSeatCount(_:)), for: .touchUpInside)
            let dicChildSeat = self.arrAddedChildSeats[indexPath.row]
            let name = dicChildSeat["name"] as! String
            let count = dicChildSeat["count"] as! String
            lblText.text = "\(name)   \(count)"
            lblText.numberOfLines = 4
            lblText.font = Globals.defaultAppFont(12)
            lblText.textAlignment = .left
            
            cell?.selectionStyle = .none
            return cell!
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 3 ||  indexPath.section == 4 {
            if indexPath.row == 0 {
                return 60.0
            }
            return 80.0
        }
        return 60.0
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        switch (section){
        case IndexSection.pickType.rawValue, IndexSection.dropType.rawValue :
            return 90
        default :
            return 60
        }
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let arrDicKeys = (self.arrDataSourceHourly[section]).keys
        
        let strKey = arrDicKeys.first!
        let strTitle = self.arrDataSourceHourly[section][strKey] as? String
        
        var  cellIndetifier = String()
        switch (section){
        case IndexSection.pickUpDate.rawValue , IndexSection.passangerName.rawValue, IndexSection.passangerNumber.rawValue, IndexSection.vehicle.rawValue, IndexSection.childSeatType.rawValue, IndexSection.hourly.rawValue, IndexSection.serviceType.rawValue:
            cellIndetifier = "cellRideLaterWithTextField"
            
            let  cellTextField = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier) as! cellReservationForTextField
            cellTextField.txtFieldCell.layer.borderColor = UIColor.lightGray.cgColor
            cellTextField.txtFieldCell.layer.borderWidth = 1.0
            cellTextField.txtFieldCell.placeholder = strKey
            cellTextField.txtFieldCell.tag = section
            cellTextField.txtFieldCell.delegate = self
            cellTextField.txtFieldCell.leftView = UIView(frame: CGRect(x: 0,y: 0,width: 12,height: 0))
            cellTextField.txtFieldCell.leftViewMode = .always
            cellTextField.txtFieldCell.rightView = UIView(frame: CGRect(x: self.view.frame.width - 20,y: 0,width: 12,height: 0))
            cellTextField.txtFieldCell.rightViewMode = .always
            cellTextField.txtFieldCell.setValue(UIColor.darkGray, forKeyPath: "_placeholderLabel.textColor")
            if strKey == Constants.PickUpDate{
                cellTextField.txtFieldCell.text = strTitle!
                cellTextField.txtFieldCell.background = UIImage(named: "txtCalendar")
                cellTextField.txtFieldCell.inputView = self.viewDatePicker
            }
            else if strKey == Constants.Vehicle
            {
                if self.strVehicle != "" {
                    cellTextField.txtFieldCell.text = self.strVehicle
                }else {
                    cellTextField.txtFieldCell.text = strKey
                }
                cellTextField.txtFieldCell.inputView = self.viewPickerView
                
                cellTextField.txtFieldCell.background = UIImage(named: "arrowRightTextField")
            } else if strKey == Constants.Service
            {
                if self.strServiceType != "" {
                    cellTextField.txtFieldCell.text = self.strServiceType
                }else {
                    cellTextField.txtFieldCell.text = strKey
                }
                cellTextField.txtFieldCell.inputView = self.viewPickerView
                
                cellTextField.txtFieldCell.background = UIImage(named: "arrowRightTextField")
            }
            else  if strKey == Constants.PassangerName  {
                if  let val = self.arrDataSourceHourly[1][Constants.PassangerName] as? String{
                    if !val.isEmpty{
                        cellTextField.txtFieldCell.text = val
                    }
                    else {
                        cellTextField.txtFieldCell.text = strTitle!
                    }
                }
                cellTextField.txtFieldCell.background  = nil
            }else  if strKey == Constants.PassangerNumber  {
                if  let val = self.arrDataSourceHourly[2][Constants.PassangerNumber] as? String{
                    if !val.isEmpty{
                        cellTextField.txtFieldCell.text = val
                    }
                    else {
                        cellTextField.txtFieldCell.text = strTitle!
                    }
                }
                cellTextField.txtFieldCell.background  = nil
            }
            else if strKey == Constants.ChildSeatType {
                cellTextField.txtFieldCell.text = strTitle!
                cellTextField.txtFieldCell.background = UIImage(named: "txtFildDownArrow")
                cellTextField.txtFieldCell.inputView = self.viewPickerView
            }else if strKey == Constants.Hourly{
                cellTextField.txtFieldCell.text = strTitle!
                cellTextField.txtFieldCell.keyboardType = .decimalPad
                cellTextField.txtFieldCell.background = UIImage(named: "txtFildDownArrow")
                cellTextField.txtFieldCell.inputAccessoryView = accessoryViewTextView
            }
            else {
                cellTextField.txtFieldCell.text = strTitle!
            }
            cellTextField.translatesAutoresizingMaskIntoConstraints = true
            cellTextField.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 60)
            let view = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height: 60))
            view.addSubview(cellTextField)
            return view
            
        case IndexSection.pickType.rawValue  :
            cellIndetifier = "cellPickUpAddress"
            let  cellPickup = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier) as! CellReservationMutlipleButtonsTBCell
            if self.btnPickupTypeTag != 0 {
                let btn = cellPickup.viewWithTag(self.btnPickupTypeTag) as! UIButton
                btn.isSelected = true
            }
            if strPickType == pickupType.None.rawValue {
                var attrsTitle : NSMutableAttributedString!
                attrsTitle = NSMutableAttributedString(string:"*", attributes: [NSForegroundColorAttributeName:UIColor.red,NSFontAttributeName: Globals.defaultAppFont(14)])
                attrsTitle.append(NSAttributedString(string: " \(cellPickup.lblTitle.text!)"))
                cellPickup.lblTitle.attributedText = attrsTitle
            }
            cellPickup.contentView.tag = section
            cellPickup.translatesAutoresizingMaskIntoConstraints = true
            cellPickup.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 90)
            let view = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height: 90))
            view.addSubview(cellPickup)
            return view
        case IndexSection.dropType.rawValue :
            cellIndetifier = "cellDropOffAddress"
            let  cellDrop = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier) as! CellReservationMutlipleButtonDropOff
            if self.btnDroppOffTypeTag != 0 {
                let btn = cellDrop.viewWithTag(self.btnDroppOffTypeTag) as! UIButton
                btn.isSelected = true
            }
            cellDrop.contentView.tag = section
            
            cellDrop.translatesAutoresizingMaskIntoConstraints = true
            cellDrop.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 90)
            let view = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height: 90))
            view.addSubview(cellDrop)
            return view
        case IndexSection.stops.rawValue, IndexSection.payment.rawValue :
            let viewbtn = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height: 60))
            let btnOption = UIButton(type: .custom)
            btnOption.frame = CGRect(x: 10,y: 0,width: self.view.frame.width - 20,height: 60)
            btnOption.layer.borderWidth = 1.0
            btnOption.layer.borderColor = UIColor.lightGray.cgColor
            btnOption.titleLabel?.numberOfLines = 4
            btnOption.contentHorizontalAlignment = .left
            btnOption.tag = section
            btnOption.backgroundColor = UIColor.white
            if (Constants.iPadScreen == true){
                btnOption.frame = CGRect(x: 20,y: 0,width: self.view.frame.width - 40,height: 60)
                btnOption.imageEdgeInsets = UIEdgeInsetsMake(0, self.view.frame.width - 80, 0, 0)
                btnOption.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0,  0)
            }else {
                btnOption.frame = CGRect(x: 10,y: 0,width: self.view.frame.width - 20,height: 60)
                btnOption.imageEdgeInsets = UIEdgeInsetsMake(0, self.view.frame.width - 47, 0, 0)
                btnOption.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0,  0)
            }
            btnOption.addTarget(self, action: #selector(PAReservationsHourlyVC.btnCellStopsAndPaymentAction(_:)), for: UIControlEvents.touchUpInside)
            if strKey == Constants.Stops {
                btnOption.setImage(UIImage(named: "plusButton"), for: UIControlState())
                let attrsTitle = NSAttributedString(string: "Stops", attributes: [NSForegroundColorAttributeName:UIColor.darkGray, NSFontAttributeName: Globals.defaultAppFont(14)])
                //btnOption.setTitle("Stops", forState: .Normal )
                btnOption.setAttributedTitle(attrsTitle, for: UIControlState())
            }else {
                btnOption.setImage(UIImage(named: "arrowRight"), for: UIControlState())
                var attrsTitle : NSAttributedString!
                if self.strPayment != "Payment" {
                    attrsTitle = NSAttributedString(string: self.strPayment, attributes: [NSForegroundColorAttributeName:UIColor.darkGray,NSFontAttributeName: Globals.defaultAppFont(14)])
                }else {
                    attrsTitle = NSAttributedString(string:"* \(self.strPayment)", attributes: [NSForegroundColorAttributeName:UIColor.red,NSFontAttributeName: Globals.defaultAppFont(14)])
                }
                btnOption.setAttributedTitle(attrsTitle, for: UIControlState())
            }
            btnOption.translatesAutoresizingMaskIntoConstraints = true
            viewbtn.addSubview(btnOption)
            return viewbtn
            //        case   :
            //            print(strTitle, strKey)
            //            let viewbtn = UIView(frame: CGRectMake(0,0,CGRectGetWidth(self.view.frame),60))
            //            let btnOption = UIButton(type: .Custom)
            //            btnOption.frame = CGRectMake(10,0,CGRectGetWidth(self.view.frame) - 20,60)
            //            btnOption.layer.borderWidth = 1.0
            //            btnOption.layer.borderColor = UIColor.lightGrayColor().CGColor
            //            btnOption.titleLabel?.numberOfLines = 4
            //            btnOption.contentHorizontalAlignment = .Left
            //            btnOption.tag = section
            //            btnOption.backgroundColor = UIColor.whiteColor()
            //            btnOption.addTarget(self, action: "btnCellStopsAndPaymentAction:", forControlEvents: UIControlEvents.TouchUpInside)
            //                btnOption.setImage(UIImage(named: "arrowRight"), forState: .Normal)
            //                let attrsTitle = NSAttributedString(string: strTitle!, attributes: [NSForegroundColorAttributeName:UIColor.darkGrayColor(),NSFontAttributeName: Globals.defaultAppFont(14)])
            //                btnOption.setAttributedTitle(attrsTitle, forState: .Normal)
            //
            //            btnOption.imageEdgeInsets = UIEdgeInsetsMake(0, CGRectGetWidth(self.view.frame) - 47, 0, 0)
            //            btnOption.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0,  0)
            //
            //            btnOption.translatesAutoresizingMaskIntoConstraints = true
            //            viewbtn.addSubview(btnOption)
        //            return viewbtn
        case IndexSection.childSeatCount.rawValue :
            let  cellIndetifier = "cellRideLaterChildSeatCount"
            //cellCarInfo
            var cellChild  = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier)
            var txtChildSeatCount : UITextField!
            var btnAddChildSeat : UIButton!
            
            if cellChild == nil {
                cellChild = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
                txtChildSeatCount = cellChild!.viewWithTag(55) as! UITextField
                btnAddChildSeat = cellChild!.viewWithTag(56)! as! UIButton
                
            }else {
                txtChildSeatCount = cellChild!.viewWithTag(55) as! UITextField
                btnAddChildSeat = cellChild!.viewWithTag(56)! as! UIButton
                
            }
            let viewleft = UIView(frame: CGRect(x: 0,y: 0,width: 12,height: 0))
            txtChildSeatCount.leftView = viewleft
            txtChildSeatCount.leftViewMode = .always
            txtChildSeatCount.inputView = self.viewPickerView
            txtChildSeatCount.layer.borderColor = UIColor.lightGray.cgColor
            txtChildSeatCount.layer.borderWidth = 1.0
            txtChildSeatCount.delegate = self
            btnAddChildSeat.layer.borderColor = UIColor.lightGray.cgColor
            btnAddChildSeat.layer.borderWidth = 1.0
            btnAddChildSeat.addTarget(self, action: #selector(PAReservationsHourlyVC.btnAddChildSeatCountAction(_:)), for: .touchUpInside)
            txtChildSeatCount.text = strTitle
            cellChild!.translatesAutoresizingMaskIntoConstraints = true
            cellChild!.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 60)
            let view = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height: 60))
            view.addSubview(cellChild!)
            return view
        case IndexSection.adultLagguge.rawValue:
            return self.viewAdultLuggageTbl
        default :
            return nil
        }
    }
    // MARK:- ====table View Delegate  ===
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if tableView == self.tableViewReservations {
            if indexPath.section == IndexSection.stops.rawValue {
                return true
            }else {
                return false
            }
        }
        return false
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        
        if(indexPath.section == IndexSection.stops.rawValue){
            
            return UITableViewCellEditingStyle.delete
            
        }
        return UITableViewCellEditingStyle.none
        
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            if indexPath.section  == IndexSection.stops.rawValue {
                
                self.arrStops.remove(at: indexPath.row)
                self.arrDataSourceHourly[indexPath.section][Constants.Stops] = self.arrStops as Any?
                self.tableViewReservations.reloadSections(IndexSet(integer: IndexSection.stops.rawValue), with: .automatic)
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
