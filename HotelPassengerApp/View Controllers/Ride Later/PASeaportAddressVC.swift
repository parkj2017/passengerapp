//
//  PASeaportAddressVC.swift
//  PassangerApp
//
//  Created by Netquall on 2/20/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import GoogleMaps

enum searchTypeSeaport : Int{
    case seaportCode = 0
    case seaportName
    case none
}
class PASeaportAddressVC: BaseViewController, UITextFieldDelegate,BackButtonActionProtocol, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    // MARK:-============ Property decalaration ==========
    @IBOutlet weak var txtInstructions: UITextField!
    @IBOutlet var heightOfAllFeilds: NSLayoutConstraint!
    @IBOutlet weak var tblSeaportResults: UITableView!
    var activeSearchType : searchTypeSeaport!
    @IBOutlet  var txtCruiseShip: UITextField!
    @IBOutlet  var txtSeaportCode: UITextField!
    @IBOutlet  var txtSeaportName: UITextField!
    @IBOutlet var bottomSpaceBottomView: NSLayoutConstraint!
    @IBOutlet var bottomViewHeight: NSLayoutConstraint!
    @IBOutlet var bottomViewAddress: PABottomViewAddress!
    var isPickUpAddress: Bool!
    var finishSelectingSeaportAddress : ((_ dicSeaportDetails : [String : Any], _ tag:Int, _ isDone:Bool) -> ())?
    var tagBtnLast:Int!
    lazy var dicSeaportDetail = [String:Any]()
    var activeTextField : UITextField!
    lazy var arrResults = [[String:Any]]()
    var dicDetails:[String:Any]!
    var autoCompleteTimer: Timer!
    var modalAdress = AddressDetails()
    // MARK: - =====view life cycle=====
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.txtSeaportCode.returnKeyType = .Search
        //        self.txtSeaportName.returnKeyType = .Search
        self.heightOfAllFeilds.constant = 50.0
        if Constants.iPadScreen == true {
            self.heightOfAllFeilds.constant = 75.0
        }
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
        if let btnDone = self.view.viewWithTag(737373) as? UIButton {
            btnDone.titleLabel?.font = Globals.defaultAppFontWithBold(16)
            Globals.layoutViewFor(btnDone, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 5)
            self.view.bringSubview(toFront: btnDone)
        }
        self.bottomViewHeight.constant =  90
        self.bottomSpaceBottomView.constant = 60
        self.bottomViewAddress.dateTime.text = "Date & Time: " + modalAdress.strDateTime
        self.bottomViewAddress.pickupAddress.text = "Pick-Up Address: " + modalAdress.PickupAddress
        self.bottomViewAddress.serviceType.text = "No of Passenger(s): " + modalAdress.noOfPax
        self.bottomViewAddress.pickupAddress.numberOfLines = 3
        self.bottomViewAddress.serviceType.font = Globals.defaultAppFont(14)
        self.bottomViewAddress.pickupAddress.font = Globals.defaultAppFont(14)
        self.bottomViewAddress.dateTime.font = Globals.defaultAppFont(14)
        self.view.bringSubview(toFront: self.bottomViewAddress)
        self.bottomViewAddress.backgroundColor = UIColor.white
        if self.isPickUpAddress {
            self.bottomViewAddress.pickupAddress.alpha = 0.0
            self.bottomViewAddress.pickupAddress.text = ""
            self.bottomViewAddress.pickupAddress.isHidden = true 
            self.bottomViewHeight.constant =  70
            self.bottomSpaceBottomView.constant = 55
            self.view.layoutIfNeeded()
            self.updateViewConstraints()
            
        }
        self.tblSeaportResults.layer.borderColor = UIColor.lightGray.cgColor
        self.tblSeaportResults.layer.borderWidth = 1.0
        self.tblSeaportResults.layer.cornerRadius = 10.0
        self.tblSeaportResults.layer.masksToBounds = true
        self.tblSeaportResults.isHidden = true
        let view = UIView(frame: CGRect(x: 0,y: 0,width: 20 ,height: 50))
        self.txtInstructions.layer.borderColor = UIColor.lightGray.cgColor
        self.txtInstructions.layer.borderWidth = 1.0
        self.txtInstructions.leftView = view
        self.txtInstructions.leftViewMode = .always
        self.txtInstructions.tintColor = UIColor.appBlackColor()
        // Do any additional setup after loading the view.
        self.addLeftViewToTextField(self.txtCruiseShip, strImage: "Cruise_Ship")
        self.addLeftViewToTextField(self.txtSeaportCode, strImage: "seaportcore")
        self.addLeftViewToTextField(self.txtSeaportName, strImage: "seaportname")
        if let btn  = self.view.viewWithTag(55) as? UIButton{
            btn.layer.borderColor = UIColor.lightGray.cgColor
            btn.layer.borderWidth = 1.0
            btn.titleLabel?.font = Globals.defaultAppFont(14)
            btn.setImage(nil, for: UIControlState())
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PASeaportAddressVC.tapGestureAction(_:)))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        if dicDetails != nil && dicDetails.count > 0{
            self.dicSeaportDetail["seaport_details"] = dicDetails as Any?
            if let description = dicDetails["description"] as? String {
                self.txtSeaportName.text = (description.characters.count > 0) ? description : "N/A"
            }
            if let code = dicDetails["code"]  {
                self.txtSeaportCode.text = (String(describing: code).characters.count > 0) ? String(describing: code) : "N/A"
            }
            if let cruise_ship_name = dicDetails["cruise_ship_name"] {
                self.txtCruiseShip.text =  (String(describing: cruise_ship_name).characters.count > 0) ? String(describing: cruise_ship_name) : "N/A"
            }
            if let instructions = dicDetails["instructions"] as? String {
                self.txtInstructions.text = "Instructions: \(instructions)"
            }
            else  if let instructions = dicDetails["seport_inst"] as? String {
                self.txtInstructions.text = "Instructions: \(instructions)"
            }
            if let btn =  self.view.viewWithTag(55) as? UIButton{
                if let address = dicDetails["address"] as? String{
                    btn.setTitle(address, for: UIControlState())
                }else {
                    btn.setTitle("N/A", for: UIControlState())
                }
            }
            if let address = dicDetails["address"]{
                let location = self.getAddressLatLongiFromAddressString(String(describing: address))
                self.seaportAddressComonenstWithLocation(location)
            }
        }
    }
    func addLeftViewToTextField(_ txtfield:UITextField, strImage : String) {
        txtfield.background = nil
        var witdhPading : CGFloat = 40.0
        if Constants.iPadScreen == true {
            witdhPading = 50.0
        }
        let image = UIImageView() //searchIcon "Pickup_FBO_Name"
        image.image = UIImage(named: strImage)
        image.frame = CGRect(x: 5, y: 10, width: witdhPading , height: witdhPading - 10)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.layer.masksToBounds = true
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: witdhPading+10 , height: witdhPading+10))
        leftView.backgroundColor = UIColor.white
        leftView.addSubview(image)
        txtfield.leftView = leftView
        txtfield.leftViewMode = .always
        txtfield.autocorrectionType = .no
        txtfield.layer.borderColor = UIColor.lightGray.cgColor
        txtfield.layer.borderWidth = 1.0
         txtfield.font = Globals.defaultAppFont(14)
        txtfield.tintColor = UIColor.appBlackColor()
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool{
        if gestureRecognizer is UITapGestureRecognizer {
            if (String(describing: touch.view!.classForCoder) == "UITableViewCellContentView"){
                return false
            }
        }
        return true
    }
    
    func tapGestureAction(_ tapGesture:UITapGestureRecognizer){
        if (!self.tblSeaportResults.isHidden) {
            self.tblSeaportResults.isHidden = true;
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        Globals.sharedInstance.delegateBack = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - =====text field delegate =====
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        
        var rect = textField.frame
        rect.size.height = 100
        rect.origin.y += 62
        self.tblSeaportResults.translatesAutoresizingMaskIntoConstraints = true
        self.tblSeaportResults.frame = rect

        if textField == txtSeaportCode{
            activeSearchType = searchTypeSeaport.seaportCode
        }
        else if textField == txtSeaportName{
            activeSearchType = searchTypeSeaport.seaportName
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtSeaportCode || textField == txtSeaportName{
            let currentString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if currentString.characters.count != 0{
                self.runScript()
            }
            else {
            }
        }
        return true
    }
    func runScript() {
        if self.autoCompleteTimer != nil{
            self.autoCompleteTimer.invalidate()
        }
        self.autoCompleteTimer = Timer.scheduledTimer(timeInterval: 0.65, target:self, selector: #selector(PASeaportAddressVC.searchAutocompleteLocationsWithSubstring), userInfo:nil , repeats: false)
    }
    
    func searchAutocompleteLocationsWithSubstring() {
        self.arrResults.removeAll()
        self.tblSeaportResults.reloadData()
        if activeSearchType == searchTypeSeaport.seaportCode{
            self.MakeRequestToSearchSeaportDeatils()
        }
        else if activeSearchType == searchTypeSeaport.seaportName{
            self.MakeRequestToSearchSeaportDeatils()
        }
        //self.view.endEditing(true)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if ((textField.text?.isEmpty)!){
            return;
        }
        textField.resignFirstResponder()
          //        if textField == self.txtSeaportCode {
        //            activeSearchType = searchTypeSeaport.SeaportCode
        //            self.MakeRequestToSearchSeaportDeatils()
        //        }else if textField == self.txtSeaportName {
        //            activeSearchType = searchTypeSeaport.SeaportName
        //             self.MakeRequestToSearchSeaportDeatils()
        //        }
    }
    
    
    
    // MARK: - ====== webcall to search seaport ==========
    
    func MakeRequestToSearchSeaportDeatils(){
        
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }
        appDelegate.showProgressWithText("fetching details")
        var param_Dictionary = [String:Any]()
        let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
        let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
        var  urlString = ""
        param_Dictionary["user_id"] = user_ID
        param_Dictionary["session"] = session
        param_Dictionary["company_id"] = Constants.CompanyID

        if activeSearchType == searchTypeSeaport.seaportCode {
            urlString = "getSeaportInformation"
            param_Dictionary["seaport_id"] = self.txtSeaportCode.text as Any?
        }else if activeSearchType == searchTypeSeaport.seaportName {
            urlString = "getSeaportInformation"
            param_Dictionary["seaport_name"] = self.txtSeaportName.text as Any?
        }
        
        ServiceManager.sharedInstance.dataTaskWithPostRequest(urlString, dicParameters: param_Dictionary, successBlock: { [weak self](dicData) -> Void in
            if  dicData["status"] as! String == "success" {
                
                self?.arrResults = dicData["record"] as! [[String:Any]]
                DispatchQueue.main.async(execute: { () -> Void in
                    self?.tblSeaportResults.reloadData()
                    self?.tblSeaportResults.isHidden = false
                    self?.view.bringSubview(toFront: (self?.tblSeaportResults)!)
                    
                })
            }else {
                appDelegate.hideProgress()
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
            appDelegate.hideProgress()
        }) { (error) -> () in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString("Network Error!")
        }
        
    }
    // MARK: - ============ Get Address from Latitude and Longitude ==========
    
    func getAddressLatLongiFromAddressString(_ strAddress: String) -> CLLocation {
        //var latitude: UnsafeMutablePointer
        //var longitude: UnsafeMutablePointer
        var latitude:Double = 0
        var longitude:Double = 0/// Swift 8bit int
        // grab the data and cast it to a Swift Int8
        
        // let esc_addr: String = strAddress.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let esc_addr: String = strAddress.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
        let req = Globals.sharedInstance.getFullApiAddressForAddressString(esc_addr)
        var result : String!
        do {
            result = try String(contentsOf: URL(string: req)!, encoding: String.Encoding.utf8)
        }catch {
            print("json Error-::\(error)")
            
        }
        
        if result  != "" && result  != nil{
            let scanner: Scanner = Scanner(string: result)
            
            if scanner.scanUpTo("\"lat\" :", into: nil) && scanner.scanString("\"lat\" :", into: nil) {
                scanner.scanDouble(&latitude)
                if scanner.scanUpTo("\"lng\" :", into: nil) && scanner.scanString("\"lng\" :", into: nil) {
                    scanner.scanDouble(&longitude)
                }
            }
        }
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    
    // MARK: - ============ Get Address from seaport location ==========
    
    func seaportAddressComonenstWithLocation(_ newLocation:CLLocation){
        weak var weakSelf = self
        GoogleMapAPIServiceManager.sharedInstance.getLocationAddressUsingLatitude(newLocation.coordinate.latitude, longitude: newLocation.coordinate.longitude, withComplitionHandler: { (dicRecievedJSON) -> () in
            
            if dicRecievedJSON["status"] as! String == "OK"{
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    var dicSeaport = Globals.sharedInstance.formatedAddressCompnentsSwift( (dicRecievedJSON ["results"] as! [Any]).first as! [String:Any])
                    if let address = weakSelf?.dicDetails["address"]{
                        dicSeaport["address"] = address
                    }
                    weakSelf?.dicSeaportDetail["seaport_address"] = dicSeaport as Any?

                    if let btn =  weakSelf?.view.viewWithTag(55) as? UIButton{
                        if let address =  (weakSelf?.dicSeaportDetail["seaport_address"]! as! [String:Any])["address"] as? String{
                            DispatchQueue.main.async(execute: {
                                btn.setTitle(address, for: UIControlState())
                            })
                        }
                    }
                    appDelegate.hideProgress()
                })
            }else {
                appDelegate.hideProgress()
                Globals.ToastAlertWithString("Please try again..")
            }
            })
        { (error) -> () in
            appDelegate.hideProgress()
            
        }
    }
    @IBAction func btnAddressSelectionAction(_ sender: UIButton)  {
        
//        let selectAddrsVC = self.storyboard?.instantiateViewController(withIdentifier: "PASelectAddressViewController") as! PASelectAddressViewController
//        selectAddrsVC.tagBtnAddressLast = self.tagBtnLast
//        selectAddrsVC.isPickUpAddress = self.isPickUpAddress
//        selectAddrsVC.finishSelectingDropOffAddress = { [weak self](dicDropOff : [String:Any], tag:Int) -> () in
//            if dicDropOff.count != 0 {
//                self?.dicSeaportDetail["address"] = dicDropOff
//                sender.setTitle((dicDropOff["address"] as! String), for: .normal)
//            }
//        }
//        selectAddrsVC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Seaport Address", imageName: "backArrow")
//        self.navigationController?.pushViewController(selectAddrsVC, animated: true)
        
    }
    // MARK: - ====== UITableView Delegate and data Source ==========
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrResults.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : UITableViewCell?
        let cellIndetifier = "cellSeaportResult"
        cell = self.tblSeaportResults.dequeueReusableCell(withIdentifier: cellIndetifier)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
        }
        let dic = self.arrResults[indexPath.row]
        var strAddCode = ""
        if let code = dic["code"] {
            strAddCode =  " \(code)"
        }
        if let addrss = dic["address"] {
            strAddCode = strAddCode + " \n (\(addrss))"
        }
        
        cell?.textLabel?.text = strAddCode
        cell?.textLabel?.numberOfLines = 5
        cell?.textLabel?.font = Globals.defaultAppFont(12)
        cell?.selectionStyle = .blue
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if self.arrResults.count != 0 {
            
            let dic = self.arrResults[indexPath.row]
            self.dicDetails = dic

            self.dicSeaportDetail["seaport_details"] = dic as Any?
            DispatchQueue.main.async(execute: { [weak self] () -> Void in
                self?.txtSeaportCode.text = String(describing: dic["code"]!)
                self?.txtSeaportName.text = String(describing: dic["description"]!)
                self?.txtInstructions.text = "Instructions: \(String(describing: dic["seport_inst"]!))"
                self?.txtCruiseShip.text =  String(describing: dic["cruise_ship_name"]!)
                })
            let location = self.getAddressLatLongiFromAddressString(String(describing: dic["address"]!))
            self.seaportAddressComonenstWithLocation(location)
            self.tblSeaportResults.isHidden = true
        }
    }
    @IBAction func btnDoneAction(_ sender: UIButton) {
        if txtCruiseShip.text == "" || txtSeaportCode.text == "" || txtSeaportName.text == "" {
            Globals.ToastAlertWithString("Please enter all field")
            return
        }
        self.finishSelectingSeaportAddress!(self.dicSeaportDetail , self.tagBtnLast, true)
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func backButtonActionMethod(_ sender: UIButton) {
        self.dicSeaportDetail.removeAll()
        self.finishSelectingSeaportAddress!(self.dicSeaportDetail, self.tagBtnLast, false)
        self.navigationController?.popViewController(animated: true)
    }
}
