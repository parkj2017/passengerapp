//
//  PASelectServiceTypeVC.swift
//  HotelPassengerApp
//
//  Created by Netquall on 21/04/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit

class PASelectServiceTypeVC: UIViewController , BackButtonActionProtocol{

    @IBOutlet var tblServiceType: UITableView!
    lazy var arrDataServiceType = [[String:Any]]()
    var dicServiceData = [String:Any]()
    var finishServiceSelection : ((_ dicService:[String:Any]?) -> Void)!
    var selectedIndex:IndexPath?
    var isEditingMode = false
    var strServiceType = "Point to Point"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblServiceType.dataSource = self
        self.tblServiceType.delegate = self
         self.tblServiceType.separatorStyle = .none
        
        if let btnDone = self.view.viewWithTag(996633) as? UIButton {
            btnDone.backgroundColor = UIColor.appRideLater_RedColor()
            btnDone.titleLabel?.font = Globals.defaultAppFont(14)
            Globals.layoutViewFor(btnDone, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 3)
        }
        
        if self.isEditingMode == false{
            if let strSevice = strServiceType as? String{
                //self?.arrDataServiceType = arrSevice
                for (index,dicService) in (self.arrDataServiceType.enumerated()) {
                    if let strDicService = dicService["service_type"] as? String {
                        if  strDicService.compare(strSevice) {
                            self.selectedIndex = IndexPath(row: index, section: 0)
                            self.setLeftBarbuttonItemTitle(strDicService)
                            self.tblServiceType.reloadData()
                            break
                        }
                    }
                }
            }
        }
        
        if self.arrDataServiceType.count == 0 {
            Globals.sharedInstance.ToastAlertInstance("No service type found. Please contact your customer support.")
        }
        // Do any additional setup after loading the view.
    }
    func setLeftBarbuttonItemTitle(_ title:String) {
        if title.isEmpty {
            self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Service Type", imageName: "backArrow")
        }
        else{
            self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(title, imageName: "backArrow")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapDoneButtonAction(_ sender: UIButton) {
        if self.selectedIndex != nil {
            if let dicService = self.arrDataServiceType[self.selectedIndex!.row] as? [String:Any]{
                self.finishServiceSelection(dicService)
                self.navigationController?.popViewController(animated: true)
            }
        }
        else{
            Globals.sharedInstance.ToastAlertInstance("Please select service type.")
        }
        
    }
    func backButtonActionMethod(_ sender: UIButton) {
        self.finishServiceSelection(nil)
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PASelectServiceTypeVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.arrDataServiceType.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellService = self.tblServiceType.dequeueReusableCell(withIdentifier: "cellServiceType", for: indexPath) as! cellServiceType
        if let dicService = self.arrDataServiceType[indexPath.row] as? [String:Any] {
            cellService.lblServiceType.text = dicService["service_type"] as? String
        }
        cellService.lblServiceType.textColor = UIColor.gray
        cellService.btnCheckService.isSelected = false
        cellService.lblServiceType.font = Globals.defaultAppFont(17)
        if self.selectedIndex == indexPath {
            cellService.lblServiceType.textColor = UIColor.appRideLater_RedColor()
            cellService.btnCheckService.isSelected = true
        
        }
        cellService.btnCheckService.isMultipleTouchEnabled = true 
        return cellService
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath
        if let dicService = self.arrDataServiceType[indexPath.row] as? [String:Any] {
            self.setLeftBarbuttonItemTitle(dicService["service_type"] as! String)
        }
        self.tblServiceType.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if Constants.iPadScreen {
            return 50
        }
        return 40
    }
}
