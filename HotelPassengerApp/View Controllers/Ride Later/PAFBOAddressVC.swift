//
//  PASeaportAddressVC.swift
//  PassangerApp
//
//  Created by Netquall on 2/20/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import GoogleMaps

class PAFBOAddressVC: BaseViewController, BackButtonActionProtocol, UITextFieldDelegate, UIGestureRecognizerDelegate {
    

     enum SearchType : Int {
        case kSearchTypeAirport
        case kSearchTypeFBO
        case kSearchTypeNone
    }
    // MARK: - ============ Property decalaration ==========
    
    @IBOutlet var contentViewHeight: NSLayoutConstraint!
    @IBOutlet var heightOfAllFeilds: NSLayoutConstraint!
    @IBOutlet var tableSearchResults: UITableView!
    @IBOutlet  var txtTailCode: UITextField!
    @IBOutlet  var txtFBOName: UITextField!
    @IBOutlet  var txtFBOCode: UITextField!
    @IBOutlet weak var verifyView: UIView!
    @IBOutlet weak var txtSearchExistingFBO: UITextField!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet var bottomSpaceBottomView: NSLayoutConstraint!
    @IBOutlet var bottomViewHeight: NSLayoutConstraint!
    @IBOutlet var bottomViewAddress: PABottomViewAddress!
    var arrResults = [[String:Any]]()
    var finishSelectingFBOAddress : ((_ dicFBODetails : [String : Any], _ tag:Int, _ isDone:Bool) -> ())!
    var tagBtnLast:Int! = 44
    var isPickUpAddress: Bool! = false
    var dicFBODetail = [String:Any]()
    var dicDetails :[String:Any]!
    var strDateTime:String?
    var autoCompleteTimer: Timer!
    
    var flightStaus:[String:Any]?
    var departureDetails:[String:Any]?
    var arrivalDetails:[String:Any]?
    
    var flightStatus:[String:Any]! = [String:Any]()
    var strReservationType:String = "toAirPort"
    var searchType:SearchType = SearchType.kSearchTypeNone
    var modalAdress = AddressDetails()
    // MARK: - ====View Life cycle =======
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        self.tableSearchResults.estimatedRowHeight = 20.0
        self.tableSearchResults.rowHeight = UITableViewAutomaticDimension
        self.tableSearchResults.setNeedsLayout()
        self.tableSearchResults.layoutIfNeeded()
        
        self.txtFBOCode.autocorrectionType = .no
        self.txtFBOCode.autocapitalizationType = .allCharacters

        
         self.heightOfAllFeilds.constant = 50.0
        if Constants.iPadScreen == true {
             self.heightOfAllFeilds.constant = 75.0
        }
        self.contentViewHeight.constant = self.heightOfAllFeilds.constant * 5.0 + 20.0
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
        if let btnDone = self.view.viewWithTag(737373) as? UIButton {
            btnDone.titleLabel?.font = Globals.defaultAppFontWithBold(16)
            Globals.layoutViewFor(btnDone, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 5)
               self.view.bringSubview(toFront: btnDone)
        }
        self.bottomViewHeight.constant =  90
        self.bottomSpaceBottomView.constant = 60
     
        self.bottomViewAddress.dateTime.text = "Date & Time:" + modalAdress.strDateTime
        self.bottomViewAddress.pickupAddress.text = "Pick-Up Address: " + modalAdress.PickupAddress
        self.bottomViewAddress.serviceType.text = "No of Passenger(s): " + modalAdress.noOfPax
        self.bottomViewAddress.pickupAddress.numberOfLines = 3
        self.bottomViewAddress.serviceType.font = Globals.defaultAppFont(14)
        self.bottomViewAddress.pickupAddress.font = Globals.defaultAppFont(14)
        self.bottomViewAddress.dateTime.font = Globals.defaultAppFont(14)
        self.view.bringSubview(toFront: self.bottomViewAddress)
        self.bottomViewAddress.backgroundColor = UIColor.white
        if modalAdress.isPickUpAddress {
            self.bottomViewAddress.pickupAddress.alpha = 0.0
            self.bottomViewAddress.pickupAddress.text = ""
            self.bottomViewAddress.pickupAddress.isHidden = true 
            self.bottomViewHeight.constant =  70
            self.bottomSpaceBottomView.constant = 55
            self.view.layoutIfNeeded()
            self.updateViewConstraints()
        }
        self.navigationController?.navigationBar.isHidden = false
        self.addLeftViewToTextField(self.txtSearchExistingFBO, strImage: "FboSearchIcon")
        self.addLeftViewToTextField(self.txtFBOName, strImage: "Pickup_FBO_Name")
        self.addLeftViewToTextField(self.txtTailCode, strImage: "Tail")
        self.addLeftViewToTextField(self.txtFBOCode, strImage: "EnterAirport")
        let btn  = self.view.viewWithTag(30) as! UIButton
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.layer.borderWidth = 1.0
        btn.titleLabel?.font = Globals.defaultAppFont(14)
       // self.txtSearchExistingFBO.returnKeyType = .Search
         self.txtSearchExistingFBO.delegate = self
       // self.txtTailCode.returnKeyType = .Search
        self.tableSearchResults.layer.borderColor = UIColor.lightGray.cgColor
        self.tableSearchResults.layer.borderWidth = 1.0
        self.tableSearchResults.layer.cornerRadius = 10.0
        self.tableSearchResults.layer.masksToBounds = true
        self.tableSearchResults.isHidden = true
        verifyView.layer.borderWidth = 1.0
        verifyView.layer.borderColor = UIColor.lightGray.cgColor
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PAFBOAddressVC.tapGestureAction(_:)))
        tapGesture.delegate = self;
        self.view.addGestureRecognizer(tapGesture)
        if dicDetails != nil && dicDetails.count > 0{
            dicFBODetail["fbo_details"] = dicDetails as Any?
            if let tailNo = dicDetails["tail"] as? String{
                self.txtTailCode.text = (tailNo.count > 0) ? tailNo : "N/A"
            }
            if let fboName = dicDetails["name"] as? String{
                self.txtFBOName.text = (fboName.count > 0) ? fboName : "N/A"
            }
            if let fboCode = dicDetails["code"] as? String{
                self.txtFBOCode.text = (fboCode.count > 0) ? fboCode : "N/A"
            }
            else if let fboCode = dicDetails["airport_code"] as? String{
                self.txtFBOCode.text = (fboCode.count > 0) ? fboCode : "N/A"
            }
            if let btn = self.view.viewWithTag(30) as? UIButton {
                if let fboAddress = dicDetails["address"] as? String{
                    btn.setTitle(String(fboAddress), for: UIControlState())
                }else {
                    btn.setTitle("N/A", for: UIControlState())
                }
            }else {
                btn.setTitle("N/A", for: UIControlState())
            }
            if let fboAddress = dicDetails["address"] as? String{
                let location = self.getAddressLatLongiFromAddressString(String(fboAddress))
                self.FBOAddressComonenstWithLocation(location)
            }
        }
    }
    func tapGestureAction(_ tapGesture:UITapGestureRecognizer){
        if (!self.tableSearchResults.isHidden) {
            self.tableSearchResults.isHidden = true;
        }
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool{
        if gestureRecognizer is UITapGestureRecognizer {
            if (String(describing: touch.view!.classForCoder) == "UITableViewCellContentView"){
                return false
            }
        }
        return true
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addLeftViewToTextField(_ txtfield:UITextField, strImage : String) {
        txtfield.background = nil
        var witdhPading : CGFloat = 40.0
        if Constants.iPadScreen == true {
            witdhPading = 50.0
        }
        let image = UIImageView() //searchIcon "Pickup_FBO_Name"
        image.image = UIImage(named: strImage)
        image.frame = CGRect(x: 5, y: 10, width: witdhPading , height: witdhPading - 10)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.layer.masksToBounds = true
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: witdhPading+10 , height: witdhPading+10))
        leftView.backgroundColor = UIColor.white
        leftView.addSubview(image)
        txtfield.leftView = leftView
        txtfield.leftViewMode = .always
        txtfield.autocorrectionType = .no
        txtfield.layer.borderColor = UIColor.lightGray.cgColor
        txtfield.layer.borderWidth = 1.0
        txtfield.tintColor = UIColor.appBlackColor()
        txtfield.font = Globals.defaultAppFont(14)
    }
    // MARK: - =====text field delegate =====
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        var rect = textField.frame
        rect.size.height = 150
        rect.origin.y += 62
        self.tableSearchResults.translatesAutoresizingMaskIntoConstraints = true
        self.tableSearchResults.frame = rect
        if textField == txtSearchExistingFBO{
            searchType = .kSearchTypeFBO
        }
        else if textField == txtFBOCode{
            searchType = .kSearchTypeAirport
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if ((textField.text?.isEmpty)!){
            return;
        }
        textField.resignFirstResponder()
    }
   func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    if textField == txtFBOCode || textField == txtSearchExistingFBO{
    let currentString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if currentString.characters.count != 0{
            self.runScript()
        }
        else {
        }
        }
        return true
    }
    //NSLog(@"The searcTextField is empty.");
    func runScript() {
        if self.autoCompleteTimer != nil{
        self.autoCompleteTimer.invalidate()
        }
        self.autoCompleteTimer = Timer.scheduledTimer(timeInterval: 0.65, target:self, selector: #selector(PAFBOAddressVC.searchAutocompleteLocationsWithSubstring), userInfo:nil , repeats: false)
    }
    func searchAutocompleteLocationsWithSubstring() {
        self.arrResults.removeAll()
        self.tableSearchResults.reloadData()
        if searchType == SearchType.kSearchTypeFBO{
        self.MakeRequestToSearchFBODeatils()
        }
        else if searchType == SearchType.kSearchTypeAirport{
         self.MakeRequestToSearchFBODeatils()
        }
        //self.view.endEditing(true)
    }
    @IBAction func btnAddressSelectionAction(_ sender: UIButton) {
//        self.view.endEditing(true)
//        let selectAddrsVC = self.storyboard?.instantiateViewController(withIdentifier: "PASelectAddressViewController") as! PASelectAddressViewController
//        selectAddrsVC.tagBtnAddressLast = self.tagBtnLast
//        selectAddrsVC.isPickUpAddress = self.isPickUpAddress
//        selectAddrsVC.finishSelectingDropOffAddress = { [weak self](dicDropOff : [String:Any], tag:Int) -> () in
//            if dicDropOff.count != 0 {
//                self?.dicFBODetail["address"] = dicDropOff
//                sender.setTitle((dicDropOff["address"] as! String), for: .normal)
//            }
//        }
//        selectAddrsVC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("FBO Address", imageName: "backArrow")
//        self.navigationController?.pushViewController(selectAddrsVC, animated: true)
    }
    @IBAction func btnDoneAction(_ sender: UIButton) {
        
        if btnVerify.isSelected == false {
            retrieveFlightDataWithAirportCode()
            return
        }
        if txtFBOName.text == "" && txtTailCode.text == "" {
            Globals.ToastAlertWithString("Please enter all field")
            return
        }
        if self.btnVerify.isSelected == true{
            
            if strReservationType == "toAirPort" &&  self.arrivalDetails != nil {
                //arrival airport
                
                if let arr_ap =  (((self.arrivalDetails?["Airport"]!  as![String:Any])["AirportId"]! as![String:Any])["AirportCode"]! as![String:Any])["text"] as? String{
                    self.dicFBODetail["arr_ap"] = arr_ap
                }
                
                //arrival dateTime
                if self.arrivalDetails?["DateTime"] != nil{
                    if let arrDate = self.arrivalDetails?["DateTime"] as? [String:Any]{
                        self.dicFBODetail["arrival_datetime"] =  "\((arrDate["Date"]! as![String:Any])["text"]!)  \(String(describing: (arrDate["Time"]! as![String:Any])["text"]))"
                    }
                    else if let arrdate = self.arrivalDetails?["DateTime"] as? [[String:Any]]{
                        let dict = arrdate.first!
                        self.dicFBODetail["arrival_datetime"] =  "\((dict["Date"]! as![String:Any])["text"]!)  \(String(describing: (dict["Time"]! as![String:Any])["text"]))"  //String(dict!["Date"]!["text"]) + " " + String(dict!["Time"]!["text"])
                    }
                }
                
            }
            else if  strReservationType != "toAirPort" &&  self.departureDetails != nil {
                if let arr_ap = ( ( ( self.departureDetails?["Airport"]! as![String:Any])["AirportId"]! as![String:Any])["AirportCode"]!as![String:Any]) ["text"] as? String{
                    self.dicFBODetail["arr_ap"] = arr_ap
                }
                //departure dateTime
                if self.departureDetails?["DateTime"] != nil{
                    if let arrDate = self.departureDetails?["DateTime"] as? [String:Any]{
                        self.dicFBODetail["departure_datetime"] = "\((arrDate["Date"]! as![String:Any])["text"]!)  \(String(describing: (arrDate["Time"]! as![String:Any])["text"]))"
                    }
                    else if let arrdate = self.departureDetails?["DateTime"] as? [[String:Any]]{
                        let dict = arrdate.first!
                        self.dicFBODetail["departure_datetime"] = "\((dict["Date"]! as![String:Any])["text"]!)  \(String(describing: (dict["Time"]! as![String:Any])["text"]))"
                    }
                }
                
            }
            
            if self.flightStatus != nil{
                if let dicStatus = self.flightStatus["FlightStatus"] as? [[String:Any]]{
                    self.dicFBODetail["flight_status"] = dicStatus.first?.keys.first as Any?
                }
                
            }
        }
        if let partentID = self.dicFBODetail["id"]{
            self.dicFBODetail["parent_airport_id"] = String(describing: partentID)
        }
        self.dicFBODetail["verify"] = self.btnVerify.isSelected ? "yes" : "no" as Any?
        self.dicFBODetail["aar_dep_status"] = "" as Any?
        self.finishSelectingFBOAddress!(self.dicFBODetail , self.tagBtnLast, true)
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - ====== webcall to search Fbo details==========
    
    func MakeRequestToSearchFBODeatils(){
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }
        self.autoCompleteTimer.invalidate()
      //  if searchType == SearchType.kSearchTypeFBO{
        var param_Dictionary = [String:Any]()
        let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
        let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            param_Dictionary["session"] = session
        param_Dictionary["user_id"] = user_ID
        param_Dictionary["company_id"] = Constants.CompanyID
         var  urlString = ""
          if searchType == SearchType.kSearchTypeFBO{
             urlString = "getFboInformation"
             if self.txtSearchExistingFBO.text?.characters.count == 0{
                return
               }
             param_Dictionary["fbo_name"] = self.txtSearchExistingFBO.text as Any?
          }else if searchType == SearchType.kSearchTypeAirport{
              urlString = "getairportcode"
            if self.txtFBOCode.text?.characters.count == 0{
                return
             }
            param_Dictionary["airportcode"] = self.txtFBOCode.text as Any?
            self.txtSearchExistingFBO.text = ""
         }
        appDelegate.showProgressWithText("fetching details")
        ServiceManager.sharedInstance.dataTaskWithPostRequest(urlString, dicParameters: param_Dictionary, successBlock: { [weak self](dicData) -> Void in
            if  dicData["status"] as! String == "success" {
                self?.arrResults.removeAll(keepingCapacity: true)
                self?.arrResults = dicData["records"] as! [[String:Any]]
                DispatchQueue.main.async(execute: { () -> Void in
                    self?.tableSearchResults.reloadData()
                    self?.tableSearchResults.isHidden = false
                    self?.view.bringSubview(toFront: (self?.tableSearchResults)!)
                })
            }else {
                self?.tableSearchResults.isHidden = true
                appDelegate.hideProgress()
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
            appDelegate.hideProgress()
        }) { (error) -> () in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString("Network Error!")
        }
    }
    // MARK: - ============ Get Address from Latitude and Longitude ==========
    func getAddressLatLongiFromAddressString(_ strAddress: String) -> CLLocation {
        //var latitude: UnsafeMutablePointer
        //var longitude: UnsafeMutablePointer
        var latitude:Double = 0
        var longitude:Double = 0/// Swift 8bit int
        // grab the data and cast it to a Swift Int8
        
        // let esc_addr: String = strAddress.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let esc_addr: String = strAddress.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
        let req = Globals.sharedInstance.getFullApiAddressForAddressString(esc_addr)
        var result : String!
        do {
            result = try String(contentsOf: URL(string: req)!, encoding: String.Encoding.utf8)
        }catch {
            print("json Error-::\(error)")
            
        }
        
        if result  != "" && result  != nil{
            let scanner: Scanner = Scanner(string: result)
            
            if scanner.scanUpTo("\"lat\" :", into: nil) && scanner.scanString("\"lat\" :", into: nil) {
                scanner.scanDouble(&latitude)
                if scanner.scanUpTo("\"lng\" :", into: nil) && scanner.scanString("\"lng\" :", into: nil) {
                    scanner.scanDouble(&longitude)
                }
            }
        }
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    // MARK: - ============ Get FBO Address components from Fbo location ==========
    
    func FBOAddressComonenstWithLocation(_ newLocation:CLLocation){
        weak var weakSelf = self
        GoogleMapAPIServiceManager.sharedInstance.getLocationAddressUsingLatitude(newLocation.coordinate.latitude, longitude: newLocation.coordinate.longitude, withComplitionHandler: { (dicRecievedJSON) -> () in
            
            if dicRecievedJSON["status"] as! String == "OK"{
                DispatchQueue.main.async(execute: { () -> Void in
                    weakSelf?.dicFBODetail["fbo_address"] = Globals.sharedInstance.formatedAddressCompnentsSwift( (dicRecievedJSON ["results"] as! [Any]).first as! [String:Any])
                    appDelegate.hideProgress()
                   // DispatchQueue.main.async(execute: { [weak self] () -> Void in
                        //let btn = self?.view.viewWithTag(30) as! UIButton
                       // if let fboAddress =  (weakSelf?.dicFBODetail["fbo_address"]! as! [String:Any])["address"] as? String{
                          //  btn.setTitle(String(fboAddress), for: UIControlState())
                        //    }
                       // })
                    })
            }else {
                appDelegate.hideProgress()
                Globals.ToastAlertWithString("Please try again..")
            }
            })
        { (error) -> () in
            appDelegate.hideProgress()
        }
    }
    // MARK: - ============table View Data Source  ==========
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrResults.count
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell
    {
        var cell : UITableViewCell?
        let cellIndetifier = "cellFBOResult"
        cell = self.tableSearchResults.dequeueReusableCell(withIdentifier: cellIndetifier)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
        }
        if searchType == SearchType.kSearchTypeFBO{
        let dic = self.arrResults[indexPath.row]
        cell?.textLabel?.text = String(describing: dic["address"]!)
        }
        else if searchType == SearchType.kSearchTypeAirport{
        let dic = self.arrResults[indexPath.row]
        cell?.textLabel?.text = String(describing: dic["description"]!) + "\n" + String(describing: dic["iata"]!)
        }
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel?.font = Globals.defaultAppFont(13)
        return cell!
    }
    // MARK: -============table View delegate  ==========
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        self.view.endEditing(true)

        if self.arrResults.count != 0 {
            var dic = self.arrResults[indexPath.row]
            var strAddress = ""
            if searchType == SearchType.kSearchTypeFBO{
                DispatchQueue.main.async(execute: { [weak self] () -> Void in
                    
                    if let tailNo = dic["tail"] as? String{
                        self?.txtTailCode.text = (tailNo.count > 0) ? tailNo : " "
                    }
                    if let tailNo = dic["code"] as? String{
                        self?.txtFBOCode.text = (tailNo.count > 0) ? tailNo : " "
                    }
                    if let fboName = dic["name"] as? String{
                        self?.txtFBOName.text = (fboName.count > 0) ? fboName : " "
                    }
                    
                    let btn = self?.view.viewWithTag(30) as! UIButton
                    if let fboAddress = dic["address"] as? String{
                        btn.setTitle(String(fboAddress), for: UIControlState())
                        strAddress = fboAddress
                    }
                    })
                
                var lat = 0.0
                var long = 0.0
                
                
                if let fbo_lat = dic["latitude"]{
                    lat = Double("\(fbo_lat)")!
                }
                if let fbo_long = dic["longitude"]{
                    long = Double("\(fbo_long)")!
                }
                if lat == 0.0 || long == 0.0 {
                    let location = self.getAddressLatLongiFromAddressString(String(strAddress))
                    self.FBOAddressComonenstWithLocation(location)
                }
                else{
                    let locaion = CLLocation(latitude: lat, longitude: long)
                    self.FBOAddressComonenstWithLocation(locaion)
                }
                 self.dicFBODetail["fbo_details"] = dic as Any?
            }
            else if searchType == SearchType.kSearchTypeAirport{
                dic["name"] = dic["description"]
                if let airPortID = dic["id"] as? String{
                    dic["parent_airport_id"] = (airPortID.count > 0) ? airPortID : " " as Any?
                }
                DispatchQueue.main.async(execute: { [weak self] () -> Void in
                    if let fboCode = dic["iata"] as? String{
                        self?.txtFBOCode.text = (fboCode.count > 0) ? fboCode : " "
                    }
                    if let fboName = dic["name"] as? String{
                        self?.txtFBOName.text = (fboName.count > 0) ? fboName : " "
                    }
                    
                    let btn = self?.view.viewWithTag(30) as! UIButton
                    if let fboAddress = dic["description"] as? String{
                        let strCountry = dic["country"] as? String
                        btn.setTitle(String(fboAddress) + strCountry!, for: UIControlState())
                    }
                    })
            

            if let fboAddress = dic["description"] as? String{
                let strCountry = dic["country"] as? String
                strAddress =  fboAddress + (strCountry == nil  ? "" : " \(strCountry!)")
            }
            else if let fboAddress = dic["address"] as? String{
                strAddress =  fboAddress
            }
            else {
                strAddress = self.txtFBOName.text!
            }
            
            self.dicFBODetail["fbo_details"] = dic as Any?
            let location = self.getAddressLatLongiFromAddressString(String(strAddress))
            self.FBOAddressComonenstWithLocation(location)
        }
        }

        self.tableSearchResults.isHidden = true
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func backButtonActionMethod(_ sender: UIButton) {
        self.dicFBODetail.removeAll()
        self.finishSelectingFBOAddress!(self.dicFBODetail , self.tagBtnLast, false)
        self.navigationController?.popViewController(animated: true)
    }
    // MARK:- Flight data verification
    @IBAction func btnVerifyClicked(_ sender: UIButton) {
        
        if txtTailCode.text?.isEmpty == true{
            Globals.ToastAlertWithString("Please enter tail number.")
            return
        }
        else if txtFBOCode.text?.isEmpty == true{
            Globals.ToastAlertWithString("Please enter FBO code.")
            return
        }
        else{
//            if sender.tag == 8888 {
//                self.btnVerify.tag = 99999
//            }
            retrieveFlightDataWithAirportCode()
        }
    }
    func retrieveFlightDataWithAirportCode() {
        
        
        if self.btnVerify.isSelected == true{
            return
        }
        
        var strURL:String = ""
        
        if (txtTailCode.text?.isEmpty)! == true{
            self.btnVerify.isSelected = true
            self.btnDoneAction(UIButton())

            return
        }
        if (self.strReservationType == "toAirPort") {
            strURL = String(format:"http://xml.flightview.com/fvLimoAllianceMasterLink/fvxml.exe?a=BLACKBIRDLIMO&b=$theiLimoSB99&acid=%@&depap=%@&depdate=%@",txtTailCode.text!, txtFBOCode.text!, strDateTime!)
        }
        else {
            strURL = String(format:"http://xml.flightview.com/fvLimoAllianceMasterLink/fvxml.exe?a=BLACKBIRDLIMO&b=$theiLimoSB99&acid=%@&arrap=%@&arrdate=%@",txtTailCode.text!, txtFBOCode.text!, strDateTime!)
        }
        

        let url = URL(string: strURL)
        if url == nil{
            Globals.ToastAlertWithString("Not a valid FBO.")
            return
        }
        let urlReq:Foundation.URLRequest = Foundation.URLRequest(url: url!)
        
        appDelegate.showProgressWithText("Please wait...")
        NSURLConnection.sendAsynchronousRequest(urlReq, queue: OperationQueue.main) { (response, data, connectionError) in
            appDelegate.hideProgress()
            if data == nil{
                Globals.ToastAlertWithString("Somthing went wrong. Please try again..")
                return
            }
            let responseString:String?
            do {
                responseString = try String(data: data!, encoding: String.Encoding.utf8)!
            }
            catch{
                responseString = ""
                print(error)
            }
            //let parserError:NSError?
            do {
                if !(responseString?.isEmpty)!{
                    let xmlDict:[String:Any] = try XMLReader.dictionary(forXMLString: responseString) as! [String:Any]
                    
                    if let dicResult = ((xmlDict["FlightViewResults"]! as! [String:Any])["QueryProcessingStamp"]! as! [String:Any])["Result"] as? [String:Any] {
                        
                        if let strResultCode = (dicResult["ResultCode"]! as! [String:Any])["text"] as? String{
                            
                            if (strResultCode != "0") {
                                DispatchQueue.main.async(execute: {[weak self]() -> Void in
                                    if let msg = (dicResult["ResultMessage"]! as! [String:Any])["text"] as? String{
                                    let alert = UIAlertView(title: "", message:msg, delegate: nil, cancelButtonTitle: "Ok")
                                    alert.show()
                                    self?.btnVerify.isSelected = true
                                    self?.view.endEditing(true)
                                        self?.btnDoneAction(UIButton())

                                    }
                                })
                              //  return
                            }
                            
                            if let dicFlightResults = (xmlDict["FlightViewResults"]! as! [String:Any])["Flight"] as? [String:Any]{
                                
                                self.flightStaus = dicFlightResults["FlightStatus"] as! [String : Any]
                                self.departureDetails = dicFlightResults["Departure"]! as! [String : Any]
                                self.arrivalDetails = dicFlightResults["Arrival"]! as! [String : Any]
                                
                                //if self.strReservationType == "toAirPort" {
                                    DispatchQueue.main.async(execute: { () -> Void in
                           
                                if let msg = (dicResult["ResultMessage"]! as! [String:Any])["text"] as? String{
                                    let alert = UIAlertView(title: "", message:msg, delegate: nil, cancelButtonTitle: "Ok")
                                            alert.show()
                                    self.btnVerify.isSelected = true
                                    self.btnDoneAction(UIButton())

                                        }
                                    })
                            
                            }
//                            DispatchQueue.main.async(execute: { [weak self] in
//                                if self?.btnVerify.tag == 99999 {
//                                    self?.btnDoneAction(UIButton())
//                                }
//                                
//                            })
                            
                        }
                        else {
                            DispatchQueue.main.async(execute: { () -> Void in
                            let alert = UIAlertView(title: "", message:"No results found.", delegate: nil, cancelButtonTitle: "Ok")
                            alert.show()

                                self.btnVerify.isSelected = true
                                self.btnDoneAction(UIButton())

                            })
                        }
                    }
                    else{
                        //if responseString is empty
                        DispatchQueue.main.async(execute: { () -> Void in
                            let alert = UIAlertView(title: "", message:"Something went wrong. Please check your inputs and try again.", delegate: nil, cancelButtonTitle: "Ok")
                            alert.show()
                            self.btnVerify.isSelected = true
                            self.btnDoneAction(UIButton())

                        })
                    }
                }
            }
            catch{
                print(error)
            }
        }
    }
    
}
