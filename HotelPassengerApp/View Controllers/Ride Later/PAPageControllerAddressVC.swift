//
//  pageController.swift
//  UIPageViewController Post
//
//  Created by Jeffrey Burt on 12/11/15.
//  Copyright © 2015 Atomic Object. All rights reserved.
//

import UIKit


class PAPageControllerAddressVC: UIPageViewController {
    
    weak var pagDelegate: PAPageViewControllerDelegate?
    var addressModal = AddressDetails()
    var finishSelectionAddress :((_ dicAddress:[String:Any], _ modal:AddressDetails, _ isDone : Bool) -> Void)!
    var addressSelection : ((_ isHide:Bool)-> Void)!
    
    
    var selectAddrsVC:SearchPlacesAddessViewController!
    var airportVC:AirportViewController!
    var FBO_VC:PAFBOAddressVC!
    var SeaportVC:PASeaportAddressVC!
    
  
    
    fileprivate(set) lazy var orderedViewControllers: [UIViewController] = {
        // The view controllers will be shown in this order
        return [self.addressViewControllerInitilaization(),
                self.airportViewControllerInitilaization(),
                self.fboViewControllerInitilaization(),
                self.seaportViewControllerInitilaization()]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // dataSource = self
        //delegate = self
        airportVC = self.storyboard?.instantiateViewController(withIdentifier: "AirportViewController") as! AirportViewController
        selectAddrsVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesAddessViewController") as!SearchPlacesAddessViewController
        FBO_VC = self.storyboard?.instantiateViewController(withIdentifier: "PAFBOAddressVC") as! PAFBOAddressVC
        SeaportVC = self.storyboard?.instantiateViewController(withIdentifier: "PASeaportAddressVC") as! PASeaportAddressVC

        pagDelegate?.tutorialPageViewController(self, didUpdatePageCount: orderedViewControllers.count)
        if self.addressModal.isEditingMode {
            let controller : UIViewController!
            if self.addressModal.isPickUpAddress  {
                switch self.addressModal.strPickupType {
                case pickupType.Address.rawValue, pickupType.POI.rawValue:
                    controller = orderedViewControllers.first!
                case pickupType.Airport.rawValue:
                    controller = orderedViewControllers[1]
                case pickupType.FBO.rawValue:
                    controller = orderedViewControllers[2]
                case pickupType.Seaport.rawValue:
                    controller = orderedViewControllers.last!
                default:
                    controller = orderedViewControllers.first!
                }
            }else {
                switch self.addressModal.strDropOffType {
                case DropOffType.Address.rawValue, DropOffType.POI.rawValue:
                    controller = orderedViewControllers.first!
                case DropOffType.Airport.rawValue:
                    controller = orderedViewControllers[1]
                case DropOffType.FBO.rawValue:
                    controller = orderedViewControllers[2]
                case DropOffType.Seaport.rawValue:
                    controller = orderedViewControllers.last!
                default:
                    controller = orderedViewControllers.first!
                }
            }
            scrollToViewController(controller)
        }else {
            let controller : UIViewController!

            if self.addressModal.isPickUpAddress  {
                switch self.addressModal.strPickupType {
                case pickupType.Address.rawValue, pickupType.POI.rawValue:
                    controller = orderedViewControllers.first!
                case pickupType.Airport.rawValue:
                    controller = orderedViewControllers[1]
                case pickupType.FBO.rawValue:
                    controller = orderedViewControllers[2]
                case pickupType.Seaport.rawValue:
                    controller = orderedViewControllers.last!
                default:
                    controller = orderedViewControllers.first!
                }
            }else {
                switch self.addressModal.strDropOffType {
                case DropOffType.Address.rawValue, DropOffType.POI.rawValue:
                    controller = orderedViewControllers.first!
                case DropOffType.Airport.rawValue:
                    controller = orderedViewControllers[1]
                case DropOffType.FBO.rawValue:
                    controller = orderedViewControllers[2]
                case DropOffType.Seaport.rawValue:
                    controller = orderedViewControllers.last!
                default:
                    controller = orderedViewControllers.first!
                }
            }

           // if let initialViewController = orderedViewControllers.first {
                scrollToViewController(controller)
           // }
        }
        
        
    }
    /**
     Scrolls to the next view controller.
     */
    func scrollToNextViewController() {
        if let visibleViewController = viewControllers?.first,
            let nextViewController = pageViewController(self,
                                                        viewControllerAfter: visibleViewController) {
            scrollToViewController(nextViewController)
        }
    }
    /**
     Scrolls to the view controller at the given index. Automatically calculates
     the direction.
     - parameter newIndex: the new index to scroll to
     */
    func scrollToViewController(index newIndex: Int) {
        if let firstViewController = viewControllers?.first,
            let currentIndex = orderedViewControllers.index(of: firstViewController) {
            let direction: UIPageViewControllerNavigationDirection = newIndex >= currentIndex ? .forward : .reverse
            let nextViewController = orderedViewControllers[newIndex]
            scrollToViewController(nextViewController, direction: direction)
        }
    }
    fileprivate func newColoredViewController(_ color: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "\(color)ViewController")
    }
    

    func addressViewControllerInitilaization() -> UIViewController {
       
        selectAddrsVC?.isPickUpAddress = self.addressModal.isPickUpAddress
        selectAddrsVC?.tagBtnAddressLast = 44
        selectAddrsVC?.modalAdress = self.addressModal

        if self.addressModal.isPickUpAddress {
            if self.addressModal.strPickupType == pickupType.Address.rawValue ||  self.addressModal.strPickupType == pickupType.POI.rawValue {
                selectAddrsVC?.dicDetails = self.addressModal.dicDetails
            }
        }else {
            if self.addressModal.strDropOffType == DropOffType.Address.rawValue || self.addressModal.strDropOffType == DropOffType.POI.rawValue{
                selectAddrsVC?.dicDetails = self.addressModal.dicDetails
            }
        }
        selectAddrsVC?.strTypeOfAddress = self.addressModal.isPickUpAddress == true ? "Pick-up Address" : "Drop-Off Address"
        selectAddrsVC?.finishSelectingAddress = { [weak self](dicDropOff : [String:Any], tag:Int, isDone:Bool) -> () in
            if self?.addressModal.isPickUpAddress == false {
                self?.addressModal.strDropOffType = DropOffType.Address.rawValue
                
            }else {
                self?.addressModal.strPickupType = pickupType.Address.rawValue
                
            }
            self?.finishSelectionAddress(dicDropOff, self!.addressModal, isDone)
        }
        return selectAddrsVC!
    }
    func airportViewControllerInitilaization() -> UIViewController {
        let dateFormatter: DateFormatter = DateFormatter()
     
        let current_Date: Date = self.addressModal.dateSelected//dateFormatter.date(from: self.addressModal.strDateTime)!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let date1  = dateFormatter.string(from: current_Date)
        let arrtime = date1.components(separatedBy: " ")
        airportVC?.strPickupDate = arrtime[1]
        dateFormatter.dateFormat = "yyyy/MM/dd"
        airportVC?.strDateTime =  dateFormatter.string(from: current_Date).replacingOccurrences(of: "/", with: "")
        airportVC?.tagBtnLast = 44
        airportVC?.strReservationType = self.addressModal.strAirportReservationType
        airportVC?.airportInstructions = NSMutableArray(array:self.addressModal.airportInstructions)
        airportVC?.strNewDateTime = self.addressModal.strDateTime
        airportVC?.strNoOfPax = self.addressModal.noOfPax
        airportVC?.strPickAddress = self.addressModal.PickupAddress
        airportVC?.editReservation = self.addressModal.isEditingMode
        if self.addressModal.isPickUpAddress {
            if self.addressModal.strPickupType == pickupType.Airport.rawValue {
                airportVC?.dicDetails = self.addressModal.dicDetails
            }
        }else {
            if self.addressModal.strDropOffType == DropOffType.Airport.rawValue {
                airportVC?.dicDetails = self.addressModal.dicDetails
            }
        }
        airportVC?.finishAirportSelection = { [weak self](dicAirdicAirPortDetails :[AnyHashable: Any]?, tag:Int, isDone:Bool)-> () in
            if self?.addressModal.isPickUpAddress == false {
                self?.addressModal.strDropOffType = DropOffType.Airport.rawValue
            }else {
                self?.addressModal.strPickupType = pickupType.Airport.rawValue
            }
            self?.finishSelectionAddress(dicAirdicAirPortDetails as! [String : Any], self!.addressModal, isDone)
        }
        return airportVC!
    }
    func fboViewControllerInitilaization() -> UIViewController {
        
        let current_Date: Date = self.addressModal.dateSelected//dateFormatter.date(from: self.addressModal.strDateTime)!
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let date1  = dateFormatter.string(from: current_Date)
        let arrtime = date1.components(separatedBy: " ")
        //    FBO_VC.strPickupDate = arrtime[1]
        dateFormatter.dateFormat = "yyyy/MM/dd"
        FBO_VC.strDateTime =  dateFormatter.string(from: current_Date).replacingOccurrences(of: "/", with: "")
        //FBO_VC.strDateTime = self.addressModal.strDateTime.replacingOccurrences(of: "/", with: "")
        let strTypeOfAddress =  self.addressModal.isPickUpAddress == true ? "Pickup FBO" : "Dropoff FBO"
        FBO_VC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(strTypeOfAddress, imageName: "backArrow")
        FBO_VC.strReservationType = self.addressModal.strAirportReservationType
        FBO_VC.modalAdress = self.addressModal
        if self.addressModal.isPickUpAddress {
            if self.addressModal.strPickupType == pickupType.FBO.rawValue {
                FBO_VC.dicDetails = self.addressModal.dicDetails
            }
        }else {
            if self.addressModal.strDropOffType == DropOffType.FBO.rawValue {
                FBO_VC.dicDetails = self.addressModal.dicDetails
            }
        }
        FBO_VC.finishSelectingFBOAddress = { [weak self](dicFBODetails :[String:Any]!, tag:Int, isDone:Bool)-> () in
            
            if self?.addressModal.isPickUpAddress == false {
                self?.addressModal.strDropOffType = DropOffType.FBO.rawValue
            }else {
                self?.addressModal.strPickupType = pickupType.FBO.rawValue
                
            }
            self?.finishSelectionAddress(dicFBODetails, self!.addressModal, isDone)
        }
        return FBO_VC
    }
    func seaportViewControllerInitilaization() -> UIViewController {
      
        var strTypeOfAddress =  self.addressModal.isPickUpAddress == true ? "Pickup Seaport" : "Dropoff Seaport"
        SeaportVC.tagBtnLast = 55
        SeaportVC.isPickUpAddress = self.addressModal.isPickUpAddress
        SeaportVC.modalAdress = self.addressModal
        if self.addressModal.isPickUpAddress {
            if self.addressModal.strPickupType == pickupType.Seaport.rawValue {
                SeaportVC.dicDetails = self.addressModal.dicDetails
            }
        }else {
            if self.addressModal.strDropOffType == DropOffType.Seaport.rawValue {
                SeaportVC.dicDetails = self.addressModal.dicDetails
            }
        }
        SeaportVC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(strTypeOfAddress, imageName: "backArrow")
        SeaportVC.finishSelectingSeaportAddress = { [weak self](dicSeaportDetails :[String:Any]!, tag:NSInteger, isDone:Bool)-> () in
            if self!.addressModal.isPickUpAddress  == true  {
                self?.addressModal.strPickupType = pickupType.Seaport.rawValue
            }else {
                self?.addressModal.strDropOffType = DropOffType.Seaport.rawValue
            }
            
            self?.finishSelectionAddress(dicSeaportDetails, self!.addressModal, isDone)
        }
        return SeaportVC
    }
    /**
     Scrolls to the given 'viewController' page.
     
     - parameter viewController: the view controller to show.
     */
    fileprivate func scrollToViewController(_ viewController: UIViewController,
                                            direction: UIPageViewControllerNavigationDirection = .forward) {
        
        setViewControllers([viewController],
                           direction: direction,
                           animated: true,
                           completion: { (finished) -> Void in
                            // Setting the view controller programmatically does not fire
                            // any delegate methods, so we have to manually notify the
                            // 'tutorialDelegate' of the new index.
                            self.notifyTutorialDelegateOfNewIndex(controller: viewController)
        })
    }
    
    /**
     Notifies '_tutorialDelegate' that the current page index was updated.
     */
    fileprivate func notifyTutorialDelegateOfNewIndex(controller:UIViewController) {
        if let index = orderedViewControllers.index(of: controller) {
            pagDelegate?.tutorialPageViewController(self, didUpdatePageIndex: index)
        }
    }
    
}

// MARK: UIPageViewControllerDataSource

extension PAPageControllerAddressVC: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            return nil //orderedViewControllers.last
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        let orderedViewControllersCount = orderedViewControllers.count
        guard orderedViewControllersCount != previousIndex else {
            return nil//orderedViewControllers.first
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            return nil//orderedViewControllers.first
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
}

extension PAPageControllerAddressVC: UIPageViewControllerDelegate {
    
    
    func pageViewControllerPreferredInterfaceOrientationForPresentation(_ pageViewController: UIPageViewController) -> UIInterfaceOrientation {
        return .portrait
    }
    func pageViewController(_ pageViewController: UIPageViewController, spineLocationFor orientation: UIInterfaceOrientation) -> UIPageViewControllerSpineLocation {
        return .mid
    }
    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        notifyTutorialDelegateOfNewIndex(controller: pageViewController)
    }
    
}

protocol PAPageViewControllerDelegate: class {
    
    /**
     Called when the number of pages is updated.
     
     - parameter pageController: the pageController instance
     - parameter count: the total number of pages.
     */
    func tutorialPageViewController(_ tutorialPageViewController: PAPageControllerAddressVC,
                                    didUpdatePageCount count: Int)
    
    /**
     Called when the current index is updated.
     
     - parameter pageController: the pageController instance
     - parameter index: the index of the currently visible page.
     */
    func tutorialPageViewController(_ tutorialPageViewController: PAPageControllerAddressVC,
                                    didUpdatePageIndex index: Int)
    
}
