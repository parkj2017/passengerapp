//
//  PASelectAddressViewController.swift
//  PassangerApp
//
//  Created by Netquall on 2/1/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader
import GoogleMaps
import GoogleMapsBase
import GooglePlaces
class PASelectAddressViewController: BaseViewController, BackButtonActionProtocol
{
    // MARK:- ============ Property decalaration ==========
    @IBOutlet weak var mapViewSelectAddress: GMSMapView!
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var finishSelectingDropOffAddress : ((_ dicDropOff : [String : Any], _ tag:Int) -> ())?
    lazy var dicSelectedAdddress  = [String:Any]()
    var tagBtnAddressLast : Int! = 0
    var isPickUpAddress: Bool!
     var strTypeOfAddress = ""
    var dicDetails :[String:Any]!
    lazy var locationManagerObjct = LocationManager.sharedInstance
     @IBOutlet weak var mapCenterPinImage: UIImageView!
      var map_CentreCoordinate : CLLocationCoordinate2D?
    var btnRightDone : UIBarButtonItem!
    
    @IBOutlet weak var btnDoneBottom: UIButton!

    // MARK:-   =====view life cycle====
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Back", imageName: "backArrow")

   
        mapViewSelectAddress.delegate = self
        mapViewSelectAddress.settings.myLocationButton = true
        mapViewSelectAddress.isMyLocationEnabled = true
        mapViewSelectAddress.settings.zoomGestures = true
       
        self.navigationController?.navigationBar.isHidden = false

        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        let subView = UIView(frame: CGRect(x: 0, y: 0,width: self.view.frame.size.width, height: 44.0))
        subView.translatesAutoresizingMaskIntoConstraints = true
      //  subView.backgroundColor = UIColor.DefaultGreenColor()
        subView.addSubview((searchController?.searchBar)!)
        self.view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.searchBar.placeholder = "Search Place"
        searchController?.searchBar.setImage(UIImage(named: "address_Black"), for: .search, state: UIControlState())
        searchController?.searchBar.tintColor = UIColor.appThemeColor()
        searchController?.view.backgroundColor = UIColor.lightGray

        searchController?.searchBar.barTintColor = UIColor(patternImage: UIImage(named:"searchBGPattern")!)

        searchController?.searchBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44.0)
        let viewSearchBar = searchController?.searchBar.subviews[0]
        for searchSubViews in viewSearchBar!.subviews {
            if searchSubViews.isKind(of: UITextField.self){
                let textField = searchSubViews as! UITextField
                textField.textColor = UIColor.black
                textField.font = Globals.defaultAppFont(15.0)
                textField.setValue(UIColor.darkGray, forKeyPath: "_placeholderLabel.textColor")
                (((textField.value(forKey: "textInputTraits"))! as Any) as AnyObject).setValue(UIColor.darkGray, forKeyPath: "insertionPointColor")
                //textField.setValue(UIColor.whiteColor(), forKeyPath: "insertionPointColor")
                textField.backgroundColor = UIColor.clear
                textField.tintColor = UIColor.clear
                textField.frame.size.height = 44
                if self.isPickUpAddress == true {
                    textField.text = self.locationManagerObjct.strCurrentLocation
                }else if dicDetails != nil && dicDetails.count != 0 {
                    if let addres = dicDetails["address"]{
                   textField.text = String(describing: addres)
                    }
                }
            }
        }
        
        searchController?.hidesNavigationBarDuringPresentation = true
        //self.navigationController?.navigationBar.hidden = true
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        self.definesPresentationContext = true
        
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(strTypeOfAddress, imageName: "backArrow")
    
        if dicDetails != nil && dicDetails.count != 0 {
            dicDetails = dicDetails.formatDictionaryForNullValues(dicDetails)
            var lati = dicDetails["latitude"]
            var longitude = dicDetails["longitude"]
            if lati is String && longitude is String{
                lati = "\(lati!)".toDouble() as Any?
                longitude = "\(longitude!)".toDouble() as Any?
            }
            if lati != nil && longitude != nil {
                DispatchQueue.main.async(execute: { [weak self] () -> Void in
                    self?.mapViewSelectAddress.clear()
                    let camera = GMSCameraPosition.camera(withLatitude: lati as! Double, longitude:longitude as! Double, zoom: 16.0)
                    self?.mapViewSelectAddress.animate(to: camera)
                    })
            }
        }else {
            DispatchQueue.main.async(execute: { [weak self] () -> Void in
                self?.mapViewSelectAddress.clear()
                let camera = GMSCameraPosition.camera(withLatitude: (LocationManager.sharedInstance.currentLocation!.coordinate.latitude), longitude:(LocationManager.sharedInstance.currentLocation!.coordinate.longitude) , zoom: 16.0)
                self?.mapViewSelectAddress.animate(to: camera)
                })
        }
      
        // Do any additional setup after loading the view.
}
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.translatesAutoresizingMaskIntoConstraints = true
        self.view.layoutIfNeeded()
        Globals.sharedInstance.delegateBack = self
        mapViewSelectAddress.bringSubview(toFront: self.mapCenterPinImage)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.isOpaque = false

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Globals.sharedInstance.delegateBack = nil
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - ===Button done action ====

    @IBAction func btnDoneClickedAction(_ sender: UIButton) {
        if self.dicSelectedAdddress.count != 0 {
            self.finishSelectingDropOffAddress!(self.dicSelectedAdddress,self.tagBtnAddressLast )
        }
        self.navigationController?.popViewController(animated: true)
    }


func backButtonActionMethod(_ sender: UIButton) {
        let dict = [String:Any]()
        self.finishSelectingDropOffAddress!(dict,self.tagBtnAddressLast )
        self.navigationController?.popViewController(animated: true)
    }
}
// Handle the user's selection.
// MARK: - ============ GMSAutocompleteResultsViewControllerDelegate  ===========
extension PASelectAddressViewController: GMSAutocompleteResultsViewControllerDelegate {
    
    /*func resultsController(resultsController: GMSAutocompleteResultsViewController!, didSelectPrediction prediction: GMSAutocompletePrediction!) -> Bool {
    return true
    }*/
func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
        didAutocompleteWith place: GMSPlace)
    {
        searchController?.isActive = false
        self.isPickUpAddress = true
      //  self.mapViewSelectAddress.clear()
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude:place.coordinate.longitude , zoom: 12.0)
        self.mapViewSelectAddress.animate(to: camera)
          self.btnDoneBottom.isEnabled = false
        //appDelegate.showProgressWithText("Getting Address Details...")
        
    }
    
func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        print(error)
    }
}



// MARK: -============ GMSMapViewDelegate===========
extension PASelectAddressViewController: GMSMapViewDelegate {
    
func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    
    self.map_CentreCoordinate = self.mapViewSelectAddress.camera.target
    let newLocation = CLLocation(latitude: (self.map_CentreCoordinate?.latitude)! , longitude: (self.map_CentreCoordinate?.longitude)!)
    if self.isPickUpAddress == true {
        self.MapStopScrollingAndGetAddressOfCurrentLocation(newLocation)
    }
    if self.isPickUpAddress == false {
        self.isPickUpAddress = true
    }
    
    
}
    
func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        // addressLabel.lock()
   // self.mapViewSelectAddress.clear()
   // self.mapCenterPinImage.hidden = false
    self.btnDoneBottom.isEnabled = false

}
    /*
    func mapView(mapView: GMSMapView!, markerInfoContents marker: GMSMarker!) -> UIView! {
    let placeMarker = marker as! PlaceMarker
    /*
    if let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView {
    infoView.nameLabel.text = placeMarker.place.name
    
    if let photo = placeMarker.place.photo {
    infoView.placePhoto.image = photo
    } else {
    infoView.placePhoto.image = UIImage(named: "generic")
    }
    
    return infoView
    } else {
    return nil
    }*/
    }
    */
func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        // mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapView.selectedMarker = nil
        return false
 }
// MARK: -============ get address from selected loaction from map ==========

func MapStopScrollingAndGetAddressOfCurrentLocation(_ newLocation:CLLocation){
    
    GoogleMapAPIServiceManager.sharedInstance.getLocationAddressUsingLatitude(newLocation.coordinate.latitude, longitude: newLocation.coordinate.longitude, withComplitionHandler: { [weak self](dicRecievedJSON) -> () in
        
            if dicRecievedJSON["status"] as! String == "OK"{
                DispatchQueue.main.async(execute: { () -> Void in
                    let viewSearchBar = self?.searchController?.searchBar.subviews[0]
                    if viewSearchBar != nil {
                    for searchTextField in (viewSearchBar?.subviews)!{
                        if searchTextField.isKind(of: UITextField.self){
                            let txtField = searchTextField as! UITextField
                            txtField.text = (dicRecievedJSON["results"]! as! [[String:Any]])[0]["formatted_address"] as? String
                            break
                        }
                    }
                }
                 })
                self?.dicSelectedAdddress = (self?.formatedAddressCompnentsSwift((dicRecievedJSON ["results"] as! [Any]).first as! [String:Any]))!
                appDelegate.hideProgress()
                   self?.btnDoneBottom.isEnabled = true
            }else {
                appDelegate.hideProgress()
                Globals.ToastAlertWithString("Please try again..")
              }
            })
            { (error) -> () in
                 appDelegate.hideProgress()
              //  JLToast.makeText("Please check your network connection", duration: JLToastDelay.ShortDelay).show()
        }
}
 
func formatedAddressCompnentsSwift(_ dicAddress:[String:Any]) -> [String:Any]{
        var dictAddressComp = [String:Any]()
      dictAddressComp["address"] =  dicAddress ["formatted_address"] as? String as Any?
     let dicGeometry = (dicAddress["geometry"]! as! [String:Any])["location"]! as! [String:Any]
   dictAddressComp["placeID"] = dicAddress["place_id"]! as! String as Any?
   dictAddressComp["latitude"] = dicGeometry["lat"] as! Double as Any?
   dictAddressComp["longitude"] = dicGeometry["lng"] as! Double as Any?
        var address: [String] = [String]()
       let arrAddresComponents = (dicAddress["address_components"] as? [[String : Any]])
    for var add_comp in arrAddresComponents!{
        if let _ = add_comp["types"] {
            let types =  (add_comp["types"] as! [String]).first!
            if types.characters.count > 0 {
                if types.compare("street_number")  {
                      dictAddressComp["street_number"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("route")  {
                    dictAddressComp["route"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("neighborhood")  {
                    dictAddressComp["neighborhood"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("sublocality_level_1")  {
                    dictAddressComp["sublocality_level_1"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("sublocality_level_2")  {
                    dictAddressComp["sublocality_level_2"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("sublocality_level_3")  {
                    dictAddressComp["sublocality_level_3"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("locality")  {
                    dictAddressComp["locality"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("administrative_area_level_1")  {
                    dictAddressComp["administrative_area_level_1"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("country")  {
                    dictAddressComp["country"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("postal_code")  {
                    dictAddressComp["postal_code"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }
            }
        }
    }
    return dictAddressComp
}
    
    
}
