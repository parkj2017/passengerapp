//
//  PAConfirmReservationVC.swift
//  HotelPassengerApp
//
//  Created by Netquall on 21/04/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
import GoogleMaps
import  CoreLocation
class PAConfirmReservationVC: UIViewController,EKEventEditViewDelegate,UINavigationControllerDelegate, BackButtonActionProtocol  {

    
    
    @IBOutlet var tblConfirmReservation: UITableView!
    var dicReservation = [String:Any]()
    var strPickType = pickupType.None.rawValue
    var saved_reservaionID = ""
    var dateSelected = Date()
    var viewFontSize : Float = 14
    var strServiceType = "Other"
    var strCardId = "Cash"
    var  strEstimateFare = "N/A"
    var stopsCount = 0
    var vehicleName = "N/A"
    var isEditMode = false 
    var finishEditReservation: ((_ dicReservation:[String:Any]?) -> Void)!
    @IBOutlet weak var topNavView: UIView!
    //Top navigation button actions
    
    @IBOutlet weak var btnScrollDown: UIButton!
    
    @IBAction func btnTopNavButton(_ sender: UIButton) {
        if  sender.tag == 100{
            if self.navigationController != nil {
                for vc: UIViewController in (self.navigationController?.viewControllers)! {
                    if (vc.isKind(of: PAReservationsRideLaterVC.self)){
                       // Constants.reservationUpdated = true
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }
        }else if sender.tag == 200{
            if self.navigationController != nil {
                for vc: UIViewController in (self.navigationController?.viewControllers)! {
                    if (vc.isKind(of: PASelectVehicleVC.self)){
                       // Constants.reservationUpdated = true
                        if finishEditReservation != nil{
                            self.finishEditReservation(self.dicReservation)
                        }
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                  
                }
            }
        }else if sender.tag == 300{
            
        }
    }
    
    @IBAction func btnBottomArrowImageClicked(_ sender: UIButton) {
        
        let endScrolling:CGFloat = self.tblConfirmReservation.contentOffset.y + self.tblConfirmReservation.frame.height
        
        var contentOffset:CGPoint = self.tblConfirmReservation.contentOffset
        contentOffset.y  = contentOffset.y + 150
        
        if  endScrolling >= self.tblConfirmReservation.contentSize.height - 150 {
            self.btnScrollDown.isHidden = true
            self.tblConfirmReservation.setContentOffset(contentOffset, animated: true)
        }else {
            self.btnScrollDown.isHidden = false
            self.tblConfirmReservation.setContentOffset(contentOffset, animated: true)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Confirmed Reservation", imageName: "homeIcon")
        Globals.layoutViewFor(self.btnScrollDown, color: UIColor.lightGray, width: 1.0, cornerRadius: 5)

        if let btnEdit = self.view.viewWithTag(996633) as? UIButton {
            Globals.layoutViewFor(btnEdit, color: UIColor.gray, width: 1.0, cornerRadius: 3)
            btnEdit.titleLabel?.numberOfLines = 5
            btnEdit.titleLabel?.font = Globals.defaultAppFont(14)
        }
        if let btnAddCalendar = self.view.viewWithTag(996644) as? UIButton {
            Globals.layoutViewFor(btnAddCalendar, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 3)
            btnAddCalendar.titleLabel?.numberOfLines = 5
            btnAddCalendar.titleLabel?.font = Globals.defaultAppFont(14)
        }
        if let arrStops = self.dicReservation["stops"] as? [[String:Any]] {
            self.stopsCount = arrStops.count
        }
        
        self.topNavView.layer.shadowColor = UIColor.lightGray.cgColor
        self.topNavView.layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
        self.topNavView.layer.shadowOpacity = 1.0
        
        self.tblConfirmReservation.dataSource = self
        self.tblConfirmReservation.delegate = self
        
        self.tblConfirmReservation.reloadData()
        

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
    }
    
   
    func backButtonActionMethod(_ sender: UIButton) {
        
        if Constants.countyApp == "1"{
            self.finishEditReservation(nil)
            self.navigationController?.popViewController(animated: true)
        }else {
        self.finishEditReservation(nil)
        if self.navigationController != nil {
            for vc: UIViewController in (self.navigationController?.viewControllers)! {
                if (vc.isKind(of: UpcomingReservationsViewController.self)){
                    Constants.reservationUpdated = true
                    self.navigationController?.popToViewController(vc, animated: true)
                }
                if  (vc.isKind(of: PAMainMenuViewController.self)) || (vc.isKind(of: PABookerMainMenuViewController.self)) {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     //MARK:  Add to Calendar Reservation Info
    @IBAction func didTapAddToCalendarAction(_ sender: UIButton) {
        self.addReservationToCalendar()
    }
    
        //MARK:  Edit Reservation Info
    @IBAction func didTapEditReservationAction(_ sender: UIButton) {
        if finishEditReservation != nil{
              self.finishEditReservation(self.dicReservation)
            self.navigationController?.popViewController(animated: true)
        }
    }

    
    
    // MARK: - ======== EKEventEditViewController Delegate   ========
    
    
    func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction){
        
        if action == .canceled{
          self.navigationController?.dismiss(animated: true, completion: nil)
        }
        else if action == .saved{
            Globals.ToastAlertWithString("Reminder Saved")
            
         
            if let event_id = controller.event?.eventIdentifier{
                Globals.sharedInstance.addEventwithEventID(event_id, reservation_id: self.saved_reservaionID)
            }
        }
        
        self.navigationController?.dismiss(animated: true, completion: {
            if self.navigationController != nil {
                for vc: UIViewController in (self.navigationController?.viewControllers)! {
                    if (vc.isKind(of: UpcomingReservationsViewController.self)){
                        Constants.reservationUpdated = true
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                    if  (vc.isKind(of: PAMainMenuViewController.self)) || (vc.isKind(of: PABookerMainMenuViewController.self)) {
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                    
                }
            }
        })
        
       

    }
    
    
    func setLeftBarbuttonItemTitle(_ title:String) {
        if title.isEmpty {
            self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Book For Later", imageName: "")
        }
        else{
            self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(title, imageName: "")
        }
    }
    
    // MARK: - ======== addReservationToCalendar   ========
    
    func addReservationToCalendar(){
  
        switch EKEventStore.authorizationStatus(for: EKEntityType.event) {
        case .authorized:
            self.insertEvent()
        case .denied:
            self.showInfoBannerWithMessage("Calender Access denied")
            print("Access denied")
        case .notDetermined:
            self.authorizationForCalendar()
        default:
            print("Case Default")
        }
    }
    
    
    func authorizationForCalendar() -> Bool {
        
        let appName = Bundle.main.infoDictionary!["CFBundleName"] as! String
        switch EKEventStore.authorizationStatus(for: EKEntityType.event) {
        case .authorized:
            return true
            
        case .denied:
            print("Access denied")
            let alert: UIAlertView = UIAlertView(title: "Calendar Services", message: "Calendar Services is disabled for this application. Please allow form iPhone Settings > \(appName) > Calendars.", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
            return false
            
        case .notDetermined:
            Globals.sharedInstance.eventStore.requestAccess(to: .event, completion: { (granted, erroer) in
                if granted{
                    self.insertEvent()
                }
                else {
                    let alert: UIAlertView = UIAlertView(title: "Calendar Services", message: "Calendar Services is disabled for this application. Please allow form iPhone Settings > \(appName) > Calendars.", delegate: nil, cancelButtonTitle: "Ok")
                    alert.show()
                }
            })
        case .restricted:
            print("Access denied")
            let alert: UIAlertView = UIAlertView(title: "Calendar Services", message: "Calendar Services is disabled for this application. Please allow form iPhone Settings > \(appName) > Calendars.", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
            return false
        default:
            return false
        }
        return false 
    }

    func getHeightFromString(withText  Text: String) -> CGFloat {
        let strAddress: NSString = Text as NSString
        let str : NSString = strAddress as NSString //.//trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) as NSString
        // let str = self.offerDescModel!.description!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let constraint: CGSize =  CGSize(width: ScreenSize.SCREEN_WIDTH - 20, height: 1500.0)
        //cCGSizeMake(CGRectGetWidth(tableView.frame), 1500.0)
        let boundingBox: CGSize = str.boundingRect(with: constraint, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName:Globals.defaultAppFont(17)], context: nil).size
        return CGFloat(boundingBox.height)
    }
    func insertEvent() {
        
        let detailsVc:EKEventEditViewController = EKEventEditViewController()
        detailsVc.isModalInPopover = false
        detailsVc.delegate = self
        detailsVc.editViewDelegate = self
        detailsVc.eventStore = Globals.sharedInstance.eventStore
        
        let event = EKEvent(eventStore: Globals.sharedInstance.eventStore)
        
            event.startDate = self.dateSelected
            event.endDate = self.dateSelected
            event.title = "New Reservation for "
            // event.eventIdentifier = NSString(string: res_id)
            event.calendar = Globals.sharedInstance.eventStore.defaultCalendarForNewEvents
            
            if let pickupInfo = self.dicReservation["pickup"] as? String {
                event.location = pickupInfo
            }
             detailsVc.event = event
            self.navigationController?.present(detailsVc, animated: true, completion: {
            })
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
  
}

extension PAConfirmReservationVC : UITableViewDataSource, UITableViewDelegate {
  
    func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
                    //MARK:  Map and Time Of Job && Fare Information
             let cellMap = self.tblConfirmReservation.dequeueReusableCell(withIdentifier: "cellMapConfirmReservation", for: indexPath) as! cellMapConfirmReservation
             cellMap.selectionStyle = .none
             cellMap.messageStatus.text = "Thank You. Your Reservation has been submitted for approval.(You can cancel this reservation in UPCOMING RESERVATION section.)"
             cellMap.messageStatus.font = Globals.defaultAppFont(12)
             self.saved_reservaionID =  self.saved_reservaionID.replacingOccurrences(of: "-", with: "")
             cellMap.confirmationNumber.text = "Confirmation No#: \(self.saved_reservaionID)"
             cellMap.confirmationNumber.textColor = UIColor.darkGray
             cellMap.jobDuration.text =   self.dicReservation["time"] as? String ?? "N/A"
             cellMap.fare.text =   self.strEstimateFare
             cellMap.fare.font = Globals.defaultAppFont(viewFontSize)
              var strDistance = "N/A"
             if appDelegate.distanceType!.contains("km") {
                if let dis = self.dicReservation["distance_kilometers"] {
                    strDistance = String(describing:dis)
                }
             }else if appDelegate.distanceType!.contains("mile") {
                if let dis = self.dicReservation["distance_miles"] {
                    strDistance = String(describing:dis)
                }
             }
             else {
                if let dis = self.dicReservation["distance"] {
                    strDistance = String(describing:dis)
                }
             }
             cellMap.distance.text = strDistance + " " + appDelegate.distanceUnit
             cellMap.distance.font = Globals.defaultAppFont(viewFontSize)
              cellMap.updateSourceDataOfMap(dicReservation: self.dicReservation)
            return cellMap
        case 1:
              //MARK:  Booking Infomartion
            let cellBooking = self.tblConfirmReservation.dequeueReusableCell(withIdentifier: "cellBookingInfoConfirmReservation", for: indexPath) as! cellBookingInfoConfirmReservation
              cellBooking.selectionStyle = .none
           let viewBookingInfo = cellBooking.viewWithTag(123456)
            if let lblInfo = viewBookingInfo?.viewWithTag(44) as? UILabel {
                    lblInfo.font = Globals.defaultAppFont(14)
                }
           
            cellBooking.pickupDateTime.text  = "Date & Time: \(self.dicReservation["puDateTime"]!)"
            
            cellBooking.serviceType.text = "Service Type: " + self.strServiceType
            var attrsTitle : NSMutableAttributedString!
            attrsTitle = NSMutableAttributedString(string:"Pick-Up Address \n", attributes: [NSForegroundColorAttributeName:UIColor.appRideLater_RedColor(),NSFontAttributeName: Globals.defaultAppFont(12)])
            if let pickupInfo = self.dicReservation["pickup"] as? String {
                attrsTitle.append(NSAttributedString(string: "\(pickupInfo as? String  ?? "N/A")"))
            }
            cellBooking.pickupAddress.setAttributedTitle(attrsTitle, for: .normal)
            let contentSize = cellBooking.pickupAddress.intrinsicContentSize
            var attrsTitleDrop : NSMutableAttributedString!
            attrsTitleDrop = NSMutableAttributedString(string:"Drop-Off Address \n", attributes: [NSForegroundColorAttributeName:UIColor.appRideLater_GreenColor(),NSFontAttributeName: Globals.defaultAppFont(12)])
            if let dropOffInfo = self.dicReservation["dropoff"] as? String {
                attrsTitleDrop.append(NSAttributedString(string: "\(dropOffInfo)".isEmpty ? "N/A" : "\(dropOffInfo)"))
            }
            else {
                attrsTitleDrop.append(NSAttributedString(string: "N/A"))
            }
            cellBooking.dropOffAdress.setAttributedTitle(attrsTitleDrop, for: .normal)
            let contentSizeDrop = cellBooking.dropOffAdress.intrinsicContentSize
            cellBooking.vehicleName.text = "Vehicle Type: " +  self.vehicleName ?? "N/A"
            cellBooking.payment.text = "Payment : " + self.strCardId
            //MARK:  Stops---- Info
            var nameOfStops = ""
            var indexOfStop = 1
            if let arrStops = self.dicReservation["stops"] as? [[String:Any]] {
                self.stopsCount = arrStops.count
                for dicStops in arrStops {
                    nameOfStops += "Stop \(indexOfStop) :  \(dicStops["stop_name"] as? String ?? "N/A")  \n"
                    indexOfStop += 1
                }
                cellBooking.stops.font = Globals.defaultAppFont(viewFontSize)
                cellBooking.stops.text = nameOfStops
                cellBooking.stops.numberOfLines = 10000
                cellBooking.stops.lineBreakMode = .byWordWrapping
                cellBooking.heightOfStops.constant = CGFloat(50 * Int(self.stopsCount))
           
            }else {
                cellBooking.heightOfStops.constant = CGFloat(0.0)
                cellBooking.stops.isHidden = true
                if let imageStops = viewBookingInfo?.viewWithTag(996633) as? UIImageView {
                    imageStops.isHidden = true 
                }
            }
            cellBooking.layoutIfNeeded()
            cellBooking.updateConstraintsIfNeeded()
           
            return cellBooking
        case 2:
            //MARK:  Booking Infomartion
            let cellPax = self.tblConfirmReservation.dequeueReusableCell(withIdentifier: "cellPassengerInfoConfirmReservation", for: indexPath) as! cellPassengerInfoConfirmReservation
            //MARK:  Passenger Info
            if let viewBookingInfo = cellPax.viewWithTag(123456) {
                if let lblInfo = viewBookingInfo.viewWithTag(44) as? UILabel {
                    lblInfo.font = Globals.defaultAppFont(14)
                }
            }
              cellPax.selectionStyle = .none
            cellPax.nameOfPax.text = "Passenger Name :  " + "\(self.dicReservation["passanger_name"] as? String ?? "N?A")"
            cellPax.nameOfPax.font = Globals.defaultAppFont(viewFontSize )
            
            cellPax.numberOfPax.text = "Passenger No :  " + "\(self.dicReservation["phone_no"] as? String ?? "N?A")"
            cellPax.numberOfPax.font = Globals.defaultAppFont(viewFontSize )
            
            cellPax.adultCount.text = "No. Of Passenger(s) :  " + "\(self.dicReservation["adult_seat_count"] as? String ?? "N?A")"
            cellPax.adultCount.font = Globals.defaultAppFont(viewFontSize )
            
            cellPax.laggageOfPax.text = "Luggage :  " + "\(self.dicReservation["luggage"] as? String ?? "N?A")"
            cellPax.laggageOfPax.font = Globals.defaultAppFont(viewFontSize )
            cellPax.laggageOfPax.isHidden = true
            
            var childSeat = ""
            if let arrChildSeat = self.dicReservation["child_seat_count"] as? [[String:Any]] {
                for value in arrChildSeat {
                    if childSeat.isEmpty {
                        childSeat = "\(value["name"]!)(\(value["count"]!))"
                    }else {
                        childSeat += ", \(value["name"]!)(\(value["count"]!))"
                    }
                }
            }
            cellPax.childSeat.text =  childSeat.isEmpty == true ? "Child Seat :  N/A" :  "Child Seat :  " + childSeat
            cellPax.childSeat.font = Globals.defaultAppFont(viewFontSize )
            return cellPax
        default :
            return UITableViewCell()
       
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0 :
            return 300
        case 1:
            var heightPickDropOff  : CGFloat = 0.0
            if let pickupInfo = self.dicReservation["pickup_info"] as? [String:Any] {
              let strAddress  = pickupInfo["address"] as? String ?? "N/A"
                heightPickDropOff = self.getHeightFromString(withText: strAddress)
            }
            if let dropOffInfo = self.dicReservation["dropoff_info"] as? [String:Any] {
                let strAddress  = dropOffInfo["address"] as? String ?? "N/A"
                heightPickDropOff = self.getHeightFromString(withText: strAddress)
            }
            heightPickDropOff = heightPickDropOff < 130 ? 130 : heightPickDropOff
            var heightStops : CGFloat = 0
            if stopsCount != 0 {
                heightStops =  CGFloat(stopsCount * 50)
            }
            return 230 + heightStops + heightPickDropOff
        case 2:
            return 200
        default:
            return 350
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let endScrolling:CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height
        
        
        if endScrolling >= scrollView.contentSize.height - 100 {
            self.btnScrollDown.isHidden = true
            
        }else {
            self.btnScrollDown.isHidden = false
        }
        
    }
    

//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        switch section {
//        case 0:
//            return self.viewMapDetails
//          
//        case 1:
//            let imageView = UIImageView(frame: CGRect(x: 10,y: self.stops.center.y,width: 20,height: 20))
//            imageView.image = UIImage(named: "dollar")
//            self.stops.addSubview(imageView)
//            return self.viewBookingInfo
//        case 2:
//            return self.viewPaxInfo
//        default:
//            return nil
//        }
//    }
}
