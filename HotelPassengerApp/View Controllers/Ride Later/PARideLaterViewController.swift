//
//  PARideLaterViewController.swift
//  PassangerApp
//
//  Created by Netquall on 12/2/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import UIKit

class PARideLaterViewController: BaseViewController, BackButtonActionProtocol {

    
    var strTypeOfReservation = String()
    // MARK: - =====view life cycle=====
    
    override func viewDidLoad() {
        super.viewDidLoad()
     self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Book for later", imageName: "backArrow")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - =====button outlet action methods=====
   
    @IBAction func btnToAirportAction(_ sender: UIButton) {
         self.strTypeOfReservation = "Hourly Reservation"
        self.performSegue(withIdentifier: "toReservationHoury", sender: self)
       // self.performSegueWithIdentifier("toReservation", sender: self)

    }

    @IBAction func btnPointToPointAction(_ sender: Any) {
        self.strTypeOfReservation = "Book For Later"
        self.performSegue(withIdentifier: "toReservation", sender: self)
    }
   
    // MARK: - ==Navigation======

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
         if segue.identifier == "toReservation"{
            let reservAtionVC = segue.destination as! PAReservationsRideLaterVC
            reservAtionVC.strAirport = self.strTypeOfReservation
            reservAtionVC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(strTypeOfReservation, imageName: "backArrow")
         }else if segue.identifier == "toReservationHoury" {
            let reservAtionVC = segue.destination as! PAReservationsHourlyVC
            reservAtionVC.strAirport = self.strTypeOfReservation
            reservAtionVC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(strTypeOfReservation, imageName: "backArrow")
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
func backButtonActionMethod(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
