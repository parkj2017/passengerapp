//
//  PASelectPaymentVC.swift
//  PassangerApp
//
//  Created by Netquall on 2/10/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import AVFoundation
class PASelectPaymentVC: BaseViewController,CardIOPaymentViewControllerDelegate, UIActionSheetDelegate, BackButtonActionProtocol, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
// MARK:-============ Property decalaration ==========
@IBOutlet weak var tablePayments: UITableView!
@IBOutlet weak var searchBar: UISearchBar!
var arrPayments = [[String:Any]]()
var PushBackBlock : ((_ cardID:String , _ PaymentName:String)->())?
var  viewUpdateCardAlert : UIView!
var strCreditCardTypeID : String!
var cardType : String!
lazy var arrFilterDataSearch = [[String:Any]]()
var dicPrefferCards : [String:Any]!
var dicCardDetails : [String:Any] = [String:Any]()
var dicNewCardDetails : [String:Any] = [String:Any]()


var shouldShowSearchResults = false
var strSearch : String!
var profilePayment = false

var fromDuePaymentScreen = false
var scanCardDetails : CardDetails = CardDetails()
var selectedCardDetails : CardDetails = CardDetails()

var pushToRequestView : ((_ dicCardDetails:[String:Any]?, _ cardID:String) -> Void)!
@IBOutlet var popupViewCardType: UIView!
@IBOutlet var tblCardTypes: UITableView!
var arrCreditCardsType = [[String:Any]]()

var cardName : String!
var payment_method : String!

var arrPreferredCards = [[String:Any]]()
    
// MARK: - =====view life cycle=====

override func viewDidLoad() {
    super.viewDidLoad()
    self.userCardDetailFromServerWithTypeOfURl(Constants.CreditCardUrlType.getCards)
    self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Payment", imageName: "backArrow")
    self.searchBar.placeholder = "Search With Existing Card Number"
    self.searchBar.delegate = self
    self.searchBar.tintColor = UIColor.white
    self.searchBar.showsCancelButton = true
    self.searchBar.setImage(UIImage(named: "searchIcon"), for: .search, state: UIControlState())
    let viewSearchBar = self.searchBar.subviews[0]
    for searchSubViews in viewSearchBar.subviews {
        if searchSubViews.isKind(of: UITextField.self){
            let textField = searchSubViews as! UITextField
            textField.textColor = UIColor.white
            textField.font = Globals.defaultAppFont(15.0)
            textField.setValue(UIColor.white, forKeyPath: "_placeholderLabel.textColor")
            (((textField.value(forKey: "textInputTraits"))! as Any) as AnyObject).setValue(UIColor.darkGray, forKeyPath: "insertionPointColor")
            textField.backgroundColor = UIColor.clear
            textField.tintColor = UIColor.white
            textField.frame.size.height = 44
        }
    }
    // Do any additional setup after loading the view.
}
override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    Globals.sharedInstance.delegateBack = self
}
override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
}

//MARK:- ======= table View data source  =============
func numberOfSections(in tableView: UITableView) -> Int {
    if tableView == tblCardTypes{
        return 1
    }
    else {
        return 2
    }
}
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
{
    if tableView == tblCardTypes{
        return self.arrCreditCardsType.count
    }
    else{
        if section == 0 {
            if self.shouldShowSearchResults == true {
                return 0
            }
            return self.arrPreferredCards.count == 0 ? 1 : self.arrPreferredCards.count
            
        }else if section == 1 {
            if self.shouldShowSearchResults == true {
                return self.arrFilterDataSearch.count == 0 ? 1 : self.arrFilterDataSearch.count
            } else
            {
                return self.arrPayments.count
            }
        }
    }
    return 0
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    
    if tableView == tblCardTypes{
        
        let cellCards : UITableViewCell? = self.tblCardTypes.dequeueReusableCell(withIdentifier: "cellCreditCardType")!
        cellCards?.textLabel?.textColor = UIColor.appBlackColor()
        cellCards?.textLabel?.font = Globals.defaultAppFontWithBold(16)
        if let cardDetils = self.arrCreditCardsType[indexPath.row] as? [String:Any]{
            if let name =  cardDetils["name"] as? String{
                cellCards?.textLabel?.text = name
            }
            else {
                cellCards?.textLabel?.text = "No Name"
            }
        }
        return cellCards!
    }
    else {
       // var cell : UITableViewCell?
        let  cellIndetifier = "cellWithPaymentMethods"
        //cellCarInfo
        var cell = self.tablePayments.dequeueReusableCell(withIdentifier: cellIndetifier, for: indexPath) as! cellPaymentProfile
       // cell = self.tablePayments.dequeueReusableCellWithIdentifier(cellIndetifier)!
        
        cell.btnDelete?.isHidden = false
        cell.btnDelete?.setImage(UIImage(named: "delete"), for:UIControlState())
        cell.btnDelete?.setImage(UIImage(named: "delete"), for:UIControlState.selected)
        cell.btnDelete?.tag = indexPath.row
        cell.btnDelete?.addTarget(self, action: #selector(PASelectPaymentVC.btnDeleteClicked(_:)), for: .touchUpInside)
        
        cell.lblTitle.numberOfLines = 4
        cell.lblTitle.font = Globals.defaultAppFontWithBold(16)
        var cardExit = true
        if indexPath.section == 0  {
            cell.lblTitle.textColor = UIColor.gray
           cell.viewOuter.layer.borderColor = UIColor.lightGray.cgColor
            cell.viewOuter.layer.borderWidth = 1.0
            cell.viewOuter.backgroundColor = UIColor.white

            if self.arrPreferredCards.count > 0 {
                cell.btnDelete?.isHidden = false

                    let dicCardDetails = self.arrPreferredCards[indexPath.row]
                    
                    var cardType = dicCardDetails["cc_type"] as? String
                    cardType = cardType == nil ? "" : cardType!

                    if let cardNo = dicCardDetails["card_no"] as? String{
                        if cardNo.count > 0{
                            cell.lblTitle.text = "\(cardType!) \(cardNo)"
                        }
                        else {
                            if let cardNo = dicCardDetails["name"] as? String{
                                cell.lblTitle.text = cardNo
                            }
                        }
                    }
                cardExit = false
            }
            if cardExit == true {
                cell.lblTitle.text = "No Preferred Cards Available"
                cell.btnDelete?.isHidden = true
            }
        } else if indexPath.section == 1{
            cell.viewOuter.layer.borderColor = UIColor.lightGray.cgColor
            cell.viewOuter.layer.borderWidth = 1.0
            if self.shouldShowSearchResults == true {
                
                if self.arrFilterDataSearch.count == 0{
                    cell.lblTitle.text = "No card found".uppercased()
                    cell.btnDelete?.isHidden = true
                    cell.viewOuter.backgroundColor = UIColor.white
                    cell.lblTitle.textColor = UIColor.gray
                }
                else
                {
                cell.lblTitle.textColor = UIColor.white
                cell.viewOuter.backgroundColor = UIColor.appGrayColor()
                let dic = self.arrFilterDataSearch[indexPath.row]
                    cell.btnDelete?.isHidden = false

                var cardType = dic["cc_type"] as? String
                cardType = cardType == nil ? "" : cardType!
                let str = dic["name"] as! String
                cell.lblTitle.text = "\(cardType!) \(str.uppercased())"
                }
            }else {
                cell.btnDelete?.isHidden = true
                let dic = self.arrPayments[indexPath.row]
                let str = dic["name"] as! String
                if str.compare("Direct Bill/Invoice") {
                    cell.lblTitle.text = "Account Payment".uppercased()
                }
                else if str.compare("Card Type") {
                    cell.lblTitle.text = "Add new card".uppercased()
                }
                else {
                    cell.lblTitle.text = str.uppercased()
                }
            }
        }
        return cell
    }
}
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if tableView == tblCardTypes{
        return 50
    }
    else {
        return 75.0
    }
}
func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if tableView == tblCardTypes{
        return 40
    }
    else
    {
        if section == 0 {
            if self.shouldShowSearchResults == true {
                return 0
            }
        }
        return 40.0
    }
}
func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    
    return 0.5
    
}
func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
{
    let headerView = UIView(frame: CGRect(x: 0,y: 0, width: self.view.frame.width,height: 40))
    let lblPreferCards = UILabel(frame: CGRect(x: 15,y: 5, width: self.view.frame.width-30,height: 30))
    lblPreferCards.textAlignment = .left
    
    if tableView == tblCardTypes{
        lblPreferCards.font = Globals.defaultAppFontWithBold(16)
        lblPreferCards.textColor = UIColor.white
        
        lblPreferCards.text = "Select card Type".uppercased()
        headerView.backgroundColor = UIColor.appThemeColor()
        headerView.addSubview(lblPreferCards)
    }
    else {
        lblPreferCards.font = Globals.defaultAppFontWithBold(16)
        lblPreferCards.textColor = UIColor.appBlackColor()
        
        headerView.backgroundColor = UIColor.white
        
        if section == 0 {
            if self.shouldShowSearchResults == true {
                return nil
            }
            lblPreferCards.text = "Preferred Cards".uppercased()
        }else if section == 1 {
            if self.shouldShowSearchResults == true {
                lblPreferCards.text = "Search Result(s)".uppercased()
            }else {
                lblPreferCards.text = "Other Options".uppercased()
            }
        }
        
        headerView.addSubview(lblPreferCards)
    }
    return headerView
}
// MARK:- ====table view delegate Methods ==========
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    //        if self.profilePayment == true {
    //            return
    //        }
    if tableView == tblCardTypes{
        popupViewCardType.isHidden = true
        if let cardDetils = self.arrCreditCardsType[indexPath.row] as? [String:Any]{
            if let name =  cardDetils["name"] as? String{
                self.cardName = name
            }
            else {
                self.cardName = "No Name"
            }
            if let method =  cardDetils["payment_method"]{
                self.payment_method =  "\(method)"
            }
            else {
                self.payment_method = "0"
            }
        }
        scanCard()
    }
    else{
        if indexPath.section == 0
        {
            if  self.shouldShowSearchResults == false && self.profilePayment == false
            {
                if self.arrPreferredCards.count > 0 {
                self.dicCardDetails = self.arrPreferredCards[indexPath.row]
                        
                if (self.dicCardDetails["card_no"] as! String).count == 0{
                        self.pushToRequestView(self.dicCardDetails,"\(self.dicCardDetails["name"] as? String)" )
                }
                else{
                    self.pushToRequestView(self.dicCardDetails,"\(self.dicCardDetails["cc_type"] as! String)  \(self.dicCardDetails["card_no"] as! String)" )
                    }
                        
                DispatchQueue.main.async(execute: { [weak self]() -> Void in
                            self?.navigationController?.popViewController(animated: true)
                            })
                    }
            }
        }
        else if indexPath.section == 1
        {
            if  self.shouldShowSearchResults == false
            {
                if  let cellData = self.arrPayments[indexPath.row] as? [String:Any]
                {
                    if  let cellType = cellData["name"] as? String
                    {
                        if cellType.compare("Card Type")  {
                            
                            if let cardType = cellData["Card Type"] as? [[String:Any]]{
                                self.arrCreditCardsType = cardType
                                tblCardTypes.reloadData()
                            }
                            let actionSheet = UIActionSheet(title: "Choose card type", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Business","Personal")
                            actionSheet.tag = 1000
                            actionSheet.show(in: self.view)
                        }
                        else {
                            //  appDelegate.showProgressWithText("Please Wait...")
                           
                            if self.profilePayment == false {
                                if let _ = self.arrPayments[indexPath.row] as? [String:Any]{
                                    self.dicCardDetails =  self.arrPayments[indexPath.row]
                                    self.pushToRequestView(self.dicCardDetails,"\(self.dicCardDetails["name"]!)")
                                    DispatchQueue.main.async(execute: { [weak self]() -> Void in
                                        self?.navigationController?.popViewController(animated: true)
                                        })
                                }
                            }
                            
                        }
                    }
                }
            }
            else
            {
                if self.arrFilterDataSearch.count > 0 {
                    
                    if let dic = self.arrFilterDataSearch[indexPath.row] as? [String:Any]
                    {
                        self.selectSearchCardForPayment(dic)
                    }
                }
            }
        }
    }
}

func selectSearchCardForPayment(_ dic:[String:Any]) {
    self.dicCardDetails = dic
    if self.profilePayment == true{
        let alert = UIAlertController(title: "Confirmation", message: "Do you want to set \(dic["name"]!) as a preferred card ?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { [weak self] (ACTION :UIAlertAction!) in
            self?.userCardDetailFromServerWithTypeOfURl(Constants.CreditCardUrlType.addPreferredCard)
            }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { [weak self] (ACTION :UIAlertAction!) in
            if self?.profilePayment == false {
                self?.pushToRequestView(dic, dic["name"] as! String)
                self?.navigationController?.popViewController(animated: true)
            }
            }))
        self.present(alert, animated: true, completion: nil)
    }
    else {
        self.pushToRequestView(self.dicCardDetails, "\(self.dicCardDetails["name"] as! String)")
        DispatchQueue.main.async(execute: { [weak self]() -> Void in
            self?.navigationController?.popViewController(animated: true)
            })
    }
    
}
func btnDeleteClicked(_ clickedBtn:UIButton){

var loginTextField: UITextField?
var alertMesag = "Do you want to delete this card?"
var dicCardDetails = [String:Any]()
if self.shouldShowSearchResults == true {
if let card_data = self.arrFilterDataSearch[clickedBtn.tag] as? [String:Any]
{
    dicCardDetails = card_data
    if let card_no = card_data["name"]{
        alertMesag = "Do you want to delete \(card_no)?"
    }
    }
}
else {
    let arrPrefferedCards = self.dicPrefferCards["Preffer_card"] as? [[String:Any]]
    if arrPrefferedCards != nil && arrPrefferedCards!.count != 0 {
        if let card_data = arrPrefferedCards![clickedBtn.tag] as? [String:Any]{
            dicCardDetails = card_data

            if let card_no = dicCardDetails["card_no"]{
                alertMesag = "Remove \(card_no) from your preferred list?"
            }
        }
    }

}
let alert = UIAlertController(title: "Confirmation", message:alertMesag , preferredStyle:.alert)



alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
    
}))

alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default, handler:{[weak self] (ACTION :UIAlertAction!)in
    
    if appDelegate.networkAvialable == true {
        
        if alert.textFields![0].text?.count == 0
        {
            return
        }
        
        //let dicCardDetails = self?.arrFilterDataSearch[clickedBtn.tag]
        
        var dicParameters = [String:Any]()
        if Constants.isBookerLogin == true
        {
            dicParameters["user_id"] = "\(appDelegate.activeUserDetailsForBooker["id"]!)"
            dicParameters["is_booker"] = "yes"
        }
        else
        {
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            dicParameters["user_id"] = user_ID
            dicParameters["session"] = session
            dicParameters["is_booker"] = "no"
        }

        dicParameters["payment_id"] = dicCardDetails["id"]

        dicParameters["password"] = alert.textFields![0].text as Any?
        
        if self?.shouldShowSearchResults == true {
            dicParameters["status"] = "delete" as Any?
        }
        else {
            dicParameters["status"] = "prefered" as Any?
        }
        
        appDelegate.showProgressWithText("Deleting card details....")
        ServiceManager.sharedInstance.dataTaskWithPostRequest("removePreferedCard", dicParameters: dicParameters, successBlock: { [weak self](dicData) -> Void in
            appDelegate.hideProgress()
            
            DispatchQueue.main.async(execute: {[weak self] () -> Void in
                if let status = dicData["status"] as? String{
                    if status.compare("success") == .orderedSame
                    {
                        Globals.ToastAlertWithString("Card removed successfully")
                        if self?.shouldShowSearchResults == true {
                            self?.arrFilterDataSearch.remove(at: clickedBtn.tag)
                        }
                        else {
                            self?.arrPreferredCards.remove(at: clickedBtn.tag)
                        }
                        self?.tablePayments.reloadData()
                    }
                    else {
                        if let msg = dicData["message"] as? String
                        {
                            Globals.ToastAlertWithString(msg)
                        }
                        else {
                            Globals.ToastAlertWithString("Something went wrong, Please try again later")
                        }
                    }
                }
                })
            
            })
        { (error) -> () in
            appDelegate.hideProgress()
            let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
        }
        
    }else {
        let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
        alert.show()
    }
    
    }))

alert.addTextField { (textField) -> Void in
    loginTextField = textField
    loginTextField?.placeholder = "Enter Password"
    loginTextField?.isSecureTextEntry = true
    
}

self.present(alert, animated: true, completion: nil)


}


// MARK: - ====scan Card =====
func scanCard() {
    
    let authStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
    if authStatus == .authorized {
        self.showCardIOCardIOPaymentView()
    }
    else if authStatus == .notDetermined {
       // NSLog("%@", "Camera access not determined. Ask for permission.")
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {[weak self](granted: Bool) -> Void in
            if granted {
                NSLog("Granted access to %@", AVMediaTypeVideo)
                self!.showCardIOCardIOPaymentView()
            }
            else {
                NSLog("Not granted access to %@", AVMediaTypeVideo)
                let alert = UIAlertView(title: "Camera Access Denied", message: "Please go to Settings -> Privacy -> Enable camera access for app.", delegate: nil, cancelButtonTitle: "Cancel")
                alert.show()
            }
            })
    }
    else  {
        // My own Helper class is used here to pop a dialog in one simple line.
        let alert = UIAlertView(title: "Camera Access Denied", message: "Please go to Settings -> Privacy -> Enable camera access for app.", delegate: nil, cancelButtonTitle: "Cancel")
        alert.show()
    }
    
}


func showCardIOCardIOPaymentView()
{
    let scanViewController: CardIOPaymentViewController = CardIOPaymentViewController(paymentDelegate: self)
    scanViewController.modalPresentationStyle = .formSheet
    scanViewController.collectCardholderName = true
    DispatchQueue.main.async { [weak self]() -> Void in
        if self == nil {
            return
        }
        self?.present(scanViewController, animated: true, completion: nil)
    }
    
}
// MARK: ========CardIOPaymentViewControllerDelegate=====
func userDidProvide(_ info: CardIOCreditCardInfo, in paymentViewController: CardIOPaymentViewController) {
    NSLog("Scan succeeded with info: %@", info)
    // Do whatever needs to be done to deliver the purchased items.
   
    self.dismiss(animated: true, completion: nil)
    scanCardDetails.card_number = info.cardNumber
    scanCardDetails.cvv_number = info.cvv
    scanCardDetails.cc_name = info.cardholderName
    scanCardDetails.expiry_date = String(format: "%02lu/%lu", info.expiryMonth, info.expiryYear)
    scanCardDetails.card_type = self.cardType
    scanCardDetails.cc_masked_no = "\(info.redactedCardNumber!)"
    
    self.userCardDetailFromServerWithTypeOfURl(Constants.CreditCardUrlType.addUserCard)
}
func userDidCancel(_ paymentViewController: CardIOPaymentViewController) {
    NSLog("User cancelled scan")
    self.dismiss(animated: true, completion: nil)
}
// MARK:- ==Web Call get user billing type =====
func getBilligTypeInfoFromServer(){
    appDelegate.showProgressWithText("Loading Cards.....")
    weak var weakSelf = self
    let url = Constants.baseUrl + Constants.getBillingTypeInfo
    ServiceManager.sharedInstance.dataTaskWithGetRequest(url, successBlock: { (dicData) -> Void in
        
        if dicData["status"] as! String == "success" {
            
            for dicRecord in dicData["records"] as! [[String:Any]]{
                if (dicRecord["method"] as! String) == "Credit Card" {
                    weakSelf?.strCreditCardTypeID = dicRecord["id"] as! String
                    
                }
            }
            
        }else if dicData["status"] as! String == "session_expired" {
     //       JLToast.makeText("Session token mismatched", duration: 3.0).show()
        }
        })
    { (error) -> () in
   //     JLToast.makeText("Network Error", duration: 3.0).show()
    }
}
// MARK:- ==Web Call get , add, delete user cards from server  =====
func userCardDetailFromServerWithTypeOfURl(_ cardUrlType:Constants.CreditCardUrlType)
{
    if appDelegate.networkAvialable == true {
        
        var dicCards  = [String:Any]()
        
        appDelegate.showProgressWithText("Card Details.....")
        var dicParameters = [String:Any]()
        var urlString : String!
        
        
        if Constants.isBookerLogin == true
        {
            dicParameters["user_id"] = "\(appDelegate.activeUserDetailsForBooker["id"]!)"
            dicParameters["is_booker"] = "yes"
        }
        else
        {
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            dicParameters["user_id"] = user_ID
            dicParameters["session"] = session
            dicParameters["is_booker"] = "no"

        }
        
        if cardUrlType == Constants.CreditCardUrlType.getCards {
            urlString = Constants.getUserPreferredCards
        }else if cardUrlType == Constants.CreditCardUrlType.addUserCard {
            dicParameters["billing_type_id"] = payment_method as Any?
            urlString =  Constants.addUserCard
            dicParameters["paypal_code"] = "" as Any?
            dicParameters["cvv_number"] = self.scanCardDetails.cvv_number
            dicParameters["expiry_date"] = self.scanCardDetails.expiry_date
            dicParameters["card_type"] = self.scanCardDetails.card_type
            dicParameters["card_number"] = self.scanCardDetails.card_number
            dicParameters["cc_name"] = self.scanCardDetails.cc_name
            
        }else if  cardUrlType == Constants.CreditCardUrlType.searchCard {
            urlString =  "getcustomer_credit_card_prefered"
            dicParameters["card_no"] = self.strSearch as Any?
            
        }else if cardUrlType == Constants.CreditCardUrlType.addPreferredCard {
            appDelegate.showProgressWithText("Preferred Card Details.....")
            urlString =  "add_user_prefered_credit_card"
            dicParameters["prefered"] = "1" as Any?
            
            if let id = self.dicCardDetails["id"] {
                dicParameters["payment_id"] = "\(id)"
            }else {
                if let id = self.dicNewCardDetails["id"]{
                dicParameters["payment_id"] = "\(id)"
                }
            }
        }
        
        ServiceManager.sharedInstance.dataTaskWithPostRequest(urlString, dicParameters: dicParameters, successBlock: {[weak self] (dicData) -> Void in
            
            appDelegate.hideProgress()
            if cardUrlType == Constants.CreditCardUrlType.getCards {
                self?.shouldShowSearchResults = false
                if (dicData["status"] as! String) == "success" {
                    dicCards = dicData.formatDictionaryForNullValues(dicData)!
                    Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(dicCards as Any?, key:Constants.TotalCardsForPayment)
                    self?.arrPayments = dicCards["records"] as! [[String:Any]]
                    self?.dicPrefferCards = self?.arrPayments.first
                    
                    if let arrPrefferedCards = self?.dicPrefferCards["Preffer_card"] as? [[String:Any]]{
                        self?.arrPreferredCards = arrPrefferedCards
                    }
                    
                    self?.arrPayments.removeFirst()
                    DispatchQueue.main.async(execute: { () -> Void in
                        self?.tablePayments.reloadData()
                    })
                    
                }else  {
                    let alert = UIAlertView(title: "Error !", message: dicData["message"] as? String, delegate: nil, cancelButtonTitle: "Ok")
                    alert.show()
                }
                
            }else if  cardUrlType == Constants.CreditCardUrlType.addUserCard{
                
                if Constants.countyApp == "1"{
                    
                    var showAlert = false
                    DispatchQueue.main.async(execute: {[weak self]() -> Void in
                        appDelegate.hideProgress()
                        
                        if (dicData["status"] as! String) == "success" {
                            if self?.profilePayment == false {
                                self?.processResponseOfAddCard(dicCardResponse: dicData)
                            }
                            let alert = UIAlertView(title: "Success", message: dicData["message"] as? String, delegate: nil, cancelButtonTitle: "Ok")
                            alert.show()
                        }
                        else
                        {
                            showAlert = true
                            if let cardID = dicData["payment_id"] {
                                if !String(describing: cardID).isEmpty {
                                    showAlert = false
                                    self?.processResponseOfAddCard(dicCardResponse: dicData)
                                }
                            }
                            if showAlert {
                                let alert = UIAlertView(title: "Error !", message: dicData["message"] as? String, delegate: nil, cancelButtonTitle: "Ok")
                                alert.show()
                                self?.showCraditCardOptions()
                                return
                            }
                        }
                    })
                }
                else {
                DispatchQueue.main.async(execute: {() -> Void in
                    appDelegate.hideProgress()
                    
                    if (dicData["status"] as! String) == "success" {
                        if self?.profilePayment == false {
                            self?.dicNewCardDetails["id"] = dicData["payment_id"]!
                            self?.dicNewCardDetails["payment_method"] = self?.payment_method as Any?
                            if let cardNo = self?.dicNewCardDetails["name"] as? String{
                            self?.pushToRequestView(self?.dicNewCardDetails, String(describing: cardNo))
                            self?.navigationController?.popViewController(animated: true)
                            }
                        }else {
                            let alert = UIAlertController(title: "Confirmation", message: "Do you want to set \(self?.scanCardDetails.cc_masked_no ?? "") as a preferred card ?", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { [weak self] (ACTION :UIAlertAction!) in
                                self?.dicNewCardDetails["id"] = dicData["payment_id"]!
                                self?.dicNewCardDetails["payment_method"] = "\(String(describing: self?.payment_method))"
                            self?.userCardDetailFromServerWithTypeOfURl(Constants.CreditCardUrlType.addPreferredCard)
                            }))
                            alert.addAction(UIAlertAction(title: "No", style: .default, handler: {  (ACTION :UIAlertAction!) in
                            }))
                            self?.present(alert, animated: true, completion: nil)
                        }
                      //  let alert = UIAlertView(title: "Success", message: dicData["message"] as? String, delegate: nil, cancelButtonTitle: "Ok")
                        //alert.show()
                    }
                    else
                    {
                        let alert = UIAlertView(title: "Error !", message: dicData["message"] as? String, delegate: nil, cancelButtonTitle: "Ok")
                        alert.show()
                        return
                    }
                })
                }
            }else if cardUrlType == Constants.CreditCardUrlType.searchCard{
                DispatchQueue.main.async(execute: { [weak self]() -> Void in
                    if (dicData["status"] as! String) == "success" {
                        self?.arrFilterDataSearch = dicData["record"] as! [[String:Any]]
                        self?.shouldShowSearchResults = true
                        DispatchQueue.main.async(execute: { () -> Void in
                           self?.tablePayments.reloadData()
                        })
                    }else {
                        Globals.ToastAlertWithString(dicData["message"] as! String)
                        //weakSelf?.shouldShowSearchResults = false
                        DispatchQueue.main.async(execute: { () -> Void in
                            self?.tablePayments.reloadData()
                        })
                    }
                    })
                
            }else if cardUrlType == Constants.CreditCardUrlType.addPreferredCard{
                DispatchQueue.main.async(execute: { [weak self]() -> Void in
                    if (dicData["status"] as! String) == "success" {
                        Globals.ToastAlertWithString(dicData["message"] as! String)
                        if self?.profilePayment == false {
                            self?.pushToRequestView(self?.dicCardDetails, "Credit-card")
                            self?.navigationController?.popViewController(animated: true)
                        }
                    }else {
                        Globals.ToastAlertWithString(dicData["message"] as! String)
                    }
                    })
            }
            })
        { [weak self](error) -> () in
            self?.shouldShowSearchResults = false
            appDelegate.hideProgress()
            let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
            return
        }
    }else {
        Globals.ToastAlertForNetworkConnection()
    }
}


func processResponseOfAddCard(dicCardResponse:[String:Any])  {
    self.dicNewCardDetails["id"] = String(describing: dicCardResponse["payment_id"]!)
    self.dicNewCardDetails["payment_method"] = String(self.payment_method!)
    if let cardNo = self.dicNewCardDetails["name"] as? String {
        self.pushToRequestView(self.dicNewCardDetails, String(cardNo))
        self.popupViewTouched()
        self.navigationController?.popViewController(animated: true)
    }
    
   
}
// MARK:- ========  Action Sheet Delegate  =======

func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int){
    
    if actionSheet.tag == 1000{
        switch buttonIndex{
        case 1 :
            self.cardType = "Business"
            showCraditCardOptions()
            
        case 2:
            self.cardType  = "Personal"
            showCraditCardOptions()
            
        default : break
        }
    }
    else
    {
    }
}

func showCraditCardOptions()  {
    popupViewCardType.isHidden = false
    tblCardTypes.reloadData()
    let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PASelectPaymentVC.popupViewTouched))
    tapGesture.numberOfTapsRequired = 1
    tapGesture.delegate = self
    popupViewCardType.addGestureRecognizer(tapGesture)
    self.view.bringSubview(toFront: popupViewCardType)
    
}
func  popupViewTouched() {
    popupViewCardType.isHidden = true
}

// MARK:- ========  GestureRecognizer Delegate  =======

func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool{
    if gestureRecognizer is UITapGestureRecognizer {
        if (String(describing: touch.view!.classForCoder) == "UITableViewLabel"){
            return false
        }
    }
    return true
}

// MARK:- ========  Search bar  Delegate  =======

func  searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool{
    self.shouldShowSearchResults = true
    return true
}
func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
{
    self.shouldShowSearchResults = true
    self.searchBar.resignFirstResponder()
    self.userCardDetailFromServerWithTypeOfURl(Constants.CreditCardUrlType.searchCard)
    
}
func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
{
    self.shouldShowSearchResults = false
    self.searchBar.resignFirstResponder()
    self.searchBar.text = ""
    self.arrFilterDataSearch.removeAll(keepingCapacity: true)
    self.tablePayments.reloadData()

}
func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
{
    self.arrFilterDataSearch.removeAll(keepingCapacity: true)
    self.strSearch = searchText
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */
func backButtonActionMethod(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
}
}
