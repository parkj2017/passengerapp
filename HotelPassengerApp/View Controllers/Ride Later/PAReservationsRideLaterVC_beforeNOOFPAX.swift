    //
    //  PAReservationsRideLaterVC.swift
    //  PassangerApp
    //
    //  Created by Netquall on 2/1/16.
    //  Copyright © 2016 Netquall. All rights reserved.
    //

    import UIKit
    import GoogleMaps
    // FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
    // Consider refactoring the code to use the non-optional operators.
    fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
    }

    // FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
    // Consider refactoring the code to use the non-optional operators.
    fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
    }

    // FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
    // Consider refactoring the code to use the non-optional operators.
    fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l <= r
    default:
        return !(rhs < lhs)
    }
    }

    enum imageNameSection : String {
    case PickupDate = "calendar_icon"
    case Service_Vehicle_Payment = "arrowRight"
    case Child_Seat_Count  = "arrowDown"

    }
    enum btnTag : Int {

    case btnAddress = 40
    case btnAirport = 41
    case btnFBO = 42
    case btnSeaport = 43
    }
    enum pickupType : String {
    case Address
    case Airport
    case FBO
    case Seaport
    case POI
    case None
    }
    enum DropOffType : String {
    case Address
    case Airport
    case FBO
    case Seaport
    case POI
    case None
    }
    class PAReservationsRideLaterVC: BaseViewController, BackButtonActionProtocol {

    fileprivate enum keyIndexSection : Int {
        case pickUpDate = 0
        case passangerName = 7
        case passangerNumber = 8
        case pickType = 2
        case dropType = 3
        case stops = 4
        case serviceType = 5
        case payment = 6
        case adultLagguge = 1
        case childSeatType = 9
        case childSeatCount = 10
        case none = 1454
        case hourly = 3333
        
    }
    var deviceHeight : CGFloat = 60.0

    // MARK:- ============ Property decalaration ==========

    @IBOutlet weak var tableViewReservations: UITableView!

    // @IBOutlet weak var tblCarInfo: UITableView!
    var arrDataSource  : [[String:Any]]! = [[Constants.PickUpDate:"Pickup Date" as Any],
                                    
                                            [Constants.AdultLagguge:"01" as Any],
                                            [Constants.PickType:"PICKUP TYPE" as Any],
                                            [Constants.DropType:"DROP OFF TYPE" as Any],
                                            [Constants.Stops:"Stops" as Any],
                                            [Constants.Service:"Service Type" as Any],
                                            [Constants.Payment:"Payment" as Any],
                                            [Constants.PassangerName:"Passenger Name" as Any],
                                            [Constants.PassangerNumber:(Constants.countyApp == "1") ? "Passenger Email ID" : "Passenger Contact Number" as Any],
                                            [Constants.ChildSeatType:"Child Seat Type" as Any],
                                            [Constants.ChildCount:"Child Seat Count"  as Any]]

    lazy var dicOnlyAirport = [String:Any]()
    lazy var dicCardDetail = [String:Any]()
    lazy var arrDataSourceCarInfo = [Any]()
    lazy var arrDataServiceType = [[String:Any]]()
    var arrKeys : [String]!
    lazy  var arrStops = [[String:Any]]()

    var arrChildSeatType = ["Booster","Toddler","Infant"]
    var arrChildSeatTypeForAPI = ["Booster Seat","Forward facing (Toddler)","Rear facing (Infant)"]
    var arrHoursCount = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"]
    var arrChildCount = ["0","1","2","3","4","5","6","7","8","9","10"]
    var arrAdults :[String] = [String] ()
    var arrLuggage = [String]()
    lazy  var dicDisAndTime = [String:Any]()
    var strAirport : String!
    lazy var dicWebServiceParam = [String:Any]()
    var activeTextField: UITextField!
    var activeTextView: UITextView!
    var activeButton : UIButton!
    var currentSection = keyIndexSection.none.rawValue
    var dateSelected : Date!
    var strDateTime : String!
    var dicCheckCallDistance = [String:Bool]()
    var dictPickAddess = [String:Any]()
    var dictDropOffAddess = [String:Any]()
    var arrAddedChildSeats = [[String:Any]]()
    var btnPickupTypeTag = 0
    var btnPickupTypeLastOne = 0
    var btnDroppOffTypeTag = 0
    var btnDroppOffTypeLastOne = 0
    var isDisable = false
    var isTxtAdultPicker = false
    var strAdult = "0"
    var strLagguge = "0"
    var strEstimateFare = ""
    var strDisability = "0"
    var strVehicle = ""
    var strServiceType = ""
    var strPayment = "Payment"
    var strPickType = pickupType.None.rawValue
    var strDropOffType = DropOffType.None.rawValue
    var tagTextView : Int!
    var pickUpLocation : CLLocation?
    var dropOffLocation : CLLocation?
    var isEditingMode:Bool = false
    var reservationDetails:[String:Any]!
    var strReservationId = "0"
    var arrAirportInstuctions:[[String:String]] = [[String:String]]()
    var strHours = "0"
    var minimumJobHours:String! = ""
    var saved_reservaionID:String = ""
    var adultLuaggeTag = Int()

    var strServiceShortCode = ""
    var dictSelectedVehicleData:[String:Any] = [String:Any]()

    @IBOutlet var viewDatePicker: UIView!

    @IBOutlet var viewAdultLuggageTbl: UIView!
    @IBOutlet weak var pickerViewCarName: UIPickerView!
    @IBOutlet var viewPickerView: UIView!
    @IBOutlet var accessoryViewTextView: UIView!
    @IBOutlet var datePicker: UIDatePicker!

    @IBOutlet weak var txtLagguge: UITextField!
    @IBOutlet weak var txtAdult: UITextField!

    @IBOutlet weak var bottomArrorwImage: UIButton!

    var dateTimer : Timer!

    // MARK: - ======== View LIfe Cycyle   ========

    func setLeftBarbuttonItemTitle(_ title:String) {
        if title.isEmpty {
            self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Book For Later", imageName: "backArrow")
        }
        else{
            self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(title, imageName: "backArrow")
        }
        //       self.navigationController?.navigationBar.isOpaque = true
    }

        @IBAction func btnBottomArrowImageClicked(_ sender: UIButton) {
            
            var contentOffset:CGPoint = self.tableViewReservations.contentOffset
            contentOffset.y  = contentOffset.y + 350
            
            
            let endScrolling:CGFloat = self.tableViewReservations.contentSize.height
            
            if  endScrolling >= self.tableViewReservations.contentSize.height {
                self.bottomArrorwImage.isHidden = true
                self.tableViewReservations.setContentOffset(contentOffset, animated: true)
                
            }else {
                self.bottomArrorwImage.isHidden = false
                self.tableViewReservations.setContentOffset(contentOffset, animated: true)

            }
        }
        override func viewDidLoad() {
        super.viewDidLoad()
        if Constants.iPadScreen  {
            deviceHeight = 95
        }
        if let btnSubmit = self.view.viewWithTag(996633) as? UIButton {
            btnSubmit.setTitle("Select Vehicle", for: .normal)
            btnSubmit.backgroundColor = UIColor.appRideLater_RedColor()
            btnSubmit.titleLabel?.font = Globals.defaultAppFontWithBold(14)
            Globals.layoutViewFor(btnSubmit, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 3)
        }
            
            Globals.layoutViewFor(self.bottomArrorwImage, color: UIColor.lightGray, width: 1.0, cornerRadius: 5)
        datePicker.minimumDate = Date()
        self.setLeftBarbuttonItemTitle("")
        for  i  in  1...10 {
            self.arrAdults.append(String(i))
        }
        for  i  in  0...10 {
            self.arrLuggage.append(String(i))
        }
        if self.isEditingMode == true{
            self.tableViewReservations.contentInset  = UIEdgeInsetsMake(-64, 0, 0, 0)
            self.tableViewReservations.scrollIndicatorInsets = UIEdgeInsetsMake(-64, 0, 0, 0)
            self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Edit Reservation", imageName: "backArrow")
            if let id = self.reservationDetails["id"] as? String{
                self.strReservationId = id
            }
            var passengerName = ""
            if let name = self.reservationDetails["passanger_name"] as? String{
                passengerName = passengerName + name
            }
            if !passengerName.isEmpty
            {
                self.arrDataSource[keyIndexSection.passangerName.rawValue][Constants.PassangerName] = passengerName as Any?
            }
            
            if let lagguge = self.reservationDetails["luggage"]{
                self.txtLagguge.text = String(describing: lagguge)
            }
            if let noPax = self.reservationDetails["no_of_pax"] {
                self.txtAdult.text = String(describing: noPax)
            }
            if let phone = self.reservationDetails["pass_phone"] as? String{
                self.arrDataSource[keyIndexSection.passangerNumber.rawValue][Constants.PassangerNumber] = phone as Any?
            }
            
            var dateTime = ""
            if let date = self.reservationDetails["pu_date"] as? String{
                dateTime = dateTime + date
            }
            if let time = self.reservationDetails["pu_time"] as? String{
                dateTime += " "
                dateTime = dateTime + time
            }
            if !dateTime.isEmpty
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = dateFormatter.date(from: dateTime)
                self.dateSelected = date!
                dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm a"
                self.arrDataSource[keyIndexSection.pickUpDate.rawValue][Constants.PickUpDate] = dateFormatter.string( from: self.dateSelected) as Any?
                self.strDateTime  = Globals.sharedInstance.dateFromTimestamp(self.dateSelected.timeIntervalSinceNow)
            }
            
            // var pickuptype = ""
            if let ptype = self.reservationDetails["pickup_type"] as? String{
                if ptype.compare( "address") || ptype.compare( "poi"){
                    self.strPickType = ptype.compare( "address") == true ? pickupType.Address.rawValue : pickupType.POI.rawValue
                    self.btnPickupTypeTag = btnTag.btnAddress.rawValue
                    self.dicWebServiceParam["pickup_type"] = "address" as Any?
                }
                else if ptype.compare("fbo"){
                    self.strPickType = pickupType.FBO.rawValue
                    self.btnPickupTypeTag = btnTag.btnFBO.rawValue
                }
                else if ptype.compare("airport"){
                    self.strPickType = pickupType.Airport.rawValue
                    self.btnPickupTypeTag = btnTag.btnAirport.rawValue
                }
                else if ptype.compare("seaport") {
                    self.strPickType = pickupType.Seaport.rawValue
                    self.btnPickupTypeTag = btnTag.btnSeaport.rawValue
                }
                self.btnPickupTypeLastOne = self.btnPickupTypeTag
            }
            if let dtype = self.reservationDetails["dropoff_type"] as? String{
                if dtype.compare("address") || dtype.compare( "poi"){
                    self.strDropOffType = dtype.compare("address") == true ? DropOffType.Address.rawValue : DropOffType.POI.rawValue
                    self.btnDroppOffTypeTag = btnTag.btnAddress.rawValue
                    self.dicWebServiceParam["drop_type"] = "address" as Any?
                    if self.dictDropOffAddess.count != 0 {
                        self.dicWebServiceParam["dropoff"] = self.dictDropOffAddess["address"]
                        self.dicWebServiceParam["dropoff_latitude"] = self.dictDropOffAddess["latitude"]
                        self.dicWebServiceParam["dropoff_longitude"] = self.dictDropOffAddess["longitude"]
                        self.dicWebServiceParam["zip_to"] = self.dictDropOffAddess["zip"]
                        self.dicWebServiceParam["dropoff_info"] = self.dictDropOffAddess as Any?
                    }
                }
                else if dtype.compare("fbo"){
                    self.strDropOffType = DropOffType.FBO.rawValue
                    self.btnDroppOffTypeTag = btnTag.btnFBO.rawValue
                }
                else if dtype.compare("airport"){
                    self.strDropOffType = DropOffType.Airport.rawValue
                    self.btnDroppOffTypeTag = btnTag.btnAirport.rawValue
                }
                else if dtype.compare("seaport"){
                    self.strDropOffType = DropOffType.Seaport.rawValue
                    self.btnDroppOffTypeTag = btnTag.btnSeaport.rawValue
                }
                self.btnDroppOffTypeLastOne = self.btnDroppOffTypeTag
            }
            
            if let pickup_info = self.reservationDetails["pickup_info"] as? [String:Any]{
                if self.strPickType == pickupType.Address.rawValue ||  self.strPickType == pickupType.POI.rawValue {
                    self.dictPickAddess = pickup_info
                }
                else if self.strPickType == pickupType.Airport.rawValue {
                    self.dicOnlyAirport["pickup"] = pickup_info as Any?
                }
                else if self.strPickType == pickupType.Seaport.rawValue {
                    self.dictPickAddess["seaport_details"] = pickup_info as Any?
                    self.dictPickAddess["seaport_address"] = pickup_info as Any?
                }
                else if self.strPickType == pickupType.FBO.rawValue {
                    self.dictPickAddess["fbo_details"] = pickup_info as Any?
                    self.dictPickAddess["fbo_address"] = pickup_info as Any?
                }
                if self.dictPickAddess.count != 0  {
                    self.dicWebServiceParam["pickup"] = self.dictPickAddess["address"]
                    self.dicWebServiceParam["pickup_latitude"] = self.dictPickAddess["latitude"]
                    self.dicWebServiceParam["pickup_longitude"] = self.dictPickAddess["longitude"]
                    self.dicWebServiceParam["zip_from"] = self.dictPickAddess["postal_code"]
                    self.dicWebServiceParam["pickup_info"] = self.dictPickAddess as Any?
                }
                self.arrDataSource[keyIndexSection.pickType.rawValue][Constants.PickType] = pickup_info as Any?
            }
            if let dropoff_info = self.reservationDetails["dropoff_info"] as? [String:Any]{
                if self.strDropOffType == DropOffType.Address.rawValue  || self.strDropOffType ==  DropOffType.POI.rawValue{
                    self.dictDropOffAddess = dropoff_info
                }
                else if self.strDropOffType == DropOffType.Airport.rawValue {
                    self.dicOnlyAirport["dropoff"] = dropoff_info as Any?
                }
                else if self.strDropOffType == DropOffType.Seaport.rawValue {
                    self.dictDropOffAddess["seaport_details"] = dropoff_info as Any?
                    self.dictDropOffAddess["seaport_address"] = dropoff_info as Any?
                    
                }
                else if self.strDropOffType == DropOffType.FBO.rawValue {
                    self.dictDropOffAddess["fbo_details"] = dropoff_info as Any?
                    self.dictDropOffAddess["fbo_address"] = dropoff_info as Any?
                }
                self.arrDataSource[keyIndexSection.dropType.rawValue][Constants.DropType] = dropoff_info as Any?
                
            }
            // Rashpinder Changes
            self.dicWebServiceParam["pickup"] = self.reservationDetails["pickup"]
            self.dicWebServiceParam["pickup_latitude"] = self.reservationDetails["pickup_latitude"]
            self.dicWebServiceParam["pickup_longitude"] = self.reservationDetails["pickup_longitude"]
            self.dicWebServiceParam["dropoff"] = self.reservationDetails["dropoff"]
            self.dicWebServiceParam["dropoff_latitude"] = self.reservationDetails["dropoff_latitude"]
            self.dicWebServiceParam["dropoff_longitude"] = self.reservationDetails["dropoff_longitude"]
            
            self.pickUpLocation = CLLocation(latitude: ((((self.dicWebServiceParam["pickup_latitude"]!) as Any) as Any) as AnyObject).doubleValue , longitude: ((self.dicWebServiceParam["pickup_longitude"]!) as AnyObject).doubleValue)
            if let dropOff =  self.dicWebServiceParam["dropoff"] {
                self.dropOffLocation = CLLocation(latitude: ((((self.dicWebServiceParam["dropoff_latitude"]!) as Any) as Any) as AnyObject).doubleValue , longitude: ((self.dicWebServiceParam["dropoff_longitude"]!) as AnyObject).doubleValue)
            }
            if let stopArr = self.reservationDetails["stops"] as? [[String:Any]]{
                if stopArr.count > 0{
                    self.arrDataSource[keyIndexSection.stops.rawValue][Constants.Stops] = stopArr as Any?
                    self.arrStops = stopArr
                    self.tableViewReservations.setEditing(true, animated: true)
                }
            }
            
            
            let dNote = self.reservationDetails["dropoff_notes"] as? String
            if dNote != nil &&  dNote?.count > 0{
                self.dictDropOffAddess[Constants.DropNotes] = dNote! as Any?
            }else {
                self.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
            }
            let pNote = self.reservationDetails["pickup_notes"] as? String
            if pNote != nil &&  pNote?.count > 0 {
                self.dictPickAddess[Constants.PickNotes] = pNote! as Any?
            } else {
                self.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
            }
           
            var serviceType = Constants.Service
            if let sType = self.reservationDetails["serviceTypeName"] as? String{
                serviceType = sType
                self.setLeftBarbuttonItemTitle(serviceType)
            }
            if !serviceType.isEmpty
            {
                var dicServiceType = [String:Any]()
                /*
                 {
                 : "466",
                 : "Point to Point",
                 : "PTP"
                 }*/
                if let sID = self.reservationDetails["service_type_id"]{
                dicServiceType["id"] = "\(sID)"
                dicServiceType["service_type"] = "\(serviceType)"
                dicServiceType["short_code"] = ""
                }
                self.arrDataSource[keyIndexSection.serviceType.rawValue][Constants.Service] = dicServiceType
                self.strServiceType = serviceType
                self.setLeftBarbuttonItemTitle(self.strServiceType)
            }
            
            //            var childSeatCount = 0
            //            if let childCount = self.reservationDetails["child_seat_count"] as? Int{
            //                childSeatCount = childCount
            //            }
            //            if childSeatCount != 0
            //            {
            //                self.arrDataSource[keyIndexSection.ChildSeatCount.rawValue][Constants.ChildCount] = String(childSeatCount)
            //            }
            
            if let childSeatArray = self.reservationDetails["child_seat_data"] as? [[String:Any]]{
                self.arrAddedChildSeats = childSeatArray
            }
            if let res_id = self.reservationDetails["reservation_id"]{
                self.saved_reservaionID = "\(res_id)"
            }
            if self.arrAddedChildSeats.count != 0 {
                self.dicWebServiceParam["child_seat_count"] = self.arrAddedChildSeats as Any?
            }else {
                self.dicWebServiceParam["child_seat_count"] = "0" as Any?
            }
            if let vType = self.reservationDetails["luggage"]
            {
                self.arrDataSource[keyIndexSection.adultLagguge.rawValue][Constants.AdultLagguge] = vType
            }
            /* case DropType = 4
             case Stops = 5
             case Vehicle = 6
             case Payment = 7
             case AdultLagguge = 8
             case ChildSeatType = 9
             case ChildSeatCount = 10*/
            // Paymment
            if let PayId = self.reservationDetails["payment_id"]{
                self.dicCardDetail["id"] = PayId//String(PayId)
            } else {
                self.dicCardDetail["id"] =  "" as Any?
            }
            if let methodPayId = self.reservationDetails["payment_method_id"]{
                self.dicCardDetail["payment_method"] =  methodPayId// String(methodPayId)
            } else {
                self.dicCardDetail["payment_method"] =  "" as Any?
            }
            if let name = self.reservationDetails["payment_method"]{
                self.dicCardDetail["name"] = name//String(name)
                self.arrDataSource[keyIndexSection.payment.rawValue][Constants.Payment] = name
            } else {
                self.dicCardDetail["name"] =  "Payment" as Any?
                self.arrDataSource[keyIndexSection.payment.rawValue][Constants.Payment] = "Payment" as Any?
            }
            self.strPayment =  self.dicCardDetail["name"] as! String
            
            if let strHandicap = self.reservationDetails["is_handicap"]   {
                self.strDisability = String(describing: strHandicap)
            }else {
                self.strDisability = "0"
            }
            self.isDisable = self.strDisability.count>0 ? self.strDisability.toBool()! : false
            if let btnView = self.viewAdultLuggageTbl.viewWithTag(55) {
                (btnView as! UIButton).isSelected = self.isDisable
            }
            if let hourly_time = self.reservationDetails["hourly_time"]{
                self.strHours = String("\(hourly_time)")
                // self.arrDataSource[keyIndexSection.Hourly.rawValue][Constants.Hourly] = self.strHours
                
            }
            self.getDistanceFromPickUpToDropOff()
        }
        else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm a"
            //     self.arrDataSource[0][Constants.PickUpDate] = dateFormatter.stringFromDate(NSDate())
            self.strDateTime  = Globals.sharedInstance.dateFromTimestamp(Date().timeIntervalSinceNow)
            
            
            
            var name = Constants.isBookerLogin == true ? "\(String(describing: appDelegate.activeUserDetailsForBooker["first_name"]!))" : Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).first_name") as! String
            let lastName =  Constants.isBookerLogin == true ? "\(String(describing: appDelegate.activeUserDetailsForBooker["last_name"]!))" : Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).last_name") as! String
            if name == "" && lastName == "" {
                name = "Full Name"
            }
            self.arrDataSource[keyIndexSection.passangerName.rawValue][Constants.PassangerName] = "\(name.capitalized) \(lastName.capitalized)" as Any?
            
            
            
            if Constants.countyApp == "1"{
                self.arrDataSource[keyIndexSection.passangerNumber.rawValue][Constants.PassangerNumber] = ""
                self.arrDataSource[keyIndexSection.passangerName.rawValue][Constants.PassangerName] = ""
            }
            else
            {
                let phoneNumber =  Constants.isBookerLogin == true ? "\(String(describing: appDelegate.activeUserDetailsForBooker["mobile"]!))" : Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).mobile") as! String
                self.arrDataSource[keyIndexSection.passangerNumber.rawValue][Constants.PassangerNumber] = phoneNumber
            }
            
            
            self.dicCheckCallDistance["Pick"] = false
            self.dicCheckCallDistance["Dropp"] = false //"Dropp":false]
            
        }
        self.viewDatePicker.frame = CGRect(x: 0, y: self.view.frame.height - 216, width: self.view.frame.width , height: 216)
        self.viewDatePicker.isHidden = true
        self.viewPickerView.frame = CGRect(x: 0, y: self.view.frame.height - 216, width: self.view.frame.width , height: 0)
        self.viewPickerView.isHidden = true
        self.tableViewReservations.contentInset = UIEdgeInsetsMake(5, 0, 0, 0)
        
        //cellTextField.txtFieldCell.inputView = self.viewPickerView
        self.viewPickerView.frame = CGRect(x: 0, y: self.view.frame.height - 216, width: self.view.frame.width , height: 260)
        self.viewPickerView.isHidden = true
        self.getingVehlciTypesAvailable()
        self.txtAdult.inputView = self.viewPickerView
        self.txtLagguge.inputView = self.viewPickerView
        //self.view.addSubview(self.viewPickerView)
        // self.view.bringSubviewToFront(self.viewPickerView)
        // Do any additional setup after loading the view.
        if Constants.countyApp == "1"{
            self.updatePickupDateFromTimer()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
        self.registerForKeybaordNotification()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.isOpaque = false
        if Constants.countyApp == "1"{
            self.startDatePickupTimer()
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        Globals.sharedInstance.delegateBack = nil
        self.unRegisterForKeboardNotification()
        if Constants.countyApp == "1"{
            self.stopDatePickupTimer()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK:- ============ Timer for change Pickup date  ==========
    func stopDatePickupTimer()   {
        if self.dateTimer != nil &&  self.dateTimer.isValid {
            self.dateTimer.invalidate()
            self.dateTimer = nil
        }
    }
    func startDatePickupTimer()  {
        self.dateTimer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector:#selector(PAReservationsRideLaterVC.updatePickupDateFromTimer) , userInfo: nil, repeats: true) //(timeInterval: 6.0, target: self, selector: , userInfo: nil, repeats: true)
    }

    func updatePickupDateFromTimer()  {
        self.dateSelected = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm a"
        self.strDateTime  = Globals.sharedInstance.dateFromTimestamp(self.dateSelected.timeIntervalSinceNow)
        self.arrDataSource[keyIndexSection.pickUpDate.rawValue][Constants.PickUpDate] = dateFormatter.string(from: self.dateSelected) as Any?
        self.reloadTableViewOnMainQueue()
    }

    class func baseUrl()-> String{
        return Constants.baseUrl
    }
    func reloadTableViewOnMainQueue(){
        DispatchQueue.main.async { [weak self]() -> Void in
            self?.tableViewReservations.reloadData()
        }
    }
    // MARK:- ============ get vehicle available ==========

    func getingVehlciTypesAvailable()
    {
        
        if let _ = self.activeTextField {
            self.activeTextField.resignFirstResponder()
        }
        self.view.endEditing(true)
        if self.arrDataSourceCarInfo.count == 0 {
            if appDelegate.networkAvialable == true {
                appDelegate.showProgressWithText("Getting available vehicle...")
                var param_Dictionary = [String : Any]()
                
                if Constants.isBookerLogin == true
                {
                    param_Dictionary["user_id"] = "\(appDelegate.activeUserDetailsForBooker["id"]!)"
                    param_Dictionary["is_booker"] = "yes"
                }
                else
                {
                    let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
                    let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
                    param_Dictionary["user_id"] = user_ID
                    param_Dictionary["session"] = session
                    param_Dictionary["is_booker"] = "no"

                }
                
                param_Dictionary["company_id"] = Constants.CompanyID as Any?
                ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.getVehicleTypes, dicParameters: param_Dictionary, successBlock: { [weak self] (dicData) -> Void in
                    appDelegate.hideProgress()
                    if dicData["status"] as! String == "success" {
                        if self?.isEditingMode == true{
                            if let minimumJobHours = dicData["first_range"]{
                                if self?.strHours == "0" {
                                    self?.strHours = String("Minimum \(minimumJobHours) hour(s) job")
                                    //self?.arrDataSource[keyIndexSection.Hourly.rawValue][Constants.Hourly] = self?.strHours
                                }
                                self?.minimumJobHours = String(describing: minimumJobHours)
                            }
                        }
                        else {
                            if let minimumJobHours = dicData["first_range"]{
                                if String(describing: minimumJobHours) == "0"
                                {
                                    self?.strHours = "0"
                                    
                                }else {
                                    self?.strHours = String("Minimum \(minimumJobHours) hour(s) job")
                                    //     self?.arrDataSource[keyIndexSection.Hourly.rawValue][Constants.Hourly] = self?.strHours
                                }
                                self?.minimumJobHours = String(describing: minimumJobHours)
                            }
                        }
                        if let arrSevice = dicData["service_type"] as? [[String:Any]]{
                            self?.arrDataServiceType = arrSevice
                        }
                        
                        if self?.isEditingMode == false{
                            
                            if let strSevice = dicData["prefered_service"] as? String{
                                //self?.arrDataServiceType = arrSevice
                                for (_,dicService) in (self?.arrDataServiceType.enumerated())!  {
                                    let dicServiceNew = dicService
                                    if let strDicService = dicServiceNew["service_type"] as? String {
                                        if  strDicService.compare(strSevice) {
                                            self?.arrDataSource[keyIndexSection.serviceType.rawValue][Constants.Service] = dicServiceNew as Any?
                                            self?.strServiceType = strSevice
                                            self?.strServiceShortCode = String(describing: dicServiceNew["short_code"]!)
                                            self?.setLeftBarbuttonItemTitle(strSevice);
                                            break
                                        }
                                        
                                    }
                                }
                            }
                        }
                        if let arrSevice = dicData["airport_instructions"] as? [[String:String]]{
                            self?.arrAirportInstuctions = arrSevice
                        }
                        if let payementArr = dicData["card"] as? [[String:Any]] {
                            if payementArr.count != 0 && self?.dicCardDetail.count == 0{
                                self?.dicCardDetail = payementArr.first!
                                if let cardNo = self?.dicCardDetail["card_no"] as? String {
                                    
                                    if cardNo.count > 0{
                                        self?.arrDataSource[keyIndexSection.payment.rawValue][Constants.Payment] = cardNo as Any?
                                        self?.strPayment = cardNo
                                        
                                    }
                                    else if let cardNo = self?.dicCardDetail["name"] as? String {
                                        self?.arrDataSource[keyIndexSection.payment.rawValue][Constants.Payment] = cardNo as Any?
                                        self?.strPayment = cardNo
                                    }
                                }
                            }
                        }
                        if let arrVeicleInfo = dicData["records"] as? [[String:Any]]{
                            self?.arrDataSourceCarInfo = arrVeicleInfo as [Any]
                        }
                        if self?.isEditingMode == true {
                            if self?.arrDataSourceCarInfo != nil &&  self?.arrDataSourceCarInfo.count != 0 {
                                for dicVehicle in self!.arrDataSourceCarInfo as! [[String:Any]] {
                                    if String(describing: dicVehicle["id"]!).compare(String(describing: self!.reservationDetails["vehicle_type"]!)) {
                                       // self!.strVehicle = String(describing: self!.reservationDetails["vehicle_type_title"]!)
                                        
                                        if let adut_cap = dicVehicle["vehicle_specific_passenger_capacity"]{
                                            self?.txtAdult.text = String(describing: adut_cap).count > 0 ?  String(describing: adut_cap) : "01"
//                                            self!.arrAdults.removeAll()
//                                            let loop = Int(self!.txtAdult.text!)!
//                                            for  i  in  1...loop {
//                                                self?.arrAdults.append(String(i))
//                                            }
                                        }
                                        if let adut_cap = dicVehicle["vehicle_specific_luggage_capacity"]{
                                            self?.txtLagguge.text =  String(describing: adut_cap).count > 0 ?  String(describing: adut_cap) : "01"
//                                            self?.arrLuggage.removeAll()
//                                            let loop = Int(self!.txtLagguge.text!)!
//                                            for  i  in  0...loop {
//                                                self?.arrLuggage.append(String(i))
//                                            }
                                        }

                                        break
                                    }
                                }
                            }
                            if self?.arrDataServiceType != nil &&  self?.arrDataServiceType.count != 0 {
                                for dicVehicle in self!.arrDataServiceType {
                                    if String(describing: dicVehicle["id"]!).compare(String(describing: self!.reservationDetails["service_type_id"]!)) {
                                        self?.arrDataSource[keyIndexSection.serviceType.rawValue][Constants.Service] = dicVehicle as Any?
                                        self!.strServiceType = String(describing: dicVehicle["service_type"]!)
                                        self?.strServiceShortCode = String(describing: dicVehicle["short_code"]!)
                                        
                                        
                                        break
                                    }
                                }
                            }
                        }
                        else{
                            
                            
                            
                            if let preferredVehicle = dicData["preferred_vehicle"]{
                                if !"\(preferredVehicle)".isEmpty{
                                    
                                    
                                    for (index, dict) in (self?.arrDataSourceCarInfo.enumerated())! {
                                        
                                        if let dict = dict as? [String:Any]{
                                            if "\(dict["id"]!)" == "\(preferredVehicle)"{
                                                
                                                
                                                
                                                let dicCars = self?.arrDataSourceCarInfo[index] as! [String:Any]
                                               // self?.strVehicle =  (dicCars["vehicle_type_title"] as? String)!
                                                if let adut_cap = dicCars["vehicle_specific_passenger_capacity"]{
                                                    self?.txtAdult.text = String(describing: adut_cap).count > 0 ?  String(describing: adut_cap) : "01"
//                                                    self!.arrAdults.removeAll()
//                                                    let loop = Int(self!.txtAdult.text!)!
//                                                    for  i  in  1...loop {
//                                                        self?.arrAdults.append(String(i))
//                                                    }
                                                }
                                                
                                                if let adut_cap = dicCars["vehicle_specific_luggage_capacity"]{
                                                    self?.txtLagguge.text =  String(describing: adut_cap).count > 0 ?  String(describing: adut_cap) : "01"
//                                                    self?.arrLuggage.removeAll()
//                                                    let loop = Int(self!.txtLagguge.text!)!
//                                                    for  i  in  0...loop {
//                                                        self?.arrLuggage.append(String(i))
//                                                    }
                                                }
                                                break
                                            }
                                        }
                                    }
                                }
                            }
                            
                            
                            
                        }
                        self?.reloadTableViewOnMainQueue()
                        // self?.setupPickerViewAgain()
                    }else {
                        if self?.activeTextField != nil  {
                            self?.activeTextField.resignFirstResponder()
                        }
                        self?.viewPickerView.isHidden = true
                        appDelegate.hideProgress()
                        Globals.ToastAlertWithString(dicData["message"] as! String)
                    }
                    }, failureBlock: { [weak self] (error) -> () in
                        if self?.activeTextField != nil  {
                            self?.activeTextField.resignFirstResponder()
                        }
                        self?.viewPickerView.isHidden = true
                        appDelegate.hideProgress()
                        Globals.ToastAlertWithString("Network Error!..")
                })
                
                
            }else {
                Globals.ToastAlertForNetworkConnection()
            }
        }else {
            // self.setupPickerViewAgain()
        }
    }
    // MARK:- ====Picker toolbar Buttons Action ===
    @IBAction func datePickerValueChangedAction(_ sender: UIDatePicker) {
        self.dateSelected = sender.date
    }

    @IBAction func btnDonePickUpDateAction(_ sender: UIBarButtonItem) {
        
        if self.dateSelected == nil {
            self.dateSelected = Date()
        }
        else {
            self.dateSelected = self.datePicker.date
        }
        let strHours = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).prohibitReservation_before")
        if strHours != nil && String(describing: strHours).compare("0") == false {
            let date = Date()
            let dayHourMinuteSecond: NSCalendar.Unit = [.day, .hour, .minute, .second]
            let components:DateComponents = (Calendar.current as NSCalendar).components(dayHourMinuteSecond, from: date, to: self.dateSelected, options: [])
            
            if components.day == 0 {
                if components.hour!+1 <= Int(String(describing:strHours!))! {
                    let alert = UIAlertView(title: "Alert", message:  "Reservations can only be accepted after \(strHours!) hours of current time", delegate: nil, cancelButtonTitle: "Dismiss")
                    alert.show()
                    return
                }
            }
        }
        
    //        let dateFormatter = DateFormatter()
    //
    //        dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm aa"
    //
    //        self.strDateTime  = Globals.sharedInstance.dateFromTimestamp(self.dateSelected.timeIntervalSinceNow)
    //        self.arrDataSource[keyIndexSection.pickUpDate.rawValue][Constants.PickUpDate] = dateFormatter.string(from: self.dateSelected) as Any?
    //        self.activeTextField.text = dateFormatter.string(from: self.dateSelected)
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           dateFormatter.timeZone = TimeZone.current
        
        let date = dateFormatter.string(from: self.dateSelected)
        if let current_Date: Date = dateFormatter.date(from: date){
            let dateFormatter2: DateFormatter = DateFormatter()
            dateFormatter2.dateFormat = "EEE, dd MMM yyyy hh:mm:ss a"
            dateFormatter2.timeZone = TimeZone.current

            let dateText: String = dateFormatter2.string(from: current_Date)
            
            self.arrDataSource[keyIndexSection.pickUpDate.rawValue][Constants.PickUpDate] = dateText
            self.activeTextField.text = dateText
            self.strDateTime = Globals.sharedInstance.dateFromTimestamp(self.dateSelected.timeIntervalSinceNow)
        }
        
        self.activeTextField.resignFirstResponder()
        self.reloadTableViewOnMainQueue()
    }
    @IBAction func btnDonePickerViewAction(_ sender: UIBarButtonItem) {
        if self.isTxtAdultPicker == false {
            if self.currentSection == keyIndexSection.serviceType.rawValue
            {
                self.arrDataSource[keyIndexSection.serviceType.rawValue][Constants.Service] = self.arrDataServiceType[pickerViewCarName.selectedRow(inComponent: 0)]  as Any?
                self.strServiceType =  (( self.arrDataServiceType[pickerViewCarName.selectedRow(inComponent: 0)] )["service_type"] as? String!)!
                self.activeTextField.text = self.strServiceType
                self.strServiceShortCode = (( self.arrDataServiceType[pickerViewCarName.selectedRow(inComponent: 0)] )["short_code"] as? String!)!
                
                self.setLeftBarbuttonItemTitle(self.strServiceType)
                //                if self.strServiceType.compare("Hourly") == true  {
                //                    self.arrDataSource.insert([Constants.Hourly:"Add Hours"], atIndex: keyIndexSection.Hourly.rawValue)
                //                }
                self.reloadTableViewOnMainQueue()
                
                //self.tableViewReservations.reloadSections(NSIndexSet(index: keyIndexSection.ServiceType.rawValue), withRowAnimation: .Automatic)
                //  self.calculateEstimateFareReservation()
            }
//               else if self.currentSection == keyIndexSection.vehicle.rawValue
//                {
//                    self.arrDataSource[self.currentSection][Constants.Vehicle] = self.arrDataSourceCarInfo[pickerViewCarName.selectedRow(inComponent: 0)] as! [String:Any] as Any?
//                    if let dicValues =  self.arrDataSourceCarInfo[pickerViewCarName.selectedRow(inComponent: 0)] as? [String:Any]{
//                        self.strVehicle =  dicValues["vehicle_type_title"] as! String
//                        self.activeTextField.text = self.strVehicle
//
//                        if let adut_cap = dicValues["vehicle_specific_passenger_capacity"]{
//                            txtAdult.text = String(describing: adut_cap).count > 0 ?  String(describing: adut_cap) : "01"
//                            self.arrAdults.removeAll(keepingCapacity: true)
//                            let loop = Int(self.txtAdult.text!)!
//                            for  i  in  1...loop {
//                                self.arrAdults.append(String(i))
//                            }
//                        }
//                        if let adut_cap = dicValues["vehicle_specific_luggage_capacity"]{
//                            txtLagguge.text =  String(describing: adut_cap).count > 0 ?  String(describing: adut_cap) : "01"
//                            self.arrLuggage.removeAll(keepingCapacity: true)
//                            let loop = Int(self.txtLagguge.text!)!
//                            for  i  in  0...loop {
//                                self.arrLuggage.append(String(i))
//                            }
//                        }
//                        //  self.calculateEstimateFareReservation()
//                    }
               //}
                else if self.currentSection == keyIndexSection.childSeatType.rawValue
                {
                    self.arrDataSource[self.currentSection][Constants.ChildSeatType] = self.arrChildSeatType[pickerViewCarName.selectedRow(inComponent: 0)] as Any?
                    self.activeTextField.text = self.arrChildSeatType[pickerViewCarName.selectedRow(inComponent: 0)]
                }else if self.currentSection == keyIndexSection.childSeatCount.rawValue
                {
                    self.arrDataSource[self.currentSection][Constants.ChildCount] = self.arrChildCount[pickerViewCarName.selectedRow(inComponent: 0)] as Any?
                    self.activeTextField.text = self.arrChildCount[pickerViewCarName.selectedRow(inComponent: 0)]
                }else  if self.currentSection == keyIndexSection.hourly.rawValue {
                    self.strHours = self.arrHoursCount[pickerViewCarName.selectedRow(inComponent: 0)]
                    self.activeTextField.text =   "\(self.strHours) Hours"
            }
        }else {
            self.isTxtAdultPicker = false
            if self.activeTextField.tag == 81 {
                self.activeTextField.text = self.arrLuggage[pickerViewCarName.selectedRow(inComponent: 0)]
                strLagguge = self.arrLuggage[pickerViewCarName.selectedRow(inComponent: 0)]
            }else if self.activeTextField.tag == 80 {
                self.activeTextField.text = self.arrAdults[pickerViewCarName.selectedRow(inComponent: 0)]
                strAdult = self.arrAdults[pickerViewCarName.selectedRow(inComponent: 0)]
            }
        }
        self.view.endEditing(true)
        self.activeTextField.resignFirstResponder()
    }
    func setupPickerViewAgain(){
        appDelegate.hideProgress()
        UIView.animate(withDuration: 0.20, animations: { [weak self]() -> Void in
            self?.pickerViewCarName.reloadAllComponents()
            self?.viewPickerView.frame = CGRect(x: 0, y: (self?.view.frame)!.height - 216, width: (self?.view.frame)!.width , height: 260)
            self?.viewPickerView.isHidden = false
            self?.view.addSubview((self?.viewPickerView)!)
            self?.view!.bringSubview(toFront: (self?.viewPickerView)!)
        })
    }

    @IBAction func btnDoneTextViewNotesAction(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        self.reloadTableViewOnMainQueue()
    }
    // MARK:- ====key will Shown and Hide Notification ===
    func registerForKeybaordNotification()
    {
        NotificationCenter.default.addObserver(self, selector:#selector(PAReservationsRideLaterVC.KeyboardWillShown(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PAReservationsRideLaterVC.KeyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    func unRegisterForKeboardNotification()
    {
        NotificationCenter.default.removeObserver(self)
    }
    func KeyboardWillShown(_ notification: Notification)
    {
        if self.currentSection == keyIndexSection.pickUpDate.rawValue || self.currentSection == keyIndexSection.adultLagguge.rawValue  || self.currentSection == keyIndexSection.none.rawValue || self.currentSection == keyIndexSection.serviceType.rawValue{
        }else{
            if self.activeTextView == nil || self.activeTextField == nil  {
                return
            }
            let userInfo = notification.userInfo!
            _ = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
            let pointInTable:CGPoint!
            if self.currentSection == keyIndexSection.pickType.rawValue || self.currentSection == keyIndexSection.dropType.rawValue {
                pointInTable = self.activeTextView.superview!.convert(self.activeTextView.frame.origin, to:self.tableViewReservations)
            }else {
                pointInTable = self.activeTextField.convert(self.activeTextField.frame.origin, to:self.tableViewReservations)
            }
            var contentOffset:CGPoint = self.tableViewReservations.contentOffset
            contentOffset.y  = pointInTable.y - 50
            if let accessoryView = self.accessoryViewTextView {
                contentOffset.y -= accessoryView.frame.size.height
            }
            self.tableViewReservations.contentOffset = contentOffset
        }
    }
    func KeyboardWillHide(_ notification: Notification)
    {
        if self.currentSection != 0  {
            self.tableViewReservations.scrollsToTop = true
            self.tableViewReservations.scrollToNearestSelectedRow(at: .middle, animated: true)
            self.tableViewReservations.scrollIndicatorInsets = UIEdgeInsets.zero
        }
    }

    // MARK: - ======== cell button Custom Methods========
    func btnDeleteChildSeatCount(_ sender:UIButton){
        self.view.endEditing(true)
        let tag = sender.superview!.superview!.tag
        self.arrAddedChildSeats.remove(at: tag);
        self.tableViewReservations.reloadData()
    }
    func btnAddChildSeatCountAction(_ sender:UIButton){
        self.view.endEditing(true)
        let name = self.arrDataSource[keyIndexSection.childSeatType.rawValue][Constants.ChildSeatType] as! String
        let count = self.arrDataSource[keyIndexSection.childSeatCount.rawValue][Constants.ChildCount] as! String
        if name == "Child Seat Type"  {
            Globals.ToastAlertWithString("Please select seat type")
            return
        }
        if  count == "Child Seat Count" {
            Globals.ToastAlertWithString("Please select seat count")
            return
        }
        var match = false
        var dicChild = [String:Any]()
        if self.arrAddedChildSeats.count != 0 {
            for (index, var dic) in self.arrAddedChildSeats.enumerated() {
                if dic["name"] as! String == name {
                    let CountInt = count.toInteger()
                    let lastCount = (dic["count"] as! String).toInteger()
                    if  CountInt + lastCount > 10 {
                        dic["count"] = "10" as Any?
                        Globals.ToastAlertWithString("Maximum seat are 10")
                    }else {
                        dic["count"] = String(CountInt + lastCount) as Any?
                    }
                    match = true
                    dicChild = dic
                    self.arrAddedChildSeats.remove(at: index)
                    self.arrAddedChildSeats.insert(dicChild, at: index)
                    break
                }
            }
        }
        if match == false {
            dicChild["name"] = name as Any?
            dicChild["count"] = count as Any?
            self.arrAddedChildSeats.append(dicChild)
        }
        self.tableViewReservations.reloadData()
    }
    @IBAction func btnDisabilityAddRemoveAction(_ sender: UIButton) {
        if sender.isSelected  == true {
            self.isDisable = false
            self.strDisability = "0"
            sender.isSelected = false
        }else {
            self.isDisable = true
            self.strDisability = "1"
            sender.isSelected = true
        }
    }
    @IBAction func btnAddAdultLaggugeAction(_ sender: UIButton) {
        self.isTxtAdultPicker = true
        self.pickerViewCarName.reloadAllComponents()
        self.currentSection = keyIndexSection.adultLagguge.rawValue
        self.activeButton = sender
        //  self.setupPickerViewAgain()
    }
    // MARK:- ====add Pickup and Drop of Address =====
    @IBAction func btnCellPickupAddressTypeAction(_ sender: UIButton) {
        self.currentSection = keyIndexSection.pickType.rawValue
        //  self.strPickType = self.isEditingMode == true ? self.strPickType : pickupType.None.rawValue
        if self.btnPickupTypeLastOne == 0 {
            self.btnPickupTypeLastOne = self.btnPickupTypeTag
        }
        self.view.endEditing(true)
        //      self.pushToSelectAddressVC(sender)
        self.showMainAddressVC(section: self.currentSection)

    }
    func setAddressTypeForButton(_ isPickup:Bool){
        if isPickup == true {
            switch self.btnPickupTypeTag {
            case btnTag.btnAddress.rawValue :
                self.strPickType = pickupType.Address.rawValue
            case btnTag.btnAirport.rawValue :
                self.strPickType = pickupType.Airport.rawValue
                break
            case btnTag.btnFBO.rawValue :
                self.strPickType = pickupType.FBO.rawValue
                break
            case btnTag.btnSeaport.rawValue :
                self.strPickType = pickupType.Seaport.rawValue
                break
            default :
                break
            }
        }else {
            switch self.btnDroppOffTypeTag {
            case btnTag.btnAddress.rawValue :
                self.strDropOffType = DropOffType.Address.rawValue
                
            case btnTag.btnAirport.rawValue :
                self.strDropOffType = DropOffType.Airport.rawValue
                break
            case btnTag.btnFBO.rawValue :
                self.strDropOffType = DropOffType.FBO.rawValue
                
                break
            case btnTag.btnSeaport.rawValue :
                self.strDropOffType = DropOffType.Seaport.rawValue
                break
            default :
                break
            }
        }
        // self.reloadTableViewOnMainQueue()
    }

    @IBAction func btnCellDropOffAddressTypeAction(_ sender:UIButton){
        //      self.strDropOffType = self.isEditingMode == true ? self.strDropOffType : DropOffType.None.rawValue
        self.currentSection = keyIndexSection.dropType.rawValue
        if self.btnDroppOffTypeLastOne == 0 {
            self.btnDroppOffTypeLastOne = self.btnDroppOffTypeTag
        }
        self.view.endEditing(true)
        self.showMainAddressVC(section: self.currentSection)
    }
    func btnCellStopsAndPaymentAction(_ sender:UIButton){
        
        if sender.tag == keyIndexSection.stops.rawValue {
            self.currentSection = keyIndexSection.stops.rawValue
            self.pushToSelectAddressVC(sender)
        }else if sender.tag == keyIndexSection.payment.rawValue {
            self.currentSection = keyIndexSection.payment.rawValue
            self.showPaymentAlertController(sender)
        }
        
    }
    func btnCellOptionAction(_ sender:UIButton){
        
        //    let arrDicKeys = (self.arrDataSource[sender.tag]).keys
        //
        //    let strKey = arrDicKeys.first!
        if sender.tag == 1 || sender.tag == 3 {
            if self.strAirport == "toAirPort"  {
                if sender.tag == 3 {
                    self.pushToAirportVC(sender)
                }else {
                    self.pushToSelectAddressVC(sender)
                }
            }else if self.strAirport == "From Airport" {
                if sender.tag == 1 {
                    self.pushToAirportVC(sender)
                }else {
                    self.pushToSelectAddressVC(sender)
                }
            }else {
                self.pushToSelectAddressVC(sender)
            }
        }else  if sender.tag ==  2 {
            self.pushToSelectAddressVC(sender)
        }else  if sender.tag ==  7 {
            self.activeButton = sender
            self.getingVehlciTypesAvailable()
        }else  if sender.tag ==  9 {
            self.activeButton = sender
            self.showPaymentAlertController(sender)
        }
        
    }
    func GetAddressOfCurrentLocationFromAirportView( _ dicAirport:[String:Any], withTag:Int)
    {
        if (dicAirport["airport_lat"]! as! String).toDouble()! == 0.0 {
            return
        }
        if appDelegate.networkAvialable == true {
            appDelegate.showProgressWithText("Loading Address....")
            var  dicReturn = [String:Any]()
            
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async { () -> Void in
                weak var weakSelf = self
                GoogleMapAPIServiceManager.sharedInstance.getLocationAddressUsingLatitude((dicAirport["airport_lat"]! as! String).toDouble()! , longitude: (dicAirport["airport_lng"]! as! String).toDouble()!, withComplitionHandler: { (dicRecievedJSON) -> () in
                    
                    if dicRecievedJSON["status"] as! String == "OK"{
                        dicReturn = Globals.sharedInstance.formatedAddressCompnentsSwift( (dicRecievedJSON ["results"] as! [Any]).first as! [String:Any] as [String : Any])
                        if withTag == keyIndexSection.pickType.rawValue {
                            weakSelf?.arrDataSource[withTag][Constants.PickType] = dicReturn as Any?
                            weakSelf?.dictPickAddess = dicReturn
                            weakSelf?.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                        } else if withTag == keyIndexSection.dropType.rawValue{
                            weakSelf?.arrDataSource[withTag][Constants.DropType] = dicReturn as Any?
                            weakSelf?.dictDropOffAddess = dicReturn
                            weakSelf?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                        }
                        
                        appDelegate.hideProgress()
                        weakSelf?.getDistanceFromPickUpToDropOff()
                        weakSelf?.reloadTableViewOnMainQueue()
                        
                    }else {
                        appDelegate.hideProgress()
                        Globals.ToastAlertForNetworkConnection()
                    }
                })
                { (error) -> () in
                    appDelegate.hideProgress()
                    Globals.ToastAlertForNetworkConnection()
                }
            }
        }
        else {
            Globals.ToastAlertForNetworkConnection()
        }
    }
    // MARK: -  ======== Push to  view Contrtollers ========
    func pushToAirportVC(_ sender:UIButton){
        let arrDicKeys = (self.arrDataSource[self.currentSection]).keys
        let strKey = arrDicKeys.first!
        let strDate = self.arrDataSource[keyIndexSection.pickUpDate.rawValue][Constants.PickUpDate] as? String
        if  strDate == ""   {
            //JLToast.makeText("Please Select Pick-Up-Date", duration: 3.0).show()
            return
        }
        let airportVC = self.storyboard?.instantiateViewController(withIdentifier: "AirportViewController") as! AirportViewController
        if self.currentSection == keyIndexSection.pickType.rawValue{
            self.strAirport = "From Airport"
            airportVC.tagBtnLast = self.btnPickupTypeLastOne
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd"
            let current_Date: Date = dateFormatter.date(from: self.strDateTime)!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
            let date1  = dateFormatter.string(from: current_Date)
            let arrtime = date1.components(separatedBy: " ")
            airportVC.strPickupDate = arrtime[1]
            if self.isEditingMode == true && self.strPickType == pickupType.Airport.rawValue{
                if let dic = self.reservationDetails["pickup_info"] as? [AnyHashable: Any] {
                    airportVC.dicDetails = dic
                    airportVC.editReservation = true
                }
            }else if self.dicOnlyAirport.count > 0 && btnTag.btnAirport.rawValue == self.btnPickupTypeLastOne{
                if let pickDic = self.dicOnlyAirport["pickup"] as? [String:Any]{
                    airportVC.dicDetails = pickDic
                    airportVC.editReservation = false
                }
            }
        }
        else if self.currentSection == keyIndexSection.dropType.rawValue {
            airportVC.tagBtnLast = self.btnDroppOffTypeLastOne
            self.strAirport = "toAirPort"
            if self.isEditingMode == true && self.strDropOffType == DropOffType.Airport.rawValue {
                if let dic = self.reservationDetails["dropoff_info"] as? [AnyHashable: Any] {
                    airportVC.dicDetails = dic
                    airportVC.editReservation = true
                }
            }else if self.dicOnlyAirport.count > 0 && btnTag.btnAirport.rawValue == self.btnDroppOffTypeLastOne{
                if let pickDic = self.dicOnlyAirport["dropoff"] as? [String:Any]{
                    airportVC.dicDetails = pickDic
                    airportVC.editReservation = false
                }
            }
        }
        airportVC.airportInstructions = NSMutableArray(array: self.arrAirportInstuctions)
        airportVC.strReservationType = self.strAirport
        airportVC.strDateTime = self.strDateTime.replacingOccurrences(of: "/", with: "")
        airportVC.finishAirportSelection = { [weak self](dicAirdicAirPortDetails :[AnyHashable: Any]?, tag:Int, isDone:Bool)-> () in
            var dateTime = ""
            self?.isEditingMode = false
            if self?.strAirport == "toAirPort" {
                if let dT = dicAirdicAirPortDetails?["departure_datetime"] as? String{
                    dateTime = dT
                }
            }
            else {
                if let dT = dicAirdicAirPortDetails?["arrival_datetime"] as? String{
                    dateTime = dT
                }
            }
            if !dateTime.isEmpty {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                if let date = dateFormatter.date(from: dateTime){
                    self?.dateSelected = date
                    dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm a"
                    self?.arrDataSource[keyIndexSection.pickUpDate.rawValue][Constants.PickUpDate] = dateFormatter.string(from: date) as Any?
                    self?.strDateTime = Globals.sharedInstance.dateFromTimestamp(date.timeIntervalSinceNow)
                }
            }
            if dicAirdicAirPortDetails?.count == 0 {
                if self?.currentSection == keyIndexSection.pickType.rawValue{
                    if self?.dictPickAddess.count != 0 && isDone == true {
                        self?.btnPickupTypeTag = tag
                        self?.setAddressTypeForButton(true)
                    }
                    else if isDone == false  && self?.dictPickAddess.count != 0   {
                        self?.btnPickupTypeTag = (self?.btnPickupTypeLastOne)!
                        self?.setAddressTypeForButton(true)
                    }
                    else {
                        self?.btnPickupTypeTag = 0
                        self?.strPickType = pickupType.None.rawValue
                    }
                }else if self?.currentSection == keyIndexSection.dropType.rawValue {
                    if self?.dictDropOffAddess.count != 0 && isDone == true{
                        self?.btnDroppOffTypeTag = tag
                        self?.setAddressTypeForButton(false)
                    }
                    else if isDone == false  && self?.dictDropOffAddess.count != 0    {
                        self?.btnDroppOffTypeTag = (self?.btnDroppOffTypeLastOne)!
                        self?.setAddressTypeForButton(false)
                    }
                    else{
                        self?.btnDroppOffTypeTag = 0
                        self?.strDropOffType = pickupType.None.rawValue
                    }
                }
                self?.reloadTableViewOnMainQueue()
                self?.strAirport = ""
                return
            }
            if self?.currentSection == keyIndexSection.pickType.rawValue{
                self?.dicOnlyAirport["pickup"] = dicAirdicAirPortDetails as! [String:Any] as Any?
                self?.pickUpLocation = CLLocation(latitude:(String(describing: dicAirdicAirPortDetails?["airport_lat"]!) ) .toDouble()!, longitude: (String(describing: dicAirdicAirPortDetails?["airport_lng"]!)) .toDouble()!)
                if let notes = self?.dictPickAddess[Constants.PickNotes]{
                    if notes as! String == "Pickup Notes"{
                        self?.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                    }
                }else {
                    self?.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                }
                
                self?.btnPickupTypeTag = (self?.btnPickupTypeTag)!
                self?.setAddressTypeForButton(true)
                self?.GetAddressOfCurrentLocationFromAirportView(dicAirdicAirPortDetails as! [String:Any], withTag: keyIndexSection.pickType.rawValue)
            }else if self?.currentSection == keyIndexSection.dropType.rawValue {
                self?.dicOnlyAirport["dropoff"] = dicAirdicAirPortDetails as! [String:Any] as Any?
                
                self?.dropOffLocation = CLLocation(latitude:(String(describing: dicAirdicAirPortDetails?["airport_lat"]!) ) .toDouble()!, longitude: (String(describing: dicAirdicAirPortDetails?["airport_lng"]!)) .toDouble()!)
                if let notes = self?.dictDropOffAddess[Constants.DropNotes]{
                    if notes as! String == "Drop Off Notes"{
                        self?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                    }
                }else {
                    self?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                }
                self?.btnDroppOffTypeTag = (self?.btnDroppOffTypeTag)!
                self?.setAddressTypeForButton(false)
                self?.GetAddressOfCurrentLocationFromAirportView(dicAirdicAirPortDetails as! [String:Any], withTag: keyIndexSection.dropType.rawValue)
            }
            self?.arrDataSource[(self?.currentSection)!][strKey] = dicAirdicAirPortDetails as Any?
            self?.btnPickupTypeLastOne = (self?.btnPickupTypeTag)!
            self?.btnDroppOffTypeLastOne = (self?.btnDroppOffTypeTag)!
            self?.getDistanceFromPickUpToDropOff()
            self?.reloadTableViewOnMainQueue()
        }
        self.navigationController?.pushViewController(airportVC, animated: true)
    }
    func pushToSelectAddressVC(_ sender:UIButton){
        // let arrDicKeys = (self.arrDataSource[self.currentSection]).keys
        // let strKey = arrDicKeys.first!
        let selectAddrsVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesAddessViewController") as! SearchPlacesAddessViewController
        selectAddrsVC.viewFrom = "rideLater"
        if self.currentSection == keyIndexSection.pickType.rawValue{
            self.strPickType = pickupType.Address.rawValue
            selectAddrsVC.isPickUpAddress = true
            selectAddrsVC.strTypeOfAddress = "Pickup Address"
            if self.isEditingMode == true && self.strPickType == pickupType.Address.rawValue  ||  self.strPickType == pickupType.POI.rawValue{
                if let dic = self.reservationDetails["pickup_info"] as? [String:Any] {
                    selectAddrsVC.dicDetails = dic
                }
            }else if self.dictPickAddess.count > 0 && btnTag.btnAddress.rawValue == self.btnPickupTypeLastOne{
                selectAddrsVC.dicDetails =  self.dictPickAddess
            }
        }
        else if self.currentSection == keyIndexSection.dropType.rawValue {
            self.strDropOffType = DropOffType.Address.rawValue
            selectAddrsVC.tagBtnAddressLast = self.btnDroppOffTypeLastOne
            selectAddrsVC.isPickUpAddress = false
            selectAddrsVC.strTypeOfAddress = "Dropoff Address"
            if self.isEditingMode == true && self.strDropOffType == DropOffType.Address.rawValue || self.strDropOffType ==  DropOffType.POI.rawValue {
                if let dic = self.reservationDetails["dropoff_info"] as? [String:Any] {
                    selectAddrsVC.dicDetails = dic
                }
            }else if self.dictDropOffAddess.count > 0 && btnTag.btnAddress.rawValue == self.btnDroppOffTypeLastOne{
                selectAddrsVC.dicDetails =  self.dictDropOffAddess
            }
        }
        else {
            selectAddrsVC.isPickUpAddress = false
            selectAddrsVC.strTypeOfAddress = "Stops Address"
        }
        selectAddrsVC.finishSelectingAddress = { [weak self](dicDropOff : [String:Any], tag:Int, isDone:Bool) -> () in
            
            self?.isEditingMode = false
            if  self?.currentSection == keyIndexSection.pickType.rawValue {
                
                if dicDropOff.count == 0  {
                    if self?.dictPickAddess.count != 0 && isDone == true{
                        self?.btnPickupTypeTag = tag
                        self?.setAddressTypeForButton(true)
                    }
                    else if isDone == false  && self?.dictPickAddess.count != 0   {
                        self?.btnPickupTypeTag = (self?.btnPickupTypeLastOne)!
                        self?.setAddressTypeForButton(true)
                    }
                    else  {
                        self?.btnPickupTypeTag = 0
                        self?.strPickType = pickupType.None.rawValue
                    }
                    self?.reloadTableViewOnMainQueue()
                    return
                }
                self?.arrDataSource[(self?.currentSection)!][Constants.PickType] = dicDropOff as Any?
                self?.dictPickAddess.removeAll()
                self?.dictPickAddess = dicDropOff as [String : Any]
                if let notes = self?.dictPickAddess[Constants.PickNotes]{
                    if notes as! String == "Pickup Notes"{
                        self?.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                    }
                }else {
                    self?.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                }
                self?.btnPickupTypeLastOne = (self?.btnPickupTypeTag)!
                self?.setAddressTypeForButton(true)
                self?.reloadTableViewOnMainQueue()
                self?.pickUpLocation = CLLocation(latitude: ((((dicDropOff["latitude"]!) as Any) as Any) as AnyObject).doubleValue , longitude: ((dicDropOff["longitude"]!) as AnyObject).doubleValue)
                self?.getDistanceFromPickUpToDropOff()
            }else if self?.currentSection == keyIndexSection.stops.rawValue {
                
                if dicDropOff.count == 0 {
                    return
                }
                var dicStops = [String:Any]()
                dicStops["stop_name"] = dicDropOff["address"]
                dicStops["latitude"] = dicDropOff["latitude"]
                dicStops["longitude"] = dicDropOff["longitude"]
                dicStops["location"] = dicDropOff["address"]
                dicStops["city"] = dicDropOff["locality"]
                dicStops["state"] = dicDropOff["administrative_area_level_1"]
                dicStops["country"] = dicDropOff["country"]
                dicStops["zip"] = dicDropOff["postal_code"]
                self?.arrStops.append(dicStops)
                self?.arrDataSource[keyIndexSection.stops.rawValue][Constants.Stops] = self?.arrStops as Any?
                self?.reloadTableViewOnMainQueue()
                self?.tableViewReservations.setEditing(true, animated: true)
            }else if self?.currentSection == keyIndexSection.dropType.rawValue {
                if dicDropOff.count == 0  {
                    if self?.dictDropOffAddess.count != 0 && isDone == true{
                        self?.btnDroppOffTypeTag = tag
                        self?.setAddressTypeForButton(false)
                    }
                    else if isDone == false  && self?.dictDropOffAddess.count != 0   {
                        self?.btnDroppOffTypeTag = (self?.btnDroppOffTypeLastOne)!
                        self?.setAddressTypeForButton(false)
                    }
                    else {
                        self?.btnDroppOffTypeTag = 0
                        self?.strDropOffType = pickupType.None.rawValue
                    }
                    self?.reloadTableViewOnMainQueue()
                    return
                }
                self?.arrDataSource[keyIndexSection.dropType.rawValue][Constants.DropType] = dicDropOff as Any?
                self?.dictDropOffAddess.removeAll()
                self?.dictDropOffAddess = dicDropOff
                if let notes = self?.dictDropOffAddess[Constants.DropNotes]{
                    if notes as! String == "Drop Off Notes"{
                        self?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                    }
                }else {
                    self?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                }              //  self?.btnDroppOffTypeTag = tag
                self?.setAddressTypeForButton(false)
                self?.btnDroppOffTypeLastOne = (self?.btnDroppOffTypeTag)!
                self?.reloadTableViewOnMainQueue()
                self?.dropOffLocation = CLLocation(latitude: (((dicDropOff["latitude"]!) as Any) as AnyObject).doubleValue , longitude: ((dicDropOff["longitude"]!) as AnyObject).doubleValue)
                
                self?.getDistanceFromPickUpToDropOff()
            }
            
        }
        
        self.navigationController?.pushViewController(selectAddrsVC, animated: true)
    }

    func showServiceSelecttionController(){
        let service = self.storyboard?.instantiateViewController(withIdentifier: "PASelectServiceTypeVC") as! PASelectServiceTypeVC
        service.strServiceType  = self.strServiceType
        service.arrDataServiceType = self.arrDataServiceType
        service.finishServiceSelection = { [weak self] (dicServiceType:[String:Any]?)  in
            if dicServiceType != nil && dicServiceType?.count != 0 {
                self?.arrDataSource[keyIndexSection.serviceType.rawValue][Constants.Service] = dicServiceType
                self?.strServiceType =  dicServiceType!["service_type"] as! String
                self?.setLeftBarbuttonItemTitle(self!.strServiceType)
                self?.strServiceShortCode = dicServiceType!["short_code"] as! String
                
                //                if self.strServiceType.compare("Hourly") == true  {
                //                    self.arrDataSource.insert([Constants.Hourly:"Add Hours"], atIndex: keyIndexSection.Hourly.rawValue)
                //                }
                self!.reloadTableViewOnMainQueue()
            }
        }
        
        self.navigationController?.pushViewController(service, animated: true)
    }
    func showPaymentAlertController(_ sender:UIButton){
        let payment = self.storyboard?.instantiateViewController(withIdentifier: "PASelectPaymentVC") as! PASelectPaymentVC
        
        payment.pushToRequestView = { [weak self] (dicCardDetails:[String:Any]?, cardID:String?) -> () in
            if let _ = dicCardDetails {
                self?.arrDataSource[(self?.currentSection)!][Constants.Payment] = cardID! as Any?
                self?.dicCardDetail = dicCardDetails!
                self?.strPayment = cardID!
            }
            self?.reloadTableViewOnMainQueue()
        }
        self.navigationController?.pushViewController(payment, animated: true)
    }
    func showSeaportAlertController(_ sender:UIButton){
        
        let arrDicKeys = (self.arrDataSource[self.currentSection]).keys
        let strKey = arrDicKeys.first!
        let SeaportVC = self.storyboard?.instantiateViewController(withIdentifier: "PASeaportAddressVC") as! PASeaportAddressVC
        var strTypeOfAddress = ""
        if self.currentSection == keyIndexSection.pickType.rawValue{
            SeaportVC.tagBtnLast = self.btnPickupTypeLastOne
            SeaportVC.isPickUpAddress = true
            strTypeOfAddress = "Pickup Seaport"
            if self.isEditingMode == true && self.strPickType == pickupType.Seaport.rawValue{
                if let dic = self.reservationDetails["pickup_info"] as? [String:Any] {
                    SeaportVC.dicDetails = dic //seaport_details
                }
            }else if self.dictPickAddess.count > 0 && btnTag.btnSeaport.rawValue == self.btnPickupTypeLastOne{
                SeaportVC.dicDetails =  self.dictPickAddess["seaport_details"] as? [String:Any]
            }
        }else if self.currentSection == keyIndexSection.dropType.rawValue {
            SeaportVC.tagBtnLast = self.btnDroppOffTypeLastOne
            SeaportVC.isPickUpAddress = false
            strTypeOfAddress = "Dropoff Seaport"
            if self.isEditingMode == true && self.strDropOffType == DropOffType.Seaport.rawValue {
                if let dic = self.reservationDetails["dropoff_info"] as? [String:Any]
                {
                    SeaportVC.dicDetails = dic
                }
            }else if self.dictDropOffAddess.count > 0 && btnTag.btnSeaport.rawValue == self.btnDroppOffTypeLastOne{
                SeaportVC.dicDetails =  self.dictDropOffAddess["seaport_details"] as? [String:Any]
            }
        }
        SeaportVC.finishSelectingSeaportAddress = { [weak self](dicSeaportDetails :[String:Any]!, tag:NSInteger, isDone:Bool)-> () in
            if dicSeaportDetails.count == 0 {
                self?.isEditingMode = false
                
                if self?.currentSection == keyIndexSection.pickType.rawValue{
                    if self?.dictPickAddess.count != 0  && isDone == true{
                        self?.btnPickupTypeTag = tag
                        self?.setAddressTypeForButton(true)
                    }
                    else if isDone == false  && self?.dictPickAddess.count != 0   {
                        self?.btnPickupTypeTag = (self?.btnPickupTypeLastOne)!
                        self?.setAddressTypeForButton(true)
                    }
                    else {
                        self?.btnPickupTypeTag = 0
                        self?.strPickType = pickupType.None.rawValue
                    }
                }else if self?.currentSection == keyIndexSection.dropType.rawValue {
                    if self?.dictDropOffAddess.count != 0 && isDone == true{
                        self?.btnDroppOffTypeTag = tag
                        self?.setAddressTypeForButton(false)
                    }
                    else if isDone == false  && self?.dictDropOffAddess.count != 0  {
                        self?.btnDroppOffTypeTag = (self?.btnDroppOffTypeLastOne)!
                        self?.setAddressTypeForButton(false)
                    }
                    else{
                        self?.btnDroppOffTypeTag = 0
                        self?.strDropOffType = pickupType.None.rawValue
                    }
                }
                self?.reloadTableViewOnMainQueue()
                return
            }
            self?.arrDataSource[(self?.currentSection)!][strKey] = dicSeaportDetails as Any?
            if self?.currentSection == keyIndexSection.pickType.rawValue{
                self?.btnPickupTypeLastOne = (self?.btnPickupTypeTag)!
                self?.dictPickAddess.removeAll()
                self?.dictPickAddess = dicSeaportDetails
                if let notes = self?.dictPickAddess[Constants.PickNotes]{
                    if notes as! String == "Pickup Notes"{
                        self?.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                    }
                }else {
                    self?.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                }
                if let addressDetails = self?.dictPickAddess["seaport_address"] as? [String:Any] {
                    let lati : Double = CDouble(addressDetails["latitude"] as! Double)
                    let longi : Double = CDouble(addressDetails["longitude"] as! Double)
                    self?.pickUpLocation = CLLocation(latitude: lati, longitude: longi)
                }
                // self?.btnPickupTypeTag = tag
                self?.setAddressTypeForButton(true)
            }else if self?.currentSection == keyIndexSection.dropType.rawValue {
                self?.btnDroppOffTypeLastOne = (self?.btnDroppOffTypeTag)!
                self?.dictDropOffAddess.removeAll()
                self?.dictDropOffAddess = dicSeaportDetails
                if let notes = self?.dictDropOffAddess[Constants.DropNotes]{
                    if notes as! String == "Drop Off Notes"{
                        self?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                    }
                }else {
                    self?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                }
                if let addressDetails = self?.dictPickAddess["seaport_address"] as? [String:Any] {
                    let lati : Double = CDouble(addressDetails["latitude"] as! Double)
                    let longi : Double = CDouble(addressDetails["longitude"] as! Double)
                    self?.dropOffLocation = CLLocation(latitude: lati, longitude: longi)
                }
                // self?.btnDroppOffTypeTag = tag
                self?.setAddressTypeForButton(false)
            }
            self?.reloadTableViewOnMainQueue()
            self?.getDistanceFromPickUpToDropOff()
        }
        SeaportVC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(strTypeOfAddress, imageName: "backArrow")
        self.navigationController?.pushViewController(SeaportVC, animated: true)
    }
    func showFBOAlertController(_ sender:UIButton){
        let arrDicKeys = (self.arrDataSource[self.currentSection]).keys
        let strKey = arrDicKeys.first!
        
        let strDate = self.arrDataSource[keyIndexSection.pickUpDate.rawValue][Constants.PickUpDate] as? String
        if  strDate == ""   {
            //    JLToast.makeText("Please Select Pick-Up-Date", duration: 3.0).show()
            return
        }
        let FBO_VC = self.storyboard?.instantiateViewController(withIdentifier: "PAFBOAddressVC") as! PAFBOAddressVC
        
        
        FBO_VC.strDateTime = self.strDateTime.replacingOccurrences(of: "/", with: "")
        
        
        var strTypeOfAddress = ""
        if self.currentSection == keyIndexSection.pickType.rawValue{
            FBO_VC.tagBtnLast = self.btnPickupTypeLastOne
            FBO_VC.strReservationType = "From Airport"
            FBO_VC.isPickUpAddress = true
            strTypeOfAddress = "Pickup FBO"
            if self.isEditingMode == true && self.strPickType == pickupType.FBO.rawValue{
                if let dic = self.reservationDetails["pickup_info"] as? [String:Any] {
                    FBO_VC.dicDetails = dic
                }
            }else if self.dictPickAddess.count > 0 && btnTag.btnFBO.rawValue == self.btnPickupTypeLastOne{
                FBO_VC.dicDetails =  self.dictPickAddess["fbo_details"] as? [String:Any]
            }
        }else if self.currentSection == keyIndexSection.dropType.rawValue {
            FBO_VC.tagBtnLast = self.btnDroppOffTypeLastOne
            FBO_VC.isPickUpAddress = false
            FBO_VC.strReservationType = "toAirPort"
            strTypeOfAddress = "Dropoff FBO"
            if self.isEditingMode == true && self.strDropOffType == DropOffType.FBO.rawValue {
                if let dic = self.reservationDetails["dropoff_info"] as? [String:Any]{
                    FBO_VC.dicDetails = dic
                }
            }else if self.dictDropOffAddess.count > 0 && btnTag.btnFBO.rawValue == self.btnDroppOffTypeLastOne{
                FBO_VC.dicDetails =  self.dictDropOffAddess["fbo_details"] as? [String:Any]
            }
            
        }
        FBO_VC.finishSelectingFBOAddress = { [weak self](dicFBODetails :[String:Any]!, tag:Int, isDone:Bool)-> () in
            self?.isEditingMode = false
            
            if dicFBODetails.count == 0 {
                if self?.currentSection == keyIndexSection.pickType.rawValue{
                    if self?.dictPickAddess.count != 0 && isDone == true{
                        self?.btnPickupTypeTag = tag
                        self?.setAddressTypeForButton(true)
                    }
                    else if isDone == false  && self?.dictPickAddess.count != 0 {
                        self?.btnPickupTypeTag = (self?.btnPickupTypeLastOne)!
                        self?.setAddressTypeForButton(true)
                    }
                    else {
                        self?.btnPickupTypeTag = 0
                        self?.strPickType = pickupType.None.rawValue
                    }
                }else if self?.currentSection == keyIndexSection.dropType.rawValue {
                    if self?.dictDropOffAddess.count != 0 && isDone == true{
                        self?.btnDroppOffTypeTag = tag
                        self?.setAddressTypeForButton(false)
                    }else if isDone == false  && self?.dictDropOffAddess.count != 0    {
                        self?.btnDroppOffTypeTag = (self?.btnDroppOffTypeLastOne)!
                        self?.setAddressTypeForButton(false)
                    }else {
                        self?.btnDroppOffTypeTag = 0
                        self?.strDropOffType = pickupType.None.rawValue
                    }
                }
                self?.reloadTableViewOnMainQueue()
                return
            }
            self?.arrDataSource[(self?.currentSection)!][strKey] = dicFBODetails as Any?
            if self?.currentSection == keyIndexSection.pickType.rawValue{
                self?.btnPickupTypeLastOne = (self?.btnPickupTypeTag)!
                self?.dictPickAddess.removeAll()
                self?.dictPickAddess = dicFBODetails as [String : Any]
                if let notes = self?.dictPickAddess[Constants.PickNotes]{
                    if notes as! String == "Pickup Notes"{
                        self?.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                    }
                }else {
                    self?.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                }
                if let addressDetails = self?.dictPickAddess["address"] as? [String:Any] {
                    let lati : Double = CDouble(addressDetails["latitude"] as! Double)
                    let longi : Double = CDouble(addressDetails["longitude"] as! Double)
                    self?.pickUpLocation = CLLocation(latitude:lati , longitude:longi )
                }
                // self?.btnPickupTypeTag = tag
                self?.setAddressTypeForButton(true)
            }else if self?.currentSection == keyIndexSection.dropType.rawValue {
                self?.btnDroppOffTypeLastOne = (self?.btnDroppOffTypeTag)!
                self?.dictDropOffAddess.removeAll()
                self?.dictDropOffAddess = dicFBODetails as [String : Any]
                if let notes = self?.dictDropOffAddess[Constants.DropNotes]{
                    if notes as! String == "Drop Off Notes"{
                        self?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                    }
                }else {
                    self?.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                }
                if let addressDetails = self?.dictPickAddess["address"] as? [String:Any] {
                    let lati : Double = CDouble(addressDetails["latitude"] as! Double)
                    let longi : Double = CDouble(addressDetails["longitude"] as! Double)
                    self?.dropOffLocation = CLLocation(latitude: lati, longitude: longi)
                }
                // self?.btnDroppOffTypeTag = tag
                self?.setAddressTypeForButton(false)
            }
            self?.reloadTableViewOnMainQueue()
            self?.getDistanceFromPickUpToDropOff()
        }
        FBO_VC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom( strTypeOfAddress, imageName: "backArrow")
        self.navigationController?.pushViewController(FBO_VC, animated: true)
    }
    func pushToSelectVehicle()  {
        if let arrKeeys : [String]? = [String](self.dicDisAndTime.keys)
        {
            if arrKeeys!.contains("duration"){
            }
        }else {
            //  self.calculateEstimateFareReservation()
            self.getDistanceFromPickUpToDropOff()
        }
        let paramDic = self.addParamtersToService_FareAPI()
        if paramDic.count == 0 {
            return
        }
  
        let vehicleVC = self.storyboard?.instantiateViewController(withIdentifier: "PASelectVehicleVC") as! PASelectVehicleVC
        vehicleVC.paramDic = paramDic
        vehicleVC.dicWebServiceParam = self.dicWebServiceParam
        vehicleVC.strServiceCode = self.strServiceShortCode
        
        vehicleVC.finishEditReservation = { [weak self] (dicReservation:[String:Any]?) in
            if dicReservation == nil {
                
                if Constants.countyApp == "1"{
                    
                    if let btnSubmit = self?.view.viewWithTag(996633) as? UIButton {
                        btnSubmit.setTitle("Select Vehicle", for: .normal)
                        btnSubmit.backgroundColor = UIColor.appRideLater_RedColor()
                        btnSubmit.titleLabel?.font = Globals.defaultAppFontWithBold(14)
                        Globals.layoutViewFor(btnSubmit, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 3)
                    }
                    self?.resetFieldsForReuse()
                    
                }else{
                    self?.dismiss(animated: true, completion: nil)
                }
            }
        }
        vehicleVC.finishVehicleSelection = {[weak self] (dicVehicle) in
            
            if let btnSubmit = self?.view.viewWithTag(996633) as? UIButton {
                btnSubmit.setTitle("Confirm Reservation", for: .normal)
//                btnSubmit.backgroundColor = UIColor.appRideLater_RedColor()
//                btnSubmit.titleLabel?.font = Globals.defaultAppFontWithBold(14)
//                Globals.layoutViewFor(btnSubmit, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 3)
            }
            
            if dicVehicle != nil && dicVehicle?.count != 0 {
                self?.dictSelectedVehicleData = dicVehicle!
                if let dicValues =  dicVehicle as? [String:Any] {
                    self!.strVehicle =  dicValues["vehicle_type_title"] as! String
                    if let adut_cap = dicValues["vehicle_specific_passenger_capacity"]{
                        let txtAdult = String(describing: adut_cap).count > 0 ?  String(describing: adut_cap) : "01"
                        self?.txtAdult.text = txtAdult
                     //   self!.arrAdults.removeAll()
                      //  let loop = Int(txtAdult)!
                       // for  i  in  1...loop {
                       //     self?.arrAdults.append(String(i))
                        //}
                    }
                    if let adut_cap = dicValues["vehicle_specific_luggage_capacity"] as? [String:Any] {
                        let txtLuggage =  String(describing: adut_cap).count > 0 ?  String(describing: adut_cap) : "01"
                        self!.arrLuggage.removeAll(keepingCapacity: true)
                        self?.txtLagguge.text = txtLuggage
//                        let loop = Int(txtLuggage)!
//                        for  i  in  0...loop {
//                            self?.arrLuggage.append(String(i))
//                        }
                    }
                    self?.reloadTableViewOnMainQueue()
                    
                    //  self?.calculateEstimateFareReservation()
                }
            }
        }
        self.navigationController?.pushViewController(vehicleVC, animated: true)
    }

    func showMainAddressVC(section:Int)  {
        let strTitle = self.arrDataSource[keyIndexSection.pickUpDate.rawValue][Constants.PickUpDate] as? String
        if  strTitle?.compare("Pickup Date") == true || self.dateSelected == nil  {
            self.showBannerWithMessage("Please select pick-up date.")
            return
        }
        let addressModal = AddressDetails()
        addressModal.airportInstructions = self.arrAirportInstuctions
        addressModal.currentSection = section
        addressModal.dateSelected = self.dateSelected
        addressModal.strDateTime = self.arrDataSource[keyIndexSection.pickUpDate.rawValue][Constants.PickUpDate] as! String
        addressModal.isPickUpAddress = section == keyIndexSection.pickType.rawValue ? true : false
        addressModal.strAirport = section == keyIndexSection.pickType.rawValue ? "From Airport" : "toAirPort"
        addressModal.strAirportReservationType =  addressModal.strAirport
        addressModal.viewFrom = "rideLater"
        addressModal.serviceType = self.strServiceType
        addressModal.PickupAddress = section == keyIndexSection.dropType.rawValue ? self.getPickupAddressWith().Addres : "N/A"
        addressModal.isEditingMode = self.isEditingMode
        if addressModal.isPickUpAddress == false {
            if self.strDropOffType != DropOffType.None.rawValue {
                addressModal.strDropOffType = self.strDropOffType
                addressModal.dicDetails = self.addDropOffAddressDetailsToChangeAddress()
            }
        }else {
            if self.strPickType != pickupType.None.rawValue {
                addressModal.strPickupType = self.strPickType
                addressModal.dicDetails = self.addPickUpAddressDetailsToChangeAddress()
            }
        }
        let mainAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "PAMainAddressVC") as! PAMainAddressVC
        mainAddressVC.addressModal = addressModal
        mainAddressVC.finishSelectionAddress = { [weak self] (dicAddress, modal, isDone) in
            if dicAddress != nil && isDone == true  {
                self?.isEditingMode = false
                
                if section == keyIndexSection.pickType.rawValue {
                    self?.setupPickupAddressValues(dicData: dicAddress!, modal: modal)
                }
                else {
                    self?.setupDropOffAddressValues(dicData: dicAddress!, modal: modal)
                }
            }
        }
        let navi = UINavigationController(rootViewController: mainAddressVC)
        navi.navigationBar.isOpaque = true
        
        self.navigationController?.present(navi, animated: true, completion: nil) //.pushViewController(mainAddressVC, animated: true)//show(mainAddressVC, sender: self)//pushViewController(mainAddressVC, animated: true)
    }
    func addPickUpAddressDetailsToChangeAddress() -> [String:Any] {
        switch self.strPickType {
        case pickupType.Airport.rawValue:
            if self.isEditingMode == true {
                if let dic = self.reservationDetails["pickup_info"] as? [String: Any] {
                    return dic
                }
            }else if self.dicOnlyAirport.count > 0 {
                if let pickDic = self.dicOnlyAirport["pickup"] as? [String:Any]{
                    return pickDic
                }
            }
        case pickupType.Address.rawValue, pickupType.POI.rawValue:
            if self.isEditingMode == true {
                if let dic = self.reservationDetails["pickup_info"] as? [String: Any] {
                    return dic
                }
            }else if self.dictPickAddess.count > 0 {
                return self.dictPickAddess
            }
        case pickupType.Seaport.rawValue:
            if self.isEditingMode == true {
                if let dic = self.reservationDetails["pickup_info"] as? [String: Any] {
                    return dic
                }
            }else if self.dictPickAddess.count > 0 {
                return self.dictPickAddess["seaport_details"] as! [String:Any]
            }
        case pickupType.FBO.rawValue:
            if self.isEditingMode == true {
                if let dic = self.reservationDetails["pickup_info"] as? [String: Any] {
                    return dic
                }
            }else if self.dictPickAddess.count > 0 {
                return  self.dictPickAddess["fbo_details"] as! [String:Any]
            }
        default :
            return [String:Any]()
        }
        return [String:Any]()
    }
    func addDropOffAddressDetailsToChangeAddress() -> [String:Any] {
        switch self.strDropOffType {
        case DropOffType.Airport.rawValue:
            
            if self.isEditingMode == true {
                if let dic = self.reservationDetails["dropoff_info"] as? [String: Any] {
                    return dic
                }
            }else if self.dicOnlyAirport.count > 0 {
                if let pickDic = self.dicOnlyAirport["dropoff"] as? [String:Any]{
                    return pickDic
                }
            }
        case DropOffType.Address.rawValue, DropOffType.POI.rawValue:
            if self.isEditingMode == true {
                if let dic = self.reservationDetails["dropoff_info"] as? [String: Any] {
                    return dic
                }
            }else if self.self.dictDropOffAddess.count > 0 {
                return self.self.dictDropOffAddess
            }
        case DropOffType.Seaport.rawValue:
            if self.isEditingMode == true {
                if let dic = self.reservationDetails["dropoff_info"] as? [String: Any] {
                    return dic
                }
            }else if self.dictDropOffAddess.count > 0 {
                if let dic = self.dictDropOffAddess["seaport_details"] as? [String:Any] {
                    return dic
                }
                //  return self.dictDropOffAddess["seaport_details"] as? [String:Any]
            }
        case DropOffType.FBO.rawValue:
            if self.isEditingMode == true {
                if let dic = self.reservationDetails["dropoff_info"] as? [String: Any] {
                    return dic
                }
            }else if self.dictDropOffAddess.count > 0 {
                if let dic = self.dictDropOffAddess["fbo_details"] as? [String:Any]{
                    return dic
                }
            }
        default :
            return [String:Any]()
        }
        return [String:Any]()
    }
    func setupPickupAddressValues(dicData:[String:Any], modal:AddressDetails)  {
        self.strPickType = modal.strPickupType
        if dicData.count == 0  {
            self.strPickType = pickupType.None.rawValue
            self.reloadTableViewOnMainQueue()
            return
        }
        switch self.strPickType {
        case pickupType.Address.rawValue, pickupType.POI.rawValue:
            self.arrDataSource[keyIndexSection.pickType.rawValue][Constants.PickType] = dicData as Any?
            self.dictPickAddess.removeAll()
            self.dictPickAddess = dicData as [String : Any]
            if let notes = self.dictPickAddess[Constants.PickNotes]{
                if notes as! String == "Pickup Notes"{
                    self.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                }
            }else {
                self.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
            }
            self.reloadTableViewOnMainQueue()
            self.pickUpLocation = CLLocation(latitude: ((((dicData["latitude"]!) as Any) as Any) as AnyObject).doubleValue , longitude: ((dicData["longitude"]!) as AnyObject).doubleValue)
            self.getDistanceFromPickUpToDropOff()
            break
        case pickupType.Airport.rawValue:
            var dateTime = ""
            if let dT = dicData["arrival_datetime"] as? String{
                dateTime = dT
            }
            if !dateTime.isEmpty {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                if let date = dateFormatter.date(from: dateTime){
                    self.dateSelected = date
                    dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm a"
                    self.arrDataSource[keyIndexSection.pickUpDate.rawValue][Constants.PickUpDate] = dateFormatter.string(from: date) as Any?
                    self.strDateTime = Globals.sharedInstance.dateFromTimestamp(date.timeIntervalSinceNow)
                }
            }
            self.dicOnlyAirport["pickup"] = dicData
            var lat = "0.0"
            var lng = "0.0"
            if let latitude = dicData["airport_lat"] {
                lat = String(describing: latitude)
            }
            if let latitude = dicData["latitude"] {
                lat = String(describing: latitude)
            }
            if let longitude = dicData["airport_lng"] {
                lng = String(describing: longitude)
            }
            if let longitude = dicData["longitude"] {
                lng = String(describing: longitude)
            }
            self.pickUpLocation = CLLocation(latitude:lat .toDouble()!, longitude: lng.toDouble()!)
            if let notes = self.dictPickAddess[Constants.PickNotes]{
                if notes as! String == "Pickup Notes"{
                    self.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                }
            }else {
                self.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
            }
            self.GetAddressOfCurrentLocationFromAirportView(dicData , withTag: keyIndexSection.pickType.rawValue)
            break
        case pickupType.Seaport.rawValue:
            self.arrDataSource[keyIndexSection.pickType.rawValue][Constants.PickType] = dicData as Any?
            self.dictPickAddess.removeAll()
            self.dictPickAddess = dicData
            if let notes = self.dictPickAddess[Constants.PickNotes]{
                if notes as! String == "Pickup Notes"{
                    self.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
                }
            }else {
                self.dictPickAddess[Constants.PickNotes] = "Pickup Notes" as Any?
            }
            if let addressDetails = self.dictPickAddess["seaport_address"] as? [String:Any] {
                let lati : Double = CDouble(addressDetails["latitude"] as! Double)
                let longi : Double = CDouble(addressDetails["longitude"] as! Double)
                self.pickUpLocation = CLLocation(latitude: lati, longitude: longi)
            }
            self.reloadTableViewOnMainQueue()
            self.getDistanceFromPickUpToDropOff()
            break
        case pickupType.FBO.rawValue:
            self.arrDataSource[keyIndexSection.pickType.rawValue][Constants.PickType] = dicData as Any?
            self.dictPickAddess.removeAll()
            self.dictPickAddess = dicData

            if let dic_details = dicData["fbo_details"] as? [String : Any]{
                if let notes = dic_details[Constants.PickNotes]{
                    if "\(notes)" == "Pickup Notes" || "\(notes)".isEmpty{
                        self.dictPickAddess[Constants.PickNotes] = "Pickup Notes"
                    }
                else {
                    self.dictPickAddess[Constants.PickNotes] = "\(notes)"
                }
            }else if let notes = dic_details["notes"]
                {
                    self.dictPickAddess[Constants.PickNotes] = "\(notes)"
                }
                var lati : Double = 0.0
                var longi : Double = 0.0
                
                if let lat = dic_details["latitude"]{
                    lati =  Double("\(lat)")!
                }
                if let lng = dic_details["longitude"]{
                    longi = Double("\(lng)")!
                }
                
                if lati == 0.0 || longi == 0.0 {
                    
                    if let fbo_address = dicData["fbo_address"] as? [String : Any]{
                        if let lat = fbo_address["latitude"]{
                            lati =  Double("\(lat)")!
                        }
                        if let lng = fbo_address["longitude"]{
                            longi = Double("\(lng)")!
                        }
                    }
                }
          
                    self.pickUpLocation = CLLocation(latitude:lati , longitude:longi )
            }
            
            self.reloadTableViewOnMainQueue()
            self.getDistanceFromPickUpToDropOff()
            break
        default:
            break
        }
    }
    func setupDropOffAddressValues(dicData:[String:Any], modal:AddressDetails)  {
        self.strDropOffType = modal.strDropOffType
        if dicData.count == 0  {
            self.strDropOffType = DropOffType.None.rawValue
            self.reloadTableViewOnMainQueue()
            return
        }
        
        switch self.strDropOffType {
        case DropOffType.Address.rawValue, DropOffType.POI.rawValue:
            self.arrDataSource[keyIndexSection.dropType.rawValue][Constants.DropType] = dicData as Any?
            self.dictDropOffAddess.removeAll()
            self.dictDropOffAddess = dicData as [String : Any]
            if let notes = self.dictDropOffAddess[Constants.DropNotes]{
                if notes as! String == "Drop Off Notes"{
                    self.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                }
            }else {
                self.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
            }
            self.reloadTableViewOnMainQueue()
            self.dropOffLocation = CLLocation(latitude: ((((dicData["latitude"]!) as Any) as Any) as AnyObject).doubleValue , longitude: ((dicData["longitude"]!) as AnyObject).doubleValue)
            self.getDistanceFromPickUpToDropOff()
            break
        case DropOffType.Airport.rawValue:
            var dateTime = ""
            self.strAirport = modal.strAirportReservationType
            if self.strAirport == "toAirPort" {
                if let dT = dicData["departure_datetime"] as? String{
                    dateTime = dT
                }
            }
            if !dateTime.isEmpty {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                if let date = dateFormatter.date(from: dateTime){
                    self.dateSelected = date
                    dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm a"
                    self.arrDataSource[keyIndexSection.pickUpDate.rawValue][Constants.PickUpDate] = dateFormatter.string(from: date) as Any?
                    self.strDateTime = Globals.sharedInstance.dateFromTimestamp(date.timeIntervalSinceNow)
                }
            }
            
            self.dicOnlyAirport["dropoff"] = dicData as! [String:Any] as Any?
            self.dropOffLocation = CLLocation(latitude:(String(describing: dicData["airport_lat"]!) ) .toDouble()!, longitude: (String(describing: dicData["airport_lng"]!)) .toDouble()!)
            if let notes = self.dictDropOffAddess[Constants.DropNotes]{
                if notes as! String == "Drop Off Notes"{
                    self.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                }
            }else {
                self.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
            }
            self.GetAddressOfCurrentLocationFromAirportView(dicData as! [String:Any], withTag: keyIndexSection.dropType.rawValue)
            break
        case DropOffType.Seaport.rawValue:
            self.arrDataSource[keyIndexSection.dropType.rawValue][Constants.DropType] = dicData as Any?
            self.dictDropOffAddess.removeAll()
            self.dictDropOffAddess = dicData
            if let notes = self.dictDropOffAddess[Constants.DropNotes]{
                if notes as! String == "Drop Off Notes"{
                    self.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                }
            }else {
                self.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
            }
            if let addressDetails = self.dictDropOffAddess["seaport_address"] as? [String:Any] {
                let lati : Double = CDouble(addressDetails["latitude"] as! Double)
                let longi : Double = CDouble(addressDetails["longitude"] as! Double)
                self.dropOffLocation = CLLocation(latitude: lati, longitude: longi)
            }
            self.reloadTableViewOnMainQueue()
            self.getDistanceFromPickUpToDropOff()
            break
        case DropOffType.FBO.rawValue:
            self.arrDataSource[keyIndexSection.dropType.rawValue][Constants.DropType] = dicData as Any?
            self.dictDropOffAddess.removeAll()
            self.dictDropOffAddess = dicData

            if let dic_details = dicData["fbo_details"] as? [String : Any]{
                if let notes = dic_details[Constants.DropNotes]{
                    if "\(notes)" == "Drop Off Notes" || "\(notes)".isEmpty{
                        self.dictDropOffAddess[Constants.DropNotes] = "Drop Off Notes" as Any?
                    }else {
                        self.dictDropOffAddess[Constants.DropNotes] = "\(notes)"
                    }
                }else if let notes = dic_details["notes"]
                {
                     self.dictDropOffAddess[Constants.DropNotes] = "\(notes)"
                }
                var lati : Double = 0.0
                var longi : Double = 0.0
               
                if let lat = dic_details["latitude"]{
                    lati =  Double("\(lat)")!
                }
                if let lng = dic_details["longitude"]{
                    longi = Double("\(lng)")!
                }
                
                if lati == 0.0 || longi == 0.0 {
                
                 if let fbo_address = dicData["fbo_address"] as? [String : Any]{
                    if let lat = fbo_address["latitude"]{
                        lati =  Double("\(lat)")!
                    }
                    if let lng = fbo_address["longitude"]{
                        longi = Double("\(lng)")!
                    }
                }
                }
                
                    self.dropOffLocation = CLLocation(latitude: lati, longitude: longi)
            }
            
          
            self.reloadTableViewOnMainQueue()
            self.getDistanceFromPickUpToDropOff(isPushToVehicle: false)
            break
        default:
            break
        }
        
    }
    // MARK: - ====calculate distance between pickp and drop off =====
    func getDistanceFromPickUpToDropOff( isPushToVehicle:Bool? = false ){
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async { [weak self]() -> Void in
            if  self?.pickUpLocation == nil || self?.dropOffLocation == nil {
                //Globals.ToastAlertWithString("Please enter Pick-up Address")
                return
            }
            
            let urlStr = Globals.sharedInstance.getFullApiAddressForDistanceToDriverDistance(self?.pickUpLocation, destinationLoc: (self?.dropOffLocation)!)
            if urlStr == "" {
                return
            }
            ServiceManager.sharedInstance.dataTaskWithGetRequest(urlStr, successBlock: { (dicData) -> Void in
                let arrRoutes = dicData["routes"] as! [[String : Any]]
                if arrRoutes.count > 0 {
                    let dicMainRoutes = arrRoutes.first!
                    let arrLegs = dicMainRoutes["legs"] as! [[String:Any]]
                    self?.dicDisAndTime["distance"] = arrLegs.first!["distance"]!
                    self?.dicDisAndTime["duration"] = arrLegs.first!["duration"]!
                    if isPushToVehicle! {
                        self?.pushToSelectVehicle()
                    }else {
                        // self?.calculateEstimateFareReservation()
                    }
                    
                }
            }, failureBlock: { (error) -> () in
                appDelegate.hideProgress()
                print("Error \(String(describing: error?.description))")
            })
        }
    }
    // MARK: - ====calculate Estimate fare between pickp and drop off =====
    func addParamtersToService_FareAPI() -> [String:Any] {
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return [String:Any]()
        }
        if  self.strServiceType == "" || self.arrDataSource[keyIndexSection.pickType.rawValue][Constants.PickType] is String  {
            self.showBannerWithMessage(self.strServiceType == "" ? "Please provide service type." : "Please add pick-Up Address.")

            return [String:Any]()
        }
        var time_U : String = "0.0"
        var distance_U : String = "0.0"
        
        var time_S : String = "0.0"
        var distance_M : String = "0.0"
        
        if let arrKeeys = [String](self.dicDisAndTime.keys) as? [String]
        {
            if arrKeeys.contains("duration"){
                if let DicTime = self.dicDisAndTime["duration"] as? [String:Any]{
                    if let time = DicTime["value"]{
                        time_U = "\((time as! Double) / 60 )"
                        time_S = "\(time)"
                    }
                }
                if let Dicdistance = self.dicDisAndTime["distance"] as? [String:Any]{
                    if let dis = Dicdistance["value"]{
                        distance_U = "\((dis  as! Float) * appDelegate.distanceMultiplier)"
                        distance_M = "\(dis)"
                    }
                }
            }
        }else {
            return [String:Any]()
        }
        
        var param_Dictionary: [String : Any] = [String : Any]()
        
        if Constants.isBookerLogin == true
        {
            param_Dictionary["user_id"] = "\(appDelegate.activeUserDetailsForBooker["id"]!)"
            param_Dictionary["is_booker"] = "yes"
        }
        else
        {
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            param_Dictionary["user_id"] = user_ID
            param_Dictionary["session"] = session
            param_Dictionary["is_booker"] = "no"

        }
        
        param_Dictionary["company_id"] = Constants.CompanyID
        if self.dictSelectedVehicleData.count != 0{
            param_Dictionary["vechile"] = self.dictSelectedVehicleData["id"]
        }
        if !(self.arrDataSource[keyIndexSection.serviceType.rawValue][Constants.Service] is String){
            let dic = self.arrDataSource[keyIndexSection.serviceType.rawValue][Constants.Service] as! [String:Any]
            param_Dictionary["service_type"] = dic["id"]
        }
        param_Dictionary["distance_type"] = appDelegate.distanceType as Any?
        param_Dictionary["template_type"] = "ptp" as Any?
      
        param_Dictionary["distance"] = "\(distance_U)"
        param_Dictionary["time"] = "\(time_U)"
        
        param_Dictionary["time_seconds"] = "\(time_S)"
        param_Dictionary["distance_meter"] = "\(distance_M)"
        
        param_Dictionary["pickup_type"] = (self.strPickType == pickupType.None.rawValue) ? "" : self.strPickType.lowercased()
        param_Dictionary["drop_type"] = (self.strDropOffType == DropOffType.None.rawValue) ? "" : self.strDropOffType.lowercased()
        
        if self.strPickType == pickupType.Address.rawValue ||  self.strPickType == pickupType.POI.rawValue
        {
            if let dicPickupAddress = self.arrDataSource[keyIndexSection.pickType.rawValue][Constants.PickType] as? [String:Any] {
                param_Dictionary["pickup_location"] = dicPickupAddress["address"]
                param_Dictionary["pickup_latitude"] = dicPickupAddress["latitude"]
                param_Dictionary["pickup_longitude"] = dicPickupAddress["longitude"]
                param_Dictionary["zip_from"] = dicPickupAddress["postal_code"]
            }
        }
        else if self.strPickType == pickupType.Airport.rawValue
        {
            self.dicWebServiceParam["pickup_type"] = "airport" as Any?
            if let airportInfo = self.dicOnlyAirport["pickup"] as? [String:Any]
            {
                if let dicDropOffAddress = self.arrDataSource[keyIndexSection.pickType.rawValue][Constants.PickType] as? [String:Any] {
                    param_Dictionary["pickup_location"] = dicDropOffAddress["address"]
                    param_Dictionary["zip_from"] = dicDropOffAddress["postal_code"]
                    param_Dictionary["pickup_latitude"] =  dicDropOffAddress["airport_lat"]
                    param_Dictionary["pickup_longitude"] = dicDropOffAddress["airport_lng"]
                    
                    param_Dictionary["pu_airport_code"] = airportInfo["iata"]
                }
            }
        }
        else if self.strPickType == pickupType.Seaport.rawValue
        {
            if self.dictPickAddess.count != 0 {
                let addressDetails = self.dictPickAddess["seaport_address"] as! [String:Any]
                param_Dictionary["pickup_location"] = addressDetails["address"]
                param_Dictionary["pickup_latitude"] = addressDetails["latitude"]
                param_Dictionary["pickup_longitude"] = addressDetails["longitude"]
                if let sea_add = self.dictPickAddess["seaport_details"] as? [String:Any]
                {
                    param_Dictionary["zip_from"] = sea_add["postal_code"]
                }else {
                    param_Dictionary["zip_from"] = ""
                }
            }
        }
        else if self.strPickType == pickupType.FBO.rawValue {
            if self.dictPickAddess.count != 0 {
                let addressDetails = self.dictPickAddess["fbo_details"] as! [String:Any]
                param_Dictionary["pickup_location"] = addressDetails["address"]
                param_Dictionary["pickup_latitude"] = addressDetails["latitude"]
                param_Dictionary["pickup_longitude"] = addressDetails["longitude"]
                
                if let fbo_add = self.dictPickAddess["fbo_address"] as? [String:Any]
                {
                    param_Dictionary["zip_from"] = fbo_add["postal_code"]
                }else {
                    param_Dictionary["zip_from"] = ""
                }
                param_Dictionary["pu_airport_code"] = addressDetails["iata"]
            }
        }
        
        if self.strDropOffType == DropOffType.Address.rawValue || self.strDropOffType ==  DropOffType.POI.rawValue
        {
            // for pickup
            if let dicPickupAddress = self.arrDataSource[keyIndexSection.dropType.rawValue][Constants.DropType] as? [String:Any] {
                param_Dictionary["dropoff_location"] = dicPickupAddress["address"]
                param_Dictionary["dropoff_latitude"] = dicPickupAddress["latitude"]
                param_Dictionary["dropoff_longitude"] = dicPickupAddress["longitude"]
                param_Dictionary["zip_to"] = dicPickupAddress["postal_code"]
            }
        }
        else if self.strDropOffType == DropOffType.Airport.rawValue
        {
            if let airportInfo = self.dicOnlyAirport["dropoff"] as? [String:Any]{
                
                if let dicDropOffAddress = self.arrDataSource[keyIndexSection.dropType.rawValue][Constants.DropType] as? [String:Any] {
                    param_Dictionary["dropoff_location"] = dicDropOffAddress["address"]
                    param_Dictionary["dropoff_latitude"] =  dicDropOffAddress["airport_lat"]
                    param_Dictionary["dropoff_longitude"] = dicDropOffAddress["airport_lng"]
                    param_Dictionary["zip_to"] = dicDropOffAddress["postal_code"]
                    param_Dictionary["do_airport_code"] = airportInfo["iata"]
                }
            }
        }
        else if self.strDropOffType == DropOffType.Seaport.rawValue
        {
            if self.dictDropOffAddess.count != 0 {
                let addressDetails = self.dictDropOffAddess["seaport_address"] as! [String:Any]
                param_Dictionary["dropoff_latitude"] = addressDetails["latitude"]
                param_Dictionary["dropoff_longitude"] = addressDetails["longitude"]
                
                param_Dictionary["dropoff_location"] = addressDetails["address"]
                
                
                if let sea_add = self.dictDropOffAddess["seaport_details"] as? [String:Any]
                {
                    param_Dictionary["zip_to"] = sea_add["postal_code"]
                }else {
                    param_Dictionary["zip_to"] = ""
                }
                
            }
        } else if self.strDropOffType == DropOffType.FBO.rawValue
        {
            if self.dictDropOffAddess.count != 0 {
                let addressDetails = self.dictDropOffAddess["fbo_details"] as! [String:Any]
                param_Dictionary["dropoff_location"] = addressDetails["address"]
                param_Dictionary["dropoff_latitude"] = addressDetails["latitude"]
                param_Dictionary["dropoff_longitude"] = addressDetails["longitude"]
                if let fbo_add = self.dictDropOffAddess["fbo_address"] as? [String:Any]
                {
                    param_Dictionary["zip_to"] = fbo_add["postal_code"]
                }else {
                    param_Dictionary["zip_to"] = ""
                }
                param_Dictionary["do_airport_code"] = addressDetails["iata"]
            }
        }
        
        
        if strServiceType.compare("Hourly") {
            if self.strHours == "0" {
                //strMessage += "Add Hours \n"
            }else {
                let minHr = self.minimumJobHours.toDouble()
                var inputHours : Double? =  0.0
                if self.strHours.compare("0") {
                    inputHours = String(self.strHours).toDouble()
                }else if self.strHours.contains("Minimum") {
                    let arrStrHours = self.strHours.components(separatedBy: " ")
                    inputHours = String(arrStrHours[1]).toDouble()
                }else {
                    inputHours = String(self.strHours).toDouble()
                }
                if minHr > inputHours{
                    Globals.ToastAlertWithString("Selected hour(s) should be greater or equal to minimum hour(s)")
                    //return
                }
                else {
                    param_Dictionary["hourly_time"] = self.strHours as Any?
                }
            }
        }
        
        return param_Dictionary
    }
    func calculateEstimateFareReservation() {
        return
        // let estFare = estDistance * Float(CarObject.base_per_km)!
        let param_Dictionary = self.addParamtersToService_FareAPI()
        if param_Dictionary.count == 0 {
            
            return
        }
        appDelegate.showProgressWithText("Calculating Estimated Fare")
        ServiceManager.sharedInstance.dataTaskWithPostRequest("get_fare_quote", dicParameters: param_Dictionary, successBlock: {[weak self] (dicData) -> Void in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success"{
                if let totalAmt = dicData["totalamount"]
                {
                    if String(describing: totalAmt)  == "0"
                    {
                        self?.strEstimateFare = "N/A"
                        let alert = self?.alertView("Alert", message:"No rates are available for this request, please contact your customer support.", delegate:nil, cancelButton:"Ok", otherButons:nil)
                        alert!.show()
                    }
                    else {
                        self?.strEstimateFare = "\(appDelegate.currency!) \(totalAmt)"
                    }
                }
                self?.reloadTableViewOnMainQueue()
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
            }, failureBlock: { (error) -> () in
                appDelegate.hideProgress()
                print(error)
                Globals.ToastAlertWithString("Netowrk Error")
        })
    }



    // MARK: - ======== Webservice call Methods========

    func addParamterForPickupAddressWithType() -> Bool
    {
        
        if self.strPickType == pickupType.Address.rawValue ||  self.strPickType == pickupType.POI.rawValue
        {
            self.dicWebServiceParam["pickup_type"] = self.strPickType.lowercased() as Any?
            // for pickup
            if self.dictPickAddess.count != 0  {
                self.dicWebServiceParam["pickup"] = self.dictPickAddess["address"]
                self.dicWebServiceParam["pickup_latitude"] = self.dictPickAddess["latitude"]
                self.dicWebServiceParam["pickup_longitude"] = self.dictPickAddess["longitude"]
                var dicPickup = [String:Any]()
                dicPickup["location"] = self.dictPickAddess["address"]
                dicPickup["address"] = self.dictPickAddess["address"]
                dicPickup["city"] = self.dictPickAddess["locality"] != nil ? self.dictPickAddess["locality"] : (self.dictPickAddess["city"] != nil ? self.dictPickAddess["city"]  : "")
                
                dicPickup["state"] =  self.dictPickAddess["administrative_area_level_1"] != nil ?  self.dictPickAddess["administrative_area_level_1"] : ( self.dictPickAddess["state"] != nil ?  self.dictPickAddess["state"]  : "")
                
                dicPickup["country"] = self.dictPickAddess["country"]
                
                //
                //                if let zip = self.dictPickAddess["postal_code"]{
                //                    if "\(zip)".isEmpty{
                //                        if let zip = self.dictPickAddess["zip_from"]{
                //                            if "\(zip)".isEmpty{
                //                                dicPickup["zip_from"] = ""
                //                                self.dicWebServiceParam["zip_from"] = ""
                //                            }
                //                            else {
                //                                dicPickup["zip_from"] = "\(zip)"
                //                                self.dicWebServiceParam["zip_from"] = "\(zip)"
                //                            }
                //                    }
                //                    }
                //                    else {
                //                         dicPickup["zip_from"] = "\(zip)"
                //                        self.dicWebServiceParam["zip_from"] = "\(zip)"
                //                    }
                //                }
                dicPickup["zip_from"] = self.dictPickAddess["postal_code"] != nil ? self.dictPickAddess["postal_code"] : (self.dictPickAddess["zip_from"] != nil ? self.dictPickAddess["zip_from"]  : "")
                
                self.dicWebServiceParam["zip_from"] = self.dictPickAddess["postal_code"] != nil ? self.dictPickAddess["postal_code"] : (self.dictPickAddess["zip_from"] != nil ? self.dictPickAddess["zip_from"]  : "")
                
                self.dicWebServiceParam["pickup_info"] = dicPickup as Any?
                return true
            }else {
                return false
            }
        }
        else if self.strPickType == pickupType.Airport.rawValue
        {
            self.dicWebServiceParam["pickup_type"] = "airport" as Any?
            if let dicAirportPickup = self.dicOnlyAirport["pickup"] as? [String:Any]
            {
                self.dicWebServiceParam["pickup_info"] = dicAirportPickup as Any?
                self.dicWebServiceParam["pickup"] =  dicAirportPickup["airport_name"]
                self.dicWebServiceParam["pickup_latitude"] =  dicAirportPickup["airport_lat"]
                self.dicWebServiceParam["pickup_longitude"] = dicAirportPickup["airport_lng"]
                return true
            }else {
                return false
            }
            
        }
        else if self.strPickType == pickupType.Seaport.rawValue
        {
            if self.dictPickAddess.count != 0 {
                self.dicWebServiceParam["pickup_type"] = "seaport" as Any?
                var dicPickup = [String:Any]()
                if let addressDetails = self.dictPickAddess["seaport_address"] as? [String:Any] {
                    self.dicWebServiceParam["pickup"] = addressDetails["address"]
                    self.dicWebServiceParam["pickup_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["pickup_longitude"] = addressDetails["longitude"]
                    dicPickup["arriving_from"] = addressDetails["address"]
                    dicPickup["city"] = addressDetails["locality"]
                    dicPickup["state"] = addressDetails["administrative_area_level_1"]
                    dicPickup["country"] = addressDetails["country"]
                    dicPickup["zip_from"] = addressDetails["postal_code"]
                    self.dicWebServiceParam["zip_from"] = addressDetails["postal_code"]
                }
                
                if let addressDetails = self.dictPickAddess["seaport_details"] as? [String:Any]{
                    self.dicWebServiceParam["pickup"] = addressDetails["address"]
                    self.dicWebServiceParam["pickup_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["pickup_longitude"] = addressDetails["longitude"]
                    dicPickup["arriving_from"] = "" as Any?
                    dicPickup["city"] = "" as Any?
                    dicPickup["state"] = "" as Any?
                    dicPickup["country"] = "" as Any?
                    dicPickup["zip_from"] = "" as Any?
                    self.dicWebServiceParam["zip_from"] = "" as Any?
                }
                if let addressDetails = self.dictPickAddess["seaport_details"] as? [String:Any]{
                    dicPickup["seaport_id"] = addressDetails["seaport_id"]
                    dicPickup["cruise_id"] = addressDetails["cruise_id"]
                    dicPickup["seaport_inst"] = addressDetails["seport_inst"]
                }
                
                self.dicWebServiceParam["pickup_info"] = dicPickup as Any?
                return true
            }else {
                return false
            }
        } else if self.strPickType == pickupType.FBO.rawValue {
            
            if self.dictPickAddess.count != 0 {
                self.dicWebServiceParam["pickup_type"] = "fbo" as Any?
                var dicPickup = [String:Any]()
                if let addressDetails = self.dictPickAddess["fbo_address"] as? [String:Any]{
                    self.dicWebServiceParam["pickup"] = addressDetails["address"]
                    self.dicWebServiceParam["pickup_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["pickup_longitude"] = addressDetails["longitude"]
                    dicPickup["address"] = addressDetails["address"]
                    dicPickup["city"] = (addressDetails["city"] == nil) ? "" : addressDetails["city"]
                    dicPickup["state"] = (addressDetails["state"] == nil) ? "" : addressDetails["state"]
                    dicPickup["country"] = (addressDetails["country"] == nil) ? "" : addressDetails["country"]
                    self.dicWebServiceParam["zip_from"] = (addressDetails["zip"] == nil) ? "" : addressDetails["zip"]
                    
                }
                if let addressDetails = self.dictPickAddess["fbo_details"] as? [String:Any]{
                    self.dicWebServiceParam["pickup"] = addressDetails["address"]
                    self.dicWebServiceParam["pickup_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["pickup_longitude"] = addressDetails["longitude"]
                }
                if let addressDetails = self.dictPickAddess["fbo_details"] as? [String:Any]{
                    for (keys,value) in addressDetails {
                        dicPickup[keys] = value
                    }
                }
                
                self.dicWebServiceParam["pickup_info"] = dicPickup as Any?
                return true
            }else {
                return false
            }
        }
        return false
    }


    //// drop offf
    func dropOffParametersAddressWithType() -> Bool
    {
        
        if self.strDropOffType == DropOffType.Address.rawValue || self.strDropOffType ==  DropOffType.POI.rawValue
        {
            self.dicWebServiceParam["drop_type"] = self.strDropOffType.lowercased() as Any?
            // for pickup
            if let dickDroppOff = self.arrDataSource[keyIndexSection.dropType.rawValue][Constants.DropType] as? [String:Any] {
                self.dicWebServiceParam["dropoff"] = dickDroppOff["address"]
                self.dicWebServiceParam["dropoff_latitude"] = dickDroppOff["latitude"]
                self.dicWebServiceParam["dropoff_longitude"] = dickDroppOff["longitude"]
                var dicPickup = [String:Any]()
                dicPickup["location"] = dickDroppOff["address"]
                dicPickup["address"] = dickDroppOff["address"]
                dicPickup["city"] = dickDroppOff["locality"] != nil ? dickDroppOff["locality"] : (dickDroppOff["city"] != nil ? dickDroppOff["city"]  : "")
                
                dicPickup["state"] = dickDroppOff["administrative_area_level_1"] != nil ? dickDroppOff["administrative_area_level_1"] : (dickDroppOff["state"] != nil ? dickDroppOff["state"]  : "")
                
                dicPickup["country"] = dickDroppOff["country"]
                dicPickup["zip_to"] = dickDroppOff["postal_code"] != nil ? dickDroppOff["postal_code"] : (dickDroppOff["zip_to"] != nil ? dickDroppOff["zip_to"]  : "")
                
                self.dicWebServiceParam["zip_to"] = dickDroppOff["postal_code"] != nil ? dickDroppOff["postal_code"] : (dickDroppOff["zip_to"] != nil ? dickDroppOff["zip_to"]  : "")
                self.dicWebServiceParam["dropoff_info"] = dicPickup as Any?
                return true
            }else {
                return false
            }
            
        }
        else if self.strDropOffType == DropOffType.Airport.rawValue
        {
            self.dicWebServiceParam["drop_type"] = "airport" as Any?
            if let dicAirportPickup = self.dicOnlyAirport["dropoff"] as? [String:Any]
            {
                self.dicWebServiceParam["dropoff_info"] = dicAirportPickup as Any?
                self.dicWebServiceParam["dropoff"] =  dicAirportPickup["airport_name"]
                self.dicWebServiceParam["dropoff_latitude"] =  dicAirportPickup["airport_lat"]
                self.dicWebServiceParam["dropoff_longitude"] = dicAirportPickup["airport_lng"]
                return true
            }else {
                return false
            }
            
        }
        else if self.strDropOffType == DropOffType.Seaport.rawValue
        {
            if self.dictDropOffAddess.count != 0 {
                self.dicWebServiceParam["drop_type"] = "seaport" as Any?
                var dicPickup = [String:Any]()
                if let addressDetails = self.dictDropOffAddess["seaport_address"] as? [String:Any] {
                    self.dicWebServiceParam["dropoff"] = addressDetails["address"]
                    self.dicWebServiceParam["dropoff_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["dropoff_longitude"] = addressDetails["longitude"]
                    dicPickup["arriving_from"] = addressDetails["address"]
                    dicPickup["city"] = addressDetails["locality"]
                    dicPickup["state"] = addressDetails["administrative_area_level_1"]
                    dicPickup["country"] = addressDetails["country"]
                    dicPickup["zip_to"] = addressDetails["postal_code"]
                    self.dicWebServiceParam["zip_to"] = addressDetails["postal_code"]
                }
                if let addressDetails = self.dictDropOffAddess["seaport_details"] as? [String:Any]  {
                    self.dicWebServiceParam["dropoff"] = addressDetails["address"]
                    self.dicWebServiceParam["dropoff_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["dropoff_longitude"] = addressDetails["longitude"]
                    dicPickup["arriving_from"] = "" as Any?
                    dicPickup["city"] = "" as Any?
                    dicPickup["state"] = "" as Any?
                    dicPickup["country"] = "" as Any?
                    dicPickup["zip_to"] = "" as Any?
                    self.dicWebServiceParam["zip_to"] = "" as Any?
                }
                if let addressDetails = self.dictDropOffAddess["seaport_details"] as? [String:Any]  {
                    dicPickup["seaport_id"] = addressDetails["seaport_id"]
                    dicPickup["cruise_id"] = addressDetails["cruise_id"]
                    dicPickup["seaport_inst"] = addressDetails["seport_inst"]
                }
                self.dicWebServiceParam["dropoff_info"] = dicPickup as Any?
                return true
            }else {
                return false
            }
        } else if self.strDropOffType == DropOffType.FBO.rawValue
        {
            if self.dictDropOffAddess.count != 0 {
                self.dicWebServiceParam["drop_type"] = "fbo"
                var dicPickup = [String:Any]()
                if let addressDetails = self.dictDropOffAddess["fbo_address"] as? [String:Any]
                {
                    self.dicWebServiceParam["dropoff"] = addressDetails["address"]
                    self.dicWebServiceParam["dropoff_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["dropoff_longitude"] = addressDetails["longitude"]
                    self.dicWebServiceParam["zip_from"] = (addressDetails["zip"] == nil) ? "" : addressDetails["zip"]
                    dicPickup["address"] = addressDetails["address"]
                    dicPickup["city"] = (addressDetails["city"] == nil) ? "" : addressDetails["city"]
                    dicPickup["state"] = (addressDetails["state"] == nil) ? "" : addressDetails["state"]
                    dicPickup["country"] = (addressDetails["country"] == nil) ? "" : addressDetails["country"]
                }
                if let addressDetails = self.dictDropOffAddess["fbo_details"] as? [String:Any]{
                    self.dicWebServiceParam["dropoff"] = addressDetails["address"]
                    self.dicWebServiceParam["dropoff_latitude"] = addressDetails["latitude"]
                    self.dicWebServiceParam["dropoff_longitude"] = addressDetails["longitude"]
                }
                if let addressDetails = self.dictDropOffAddess["fbo_details"] as? [String:Any]{
                    for (keys,value) in addressDetails {
                        dicPickup[keys] = value
                    }
                }
                self.dicWebServiceParam["dropoff_info"] = dicPickup as Any?
                
                return true
            }else {
                return false
            }
        }
        return false
    }

    func MakeRequestWithAllTypeOfData()
    {
 
        if self.dateSelected == nil  {
            self.showBannerWithMessage("Please select pick-up date.")
            return
        }
        let pickUp = self.addParamterForPickupAddressWithType()
        self.dropOffParametersAddressWithType()
        var strMessage = ""
        
      
        if pickUp == false  {
            strMessage += "Pickup Address\n"
        }
        if self.arrDataSource.count != 0 {
                //   var param_Dictionary: [String: Any] = [String: Any]()
                let alias_id  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).alias_id")
                
                if Constants.isBookerLogin == true
                {
                    let bookerID = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).booker_id") as? String
                    self.dicWebServiceParam["user_id"] = "\(appDelegate.activeUserDetailsForBooker["id"]!)"
                    self.dicWebServiceParam["is_booker"] = "yes"
                    self.dicWebServiceParam["booker_id"] = bookerID
                }
                else
                {
                    let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
                    let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
                    self.dicWebServiceParam["user_id"] = user_ID
                    self.dicWebServiceParam["session"] = session
                    self.dicWebServiceParam["is_booker"] = "no"

                }
                
                self.dicWebServiceParam["alias_id"] = alias_id
                self.dicWebServiceParam["job_type"] = "INH" as Any?
                // for pickup date and time
                
                
                    let dateFormat: DateFormatter = DateFormatter()
                dateFormat.timeZone = TimeZone.current
                
                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let strDate = dateFormat.string(from: self.dateSelected)
                
                if strDate.isEmpty == true || strDate.compare("Pickup Date"){
                    strMessage += "Pickup Date \n"
                }
                else {
                    let strHours = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).prohibitReservation_before")
                    if strHours != nil && String(describing: strHours!).compare("0") == false {
                        let date = Date()
                        if self.dateSelected == nil {
                            self.dateSelected = Date()
                        }
                        let dayHourMinuteSecond: NSCalendar.Unit = [.day, .hour, .minute, .second]
                        let components:DateComponents = (Calendar.current as NSCalendar).components(dayHourMinuteSecond, from: date, to: self.dateSelected, options: [])
                        if components.day == 0 {
                            if components.hour!+1 <= Int(String(strHours! as! NSString))! {
                                let alert = UIAlertView(title: "Alert", message:  "Reservations can only be accepted after \(strHours!) hours of current time", delegate: nil, cancelButtonTitle: "Dismiss")
                                alert.show()
                                return
                            }
                        }
                    }
                    let  dateStrFormated = Globals.sharedInstance.getDateFormatForDateString(strDate)
                    let arrDateTime = dateStrFormated.components(separatedBy: CharacterSet.whitespaces)
                    self.dicWebServiceParam["pu_date"] = arrDateTime[0]
                    self.dicWebServiceParam["pu_time"] = arrDateTime[1]
                }
                // for Service Type
                if let dicService = self.arrDataSource[keyIndexSection.serviceType.rawValue][Constants.Service] as? [String:Any] {
                    self.dicWebServiceParam["service_type"] = dicService["id"]
                }else {
                    strMessage += "Service Type \n"
                }
                if strServiceType.compare("Hourly") {
                    if self.strHours == "0" {
                        strMessage += "Add Hours \n"
                    }else {
                        let minHr = self.minimumJobHours.toDouble()
                        var inputHours : Double? =  0.0
                        if self.strHours.compare("0") {
                            inputHours = String(self.strHours).toDouble()
                        }else if self.strHours.contains("Minimum") {
                            let arrStrHours = self.strHours.components(separatedBy: " ")
                            inputHours = String(arrStrHours[1]).toDouble()
                        }else {
                            inputHours = String(self.strHours).toDouble()
                        }
                        if minHr > inputHours{
                            Globals.ToastAlertWithString("Selected hour(s) should be greater or equal to minimum hour(s)")
                            return
                        }
                        self.dicWebServiceParam["hourly_time"] = self.strHours as Any?
                    }
                }
        self.dicWebServiceParam["service_type_name"] = self.strServiceType
                if let passName = self.arrDataSource[keyIndexSection.passangerName.rawValue][Constants.PassangerName] as? String{
                    if passName.isEmpty{
                         strMessage += "Passenger Name \n"
                    }else {
                    self.dicWebServiceParam["passanger_name"] = passName as Any?
                    }
                }
                // for card payment
                if strPayment.compare("Payment"){
                    strMessage += "Payment Option \n"
                }
                if strMessage.count > 0 {
                    let alert = UIAlertView(title: "Please provide following details", message: strMessage, delegate: nil, cancelButtonTitle: "Dismiss")
                    alert.show()
                    return
                }
                var paymentNil = true
                if self.dicCardDetail.count != 0  {
                    if let _ = self.dicCardDetail["id"]
                    {
                        if  self.dicCardDetail["id"] as! String != ""{
                            self.dicWebServiceParam["credit_card_id"] = self.dicCardDetail["id"]
                            self.dicWebServiceParam["payment_method"] = self.dicCardDetail["payment_method"]
                          
                            paymentNil = false
                        }
                    }
                }
                if paymentNil == true {
                    self.dicWebServiceParam["credit_card_id"] = "" as Any?
                    self.dicWebServiceParam["payment_method"] = self.dicCardDetail["payment_method"]
                   
                }
            
                self.dicWebServiceParam["payment_name"] = self.strPayment
                ////Stops ===
                if var arrStop = self.arrDataSource[keyIndexSection.stops.rawValue][Constants.Stops] as?  [[String:Any]]{
                    if arrStop.count != 0 {
                        for (index, value) in arrStop.enumerated()   {
                            var dic = value
                            dic["stop_type"] = "address" as Any?
                            dic["flag"] = "true" as Any?
                            arrStop[index] = dic
                        }
                        self.dicWebServiceParam["stops"] = arrStop as Any?
                    }
                }
                if let pickupNotes = self.dictPickAddess[Constants.PickNotes]  as? String
                {
                    if pickupNotes == "Pickup Notes"{
                        self.dicWebServiceParam["pickup_notes"] = "" as Any?
                    }
                    else {
                        self.dicWebServiceParam["pickup_notes"] = pickupNotes as Any?
                    }
                }
                if let dropoffNotes = self.dictDropOffAddess[Constants.DropNotes]  as? String
                {
                    if dropoffNotes == "Drop Off Notes"{
                        self.dicWebServiceParam["dropoff_notes"] = "" as Any?
                    }
                    else {
                        self.dicWebServiceParam["dropoff_notes"] = dropoffNotes as Any?
                    }
                }
                ////distancek time
                let arrKeeys : [String]? = [String](self.dicDisAndTime.keys)
                if arrKeeys!.contains("duration"){
                    let DicTime = self.dicDisAndTime["duration"]! as! [String:Any]
                    let Time = (DicTime["text"] as! String)
                    self.dicWebServiceParam["time"] = Time as Any?;
                    let Dicdistance = self.dicDisAndTime["distance"]! as! [String:Any]
                    if let distance = Dicdistance["value"]  as? Float  {  // * 0.000621371 //appDelegate.distanceMultiplier Km -- 0.001 Miles --
                        self.dicWebServiceParam["distance"] = distance
                        self.dicWebServiceParam["distance_miles"] = String(format:"%.2f", distance * 0.000621371)
                        self.dicWebServiceParam["distance_kilometers"] = String(format:"%.2f", distance *  0.001)
                    }
                    self.dicWebServiceParam["distance_type"] = appDelegate.distanceType
                    
                    //                    let distance = (Dicdistance["value"]  as! Float) * 0.001
                    //                    self.dicWebServiceParam["distance"] = distance
                }
                if self.arrAddedChildSeats.count != 0 {
                    self.dicWebServiceParam["child_seat_count"] = self.arrAddedChildSeats as Any?
                }else {
                    self.dicWebServiceParam["child_seat_count"] = "0" as Any?
                }
                self.dicWebServiceParam["company_id"] = Constants.CompanyID as Any?
                self.dicWebServiceParam["luggage"] = self.txtLagguge.text as Any?
                self.dicWebServiceParam["is_handicap"] = self.strDisability as Any?
                self.dicWebServiceParam["adult_seat_count"] = self.txtAdult.text as Any?
                self.dicWebServiceParam["reservation_source"] = "mobile" as Any?
                if self.saved_reservaionID.isEmpty == false {
                    self.strReservationId = self.saved_reservaionID
                }
                self.dicWebServiceParam["reservation_id"] = self.saved_reservaionID// self.strReservationId as Any?
                self.dicWebServiceParam["template_type"] = "ptp" as Any?
             
                if let pss_no = self.arrDataSource[keyIndexSection.passangerNumber.rawValue][Constants.PassangerNumber] as? String{
                    
                    if Constants.countyApp == "1"{
                        self.dicWebServiceParam["pass_email"] = pss_no as Any?
                        self.dicWebServiceParam["phone_no"] = ""
                    }else {
                        self.dicWebServiceParam["phone_no"] = pss_no as Any?
                        self.dicWebServiceParam["pass_email"] = ""
                    }
                }
            
            if self.dictSelectedVehicleData.count == 0 {
                if self.dropOffLocation == nil  {
                    Globals.ToastAlertWithString("Please enter Dropoff-off Address")
                }else {
                    self.getDistanceFromPickUpToDropOff(isPushToVehicle: true)
                }
            }
            
           
        }
    }

    func resetFieldsForReuse() {
        for (index, value) in self.arrDataSource.enumerated() {
            switch index {
            case keyIndexSection.pickType.rawValue:
                self.dictPickAddess.removeAll()
                self.strPickType = pickupType.None.rawValue
                self.btnPickupTypeTag = 0 ;
                self.btnPickupTypeLastOne = 0;
                self.arrDataSource[index] = [Constants.PickType:"PICKUP TYPE" ]
            case keyIndexSection.dropType.rawValue:
                self.dictDropOffAddess.removeAll()
                self.strDropOffType = DropOffType.None.rawValue
                self.btnDroppOffTypeTag = 0 ;
                self.btnDroppOffTypeLastOne = 0;
                self.arrDataSource[index] = [Constants.DropType:"DROP OFF TYPE" ]
            case keyIndexSection.stops.rawValue:
                self.arrStops.removeAll()
            case keyIndexSection.stops.rawValue:
                self.arrStops.removeAll()
                self.arrDataSource[index] = [Constants.Stops:"Stops"]
            case keyIndexSection.payment.rawValue:
                self.strPayment = "Payment"
                self.dicCardDetail.removeAll()
                self.arrDataSource[index] = [Constants.Payment:"Payment"]
            case keyIndexSection.passangerNumber.rawValue:
                self.arrDataSource[index] = [Constants.PassangerNumber:""]
            case keyIndexSection.adultLagguge.rawValue:
                self.arrDataSource[index] = [Constants.AdultLagguge:"01"]
            case keyIndexSection.childSeatType.rawValue:
                self.arrDataSource[index] = [Constants.ChildSeatType:"Child Seat Type"]
            case keyIndexSection.childSeatCount.rawValue:
                self.arrDataSource[index] = [Constants.ChildCount:"Child Seat Count"]
                self.arrAddedChildSeats.removeAll()
                
            default:
                break
            }
        }
        
        self.reloadTableViewOnMainQueue()
        
        tableViewReservations.reloadData()
    }
    func reservaionCreated(response:[String:Any]) {
        let confirmVC = self.storyboard?.instantiateViewController(withIdentifier: "PAConfirmReservationVC") as! PAConfirmReservationVC
        
        if let conf_id = response["conf_id"]{
            confirmVC.saved_reservaionID = "\(conf_id)"
        }
        confirmVC.dicReservation = self.dicWebServiceParam
        confirmVC.dateSelected = self.dateSelected
        confirmVC.strPickType =  self.strServiceType
        confirmVC.strCardId = self.strPayment
        
        if "\(response["totalamount"]!)".isEmpty {
            confirmVC.strEstimateFare = "N/A"
        }
        else {
            confirmVC.strEstimateFare = appDelegate.currency + " " + String(describing:"\(response["totalamount"]!)")
        }
        confirmVC.vehicleName = self.strVehicle
        confirmVC.strServiceType = self.strServiceType
        confirmVC.isEditMode = self.isEditingMode
        
        confirmVC.finishEditReservation = { [weak self] (dicReservation:[String:Any]?) in
            if dicReservation == nil {
                
                if Constants.countyApp == "1"{
                    
                    if let btnSubmit = self?.view.viewWithTag(996633) as? UIButton {
                        btnSubmit.setTitle("Select Vehicle", for: .normal)
                        btnSubmit.backgroundColor = UIColor.appRideLater_RedColor()
                        btnSubmit.titleLabel?.font = Globals.defaultAppFontWithBold(14)
                        Globals.layoutViewFor(btnSubmit, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 3)
                    }
                    self?.resetFieldsForReuse()

                }else{
                    self?.dismiss(animated: true, completion: nil)
                }
            }
        }
        self.navigationController?.pushViewController(confirmVC, animated: true)
    }
    // MARK: -======== Submitt button action ========
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        if self.strVehicle.compare(Constants.partyBus) == true && self.strServiceShortCode.compare("HOURLY") == false && Constants.CompanyID == Constants.RoyalLimo {
            let alert = self.alertView("Info", message:"Your must choose service type as hourly for party bus or contact customer support at 1866-690-2200 for questions.", delegate:nil, cancelButton:"Ok", otherButons:nil)
            alert!.show()
        }
        else {
         
                self.MakeRequestWithAllTypeOfData()
        }
        
    }
    func verifiy() -> Bool{
        var flag:Bool = false
        for dict in self.arrDataSource {
            for (key , value) in dict as [String: Any]{
                if value is String && value as! String == ""{
                    if key == Constants.Stops || key == Constants.DropType || key == Constants.ChildCount || key == Constants.ChildSeatType {
                        continue
                    }
                    else {
                        flag = true
                        break
                    }
                }
            }
            if flag == true{
                break
            }
        }
        return flag
    }
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    // MARK: -======== back button actions ========
    func backButtonActionMethod(_ sender: UIButton) {
        
        if isEditingMode {
            if self.navigationController != nil {
                for vc: UIViewController in (self.navigationController?.viewControllers)! {
                    if (vc.isKind(of: UpcomingReservationsViewController.self)) {
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }
        }
        else {
            if self.navigationController != nil {
                for vc: UIViewController in (self.navigationController?.viewControllers)! {
                    if (vc.isKind(of: PAMainMenuViewController.self)) || (vc.isKind(of: PABookerMainMenuViewController.self)) {
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }
            
        }
    }


    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
            let endScrolling:CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height
    

            if endScrolling >= scrollView.contentSize.height - 200 {
                self.bottomArrorwImage.isHidden = true
              
            }else {
                self.bottomArrorwImage.isHidden = false
        }
        
    }

    }
    extension PAReservationsRideLaterVC : UITextViewDelegate {

    // MARK:- ====Text View delegate ===
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        self.activeTextView = textView
        if self.activeTextView.text == "Pickup Notes" || self.activeTextView.text == "Drop Off Notes"{
            self.activeTextView.text = "" //changes Prakash
        }
        self.currentSection = (textView.superview?.tag)!
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView){
        // self.arrDataSource[10]["Pickup Notes"] = textView.text
        let sectionTag =  textView.superview!.tag
        if  sectionTag == keyIndexSection.pickType.rawValue{
            self.dictPickAddess[Constants.PickNotes] = textView.text.isEmpty == false ? textView.text : "Pickup Notes"
        }else if sectionTag == keyIndexSection.dropType.rawValue{
            self.dictDropOffAddess[Constants.DropNotes] =  textView.text.isEmpty == false ? textView.text : "Drop Off Notes"
        }
    }

    }
    extension PAReservationsRideLaterVC : UITextFieldDelegate {
    // MARK: - ======== text Field Delegate  ========
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        
        return true
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }



    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        self.isTxtAdultPicker = false
        self.activeTextField = textField
        self.currentSection = textField.tag
        self.viewDatePicker.isHidden = false
        self.pickerViewCarName.selectRow(0, inComponent: 0, animated: true)
        if textField.tag == keyIndexSection.hourly.rawValue {
            self.currentSection = keyIndexSection.hourly.rawValue
            self.pickerViewCarName.reloadAllComponents()
            self.viewPickerView.isHidden = false
        }else
            if textField.tag == 55 {
                self.currentSection = keyIndexSection.childSeatCount.rawValue
                self.pickerViewCarName.reloadAllComponents()
                self.viewPickerView.isHidden = false
            }else  if textField.tag == keyIndexSection.childSeatType.rawValue {
                self.currentSection = keyIndexSection.childSeatType.rawValue
                self.pickerViewCarName.reloadAllComponents()
                self.viewPickerView.isHidden = false
            }else if textField.tag == 80 || textField.tag == 81  {
                self.isTxtAdultPicker = true
                self.adultLuaggeTag = textField.tag
                if  self.adultLuaggeTag == 80 {
                    if self.arrAdults.count == 0 {
                        Globals.sharedInstance.ToastAlertInstance("Please select preferred vehicle to get adult count.")
                        return false
                    }
                }else if self.adultLuaggeTag == 81 {
                    if self.arrLuggage.count == 0 {
                        Globals.sharedInstance.ToastAlertInstance("Please select preferred vehicle to get luggage count.")
                        return  false
                    }
                }
                self.currentSection = keyIndexSection.adultLagguge.rawValue
                self.pickerViewCarName.reloadAllComponents()
                self.viewPickerView.isHidden = false
            } else  if textField.tag == keyIndexSection.serviceType.rawValue {
                self.currentSection = keyIndexSection.serviceType.rawValue
                if self.arrDataServiceType.count != 0 {
                    textField.resignFirstResponder()
                    self.getingVehlciTypesAvailable()
                }
                self.showServiceSelecttionController()
                return false
                //                if self.arrDataServiceType.count != 0 {
                //                    self.pickerViewCarName.reloadAllComponents()
                //                    self.viewPickerView.isHidden = false
                //                }else {
                //                    textField.resignFirstResponder()
                //                    self.getingVehlciTypesAvailable()
                //                    return false
                //                }
        }
        return true
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if Constants.countyApp == "1"{
            if textField.tag == keyIndexSection.passangerNumber.rawValue {
                if textField.text != nil &&  !(textField.text!.isEmpty) &&  self.isValidEmail(testStr: textField.text!) == false {
                    self.showBannerWithMessage("Please enter valid email id.")
                    return true
                } else {
                    self.arrDataSource[textField.tag][Constants.PassangerNumber] = textField.text as Any?
                }
            }
            if textField.tag == keyIndexSection.passangerName.rawValue {
                self.arrDataSource[textField.tag][Constants.PassangerName] = textField.text as Any?
            }
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        if  textField.tag == keyIndexSection.hourly.rawValue {
            self.strHours = self.arrHoursCount[pickerViewCarName.selectedRow(inComponent: 0)]
            //       self.arrDataSource[keyIndexSection.Hourly.rawValue][Constants.Hourly] = self.strHours
            // self.tableViewReservations.reloadData()
            textField.text =   "\(self.strHours) Hours"
            self.reloadTableViewOnMainQueue()
            return
        }
        self.activeTextField.resignFirstResponder()
        let arrDicKeys = (self.arrDataSource[ self.currentSection]).keys
        let strKey = arrDicKeys.first!
        if  self.currentSection == keyIndexSection.adultLagguge.rawValue
        {
            self.arrDataSource[self.currentSection][strKey] = textField.text as Any?
        }else if self.currentSection == keyIndexSection.passangerName.rawValue  {
            self.arrDataSource[self.currentSection][Constants.PassangerName] = textField.text as Any?
        } else if  self.currentSection == keyIndexSection.passangerNumber.rawValue {
            
            if Constants.countyApp == "1"{
                if textField.tag == keyIndexSection.passangerNumber.rawValue {
                    if textField.text != nil &&  !(textField.text!.isEmpty) &&  self.isValidEmail(testStr: textField.text!) == false {
                        self.showBannerWithMessage("Please enter valid email id.")
                    }
                    else {
                        self.arrDataSource[textField.tag][Constants.PassangerNumber] = textField.text as Any?
                    }
                }
            }
            else {
                self.arrDataSource[textField.tag][Constants.PassangerNumber] = textField.text as Any?
            }
        }
    }

    }
    extension PAReservationsRideLaterVC : UIPickerViewDelegate, UIPickerViewDataSource {
    // MARK: - =======picker View Data Source and Delegate ======
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if self.isTxtAdultPicker == false {
            if self.currentSection == keyIndexSection.serviceType.rawValue {
                return self.arrDataServiceType.count
            }else
                if self.currentSection == keyIndexSection.childSeatType.rawValue
                {
                    return self.arrChildSeatType.count
                }else if self.currentSection == keyIndexSection.childSeatCount.rawValue
                {
                    return self.arrChildCount.count
                }else if self.currentSection == keyIndexSection.hourly.rawValue
                {
                    return self.arrHoursCount.count
            }
        }else {
            if self.adultLuaggeTag == 80 {
                return self.arrAdults.count
            }else
                if self.adultLuaggeTag == 81 {
                    return self.arrLuggage.count
            }
            return self.arrChildCount.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let viewReturn = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width, height: 80))
        let lblTitle = UILabel(frame: CGRect(x: 0,y: 0,width: self.view.frame.width, height: 80))
        lblTitle.textAlignment = .center
        lblTitle.numberOfLines = 3
        lblTitle.textColor = UIColor.darkGray
        lblTitle.font = Globals.defaultAppFontWithBold(22)
        viewReturn.addSubview(lblTitle)
        if self.isTxtAdultPicker == false {
            if self.currentSection == keyIndexSection.serviceType.rawValue {
                let dicStops = self.arrDataServiceType[row]
                lblTitle.text = dicStops["service_type"] as? String
            }else
                if self.currentSection == keyIndexSection.childSeatType.rawValue
                {
                    lblTitle.text = self.arrChildSeatTypeForAPI[row]
                }
                else if self.currentSection == keyIndexSection.childSeatCount.rawValue
                {
                    lblTitle.text = self.arrChildCount[row]
                }else if self.currentSection == keyIndexSection.hourly.rawValue
                {
                    lblTitle.text =  self.arrHoursCount[row]
            }
        }else {
            if self.activeTextField.tag == 81 {
                strLagguge =  self.arrLuggage[row]
                lblTitle.text = strLagguge
            }else if self.activeTextField.tag == 80 {
                strAdult =  self.arrAdults[row]
                lblTitle.text = strAdult
            }
        }
        return viewReturn
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat
    {
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.isTxtAdultPicker == false {
            if self.currentSection == keyIndexSection.serviceType.rawValue {
                self.arrDataSource[keyIndexSection.serviceType.rawValue][Constants.Service] = self.arrDataServiceType[row]  as Any?
                let dicCars = self.arrDataServiceType[row]
                self.strServiceType =  (dicCars["service_type"] as? String)!
                self.activeTextField.text = strServiceType
            }else if self.currentSection == keyIndexSection.childSeatType.rawValue
            {
                self.activeTextField.text = self.arrChildSeatType[row]
                self.arrDataSource[keyIndexSection.childSeatType.rawValue][Constants.ChildSeatType] = self.arrChildSeatType[row] as Any?
            }else if self.currentSection == keyIndexSection.childSeatCount.rawValue
            {
                self.activeTextField.text = self.arrChildCount[row]
                self.arrDataSource[keyIndexSection.childSeatCount.rawValue][Constants.ChildCount] = self.arrChildCount[row] as Any?
            }else if self.currentSection == keyIndexSection.hourly.rawValue
            {
                self.activeTextField.text =  self.arrHoursCount[row]
                self.strHours = self.arrHoursCount[row]
                //     self.arrDataSource[keyIndexSection.Hourly.rawValue][Constants.Hourly] = self.strHours
            }
        }else {
            if self.activeTextField.tag == 81 {
                strLagguge =  self.arrLuggage[row]
                self.activeTextField.text = strLagguge
            }else if self.activeTextField.tag == 80 {
                strAdult =  self.arrAdults[row]
                self.activeTextField.text = strAdult
            }
        }
    }
    }
    extension PAReservationsRideLaterVC : UITableViewDataSource, UITableViewDelegate {
    // MARK: - ======== table View data Source  ========
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.arrDataSource.count
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        switch section {
        case keyIndexSection.pickType.rawValue :
            if strPickType != pickupType.None.rawValue {
                return 1
            }
        case keyIndexSection.dropType.rawValue:
            if strDropOffType != DropOffType.None.rawValue {
                return 1
            }
        case keyIndexSection.stops.rawValue :
            return self.arrStops.count
            
        case keyIndexSection.childSeatCount.rawValue :
            return self.arrAddedChildSeats.count
            
        case keyIndexSection.serviceType.rawValue :
            if self.strServiceType.compare("Hourly") == true  {
                return 1
            }
            return 0
        default :
            return 0
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : UITableViewCell?
        if indexPath.section ==  keyIndexSection.pickType.rawValue  || indexPath.section == keyIndexSection.dropType.rawValue
        {
            if indexPath.row == 1
            {
                let cellIndetifier = "cellRideLaterWithButton"
                let  cellButton = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier) as! cellReservationForButton
                // cellButton.btnCellOptions.layer.borderColor = UIColor.lightGrayColor().CGColor
                //cellButton.btnCellOptions.layer.borderWidth = 1.0
                cellButton.btnCellOptions.titleLabel?.numberOfLines = 4
                
                //cellButton.btnCellOptions.addTarget(self, action: "btnCellOptionAction:", forControlEvents: UIControlEvents.TouchUpInside)
                cellButton.btnCellOptions.tag = indexPath.row
                cellButton.btnCellOptions.titleLabel?.font = Globals.defaultAppFont(14)
                cellButton.btnCellOptions.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
                cellButton.btnCellOptions.titleEdgeInsets = UIEdgeInsetsMake(0, 13, 0,0)
                var text = ""
                if indexPath.section == keyIndexSection.pickType.rawValue {
                    switch self.strPickType {
                    case pickupType.Airport.rawValue :
                        if let dic = self.dicOnlyAirport["pickup"] as? [String:Any]{
                            if let strAdres = dic["airport_name"] as? String{
                                text = strAdres
                            }
                        }
                    case pickupType.Address.rawValue, pickupType.POI.rawValue :
                        if let strAdres = self.dictPickAddess["address"] as? String{
                            text = strAdres
                        }
                    case pickupType.FBO.rawValue :
                        if let dic = self.dictPickAddess["fbo_details"] as? [String:Any]{
                            if let strAdres = dic["address"] as? String{
                                text = strAdres
                            }else {
                                if let dic = self.dictPickAddess["fbo_address"] as? [String:Any] {
                                    if let strAdres = dic["address"] as? String{
                                        text = strAdres
                                    }
                                }
                            }
                        }
                    case pickupType.Seaport.rawValue :
                        if let dic = self.dictPickAddess["seaport_address"] as? [String:Any]{
                            if let strAdres = dic["address"] as? String{
                                text = strAdres
                            }
                        }
                    default :
                        break
                    }
                    cellButton.btnCellOptions.setImage(UIImage(named: "pickup"), for: UIControlState())
                }else if indexPath.section == keyIndexSection.dropType.rawValue {
                    switch self.strDropOffType {
                    case DropOffType.Airport.rawValue :
                        if let dic = self.dicOnlyAirport["dropoff"] as? [String:Any]{
                            if let strAdres = dic["airport_name"] as? String{
                                text = strAdres
                            }
                        }
                    // text = self.dicOnlyAirport["dropoff"]!["airport_name"] as! String
                    case DropOffType.Address.rawValue , DropOffType.POI.rawValue:
                        if let strAdres = self.dictDropOffAddess["address"] as? String{
                            text = strAdres
                        }
                    //text = self.dictDropOffAddess["address"] as! String
                    case DropOffType.FBO.rawValue :
                        if let dic = self.dictDropOffAddess["fbo_details"] as? [String:Any]{
                            if let strAdres = dic["address"] as? String{
                                text = strAdres
                            }else {
                                if let dic = self.dictDropOffAddess["fbo_address"] as? [String:Any] {
                                    if let strAdres = dic["address"] as? String{
                                        text = strAdres
                                    }
                                }
                            }
                        }
                    // text = self.dictDropOffAddess["fbo_details"]!["address"] as! String
                    case DropOffType.Seaport.rawValue :
                        if let dic = self.dictDropOffAddess["seaport_address"] as? [String:Any]{
                            if let strAdres = dic["address"] as? String{
                                text = strAdres
                            }
                        }
                    // text = self.dictDropOffAddess["seaport_address"]!["address"] as! String
                    default :
                        break
                    }
                    cellButton.btnCellOptions.setImage(UIImage(named: "dropoff"), for: UIControlState())
                }
                if text == "" {
                    text = "N/A"
                }
                cellButton.btnCellOptions.setTitle(text, for: UIControlState())
                
                return cellButton
            }
                
                
                // MARK:------- Notes Pickup && DropOff cell-----
            else if indexPath.row == 0
            {
                let cellIndetifier = "cellRideLaterWithTextView"
                let cell : UITableViewCell  = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier, for: indexPath) //dequeueReusableCell(withIdentifier: cellIndetifier)! as UITableViewCell
                let textViewNotes = cell.viewWithTag(41) as! UITextView
                textViewNotes.translatesAutoresizingMaskIntoConstraints = true
                textViewNotes.frame = CGRect(x: 15,y: 10,width: self.view.frame.width - 30,height: deviceHeight - 1)
                Globals.layoutViewFor( textViewNotes, color: UIColor.lightGray, width: 1.0, cornerRadius: 3)
                textViewNotes.delegate = self
                textViewNotes.inputAccessoryView = self.accessoryViewTextView
                textViewNotes.font = Globals.defaultAppFont(14)
                cell.contentView.tag = indexPath.section
                var text :String! = ""
                if indexPath.section == keyIndexSection.pickType.rawValue {
                    if let text1 = self.dictPickAddess[Constants.PickNotes] {
                        text = String(describing: text1)
                    }
                }else if indexPath.section == keyIndexSection.dropType.rawValue {
                    if let text2 = self.dictDropOffAddess[Constants.DropNotes] {
                        text = String(describing: text2)
                    }
                }
                textViewNotes.text = text
                return cell
            }
        } else if indexPath.section == keyIndexSection.stops.rawValue
        {
            let  cellIndetifier = "cellRideLaterWithStops"
            //cellCarInfo
            cell   = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier, for: indexPath)//dequeueReusableCell(withIdentifier: cellIndetifier)!
            var lblText : UILabel!
            var viewOuter : UIView!
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
                lblText = cell!.viewWithTag(63) as! UILabel
                viewOuter = cell!.viewWithTag(62)! as UIView
                
            }else {
                lblText = cell!.viewWithTag(63) as! UILabel
                viewOuter = cell!.viewWithTag(62)! as UIView
                
            }
            Globals.layoutViewFor( viewOuter, color: UIColor.lightGray, width: 1.0, cornerRadius: 3)
            
                let dicStops = self.arrStops[indexPath.row]
                var strStopAddress = ""
                if let strStop = dicStops["location"] as? String {
                    strStopAddress = strStop
                }
                if let strStop = dicStops["stop_name"] as? String {
                    strStopAddress = strStop
                }
                lblText.text = strStopAddress
                lblText.numberOfLines = 4
                lblText.font = Globals.defaultAppFont(14)
                lblText.textAlignment = .center
            return cell!
        }
        else if indexPath.section == keyIndexSection.childSeatCount.rawValue {
            
            let  cellIndetifier = "cellRideLaterAddedChildSeatCount"
            //cellCarInfo
            cell   = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier)!
            var lblText : UILabel!
            var viewOuter : UIView!
            var btnDelete : UIButton!
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
                
                viewOuter = cell!.viewWithTag(100)! as UIView
                lblText = viewOuter!.viewWithTag(20) as! UILabel
                btnDelete = viewOuter!.viewWithTag(21) as! UIButton
            }else {
                viewOuter = cell!.viewWithTag(100)! as UIView
                lblText = viewOuter!.viewWithTag(20) as! UILabel
                btnDelete = viewOuter!.viewWithTag(21) as! UIButton
            }
            cell!.contentView.tag = indexPath.row
            Globals.layoutViewFor( viewOuter, color: UIColor.lightGray, width: 1.0, cornerRadius: 3)
            btnDelete.addTarget(self, action: #selector(PAReservationsRideLaterVC.btnDeleteChildSeatCount(_:)), for: .touchUpInside)
            let dicChildSeat = self.arrAddedChildSeats[indexPath.row]
            let name = dicChildSeat["name"] as! String
            let count = dicChildSeat["count"] as! String
            lblText.text = "\(name)   \(count)"
            lblText.numberOfLines = 4
            lblText.font = Globals.defaultAppFont(14)
            lblText.textAlignment = .left
            
            cell?.selectionStyle = .none
            
            return cell!
        }else  if indexPath.section == keyIndexSection.serviceType.rawValue {
            
            let cellIndetifier = "cellRideLaterAddHours"
            let  cellTextField = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier, for: indexPath) as! cellReservationForTextField
            //dequeueReusableCellWithIdentifier(cellIndetifier) as! cellReservationForTextField
            cellTextField.txtFieldCell.translatesAutoresizingMaskIntoConstraints = true
            cellTextField.txtFieldCell.frame = CGRect(x: 15, y: 8, width: ScreenSize.SCREEN_WIDTH - 30.0, height: deviceHeight - 2.0)
            cellTextField.txtFieldCell.layer.borderColor = UIColor.lightGray.cgColor
            cellTextField.txtFieldCell.layer.borderWidth = 1.0
            Globals.layoutViewFor(cellTextField.txtFieldCell, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 3.0)
            cellTextField.txtFieldCell.tag = keyIndexSection.hourly.rawValue
            cellTextField.txtFieldCell.delegate = self
            cellTextField.txtFieldCell.leftView = UIView(frame: CGRect(x: 0,y: 0,width: 12,height: 0))
            cellTextField.txtFieldCell.leftViewMode = .always
            cellTextField.txtFieldCell.font = Globals.defaultAppFont(14)
            cellTextField.txtFieldCell.rightView =  self.addRightViewToTextField(withImage: imageNameSection.Service_Vehicle_Payment.rawValue)//UIView(frame: CGRect(x: self.view.frame.width - 20,y: 0,width: 12,height: 0))
            cellTextField.txtFieldCell.rightViewMode = .always
            cellTextField.txtFieldCell.inputView = self.viewPickerView
            //    cellTextField.txtFieldCell.background = UIImage(named: "arrowRightTextField")
            var attrsTitle : NSMutableAttributedString!
            if self.strHours.compare("0") {
                cellTextField.txtFieldCell.background = self.addBackGroundPattrenImage(isRedColor: true, view: cellTextField.txtFieldCell)
                attrsTitle = NSMutableAttributedString(string:"*", attributes: [NSForegroundColorAttributeName:UIColor.red,NSFontAttributeName: Globals.defaultAppFont(14)])
                attrsTitle.append(NSAttributedString(string: "  Add Hours"))
                cellTextField.txtFieldCell.attributedPlaceholder  = attrsTitle
                cellTextField.txtFieldCell.text = ""
            }else if strHours.contains("Minimum") == true {
                cellTextField.txtFieldCell.background = self.addBackGroundPattrenImage(isRedColor: true, view: cellTextField.txtFieldCell)
                attrsTitle = NSMutableAttributedString(string:"*", attributes: [NSForegroundColorAttributeName:UIColor.red,NSFontAttributeName: Globals.defaultAppFont(14)])
                attrsTitle.append(NSAttributedString(string: "  \(strHours)"))
                cellTextField.txtFieldCell.attributedPlaceholder  = attrsTitle
            } else {
                cellTextField.txtFieldCell.background = self.addBackGroundPattrenImage(isRedColor: false, view: cellTextField.txtFieldCell)
                cellTextField.txtFieldCell.text = Int(strHours) == 1 ?  "\(strHours) Hour" :  "\(strHours) Hours"
            }
            return cellTextField
        }
        
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == keyIndexSection.pickType.rawValue ||  indexPath.section == keyIndexSection.dropType.rawValue {
            if indexPath.row == 0 {
                return deviceHeight + 10
            }
            return deviceHeight + 10
        }
        if indexPath.section == keyIndexSection.serviceType.rawValue {
            return deviceHeight + 10
        }
        return deviceHeight
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch (section){
        case keyIndexSection.pickType.rawValue :
            if  self.strPickType != pickupType.None.rawValue {
                let height = self.getHeightFromString(withText: self.getPickupAddressWith().Addres)
                return height <= deviceHeight ? deviceHeight: height + 20
            }
            return deviceHeight
        case  keyIndexSection.dropType.rawValue :
            if  self.strDropOffType != DropOffType.None.rawValue {
                let height = self.getHeightFromString(withText: self.getDropOffAdressWith().Addres)
                return height <=  deviceHeight ? deviceHeight: height + 20
            }
            return deviceHeight
        case keyIndexSection.pickUpDate.rawValue, keyIndexSection.passangerName.rawValue :
            return deviceHeight + 35
        default :
            return deviceHeight
        }
        
        
    }


    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let arrDicKeys = (self.arrDataSource[section]).keys
        
        let strKey = arrDicKeys.first!
        let strTitle = self.arrDataSource[section][strKey] as? String
        var  cellIndetifier = String()
        switch (strKey){
        // MARK: Pick Date && Name
        case  Constants.PickUpDate, Constants.PassangerName :
            cellIndetifier = "cellTextFieldWithHeaderInfo"
            let  cellTextField = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier) as! cellHeaderSectionTextField  //
            Globals.layoutViewFor( cellTextField.txtFieldCell, color: UIColor.lightGray, width: 1.0, cornerRadius: 3)
            cellTextField.txtFieldCell.placeholder = strKey
            cellTextField.txtFieldCell.tag = section
            cellTextField.lblHeaderTitle.tag = section + 1000
            cellTextField.txtFieldCell.delegate = self
            cellTextField.txtFieldCell.leftView = UIView(frame: CGRect(x: 0,y: 0,width: 12,height: 0))
            cellTextField.txtFieldCell.leftViewMode = .always
            //UIView(frame: CGRect(x: self.view.frame.width - 20,y: 0,width: 12,height: 0))
            cellTextField.txtFieldCell.rightViewMode = .always
            cellTextField.txtFieldCell.setValue(UIColor.red, forKeyPath: "_placeholderLabel.textColor")
            cellTextField.txtFieldCell.placeholder = "* " + strKey
            cellTextField.lblHeaderTitle.font = Globals.defaultAppFont(16)
            cellTextField.txtFieldCell.font = Globals.defaultAppFont(14)
            if strKey == Constants.PickUpDate{
                cellTextField.txtFieldCell.rightView =  self.addRightViewToTextField(withImage: imageNameSection.PickupDate.rawValue)
                cellTextField.lblHeaderTitle.text = "Booking Information"
                cellTextField.txtFieldCell.text = strTitle!
                if strTitle?.compare("Pickup Date") == true {
                    cellTextField.txtFieldCell.setValue(UIColor.gray, forKeyPath: "_placeholderLabel.textColor")
                    var attrsTitle : NSMutableAttributedString!
                    attrsTitle = NSMutableAttributedString(string:"", attributes: [NSForegroundColorAttributeName:UIColor.red,NSFontAttributeName: Globals.defaultAppFont(14)])
                    attrsTitle.append(NSAttributedString(string: " \(strTitle!)"))
                    cellTextField.txtFieldCell.attributedPlaceholder  = attrsTitle
                    cellTextField.txtFieldCell.text = ""
                    cellTextField.txtFieldCell.background = self.addBackGroundPattrenImage(isRedColor: true, view: cellTextField.txtFieldCell)
                }
                else {
                    cellTextField.txtFieldCell.background = self.addBackGroundPattrenImage(isRedColor: false, view: cellTextField.txtFieldCell)
                }
                
                cellTextField.txtFieldCell.inputView = self.viewDatePicker
                
            }
            else  if strKey == Constants.PassangerName  {
                // cellTextField.txtFieldCell.layer.borderColor = UIColor.appRideLater_RedColor().cgColor
                cellTextField.lblHeaderTitle.text = "Passenger Information"
                
                if  let val = self.arrDataSource[keyIndexSection.passangerName.rawValue][Constants.PassangerName] as? String{
                    if !val.isEmpty{
                        cellTextField.txtFieldCell.text = val
                    }
                    else {
                        cellTextField.txtFieldCell.text = strTitle!
                    }
                }
                cellTextField.txtFieldCell.background  = nil
            }
            else {
                cellTextField.txtFieldCell.text = strTitle!
            }
            cellTextField.translatesAutoresizingMaskIntoConstraints = true
            cellTextField.lblHeaderTitle.translatesAutoresizingMaskIntoConstraints = true
            cellTextField.txtFieldCell.translatesAutoresizingMaskIntoConstraints = true
            cellTextField.lblHeaderTitle.frame = CGRect(x: 15,y: 0,width: self.view.frame.width - 30,height: 30)
            cellTextField.txtFieldCell.frame = CGRect(x: 15,y: 32,width: self.view.frame.width - 30,height: deviceHeight)
            cellTextField.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: deviceHeight + 35)
            let view = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height: deviceHeight + 35))
            view.addSubview(cellTextField)
            return view
            
        // MARK: PassangerNumber && ==Vehicle === ChildSeatType=== Service
        case  Constants.PassangerNumber, Constants.ChildSeatType, Constants.Service :
            cellIndetifier = "cellRideLaterWithTextField"  //cellTextFieldWithHeaderInfo
            let  cellTextField = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier) as! cellReservationForTextField  //
            Globals.layoutViewFor( cellTextField.txtFieldCell, color: UIColor.lightGray, width: 1.0, cornerRadius: 3)
            
            if Constants.countyApp == "1"{
                cellTextField.txtFieldCell.placeholder = "Passenger Email ID"
            }
            else {
                cellTextField.txtFieldCell.placeholder = strKey
            }
            
            cellTextField.txtFieldCell.tag = section
            cellTextField.txtFieldCell.delegate = self
            cellTextField.txtFieldCell.leftView = UIView(frame: CGRect(x: 0,y: 0,width: 12,height: 0))
            cellTextField.txtFieldCell.leftViewMode = .always
            cellTextField.txtFieldCell.rightViewMode = .always
            cellTextField.txtFieldCell.font = Globals.defaultAppFont(14)
            if section == keyIndexSection.passangerNumber.rawValue {
                cellTextField.txtFieldCell.setValue(UIColor.red, forKeyPath: "_placeholderLabel.textColor")
                
                if Constants.countyApp == "1"{
                    cellTextField.txtFieldCell.placeholder = "Passenger Email ID"
                }
                else {
                    cellTextField.txtFieldCell.placeholder = strKey
                }
            }
            else
            {
                cellTextField.txtFieldCell.setValue(UIColor.gray, forKeyPath: "_placeholderLabel.textColor")
                cellTextField.txtFieldCell.placeholder = strKey
            }
             if strKey == Constants.Service
            {
                cellTextField.txtFieldCell.rightView = self.addRightViewToTextField(withImage: imageNameSection.Service_Vehicle_Payment.rawValue)
                if self.strServiceType != "" {
                    cellTextField.txtFieldCell.background = self.addBackGroundPattrenImage(isRedColor: false, view: cellTextField.txtFieldCell)
                    cellTextField.txtFieldCell.text = self.strServiceType
                }else {
                    cellTextField.txtFieldCell.text = strKey
                    cellTextField.txtFieldCell.background = self.addBackGroundPattrenImage(isRedColor: true, view: cellTextField.txtFieldCell)
                }
            }
            else  if strKey == Constants.PassangerNumber  {
                if  let val = self.arrDataSource[keyIndexSection.passangerNumber.rawValue][Constants.PassangerNumber] as? String{
                    if !val.isEmpty{
                        cellTextField.txtFieldCell.text = val
                    }
                    else {
                        cellTextField.txtFieldCell.text = strTitle!
                    }
                }
                cellTextField.txtFieldCell.background  = nil
            }
            else if strKey == Constants.ChildSeatType {
                cellTextField.txtFieldCell.rightView = self.addRightViewToTextField(withImage: imageNameSection.Child_Seat_Count.rawValue)
                cellTextField.txtFieldCell.text = strTitle!
                cellTextField.txtFieldCell.background = nil
                
                cellTextField.txtFieldCell.inputView = self.viewPickerView
            }else {
                cellTextField.txtFieldCell.text = strTitle!
            }
            cellTextField.translatesAutoresizingMaskIntoConstraints = true
            cellTextField.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: deviceHeight)
            let view = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height: deviceHeight))
            view.addSubview(cellTextField)
            return view
        // MARK: PickType
        case Constants.PickType  :
            cellIndetifier = "cellPickUpAddress"
            let  cellPickup = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier) as! CellReservationMutlipleButtonsTBCell
            cellPickup.btnCellOptions.addTarget(self, action: #selector(PAReservationsRideLaterVC.btnCellPickupAddressTypeAction(_:)), for: .touchUpInside)
            cellPickup.btnCellOptions.setImage(UIImage(named: "pickup"), for: UIControlState())
            Globals.layoutViewFor( cellPickup.btnCellOptions, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 3)
            cellPickup.btnCellOptions.titleLabel?.numberOfLines = 10
            cellPickup.btnCellOptions.titleLabel?.font = Globals.defaultAppFont(14)
            cellPickup.btnCellOptions.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
            cellPickup.btnCellOptions.titleEdgeInsets = UIEdgeInsetsMake(0, 13, 0,5)
            cellPickup.btnCellOptions.tag = section + btnTag.btnAddress.rawValue
            cellPickup.btnCellOptions.setBackgroundImage(self.addBackGroundPattrenImage(isRedColor: self.strPickType != pickupType.None.rawValue ? false : true , view: cellPickup.btnCellOptions), for: .normal)
            cellPickup.btnCellOptions.setAttributedTitle(self.getPickupAddressWith().Attrs, for: .normal)
            cellPickup.contentView.tag = section
            cellPickup.translatesAutoresizingMaskIntoConstraints = true
            var cellHeight :CGFloat = deviceHeight
            if  self.strPickType != pickupType.None.rawValue {
                cellHeight = self.getHeightFromString(withText: self.getPickupAddressWith().Addres)
            }
            cellPickup.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: cellHeight <= deviceHeight ? deviceHeight : cellHeight + 20)
            let view = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height:  cellHeight <= deviceHeight ? deviceHeight : cellHeight + 20))
            view.addSubview(cellPickup)
            return view
        // MARK: DropType
        case Constants.DropType :
            cellIndetifier = "cellDropOffAddress"
            let  cellDrop = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier) as! CellReservationMutlipleButtonDropOff
            cellDrop.btnCellOptions.addTarget(self, action: #selector(PAReservationsRideLaterVC.btnCellDropOffAddressTypeAction(_:)), for: .touchUpInside)
            cellDrop.btnCellOptions.tag = section + btnTag.btnAddress.rawValue
            Globals.layoutViewFor( cellDrop.btnCellOptions, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 3)
            cellDrop.btnCellOptions.setImage(UIImage(named: "dropoff"), for: UIControlState())
            cellDrop.btnCellOptions.titleLabel?.numberOfLines = 10
            cellDrop.btnCellOptions.titleLabel?.font = Globals.defaultAppFont(14)
            cellDrop.btnCellOptions.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
            cellDrop.btnCellOptions.titleEdgeInsets = UIEdgeInsetsMake(0, 13, 0,5)
            cellDrop.btnCellOptions.setBackgroundImage(self.addBackGroundPattrenImage(isRedColor: self.strDropOffType != DropOffType.None.rawValue ? false : true , view: cellDrop.btnCellOptions), for: .normal)
            cellDrop.btnCellOptions.setAttributedTitle(self.getDropOffAdressWith().Attrs, for: .normal)
            cellDrop.contentView.tag = section
            cellDrop.translatesAutoresizingMaskIntoConstraints = true
            var cellHeight :CGFloat = deviceHeight
            if  self.strDropOffType != DropOffType.None.rawValue {
                cellHeight = self.getHeightFromString(withText: self.getDropOffAdressWith().Addres)
            }
            cellDrop.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: cellHeight <= deviceHeight ? deviceHeight : cellHeight + 20)
            let view = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height: cellHeight <= deviceHeight ? deviceHeight : cellHeight + 20))
            view.addSubview(cellDrop)
            return view
        // MARK: Stops===== Payment
        case Constants.Stops, Constants.Payment :
            let view = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height: deviceHeight))
            let btnOption = UIButton(type: .custom)
            if (Constants.iPadScreen == true){
                btnOption.frame = CGRect(x: 15,y: 0,width: self.view.frame.width - 30,height: deviceHeight)
                btnOption.imageEdgeInsets = UIEdgeInsetsMake(0, self.view.frame.width - 60, 0, 0)
                btnOption.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0,  0)
            }else {
                btnOption.frame = CGRect(x: 15,y: 0,width: self.view.frame.width - 30,height: deviceHeight)
                btnOption.imageEdgeInsets = UIEdgeInsetsMake(0, self.view.frame.width - 60, 0, 0)
                //    btnOption.imageRect(forContentRect: CGRect(x: self.view.frame.width - 90,y: 20,width: 20,height: 20))
                btnOption.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0,  0)
            }
            Globals.layoutViewFor( btnOption, color: UIColor.lightGray, width: 1.0, cornerRadius: 3)
            btnOption.titleLabel?.numberOfLines = 4
            btnOption.contentHorizontalAlignment = .left
            btnOption.tag = section
            btnOption.backgroundColor = UIColor.white
            btnOption.addTarget(self, action: #selector(PAReservationsRideLaterVC.btnCellStopsAndPaymentAction(_:)), for: UIControlEvents.touchUpInside)
            if strKey == Constants.Stops {
                btnOption.setImage(UIImage(named: "plusButton"), for: UIControlState())
                let attrsTitle = NSAttributedString(string: "Stops", attributes: [NSForegroundColorAttributeName:UIColor.darkGray, NSFontAttributeName: Globals.defaultAppFont(14)])
                btnOption.setAttributedTitle(attrsTitle, for: UIControlState())
                
            }else {//arrowRightTextField  arrowRight
                //   btnOption.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
                //   btnOption.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10)
                //let imageView = UIImageView(frame: CGRect(x: self.view.frame.width - 90,y: 20,width: 20,height: 20))
                // imageView.image = UIImage(named: imageNameSection.Service_Vehicle_Payment.rawValue)
                //btnOption.imageView = imageView
                //btnOption.imageEdgeInsets = UIEdgeInsetsMake(0, self.view.frame.width - 45, 0, 0)
                btnOption.imageEdgeInsets = UIEdgeInsetsMake(0, self.view.frame.width - 55, 0, 0)
                btnOption.setImage(UIImage(named: "arrowRightButton"), for: UIControlState())
                //btnOption.setBackgroundImage(UIImage(named: "arrowRightTextField"), for: UIControlState())
                btnOption.setBackgroundImage(self.addBackGroundPattrenImage(isRedColor: true, view: btnOption), for: .normal)
                // btnOption.setImage(, forState: .Normal)
                if self.strPayment != "Payment" {
                    let attrsTitle = NSAttributedString(string: self.strPayment, attributes: [NSForegroundColorAttributeName:UIColor.darkGray,NSFontAttributeName: Globals.defaultAppFont(14.0)])
                    btnOption.setAttributedTitle(attrsTitle, for: UIControlState())
                    btnOption.setBackgroundImage(self.addBackGroundPattrenImage(isRedColor: false, view: btnOption), for: .normal)
                }else {
                    let attrsMUTitle : NSMutableAttributedString!
                    attrsMUTitle = NSMutableAttributedString(string:"", attributes: [NSForegroundColorAttributeName:UIColor.red,NSFontAttributeName: Globals.defaultAppFont(14.0)])
                    let attrsTitle = NSAttributedString(string: " \(self.strPayment)", attributes: [NSForegroundColorAttributeName:UIColor.gray,NSFontAttributeName: Globals.defaultAppFont(14.0)])
                    attrsMUTitle.append(attrsTitle)
                    btnOption.setAttributedTitle(attrsMUTitle, for: UIControlState())
                }
            }
            btnOption.translatesAutoresizingMaskIntoConstraints = true
            view.addSubview(btnOption)
            return view
        // MARK: ChildCount
        case Constants.ChildCount :
            let  cellIndetifier = "cellRideLaterChildSeatCount"
            //cellCarInfo
            var cellChild  = self.tableViewReservations.dequeueReusableCell(withIdentifier: cellIndetifier)!
            var txtChildSeatCount : UITextField!
            var btnAddChildSeat : UIButton!
            if cellChild == nil {
                cellChild = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
                txtChildSeatCount = cellChild.viewWithTag(55) as! UITextField
                btnAddChildSeat = cellChild.viewWithTag(56)! as! UIButton
            }else {
                txtChildSeatCount = cellChild.viewWithTag(55) as! UITextField
                btnAddChildSeat = cellChild.viewWithTag(56)! as! UIButton
            }
            txtChildSeatCount.font = Globals.defaultAppFont(14)
            btnAddChildSeat.titleLabel?.font = Globals.defaultAppFont(14)
            let view = UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height: deviceHeight))
            txtChildSeatCount.frame = CGRect(x: 15,y: 0,width: self.view.frame.width - 115 ,height: deviceHeight)
            btnAddChildSeat.frame = CGRect(x: self.view.frame.width - 105,y: 0,width: 105 ,height: deviceHeight)
            let viewleft = UIView(frame: CGRect(x: 0,y: 0,width: 12,height: 0))
            Globals.layoutViewFor( txtChildSeatCount, color: UIColor.lightGray, width: 1.0, cornerRadius: 3)
            
            txtChildSeatCount.leftView = viewleft
            txtChildSeatCount.leftViewMode = .always
            txtChildSeatCount.inputView = self.viewPickerView
            
            txtChildSeatCount.delegate = self
            Globals.layoutViewFor( btnAddChildSeat, color: UIColor.appRideLater_RedColor(), width: 1.0, cornerRadius: 3)
            btnAddChildSeat.backgroundColor = UIColor.appRideLater_RedColor()
            btnAddChildSeat.setTitleColor(UIColor.white, for: .normal)
            btnAddChildSeat.addTarget(self, action: #selector(PAReservationsRideLaterVC.btnAddChildSeatCountAction(_:)), for: .touchUpInside)
            txtChildSeatCount.text = strTitle
            let viewRight = UIView(frame: CGRect(x: self.view.frame.width - 135.0,y: 20,width: 25,height: 20))
            let imageView = UIImageView(frame: CGRect(x: 0,y: 0,width: 20,height: 20))
            imageView.image = UIImage(named: imageNameSection.Child_Seat_Count.rawValue)
            viewRight.addSubview(imageView)
            txtChildSeatCount.rightView = viewRight
            txtChildSeatCount.rightViewMode = .always
            txtChildSeatCount.background = nil
            cellChild.translatesAutoresizingMaskIntoConstraints = true
            cellChild.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: deviceHeight)
            
            view.addSubview(cellChild)
            return view
        case Constants.AdultLagguge:
            self.txtAdult.font = Globals.defaultAppFont(14)
            self.txtLagguge.font = Globals.defaultAppFont(14)
            if let lblAdult = self.viewAdultLuggageTbl.viewWithTag(666) as? UILabel {
                lblAdult.font = Globals.defaultAppFont(14)
            }
            if let lbllaguge = self.viewAdultLuggageTbl.viewWithTag(777) as? UILabel {
                lbllaguge.font = Globals.defaultAppFont(14)
            }
            if let lblAdult = self.viewAdultLuggageTbl.viewWithTag(888) as? UILabel {
                lblAdult.font = Globals.defaultAppFont(14)
            }
            return self.viewAdultLuggageTbl
        default :
            return nil
        }
    }

    // MARK: - ====table View Delegate === ===
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if tableView == self.tableViewReservations {
            if indexPath.section == keyIndexSection.stops.rawValue {
                return true
            }else {
                return false
            }
        }
        return false
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        
        if(indexPath.section == keyIndexSection.stops.rawValue){
            
            return UITableViewCellEditingStyle.delete
            
        }
        return UITableViewCellEditingStyle.none
        
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        if editingStyle == .delete{
            if indexPath.section  == keyIndexSection.stops.rawValue {
                self.arrStops.remove(at: indexPath.row)
                self.arrDataSource[indexPath.section][Constants.Stops] = self.arrStops as Any?
                self.reloadTableViewOnMainQueue()
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        }
    }
    // MARK: ---======= Some global Methos =====
    extension PAReservationsRideLaterVC {
    func addRightViewToTextField(withImage Image:String) -> UIView {
        let viewRight = UIView(frame: CGRect(x: self.view.frame.width - 90,y: 20,width: 25,height: 20))
        let imageView = UIImageView(frame: CGRect(x: 0,y: 0,width: 20,height: 20))
        imageView.image = UIImage(named: Image)
        viewRight.addSubview(imageView)
        return viewRight
    }
    func addBackGroundPattrenImage(isRedColor selected:Bool, view:UIView) -> UIImage {
        var strImageName = "backPattrenGreen"
        view.layer.borderColor = UIColor.appRideLater_GreenColor().cgColor
        if selected {
            strImageName = "backPattrenRed"
            view.layer.borderColor = UIColor.appRideLater_RedColor().cgColor
        }
        return UIImage(named: strImageName)!
    }
    func getPickupAddressWith() ->  ( Attrs:NSMutableAttributedString,Addres: String ) {
        var attrsTitle : NSMutableAttributedString!
        attrsTitle = NSMutableAttributedString(string:"Pick-Up Address \n", attributes: [NSForegroundColorAttributeName:UIColor.appRideLater_RedColor(),NSFontAttributeName: Globals.defaultAppFont(12)])
        var address = "N/A"
        switch self.strPickType {
        case pickupType.Airport.rawValue :
            if let dic = self.dicOnlyAirport["pickup"] as? [String:Any]{
                if let strAdres = dic["airport_name"]{
                    address = "\(strAdres)"
                }
            }
        case pickupType.Address.rawValue, pickupType.POI.rawValue :
            if let strAdres = self.dictPickAddess["address"] as? String{
                address = strAdres
            }
        case pickupType.FBO.rawValue :
                if let dic = self.dictPickAddess["fbo_details"] as? [String:Any]{
                if let strAdres = dic["address"]{
                    address = "\(strAdres)"
                }else {
                    if let dic = self.dictPickAddess["fbo_address"] as? [String:Any] {
                        if let strAdres = dic["address"] {
                            address = "\(strAdres)"
                        }
                    }
                }
            }
        case pickupType.Seaport.rawValue :
            if let dic = self.dictPickAddess["seaport_address"] as? [String:Any]{
                if let strAdres = dic["address"]{
                    address = "\(strAdres)"
                }
            }
        default :
            address = "Enter Pick-Up Address"
        }
        attrsTitle.append(NSAttributedString(string: "\(address)"))
        return(attrsTitle, address)
    }
    func getDropOffAdressWith() -> ( Attrs:NSMutableAttributedString,Addres: String ) {
        var attrsTitle : NSMutableAttributedString!
        attrsTitle = NSMutableAttributedString(string:"Drop-Off Address \n", attributes: [NSForegroundColorAttributeName:UIColor.appRideLater_GreenColor(),NSFontAttributeName: Globals.defaultAppFont(12)])
        var address = "N/A"
        switch self.strDropOffType {
        case DropOffType.Airport.rawValue :
            if let dic = self.dicOnlyAirport["dropoff"] as? [String:Any]{
                if let strAdres = dic["airport_name"]{
                    address = "\(strAdres)"
                }
            }
        case DropOffType.Address.rawValue , DropOffType.POI.rawValue:
            if let strAdres = self.dictDropOffAddess["address"]{
                address = "\(strAdres)"
            }
        case DropOffType.FBO.rawValue :
             if let dic = self.dictDropOffAddess["fbo_details"] as? [String:Any]{
                if let strAdres = dic["address"] {
                    address = "\(strAdres)"
                }else {
                    if let dic = self.dictDropOffAddess["fbo_address"] as? [String:Any] {
                        if let strAdres = dic["address"]{
                            address = "\(strAdres)"
                        }
                    }
                }
            }
        case DropOffType.Seaport.rawValue :
            if let dic = self.dictDropOffAddess["seaport_address"] as? [String:Any]{
                if let strAdres = dic["address"]{
                    address = "\(strAdres)"
                    
                }
            }
        default :
            address = "Enter Drop-Off Address"
        }
        attrsTitle.append(NSAttributedString(string: "\(address)".isEmpty ? "N/A" : "\(address)"))
        return(attrsTitle, address)
    }
    func getHeightFromString(withText  Text: String) -> CGFloat {
        let strAddress: NSString = Text as NSString
        let str : NSString = strAddress as NSString //.//trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) as NSString
        // let str = self.offerDescModel!.description!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        var font = 17
        var screenWidth : CGFloat = 20
        if  Constants.iPadScreen == true {
            font = 20
            screenWidth =  40
        }
        let constraint: CGSize =  CGSize(width: ScreenSize.SCREEN_WIDTH - screenWidth, height: 1500.0)
        //cCGSizeMake(CGRectGetWidth(tableView.frame), 1500.0)
        
        let boundingBox: CGSize = str.boundingRect(with: constraint, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName:Globals.defaultAppFont(Float(font))], context: nil).size
        return CGFloat(boundingBox.height)
    }
    }
