//
//  PABottomViewAddress.swift
//  HotelPassengerApp
//
//  Created by Netquall on 26/04/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit

class PABottomViewAddress: UIView {

  //  @IBOutlet var viewSub: UIView!
    @IBOutlet var pickupAddress: UILabel!
    @IBOutlet var serviceType: UILabel!
    @IBOutlet var dateTime: UILabel!
      var viewMain : UIView!
    
 
    
    
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
    //    xibSetup()
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
    }
    override func awakeFromNib() {
        
        /*  let leftView1 = UIView(frame: CGRectMake(0,0,12,0))
         self.txtCardNumber.text! = strCardNumber
         self.txtCardNumber.layer.cornerRadius = 4.0
         self.txtCardNumber.layer.borderWidth = 1.0
         self.txtCardNumber.layer.borderColor = UIColor.DefaultGreenColor().CGColor
         self.txtCardNumber.leftView = leftView1
         self.txtCardNumber.leftViewMode = .Always
         
         let leftView2 = UIView(frame: CGRectMake(0,0,12,0))
         self.txtExpDateOfCard.text! = strExpDateCard
         self.txtExpDateOfCard.layer.cornerRadius = 4.0
         self.txtExpDateOfCard.layer.borderColor = UIColor.DefaultGreenColor().CGColor
         self.txtExpDateOfCard.layer.borderWidth = 1.0
         self.txtExpDateOfCard.leftView = leftView2
         self.txtExpDateOfCard.leftViewMode = .Always
         
         let leftView3 = UIView(frame: CGRectMake(0,0,12,0))
         self.txtCvvNumberOfCard.layer.cornerRadius = 4.0
         self.txtCvvNumberOfCard.layer.borderColor = UIColor.DefaultGreenColor().CGColor
         self.txtCvvNumberOfCard.layer.borderWidth = 1.0
         self.txtCvvNumberOfCard.leftView = leftView3
         self.txtCvvNumberOfCard.leftViewMode = .Always
         self.txtCvvNumberOfCard.text! = strCvvNumberCard */
        
    }
    
    func xibSetup() {
        
        
        viewMain  = Bundle.main.loadNibNamed("BottomViewAddress", owner: self, options: nil)?[0] as! UIView
        // use bounds not frame or it'll be offset
        viewMain.frame = bounds
        viewMain.tag = 32
        // Make the view stretch with containing view
        viewMain.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        //vieviewMainw.layer.cornerRadius = 5.0
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(viewMain)
        
    }
   
}
