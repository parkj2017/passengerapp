//
//  PASelectVehicleVC.swift
//  HotelPassengerApp
//
//  Created by Netquall on 26/04/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader

class PASelectVehicleVC: UIViewController, BackButtonActionProtocol {

    @IBOutlet var tblVehicle: UITableView!
    @IBOutlet var collectionVehicle: UICollectionView!
    var paramDic = [String:Any]()
    var arrVehicleType = [String]()
    var dicVehicleRecords = [[String:Any]]()
    var selectedIndex = 0
    var arrAllVehicleData = [[String:Any]]()
    var finishVehicleSelection:((_ dicVehicle:[String:Any]?)-> Void)!
    var strServiceCode:String = ""
    var contactNumberSupport:String = ""

    var dicWebServiceParam:[String:Any] = [String:Any]()
    
    var dictSelectedVehicleData = [String:Any]()
    var strPayment = "Payment"
    var dateSelected:NSDate = NSDate()
    
    var finishEditReservation: ((_ dicReservation:[String:Any]?) -> Void)!
    
    var isEditingMode:Bool = false
    var saved_reservaionID:String = ""
    @IBOutlet weak var topNavView: UIView!
    //Top navigation button actions
    
    
    @IBAction func btnTopNavButton(_ sender: UIButton) {
        if  sender.tag == 100{
            if self.navigationController != nil {
                for vc: UIViewController in (self.navigationController?.viewControllers)! {
                    if (vc.isKind(of: PAReservationsRideLaterVC.self)){
                        if self.finishEditReservation != nil{
                        self.finishEditReservation(self.dicWebServiceParam)
                        }
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }
        }else if sender.tag == 200{
            
        }else if sender.tag == 300{
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
      self.getVehicleRecords()
         self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Select Vehicle", imageName: "backArrow")
       // let recognizerRight = UISwipeGestureRecognizer(target: self, action: #selector(PASelectVehicleVC.didSwipeRight/(recognizer:)))
       // recognizerRight.direction = .left
       // self.tblVehicle.addGestureRecognizer(recognizerRight)//addGestureRecognizer(recognizer)
        //let recognizerLeft = UISwipeGestureRecognizer(target: self, action: #selector(PASelectVehicleVC.didSwipeLeft(recognizer:)))
       // recognizerLeft.direction = .right
       // self.tblVehicle.addGestureRecognizer(recognizerLeft)
        self.collectionVehicle.showsHorizontalScrollIndicator = false
        // Do any additional setup after loading the view.
        
        self.topNavView.layer.shadowColor = UIColor.lightGray.cgColor
        self.topNavView.layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
        self.topNavView.layer.shadowOpacity = 1.0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
    }
    func backButtonActionMethod(_ sender: UIButton) {
           self.finishVehicleSelection(nil)
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func MakeRequestWithAllTypeOfData()
    {
            if appDelegate.networkAvialable == true {

                if self.dictSelectedVehicleData.count != 0 {
                    self.dicWebServiceParam["primary_car"] = self.dictSelectedVehicleData["id"]
                    self.dicWebServiceParam["vehicle_type_title"] = self.dictSelectedVehicleData["vehicle_type_title"]

                }else {
                    self.showBannerWithMessage("Please select you preferred vehicle.")
                }
                appDelegate.showProgressWithText("Please Wait...")
                print("Please Wait... dicWebServiceParam \(self.dicWebServiceParam)")
                
                ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.createReservation, dicParameters: self.dicWebServiceParam as [String : Any], successBlock: { [weak self] (resDicData) -> Void in
                    appDelegate.hideProgress()
                    
                    var dicData = resDicData.formatDictionaryForNullValues(resDicData)!
                    if dicData["status"] as! String == "success"{
                        if let res_id = dicData["reservation_id"]{
                          //  self?.saved_reservaionID = "\(res_id)"
                            appDelegate.activeReservationID = "\(res_id)"
                            self?.dicWebServiceParam["reservation_id"] = "\(res_id)"

                            self?.reservaionCreated(response: dicData)
                        }
                    }else {
                        Globals.ToastAlertWithString(dicData["message"] as! String)
                    }
                    }, failureBlock: { (error) -> () in
                        appDelegate.hideProgress()
                        print(error)
                        Globals.ToastAlertWithString("Netowrk Error")
                })
            }else {
                appDelegate.hideProgress()
                Globals.ToastAlertForNetworkConnection()
            }
    }
    
    func reservaionCreated(response:[String:Any]) {
        let confirmVC = self.storyboard?.instantiateViewController(withIdentifier: "PAConfirmReservationVC") as! PAConfirmReservationVC
        
        if let conf_id = response["conf_id"]{
            confirmVC.saved_reservaionID = "\(conf_id)"
        }
        confirmVC.dicReservation = self.dicWebServiceParam
        confirmVC.dateSelected = self.dateSelected as Date
        confirmVC.strPickType = ""
        confirmVC.strCardId = String(describing: self.dicWebServiceParam["payment_name"]!)
        
        if "\(response["totalamount"]!)".isEmpty {
            confirmVC.strEstimateFare = "N/A"
        }
        else {
            confirmVC.strEstimateFare = appDelegate.currency + " " + String(describing:"\(response["totalamount"]!)")
        }
        confirmVC.vehicleName = String(describing: self.dictSelectedVehicleData["vehicle_type_title"]!)
        confirmVC.strServiceType =  String(describing: self.dicWebServiceParam["service_type_name"]!)
        confirmVC.isEditMode = self.isEditingMode
       

        confirmVC.finishEditReservation = { [weak self] (dicReservation:[String:Any]?) in
            if dicReservation == nil {
                self?.finishEditReservation(dicReservation)
                self?.dismiss(animated: true, completion: nil)
            }
        }
        self.navigationController?.pushViewController(confirmVC, animated: true)

    }
    func getVehicleRecords() {
        // let estFare = estDistance * Float(CarObject.base_per_km)!
         if self.paramDic.count == 0 {
            return
        }
        appDelegate.showProgressWithText("Vehicle Listing ")
        ServiceManager.sharedInstance.dataTaskWithPostRequest("get_vechile_listing", dicParameters: self.paramDic, successBlock: {[weak self] (dicData) -> Void in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success"{
                let vehichleData:[String : Any] = dicData.formatDictionaryForNullValues(dicData)!
//                if let vehicleCast = vehichleData["vehicle_cats"] as? [String] {
//                    self?.arrVehicleType = vehicleCast.first!.components(separatedBy: ",")
//                    self?.arrVehicleType.insert("All", at: 0)
//                }
                if let vehicleRecord = vehichleData["vehicleRecords"] as? [[String:Any]] {
                        self?.dicVehicleRecords = vehicleRecord
                  
                }
                if self?.dicVehicleRecords.count == 0 {
                    if let capacity = self?.paramDic["capacity"]{
                        Globals.ToastAlertWithString("No vehicle(s) found with passenger capacity \(capacity)")
                    }else {
                        Globals.ToastAlertWithString("No vehicle(s) found")
                    }
                }
                else {
                if let contact = vehichleData["contact"] {
                    if !"\(contact)".isEmpty{
                        self?.contactNumberSupport = "\(contact)"
                    }
                }
                
                for arrVehicle:[String:Any] in self!.dicVehicleRecords {
                    //for dicVehicle in arrVehicle {
                        self?.arrAllVehicleData.append(arrVehicle)
                    //}
                }
            }
                DispatchQueue.main.async(execute: { [weak self] in
                     //self?.collectionVehicle.reloadData()
                     self?.tblVehicle.reloadData()
                })
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
            }, failureBlock: { (error) -> () in
                appDelegate.hideProgress()
                Globals.ToastAlertWithString("Netowrk Error")
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func didTapSelectVehicleAction(sender:UIButton)  {
        var dicData : [String:Any]!
        if self.selectedIndex == 0 {
            dicData =  self.arrAllVehicleData[sender.tag % 1000]
        }
//        else {
//            let arrVehicle =  self.dicVehicleRecords[self.arrVehicleType[self.selectedIndex]]
//            if arrVehicle != nil && arrVehicle?.count != 0 {
//                dicData =  arrVehicle![sender.tag % 1000]
//            }
//        }
        
        //    dicData["vehicle_type_title"] = self.arrVehicleType[self.selectedIndex]
        if let vehicleId = dicData["vehicle_type_title"] {
            if String(describing: vehicleId).compare(Constants.partyBus) == true && self.strServiceCode.compare("HOURLY") == false && Constants.CompanyID == Constants.RoyalLimo {
                let alert = self.alertView("Info", message:"Your must choose service type as hourly for party bus or contact customer support at 1866-690-2200 for questions.", delegate:nil, cancelButton:"Ok", otherButons:nil)
                alert!.show()
                return
            }
        }
        
        self.dictSelectedVehicleData = dicData
        self.MakeRequestWithAllTypeOfData()
        
       // self.finishVehicleSelection(dicData)
      //  self.navigationController?.popViewController(animated: true)
    }
    
    
    func didTapFareBreakDownAction(sender:UIButton)  {
        let detailsVc = appDelegate.storybaord?.instantiateViewController(withIdentifier: "PAFareBreakDownPopupVC") as! PAFareBreakDownPopupVC
        var vehicleData : VehicleData!
        if self.selectedIndex == 0 {
            vehicleData = VehicleData()
            vehicleData = vehicleData.setupValueWithDataSource(dicParam: self.arrAllVehicleData[sender.tag % 10])
        }
//        else {
//            let arrVehicle =  self.dicVehicleRecords[self.arrVehicleType[self.selectedIndex]]
//            if arrVehicle != nil && arrVehicle?.count != 0 {
//                vehicleData = VehicleData()
//                vehicleData = vehicleData.setupValueWithDataSource(dicParam: arrVehicle![sender.tag % 10])
//            }
//        }
        if vehicleData == nil {
            self.showBannerWithMessage("No breakdown available.")
            return
        }
        
        detailsVc.currency = appDelegate.currency

        if let fare = vehicleData.totalamount as? String{
            if fare.isEmpty == false {
                //fare = fare.replacingOccurrences(of: ",", with: "")
                detailsVc.strTotalFare = String(format: "%@ %@",appDelegate.currency, fare)
            }
            else {
                detailsVc.strTotalFare = "0.0"
                self.showBannerWithMessage("No breakdown available.")

                return
            }
        }
        if let dicFare = vehicleData.farebreakdown as? [String:Any] {
            detailsVc.dicFareDetails = dicFare
        }
        if let dicFare = vehicleData.farebreakdown_arr as? [[String:Any]] {
            detailsVc.arrFareDetails = dicFare
        }
        detailsVc.showdisclaimer = true
        detailsVc.view.backgroundColor = UIColor.clear
        detailsVc.modalPresentationStyle = .overFullScreen
        detailsVc.providesPresentationContextTransitionStyle = true
        detailsVc.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overFullScreen
        self.present(detailsVc, animated: true, completion: nil)
    }
    func didTapCallNowAction(sender:UIButton)  {
        
        
    }
    func didChangeVehicleType(sender:UIButton)  {
        self.reloadTableViewWithTranstionAnimation(index: sender.tag)
    }
    func reloadTableViewWithTranstionAnimation(index:Int)  {
        if self.selectedIndex == index {
            return
        }
        if index == 0 || (self.selectedIndex  == 0 && index != 0) {
              self.selectedIndex = index
               self.tblVehicle.reloadData()
        }else {
             let transionType: UITableViewRowAnimation =  self.selectedIndex > index ? .right : .left
                self.selectedIndex = index
             let indexSet = NSIndexSet(index: 0)
              self.tblVehicle.reloadSections(indexSet as IndexSet, with: transionType)
        }
        self.collectionVehicle.reloadData()
        let indexPath = IndexPath(item: self.selectedIndex, section: 0)
        let scrollPosstion : UICollectionViewScrollPosition = self.selectedIndex > index ? .right : .left
        self.collectionVehicle.scrollToItem(at: indexPath, at: scrollPosstion, animated: true)
        
    }
    func didSwipeRight(recognizer: UIGestureRecognizer) {
        if recognizer.state == UIGestureRecognizerState.ended {
            if self.selectedIndex  == self.arrVehicleType.count - 1  {
                return
            }
                 self.reloadTableViewWithTranstionAnimation(index:self.selectedIndex + 1)
        }
    }
    func didSwipeLeft(recognizer: UIGestureRecognizer) {
        if recognizer.state == UIGestureRecognizerState.ended {
            if  self.selectedIndex == 0  {
                return
            }
            self.reloadTableViewWithTranstionAnimation(index:self.selectedIndex - 1)
        }
    }

}
extension  PASelectVehicleVC : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrVehicleType.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cellVehicle = collectionView.dequeueReusableCell(withReuseIdentifier: "cellVehicleDataCollection", for: indexPath) as! cellVehicleDataCollection
        cellVehicle.vehicleType.isSelected = false
        if selectedIndex == indexPath.row {
            cellVehicle.vehicleType.isSelected = true
        }
        cellVehicle.vehicleType.setTitle(arrVehicleType[indexPath.row], for: .normal)
        cellVehicle.vehicleType.titleLabel?.font = Globals.defaultAppFont(14)
        cellVehicle.vehicleType.tag = indexPath.row
        cellVehicle.vehicleType.setTitleColor(UIColor.appRideLater_TextColor(), for: .normal)
       cellVehicle.vehicleType.addTarget(self, action: #selector(PASelectVehicleVC.didChangeVehicleType(sender:)), for: .touchUpInside)
        cellVehicle.vehicleType.setBackgroundImage(UIImage(named:"address_select"), for: .selected)
        cellVehicle.vehicleType.setBackgroundImage(nil, for: .normal)
        return cellVehicle
    }
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let lblSize = UILabel()
        lblSize.text = arrVehicleType[indexPath.row]
        if Constants.iPadScreen == false   {
            lblSize.font = Globals.defaultAppFont(16)
            lblSize.sizeToFit()
            return CGSize(width: lblSize.frame.size.width < 50 ? 50 : lblSize.frame.size.width , height: 50)
        }
        else {
            lblSize.font = Globals.defaultAppFont(22)
            lblSize.sizeToFit()
          return  CGSize(width: lblSize.frame.size.width < 80 ? 80 : lblSize.frame.size.width , height: 50)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }

    
    
}

extension PASelectVehicleVC : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex == 0 {
            return self.arrAllVehicleData.count
        }else
        {
//            if let arrVehcile = self.dicVehicleRecords[self.arrVehicleType[self.selectedIndex]] {
//                return arrVehcile.count
//            }
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var vehicleData : VehicleData!
        if self.selectedIndex == 0 {
            vehicleData = VehicleData()
            vehicleData =   vehicleData.setupValueWithDataSource(dicParam: self.arrAllVehicleData[indexPath.row])
        }
//        else {
//            let arrVehicle =  self.dicVehicleRecords[self.arrVehicleType[self.selectedIndex]]
//            if arrVehicle != nil && arrVehicle?.count != 0 {
//                 vehicleData = VehicleData()
//                vehicleData = vehicleData.setupValueWithDataSource(dicParam: arrVehicle![indexPath.row])
//            }
//        }
        if vehicleData == nil {
           let cellNoVehicle = self.tblVehicle.dequeueReusableCell(withIdentifier: "cellNoVehicleAvailable", for: indexPath) 
            cellNoVehicle.textLabel?.text = "Opps! No Vehicle...."
              cellNoVehicle.textLabel?.font = Globals.defaultAppFontWithBold(18)
              cellNoVehicle.textLabel?.textColor = UIColor.appRideLater_RedColor()
            cellNoVehicle.textLabel?.textAlignment = .center
         //    cellNoVehicle.backgroundColor = UIColor.appRideLater_GrayColor()
            return cellNoVehicle
        }
        let cellList : cellVehicleTypeList  = self.tblVehicle.dequeueReusableCell(withIdentifier: "cellVehicleTypeList", for: indexPath) as! cellVehicleTypeList   //cellNoVehicleAvailable
          cellList.vehicleImage.contentMode = .scaleAspectFit
        cellList.vehicleImage.backgroundColor = UIColor.white
        DLImageLoader.sharedInstance().image(fromUrl:vehicleData.web_icon) { (error, image ) in
            if image != nil {
                cellList.vehicleImage.image = image
                cellList.activityIndicator.stopAnimating()
            }
            else{
                cellList.vehicleImage.image = UIImage(named:"noImage")
                cellList.activityIndicator.stopAnimating()
                
            }
        }
        cellList.vehicleName.text = vehicleData.fleetName
        cellList.totalFare.text = appDelegate.currency + " " + (vehicleData.totalamount == "" ? "N/A" : vehicleData.totalamount)
        
        cellList.adultCount.setTitle(vehicleData.vehicle_specific_passenger_capacity, for: .normal)
        cellList.lagguageCount.setTitle(vehicleData.vehicle_specific_luggage_capacity, for: .normal)
        cellList.tapForBreakDownButton.addTarget(self, action: #selector(PASelectVehicleVC.didTapFareBreakDownAction(sender:)), for: .touchUpInside)
        cellList.tapForBreakDownButton.tag = 10 + indexPath.row
        cellList.selectVehicleButton.addTarget(self, action: #selector(PASelectVehicleVC.didTapSelectVehicleAction(sender:)), for: .touchUpInside)
        cellList.selectVehicleButton.tag = 1000 + indexPath.row
   //     cellList.callNowButton.addTarget(self, action: #selector(PASelectVehicleVC.didTapFareBreakDownAction(sender:)), for: .touchUpInside)
           cellList.callNowButton.tag = 10000 + indexPath.row
        
        cellList.callNowButton.addTarget(self, action: #selector(PASelectVehicleVC.callSupport(sender:)), for: .touchUpInside)
        
        cellList.prizeNote.text = "Price includes Base Fare, Gratuity, STC charge does not include parking and toll charges"
        cellList.backgroundColor = indexPath.row % 2 == 0 ? UIColor.appRideLater_cellBackground(): UIColor.white
        return cellList
    }
    
     func callSupport(sender:UIButton)  {
        if !self.contactNumberSupport.isEmpty{
            if let url = URL(string: "tel://\(self.contactNumberSupport.replacingOccurrences(of: " ", with: ""))") {
                if (UIApplication.shared.canOpenURL(url)) {
                    UIApplication.shared.openURL(url)
                }
            }
            else {
                Globals.ToastAlertWithString("Not a valid contact number")
            }
        }
        else {
            Globals.ToastAlertWithString("Not a valid contact number")
        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if Constants.iPadScreen {
            if self.selectedIndex == 0 {
                 return 200
            }
//            else {
//                let arrVehicle =  self.dicVehicleRecords[self.arrVehicleType[self.selectedIndex]]
//                if arrVehicle != nil && arrVehicle?.count != 0 {
//                    return 200
//                }
//            }
            return 60
        }else {
            if self.selectedIndex == 0 {
                return 145
            }
//            else {
//                let arrVehicle =  self.dicVehicleRecords[self.arrVehicleType[self.selectedIndex]]
//                if arrVehicle != nil && arrVehicle?.count != 0 {
//                    return 145
//                }
//            }
            return 60
        }
    }
}
