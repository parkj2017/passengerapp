//
//  TutorialViewController.swift
//  UIPageViewController Post
//
//  Created by Jeffrey Burt on 2/3/16.
//  Copyright © 2016 Seven Even. All rights reserved.
//

import UIKit

class PAMainAddressVC: UIViewController, BackButtonActionProtocol {

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var containerView: UIView!
   var  addressModal = AddressDetails()
var finishSelectionAddress :((_ dicAddress:[String:Any]?, _ modal:AddressDetails, _ isDone : Bool) -> Void)!
    var pageViewController: PAPageControllerAddressVC? {
        didSet {
            pageViewController?.pagDelegate = self
        }
    }
    
    @IBOutlet weak var topAddressSelection: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        var imageinsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
        if ScreenSize.SCREEN_WIDTH == 320 {
          //  imageinsets.left = -
        }
        
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(addressModal.isPickUpAddress == true ? "Pick-Up Address" : "Drop-Off Address", imageName: "backArrow")
        for subViewsBtn in self.topAddressSelection.subviews {
            if subViewsBtn.isKind(of: UIButton.self) {
                let btnSelecteds = subViewsBtn as! UIButton
                  btnSelecteds.titleLabel?.font = Globals.defaultAppFont(14)
               btnSelecteds.setBackgroundImage(UIImage(named:"address_select"), for: .selected)
                btnSelecteds.setBackgroundImage(nil, for: .normal)
                if DeviceType.IS_IPAD {
                    if btnSelecteds.tag != 3 {
                        btnSelecteds.imageEdgeInsets = UIEdgeInsetsMake(-20, +10, 0, 0)
                    }
                }
            }
        }
       // self.didChangeSelectedButton(withTag: 1)
      //  segmentControl.addTarget(self, action: "didChangePageControlValue", for: .valueChanged)
      //  segmentControl.setImage(UIImage(named:"address_select"), forSegmentAt: 0)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //  NotificationCenter.default.addObserver(self, selector: #selector(PAMainAddressVC.hideTopButtons), name: NSNotification.Name(rawValue: Constants.kUpdateAddressSearchView), object: nil)
        Globals.sharedInstance.delegateBack = self
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.isOpaque = false
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
      //  NotificationCenter.default.removeObserver(self, name:  NSNotification.Name(rawValue: Constants.kUpdateAddressSearchView), object: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let pageViewController = segue.destination as? PAPageControllerAddressVC {
            self.pageViewController = pageViewController
            self.pageViewController?.addressModal = self.addressModal
            self.pageViewController?.finishSelectionAddress = { (dicAddress, modal,isDone) in
                // self.navigationController?.popViewController(animated: true)
                 self.finishSelectionAddress(dicAddress, modal, isDone)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    @IBAction func didTapChangePageContrllerViewButtons(_ sender: UIButton) {
           pageViewController?.scrollToViewController(index: sender.tag - 1)
          self.didChangeSelectedButton(withTag: sender.tag)
    }
    func backButtonActionMethod(_ sender: UIButton) {
        self.finishSelectionAddress(self.addressModal.dicDetails , self.addressModal , false)
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    /**
     Fired when the user taps on the pageControl to change its current page.
     */
    func didChangePageControlValue() {
        pageViewController?.scrollToViewController(index: segmentControl.selectedSegmentIndex)
    }
    func didChangeSelectedButton(withTag tag : Int)
    {
        for subViewsBtn in self.topAddressSelection.subviews {
            if subViewsBtn.isKind(of: UIButton.self) {
             let btnSelecteds = subViewsBtn as! UIButton
                 if btnSelecteds.tag == tag {
                    btnSelecteds.isSelected = true
                  }
                 else {
                        btnSelecteds.isSelected = false
                 }
            }
        }
    }
}

extension PAMainAddressVC: PAPageViewControllerDelegate {
    /**
     Called when the number of pages is updated.
     
     - parameter pageController: the pageController instance
     - parameter count: the total number of pages.
     */

    
    func tutorialPageViewController(_ tutorialPageViewController: PAPageControllerAddressVC,
        didUpdatePageCount count: Int) {
       // segment.numberOfPages = count
    }
    
    func tutorialPageViewController(_ tutorialPageViewController: PAPageControllerAddressVC,
        didUpdatePageIndex index: Int) {
        self.didChangeSelectedButton(withTag: index + 1)
        //segmentControl.selectedSegmentIndex = index
    }
    
}
