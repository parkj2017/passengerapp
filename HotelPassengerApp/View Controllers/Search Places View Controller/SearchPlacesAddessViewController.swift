//
//  SearchPlacesAddessViewController.swift
//  PassangerApp
//
//  Created by Netquall on 12/18/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GoogleMapsBase
class SearchPlacesAddessViewController: BaseViewController, BackButtonActionProtocol, GMSMapViewDelegate  {

// MARK:- ============ Property decalaration ==========

@IBOutlet var bottomSpaceBottomView: NSLayoutConstraint!
@IBOutlet var bottomViewHeight: NSLayoutConstraint!
@IBOutlet weak var lblTitleOfView: UILabel!
var resultsViewController: GMSAutocompleteResultsViewController?
var searchController: UISearchController?
var strTitleOfView : String!
var customSearchController: CustomSearchController!

@IBOutlet var bottomViewAddress: PABottomViewAddress!
@IBOutlet weak var doneHeigthConstraint: NSLayoutConstraint!
let strHome = "Home"
let strWork = "Work"
var isHomeAddress = false
var isWorkAddress = false
@IBOutlet weak var viewNavigation: UIView!
// call back block

@IBOutlet var viewImagePinLoc: UIView!
var finishSelectingDropOffAddress : ((_ dicDropOff : [String : Any]) -> ())?

var finishSelectingAddress : ((_ dicDropOff : [String : Any], _ tag:Int, _ isDone:Bool) -> ())?
@IBOutlet weak var btnHomeAddress: UIButton!

@IBOutlet weak var btnWorkAddress: UIButton!
var dicAddressDetails : [String:Any]!

@IBOutlet weak var mapViewSelectAddress: GMSMapView!

lazy var dicSelectedAdddress  = [String:Any]()

@IBOutlet weak var btnDoneBottom: UIButton!
lazy var locationManagerObjct = LocationManager.sharedInstance

var isPickUpAddress: Bool!
var map_CentreCoordinate : CLLocationCoordinate2D?
var btnRightDone : UIBarButtonItem!
var strTypeOfAddress:String?
var dicDetails :[String:Any]!
var tagBtnAddressLast : Int! = 0
var viewFrom:String? = ""
 var searchBarView = UIView()
var modalAdress = AddressDetails()
var isStopsAddress = false
    
    @IBOutlet weak var markerPinImage: UIImageView!
    
// MARK:- ============View Life Cycyle =============
override func viewDidLoad() {
    
    super.viewDidLoad()
    self.getHomeAndOfficeDetails()
    self.navigationController?.navigationBar.isHidden = false
    self.navigationController?.navigationBar.isTranslucent = false
            self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(strTypeOfAddress!, imageName: "backArrow")
    if strTypeOfAddress!.compare("Stops Address") || viewFrom!.compare("onDemand") {
         self.isStopsAddress = true
        self.bottomViewAddress.isHidden = true
        var mapFrame =    self.mapViewSelectAddress.frame
        mapFrame.size.height = self.view.frame.size.height - 64.0 - 50.0
        mapFrame.size.width = self.view.frame.size.width
        self.mapViewSelectAddress.translatesAutoresizingMaskIntoConstraints = true
        self.mapViewSelectAddress.frame = mapFrame
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
    
    }else {
        self.bottomViewHeight.constant = (self.isPickUpAddress == true ? 70 : 100)
        self.view.layoutIfNeeded()
        self.updateViewConstraints()
        self.bottomViewAddress.dateTime.text = "Date & Time: " + modalAdress.strDateTime
        self.bottomViewAddress.pickupAddress.text = "Pick-Up Address: " + modalAdress.PickupAddress
        self.bottomViewAddress.serviceType.text = "No of Passenger(s): " + modalAdress.noOfPax
        self.bottomViewAddress.pickupAddress.numberOfLines = 3
        self.bottomViewAddress.serviceType.font = Globals.defaultAppFont(14)
        self.bottomViewAddress.pickupAddress.font = Globals.defaultAppFont(14)
        self.bottomViewAddress.dateTime.font = Globals.defaultAppFont(14)
        self.view.bringSubview(toFront: self.bottomViewAddress)
        self.bottomViewAddress.backgroundColor = UIColor.white
        if self.isPickUpAddress {
            self.bottomViewAddress.pickupAddress.alpha = 0.0
            self.bottomViewAddress.pickupAddress.text = ""
        }
    }
    
    self.showBottomDone(enable: false)
    self.btnDoneBottom.setTitle("Select \(strTypeOfAddress!)", for: UIControlState())
    self.btnDoneBottom.titleLabel?.font = Globals.defaultAppFontWithBold(16)
     self.btnDoneBottom.isEnabled = false
    mapViewSelectAddress.delegate = self
    mapViewSelectAddress.settings.myLocationButton = true
    mapViewSelectAddress.isMyLocationEnabled = true
    mapViewSelectAddress.settings.zoomGestures = true
    resultsViewController = GMSAutocompleteResultsViewController()
    resultsViewController?.delegate = self
    searchController = UISearchController(searchResultsController: resultsViewController)
    searchController?.searchResultsUpdater = resultsViewController
   self.searchBarView = UIView(frame: CGRect(x: 15, y: 5 ,width: self.view.frame.size.width - 30, height: 44.0))
     self.searchBarView.addSubview((searchController?.searchBar)!)
    let imageBG = UIImageView(frame: CGRect(x: 0, y: 0 ,width:  self.searchBarView.frame.size.width , height: 44.0))
    imageBG.image = UIImage(named: "backPattrenGreen")
    
    self.view.addSubview( self.searchBarView)
    searchController?.searchBar.sizeToFit()
    searchController?.searchBar.placeholder = "Search Places"
      searchController?.delegate = self
    searchController?.searchBar.backgroundColor =  UIColor(patternImage: UIImage(named:"searchBGPattern")!)
    searchController?.searchBar.setImage(UIImage(named: "address_Black"), for: .search, state: UIControlState())
    searchController?.searchBar.tintColor = UIColor.appThemeColor()
    searchController?.view.backgroundColor = UIColor.lightGray
    searchController?.searchBar.frame = CGRect(x: 0, y: 0, width:  self.searchBarView.frame.width, height: 44.0)
    Globals.layoutViewFor( self.searchBarView, color: UIColor.appRideLater_GreenColor(), width: 1.0, cornerRadius: 3.0)
    var textField : UITextField!
    let viewSearchBar = searchController?.searchBar.subviews[0]
    for searchSubViews in viewSearchBar!.subviews {
        if searchSubViews.isKind(of: UITextField.self){
          textField = searchSubViews as! UITextField
            textField.textColor = UIColor.black
            textField.font = Globals.defaultAppFont(15)
            textField.setValue(UIColor.darkGray, forKeyPath: "_placeholderLabel.textColor")
            ((((textField.value(forKey: "textInputTraits"))! as Any) as Any) as AnyObject).setValue(UIColor.darkGray, forKeyPath: "insertionPointColor")
            textField.backgroundColor = UIColor.clear
            textField.placeholder  = "Search Places"
            textField.frame.size.height = 44
            if self.isPickUpAddress == true {
             textField.text = self.locationManagerObjct.strCurrentLocation
            }else if dicDetails != nil && dicDetails.count != 0 {
                if let addres = dicDetails["address"]{
                    textField.text = String(describing: addres)
                }
            }
        }
        else if !searchSubViews.isKind(of: UIView.self) {
                 searchSubViews.backgroundColor = UIColor.appRideLater_GreenColor()
        }
    }

    let uisearchBG = viewSearchBar?.subviews[0]
    uisearchBG?.removeFromSuperview()
    viewSearchBar?.addSubview(imageBG)
    viewSearchBar?.insertSubview(imageBG, belowSubview: textField)
    viewSearchBar?.bringSubview(toFront: textField)
    searchController?.hidesNavigationBarDuringPresentation = false
    
     self.definesPresentationContext = false
    self.btnHomeAddress.titleLabel?.numberOfLines = 3;
    self.btnHomeAddress.titleLabel?.font = Globals.defaultAppFont(13)
    self.btnWorkAddress.titleLabel?.font = Globals.defaultAppFont(13)
   
    self.btnHomeAddress.setAttributedTitle(self.customizeAttributedStringWithTitles("Home Address", secondtTitle: ""), for: UIControlState())
    self.btnWorkAddress.setAttributedTitle(self.customizeAttributedStringWithTitles("Work Address", secondtTitle: ""), for: UIControlState())

//        if let strHomeAddress = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.HomeAddress).address") as? String{
//            if strHomeAddress.isEmpty{
//                self.btnHomeAddress.setAttributedTitle(self.customizeAttributedStringWithTitles("Home", secondtTitle: "Add home address"), for: UIControlState())
//            }
//            else{
//            }
//        }else {
//            self.btnHomeAddress.setAttributedTitle(self.customizeAttributedStringWithTitles("Home", secondtTitle: "Add home"), for: UIControlState())
//
//        }
//        if let strWorkAddress = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.WorkAddress).address") as? String{
//            if strWorkAddress.isEmpty{
//                self.btnWorkAddress.setAttributedTitle(self.customizeAttributedStringWithTitles("Work", secondtTitle: "Add work address"), for: UIControlState())
//            }
//            else{
//            }
//        }else {
//            self.btnWorkAddress.setAttributedTitle(self.customizeAttributedStringWithTitles("Work", secondtTitle: "Add work"), for: UIControlState())
//
//        }        // set home address view with tag
    if let viewHome = self.view.viewWithTag(60) {
       viewHome.layer.borderColor = UIColor.lightGray.cgColor
        viewHome.layer.borderWidth = 1.0
        if let editButton = viewHome.viewWithTag(8844) as? UIButton {
            editButton.titleLabel?.font = Globals.defaultAppFont(14)
            editButton.setTitleColor(UIColor.appThemeColor(), for: .normal)
        }
    }
    if let viewWork = self.view.viewWithTag(61) {
        viewWork.layer.borderColor = UIColor.lightGray.cgColor
        viewWork.layer.borderWidth = 1.0
        if let editButton = viewWork.viewWithTag(8844) as? UIButton {
            editButton.titleLabel?.font = Globals.defaultAppFont(14)
            editButton.setTitleColor(UIColor.appThemeColor(), for: .normal)
        }
    }
    
    mapViewSelectAddress.delegate = self

//        if self.modalAdress.isEditingMode == true{
////            if self.modelAddress.strPickupType == pickupType.Address.rawValue
////            if self.modelAddress.strDropOffType == DropOffType.Address.rawValue
//            self.finishSelectingDropOffAddress!(self.modalAdress.dicDetails)
//
//        }
//        // set home address view with tag
//    else {
    // Do any additional setup after loading the view.
    if dicDetails != nil && dicDetails.count != 0 {
        dicDetails = dicDetails.formatDictionaryForNullValues(dicDetails)
        var lati = dicDetails["latitude"]
        var longitude = dicDetails["longitude"]
        if lati is String && longitude is String{
            lati = "\(lati!)".toDouble() as Any?
            longitude = "\(longitude!)".toDouble() as Any?
        }
        if lati != nil && longitude != nil {
            DispatchQueue.main.async(execute: { [weak self] () -> Void in
                self?.mapViewSelectAddress.clear()
                let camera = GMSCameraPosition.camera(withLatitude: lati as! Double, longitude:longitude as! Double, zoom: 16.0)
                self?.mapViewSelectAddress.animate(to: camera)
                })
        }
    }else {
        DispatchQueue.main.async(execute: { [weak self] () -> Void in
            self?.mapViewSelectAddress.clear()
            let camera = GMSCameraPosition.camera(withLatitude: (LocationManager.sharedInstance.currentLocation!.coordinate.latitude), longitude:(LocationManager.sharedInstance.currentLocation!.coordinate.longitude) , zoom: 16.0)
            self?.mapViewSelectAddress.animate(to: camera)
           // self?.markerPinImage.center = CGPoint(x: (self?.markerPinImage.center.x)!, y: (self?.markerPinImage.center.y)! - 55)
            })
    }
  //  }

}

@IBAction func btnDoneClickedAction(_ sender: UIButton) {
    if viewFrom == "onDemand"{
        if self.dicSelectedAdddress.count != 0 {
            self.finishSelectingDropOffAddress!(self.dicSelectedAdddress)
        }
        self.navigationController?.dismiss(animated: true, completion:nil)
    }
    else {
        if self.dicSelectedAdddress.count != 0 {
            self.finishSelectingAddress!(self.dicSelectedAdddress,self.tagBtnAddressLast , true)
        }
        self.navigationController?.popViewController(animated: true)
    }
}

func showBottomDone(enable:Bool){
    if  enable == false  {
                    self.btnDoneBottom.alpha = 0.60
                }else {
                    self.btnDoneBottom.alpha = 1.0
                }
    }

func getHomeAndOfficeDetails()  {
    if appDelegate.networkAvialable == true {
        var dicParameters = [String:Any]()

        
        if Constants.isBookerLogin == true
        {
            dicParameters["user_id"] = "\(appDelegate.activeUserDetailsForBooker["id"]!)"
            dicParameters["is_booker"] = "yes"
        }
        else
        {
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            dicParameters["user_id"] = user_ID
            dicParameters["session"] = session
            dicParameters["is_booker"] = "no"

        }
        dicParameters["status"] = "get" as Any
        appDelegate.showProgressWithText("Fetching home and work addresses...")
        weak var weakSelf = self
        ServiceManager.sharedInstance.dataTaskWithPostRequest("getUpdateAccountAddress", dicParameters: dicParameters, successBlock: {[weak self] (dicData) -> Void in
            appDelegate.hideProgress()
            if weakSelf == nil {
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                if let dict = dicData["record"] as? [String:Any]{
                    self?.dicAddressDetails = dict
                    
                    if let homeAdd = dict["home_address"] as? String
                    {
                        self?.btnHomeAddress.setAttributedTitle(self?.customizeAttributedStringWithTitles("Home Address", secondtTitle: homeAdd), for: UIControlState())
                        // self?.btnHomeAddress.setAttributedTitle(self?.customizeAttributedStringWithTitles("Home", secondtTitle: "Add Home"), forState: .Normal)
                    }
                    if let workAdd = dict["work_address"] as? String
                    {
                        self?.btnWorkAddress.setAttributedTitle(self?.customizeAttributedStringWithTitles("Work Address", secondtTitle: workAdd), for: UIControlState())
                        // self?.btnWorkAddress.setAttributedTitle(self?.customizeAttributedStringWithTitles("Work", secondtTitle: "Add Work"), forState: .Normal)
                    }
                }
            })
            
            })
        { (error) -> () in
            appDelegate.hideProgress()
            let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
        }
        
    }else {
        let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
        alert.show()
    }
}

func setHomeWorkAddress(_ address:[String:Any], isHomeAdd:Bool)  {
    if appDelegate.networkAvialable == true {
        var dicParameters = [String:Any]()
       
        
        if Constants.isBookerLogin == true
        {
            dicParameters["user_id"] = "\(appDelegate.activeUserDetailsForBooker["id"]!)"
            dicParameters["is_booker"] = "yes"
        }
        else
        {
        let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
        let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
        dicParameters["user_id"] = user_ID
        dicParameters["session"] = session
            dicParameters["is_booker"] = "no"

        }
        
        dicParameters["status"] = "update"
        
        if isHomeAdd == true{
            
            dicParameters["home_address"] = address["address"] as! String as Any?
            dicParameters["home_lat_long"] = String(format: "%@,%@",  "\(address["latitude"]!)",  "\(address["longitude"]!)") as Any?
        }
        else {
            dicParameters["work_address"] = address["address"] as! String as Any?
            dicParameters["work_lat_long"] = String(format: "%@,%@",  "\(address["latitude"]!)",  "\(address["longitude"]!)") as Any?
        }
        weak var weakSelf = self
        appDelegate.showProgressWithText("Updating address.....")
        
        ServiceManager.sharedInstance.dataTaskWithPostRequest("getUpdateAccountAddress", dicParameters: dicParameters, successBlock: { (dicData) -> Void in
            appDelegate.hideProgress()
            if weakSelf == nil {
                return
            }
            DispatchQueue.main.async(execute: {[weak self] () -> Void in
                if let dict = dicData["record"] as? [String:Any]{
                    appDelegate.hideProgress()
                    self?.dicAddressDetails = dict
                    if isHomeAdd == true{
                        self?.btnHomeAddress.setAttributedTitle(self?.customizeAttributedStringWithTitles("Home", secondtTitle: address["address"] as! String), for: UIControlState())
                    }
                    else {
                        self?.btnWorkAddress.setAttributedTitle(self?.customizeAttributedStringWithTitles("Work", secondtTitle: address["address"] as! String), for: UIControlState())
                    }
                }
                })
            
            })
        { (error) -> () in
            let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
        }
        
    }else {
        let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
        alert.show()
    }
}


override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    Globals.sharedInstance.delegateBack = self
    if let imagePin = self.view.viewWithTag(66778899) {
           self.view.bringSubview(toFront: imagePin)
    }

}
override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
 //   self.updateSearchbarWith(height: 5)
}

override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
}
func customizeAttributedStringWithTitles(_ firstTitle:String, secondtTitle:String) -> NSAttributedString{
    let   strHomeNew = " \(firstTitle)\n " + "\(secondtTitle)"
    //let thirdWord   = "Third Word"
    let attributedText = NSMutableAttributedString(string:strHomeNew)
    let attrs      = [NSFontAttributeName: Globals.defaultAppFont(12), NSForegroundColorAttributeName: UIColor.darkGray]
    let attrsName      = [NSFontAttributeName: Globals.defaultAppFont(11), NSForegroundColorAttributeName: UIColor.black]
    let range = NSString(string: strHomeNew).range(of: secondtTitle)
    attributedText.addAttributes(attrs, range: range)
    let rangeName = NSString(string: strHomeNew).range(of: firstTitle)
    attributedText.addAttributes(attrsName, range:rangeName)
    return attributedText
}
// Present the Autocomplete view controller when the button is pressed.


@IBAction func autocompleteClicked(_ sender: Any) {
    
    // self.presentViewController(autocompleteController, animated: true, completion: nil)
}
// MARK: CustomSearchControllerDelegate functions


@IBAction func btnEditHomeAndWorkAddressAction(_ sender: UIButton) {
    //     let viewSearchBar = searchController?.searchBar.subviews[0]
    //    for searchTextField in (viewSearchBar?.subviews)!{
    //        if searchTextField.isKindOfClass(UITextField){
    //            let txtField = searchTextField as! UITextField
    //            txtField.becomeFirstResponder()
    //            break
    //        }
    //    }
    switch sender.superview!.tag {
    case 60 : // Home Address
        self.updateHomeAddress()
        self.isHomeAddress = true
        self.isWorkAddress = false
        break
    case 61 : // work Address
        self.updateWorkAddress()
        self.isHomeAddress = false
        self.isWorkAddress = true
    default :
        break
    }
}


func updateHomeAddress(){

    let selectAddrsVC = appDelegate.storybaord.instantiateViewController(withIdentifier: "PASelectAddressViewController") as! PASelectAddressViewController
    selectAddrsVC.isPickUpAddress = true
    selectAddrsVC.strTypeOfAddress = "Home Address"
    selectAddrsVC.finishSelectingDropOffAddress = { [weak self](dicAddress : [String:Any], tag:Int) -> () in
        if dicAddress.count != 0 {
            self?.setHomeWorkAddress(dicAddress, isHomeAdd: true)
        }
    }
    self.navigationController?.pushViewController(selectAddrsVC, animated: true)
}

func updateWorkAddress(){

    let selectAddrsVC = appDelegate.storybaord.instantiateViewController(withIdentifier: "PASelectAddressViewController") as! PASelectAddressViewController
    selectAddrsVC.isPickUpAddress = true
    selectAddrsVC.strTypeOfAddress = "Work Address"
    selectAddrsVC.finishSelectingDropOffAddress = { [weak self](dicAddress : [String:Any], tag:Int) -> () in
        if dicAddress.count != 0 {
            self?.setHomeWorkAddress(dicAddress, isHomeAdd: false)
        }
    }
    self.navigationController?.pushViewController(selectAddrsVC, animated: true)
    
}



@IBAction func btnSetHomeOrWorkAddressAction(_ sender: UIButton) {
    switch sender.superview!.tag {
    case 60 : // home address
        if let dicHomeAddress = self.dicAddressDetails as? [String:String]{
            var homeAdd = [String:String]()
            
            if let home_add = dicHomeAddress["home_address"]{
            homeAdd["address"] = "\(home_add)"
            homeAdd["placeID"] = ""
            let arr:[String] = ((dicHomeAddress["home_lat_long"])!.components(separatedBy: ","))
            homeAdd["latitude"] = "\(arr.first!)"
            homeAdd["longitude"] = "\(arr.last!)"
            if !"\(home_add)".isEmpty{
                let pickUpLocation = CLLocation(latitude: (homeAdd["latitude"] as AnyObject).doubleValue, longitude: (homeAdd["longitude"]! as AnyObject).doubleValue)

                self.MapStopScrollingAndGetAddressOfCurrentLocation(pickUpLocation, homeWorkAddress: true)
            }
            }
            
        }else {
            self.btnEditHomeAndWorkAddressAction(sender)
        }
        break
    case 61 : // work Address
        if let dicWorkAddress = self.dicAddressDetails{
            var workAdd = [String:Any]()
            if let work_add = dicWorkAddress["work_address"]{
            workAdd["address"] = "\(work_add)"
            workAdd["placeID"] = "" as Any?
            let arr:[String] = (((dicWorkAddress["work_lat_long"] as Any) as AnyObject).components(separatedBy: ","))
            workAdd["latitude"] =  "\(arr.first!)" as Any
            workAdd["longitude"] = "\(arr.last!)" as Any
                
            if !"\(work_add)".isEmpty{
                let pickUpLocation = CLLocation(latitude: (workAdd["latitude"]! as AnyObject).doubleValue , longitude: (workAdd["longitude"]! as AnyObject).doubleValue)
                self.MapStopScrollingAndGetAddressOfCurrentLocation(pickUpLocation, homeWorkAddress: true)
                }
            }
        }else {
            self.btnEditHomeAndWorkAddressAction(sender)
        }
        
        break
        
    default :
        break
        
        
    }
}
/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */
func backButtonActionMethod(_ sender: UIButton) {
    if viewFrom == "onDemand"{
        self.dismiss(animated: true, completion: nil)
    }
    else
    {
        let dict = [String:Any]()
        self.finishSelectingAddress!(dict,self.tagBtnAddressLast , false)
        self.navigationController?.popViewController(animated: true)
    }
    
}
func updateSearchbarWith(height:CGFloat)
{
  //  DispatchQueue.main.async {[weak self] () -> Void in
        UIView.animate(withDuration: 1.00, animations: { [weak self]() -> Void in
          self?.searchBarView.frame.origin.y = height
            self?.view.layoutIfNeeded()
            self?.view.updateConstraintsIfNeeded()
        })
    //}
}
}

// MARK: - ======== GMSAutocompleteResultsViewControllerDelegate ======

extension SearchPlacesAddessViewController : UISearchControllerDelegate {
func didDismissSearchController(_ searchController: UISearchController) {
    //   self.updateSearchbarWith(height: 5.0)
    self.searchBarView.frame.origin.y = 5
    self.view.layoutIfNeeded()
    self.view.updateConstraintsIfNeeded()
}

}
// Handle the user's selection.
extension SearchPlacesAddessViewController: GMSAutocompleteResultsViewControllerDelegate {
/**
 * Called when a non-retryable error occurred when retrieving autocomplete predictions or place
 * details. A non-retryable error is defined as one that is unlikely to be fixed by immediately
 * retrying the operation.
 * <p>
 * Only the following values of |GMSPlacesErrorCode| are retryable:
 * <ul>
 * <li>kGMSPlacesNetworkError
 * <li>kGMSPlacesServerError
 * <li>kGMSPlacesInternalError
 * </ul>
 * All other error codes are non-retryable.
 * @param resultsController The |GMSAutocompleteResultsViewController| that generated the event.
 * @param error The |NSError| that was returned.
 */

public func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
    //self.updateSearchbarWith(height: 5.0)
}
func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didSelect prediction: GMSAutocompletePrediction) -> Bool {
    return true
}
func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {

}

func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
    self.updateSearchbarWith(height:  self.isStopsAddress == true ? -40 : -90)
}
func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                       didAutocompleteWith place: GMSPlace)
{
    
    self.updateSearchbarWith(height: 5.0)
    searchController?.isActive = false
    // Do something with the selected place.
    var dicDropOffAddress = [String:Any]()
    dicDropOffAddress["address"] = place.name as String as Any?
    dicDropOffAddress["placeID"] = place.placeID as String as Any?
    dicDropOffAddress["latitude"] = place.coordinate.latitude as Double as Any?
    dicDropOffAddress["longitude"] = place.coordinate.longitude as Double as Any?
    self.isPickUpAddress = true
    //  self.mapViewSelectAddress.clear()
    let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude:place.coordinate.longitude , zoom: 12.0)
    self.mapViewSelectAddress.animate(to: camera)
    self.btnDoneBottom.isEnabled = false
    self.showBottomDone(enable: false)
}


//// MARK: -============ GMSMapViewDelegate===========

func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    self.map_CentreCoordinate = self.mapViewSelectAddress.camera.target
    let newLocation = CLLocation(latitude: (self.map_CentreCoordinate?.latitude)! , longitude: (self.map_CentreCoordinate?.longitude)!)
    if self.isPickUpAddress == true {
        self.MapStopScrollingAndGetAddressOfCurrentLocation(newLocation, homeWorkAddress: false)
    }
    if self.isPickUpAddress == false {
        self.isPickUpAddress = true
    }
}

func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
    self.btnDoneBottom.isEnabled = false
    self.showBottomDone(enable: false)
}

func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    return false
}

func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
    mapView.selectedMarker = nil
    return false
}
// MARK: -============ get address from selected loaction from map ==========

func MapStopScrollingAndGetAddressOfCurrentLocation(_ newLocation:CLLocation, homeWorkAddress:Bool){
    GoogleMapAPIServiceManager.sharedInstance.getLocationAddressUsingLatitude(newLocation.coordinate.latitude, longitude: newLocation.coordinate.longitude, withComplitionHandler: { [weak self](dicRecievedJSON) -> () in
        appDelegate.hideProgress()
        if dicRecievedJSON["status"] as! String == "OK"{
            if (dicRecievedJSON ["results"] as! [Any]).count  != 0 {
                DispatchQueue.main.async(execute: { () -> Void in
                    let viewSearchBar = self?.searchController?.searchBar.subviews[0]
                    if viewSearchBar != nil {
                        for searchTextField in (viewSearchBar?.subviews)!{
                            if searchTextField.isKind(of: UITextField.self){
                                let txtField = searchTextField as! UITextField
                                txtField.text = ( (dicRecievedJSON["results"]! as! [[String:Any]])[0] as! [String:Any])["formatted_address"] as? String
                                break
                            }
                        }
                    }
                })
                self?.dicSelectedAdddress = (self?.formatedAddressCompnentsSwift((dicRecievedJSON ["results"] as! [Any]).first as! [String:Any]))!
                appDelegate.hideProgress()
                self?.btnDoneBottom.isEnabled = true
                // self?.btnDoneBottom.alpha =  1
                self?.btnDoneBottom.setTitle("Select \(self!.strTypeOfAddress!)", for: UIControlState())
                self?.showBottomDone(enable: true)
                //                dispatch_async(dispatch_get_main_queue()) {[weak self] () -> Void in
                //
                //                }
                if homeWorkAddress == true{
                    if self?.viewFrom == "onDemand"{
                        if self?.dicSelectedAdddress.count != 0 {
                            self?.finishSelectingDropOffAddress!((self?.dicSelectedAdddress)!)
                        }
                        self?.navigationController?.dismiss(animated: true, completion:nil)
                    }
                    else {
                        if self?.dicSelectedAdddress.count != 0 {
                            self?.finishSelectingAddress!((self?.dicSelectedAdddress)!,(self?.tagBtnAddressLast)!, true)
                        }
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
                return
              }
            return
         }
            appDelegate.hideProgress()
           // Globals.ToastAlertWithString("Please try again..")
        })
    { (error) -> () in
        appDelegate.hideProgress()
        //JLToast.makeText("Please check your network connection", duration: JLToastDelay.ShortDelay).show()
    }
}
func formatedAddressCompnentsSwift(_ dicAddress:[String:Any]) -> [String:Any]{
    var dictAddressComp = [String:Any]()
    dictAddressComp["address"] =  dicAddress ["formatted_address"] as? String as Any?
    let dicGeometry = (dicAddress["geometry"]! as! [String:Any])["location"]! as! [String:Any]
    dictAddressComp["placeID"] = dicAddress["place_id"]! as! String as Any?
    dictAddressComp["latitude"] = dicGeometry["lat"] as! Double as Any?
    dictAddressComp["longitude"] = dicGeometry["lng"] as! Double as Any?
    var address: [String] = [String]()
    let arrAddresComponents = (dicAddress["address_components"] as? [[String : Any]])
    for var add_comp in arrAddresComponents!{
        if let _ = add_comp["types"] {
            let types =  (add_comp["types"] as! [String]).first!
            if types.characters.count > 0 {
                if types.compare("street_number")  {
                    dictAddressComp["street_number"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("route")  {
                    dictAddressComp["route"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("neighborhood")  {
                    dictAddressComp["neighborhood"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("sublocality_level_1")  {
                    dictAddressComp["sublocality_level_1"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("sublocality_level_2")  {
                    dictAddressComp["sublocality_level_2"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("sublocality_level_3")  {
                    dictAddressComp["sublocality_level_3"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("locality")  {
                    dictAddressComp["locality"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("administrative_area_level_1")  {
                    dictAddressComp["administrative_area_level_1"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("country")  {
                    dictAddressComp["country"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }else  if types.compare("postal_code")  {
                    dictAddressComp["postal_code"] = add_comp["long_name"]
                    if address.contains(add_comp["long_name"] as! String) {
                        address.append(add_comp["long_name"] as! String)
                    }
                }
            }
        }
    }
    return dictAddressComp
}
}




