//
//  PAReservationsDetailsVC.swift
//  PassangerApp
//
//  Created by Netquall on 2/5/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader
import GoogleMaps
enum driverDetailsTag :Int {
    case driverDetails = 150
    case bookingStatus = 151
    case share = 152
    
}

class PAReservationsDetailsVC: BaseViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate,BackButtonActionProtocol {
    
    // MARK: - ============ Property decalaration ==========
    
    @IBOutlet var viewFareDisTimeDetails: UIView!
    
    @IBOutlet var viewPickUpDropOffDetails: UIView!
    
    @IBOutlet var viewDateVehicleDetails: UIView!
    
    @IBOutlet var mapViewDetails: GMSMapView!
    
    @IBOutlet weak var floatingRatesStar: CosmosView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet var viewDriverBookingShareBtns: UIView!
    @IBOutlet var viewTextNotesEditable: UIView!
    @IBOutlet var viewStopsDetails: UIView!
    @IBOutlet weak var txtViewNotesDriver: UITextView!
    
    @IBOutlet weak var ViewEditCommentsAndRating: UIView!
    
    @IBOutlet weak var viewEditStarRating: CosmosView!
    @IBOutlet weak var txtViewEditableComments: UITextView!
    
    @IBOutlet var toolBarTextView: UIToolbar!
    @IBOutlet var viewRatingOfDriver: UIView!
    @IBOutlet weak var tblReservationDetails: UITableView!
    
    
    @IBOutlet var viewPopUpStatus: UIView!
    @IBOutlet var viewEditCancelView: UIView!

    var mapBounds : GMSCoordinateBounds!
    var isPastReservation = false
    lazy var dicDataSource = [String:Any]()
    var iPadScreen = false
    lazy var arrStops = [[String:Any]]()
    // MARK: - ====View Life cycle =======
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let stopsArr = self.dicDataSource["stops"] as? [[String:Any]]{
            self.arrStops = stopsArr
        }
        // self.arrStops = self.dicDataSource["stops"] as! [String]
        self.tblReservationDetails.reloadData()
        //  mapViewDetails.settings.myLocationButton = true
        //  mapViewDetails.myLocationEnabled = true
        mapViewDetails.settings.zoomGestures = true
        self.tblReservationDetails.contentInset = UIEdgeInsetsMake(5, 0, 0, 0)
        self.txtViewNotesDriver.inputAccessoryView = self.toolBarTextView
        // Do any additional setup after loading the view.
        self.floatingRatesStar.isUserInteractionEnabled = false
        self.tblReservationDetails.backgroundColor = UIColor.white
        self.ViewEditCommentsAndRating.frame.origin.y = 5000000.0
        self.ViewEditCommentsAndRating.isHidden = true
        self.txtViewEditableComments.inputAccessoryView = self.toolBarTextView
        self.txtViewEditableComments.delegate = self

        // var mapBounds : GMSCoordinateBounds!
            if let lati = self.dicDataSource["pickup_latitude"]  as? String {
                
                var pickupLocation:CLLocation?
                if lati.count != 0 && lati != "0.0"{
                    let PickupLatitude = CDouble(self.dicDataSource["pickup_latitude"] as! String)
                    let PickupLongi = CDouble(self.dicDataSource["pickup_longitude"] as! String)
                    pickupLocation = CLLocation(latitude:PickupLatitude! , longitude: PickupLongi!)
                }
                else {
                    if let address = self.dicDataSource["pickup"]  as? String {
                        pickupLocation = getAddressLatLongiFromAddressString(address, isPickup: true)
                    }
                }
                
                if pickupLocation != nil{
                    self.mapBounds = GMSCoordinateBounds(coordinate: pickupLocation!.coordinate, coordinate: pickupLocation!.coordinate)
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2DMake(pickupLocation!.coordinate.latitude, pickupLocation!.coordinate.longitude)
                    marker.icon = UIImage(named: "pickupPin")
                    marker.map = self.mapViewDetails
                }
            }
            if let lati =  self.dicDataSource["dropoff_latitude"]  as? String{
                
                var dropLocation:CLLocation?
                
                if lati.count != 0  && lati != "0.0" {
                    // String(format: "%@", locale: lati as! NSLocale)
                    let dropLatitude = CDouble(self.dicDataSource["dropoff_latitude"] as! String)
                    let dropLongi = CDouble(self.dicDataSource["dropoff_longitude"] as! String)
                    dropLocation = CLLocation(latitude:dropLatitude! , longitude: dropLongi!)
                }
                else {
                    if let address = self.dicDataSource["dropoff"]  as? String {
                        dropLocation =  getAddressLatLongiFromAddressString(address, isPickup: false)
                    }
                }
                
                if dropLocation != nil{
                    let boundsSource = GMSCoordinateBounds(coordinate:dropLocation!.coordinate, coordinate: dropLocation!.coordinate)
                    self.mapBounds = self.mapBounds.includingBounds(boundsSource)
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2DMake(dropLocation!.coordinate.latitude, dropLocation!.coordinate.longitude)
                    marker.icon = UIImage(named: "dropOffPin")
                    marker.map = self.mapViewDetails
                }
            }
        
        
        if self.arrStops.count > 0 {
            for dicStops in self.arrStops{
                if let lati = dicStops["pickup_latitude"]  as? String {
                    var pickupLocation:CLLocation?
                    if lati.count != 0 && lati != "0.0"{
                        let PickupLatitude = CDouble(dicStops["pickup_latitude"] as! String)
                        let PickupLongi = CDouble(dicStops["pickup_longitude"] as! String)
                        pickupLocation = CLLocation(latitude:PickupLatitude! , longitude: PickupLongi!)
                    }
                    
                    if pickupLocation != nil{
                        self.mapBounds = GMSCoordinateBounds(coordinate: pickupLocation!.coordinate, coordinate: pickupLocation!.coordinate)
                        let marker = GMSMarker()
                        marker.position = CLLocationCoordinate2DMake(pickupLocation!.coordinate.latitude, pickupLocation!.coordinate.longitude)
                        marker.icon = UIImage(named: "iconStops")
                        marker.map = self.mapViewDetails
                    }
                }
            }
            
        }
            weak var weakSelf = self
            UIView.animate(withDuration: 0.50, animations: { () -> Void in
                DispatchQueue.main.async(execute: {  () -> Void in
                    weakSelf?.mapViewDetails.animate(to: weakSelf!.mapViewDetails.camera(for: weakSelf!.mapBounds, insets: UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0))!)
                })
            })
        
        if let strStatus = self.dicDataSource["status"] as? String{
            if strStatus.compare("drop off") == true{
                viewFareDisTimeDetails.isHidden = true
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let strbookingID = String(describing: self.dicDataSource["conf_id"]!).replacingOccurrences(of: "-", with: "")

        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Booking No: " + "\(strbookingID)", imageName: "backArrow")
        Globals.sharedInstance.delegateBack = self
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       // Globals.sharedInstance.delegateBack = nil
    }
    
    func backButtonActionMethod(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - ============text view delegate  ==========
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if self.txtViewEditableComments == textView {
            if self.txtViewEditableComments.text == "Add Comments" {
                self.txtViewEditableComments.text = ""
            }
        }
        return true
    }
    
    // MARK: - ============ Get Address from Latitude and Longitude ==========
    
    func getAddressLatLongiFromAddressString(_ strAddress: String, isPickup:Bool) -> CLLocation?
    {
        if strAddress.count == 0 {
            return nil
        }
        //var latitude: UnsafeMutablePointer
        //var longitude: UnsafeMutablePointer
        var latitude:Double = 0
        var longitude:Double = 0/// Swift 8bit int
        // grab the data and cast it to a Swift Int8
        
        //let esc_addr: String = strAddress.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let esc_addr: String = strAddress.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
        
        let req = Globals.sharedInstance.getFullApiAddressForAddressString(esc_addr)
        var result : String!
        do {
            result = try String(contentsOf: URL(string: req)!, encoding: String.Encoding.utf8)
        }catch {
            print("json Error-::\(error)")
            
        }
        
        if result  != "" && result  != nil{
            let scanner: Scanner = Scanner(string: result)
            
            if scanner.scanUpTo("\"lat\" :", into: nil) && scanner.scanString("\"lat\" :", into: nil) {
                scanner.scanDouble(&latitude)
                if scanner.scanUpTo("\"lng\" :", into: nil) && scanner.scanString("\"lng\" :", into: nil) {
                    scanner.scanDouble(&longitude)
                }
            }
        }
        
        let location = CLLocation(latitude: latitude, longitude: longitude)
        
        return location
    }
    
    
    
    // MARK: - ============table View Data Source  ==========
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if  self.arrStops.count == 0{
            return 6
        }else {
            return 6
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.arrStops.count == 0{
            return 0
        }else {
            if section == 12 {
                return self.arrStops.count
            }else {
                return 0
            }
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell : UITableViewCell?
        
        let  cellIndetifier = "cellRideLaterWithStops"
        //cellCarInfo
        cell  = self.tblReservationDetails.dequeueReusableCell(withIdentifier: cellIndetifier)!
        var lblText : UILabel!
        var viewOuter : UIView!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
            lblText = cell!.viewWithTag(63) as! UILabel
            viewOuter = cell!.viewWithTag(62)! as UIView
            
        }else {
            lblText = cell!.viewWithTag(63) as! UILabel
            viewOuter = cell!.viewWithTag(62)! as UIView
            
        }
        viewOuter.layer.borderColor = UIColor.lightGray.cgColor
        viewOuter.layer.borderWidth = 1.0
        let dicStops = self.arrStops[indexPath.row]
        lblText.text = String(describing: dicStops["location"])
        lblText.numberOfLines = 4
        lblText.font = Globals.defaultAppFont(14)
        lblText.textColor = UIColor.black
        return cell!
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch (section){
            
        case 0:

            return DeviceType.IS_IPAD ? 100 : 80
        case 1:
             return  self.view.frame.size.height - (DeviceType.IS_IPAD ?  550 : 300)
        case 2:
            return DeviceType.IS_IPAD ? 171 : 114
        case 3:
            if let feedback = self.dicDataSource["passenger_comments"] as? [String:Any]{
                if let comments = feedback["passenger_comment"] as? String
                {
                    let constraint: CGSize = CGSize(width: self.txtViewNotesDriver.frame.width, height: 20000.0)
                    let boundingBox: CGSize = comments.boundingRect(with: constraint, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName:Globals.defaultAppFontItalic(13)], context: nil).size
                    let size = CGSize(width: ceil(boundingBox.width), height: ceil(boundingBox.height))
                    
                    let height = size.height < 80 ? 90 : (size.height + 20)
                    self.viewTextNotesEditable.translatesAutoresizingMaskIntoConstraints = true
                    self.viewTextNotesEditable.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: height)

                    return height
                }
            }
            return DeviceType.IS_IPAD ? 150 : 80
            
        case 4:
            return 70
         case 5:
            return DeviceType.IS_IPAD ? 130 : 80
        default :
            return 70
        }
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        switch(section){
        case 0:
            self.viewDriverBookingShareBtns.translatesAutoresizingMaskIntoConstraints = true
            self.viewDriverBookingShareBtns.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: DeviceType.IS_IPAD ? 100 : 80)
            return self.viewDriverBookingShareBtns
        case 1:
            self.mapViewDetails.translatesAutoresizingMaskIntoConstraints = true
            let height = self.view.frame.size.height - (DeviceType.IS_IPAD ?  550 : 300)

            self.mapViewDetails.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: height)
            return self.mapViewDetails
            
        case 2:
            let lblPickUp = self.viewPickUpDropOffDetails.viewWithTag(20) as! UILabel
            
            lblPickUp.text = (self.dicDataSource["pickup"] as! String)
            
            let lblDropOff = self.viewPickUpDropOffDetails.viewWithTag(21) as! UILabel ;
            
            if let strDrop = self.dicDataSource["dropoff"] as? String {
                lblDropOff.text = strDrop
            }else {
                lblDropOff.text = "N/A"
            }
            
            self.viewPickUpDropOffDetails.translatesAutoresizingMaskIntoConstraints = true
            self.viewPickUpDropOffDetails.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 114)
            
            return  self.viewPickUpDropOffDetails
        case 3:
            var nameDriver = ""
            if let firstName = self.dicDataSource["first_name"] as? String {
                nameDriver = firstName
            }
            if let lastName = self.dicDataSource["last_name"] as? String {
                nameDriver = nameDriver + " " + lastName
            }
            if nameDriver.count>1 {
                self.lblDriverName.text = "Driver Name: " + nameDriver
            }
            else {
                self.lblDriverName.text = "Driver Name: N/A"
            }
            if let feedback = self.dicDataSource["passenger_comments"] as? [String:Any]{
                if let comments = feedback["passenger_comment"] as? String
                {
                    self.txtViewNotesDriver.text = comments == "" ? "No feedback shared for this job.": comments
                }
            }
            else {
                self.txtViewNotesDriver.text = "No feedback shared for this job."
            }
             return self.viewTextNotesEditable
            
        case 4:
            if let feedback = self.dicDataSource["passenger_comments"] as? [String:Any]{
                if let rating = feedback["passenger_rating"] as? String{
                    self.floatingRatesStar.rating = rating.toDouble()!
                }
                else {
                    self.floatingRatesStar.rating = 0
                }
            }
            else {
                self.floatingRatesStar.rating = 0
            }
            self.floatingRatesStar.totalStars = 5
            self.floatingRatesStar.didFinishTouchingCosmos = { [weak self](rating:Double) in
                self?.floatRatingViewAction(rating)
            }
            self.viewRatingOfDriver.translatesAutoresizingMaskIntoConstraints = true
            self.viewRatingOfDriver.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 70)
            
            return self.viewRatingOfDriver
            
        case 5:
            if self.isPastReservation == true {
                self.setupFareDisTimeViewDetais()
                return  self.viewFareDisTimeDetails
            }
            return nil
        default :
            break
            
        }
        return nil
    }
    
    func setupFareDisTimeViewDetais(){
        //self.viewFareDisTimeDetails.layer.borderColor = UIColor.lightGrayColor().CGColor
        //self.viewFareDisTimeDetails.layer.borderWidth = 1.0
        
        let lblFare = self.viewFareDisTimeDetails.viewWithTag(20) as! UILabel
        
        if var fare = self.dicDataSource["totalfare"] as? String{
            if fare.count > 0 {
                fare = fare.replacingOccurrences(of: ",", with: "")
                lblFare.text =  fare //String(format: "%@ %.2f", appDelegate.currency, fare.toFloat())
            }
            else {
                lblFare.text = "N/A"
            }
        }
        
        let lblDistance = self.viewFareDisTimeDetails.viewWithTag(21) as! UILabel
        lblDistance.text = "N/A"
        
        if appDelegate.distanceType.contains("km"){
        if let dis = self.dicDataSource["distance_kilometers"]{
            if "\(dis)".count != 0 {
            lblDistance.text = "\(dis) \(appDelegate.distanceUnit!)"
        }
        }
        }
        else {
            if let dis = self.dicDataSource["distance_miles"]{
                if "\(dis)".count != 0 {
                    lblDistance.text = "\(dis) \(appDelegate.distanceUnit!)"
                }
            }
        }
        let lblTime = self.viewFareDisTimeDetails.viewWithTag(22) as! UILabel
        lblTime.numberOfLines = 2
        if let time = self.dicDataSource["time"], let dis_pref = self.dicDataSource["duration_preferance"]{
            if "\(time)".isEmpty {
                lblTime.text = "N/A"
            }else {
                if "\(time)".contains("mins"){
                    lblTime.text = "\(time)"
                }else {
                    
//                    var index1 = "\(dis_pref)".index("\(dis_pref)".endIndex, offsetBy: 4)
//                    
//                    var substring1 = "\(dis_pref)".substring(to: index1)
                    
                    let unit = "\(dis_pref)" == "minutes" ? "\(dis_pref)".replacingOccurrences(of: "minutes", with: "mins") : "\(dis_pref)"
                    
                    lblTime.text = "\(time) \(unit)"
                }

            }

        }
//        if (self.dicDataSource["time"] as! String).characters.count != 0 {
//            lblTime.text = Globals.sharedInstance.formatArrivalTimeStringWith((self.dicDataSource["time"]! as! String))
//        }else {
//            lblTime.text = "N/A"
//        }
        
        self.viewFareDisTimeDetails.translatesAutoresizingMaskIntoConstraints = true
        self.viewFareDisTimeDetails.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 90)
    }
    
    
    @IBAction func didTapFareBreakDownAction(_ sender: UIButton) {
        let detailsVc = appDelegate.storybaord?.instantiateViewController(withIdentifier: "PAFareBreakDownPopupVC") as! PAFareBreakDownPopupVC
        if let dicFare = self.dicDataSource["rates"] as? [String:String] {
            detailsVc.dicFareDetails = dicFare
        }
        if let fare = self.dicDataSource["rates_arr"] as? [[String:Any]] {
            detailsVc.arrFareDetails = fare
        }
     
        if let currency = self.dicDataSource["currency"] as? String {
            detailsVc.currency = currency
        }
        else{
            detailsVc.currency = appDelegate.currency
        }
        
        if let fare = self.dicDataSource["totalfare"] as? String{
            if fare.isEmpty == false {
                //fare = fare.replacingOccurrences(of: ",", with: "")
                detailsVc.strTotalFare = String(format: "%@", fare)
            }
            else {
                detailsVc.strTotalFare = "0.0"
            }
        }
        
        detailsVc.view.backgroundColor = UIColor.clear
        detailsVc.modalPresentationStyle = .overFullScreen
        detailsVc.providesPresentationContextTransitionStyle = true
        detailsVc.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overFullScreen
        self.present(detailsVc, animated: true, completion: nil)
        
    }

    @IBAction func btnEditNotesAction(_ sender: UIButton) {
        
         if String(describing: self.dicDataSource["primary_driver"]!) == "0" {
            Globals.ToastAlertWithString("Driver yet not assigned for job.")
            return
        }
        
        if let feedback = self.dicDataSource["passenger_comments"] as? [String:Any]{
            if let comments = feedback["passenger_comment"] as? String
            {
                self.txtViewEditableComments.text = comments == "" ? "Add Comments": comments
            }
            
            if let rating = feedback["passenger_rating"] as? String{
                self.viewEditStarRating.rating = rating.toDouble()!
            }
            else {
                self.viewEditStarRating.rating = 0
            }
        }
        else {
            self.txtViewEditableComments.text = "Add Comments"
            self.viewEditStarRating.rating = 0
        }
   
         self.viewEditStarRating.isUserInteractionEnabled = true
        self.viewEditStarRating.updateOnTouch = true
        UIView.animate(withDuration: 0.10, animations: { [weak self] in
            self?.ViewEditCommentsAndRating.frame.origin.y = 0.0
            self?.ViewEditCommentsAndRating.isHidden = false
            self?.viewEditCancelView.isHidden = false
            self?.viewPopUpStatus.isHidden = true
            self?.view.bringSubview(toFront: self!.ViewEditCommentsAndRating)
        }) 
    }
    
    @IBAction func btnCrossPopupAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.10, animations: { [weak self] in
            self?.ViewEditCommentsAndRating.isHidden = true
        }) 
        
    }

    @IBAction func btnDriverDetailsOrShareOrBookingStatusAction(_ sender: UIButton) {
        sender.isSelected = true
        weak var weakSender = sender
        
        if sender.tag == 150 {
            if let driverID = self.dicDataSource["primary_driver"] as? String{
                if  driverID.count > 0 && driverID != "0" {
                    
                    let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "PADriverFeedbackPopupVC") as! PADriverFeedbackPopupVC
                    
                    detailsVc.dicDriverInfo = self.dicDataSource
                    detailsVc.isPresented = true
                    detailsVc.isPastJob =  true
                    detailsVc.view.backgroundColor = UIColor.clear
                    detailsVc.modalPresentationStyle = .overFullScreen
                    detailsVc.providesPresentationContextTransitionStyle = true
                    detailsVc.definesPresentationContext = true
                    self.providesPresentationContextTransitionStyle = true
                    self.definesPresentationContext = true
                    self.modalTransitionStyle = .crossDissolve
                    self.modalPresentationStyle = .overFullScreen
                    self.present(detailsVc, animated: true) { () -> Void in
                        weakSender?.isSelected = false
                    }
                }
                else {
                    Globals.ToastAlertWithString("Driver yet not assigned for job")
                    sender.isSelected = false
                }
            }
            else {
                Globals.ToastAlertWithString("Driver yet not assigned for job")
                sender.isSelected = false
            }
        }else if sender.tag == 151 { // Booking Status
            self.ViewEditCommentsAndRating.isHidden = false
            
            var strStatus = self.dicDataSource["status"] as? String
            if strStatus == nil {
                strStatus = ""
            }
            if let lblPopupDetailsText = viewPopUpStatus.viewWithTag(1002) as? UILabel{
            lblPopupDetailsText.text = strStatus == "" ? "N/A \n \n \n " : "\(strStatus!) \n \n \n \n "
            UIView.animate(withDuration: 0.10, delay: 0.10, options:.overrideInheritedCurve, animations: { [weak self] in
                self?.viewEditCancelView.isHidden = true
                self?.viewPopUpStatus.isHidden = false
             }, completion: { (bol : Bool) in
                    weakSender?.isSelected = false
            })
            }
        }else  {
            
            if let strStatus = self.dicDataSource["status"] as? String
            {
                if let fare = self.dicDataSource["totalfare"] as? String, let jobStatus = self.dicDataSource["status"] as? String{
                    if strStatus == "Cancelled" && fare == "0.00"{
                        Globals.ToastAlertWithString("No invoice generated for this job.")
                        sender.isSelected = false
                        return
                    }
                    if jobStatus.compare("drop off") == true{
                        Globals.ToastAlertWithString("Please wait, Job is not yet finished by driver.")
                        sender.isSelected = false
                        return
                    }
                }
                
                let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "ShareTripDetailsViewController") as! ShareTripDetailsViewController
                detailsVc.strShareType = "invoice"
                detailsVc.reservation_id = self.dicDataSource["id"] as! String
                detailsVc.emailBillling = String(describing: self.dicDataSource["billingEmail"]!)
                detailsVc.view.backgroundColor = UIColor.clear
                detailsVc.modalPresentationStyle = .overFullScreen
                detailsVc.providesPresentationContextTransitionStyle = true
                detailsVc.definesPresentationContext = true
                self.providesPresentationContextTransitionStyle = true
                self.definesPresentationContext = true
                self.modalTransitionStyle = .crossDissolve
                self.modalPresentationStyle = .overFullScreen
                self.present(detailsVc, animated: true) { () -> Void in
                    weakSender?.isSelected = false
                }
            }
        }
    }
    
    @IBAction func btnDoneOrCancelAction(_ sender: UIBarButtonItem) {
        if sender.tag == 19 {
            self.txtViewEditableComments.text = "Add Comments"
        }
        self.view.endEditing(true)
    }
    // MARK:- ===== Rating View Delegate ====
    @IBAction func btnCancelAndDoneEditableRatingCommentsAction(_ sender: UIButton) {
        
        
        if sender.tag == 46 { // done button block
            if self.txtViewEditableComments.text == "Add Comments" || self.txtViewEditableComments.text.isEmpty {
               // Globals.ToastAlertWithString("Please add comments")
                //return
            }
            self.view.endEditing(true)

            self.txtViewNotesDriver.text = self.txtViewEditableComments.text
            if self.floatingRatesStar.rating != self.viewEditStarRating.rating && self.viewEditStarRating.rating != 0.0 {
                self.floatingRatesStar.rating = self.viewEditStarRating.rating
            }
            self.floatRatingViewAction(self.floatingRatesStar.rating)
        }else if sender.tag == 45 { // Cancel button block
            UIView.animate(withDuration: 0.10, animations: { [weak self] in
                self?.ViewEditCommentsAndRating.frame.origin.y = 5000000.0
                self?.ViewEditCommentsAndRating.isHidden = true
  
            }) 
        }
    }
    func floatRatingViewAction(_ rating: Double) {
        if appDelegate.networkAvialable == false   {
            return
        }
        appDelegate.showProgressWithText("Please Wait...")
        let userInfo = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        var dicParameters = [String:Any]()
        dicParameters["driver_id"] = self.dicDataSource["primary_driver"]!
        dicParameters["user_id"] = userInfo["id"]!
        dicParameters["session"] = userInfo["session"]!
        dicParameters["reservation_id"] = self.dicDataSource["id"]!
        dicParameters["rating"] = "" as Any?
        dicParameters["pass_feedback"] = "" as Any?
        dicParameters["avg_rating"] = rating as Any?
        if let feedback = self.dicDataSource["passenger_comments"] as? [String:Any]{
            if let feedback_id = feedback["feedback_id"] as? String
            {
                dicParameters["feedback_id"] = feedback_id as Any?
            }
            else {
                dicParameters["feedback_id"] = "" as Any?
            }
        }
        else {
            dicParameters["feedback_id"] = "" as Any?
        }
        dicParameters["rating_type"] = "single" as Any?
        dicParameters["comment"] = self.txtViewEditableComments.text == "Add Comments" ? "" : self.txtViewEditableComments.text
        dicParameters["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?
        dicParameters["created_on"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?
        appDelegate.showProgressWithText("Please wait..")
        ServiceManager.sharedInstance.dataTaskWithPostRequest("customerfeedback", dicParameters: dicParameters, successBlock: { (dicData:[String:Any]!) in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success"{
                Constants.reservationUpdated = true
                Globals.ToastAlertWithString(dicData["message"] as! String)
                UIView.animate(withDuration: 0.10, animations: { [weak self] in
                    self?.ViewEditCommentsAndRating.frame.origin.y = 5000000.0
                    self?.ViewEditCommentsAndRating.isHidden = true
                }) 
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString((error?.description)!)
        }
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}// class brackets
