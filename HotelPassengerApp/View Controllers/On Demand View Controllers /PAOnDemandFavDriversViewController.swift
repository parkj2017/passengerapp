//
//  PAOnDemandFavDriversViewController.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 18/07/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import MapKit
import DLImageLoader
class tableHeaderView:UITableViewCell
{
    @IBOutlet var btnBookNow: UIButton!
    @IBOutlet var btnCheckBox: UIButton!
}

class cellTypeFavDriverOnDemand:UITableViewCell
{
    @IBOutlet var imageDriver: UIImageView!
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var lblDistanceInfo: UILabel!
    @IBOutlet var btnCheckBox: UIButton!
    @IBOutlet var btnEstFare: UILabel!

    @IBOutlet var activityView: UIActivityIndicatorView!
    
    func setupDriverView(_ imageUrl:String)  {
        
       // self.activityView.hidden = false;
        self.activityView.startAnimating()
        if appDelegate.networkAvialable == true {
            DLImageLoader.sharedInstance().image(fromUrl: imageUrl, completed: { [weak self](error, image) -> () in
                DispatchQueue.main.async(execute: { () -> Void in
                    self?.activityView.stopAnimating()
                    self?.activityView.isHidden = true;
                    if image != nil {
                        self?.imageDriver.image = image
                    }else {
                        self?.imageDriver.image = UIImage(named: "profile_placeholder")
                    }
                })
                })
        }
        else {
            DispatchQueue.main.async(execute: { () -> Void in
                self.activityView.stopAnimating()
                self.activityView.isHidden = true;
                self.imageDriver.image = UIImage(named: "profile_placeholder")
            })
        }
        
    }
    func setDistance(_ source:CLLocation, destination:CLLocation, fare:Float){
        let urlStr = Globals.sharedInstance.getFullApiAddressForDistanceToDriverDistance(source, destinationLoc: destination)
        
        if urlStr == "" {
            return
        }
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: { () -> Void in
            ServiceManager.sharedInstance.dataTaskWithGetRequest(urlStr, successBlock: { [weak self](dicData) -> Void in
                if dicData["status"] as! String == "OK" {
                    let arrRoutes = dicData["routes"] as! [[String : Any]]
                    let arrLegs = arrRoutes.first!["legs"] as! [[String:Any]]
                   // let dicDistance = arrLegs.first!["distance"] as! [String:Any]
                    let dicTime = arrLegs.first!["duration"] as! [String:Any]
                   // var distanceValue:Float! = 0.0//dicDistance["value"] as! Float
                    let timeValue = (dicTime["value"] as! Float/60)
                    
                    if fare != 0.0{
                        self?.btnEstFare.text = String(format: "Estimated Fare: %@ %.2f", appDelegate.currency, fare)
                    }
                    else {
                        self?.btnEstFare.text = ""
                    }
                    
                    self?.lblDistanceInfo.text = timeValue<=1 ? "1 Min away from your location" : String(format: "%.0f Mins away from your location", timeValue)
                }
                },                failureBlock: { (error) -> () in
                    appDelegate.hideProgress()
                    print("caculate fare error \(error?.description)")
            })
        })
    }
    
    override func  awakeFromNib() {
        self.imageDriver.layer.cornerRadius = DeviceType.IS_IPAD ? 50 : 40
        self.imageDriver.layer.masksToBounds = true
        self.imageDriver.layer.borderColor = UIColor.appBlackColor().cgColor
        self.imageDriver.layer.borderWidth = 1.0
        
    }
    
}

class PAOnDemandFavDriversViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet var tblFavDrivers: UITableView!
    var preAuthAmountForJob:String!
    
    lazy var locationManagerObjct = LocationManager.sharedInstance

    var favDriversArray:[[String:Any]] = [[String:Any]]()
    var selectedDriversArray:[[String:Any]] = [[String:Any]]()
    

    var selectionStatus:[Int] = [Int]()
    var didFinishSelectingDrivers:((_ selectedDrivers:[[String:Any]])->())?

    @IBOutlet var bgView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureAction(_:)))
        tapGesture.delegate = self
        //tapGesture.cancelsTouchesInView = true
        bgView.addGestureRecognizer(tapGesture)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func tapGestureAction(_ tapGesture:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool
    {
            return true
    }

    //MARK:- ======= table View Data Source =============
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.favDriversArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        var cell   = tblFavDrivers.dequeueReusableCell(withIdentifier: "cellFavDriverIdentifier") as? cellTypeFavDriverOnDemand
        if cell == nil{
            cell = UITableViewCell(style: .default, reuseIdentifier: "cellFavDriverIdentifier")  as? cellTypeFavDriverOnDemand
        }
        
        if indexPath.row % 2 == 0{
            cell?.backgroundColor = UIColor(red: 214.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0)
        }
        else {
            cell?.backgroundColor = UIColor.white
        }
        
        var dic = self.favDriversArray[indexPath.row]
        
        var driverName = ""
        if let fName = dic["driver_name"] as? String{
            driverName = fName
        }

        cell?.setupDriverView(dic["image_url"] as! String)
        cell?.lblDriverName.text = driverName

        let postion = CLLocation(latitude: (dic["latitude"]! as! AnyObject).doubleValue, longitude: (dic["longitude"]! as! AnyObject).doubleValue)
        var estFare:Float = 0.0
        if let strFareEstimated =  dic["grandTotal"]{
              estFare =  "\(strFareEstimated)".replacingOccurrences(of: ",", with: "").toFloat()
        }
         cell!.setDistance(postion, destination: self.locationManagerObjct.pickUpLocation!, fare: estFare)
        cell!.btnCheckBox.tag = 100 + indexPath.row
        cell!.btnCheckBox.addTarget(self, action: #selector(self.btnCheckBoxClicked(_:)), for: .touchUpInside)
        
        if selectionStatus.contains(1001)
        {
             cell!.btnCheckBox.isSelected = true
        }
        else {
            cell!.btnCheckBox.isSelected = selectionStatus.contains(100+indexPath.row)
        }
        cell!.selectionStyle = UITableViewCellSelectionStyle.none
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return DeviceType.IS_IPAD ? 150 : 120.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return DeviceType.IS_IPAD ? 60 : 50
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        var cell   = tblFavDrivers.dequeueReusableCell(withIdentifier: "headerIdentifier") as? tableHeaderView
        if cell == nil{
            cell = UITableViewCell(style: .default, reuseIdentifier: "headerIdentifier")  as? tableHeaderView
        }
        cell?.btnBookNow.layer.cornerRadius = 5
        cell?.btnBookNow.layer.borderColor = UIColor.white.cgColor
        cell?.btnBookNow.layer.borderWidth = 1.0
        cell!.btnBookNow.addTarget(self, action: #selector(self.btnBookNowClicked(_:)), for: .touchUpInside)

        cell?.btnCheckBox.tag = 1001;
        cell?.btnCheckBox.addTarget(self, action: #selector(self.btnCheckBoxClicked(_:)), for: .touchUpInside)

        cell?.btnCheckBox.isSelected = selectionStatus.contains((cell?.btnCheckBox.tag)!)
        
        return cell!

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
//        var dicParam = [String:Any]()
//        let dic = self.favDriversArray[indexPath.row]
//        
    }

    func btnCheckBoxClicked(_ sender:UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true{
            if sender.tag == 1001{
                //selectedDriversArray.removeAll()
                selectionStatus.removeAll()
               // selectedDriversArray.appendContentsOf(favDriversArray)
                for index  in 0..<favDriversArray.count{
                    selectionStatus.append(100+index)
                }
            }
            else {
               // selectedDriversArray.append(favDriversArray[sender.tag % 100])
            }
            selectionStatus.append(sender.tag)
         }
        else{
            if sender.tag == 1001{
               // selectedDriversArray.removeAll()
                selectionStatus.removeAll()
            }
            else {
                if selectionStatus.contains(1001){
                    selectionStatus.remove(1001)
                }
                selectionStatus.remove(sender.tag)
             //   selectedDriversArray.removeAtIndex(sender.tag % 100)
            }
         }
        tblFavDrivers.reloadData()
    }
    func btnBookNowClicked(_ sender:UIButton) {

//        let payment = self.storyboard?.instantiateViewControllerWithIdentifier("PAPaymentForOnDemandVC") as! PAPaymentForOnDemandVC
//        payment.arrOndemanCarsSelect = favDriversArray
//        payment.isJobRequest = true
//        payment.strPreAuthAmountForJob = preAuthAmountForJob
//    
        if selectionStatus.contains(1001){
            selectionStatus.remove(1001)
        }
        if self.selectionStatus.count == 0{
            Globals.ToastAlertWithString("Please select at least one driver")
        }
        else{
            selectedDriversArray.removeAll()
            for index  in 0..<selectionStatus.count{
                selectedDriversArray.append(favDriversArray[selectionStatus[index] % 100])
            }
        self.didFinishSelectingDrivers!(selectedDriversArray)
        self.dismiss(animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
