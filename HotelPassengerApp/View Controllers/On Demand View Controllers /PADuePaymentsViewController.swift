//
//  PADuePaymentsViewController.swift
//  PassangerApp
//
//  Created by Netquall on 9/26/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
class cellDuePaymentReservation: UITableViewCell {
    
    @IBOutlet weak var lblBookingID: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblPickup: UILabel!
    @IBOutlet weak var lblDropoff: UILabel!
    @IBOutlet weak var lblDueAmount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.borderColor = UIColor.lightGray.cgColor
        containerView.layer.borderWidth = 0.5
        lblPickup.numberOfLines = 3
        lblDropoff.numberOfLines = 3
        containerView.backgroundColor = UIColor.clear
    }
    
    func setCellValues(_ dic:[String:Any]) {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
class PADuePaymentsViewController: BaseViewController, BackButtonActionProtocol {
    
    var arrUnpaidReservations:[[String:Any]] = [[String:Any]]()
    
    @IBOutlet weak var tableView: UITableView!
    var totalDueAmount:Float = 0.0
    
    
    @IBOutlet weak var lblDueAmount: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("On demand", imageName: "backArrow")
        // Do any additional setup after loading the view.
        
            tableView.isHidden = false
        
            for index in 0..<arrUnpaidReservations.count
            {
                let valuesDict = arrUnpaidReservations[index]
                
                if let strFareEstimated =  valuesDict["grand_total"] as? String{
                    let estFare =  String(strFareEstimated.replacingOccurrences(of: ",", with: "")).toFloat()
                    totalDueAmount += estFare
                }
                else {
                    
                }
            }
            lblDueAmount.text = appDelegate.currency + "\(totalDueAmount)"
            tableView.reloadData()
    
    }
    
    override func  viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         Globals.sharedInstance.delegateBack = nil
    }
    override func  viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- ======= table View Data Source =============
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrUnpaidReservations.count
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        var cell   = tableView.dequeueReusableCell(withIdentifier: "dueAmountCellIdentifier") as? cellDuePaymentReservation
        if cell == nil{
            cell = UITableViewCell(style: .default, reuseIdentifier: "dueAmountCellIdentifier")  as? cellDuePaymentReservation
        }
        
        if indexPath.row % 2 == 0{
            cell?.backgroundColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        }
        else {
            cell?.backgroundColor = UIColor.white
        }
        
        var dic = self.arrUnpaidReservations[indexPath.row]
        /*
         {
         "reservation_id": "6812",
         "company_id": "40",
         "grand_total": "70.00",
         "conf_id": "-10-",
         "pu_date": "2016-09-26",
         "pu_time": "14:39:00",
         "pickup": "Unnamed Road, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 140308, India",
         "dropoff": ""
         }*/
        
        if var confID = dic["conf_id"] as? String{
            confID = confID.replacingOccurrences(of: "-", with: "")
            cell?.lblBookingID.attributedText = confID.setAttributedStringWithColorSize("Booking ID: ", secondStr: "#" + confID, firstColor: UIColor.black, secondColor: UIColor.red, font1: Globals.defaultAppFontWithBold(15), font2:Globals.defaultAppFontWithBold(15))
        }
        else {
            cell?.lblBookingID.attributedText = "".setAttributedStringWithColorSize("Booking ID: ", secondStr: "N/A", firstColor: UIColor.gray, secondColor: UIColor.red, font1: Globals.defaultAppFont(15), font2:Globals.defaultAppFontWithBold(15))
        }
        
        if let pickup = dic["pickup"] as? String{
            cell?.lblPickup.attributedText = pickup.setAttributedStringWithColorSize("Pickup:\n", secondStr: pickup, firstColor: UIColor.black, secondColor: UIColor.lightBlack(), font1: Globals.defaultAppFontWithBold(12), font2:Globals.defaultAppFont(12))
        }
        else {
            cell?.lblPickup.attributedText = "".setAttributedStringWithColorSize("Pickup:\n", secondStr: "N/A", firstColor: UIColor.black, secondColor: UIColor.lightBlack(), font1: Globals.defaultAppFontWithBold(13), font2:Globals.defaultAppFont(13))
        }
        
        if let dropoff = dic["dropoff"] as? String{
            cell?.lblDropoff.attributedText = dropoff.setAttributedStringWithColorSize("Dropoff:\n", secondStr: dropoff, firstColor: UIColor.black, secondColor: UIColor.lightBlack(), font1: Globals.defaultAppFontWithBold(13), font2:Globals.defaultAppFont(13))
        }
        else{
            cell?.lblDropoff.attributedText = "".setAttributedStringWithColorSize("Dropoff:\n", secondStr: "N/A", firstColor: UIColor.black, secondColor: UIColor.lightBlack(), font1: Globals.defaultAppFontWithBold(13), font2:Globals.defaultAppFont(13))
        }
        
        if let total = dic["grand_total"] as? String{
            cell?.lblDueAmount.text = appDelegate.currency +  total
        }
        else {
            cell?.lblDueAmount.text = appDelegate.currency +  "0.00"
        }
        if var date = dic["pu_date"] as? String{
            if let time = dic["pu_time"] as? String{
                date = date + " " + time
            }
            cell?.lblDateTime.text = Globals.getFormattedDateString(date)
        }
        else {
            cell?.lblDateTime.text = "N/A"
        }
        
        cell!.selectionStyle = UITableViewCellSelectionStyle.none
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    /*
     
     https://staging.loginla.com/webservice/chargePendingTrips
     Keys :-
     {
     "current" : "2016-09-15 12:24:43",
     "user_id":"15778",
     "payment_id": "1206",
     "records": [
     {
     "reservation_id": "5354",
     "company_id": "40",
     "grand_total": "29.50"
     }
     ]
     }*/
    func makePayment(_ paymentID:String)
    {
        if appDelegate.networkAvialable == true {
            var dicParameters = [String:Any]()
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")

            dicParameters["user_id"] = user_ID
            dicParameters["session"] = session
            dicParameters["company_id"] = Constants.CompanyID as Any?
            dicParameters["payment_id"] = paymentID as Any?
            dicParameters["records"] = arrUnpaidReservations as Any?
  
            weak var weakSelf = self
            appDelegate.showProgressWithText("Please wait...")
            ServiceManager.sharedInstance.dataTaskWithPostRequest("chargePendingTrips", dicParameters: dicParameters, successBlock: { (dicData) -> Void in
                appDelegate.hideProgress()
                if weakSelf == nil {
                    return
                }
                if (dicData["status"] as! String) == "success" {
                    let alert = UIAlertView(title: "Thank You", message:"Your payment has been successfully processed.", delegate: nil, cancelButtonTitle: "Ok")
                    alert.show()
                    self.backButtonActionMethod(UIButton())
                }else  {
                    let alert = UIAlertView(title: "Error !", message: dicData["message"] as? String, delegate: nil, cancelButtonTitle: "Ok")
                    alert.show()
                }
        })
        { (error) -> () in
            appDelegate.hideProgress()
            let alert = UIAlertView(title: "Sorry!", message: "Please try again later.", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
            return
        }
    }else {
    Globals.ToastAlertForNetworkConnection()
    }
}

    // MARK: - IBActions
    @IBAction func btnPaynowClicked(_ sender: Any) {
        
        let payment = self.storyboard?.instantiateViewController(withIdentifier: "PASelectPaymentVC") as! PASelectPaymentVC
        //payment.fromDuePaymentScreen = true
        payment.pushToRequestView = { [weak self] (dicCardDetails:[String:Any]?, cardID:String?) -> () in
            if let _ = dicCardDetails
            {
                //["id": 1298, "payment_method": 11, "name": Cash]
                //["id": 1302, "payment_method": 3, "name": Direct Bill/Invoice]
                if let name = dicCardDetails!["name"] as? String  {
                    if name.compare("cash") || name.compare("Direct Bill/Invoice"){
                        appDelegate.showProgressWithText("Payment can only be done via card.")
                    }
                    else{
                        if let paymentID = dicCardDetails!["id"]  {
                            self?.makePayment(String(describing: paymentID))
                        }
                    }
                }
               
            }
        }
        self.navigationController?.pushViewController(payment, animated: true)

    }
    
     // MARK: - Backbutton delegate
    func backButtonActionMethod(_ sender: UIButton) {
        
        appDelegate.hideProgress()
        
        DispatchQueue.main.async { [weak self]() -> Void in
            if self == nil {
                return
            }
            for vc: UIViewController in (self?.navigationController?.viewControllers)! {
                if (vc.isKind(of: PAMainMenuViewController.self)) {
                    self?.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }
    }
    

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
