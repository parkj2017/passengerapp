//
//  OnDemandViewController.swift
//  PassangerApp
//
//  Created by Netquall on 12/2/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader
import GoogleMaps
import Alamofire
import SwiftyJSON
import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

class carMarker:GMSMarker {
   
    var marker_location:CLLocation = CLLocation(latitude: 0.0, longitude: 0.0)
    var marker_Image:UIImage = UIImage()
    var marker_id:String = ""
    var marker_Type:String = ""
    
    func addMarkerWithData(locaion:CLLocation, markerImage:UIImage, id:String, type:String){
        self.marker_location = location
        self.marker_Image = markerImage
        self.marker_id = id
        self.marker_Type = type
    }
}


class vehicleSelectionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblVehicleType: UILabel!
    @IBOutlet weak var imageCarIcon: UIImageView!
    @IBOutlet weak var lblPax: UILabel!
    
    @IBOutlet weak var lblNoFleets: UILabel!
    @IBOutlet weak var lblTotalFare: UILabel!
    @IBOutlet weak var lblETA: UILabel!
    @IBOutlet weak var lblLugg: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
   
    @IBOutlet weak var layoutView: UIView!
    @IBOutlet weak var imageCheckbox: UIImageView!
    
    @IBOutlet weak var lblCompanyName: UILabel!
    
    
    
    func selectCell(flag:Bool){
        if flag{
            self.imageCheckbox.isHidden = false
            Globals.layoutViewFor(self.layoutView, color: UIColor.appThemeColor(), width: 2.0, cornerRadius: 5.0)

        }else {
            self.imageCheckbox.isHidden = true

            Globals.layoutViewFor(self.layoutView, color: UIColor.lightGray, width: 2.0, cornerRadius: 5.0)
        }
    }
}
class OnDemandViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, BackButtonActionProtocol, UICollectionViewDataSource, UICollectionViewDelegate {
    // MARK:- ===== Property Decalaration ========
    
    @IBOutlet weak var tableViewCars: UITableView!
    
    @IBOutlet weak var viewHeaderTableView: UIView!
    
    @IBOutlet var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewDefault: UIScrollView!
    
    @IBOutlet weak var btnSourceAddress: UIButton!
    @IBOutlet  var mapView: GMSMapView!
    @IBOutlet weak var mapCenterPinImage: UIImageView!
    
    
    @IBOutlet weak var btnDestinationAddress: UIButton!
    
    @IBOutlet var viewNearbyFavDrivers: UIView!
    @IBOutlet var viewConfirmation: UIView!
    
    @IBOutlet weak var constraint_QuickBookButtonHeight: NSLayoutConstraint!
    var markerDestinationAddress = GMSMarker()
    lazy  var markerPickUpAddress : GMSMarker? = GMSMarker()
    var map_CentreCoordinate : CLLocationCoordinate2D?
    //var mapBounds : GMSCoordinateBounds!
    var carLocationNearest : CLLocation?
    let searchRadius: Double = 100
    var ZoomOut_In : Float = 16.0
    var onDemand_carInformationArray = [[String: Any]]()
    lazy var dicOnDemandCarsAvailable = [String:[[String: Any]]]()
    lazy var dicArrivalTimeOfCarType = [String: Any]()
    var carType_Title = String()
    // images of cars nsdata objects
    lazy var image_DataSedan : Data? = Data()
    lazy var image_DataPremSedan = Data()
    lazy var image_DataSuv = Data()
    // few
    lazy var arrTotalCarsKeys = [String]()
    lazy var vehicleTypeID: String  = String()
    var indexPathCellSelected : IndexPath!
    lazy var locationManagerObjct = LocationManager.sharedInstance
    var arrVehicleInfo = [[String:Any]]()
    lazy var checkForFocusByDestination =  Bool()
    
    var timerCarList : Timer!
    var isActiveJobNotificationTap = false
    
    var nearByFavDrivers:[[String:Any]] = [[String:Any]]()
    var maxFareAmount:Float = 0.0
    var filterElementsData:[String:Any] = [String:Any]()
    @IBOutlet weak var txtPassengerName_OR_roomNo: UITextField!
    
    @IBOutlet weak var collectionOndemandCars: UICollectionView!
    
    
    var etaLblShowTime:UILabel!

    var pickup_loc:CLLocation = CLLocation(latitude: 0.0, longitude: 0.0)
    var dropoff_loc:CLLocation = CLLocation(latitude: 0.0, longitude: 0.0)

    // MARK:- ===== View Life cycle ========
    
    let dicActiveUser = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as? [String:Any]
    //    let contentSize = cellBooking.pickupAddress.intrinsicContentSize
    //    print("contentSize pickup --\(contentSize)")
    //    //       self.pickupAddress.font = Globals.defaultAppFont(viewFontSize )

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.locationManagerObjct.dicDestinationLocation!.removeAll()
        self.locationManagerObjct.destinationLocation = nil
        
        self.btnSourceAddress.titleLabel?.numberOfLines = 3;
        self.btnDestinationAddress.titleLabel?.numberOfLines = 3;
        
        self.txtPassengerName_OR_roomNo.delegate = self
        
        viewNearbyFavDrivers.backgroundColor =  UIColor(patternImage: UIImage(named:"greenPattern")!)
        
        /*
        let navLeftBtn: UIButton = UIButton(type: .custom)
        navLeftBtn.addTarget(self, action: #selector(OnDemandViewController.btnFilterClicked(_:)), for: .touchUpInside)
        
        navLeftBtn.setImage(UIImage(named: "filter_unslected"), for: .normal)
        navLeftBtn.setImage(UIImage(named: "filter_slected"), for: .selected)
        
        navLeftBtn.contentMode = .scaleAspectFit
        navLeftBtn.frame = CGRect(x: 0, y: 0, width: 110, height: 41) //CGRect(0, 0,41, 41)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: navLeftBtn)
        navigationItem.rightBarButtonItem = leftBarButton;
        */
        DispatchQueue.main.async {[weak self] () -> Void in
            self?.viewHeightConstraint.constant = 0.0
            self?.view.layoutIfNeeded()
            self?.view.translatesAutoresizingMaskIntoConstraints = true
        }
        self.mapCenterPinImage.isHidden = false
        self.showEtaMethodsOnMapWith("Loading...", imageString: "mappin")

        // self.setNavigationBarItem()
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Book for now", imageName: "backArrow")
        //self.navigationController?.navigationBar.SetNavigationBarNormalColor()
        self.setupPickupAddressOnButton(strAddress: self.locationManagerObjct.strCurrentLocation)
        self.setupDropOffAddressOnButton(strAddress: "Add Drop-off Address")
        //       self.btnSourceAddress.setTitle(self.locationManagerObjct.strCurrentLocation, for: UIControlState())

        mapView.delegate = self
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.settings.zoomGestures = true
        self.checkForFocusByDestination = false
        self.locationManagerObjct.destinationLocation = nil
        // map center view button
        DispatchQueue.main.async {[weak self] () -> Void in
            self?.viewHeightConstraint.constant = 0.0
            if Constants.needForHotel == "1"{
                self?.constraint_QuickBookButtonHeight.constant = 40
            }
            else {
                self?.constraint_QuickBookButtonHeight.constant = 0
            }
            self?.view.layoutIfNeeded()
            self?.view.translatesAutoresizingMaskIntoConstraints = true
        }
        // self.btnMapViewCenter.titleLabel?.font = UIFont.systemFontOfSize(10.0)
        // self.btnMapViewCenter.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 22, 0)
       
        self.locationManagerObjct.pickUpLocation = self.locationManagerObjct.currentLocation
        
        DispatchQueue.main.async(execute: { [weak self]() -> Void in
            self?.mapView.clear()
            let camera = GMSCameraPosition.camera(withLatitude: (LocationManager.sharedInstance.currentLocation!.coordinate.latitude), longitude:(LocationManager.sharedInstance.currentLocation!.coordinate.longitude) , zoom: 16.0)
            self?.mapView.animate(to: camera)
        })
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {[weak self] () -> Void in
            self?.refreshMapViewForCheckOnDemandCars()
        }
        self.MapStopScrollingAndGetAddressOfCurrentLocation(self.locationManagerObjct.currentLocation!)

        // Do any additional setup after loading the view.
        
        if Constants.ondemandCollectionView == "1"{
            self.tableViewCars.isHidden = true
            self.collectionOndemandCars.isHidden = false
            self.collectionOndemandCars.delegate = self
            self.collectionOndemandCars.dataSource = self
        }
        else {
            self.tableViewCars.isHidden = false
            self.collectionOndemandCars.isHidden = true
            self.tableViewCars.delegate = self
            self.tableViewCars.dataSource = self
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        appDelegate.hideProgress()
        Globals.sharedInstance.delegateBack = self
        //Globals.ToastAlertWithString("Loading nearby cars")
        self.refreshNearCarsList()
        
        self.mapView.bringSubview(toFront: self.mapCenterPinImage)
        if let btn = self.mapView.viewWithTag(30) {
            self.mapView.bringSubview(toFront: btn)
        }
        if let btn = self.mapView.viewWithTag(31) {
            self.mapView.bringSubview(toFront: btn)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopRefreshCarListTimer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK:- ===add address to pickup dropOff Location  ===== //
    
    
    func btnFilterClicked( _ sender: UIButton) {
        
        if onDemand_carInformationArray.count == 0{
            Globals.ToastAlertWithString("Please wait... loading nearby cars")
            return
        }
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected == true{
            let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "PAOndemandFilterViewController") as! PAOndemandFilterViewController
            detailsVc.maxFare = Double(maxFareAmount)
            detailsVc.didFinishApplyingFilters = {[weak self](filterElements:[String:Any]?) -> () in
                
                if let rightBarBtn = self?.navigationItem.rightBarButtonItem?.customView as? UIButton{
                    if filterElements != nil
                    {
                        self?.filterElementsData = filterElements!
                        rightBarBtn.isSelected = true
                        self?.refreshMapViewForCheckOnDemandCars()
                    }
                    else {
                        rightBarBtn.isSelected = false
                    }
                }
            }
            detailsVc.view.backgroundColor = UIColor.clear
            detailsVc.modalPresentationStyle = .overFullScreen
            detailsVc.providesPresentationContextTransitionStyle = true
            detailsVc.definesPresentationContext = true
            self.providesPresentationContextTransitionStyle = true
            self.definesPresentationContext = true
            self.modalTransitionStyle = .crossDissolve
            self.modalPresentationStyle = .overFullScreen
            self.present(detailsVc, animated: true) {}
        }
        else {
            
            self.filterElementsData = [String:Any]()
            self.refreshMapViewForCheckOnDemandCars()
            //  if let rightBarBtn = self.navigationItem.rightBarButtonItem?.customView as? UIButton{
            //     rightBarBtn.isSelected = false
            // }
        }
    }
    //Add Destination To Get A Fare Quote
    func setupPickupAddressOnButton(strAddress:String?)  {
        var attrsTitle : NSMutableAttributedString!
        attrsTitle = NSMutableAttributedString(string:"Pick-Up Address \n", attributes: [NSForegroundColorAttributeName:UIColor.appThemeColor(),NSFontAttributeName: Globals.defaultAppFont(12)])
        if strAddress != nil {
            attrsTitle.append(NSAttributedString(string: strAddress!, attributes: [NSForegroundColorAttributeName:UIColor.black,NSFontAttributeName: Globals.defaultAppFont(14)]))
        }
        self.btnSourceAddress.setAttributedTitle(attrsTitle, for: .normal)
        
    }
    func setupDropOffAddressOnButton(strAddress:String?)  {
        var attrsTitleDrop : NSMutableAttributedString!
        attrsTitleDrop = NSMutableAttributedString(string:"Drop-Off Address \n", attributes: [NSForegroundColorAttributeName:UIColor.appRideLater_GreenColor(),NSFontAttributeName: Globals.defaultAppFont(12)])
        if strAddress != nil {
            attrsTitleDrop.append(NSAttributedString(string: strAddress!, attributes: [NSForegroundColorAttributeName:UIColor.black,NSFontAttributeName: Globals.defaultAppFont(14)]))
        }
        self.btnDestinationAddress.setAttributedTitle(attrsTitleDrop, for: .normal)
    }
    // MARK:- ===timer method to refresh near car list =====
    func refreshNearCarsList(){
        // self.refreshMapViewForCheckOnDemandCars()
        self.timerCarList = Timer.scheduledTimer(timeInterval: 10.0, target:self, selector: #selector(OnDemandViewController.refreshMapViewForCheckOnDemandCars), userInfo:nil , repeats: true)
    }
    func stopRefreshCarListTimer(){
        if self.timerCarList !=  nil {
            if self.timerCarList.isValid{
                self.timerCarList.invalidate()
                
            }
        }
    }
    // MARK: ===== Custom Button Methodsss ========
    @IBAction func btnZoomInMapViewACtion(_ sender: UIButton) {
        if ZoomOut_In < 0.0{
            ZoomOut_In = 0.0
        }
        if ZoomOut_In >= 0.0 {
            ZoomOut_In += 3.0
            self.mapView.animate(toZoom: ZoomOut_In)
        }
    }
    
    @IBAction func btnZoomOutMapViewAction(_ sender: UIButton) {
        if ZoomOut_In < 0.0{
            ZoomOut_In = 0.0
        }
        if ZoomOut_In >= 0.0 {
            ZoomOut_In -= 3.0
            self.mapView.animate(toZoom: ZoomOut_In)
        }
    }
    @IBAction func btnCarsPressedForGettingBaseRate(_ sender: UIButton) {
    }
    // MARK:- ===add custom pickup address button action =====
    
    @IBAction func btnAddSourceAddressAction(_ sender: UIButton) {
        self.checkForFocusByDestination = false
        // weak var weakSelf = self
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesAddessViewController") as! SearchPlacesAddessViewController
        searchVC.strTitleOfView = "Source Address"
        searchVC.isPickUpAddress = true
        searchVC.viewFrom = "onDemand"
        searchVC.finishSelectingDropOffAddress = {[weak self] (dicDropOff : [String:Any]) -> () in
            if dicDropOff.count != 0 {
                self?.locationManagerObjct.dicSourceCustomLocation = dicDropOff
                self?.locationManagerObjct.pickUpLocation! = CLLocation(latitude: (((dicDropOff["latitude"]!) as Any) as AnyObject).doubleValue, longitude: ((dicDropOff["longitude"]!) as AnyObject).doubleValue)
                self?.setupPickupAddressOnButton(strAddress: dicDropOff["address"] as? String ?? "N/A")
                self?.locationManagerObjct.strCurrentLocation = (dicDropOff["address"] as? String)!
                self?.showPickUpAddressOnMap()
                self?.refreshMapViewForCheckOnDemandCars()
                if  self?.locationManagerObjct.destinationLocation != nil {
                    self?.calulateEstTotalFairBtSourceDestination()
                }
            }
        }
        let nvc = UINavigationController(rootViewController: searchVC)
        searchVC.strTypeOfAddress = "Pick Up Address"
        self.navigationController?.present(nvc, animated: true, completion: nil)
    }
    // MARK:- ===add custom Drop off address button action =====
    
    @IBAction func btnAddDestinationToGetAFareAction(_ sender: UIButton) {
        
        self.checkForFocusByDestination = false
        // weak var weakSelf = self
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesAddessViewController") as! SearchPlacesAddessViewController
        searchVC.strTitleOfView = ""
        searchVC.isPickUpAddress = false
        searchVC.viewFrom = "onDemand"
        searchVC.finishSelectingDropOffAddress = { [weak self](dicDropOff : [String:Any]) -> () in
            if dicDropOff.count != 0 {
                self?.locationManagerObjct.dicDestinationLocation = dicDropOff
                self?.locationManagerObjct.destinationLocation = CLLocation(latitude: ((dicDropOff["latitude"]!) as AnyObject).doubleValue, longitude: ((dicDropOff["longitude"]!) as AnyObject).doubleValue)
                
                if let dropoff = dicDropOff["address"]{
                    self?.setupDropOffAddressOnButton(strAddress:"\(dropoff)".isEmpty ? "Add Drop-off Address" : "\(dropoff)")
                }
                self?.showDestinationAddressOnMap(dicDropOff)
                self?.refreshMapViewForCheckOnDemandCars()
                self?.calulateEstTotalFairBtSourceDestination()
            }
        }
        let nvc = UINavigationController(rootViewController: searchVC)
        //searchVC.title = "Destination Address"
        searchVC.strTypeOfAddress = "Drop-Off Address"
        self.navigationController?.present(nvc, animated: true, completion: nil)
    }
    // MARK: ========Show Marker on Map of pickup and Destination Address ==================
    func showPickUpAddressOnMap(){
        self.checkForFocusByDestination = false
        self.markerPickUpAddress!.map = nil
        //let destinationLocation = CLLocation(latitude: dicDestination["latitude"] as! Double, longitude: dicDestination["longitude"] as! Double)
        self.markerPickUpAddress!.position = CLLocationCoordinate2DMake((self.locationManagerObjct.pickUpLocation?.coordinate.latitude)!, (self.locationManagerObjct.pickUpLocation?.coordinate.longitude)!)
        // self.markerPickUpAddress!.userData = dicDestination["address"] as? String
        DispatchQueue.main.async(execute: { [weak self]() -> Void in
            self?.mapCenterPinImage.isHidden = false
            // self?.markerPickUpAddress!.icon = UIImage(named: "mappin")
            // self?.markerPickUpAddress!.map = self?.mapView
        })
        let mapBounds:GMSCoordinateBounds = GMSCoordinateBounds(coordinate:self.locationManagerObjct.pickUpLocation!.coordinate , coordinate: self.locationManagerObjct.pickUpLocation!.coordinate)
        //weak var weakSelf = self
        UIView.animate(withDuration: 0.20, animations: { [weak self]() -> Void in
            
            if self?.viewNearbyFavDrivers.isHidden == false{
                self?.mapView.animate(to: (self?.mapView.camera(for: mapBounds, insets:UIEdgeInsetsMake(90.0, 40.0, 40.0, 40.0)))!)
            }else{
                self?.mapView.animate(to: (self?.mapView.camera(for: mapBounds, insets:UIEdgeInsetsMake(40.0, 40.0, 40.0, 40.0)))!)
            }
        })
    }
    func showDestinationAddressOnMap(_ dicDestination:[String:Any]){
        self.checkForFocusByDestination = false
        self.showPickupDropoffMarkers()
    }
    
    
    func showPickupDropoffMarkers()
    {
        if self.locationManagerObjct.destinationLocation == nil{
            return
        }
        self.markerDestinationAddress.position = CLLocationCoordinate2DMake((self.locationManagerObjct.destinationLocation?.coordinate.latitude)!, (self.locationManagerObjct.destinationLocation?.coordinate.longitude)!)
        self.markerDestinationAddress.icon = UIImage(named: "drop-pin")
        self.markerDestinationAddress.map = self.mapView
        
        if let _ = self.locationManagerObjct.pickUpLocation {
            self.markerPickUpAddress!.position = CLLocationCoordinate2DMake((self.locationManagerObjct.pickUpLocation?.coordinate.latitude)!, (self.locationManagerObjct.pickUpLocation?.coordinate.longitude)!)
            self.mapCenterPinImage.isHidden = true
            self.markerPickUpAddress!.icon = UIImage(named: "pick-pin")
            self.markerPickUpAddress!.map = self.mapView
        }
        
        DispatchQueue.main.async(execute: { [weak self]() -> Void in
            self?.addOverlayToMapView(pickupLoc: (self?.locationManagerObjct.pickUpLocation)!, dropOff: (self?.locationManagerObjct.destinationLocation)!)
            self?.focusMapOnCurrentLocationWith((self?.markerDestinationAddress.position.latitude)!, longitude: (self?.markerDestinationAddress.position.longitude)!)

        })
    }
    // MARK:- ===== update Location and Zoom ========
    func focusMapOnCurrentLocationWith(_ latitude:Double, longitude:Double){
        //weak var weakSelf = self
        let locationFocus = CLLocation(latitude: Double(latitude), longitude: Double(longitude))
        // self.mapBounds = nil
        var mapBounds:GMSCoordinateBounds = GMSCoordinateBounds(coordinate:locationFocus.coordinate , coordinate: locationFocus.coordinate)
        let boundsSource = GMSCoordinateBounds(coordinate:locationManagerObjct.pickUpLocation!.coordinate, coordinate: locationManagerObjct.pickUpLocation!.coordinate)
        if let _ = self.carLocationNearest {
            if let boundsCar = GMSCoordinateBounds(coordinate:self.carLocationNearest!.coordinate, coordinate: self.carLocationNearest!.coordinate) as? GMSCoordinateBounds{
                mapBounds = mapBounds.includingBounds(boundsCar)
            }
        }
        mapBounds = mapBounds.includingBounds(boundsSource)
        DispatchQueue.main.async { () -> Void in
            UIView.animate(withDuration: 0.20, animations: { [weak self]() -> Void in
                if self?.viewNearbyFavDrivers.isHidden == false{
                    self?.mapView.animate(to: (self?.mapView.camera(for: mapBounds, insets:UIEdgeInsetsMake(90.0, 40.0, 40.0, 40.0)))!)
                }else{
                    self?.mapView.animate(to: (self?.mapView.camera(for: mapBounds, insets:UIEdgeInsetsMake(40.0, 40.0, 40.0, 40.0)))!)
                }
            })
        }
    }
    // MARK: ===== Estimantion Fair Between source and Destination ========
    func calulateEstTotalFairBtSourceDestination(){
        //        if locationManagerObjct.destinationLocation == nil {
        //            return
        //        }
        for key in  (self.arrTotalCarsKeys) {
            let dicCarArray = (self.dicOnDemandCarsAvailable[key])! as [[String:Any]]
            var maxAmt:Float = 0.0
            var minAmt:Float = 0.0
            for index in 0..<dicCarArray.count
            {
                let valuesDict = dicCarArray[index]
                if let strFareEstimated =  valuesDict["grandTotal"]{
                    let estFare =  "\(strFareEstimated)".replacingOccurrences(of: ",", with: "").toFloat()
                    //String(describing: strFareEstimated).toFloat()
                    if index == 0{
                        self.dicArrivalTimeOfCarType[key + "fare"] = String(format: "%.2f", estFare) as Any?
                        minAmt = estFare
                        maxAmt = estFare
                    }
                    else {
                        if estFare > maxAmt{
                            maxAmt = estFare
                        }
                        if estFare < minAmt{
                            minAmt = estFare
                        }
                    }
                }
            }
            if maxAmt > maxFareAmount{
                maxFareAmount = maxAmt
            }
            
            self.dicArrivalTimeOfCarType[key + "preAuthAmt"] = String(format: "%.2f", maxAmt) as Any?
            self.dicArrivalTimeOfCarType[key + "maxAmt"] = String(format: "%.2f", maxAmt) as Any?
            self.dicArrivalTimeOfCarType[key + "minAmt"] = String(format: "%.2f", minAmt) as Any?
            self.dicArrivalTimeOfCarType[key + "noOfFleets"] = String(format: "%d", dicCarArray.count) as Any?
            
        }
        DispatchQueue.main.async(execute: { [weak self]() -> Void in
             if Constants.ondemandCollectionView == "1"{
            self?.collectionOndemandCars.reloadData()
             }else {
                self?.tableViewCars.reloadData()
            }

        })
    }
    // MARK:- ===== Webcall to Check OnDemand cars Avaialble ========
    func refreshMapViewForCheckOnDemandCars()
    {
        if appDelegate.networkAvialable == true {
            if  let  newLocation = self.locationManagerObjct.pickUpLocation{
                
                // weak var weakSelf  = self
                if dicActiveUser == nil{
                    return
                }
                else if newLocation.coordinate.latitude == 0.0 || newLocation.coordinate.longitude == 0.0{
                    return
                }
                var dicParameters = [String : Any]()
                
                if carType_Title != ""{
                    dicParameters["vehicle_type"] = carType_Title as Any?
                }
                dicParameters["user_id"] = dicActiveUser!["id"]
                dicParameters["session"] = dicActiveUser!["session"]
                dicParameters["latitude"] = "\(newLocation.coordinate.latitude)" as Any?
                dicParameters["longitude"] = "\(newLocation.coordinate.longitude)" as Any?
                
                dicParameters["on_demand_radius"] = appDelegate.ondemandRadius as Any?
                dicParameters["pickup_date_time"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any? //"2016-05-05  19:49:12"
                dicParameters["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?
                
                dicParameters["company_id"] = Constants.CompanyID as Any?
                
                if  let  newLocation = self.locationManagerObjct.destinationLocation{
                    
                    dicParameters["dropoff_latitude"] = "\(newLocation.coordinate.latitude)" as Any?
                    dicParameters["dropoff_longitude"] = "\(newLocation.coordinate.longitude)" as Any?
                }
                else {
                    dicParameters["dropoff_latitude"] = "" as Any?
                    dicParameters["dropoff_longitude"] = "" as Any?
                }
                
                
                if self.filterElementsData.count != 0{
                    dicParameters["rating_filter"] = self.filterElementsData["rating"]//"3,4,2"
                    dicParameters["price_filter"] = self.filterElementsData["price_range"]//"12,200"
                    
                    if let radius = self.filterElementsData["radius"] as? String{
                        if !radius.isEmpty{
                            dicParameters["on_demand_radius"] = radius
                        }
                    }
                    
                }
                DispatchQueue.main.async(execute: { [weak self]() -> Void in

                if Constants.ondemandCollectionView == "1"{
                    self?.collectionOndemandCars.isUserInteractionEnabled = false
                }else {
                    self?.tableViewCars.isUserInteractionEnabled = false
                    (self?.viewHeaderTableView.viewWithTag(13) as! UIActivityIndicatorView).startAnimating()
                }
                })
                let url = Constants.baseUrl + Constants.getNearestCarsNew
                let URL = Foundation.URL(string:url)!
                let mutableURLRequest = NSMutableURLRequest(url: URL)
                mutableURLRequest.httpMethod = "POST"
                
                // let parameters = ["foo": "bar"]
                
                do {
                    mutableURLRequest.httpBody = try JSONSerialization.data(withJSONObject: dicParameters, options: JSONSerialization.WritingOptions())
                    mutableURLRequest.timeoutInterval = 45
                } catch {
                    print("Error--- \(error)")
                    return
                }
                NSLog("%@", url)
                
                mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                let jsonString : String!
                var jsonData : Data! = Data()
                do {
                    jsonData = try JSONSerialization.data(withJSONObject: dicParameters, options: JSONSerialization.WritingOptions.prettyPrinted)
                    jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
                    print(" \nprint data dic of webservice hit--- \(jsonString)")
                }catch{
                    print("Error--- \(error)")
                    return
                }
                
                
                //   Alamofire.request(mutableURLRequest).validate().responseJSON {[weak self] (Response) -> Void in
                Alamofire.request(URL, method: .post, parameters: dicParameters, encoding:JSONEncoding.default, headers: nil).responseJSON { (Response) in
                    
                    //                    if Response.result{
                    if let  jsonDataRecieved = Response.data{
                        do {
                            let  dic = try JSONSerialization.jsonObject(with: jsonDataRecieved, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:Any]//.jsonObject(with: Response.result.value!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                            if dic["status"] as! String == "session_expired"{
                                let navigation = appDelegate.window?.rootViewController as? UINavigationController
                                if let _ = navigation {
                                    navigation!.popToRootViewController(animated: true)
                                }
                                appDelegate.hideProgress()
                                Globals.logout()
                            }
                            if dic["status"] as! String == "success" {
                                
                                
                                let arrKeys = dic.keys
                                if  !(arrKeys.contains("message"))  {
                                    if let carsDetails = dic.formatDictionaryForNullValues(dic as [String : Any]){
                                        self.showCarsOnMap(carsDetails)
                                        self.showFavouriteDriverIfAny()
                                        self.calulateEstTotalFairBtSourceDestination()
                                    }
                                    else {
                                        self.showErrorMessage(dic["message"] as! String)
                                    }
                                } else {
                                    self.showErrorMessage(dic["message"] as! String)
                                }
                            }
                            else if dic["status"] as! String == "payment_pending" {
                                /*  {
                                 "status": "payment_pending",
                                 "message": "Pending Payment",
                                 "records": [
                                 {
                                 "reservation_id": "6812",
                                 "company_id": "40",
                                 "grand_total": "70.00",
                                 "conf_id": "-10-",
                                 "pu_date": "2016-09-26",
                                 "pu_time": "14:39:00",
                                 "pickup": "Unnamed Road, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 140308, India",
                                 "dropoff": ""
                                 }
                                 ]
                                 }*/
                                if let dicRecords = dic["records"] as? [[String : Any]]
                                {
                                    if !((self.navigationController?.topViewController?.isKind(of: PADuePaymentsViewController.self))!){
                                        self.stopRefreshCarListTimer()
                                        let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "PADuePaymentsViewController") as! PADuePaymentsViewController
                                        detailsVc.arrUnpaidReservations = dicRecords
                                        self.navigationController?.pushViewController(detailsVc, animated: false)
                                    }
                                }
                            }
                            
                        } catch {
                            print(error)
                        }
                        
                        /* let jsonDicRecieved = JSON(data:jsonDataRecieved )
                         if let dic = jsonDicRecieved.dictionaryObject {
                         if dic["status"] as! String == "session_expired"{
                         let navigation = appDelegate.window?.rootViewController as? UINavigationController
                         if let _ = navigation {
                         navigation!.popToRootViewController(animated: true)
                         }
                         appDelegate.hideProgress()
                         Globals.logout()
                         }
                         print("cars avaible near ---\(dic)")
                         if dic["status"] as! String == "success" {
                         
                         
                         let arrKeys = dic.keys
                         if  !(arrKeys.contains("message"))  {
                         if let carsDetails = dic.formatDictionaryForNullValues(dic as [String : Any]){
                         self.showCarsOnMap(carsDetails)
                         self.showFavouriteDriverIfAny()
                         self.calulateEstTotalFairBtSourceDestination()
                         }
                         else {
                         self.showErrorMessage(dic["message"] as! String)
                         }
                         } else {
                         self.showErrorMessage(dic["message"] as! String)
                         }
                         }
                         else if dic["status"] as! String == "payment_pending" {
                         /*  {
                         "status": "payment_pending",
                         "message": "Pending Payment",
                         "records": [
                         {
                         "reservation_id": "6812",
                         "company_id": "40",
                         "grand_total": "70.00",
                         "conf_id": "-10-",
                         "pu_date": "2016-09-26",
                         "pu_time": "14:39:00",
                         "pickup": "Unnamed Road, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 140308, India",
                         "dropoff": ""
                         }
                         ]
                         }*/
                         if let dicRecords = dic["records"] as? [[String : Any]]
                         {
                         if !((self.navigationController?.topViewController?.isKind(of: PADuePaymentsViewController.self))!){
                         self.stopRefreshCarListTimer()
                         let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "PADuePaymentsViewController") as! PADuePaymentsViewController
                         detailsVc.arrUnpaidReservations = dicRecords
                         self.navigationController?.pushViewController(detailsVc, animated: false)
                         }
                         }
                         }
                         }*/
                        // else {
                        //  self.showErrorMessage("Error")
                        //     }
                    }else {
                        let error = Response.result.error
                        print("error near by cars -- \(Response.result.error)")
                        self.showErrorMessage("Network Error")
                    }
                }
            }
        }
    }
    
    func showErrorMessage(_ message:String) {
        DispatchQueue.main.async {
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)
            self.viewNearbyFavDrivers.isHidden = true
            CATransaction.commit()
            self.noCarsAvailbleShowToTable(message)
        }
    }
    
    
    func showFavouriteDriverIfAny()  {
        
        nearByFavDrivers.removeAll()
        
        if self.arrVehicleInfo.count>0{
            for index in 0..<self.arrVehicleInfo.count
            {
                let dicVehicleType = self.arrVehicleInfo[index]
                
                if let arrOnDemandCars = self.dicOnDemandCarsAvailable[String(describing: dicVehicleType["vehicle_type_title"]!)]
                {
                    let temp:[[String:Any]]! = arrOnDemandCars.filter({ (car:[String:Any]) -> Bool in
                        if let distace = car["fav_driver"] as? String
                        {
                            if distace == "1" {
                                return true
                            }
                            else {
                                return false
                            }
                        }
                        return false
                    })
                    nearByFavDrivers.append(contentsOf: temp)
                }
                
                if nearByFavDrivers.count > 0{
                    
                    (self.viewNearbyFavDrivers.viewWithTag(1000))!.alpha = 1.0
                    (self.viewNearbyFavDrivers.viewWithTag(1001))!.alpha = 1.0
                    
                    
                    DispatchQueue.main.async {
                        self.viewNearbyFavDrivers.isHidden = false
                        
                        UIView.animate (withDuration: 1.0, delay: 0.0, options:[.autoreverse, .repeat, .allowUserInteraction] ,animations: {
                            (self.viewNearbyFavDrivers.viewWithTag(1000))?.alpha = 0.1
                            (self.viewNearbyFavDrivers.viewWithTag(1001))?.alpha = 0.1
                            
                        }, completion: { _ in
                        })
                    }
                    
                }
                else {
                    DispatchQueue.main.async {
                        CATransaction.begin()
                        CATransaction.setAnimationDuration(1.0)
                        self.viewNearbyFavDrivers.isHidden = true
                        CATransaction.commit()
                    }
                }
            }
        }
    }
    
    @IBAction func showMyFavDrivers(_ sender: Any) {
        
        if nearByFavDrivers.count > 0 {
            let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "PAOnDemandFavDriversViewController") as! PAOnDemandFavDriversViewController
            detailsVc.favDriversArray = nearByFavDrivers
            
            detailsVc.didFinishSelectingDrivers = {[weak self] (selectedDrivers:[[String:Any]]) -> () in
                
                var preAuthAmount:String = ""
                
                // if self?.locationManagerObjct.destinationLocation != nil{
                var amt:Float = 0.0
                for index in 0..<selectedDrivers.count
                {
                    let valuesDict = selectedDrivers[index]
                    
                    if let strFareEstimated =  valuesDict["grandTotal"]{
                        let estFare =  "\(strFareEstimated)".replacingOccurrences(of: ",", with: "").toFloat()
                        if estFare > amt{
                            amt = estFare
                        }
                    }
                }
                preAuthAmount =  "\(amt)"
                
                // }
                
                if preAuthAmount.isEmpty || preAuthAmount.toFloat() == 0.00{
                    //     JLToast.makeText("Fare is not available for this vehicle.", duration: JLToastDelay.ShortDelay).show()
                    
                }
                else {
                    let payment = self?.storyboard?.instantiateViewController(withIdentifier: "PAPaymentForOnDemandVC") as! PAPaymentForOnDemandVC
                    payment.arrOndemanCarsSelect = selectedDrivers
                    payment.isJobRequest = true
                    payment.strPreAuthAmountForJob = preAuthAmount
                    self?.navigationController?.pushViewController(payment, animated: true)
                }
            }
            
            detailsVc.view.backgroundColor = UIColor.clear
            detailsVc.modalPresentationStyle = .overFullScreen
            detailsVc.providesPresentationContextTransitionStyle = true
            detailsVc.definesPresentationContext = true
            self.providesPresentationContextTransitionStyle = true
            self.definesPresentationContext = true
            self.modalTransitionStyle = .crossDissolve
            self.modalPresentationStyle = .overFullScreen
            self.present(detailsVc, animated: true) {}
        }
        else {
            Globals.ToastAlertWithString("Please wait...")
        }
    }
    // MARK:- ===update table and map when no cars available  =====
    
    func noCarsAvailbleShowToTable(_ strMessage:String){
        self.carLocationNearest = nil

        for keyCar in self.arrTotalCarsKeys {
            self.dicArrivalTimeOfCarType[keyCar] = "Loading..." as Any?
        }
        if Constants.ondemandCollectionView == "1"{
            self.collectionOndemandCars.isUserInteractionEnabled = true
        }else {
            self.tableViewCars.isUserInteractionEnabled = true
            (self.viewHeaderTableView.viewWithTag(13) as! UIActivityIndicatorView).stopAnimating()
            self.tableViewCars.reloadData()
        }
        self.mapView.clear();
        
        self.showTableViewForCars(false, withHeight: 0.0)
        Globals.ToastAlertWithString(strMessage)
    }
    // MARK:- ===update table and map when cars available  =====
    
    
    func reloadMapWithIcons(){
        
        self.mapView.clear()
        if self.locationManagerObjct.pickUpLocation != nil{
            self.mapCenterPinImage.isHidden = false
        }
        if self.locationManagerObjct.dicDestinationLocation!.count != 0 {
            self.showDestinationAddressOnMap(self.locationManagerObjct.dicDestinationLocation!)
        }
        
//        var mapBounds:GMSCoordinateBounds = GMSCoordinateBounds(coordinate: self.locationManagerObjct.pickUpLocation!.coordinate, coordinate: self.locationManagerObjct.pickUpLocation!.coordinate)
//
//
//        if let _ = self.locationManagerObjct.carNearestLocation {
//            if self.locationManagerObjct.carNearestLocation!.coordinate.latitude != 0.0 ||  self.locationManagerObjct.carNearestLocation!.coordinate.longitude != 0.0{
//                let carBounds = GMSCoordinateBounds(coordinate: (self.locationManagerObjct.carNearestLocation?.coordinate)!, coordinate: (self.locationManagerObjct.carNearestLocation?.coordinate)!)
//                if  mapBounds == nil{
//                    mapBounds = mapBounds.includingBounds(carBounds)
//                }else {
//                    mapBounds = GMSCoordinateBounds(coordinate: (self.locationManagerObjct.carNearestLocation?.coordinate)!, coordinate: (self.locationManagerObjct.carNearestLocation?.coordinate)!)
//                }
//            }
//        }
//
//        if let _ = self.locationManagerObjct.destinationLocation {
//            if self.locationManagerObjct.destinationLocation!.coordinate.latitude != 0.0 ||  self.locationManagerObjct.destinationLocation!.coordinate.longitude != 0.0{
//                let carBounds = GMSCoordinateBounds(coordinate: (self.locationManagerObjct.destinationLocation?.coordinate)!, coordinate: (self.locationManagerObjct.destinationLocation?.coordinate)!)
//                if  mapBounds == nil{
//                    mapBounds = mapBounds.includingBounds(carBounds)
//                }else {
//                    mapBounds = GMSCoordinateBounds(coordinate: (self.locationManagerObjct.destinationLocation?.coordinate)!, coordinate: (self.locationManagerObjct.destinationLocation?.coordinate)!)
//                }
//            }
//        }
//        DispatchQueue.main.async() {[weak self] () -> Void in
//            self?.mapView.animate(to: (self?.mapView.camera(for: mapBounds, insets: UIEdgeInsetsMake(100.0, 50.0, 50, 50.0)))!)
 //       }
        
    }
    func showCarsOnMap(_ dicRecieved:[String: Any]){
        
        var arrKeys = [String]()
        for keys in dicRecieved.keys{
            arrKeys.append(keys)
        }
        if arrKeys.contains("message"){
            (self.viewHeaderTableView.viewWithTag(13) as! UIActivityIndicatorView).stopAnimating()
            self.showTableViewForCars(false, withHeight: 0.0)
            self.tableViewCars.isUserInteractionEnabled = true
            return
        }
        var flagNeedToRefreshMap = false

       if self.locationManagerObjct.destinationLocation != nil {
        if self.locationManagerObjct.destinationLocation?.coordinate.latitude != self.dropoff_loc.coordinate.latitude || self.locationManagerObjct.destinationLocation?.coordinate.longitude != self.dropoff_loc.coordinate.longitude{
            self.dropoff_loc = self.locationManagerObjct.destinationLocation!
                flagNeedToRefreshMap = true
        }
        }
        if self.locationManagerObjct.pickUpLocation?.coordinate.latitude != self.pickup_loc.coordinate.latitude || self.locationManagerObjct.pickUpLocation?.coordinate.longitude != self.pickup_loc.coordinate.longitude{
            self.pickup_loc = self.locationManagerObjct.pickUpLocation!
                flagNeedToRefreshMap = true
        }
        self.dicOnDemandCarsAvailable.removeAll()
        self.arrTotalCarsKeys.removeAll()
        self.arrVehicleInfo.removeAll()
        self.arrVehicleInfo = ((dicRecieved["records"]! as! [String:Any]) ["vehicle_types"] as! [[String : Any]])
        
        for dic in self.arrVehicleInfo {
            self.arrTotalCarsKeys.append(dic["vehicle_type_title"] as! String)
        }
        if self.arrVehicleInfo.count != 0 {
            if Constants.ondemandCollectionView == "1"{
                self.tableViewCars.reloadData()
            }else{
                self.collectionOndemandCars.reloadData()
            }
            let heightTable = CGFloat(100) + CGFloat(80 * self.arrTotalCarsKeys.count)
            self.showTableViewForCars(true, withHeight: heightTable)
        }
        
        //  print("First Lop \(firstLoop)")
        let dicRecords = dicRecieved["records"] as! [String : Any]
        var flag = true
          var arrAllNearbyCarsData = [[String: Any]]()
        for key in self.arrTotalCarsKeys {
            var arrTypeCar = [[String : Any]]()
            arrTypeCar = dicRecords[key] as! [[String : Any]]
            for dicRecords  in arrTypeCar{
                let marker = GMSMarker()
                var latitude: Double = 0.0
                var longitude: Double = 0.0
                
                if let lat = dicRecords["latitude"] as? String{
                    if !lat.isEmpty{
                        latitude = Double(lat)!
                    }
                }
                if let lng = dicRecords["longitude"] as? String{
                    if !lng.isEmpty{
                        longitude = Double(lng)!
                    }
                }
                if latitude != 0 && longitude != 0{
                    let postion = CLLocationCoordinate2DMake(latitude, longitude)
                    marker.position = postion
                    marker.isTappable = false
                    marker.title = "car"
                    DLImageLoader.sharedInstance().image(fromUrl: dicRecords["map_icon"]! as! String , completed: { (error, image) -> () in
                        if image != nil {
                            let size = CGSize(width: 40 , height: 40)
                            UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
                            image?.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
                            let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
                            UIGraphicsEndImageContext()
                            marker.icon = newImage
                            
                        }else {
                            marker.icon = UIImage(named: "sedan")
                        }
                    })
                    DispatchQueue.main.async(execute: { [weak self] () -> Void in
                        marker.map = self?.mapView
                    })
                    arrAllNearbyCarsData.append(dicRecords)
                }
            }
            arrTypeCar.sort(by: { (s1 : [String : Any]!, s2: [String : Any]!) -> Bool in
                CDouble(s1["distance"] as! String) <  CDouble(s2["distance"] as! String)
            })
            
            self.getNearestCarDistance(arrTypeCar.first!, calculateNearestCarData: flag)
            flag = false

            self.dicOnDemandCarsAvailable[key] = arrTypeCar
            arrTypeCar.removeAll()
        }
        
        
        if self.onDemand_carInformationArray.count != arrAllNearbyCarsData.count{
            self.onDemand_carInformationArray.removeAll()
            self.onDemand_carInformationArray = arrAllNearbyCarsData
            flagNeedToRefreshMap = true
        }
        self.onDemand_carInformationArray.sort(by: { (s1 : [String : Any]!, s2: [String : Any]!) -> Bool in
            CDouble(s1["distance"] as! String) <  CDouble(s2["distance"] as! String)
        })
       
   
        
        //if flagNeedToRefreshMap{
            self.reloadMapWithIcons()
        //}
        
        
        self.mapView.mar
    }
    
    func getNearestCarDistance(_ carsObject:[String:Any], calculateNearestCarData:Bool){
        
        if self.onDemand_carInformationArray.count != 0 {
            //weak var weakSelf = self
            //  self.vehicleTypeID = (carsObject.vehicle_type_id)
            let destiLoc : CLLocation = CLLocation(latitude:CDouble(carsObject["latitude"] as! String)!
                ,longitude: CDouble(carsObject["longitude"] as! String)!)
            
            if calculateNearestCarData {
                self.locationManagerObjct.carNearestLocation = destiLoc
                self.carLocationNearest = destiLoc
            }
            let urlStr = Globals.sharedInstance.getFullApiAddressForDistanceToDriverDistance(self.locationManagerObjct.pickUpLocation, destinationLoc: destiLoc)
            
            if urlStr == "" {
                return
            }
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).sync(execute: { () -> Void in
                ServiceManager.sharedInstance.dataTaskWithGetRequest(urlStr, successBlock: {[weak self] (dicData) -> Void in
                    
                    self?.getNearCarsArrivalTimeInfo(dicData as [String:Any], carObject: carsObject, calculateNearestCarData:calculateNearestCarData)
                    }, failureBlock: { (error) -> () in
                        (UIApplication.shared.delegate! as! AppDelegate).hideProgress()
                        print("get near car distance error \(error?.description)")
                })
            })
        }
        
    }
    func getNearCarsArrivalTimeInfo(_ dicCarsTimeInfo: [String:Any], carObject: [String:Any] , calculateNearestCarData:Bool) {
        
        let arrRoutes = dicCarsTimeInfo["routes"] as! [[String : Any]]
        if arrRoutes.count > 0 {
            
            let dicMainRoutes = arrRoutes.first!
            let arrLegs = dicMainRoutes["legs"] as! [[String:Any]]
            let dicDuration = arrLegs.first!["duration"] as! [String:Any]
            //   weak var weakSelf = self
            var strTimeArrival = dicDuration["text"] as! String
            
            if strTimeArrival.contains("hours") {
                
                strTimeArrival = strTimeArrival.replacingOccurrences(of: "hours", with: "h")
                strTimeArrival = strTimeArrival.replacingOccurrences(of: "mins", with: "m")
                strTimeArrival = strTimeArrival.replacingOccurrences(of: "min", with: "m")
                
                if calculateNearestCarData {
                    self.showEtaMethodsOnMapWith(strTimeArrival, imageString: "mappin")
                }

            }
            else if strTimeArrival.contains("hour") {
                strTimeArrival = strTimeArrival.replacingOccurrences(of: "hour", with: "h")
                strTimeArrival = strTimeArrival.replacingOccurrences(of: "mins", with: "m")
                strTimeArrival = strTimeArrival.replacingOccurrences(of: "min", with: "m")
                if calculateNearestCarData {
                    self.showEtaMethodsOnMapWith(strTimeArrival, imageString: "mappin")
                }

            }
            else{
                if calculateNearestCarData {
                let arr = strTimeArrival.components(separatedBy: " ")
                if arr.count == 2{
                    let val = "\(arr[0])".toInteger()
                    self.showEtaMethodsOnMapWith("\(val)-\(val + 5) \(arr[1])", imageString: "mappin")

                }else {
                    self.showEtaMethodsOnMapWith(strTimeArrival, imageString: "mappin")
                }
            }
            }
            (UIApplication.shared.delegate! as! AppDelegate).hideProgress()
            self.dicArrivalTimeOfCarType[carObject["mapTypeVehicle"] as! String] = strTimeArrival as Any?
           
             if Constants.ondemandCollectionView == "1"{
                self.collectionOndemandCars.isUserInteractionEnabled = true
                DispatchQueue.main.async(execute: {[weak self]() -> Void in
                    self?.collectionOndemandCars.reloadData()
                    self?.collectionOndemandCars.selectItem(at: self?.indexPathCellSelected, animated: false, scrollPosition: UICollectionViewScrollPosition.left)
                    self?.collectionOndemandCars.reloadData()
                })
            }
            else {
            self.tableViewCars.isUserInteractionEnabled = true
            (self.viewHeaderTableView.viewWithTag(13) as! UIActivityIndicatorView).stopAnimating()
            DispatchQueue.main.async(execute: {[weak self]() -> Void in
                self?.tableViewCars.reloadData()
                self?.tableViewCars.selectRow(at: self?.indexPathCellSelected, animated: false, scrollPosition:.none)
                self?.collectionOndemandCars.reloadData()
            })
            }
        }
    }
    
    
    func showEtaMethodsOnMapWith(_ etaString:String, imageString:String){
        
        let viewBack = UIView(frame: CGRect(x: 0,y: 0,width: 60,height: 60))
        viewBack.backgroundColor = UIColor.clear
        let pinImageView : UIImageView = UIImageView(image: UIImage(named: imageString))
        pinImageView.contentMode = .scaleAspectFill
        pinImageView.frame = viewBack.frame
        self.etaLblShowTime = UILabel(frame: CGRect(x: 5,y: 5,width: 50,height: 30))
        self.etaLblShowTime.textAlignment = .center
        self.etaLblShowTime.numberOfLines = 2
        self.etaLblShowTime.text = etaString
        // self.etaLblShowTime.backgroundColor = UIColor.brownColor()
        self.etaLblShowTime.textColor = UIColor.white
        self.etaLblShowTime.font = UIFont.systemFont(ofSize: 10)
        viewBack.addSubview(pinImageView)
        viewBack.addSubview(self.etaLblShowTime)
        self.mapCenterPinImage.center = CGPoint(x: self.mapView.center.x, y: self.mapView.center.y - 35)

        // viewBack.bringSubviewToFront(self.etaLblShowTime)
        if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
            UIGraphicsBeginImageContextWithOptions(viewBack.frame.size, false, UIScreen.main.scale)
        }else {
            UIGraphicsBeginImageContext(viewBack.frame.size)
        }
        viewBack.layer.render(in: UIGraphicsGetCurrentContext()!)
        self.mapCenterPinImage.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        // return self.finalImage
    }
    func showTableViewForCars(_ show:Bool, withHeight:CGFloat){
        
        DispatchQueue.main.async {[weak self] () -> Void in
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                if show == true {
                    if Constants.needForHotel == "1"{
                        self?.viewHeightConstraint.constant = withHeight > 169 ? 196 : withHeight
                    }else {
                        if Constants.ondemandCollectionView == "1"{
                            self?.viewHeightConstraint.constant = withHeight > 196 ? 196 : withHeight
                        }else {
                            self?.viewHeightConstraint.constant = withHeight > 230 ? 230 : withHeight
                        }
                    }
                }else {
                    self?.viewHeightConstraint.constant = 0.0
                }
                self?.view.layoutIfNeeded()
            })
        }
    }
    func showSelectdTypeOfCarOnMap(_ arrCarObjectS:[OnDemandCarsAvailable]){
        self.mapView.clear()
        // show destination address on Map
        if locationManagerObjct.dicDestinationLocation!.count != 0 {
            self.showDestinationAddressOnMap(locationManagerObjct.dicDestinationLocation!)
            
        }
        for carsObjct in arrCarObjectS{
            let marker = GMSMarker()
            let latitude: Double = CDouble(carsObjct.latitude)!
            let longitude: Double = CDouble(carsObjct.longitude)!
            let postion = CLLocationCoordinate2DMake(latitude, longitude)
            marker.position = postion
            let imageData : Data? = Globals.sharedInstance.getValueFromUserDefaultsForKey(carsObjct.vehicle_type_id) as? Data
            if imageData != nil {
                image_DataSedan = imageData!
            }else {
                if let  _ = Data().vehicleImageForType(carsObjct.vehicle_type_id) {
                    self.image_DataSedan = Data().vehicleImageForType(carsObjct.vehicle_type_id)!
                }
            }
            if let imageCar = UIImage(data: image_DataSedan!, scale: 1.5) {
                marker.icon = imageCar
            }else {
                marker.icon = UIImage(named: "sedan")
                
            }
            
            marker.userData = carsObjct
            marker.map = self.mapView
        }
    }
    func getHeightFromString(withText  Text: String) -> CGFloat {
        let strAddress: NSString = Text as NSString
        let str : NSString = strAddress as NSString //.//trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) as NSString
        // let str = self.offerDescModel!.description!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let constraint: CGSize =  CGSize(width: ScreenSize.SCREEN_WIDTH - 20, height: 1500.0)
        //cCGSizeMake(CGRectGetWidth(tableView.frame), 1500.0)
        let boundingBox: CGSize = str.boundingRect(with: constraint, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName:Globals.defaultAppFont(17)], context: nil).size
        return CGFloat(boundingBox.height)
    }
    

    // MARK:- ========CollectionView Datasource and  Delegate-=============
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrVehicleInfo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vehicleTypeCollectionCell", for: indexPath) as? vehicleSelectionCollectionViewCell
        {
            let dicVehicle = self.arrVehicleInfo[indexPath.row]
            let vehicleName = String(describing: dicVehicle["vehicle_type_title"]!)
           
            if self.indexPathCellSelected != nil && self.indexPathCellSelected == indexPath{
                cell.selectCell(flag: true)
            }
            else {
                cell.selectCell(flag: false)
            }
           // if cell.isSelected {
                if appDelegate.networkAvialable == true {
                    if let keyForImage = dicVehicle["active_icon"]{
                        DLImageLoader.sharedInstance().image(fromUrl: keyForImage as! String, completed: { (error, image) -> () in
                            if image != nil {
                                cell.imageCarIcon.image = image
                            }else {
                                cell.imageCarIcon.image = UIImage(named: "Defaultcar_Selectec")
                            }
                        })
                    }
                    else {
                        cell.imageCarIcon.image = UIImage(named: "Defaultcar_Selectec")
                    }
                }
           // }
//            else {
//                if appDelegate.networkAvialable == true {
//                    if let keyForImage = dicVehicle["inactive_icon"]{
//                        DLImageLoader.sharedInstance().image(fromUrl: keyForImage as! String, completed: { (error, image) -> () in
//                            if image != nil {
//                                cell.imageCarIcon.image = image
//                            }else {
//                                cell.imageCarIcon.image = UIImage(named: "Defaultcar")
//                            }
//                        })
//                    }
//                    else {
//                        cell.imageCarIcon.image = UIImage(named: "Defaultcar")
//                    }
//                }
//            }
            if let strArivalTime = self.dicArrivalTimeOfCarType[vehicleName] as? String{
                cell.activityIndicator.isHidden = true
                cell.activityIndicator.stopAnimating()
                cell.lblETA.text! = strArivalTime
                cell.lblETA.isHidden = false
            }else {
                cell.lblETA.isHidden = true
                cell.activityIndicator.isHidden = false
                cell.activityIndicator.startAnimating()
            }
            
        if let arrOnDemandCars = self.dicOnDemandCarsAvailable[vehicleName] {
            let onDemandCarObjct = arrOnDemandCars.first!
            if let noOfFleets =  self.dicArrivalTimeOfCarType["\(vehicleName)" + "noOfFleets"]{
                cell.lblVehicleType.text = "\(vehicleName)"
                cell.lblNoFleets.text = "\(noOfFleets)" == "1" ? "1 Fleet" : "\(noOfFleets) Fleets"
            }
            if let seat_capacity = onDemandCarObjct["max_seat_available"]
            {
                cell.lblPax.text = String(format: "%@ Pax", "\(String(describing: seat_capacity))" )
            }
            if let lug_capacity = onDemandCarObjct["luggage_capacity"]
            {
                cell.lblLugg.text = String(format: "%@ lugg", "\(String(describing: lug_capacity))" )
            }
            
//            if let company_name = onDemandCarObjct["company_name"]{
//                cell.lblCompanyName.text = "\(company_name)"
//            }
            if let strFareEstimated =  onDemandCarObjct["grandTotal"]{
                cell.lblTotalFare.adjustsFontSizeToFitWidth = true
            let strFareEstimated =  "\(strFareEstimated)".replacingOccurrences(of: ",", with: "").toFloat()
                if let currency = onDemandCarObjct["currency"] as? String{
                    if !currency.isEmpty{

                    if let maxAmt =  self.dicArrivalTimeOfCarType["\(vehicleName)" + "maxAmt"], let minAmt =  self.dicArrivalTimeOfCarType["\(vehicleName)" + "minAmt"]{
                            cell.lblTotalFare.text! =  ("\(minAmt)" != "\(maxAmt)") ? ("\(currency)\(minAmt) - \(currency)\(maxAmt)") : "\(currency)\(minAmt)"
                    }else {
                    cell.lblTotalFare.text! =  String(format: "%.2f", strFareEstimated)
                    }
                    }else{
                cell.lblTotalFare.text! =  String(format: "\(currency) %.2f", strFareEstimated)
                }
                }
            }
            else {
                cell.lblTotalFare.text! = ""
            }
        }
            return cell
            
        }
        return UICollectionViewCell()
        // cell.setData(dicVehicle)
        
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if let cell = collectionView.cellForItem(at: indexPath) as? vehicleSelectionCollectionViewCell{
            cell.selectCell(flag: true)
            if Constants.needForHotel == "1"{
                self.bookCarForOnDemandForETS(selectedIndex: indexPath)
                //self.bookCarForOnDemand(selectedIndex: indexPath)
                
            }
            else{
                self.bookCarForOnDemand(selectedIndex: indexPath)
            }
            
        }
    }
  
    // MARK:- ========table View data source and  Delegate-=============
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrVehicleInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIndetifier = "cellAvailableCars"
        var cell : CarsAvailableCellOnDemand? = self.tableViewCars.dequeueReusableCell(withIdentifier: cellIndetifier, for: indexPath) as? CarsAvailableCellOnDemand
        // var cell : CarsAvailableCellOnDemand? = self.tableViewCars.dequeueReusableCellWithIdentifier(cellIndetifier) as? CarsAvailableCellOnDemand
        if cell == nil {
            cell! = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier) as! CarsAvailableCellOnDemand
        }
        
        let dicVehicle = self.arrVehicleInfo[indexPath.row]
        let vehicleName = String(describing: dicVehicle["vehicle_type_title"]!)
        var vehicleType_Name = ""
        
        if cell!.isSelected {
            
            if appDelegate.networkAvialable == true {
                if let keyForImage = dicVehicle["active_icon"]{
                    DLImageLoader.sharedInstance().image(fromUrl: keyForImage as! String, completed: { (error, image) -> () in
                        if image != nil {
                            cell!.imageCarIcon.image = image
                        }else {
                            cell!.imageCarIcon.image = UIImage(named: "Defaultcar_Selectec")
                        }
                    })
                }
                else {
                    cell!.imageCarIcon.image = UIImage(named: "Defaultcar_Selectec")
                }
                
            }
            
            cell?.SetFontColorOfCellSubViews(cell!, color: UIColor.white)
            cell!.viewCarNamePrize.backgroundColor = UIColor.appThemeColor()
            
            cell!.imageTimeClock.image = UIImage(named: "timeWhite")
            cell!.contentView.backgroundColor = UIColor.appThemeColor()
        }
        else {
            if appDelegate.networkAvialable == true {
                if let keyForImage = dicVehicle["inactive_icon"]{
                    DLImageLoader.sharedInstance().image(fromUrl: keyForImage as! String, completed: { (error, image) -> () in
                        if image != nil {
                            cell!.imageCarIcon.image = image
                        }else {
                            cell!.imageCarIcon.image = UIImage(named: "Defaultcar")
                        }
                    })
                }
                else {
                    cell!.imageCarIcon.image = UIImage(named: "Defaultcar")
                }
                
            }
            
            cell?.SetFontColorOfCellSubViews(cell!, color: UIColor.black)
            cell!.viewCarNamePrize.backgroundColor = UIColor.white
            cell!.imageTimeClock.image = UIImage(named: "time")
            cell!.contentView.backgroundColor = UIColor.white
            
        }
        if let strArivalTime = self.dicArrivalTimeOfCarType[vehicleName] as? String{
            cell!.activityIndicator.isHidden = true
            cell!.activityIndicator.stopAnimating()
            cell!.lblArivalTime.text! = strArivalTime
            cell!.lblArivalTime.isHidden = false
        }else {
            cell!.lblArivalTime.isHidden = true
            cell!.activityIndicator.isHidden = false
            cell!.activityIndicator.startAnimating()
        }
        
        print( self.dicOnDemandCarsAvailable)
        if let arrOnDemandCars = self.dicOnDemandCarsAvailable[vehicleName] {
            let onDemandCarObjct = arrOnDemandCars.first!
            if let maxAmt =  self.dicArrivalTimeOfCarType["\(vehicleName)" + "maxAmt"], let minAmt =  self.dicArrivalTimeOfCarType["\(vehicleName)" + "minAmt"], let noOfFleets =  self.dicArrivalTimeOfCarType["\(vehicleName)" + "noOfFleets"]{
                
                if let currency = onDemandCarObjct["currency"]{
                    let priceRange =  ("\(minAmt)" != "\(maxAmt)") ? ("\(currency)\(minAmt) - \(currency)\(maxAmt)") : "\(currency)\(minAmt)"
                    let num_fleets = "\(noOfFleets)" == "1" ? "(1 Fleet)" : "(\(noOfFleets) Fleets)"
                    cell?.lblCarName.attributedText = "".setAttributedStringWithColorSize("\(vehicleName.uppercased()) \(num_fleets) \n", secondStr: priceRange, firstColor: UIColor.black, secondColor: UIColor.darkGray, font1: Globals.defaultAppFontWithBold(14), font2: Globals.defaultAppFont(12))
                }
            }
            if let seat_capacity = onDemandCarObjct["max_seat_available"]
            {
                cell!.lblMaxSeats.text = String(format: "%@ Pax", "\(String(describing: seat_capacity))" )
            }
            if let strFareEstimated =  onDemandCarObjct["grandTotal"]{
                let strFareEstimated =  "\(strFareEstimated)".replacingOccurrences(of: ",", with: "").toFloat()
                if let currency = onDemandCarObjct["currency"] as? String{
                    if currency.isEmpty{
                        cell!.lblCarPrice.text! =  String(format: "%.2f", strFareEstimated)
                        
                    }else{
                        cell!.lblCarPrice.text! =  String(format: "\(currency) %.2f", strFareEstimated)
                    }
                }                        }
            else {
                cell!.lblCarPrice.text! = ""
            }
        }
        
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.viewHeaderTableView!
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return DeviceType.IS_IPAD ? 60 : 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DeviceType.IS_IPAD ? 100 : 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        // self.tableViewCars.setEditing(true, animated: false)
        
        
        if Constants.needForHotel == "1"{
            self.bookCarForOnDemandForETS(selectedIndex: indexPath)
            //self.bookCarForOnDemand(selectedIndex: indexPath)
            
        }
        else{
            self.bookCarForOnDemand(selectedIndex: indexPath)
        }
    }
    func bookCarForOnDemand(selectedIndex indexPath:IndexPath) {
        
        let dicVehicleType = self.arrVehicleInfo[indexPath.row]
        if let strArivalTime = self.dicArrivalTimeOfCarType[dicVehicleType["vehicle_type_title"]! as! String] as? String
        {
            if strArivalTime != Constants.strNoCars {
              
                if Constants.ondemandCollectionView == "1"{
                    if let cell = collectionOndemandCars.cellForItem(at: indexPath) as? vehicleSelectionCollectionViewCell{
                        cell.selectCell(flag: false)
                    }
                    self.collectionOndemandCars.reloadData()
                    self.collectionOndemandCars.selectItem(at: indexPath, animated: false, scrollPosition: .left)
                }else {
                    if let cell = self.tableViewCars.cellForRow(at: indexPath) as? CarsAvailableCellOnDemand{
                        cell.setSelected(true, animated: false)
                    }
                self.tableViewCars.reloadData()
                self.tableViewCars.selectRow(at: indexPath, animated: false, scrollPosition:.none)
                }
                if let arrOnDemandCars = self.dicOnDemandCarsAvailable[dicVehicleType["vehicle_type_title"]! as! String]
                {
                    self.indexPathCellSelected = indexPath
                    let onDemandCarObjct = arrOnDemandCars.first!
                    // self.showSelectdTypeOfCarOnMap(arrOnDemandCars)
                    self.vehicleTypeID = String(describing: onDemandCarObjct["id"])
                    
                    var preAuthAmount:String = ""
                    if let vehicleType = dicVehicleType["vehicle_type_title"] as? String
                    {
                        if let amt =  self.dicArrivalTimeOfCarType[vehicleType + "preAuthAmt"] as? String{
                            preAuthAmount = amt
                        }
                    }
                    
                    if preAuthAmount.isEmpty || preAuthAmount.toFloat() == 0.00{
                        //JLToast.makeText("Fare is not available for this vehicle.", duration: JLToastDelay.ShortDelay).show()
                        
                    }
                    else {
                        self.locationManagerObjct.strVehicleType = String(describing: dicVehicleType["vehicle_type_title"]!)
                        
                        let payment = self.storyboard?.instantiateViewController(withIdentifier: "PAPaymentForOnDemandVC") as! PAPaymentForOnDemandVC
                        payment.arrOndemanCarsSelect = arrOnDemandCars
                        payment.isJobRequest = true
                        payment.strPreAuthAmountForJob = preAuthAmount
                        self.navigationController?.pushViewController(payment, animated: true)
                    }
                }
                
            }
        }
        else {
            self.dicArrivalTimeOfCarType[dicVehicleType["vehicle_type_title"]! as! String] = Constants.strNoCars as Any?
            if Constants.ondemandCollectionView == "1"{
            }else {
            self.tableViewCars.setEditing(false, animated: false)
            self.tableViewCars.reloadData()
            }
        }
    }
    var pre_authorized_amount:String = ""
    var arr_SelectedCarData  = [[String:Any]]()
    
    @IBAction func btnQuickBookClicked(_ sender: UIButton) {
        
        NSLog("%@", self.onDemand_carInformationArray)
        
        var preAuthAmount:String = ""
        var amt:Float = 0.0
        for index in 0..<self.onDemand_carInformationArray.count
        {
            let valuesDict = self.onDemand_carInformationArray[index]
            
            if let strFareEstimated =  valuesDict["grandTotal"]{
                let estFare =  String(describing: strFareEstimated).toFloat()
                if estFare > amt{
                    amt = estFare
                }
            }
        }
        preAuthAmount =  "\(amt)"
        
        
        if preAuthAmount.isEmpty || preAuthAmount.toFloat() == 0.00{
            Globals.ToastAlertWithString("Fare is not available for this vehicle.")
            
        }
        else {
            self.arr_SelectedCarData = self.onDemand_carInformationArray
            self.setupViewConfirmation(false)
        }
        
    }
    @IBAction func didTapRequestAction(_ sender: UIButton) {
        
        if sender.tag == 56 {
            self.txtPassengerName_OR_roomNo.text = ""
            self.viewConfirmation.removeFromSuperview()
            //self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
        else {
            if self.txtPassengerName_OR_roomNo.text == nil {
                Globals.ToastAlertWithString("Enter either passenger name or room number.")
                return
            }
            self.view.endEditing(true)
            
            self.setupViewConfirmation(true)
            
            //  let payment =  self.storyboard?.instantiateViewController(withIdentifier: "PAActiveJobViewController") as! PAActiveJobViewController
            let payment = self.storyboard?.instantiateViewController(withIdentifier: "PAPaymentForOnDemandVC") as! PAPaymentForOnDemandVC
            
            payment.arrOndemanCarsSelect = self.arr_SelectedCarData
            payment.isJobRequest = true
            if let pax = self.txtPassengerName_OR_roomNo.text {
                payment.paxName = pax
            }
            
            payment.strPreAuthAmountForJob = pre_authorized_amount
            self.navigationController?.pushViewController(payment, animated: true)
            
        }
    }
    func bookCarForOnDemandForETS(selectedIndex indexPath:IndexPath) {
        // self.tableViewCars.setEditing(true, animated: false)
        
        let dicVehicleType = self.arrVehicleInfo[indexPath.row]
        if let strArivalTime = self.dicArrivalTimeOfCarType[dicVehicleType["vehicle_type_title"]! as! String] as? String
        {
            if strArivalTime != Constants.strNoCars {
                let cell = self.tableViewCars.cellForRow(at: indexPath) as! CarsAvailableCellOnDemand
                cell.setSelected(true, animated: false)
                self.tableViewCars.reloadData()
                self.tableViewCars.selectRow(at: indexPath, animated: false, scrollPosition:.none)
                if let arrOnDemandCars = self.dicOnDemandCarsAvailable[dicVehicleType["vehicle_type_title"]! as! String]
                {
                    self.indexPathCellSelected = indexPath
                    let onDemandCarObjct = arrOnDemandCars.first!
                    // self.showSelectdTypeOfCarOnMap(arrOnDemandCars)
                    self.vehicleTypeID = String(describing: onDemandCarObjct["id"])
                    
                    if let vehicleType = dicVehicleType["vehicle_type_title"] as? String
                    {
                        if let amt =  self.dicArrivalTimeOfCarType[vehicleType + "preAuthAmt"] as? String{
                            pre_authorized_amount = amt
                        }
                    }
                    
                    if pre_authorized_amount.isEmpty || pre_authorized_amount.toFloat() == 0.00{
                        //   JLToast.makeText("Fare is not available for this vehicle.", duration: JLToastDelay.ShortDelay).show()
                    }
                    else {
                        self.locationManagerObjct.strVehicleType = String(describing: dicVehicleType["vehicle_type_title"]!)
                        self.arr_SelectedCarData = arrOnDemandCars as [[String : Any]]
                        self.setupViewConfirmation(false)
                        
                    }
                }
            }
        }
        else {
            self.dicArrivalTimeOfCarType[dicVehicleType["vehicle_type_title"]! as! String] = Constants.strNoCars
            self.tableViewCars.setEditing(false, animated: false)
            self.tableViewCars.reloadData()
        }
    }
    func setupViewConfirmation(_ isHide:Bool)  {
        var frameView = self.view!.frame
        self.view.addSubview(self.viewConfirmation)
        
        if let view = self.viewConfirmation.viewWithTag(100)
        {
            view.layer.cornerRadius = 10.0
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOffset = CGSize(width: 0, height: 1)
            
            //corner radius for buttons
            for _btn in view.subviews{
                if _btn.isKind(of: UIButton.self){
                    view.layer.cornerRadius = 5.0
                }
            }
        }
        
        self.view.bringSubview(toFront: self.viewConfirmation)
        frameView.origin.y = isHide == true ? self.view.frame.maxY + 10.0 : 0.0
        
        //UIView.animateWithDuration(0.40) { [weak self] in
        
        self.viewConfirmation.frame = frameView
       // self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        //}
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a  little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier! == "toPaymentAlert" {
            
            _ = segue.destination as! PaymentAlertViewController
            
            
        }
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    func backButtonActionMethod(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.locationManagerObjct.dicDestinationLocation!.removeAll()
        self.locationManagerObjct.destinationLocation = nil
    }
}

// class brackets

// MARK: - =======UITextFieldDelegate================

extension OnDemandViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        UIView.animate(withDuration: 0.30) { [weak self] in
            //   self?.txtPassengerName_OR_roomNo.superview!.frame.origin.y = (self?.txtPassengerName_OR_roomNo.superview!.frame.origin.y)! + 250
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if Constants.iPadScreen == false{
            if textField == self.txtPassengerName_OR_roomNo{
                UIView.animate(withDuration: 0.30) { [weak self] in
                    self?.txtPassengerName_OR_roomNo.superview!.frame.origin.y = (self?.view.frame.size.height)! - (self?.txtPassengerName_OR_roomNo.superview!.frame.size.height)! - 250
                }
            }
        }
        return true
    }
    //    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    //        if textField == self.txtPassengerName_OR_roomNo{
    //            UIView.animate(withDuration: 0.30) { [weak self] in
    //                self?.txtPassengerName_OR_roomNo.superview!.frame.origin.y = (self?.view.center.y)! - ((self?.txtPassengerName_OR_roomNo.superview!.frame.size.height)! / 2)
    //            }
    //
    //        }
    //        return true
    //    }
    
}
extension OnDemandViewController {
    // MARK: - ======== Pickup DropOff Markers  ========

    func addPolyLineWithEncodedStringInMap(_ encodedString: String) {
        let path = GMSMutablePath(fromEncodedPath: encodedString)
        let polyLine = GMSPolyline(path: path)
        polyLine.strokeWidth = 2
        polyLine.geodesic = true
        polyLine.strokeColor = UIColor.appThemeColor()
        polyLine.isTappable = true
        polyLine.map = self.mapView
    }
    func addOverlayToMapView(pickupLoc origin:CLLocation, dropOff destination:CLLocation){
        let request = OCDirectionsRequest(originLocation: origin, andDestinationLocation: destination)
        request?.transitMode = .bus
        request?.trafficModel = .default
        
        request?.alternatives = true
        request?.travelMode = OCDirectionsRequestTravelMode.driving
        request?.waypointsOptimise = true
        
        let direction = OCDirectionsAPIClient(noKeyUseHttps: true)
        direction?.directions(request, response: { (response, error) in
            //print("response ---\(response?.dictionary)")
            if ((error) != nil) {
                return;
            }
            if (response?.status != OCDirectionsResponseStatus.OK) {
                return
            }
            DispatchQueue.main.async(execute: { [weak self] in
                self?.addPolyLineWithEncodedStringInMap(response?.route().overviewPolyline.dictionary["points"] as! String)
            })
            
            if let legs = response?.route().legs.first as? OCDirectionsLeg {
                // print("duration ---\(legs.duration.text)")
            }
            
        })
        
    }
   
}

// MARK: - =======GMSMapViewDelegate================


extension OnDemandViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        //if  self.checkForFocusByDestination {
        if self.locationManagerObjct.destinationLocation == nil{
            
            self.mapCenterPinImage.isHidden = false
            self.map_CentreCoordinate = self.mapView.camera.target
            let newLocation = CLLocation(latitude: (self.map_CentreCoordinate?.latitude)! , longitude: (self.map_CentreCoordinate?.longitude)!)
            if ((self.locationManagerObjct.pickUpLocation!.coordinate.latitude != newLocation.coordinate.latitude) && (self.locationManagerObjct.pickUpLocation!.coordinate.longitude != newLocation.coordinate.longitude)) {
                self.locationManagerObjct.pickUpLocation! = newLocation
                self.MapStopScrollingAndGetAddressOfCurrentLocation(newLocation)
                self.showPickupDropoffMarkers()
                if self.locationManagerObjct.destinationLocation != nil {
                    self.calulateEstTotalFairBtSourceDestination()
                }
            }
        }else {
            if self.markerPickUpAddress!.map == nil {
                self.mapCenterPinImage.isHidden = true
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView!, willMove gesture: Bool) {
        // addressLabel.lock()
        if (gesture)
        {
            if self.locationManagerObjct.destinationLocation == nil{

            self.locationManagerObjct.dicSourceCustomLocation!.removeAll()
            self.markerPickUpAddress!.map = nil
            self.mapCenterPinImage.isHidden = false
            self.checkForFocusByDestination = true
         if Constants.ondemandCollectionView == "1"{
            self.collectionOndemandCars.isUserInteractionEnabled = false
         }else{
            self.tableViewCars.isUserInteractionEnabled = false
            (self.viewHeaderTableView.viewWithTag(13) as! UIActivityIndicatorView).startAnimating()
            }
            mapView.selectedMarker = nil
            } else {
                if self.markerPickUpAddress!.map == nil {
                    self.mapCenterPinImage.isHidden = true
                }
            }
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        // mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if self.locationManagerObjct.destinationLocation == nil{

        if self.markerPickUpAddress!.map == nil {
            self.mapCenterPinImage.isHidden = false
        }
        if let myLocation = mapView.myLocation {
            self.locationManagerObjct.pickUpLocation = myLocation
            self.showPickUpAddressOnMap()
            self.MapStopScrollingAndGetAddressOfCurrentLocation(myLocation)
            self.calulateEstTotalFairBtSourceDestination()
            
        }
        }else{
            if self.markerPickUpAddress!.map == nil {
            self.mapCenterPinImage.isHidden = true
            }
        }
        return true
    }
    // MARK: - ===get address from map location stops=======
    
    func MapStopScrollingAndGetAddressOfCurrentLocation(_ newLocation:CLLocation){
        
        //weak var weakSelf = self
        GoogleMapAPIServiceManager.sharedInstance.getLocationAddressUsingLatitude(newLocation.coordinate.latitude, longitude: newLocation.coordinate.longitude, withComplitionHandler: {  (dicRecievedJSON) -> () in
            if dicRecievedJSON["status"] as! String == "OK"{
                if (dicRecievedJSON["results"] as! [Any]).count != 0 {
                   
                    DispatchQueue.main.async(execute: { [weak self]() -> Void in
                        self?.setupPickupAddressOnButton(strAddress: ((dicRecievedJSON["results"]! as! [[String:Any]]) [0])["formatted_address"] as? String ?? "N/A")
                        self?.locationManagerObjct.strCurrentLocation = (((dicRecievedJSON["results"]!  as! [[String:Any]])[0] )["formatted_address"] as? String)!
                        
                    })
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async(execute: { [weak self]() -> Void in
                        self?.refreshMapViewForCheckOnDemandCars()
                    })
                }
                
            }
        })
        { (error) -> () in
            print("map error \(error)")
            
        }
    }
    
    
}
