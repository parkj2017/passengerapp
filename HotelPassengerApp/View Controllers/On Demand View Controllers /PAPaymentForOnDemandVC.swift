//
//  PAPaymentForOnDemandVC.swift
//  PassangerApp
//
//  Created by Netquall on 2/17/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import AVFoundation
class PAPaymentForOnDemandVC: BaseViewController,CardIOPaymentViewControllerDelegate, UIActionSheetDelegate, BackButtonActionProtocol, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    // MARK:- ========Property Decalartion -=============
    
    @IBOutlet weak var tablePayments: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var arrPayments = [[String:Any]]()
    var PushBackBlock : ((_ cardID:String , _ PaymentName:String)->())?
    var  viewUpdateCardAlert : UIView!
    var strCreditCardTypeID : String!
    var cardType : String!
    var cardName : String!
    
    var payment_method : String!
    
    @IBOutlet var popupViewCardType: UIView!
    @IBOutlet var tblCardTypes: UITableView!
    
    var arrCreditCardsType = [[String:Any]]()
    
    lazy var arrFilterDataSearch = [[String:Any]?]()
    var isJobRequest : Bool!
    var arrOndemanCarsSelect : [[String:Any]]!
    var dicPrefferCards : [String:Any]!
    var dicCardDetails : [String:Any]!
    var shouldShowSearchResults = false
    var strSearch : String!
    var pushToRequestView : ((_ dicCardDetails:[String:Any]?, _ cardID:String?) -> ())!
    
    var scanCardDetails : CardDetails = CardDetails()
    var selectedCardDetails : CardDetails = CardDetails()
    
    var strPreAuthAmountForJob:String!  = ""
    var paxName:String = ""
    
    var selectedIndex:NSIndexPath!
    @IBOutlet weak var btnRateCard: UIButton!
    // MARK:- ==== view life cycle======
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userCardDetailFromServerWithTypeOfURl(Constants.CreditCardUrlType.getCards)
        
        Globals.layoutViewFor(self.btnRateCard, color: UIColor.lightGray, width: 1.0, cornerRadius: 5.0)
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Payment", imageName: "backArrow")
        self.searchBar.backgroundColor = UIColor.appThemeColor()
        self.searchBar.placeholder = "Search With Existing Card Number"
        self.searchBar.delegate = self
        self.searchBar.tintColor = UIColor.white
        self.searchBar.showsCancelButton = true
        self.searchBar.setImage(UIImage(named: "searchIcon"), for: .search, state: UIControlState())
        let viewSearchBar = self.searchBar.subviews[0]
        for searchSubViews in viewSearchBar.subviews {
            if searchSubViews.isKind(of: UITextField.self){
                let textField = searchSubViews as! UITextField
                textField.textColor = UIColor.white
                  textField.font = Globals.defaultAppFont(15.0)
                textField.setValue(UIColor.white, forKeyPath: "_placeholderLabel.textColor")
                (((textField.value(forKey: "textInputTraits"))! as Any) as AnyObject).setValue(UIColor.darkGray, forKeyPath: "insertionPointColor")
                textField.backgroundColor = UIColor.clear
                textField.tintColor = UIColor.white
                textField.frame.size.height = 44
            }
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- ======= table View Data Source =============
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblCardTypes{
            return 1
        }
        else {
            return 2
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblCardTypes{
            return self.arrCreditCardsType.count
        }
        else{
            if section == 0 {
                if self.shouldShowSearchResults == true {
                    return 0
                }
                if self.dicPrefferCards != nil {
                    if let arrPrefferCard = self.dicPrefferCards["Preffer_card"] as? [[String:Any]]{
                        return arrPrefferCard.count == 0 ? 1 : arrPrefferCard.count
                    }
                }
                return 1
            }else if section == 1 {
                if self.shouldShowSearchResults == true {
                    return self.arrFilterDataSearch.count == 0 ? 1 : self.arrFilterDataSearch.count
                } else
                {
                    return self.arrPayments.count
                }
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if tableView == tblCardTypes{
            
            let cell : UITableViewCell? = self.tblCardTypes.dequeueReusableCell(withIdentifier: "cellCreditCardType")!
            cell?.textLabel?.textColor = UIColor.appBlackColor()
            cell?.textLabel?.font = Globals.defaultAppFontWithBold(16)
            if let cardDetils = self.arrCreditCardsType[indexPath.row]  as? [String:Any]{
                if let name =  cardDetils["name"] as? String{
                    cell?.textLabel?.text = name
                }
                else {
                    cell?.textLabel?.text = "No Name"
                }
            }
            return cell!
        }
        else {
            var cell : UITableViewCell?
            
            let  cellIndetifier = "cellWithPaymentMethods"
            //cellCarInfo
            if let cell = self.tablePayments.dequeueReusableCell(withIdentifier: cellIndetifier){
            var lblText = cell.viewWithTag(63) as! UILabel
            var viewOuter = cell.viewWithTag(62) as! UIView
            var checkImage = cell.viewWithTag(64) as! UIImageView
            
            lblText.numberOfLines = 4

            lblText.font = Globals.defaultAppFontWithBold(16)
            var cardExit = true
            if indexPath.section == 0  {
                if self.dicPrefferCards != nil {
                    let arrPrefferedCards = self.dicPrefferCards["Preffer_card"] as? [[String:Any]]
                    if arrPrefferedCards != nil && arrPrefferedCards!.count != 0 {
                        let dicCardDetails = arrPrefferedCards![indexPath.row]
                        var cardType = dicCardDetails["cc_type"] as? String
                        cardType = cardType == nil ? "" : cardType!
                        if let cardNo = dicCardDetails["card_no"] as? String{
                            if cardNo.count > 0{
                                lblText.text = "\(cardType!)  \(cardNo)"
                            }
                            else {
                                if let cardNo = dicCardDetails["name"] as? String{
                                    lblText.text = cardNo
                                }
                            }
                        }
                    }
                        cardExit = false
                    
                }
                if cardExit == true {
                    lblText.text = "No Preferred Cards Available"
                }
                
                if self.selectedIndex == nil{
                    self.selectedIndex = indexPath as NSIndexPath
                }
                lblText.textColor = UIColor.gray
              
                if (self.selectedIndex.row == indexPath.row && self.selectedIndex.section == indexPath.section ) && cardExit == false {
                    viewOuter.layer.borderColor = UIColor(red: 55.0/255.0, green: 184.0/255.0, blue: 115.0/255.0, alpha: 1.0).cgColor
                    viewOuter.layer.borderWidth = 2.0
                    viewOuter.backgroundColor = UIColor(red: 235.0/255.0, green: 248.0/255.0, blue: 241.0/255.0, alpha: 1.0)
                    checkImage.isHidden = false
                    lblText.textAlignment = .left
                }
                else {
                    viewOuter.layer.borderColor = UIColor.lightGray.cgColor
                    viewOuter.layer.borderWidth = 1.0
                    viewOuter.backgroundColor = UIColor.white
                    checkImage.isHidden = true
                    lblText.textAlignment = .center

                }
            

            } else if indexPath.section == 1{
                viewOuter.layer.borderColor = UIColor.lightGray.cgColor
                viewOuter.layer.borderWidth = 1.0
                lblText.textAlignment = .center
                checkImage.isHidden = true

                if self.shouldShowSearchResults == true {
                     if self.arrFilterDataSearch.count == 0{
                       // viewOuter.backgroundColor = UIColor.white
                        lblText.textColor = UIColor.gray
                        lblText.text = "No card found".uppercased()
                    }
                    else
                    {
                    //viewOuter.backgroundColor = UIColor.appGrayColor()
                    lblText.textColor = UIColor.gray
                        let dic = self.arrFilterDataSearch[indexPath.row]!
                        var cardType = dic["cc_type"] as? String
                        cardType = cardType == nil ? "" : cardType!
                        let str = dic["name"] as! String
                        lblText.text =  "\(cardType!)  \(str.uppercased())"
                    }
                    
              
                }else {
                    let dic = self.arrPayments[indexPath.row]
                    let str = dic["name"] as! String
               
                    if str.compare("Card Type") {
                        lblText.text = "Add new card".uppercased()
                    }else {
                    if str.compare("Direct Bill/Invoice") {
                        lblText.text = "Account Payment".uppercased()
                    }
                    else {
                        lblText.text = str.uppercased()
                    }
                }
                }
                
                if lblText.text?.compare("Add new card".uppercased()) == false{
                    if (self.selectedIndex.row == indexPath.row && self.selectedIndex.section == indexPath.section ) {
                        viewOuter.layer.borderColor = UIColor(red: 55.0/255.0, green: 184.0/255.0, blue: 115.0/255.0, alpha: 1.0).cgColor
                        viewOuter.layer.borderWidth = 2.0
                        viewOuter.backgroundColor = UIColor(red: 235.0/255.0, green: 248.0/255.0, blue: 241.0/255.0, alpha: 1.0)
                        checkImage.isHidden = false
                        lblText.textAlignment = .left
                    }
                    else {
                        viewOuter.layer.borderColor = UIColor.lightGray.cgColor
                        viewOuter.layer.borderWidth = 1.0
                        viewOuter.backgroundColor = UIColor.white
                        checkImage.isHidden = true
                        lblText.textAlignment = .center
                        
                    }
                }
            }
            return cell
        }
            return UITableViewCell()

        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblCardTypes{
            return 50
        }
        else {
            return 75.0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tblCardTypes{
            return 40
        }
        else
        {
            if section == 0 {
                if self.shouldShowSearchResults == true {
                    return 0
                }
            }
            return 40.0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0,y: 0, width: self.view.frame.width,height: 40))
        let lblPreferCards = UILabel(frame: CGRect(x: 15,y: 5, width: self.view.frame.width-30,height: 30))
        lblPreferCards.textAlignment = .left
        
        if tableView == tblCardTypes{
            lblPreferCards.font = Globals.defaultAppFontWithBold(15)
            lblPreferCards.textColor = UIColor.white

            lblPreferCards.text = "Select card Type".uppercased()
            headerView.backgroundColor = UIColor.appThemeColor()
            headerView.addSubview(lblPreferCards)

        }
        else {
            lblPreferCards.font = Globals.defaultAppFontWithBold(15)
            lblPreferCards.textColor = UIColor.appBlackColor()

            headerView.backgroundColor = UIColor.white
            
            if section == 0 {
                if self.shouldShowSearchResults == true {
                    return nil
                }
                lblPreferCards.text = "Preferred Cards".uppercased()
            }else if section == 1 {
                if self.shouldShowSearchResults == true {
                    lblPreferCards.text = "Search Result(s)".uppercased()
                }else {
                    lblPreferCards.text = "Other Options".uppercased()
                }
            }
            
            headerView.addSubview(lblPreferCards)
        }
        return headerView
    }
    // MARK:- ====table view delegate Methods ==========
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblCardTypes{
            popupViewCardType.isHidden = true
            if let cardDetils = self.arrCreditCardsType[indexPath.row] as? [String:Any]{
                if let name =  cardDetils["name"] as? String{
                    self.cardName = name
                }
                else {
                    self.cardName = "No Name"
                }
               if let method =  cardDetils["payment_method"]{
                self.payment_method =  "\(method)"
                }
                  else {
                    self.payment_method = "0"
                }
            }
            scanCard()
        }
        else{
            
            if indexPath.section == 0
            {
                self.selectedIndex = indexPath as NSIndexPath

                if  self.shouldShowSearchResults == false
                {
                    if self.dicPrefferCards != nil {
                        let arrPrefferedCards = self.dicPrefferCards["Preffer_card"] as? [[String:Any]]
                        if arrPrefferedCards != nil && arrPrefferedCards!.count != 0 {
                            self.dicCardDetails = arrPrefferedCards![indexPath.row]
                        }
                    }
                }
            }else if indexPath.section == 1
            { 
                if  self.shouldShowSearchResults == false
                {
                    if  let cellData = self.arrPayments[indexPath.row] as? [String:Any]
                    {
                        if  let cellType = cellData["name"] as? String
                        {
                            if cellType.compare("Card Type")  {
                                
                                if let cardType = cellData["Card Type"] as? [[String:Any]]{
                                    self.arrCreditCardsType = cardType
                                    tblCardTypes.reloadData()
                                }
                                let actionSheet = UIActionSheet(title: "Choose card type", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Business","Personal")
                                actionSheet.tag = 1000
                                actionSheet.show(in: self.view)
                            }
                            else {
                                self.selectedIndex = indexPath as NSIndexPath

                               if let _ = self.arrPayments[indexPath.row] as? [String:Any]{
                                    self.dicCardDetails =  self.arrPayments[indexPath.row] as? [String:Any]
                                }
                            }
                        }
                    }
                    
                }
                else
                {
                    if self.arrFilterDataSearch.count > 0{
                        self.selectedIndex = indexPath as NSIndexPath

                        if let dic = self.arrFilterDataSearch[indexPath.row]
                        {
                            self.dicCardDetails = dic //self.selectSearchCardForPayment(dic)
                        }
                    }
                }
            }
            self.tableReloadwithSelectedIndex()
        }
    }
    
    func tableReloadwithSelectedIndex()
    {
      self.tablePayments.reloadData()
    }
    func selectSearchCardForPayment(_ dic:[String:Any]) {
            self.dicCardDetails = dic
            self.performSegue(withIdentifier: "toActiveJob", sender: self)
}
@IBAction func btnConfirmationClicked(_ sender: UIButton) {
    
    if self.selectedIndex.section == 0
    {
        if  self.shouldShowSearchResults == false
        {
            if self.dicPrefferCards != nil {
                if let arrPrefferedCards = self.dicPrefferCards["Preffer_card"] as? [[String:Any]]{
                    if arrPrefferedCards.count != 0 {
                        self.dicCardDetails = arrPrefferedCards[self.selectedIndex.row]
                    }
                }
            }
        }
    }else if self.selectedIndex.section == 1{
        if  self.shouldShowSearchResults == false
        {
            if  let cellData = self.arrPayments[self.selectedIndex.row] as? [String:Any]
            {
                if  let cellType = cellData["name"] as? String
                {
                    if cellType.compare("Card Type")  {
                    
                    }
                    else {
                        if let _ = self.arrPayments[self.selectedIndex.row] as? [String:Any]{
                            self.dicCardDetails =  self.arrPayments[self.selectedIndex.row] as? [String:Any]
                        }
                    }
                }
            }
            
        }
        else
        {
            if self.arrFilterDataSearch.count > 0{
                if let dic = self.arrFilterDataSearch[self.selectedIndex.row]
                {
                     self.dicCardDetails = dic
                }
            }
        }
    }
    
    print(self.dicCardDetails)
    if self.dicCardDetails != nil{
                DispatchQueue.main.async(execute: { [weak self]() -> Void in
                    self?.performSegue(withIdentifier: "toActiveJob", sender: nil)
                })
            }
            else {
                Globals.ToastAlertWithString("Please select your payment method.")
            }
}
    // MARK: - ====scan Card =====
    func scanCard() {
        
        let authStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if authStatus == .authorized {
            self.showCardIOCardIOPaymentView()
        }
        else if authStatus == .notDetermined {
            NSLog("%@", "Camera access not determined. Ask for permission.")
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {[weak self](granted: Bool) -> Void in
                if granted {
                    NSLog("Granted access to %@", AVMediaTypeVideo)
                    self?.showCardIOCardIOPaymentView()
                }
                else {
                    NSLog("Not granted access to %@", AVMediaTypeVideo)
                    let alert = UIAlertView(title: "Camera Access Denied", message: "Please go to Settings -> Privacy -> Enable camera access for app.", delegate: nil, cancelButtonTitle: "Cancel")
                    alert.show()
                }
                })
        }
        else  {
            // My own Helper class is used here to pop a dialog in one simple line.
            let alert = UIAlertView(title: "Camera Access Denied", message: "Please go to Settings -> Privacy -> Enable camera access for app.", delegate: nil, cancelButtonTitle: "Cancel")
            alert.show()
        }
        
    }
    
    
    func showCardIOCardIOPaymentView()
    {
        let scanViewController: CardIOPaymentViewController = CardIOPaymentViewController(paymentDelegate: self)
        scanViewController.modalPresentationStyle = .formSheet
        scanViewController.collectCardholderName = true

        DispatchQueue.main.async { [weak self]() -> Void in
            if self == nil {
                return
            }
            self?.present(scanViewController, animated: true, completion: nil)
        }
        
    }
    // MARK: ========CardIOPaymentViewControllerDelegate=====
    func userDidProvide(_ info: CardIOCreditCardInfo, in paymentViewController: CardIOPaymentViewController) {
        NSLog("Scan succeeded with info: %@", info)
        // Do whatever needs to be done to deliver the purchased items.
        self.dismiss(animated: true, completion: nil)
        scanCardDetails.card_number = info.cardNumber
        scanCardDetails.cvv_number = info.cvv
        scanCardDetails.cc_name = info.cardholderName
        scanCardDetails.expiry_date = String(format: "%02lu/%lu", info.expiryMonth, info.expiryYear)
        scanCardDetails.card_type = self.cardType
        self.userCardDetailFromServerWithTypeOfURl(Constants.CreditCardUrlType.addUserCard)
    }
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController) {
        NSLog("User cancelled scan")
        self.dismiss(animated: true, completion: nil)
    }
    // MARK:- ==Web Call to Add User Card  =====
    func getBilligTypeInfoFromServer(){
        appDelegate.showProgressWithText("Loading Cards.....")
        let url = Constants.baseUrl + Constants.getBillingTypeInfo
        ServiceManager.sharedInstance.dataTaskWithGetRequest(url, successBlock: {[weak self] (dicData) -> Void in
            
            if dicData["status"] as! String == "success" {
                
                for dicRecord in dicData["records"] as! [[String:Any]]{
                    if (dicRecord["method"] as! String) == "Credit Card" {
                        self?.strCreditCardTypeID = dicRecord["id"] as! String
                    }
                }
            }else if dicData["status"] as! String == "session_expired" {
         //       JLToast.makeText("Session token mismatched", duration: 3.0).show()
            }
            })
        { (error) -> () in
           // JLToast.makeText("Network Error", duration: 3.0).show()
        }
    }
    func userCardDetailFromServerWithTypeOfURl(_ cardUrlType:Constants.CreditCardUrlType)
    {
        if appDelegate.networkAvialable == true {
            
            var dicParameters = [String:Any]()
            var urlString : String!
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            dicParameters["user_id"] = user_ID
            dicParameters["session"] = session
            
            if cardUrlType == Constants.CreditCardUrlType.getCards {
                appDelegate.showProgressWithText("Getting Card Details.....")
                urlString = Constants.getUserPreferredCards
            }else if cardUrlType == Constants.CreditCardUrlType.addUserCard {
                appDelegate.showProgressWithText("Adding Card Details.....")
                
                dicParameters["billing_type_id"] = self.payment_method as Any?

                urlString =  Constants.addUserCard
                //                dicParameters["user_id"] = user_ID
                //                dicParameters["session"] = session
                dicParameters["paypal_code"] = "" as Any?
                dicParameters["cvv_number"] = self.scanCardDetails.cvv_number
                dicParameters["expiry_date"] = self.scanCardDetails.expiry_date
                dicParameters["card_type"] = self.scanCardDetails.card_type
                dicParameters["card_number"] = self.scanCardDetails.card_number
                dicParameters["cc_name"] = self.scanCardDetails.cc_name

                
            }else if  cardUrlType == Constants.CreditCardUrlType.searchCard {
                appDelegate.showProgressWithText("Searching Card Details.....")
                urlString =  "getcustomer_credit_card_prefered" 
                dicParameters["user_id"] = user_ID
                dicParameters["card_no"] = self.strSearch as Any?
                
            }else if cardUrlType == Constants.CreditCardUrlType.addPreferredCard {
                appDelegate.showProgressWithText("Preferred Card Details.....")
                urlString =  "add_user_prefered_credit_card"
                dicParameters["user_id"] = user_ID
                dicParameters["prefered"] = "1" as Any?
                if let id = self.dicCardDetails["id"] {
                    dicParameters["payment_id"] = id
                }
            }
            var dicCards =  [String:Any]()
            weak var weakSelf = self
            ServiceManager.sharedInstance.dataTaskWithPostRequest(urlString, dicParameters: dicParameters, successBlock: { (dicData) -> Void in
                appDelegate.hideProgress()
                if weakSelf == nil {
                    return
                }
                if cardUrlType == Constants.CreditCardUrlType.getCards {
                    weakSelf!.shouldShowSearchResults = false
                    if (dicData["status"] as! String) == "success" {
                        dicCards = dicCards.formatDictionaryForNullValues(dicData)! as [String : Any]
                        
                        Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(dicCards as Any?, key:Constants.TotalCardsForPayment)
                        weakSelf!.arrPayments = dicCards["records"] as! [[String:Any]]
                        weakSelf!.dicPrefferCards = weakSelf?.arrPayments.first
                        
                        weakSelf!.arrPayments.removeFirst()
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            weakSelf!.tablePayments.reloadData()
                        })
                    }else  {
                        let alert = UIAlertView(title: "Error !", message: dicData["message"] as? String, delegate: nil, cancelButtonTitle: "Ok")
                        alert.show()
                    }
                    
                }else if  cardUrlType == Constants.CreditCardUrlType.addUserCard{
                    
                    DispatchQueue.main.async(execute: { [weak self]() -> Void in
                        appDelegate.hideProgress()

                        if (dicData["status"] as! String) == "success" {
                            let alert = UIAlertView(title: "Success", message: dicData["message"] as? String, delegate: nil, cancelButtonTitle: "Ok")
                            alert.show()
                                   }
                        else
                        {
                            let alert = UIAlertView(title: "Error !", message: dicData["message"] as? String, delegate: nil, cancelButtonTitle: "Ok")
                            alert.show()
                            return
                        }
                        })
                }else if cardUrlType == Constants.CreditCardUrlType.searchCard{
                    DispatchQueue.main.async(execute: { [weak self]() -> Void in
                        if (dicData["status"] as! String) == "success" {
                            self!.arrFilterDataSearch = dicData["record"] as! [[String:Any]]
                            weakSelf!.shouldShowSearchResults = true
                            weakSelf!.tablePayments.reloadData()
                        }else {
                            Globals.ToastAlertWithString(dicData["message"] as! String)
                          //  weakSelf!.shouldShowSearchResults = false
                            weakSelf!.tablePayments.reloadData()
                        }
                        })
                    
                }else if cardUrlType == Constants.CreditCardUrlType.addPreferredCard{
                    DispatchQueue.main.async(execute: { [weak self]() -> Void in
                        if (dicData["status"] as! String) == "success" {
                            self?.performSegue(withIdentifier: "toActiveJob", sender: self)
                        }else {
                            Globals.ToastAlertWithString(dicData["message"] as! String)
                        }
                        })
                }
                })
            { (error) -> () in
                weakSelf!.shouldShowSearchResults = false
                appDelegate.hideProgress()
                let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
                return
            }
        }else {
            Globals.ToastAlertForNetworkConnection()
        }
    }

   
    // MARK:- ========  Action Sheet Delegate  =======
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int){
        
        if actionSheet.tag == 1000{
            switch buttonIndex{
            case 1 :
                self.cardType = "Business"
                showCraditCardOptions()
                
            case 2:
                self.cardType  = "Personal"
                showCraditCardOptions()
                
            default : break
            }
        }
        else
        {
        }
    }
    func showCraditCardOptions()  {
        popupViewCardType.isHidden = false
        tblCardTypes.reloadData()
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PAPaymentForOnDemandVC.popupViewTouched))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.delegate = self
        popupViewCardType.addGestureRecognizer(tapGesture)
    }
    //Rate card button action
    @IBAction func btnRateCardClicked(sender: AnyObject) {
        var dicParam = [String:Any]()
        var amt:Float = 0.0
        var carData:[String:Any] = [String:Any]()
        for index in 0..<arrOndemanCarsSelect.count
        {
            let valuesDict = arrOndemanCarsSelect[index]
            if let strFareEstimated =  valuesDict["grandTotal"]{
                let estFare =  "\(strFareEstimated)".replacingOccurrences(of: ",", with: "").toFloat()//OccurrencesOfString(",", withString: "").toFloat()
                if estFare > amt{
                    amt = estFare
                    carData = arrOndemanCarsSelect[index]
                }
            }
        }
        if let dicRates = carData["rate"] as? [String:Any] , var currency = carData["currency"] as? String{
            
            if let base_fare = dicRates["base_fare"] as? String{
                dicParam[Constants.BaseFare] = base_fare
            }
            else{
                dicParam[Constants.BaseFare] = "0.0"
            }
            
            if let mile_rate = dicRates["mile_rate"] as? String{
                dicParam[Constants.PerMileRate] = mile_rate
            }else {
                dicParam[Constants.PerMileRate] = "0.0"
            }
            
            if let min_fare = dicRates["min_fare"] as? String{
                dicParam[Constants.MinFare] = min_fare
            }else {
                dicParam[Constants.MinFare] = "0.0"
            }
            
            if let km_rate = dicRates["km_rate"] as? String{
                dicParam[Constants.PerKMRate] = km_rate
            }else {
                dicParam[Constants.PerKMRate] = "0.0"
            }
            
            if let min_rate = dicRates["min_rate"] as? String{
                dicParam[Constants.PerMinRate] = min_rate
            }else {
                dicParam[Constants.PerMinRate] = "0.0"
            }
            
            if currency.isEmpty == true{
                currency = appDelegate.currency
            }
            
            let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "PADriverAndRateCardAlertVC") as! PADriverAndRateCardAlertVC
            detailsVc.isDriverProfile = false
            detailsVc.dicDetails = dicParam
            detailsVc.currency = currency
            detailsVc.view.backgroundColor = UIColor.clear
            detailsVc.modalPresentationStyle = .overFullScreen
            detailsVc.providesPresentationContextTransitionStyle = true
            detailsVc.definesPresentationContext = true
            self.providesPresentationContextTransitionStyle = true
            self.definesPresentationContext = true
            self.modalTransitionStyle = .crossDissolve
            self.modalPresentationStyle = .overFullScreen
            self.present(detailsVc, animated: true, completion: nil)
            
        }
    }
    // MARK:- ========  GestureRecognizer Delegate  =======

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool{
        if gestureRecognizer is UITapGestureRecognizer {
            if (String(describing: touch.view!.classForCoder) == "UITableViewLabel"){
                return false
            }
        }
        return true
    }

    @IBAction func btnCrossClicked(_ sender: Any) {
        popupViewCardType.isHidden = true
    }
    func  popupViewTouched() {
        popupViewCardType.isHidden = true
    }
    // MARK:- ========  search bar Delegate  =======
    
    func  searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool{
        self.shouldShowSearchResults = true
        self.tablePayments.reloadData()

        return true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.shouldShowSearchResults = true
        self.searchBar.resignFirstResponder()
        self.userCardDetailFromServerWithTypeOfURl(Constants.CreditCardUrlType.searchCard)
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        self.shouldShowSearchResults = false
        self.searchBar.resignFirstResponder()
        self.searchBar.text = ""
        self.tablePayments.reloadData()
        self.arrFilterDataSearch.removeAll(keepingCapacity: true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        self.arrFilterDataSearch.removeAll(keepingCapacity: true)
        
        self.strSearch = searchText
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toActiveJob" {
            let ObjctActiveJob = segue.destination as! PAActiveJobViewController
            ObjctActiveJob.arrOndemanCarsSelected = self.arrOndemanCarsSelect
            ObjctActiveJob.dicCardDetails = self.dicCardDetails
            ObjctActiveJob.isJobRequest = true
            ObjctActiveJob.paxName = self.paxName
            ObjctActiveJob.preAuthAmountForJob = self.strPreAuthAmountForJob
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    func backButtonActionMethod(_ sender: UIButton) {
        // self.dismissViewControllerAnimated(true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
