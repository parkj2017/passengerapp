//
//  PARateDriverVC.swift
//  PassangerApp
//
//  Created by Netquall on 6/24/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit

class PARateDriverVC: BaseViewController, UITextViewDelegate, BackButtonActionProtocol {

    @IBOutlet var toolBarView: UIToolbar!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewRadioButtons: UIView!
    
    @IBOutlet weak var txtViewComments: UITextView!
    @IBOutlet weak var starRatingViewCosmo: CosmosView!
    var strPaxComments = ""
    var reservationId = ""
    var driverID = ""
    
    // MARK:- ====view life cycle ====
    override func viewDidLoad() {
        super.viewDidLoad()
       self.starRatingViewCosmo.updateOnTouch = true
        self.starRatingViewCosmo.rating = 0.0
        self.txtViewComments.text = "Add Comments"
         self.txtViewComments.textColor = UIColor.white
        self.txtViewComments.inputAccessoryView = self.toolBarView
        self.txtViewComments.delegate = self
        Globals.layoutViewFor(self.txtViewComments, color: UIColor.white, width: 1.0, cornerRadius: 0.0)
        // Do any additional setup after loading the view.
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Chauffeur Feedback", imageName: "backArrow")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         Globals.sharedInstance.delegateBack = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ============text view delegate  ==========
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if self.txtViewComments == textView {
            if self.txtViewComments.text == "Add Comments" {
                self.txtViewComments.text = ""
            }
        }
        
        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height-100.0)
        
        if Constants.screenHeight <= 568.0 {
        if self.view.frame.origin.y == 64.0 {
        UIView.animate(withDuration: 0.10, animations: { [weak self] in
            self?.view.frame.origin.y -= 150.0
          }) 
         }
        }
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if Constants.screenHeight <= 568.0 {
        UIView.animate(withDuration: 0.10, animations: { [weak self] in
            self?.view.frame.origin.y = 64.0
          }) 
        }
    }
    
    @IBAction func btnDoneOrCancelTextViewAction(_ sender: UIBarButtonItem) {
        if sender.tag == 19 {
            self.txtViewComments.text = "Add Comments"
        }
        self.view.endEditing(true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnRadioSatisfyOrNotAllSidesClickedAction(_ sender: UIButton) {
        
        self.strPaxComments = sender.title(for: UIControlState())!
        sender.isSelected = true
        
        if sender.tag == 50{
            self.starRatingViewCosmo.rating = 5
        }
        else if sender.tag == 51{
            self.starRatingViewCosmo.rating = 3
        }
        else if sender.tag == 52{
            self.starRatingViewCosmo.rating = 1
        }
        
        for btnView in self.viewRadioButtons.subviews {
            if btnView.isKind(of: UIButton.self) {
            if btnView.tag != sender.tag {
                let btn = btnView as! UIButton
                btn.isSelected = false
             }
           }
        }
        
    }
    
    @IBAction func btnAddDriverAsFavouriteAction(_ sender: UIButton) {
        if appDelegate.networkAvialable == false   {
            return
        }
        appDelegate.showProgressWithText("Please Wait...")
        let userInfo = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        var dicParameters = [String:Any]()
        dicParameters["driver_id"] = self.driverID as Any?
        dicParameters["user_id"] = userInfo["id"]!
        dicParameters["session"] = userInfo["session"]!
        dicParameters["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?
        appDelegate.showProgressWithText("Please wait..")
        ServiceManager.sharedInstance.dataTaskWithPostRequest("addtoFavouriteList", dicParameters: dicParameters, successBlock: { (dicData:[String:Any]!) in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success"{
                Globals.ToastAlertWithString(dicData["message"] as! String)
                
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString((error?.description)!)
        }

    }
    
    @IBAction func btnRateYourRideClickedAction(_ sender: UIButton) {
        if appDelegate.networkAvialable == false   {
            return
        }
        if self.starRatingViewCosmo.rating == 0{
            Globals.ToastAlertWithString("Please rate your ride.")
            return
        }
        appDelegate.showProgressWithText("Please Wait...")
        let userInfo = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        var dicParameters = [String:Any]()
        dicParameters["driver_id"] = self.driverID as Any?
        dicParameters["user_id"] = userInfo["id"]!
        dicParameters["session"] = userInfo["session"]!
        dicParameters["reservation_id"] = self.reservationId as Any?
        dicParameters["rating"] = self.starRatingViewCosmo.rating as Any?
        dicParameters["pass_feedback"] = self.strPaxComments as Any?
        dicParameters["avg_rating"] = self.starRatingViewCosmo.rating as Any?
        dicParameters["rating_type"] = "single" as Any?
        
        if self.txtViewComments.text == "Add Comments" {
            dicParameters["comment"] = ""
        }else {
            dicParameters["comment"] = self.txtViewComments.text as Any?
        }
        dicParameters["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?
        dicParameters["created_on"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?
        appDelegate.showProgressWithText("Please wait..")
        ServiceManager.sharedInstance.dataTaskWithPostRequest("customerfeedback", dicParameters: dicParameters, successBlock: { [weak self](dicData:[String:Any]!) in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success"{
            Globals.ToastAlertWithString(dicData["message"] as! String)
            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.activeJobNotification)
            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.currentJobStaus)
                self?.backToOnDemandViewController()
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString((error?.description)!)
        }

    }
    // MARK:---======back button -========
    func backButtonActionMethod(_ sender: UIButton) {
//        
//        Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.activeJobNotification)
//        Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.currentJobStaus)
//       self.backToOnDemandViewController()
    }
func backToOnDemandViewController(){
        DispatchQueue.main.async { [weak self]() -> Void in
            if self == nil {
                return
            }
        for vc: UIViewController in (self?.navigationController?.viewControllers)! {
                    if (vc.isKind(of: SWRevealViewController.self)) {
                        self?.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }
        }
}
