//
//  PADriverFeedbackPopupVC.swift
//  PassangerApp
//
//  Created by Netquall on 6/23/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader
enum viewTagsName :Int {
    case lblDateTitle = 15
    case lblTimeTitle = 16

    case lblDate = 20
    case lblTime = 21
    case lblVehicleType = 22
    case lblVehicleName = 23
    case imageDriver = 50
    case ratingView = 60
    case driverName = 51
}
class PADriverFeedbackPopupVC: BaseViewController, BackButtonActionProtocol {
    
    // MARK: - ===property ===
    @IBOutlet var consTopSpaceTable: NSLayoutConstraint!
    @IBOutlet var viewTimeDateVehicleDetails: UIView!
    @IBOutlet var viewDriverImage: UIView!
    @IBOutlet var viewRating: UIView!
    @IBOutlet weak var tblViewDriverFeedback: UITableView!
    @IBOutlet weak var btnAddAsFavouriteDriver: UIButton!
    
    //var isPushedFromActiveJob:Bool! = false
    
    var dicJobDeatils:[String:Any]!
    var dicDriverInfo : [String:Any]!
    var isPresented:Bool! = false
    var driverImage:UIImage!
    var isPastJob:Bool?
    // MARK: - ===view life cycle ===
    override func viewDidLoad() {
        super.viewDidLoad()
        if isPresented == false{
            self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Receipt", imageName: "backArrow")
        }
        self.getDriverInfomation()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PADriverFeedbackPopupVC.tapGestureAction(_:)))
        self.view.viewWithTag(96)!.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
        //print( dicJobDeatils)
        //print(dicDriverInfo)
        
        self.viewRating.isUserInteractionEnabled = false
        
    }
    override func viewDidLayoutSubviews() {
        if isPastJob == true {
            if let isFave = self.dicDriverInfo["fav_driver"] {
                if String(describing: isFave).compare("1") {
                    self.btnAddAsFavouriteDriver.isHidden = true
                    self.consTopSpaceTable.constant = 0.0
                    self.view.updateConstraints()
                    self.view.layoutIfNeeded()
                }
            }
        }
        else {
            self.btnAddAsFavouriteDriver.isHidden = true
            self.consTopSpaceTable.constant = 0.0
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if isPresented == false{
            Globals.sharedInstance.delegateBack = self
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if isPresented == false{
            Globals.sharedInstance.delegateBack = nil
        }
    }
    
    @IBAction func btnCancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func backButtonActionMethod(_ sender: UIButton) {
        
        Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.activeJobNotification)
        Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.currentJobStaus)
        DispatchQueue.main.async { [weak self]() -> Void in
            if self == nil {
                return
            }
            for vc: UIViewController in (self?.navigationController?.viewControllers)! {
                if (vc.isKind(of: SWRevealViewController.self)) {
                    self?.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tapGestureAction(_ tapGesture:UITapGestureRecognizer){
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func reloadViewAgain()  {
        
        let imageDriver = self.viewDriverImage.viewWithTag(viewTagsName.imageDriver.rawValue) as! UIImageView
        imageDriver.image = UIImage(named: "profile_placeholder")
        
        if let str = self.dicJobDeatils["image"] as? String{
            DLImageLoader.sharedInstance().image(fromUrl: str) { [weak self](error, image) -> () in
                if image != nil
                {
                    self?.driverImage = image
                }
                else {
                    self?.driverImage = UIImage(named: "profile_placeholder")
                }
            }
        }
        else {
            self.driverImage = UIImage(named: "profile_placeholder")
        }
        
        DispatchQueue.main.async { [weak self] in
            self?.tblViewDriverFeedback.reloadData()
        }
    }
    
    @IBAction func btnAddAsFavouriteDriverAction(_ sender: UIButton) {
        if appDelegate.networkAvialable == false   {
            return
        }
        appDelegate.showProgressWithText("Please Wait...")
        let userInfo = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        var dicParameters = [String:Any]()
        dicParameters["driver_id"] = self.dicDriverInfo["primary_driver"]!
        dicParameters["user_id"] = userInfo["id"]!
        dicParameters["session"] = userInfo["session"]!
        dicParameters["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?
        appDelegate.showProgressWithText("Please wait..")
        ServiceManager.sharedInstance.dataTaskWithPostRequest("addtoFavouriteList", dicParameters: dicParameters, successBlock: { (dicData:[String:Any]!) in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success"{
                Globals.ToastAlertWithString(dicData["message"] as! String)
                
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString((error?.description)!)
        }
        
    }
    
    // MARK: - ============table View Data Source  ==========
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int
    {
        if self.dicJobDeatils == nil {
            return 0
        }
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 2 {
            if let arrComment = self.dicJobDeatils["comments"] as? [Any] {
                return arrComment.count
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        var cell : UITableViewCell?
        
        let  cellIndetifier = "cellFeedbackDriverComments"
        //cellCarInfo
        cell  = self.tblViewDriverFeedback.dequeueReusableCell(withIdentifier: cellIndetifier, for: indexPath)
        var lblPaxName : UILabel!
        var txtViewComments : UITextView!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
            lblPaxName = cell!.viewWithTag(62) as! UILabel
            txtViewComments = cell!.viewWithTag(63)! as! UITextView
        }else {
            lblPaxName = cell!.viewWithTag(62) as! UILabel
            txtViewComments = cell!.viewWithTag(63)! as! UITextView
        }
        if let arrComment = self.dicJobDeatils["comments"] as? [[String:Any]] {
            let dicComments = arrComment[indexPath.row]
            if let strPaxname = dicComments["passenger_name"] as? String{
                lblPaxName.text = strPaxname == "" ? "N/A" : strPaxname
            }
            else {
                lblPaxName.text =  "N/A"
            }
            
            if let strComments = dicComments["passenger_comment"] as? String{
                txtViewComments.text = strComments.isEmpty ?  "No comments found" : strComments
            }
            else {
                txtViewComments.text =  "No comments found"
            }
        }
        lblPaxName.numberOfLines = 1
        lblPaxName.font = Globals.defaultAppFontWithBold(15)
        lblPaxName.textColor = UIColor.black
        txtViewComments.font = Globals.defaultAppFontItalic(14)
        txtViewComments.textColor = UIColor.lightBlack()
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        if let arrComment = self.dicJobDeatils["comments"] as? [[String:Any]] {
            let constraint: CGSize = CGSize(width: tblViewDriverFeedback.frame.width, height: 20000.0)
            let dicComments = arrComment[indexPath.row]
            let strComments = dicComments["passenger_comment"] as? String
            var size: CGSize
            let boundingBox: CGSize = strComments!.boundingRect(with: constraint, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName:Globals.defaultAppFontItalic(15)], context: nil).size
            size = CGSize(width: ceil(boundingBox.width), height: ceil(boundingBox.height))
            
            
            return size.height > (DeviceType.IS_IPAD ? 80.0 : 70) ? size.height + 30 : (DeviceType.IS_IPAD ? 80.0 : 70)
        }
        return DeviceType.IS_IPAD ? 80.0 : 70.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch (section){
        case 0:
            return DeviceType.IS_IPAD ? 165 : 160
        case 1:
            return DeviceType.IS_IPAD ? 170 : 156
        case 2:
            return 40
        case 3:
            return 70
        default :
            return 70
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch(section){
        case 0:
            self.viewDriverImage.translatesAutoresizingMaskIntoConstraints = true
            self.viewDriverImage.frame = CGRect(x: 0, y: 0, width: self.tblViewDriverFeedback.frame.width, height: 160)
            self.viewDriverImage.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            
            
            let imageDriver = self.viewDriverImage.viewWithTag(viewTagsName.imageDriver.rawValue) as! UIImageView
            imageDriver.image = UIImage(named: "profile_placeholder")
            
            if Constants.iPadScreen == true {
                imageDriver.layer.cornerRadius = imageDriver.frame.height / 2.0
            }else {
                imageDriver.frame.size = CGSize(width: 90, height: 90)
                imageDriver.layer.cornerRadius = 45.0
                imageDriver.layer.borderColor = UIColor.appGrayColor().cgColor
                imageDriver.layer.borderWidth = 0.5
                
            }
            imageDriver.layer.masksToBounds = true
            
            if let str = self.dicJobDeatils["image"] as? String{
                if let activity = self.viewDriverImage.viewWithTag(52) as? UIActivityIndicatorView
                {
                    activity.isHidden = false
                    activity.startAnimating()
                }
                DLImageLoader.sharedInstance().image(fromUrl: str) { [weak self](error, image) -> () in
                    if image != nil
                    {
                        imageDriver.image = image
                    }
                    else {
                        imageDriver.image = UIImage(named: "profile_placeholder")
                    }
                    
                    DispatchQueue.main.async { [weak self] in
                        
                        if let activity = self?.viewDriverImage.viewWithTag(52) as? UIActivityIndicatorView
                        {
                            activity.isHidden = true
                            activity.stopAnimating()
                        }
                    }
                }
            }
            else {
                DispatchQueue.main.async { [weak self] in
                    
                    imageDriver.image = UIImage(named: "profile_placeholder")
                    if let activity = self?.viewDriverImage.viewWithTag(52) as? UIActivityIndicatorView
                    {
                        activity.isHidden = true
                        activity.stopAnimating()
                    }
                }
            }
            
            if let lblDriverName = self.viewDriverImage.viewWithTag(viewTagsName.driverName.rawValue) as? UILabel
            {
                var driverName = ""
                
                if let f_name = self.dicJobDeatils["first_name"]{
                    driverName = "\(f_name)"
                    if let l_name = self.dicJobDeatils["last_name"]{
                        driverName = "\(f_name) \(l_name)"
                    }
                }
                lblDriverName.text = driverName.isEmpty ? "N/A" : "\(driverName)"
            }
            self.viewDriverImage.updateConstraints()
            
            return self.viewDriverImage
        case 1:
            
            if isPastJob == true{
            let lblDate = self.viewTimeDateVehicleDetails.viewWithTag(viewTagsName.lblDate.rawValue) as! UILabel
            if let pu_date = self.dicJobDeatils["pu_date"] as? String {
                lblDate.text = pu_date
            }else {
                lblDate.text = "N/A"
            }
            let lblTime = self.viewTimeDateVehicleDetails.viewWithTag(viewTagsName.lblTime.rawValue) as! UILabel
            if let pu_date = self.dicJobDeatils["pu_time"] as? String {
                lblTime.text = pu_date
            }else {
                lblTime.text = "N/A"
            }
            }
            else {
                if let lblplateNo = self.viewTimeDateVehicleDetails.viewWithTag(viewTagsName.lblDateTitle.rawValue) as? UILabel{
                    lblplateNo.text =  "plate number".uppercased()
                }
                if let lblDateLicence = self.viewTimeDateVehicleDetails.viewWithTag(viewTagsName.lblTimeTitle.rawValue) as? UILabel{
                    lblDateLicence.text = "license number".uppercased()
                }

                if let lblPlate = self.viewTimeDateVehicleDetails.viewWithTag(viewTagsName.lblDate.rawValue) as? UILabel{
                if let p_n = self.dicJobDeatils["plate_number"]  {
                    lblPlate.text = "\(p_n)".isEmpty ? "N/A" : "\(p_n)"
                    }
                }
                let lblLicence = self.viewTimeDateVehicleDetails.viewWithTag(viewTagsName.lblTime.rawValue) as! UILabel
                if let d_l = self.dicJobDeatils["driver_license"]  {
                    lblLicence.text = "\(d_l)".isEmpty ? "N/A" : "\(d_l)"
                }
            }
            let lblVehicleType = self.viewTimeDateVehicleDetails.viewWithTag(viewTagsName.lblVehicleType.rawValue) as! UILabel
            if let vehicle_type = self.dicJobDeatils["vehicle_type"] {
                lblVehicleType.text = "\(vehicle_type)".isEmpty ? "N/A" : "\(vehicle_type)"
            }else {
                lblVehicleType.text = "N/A"
            }
            let lblVehicleName = self.viewTimeDateVehicleDetails.viewWithTag(viewTagsName.lblVehicleName.rawValue) as! UILabel
            if let fleet = self.dicJobDeatils["fleet"]  {
                lblVehicleName.text = "\(fleet)".isEmpty ? "N/A" : "\(fleet)"
            }else {
                lblVehicleName.text = "N/A"
            }
            self.viewTimeDateVehicleDetails.translatesAutoresizingMaskIntoConstraints = true
            self.viewTimeDateVehicleDetails.frame = CGRect(x: 0,y: 0,width: self.tblViewDriverFeedback.frame.width,height: 156)
            return  self.viewTimeDateVehicleDetails
            
        case 2:
            let view:UIView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40.0))
            let textLabel:UILabel = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.width-20, height: 40.0))
            view.backgroundColor = UIColor.white
            textLabel.numberOfLines = 2
            textLabel.textColor = UIColor.appBlackColor()
            textLabel.font = Globals.defaultAppFontWithBold(14)
            textLabel.text = "Passenger comments on last three rides"
            textLabel.textAlignment = NSTextAlignment.center
            view.addSubview(textLabel)
            view.layer.borderWidth = 0.5
            view.layer.borderColor = UIColor.appGrayColor().cgColor
            return view
        case 3:
            var ratingStr = self.dicJobDeatils["rating"] as? String
            if ratingStr == nil {
                ratingStr = ""
            }
            let ratinFloat = ratingStr == "" ? 0.0 : ratingStr!.toDouble()
            let floatingRatesStar = self.viewRating.viewWithTag(viewTagsName.ratingView.rawValue) as! CosmosView
            floatingRatesStar.rating = ratinFloat!
            floatingRatesStar.totalStars = 5
            floatingRatesStar.isUserInteractionEnabled = false
            self.viewRating.translatesAutoresizingMaskIntoConstraints = true
            self.viewRating.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 70)
            return self.viewRating
            
        default :
            break
        }
        return nil
    }
    func getDriverInfomation()  {
        if appDelegate.networkAvialable == false   {
            return
        }
        appDelegate.showProgressWithText("Please Wait...")
        let userInfo = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        var dicParameters = [String:Any]()
        dicParameters["driver_id"] = self.dicDriverInfo["primary_driver"]!
        dicParameters["user_id"] = userInfo["id"]!
        dicParameters["session"] = userInfo["session"]!
        if let reservation_id = self.dicDriverInfo["id"] as? String{
            dicParameters["reservation_id"] = reservation_id as Any?
        }
        ServiceManager.sharedInstance.dataTaskWithPostRequest("getDriverAllInfomation", dicParameters: dicParameters, successBlock: { [weak self](dicData:[String:Any]!) in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success"{
                self?.dicJobDeatils = dicData["record"] as! [String:Any]
                
                if let ratingStr = self?.dicJobDeatils["rating"]{
                    
                    let floatingRatesStar = self?.viewRating.viewWithTag(viewTags.ratingView.rawValue) as! CosmosView
                    floatingRatesStar.rating = "\(ratingStr)".toDouble()!
                    floatingRatesStar.totalStars = 5
                    floatingRatesStar.isUserInteractionEnabled = false
                }
                self?.reloadViewAgain()
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString(error!.description)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
