//
//  PAOndemandFilterViewController.swift
//  HotelPassengerApp
//
//  Created by Netquall on 1/23/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit

class PAOndemandFilterViewController: UIViewController {

    @IBOutlet weak var pricingRangeSlider: RangeSlider!
    @IBOutlet weak var distanceRangeSlider: RangeSlider!
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var minDistance: UILabel!
    @IBOutlet weak var maxDistance: UILabel!
    
    @IBOutlet weak var minPrice: UILabel!
    @IBOutlet weak var maxPrice: UILabel!
    
    var maxFare:Double = 0.0
    
    
    var fare_Range:String = ""
    var radius_Range:String = ""
    
    var arr_selectedRatings:[String] = [String]()
     var didFinishApplyingFilters:((_ filterElements:[String:Any]?)->())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PAOndemandFilterViewController.tapGestureAction(tapGesture:)))
        bgView.addGestureRecognizer(tapGesture)

        // Do any additional setup after loading the view.
        
        distanceRangeSlider.lowerValue = 0.0
        distanceRangeSlider.upperValue = 0.0
        distanceRangeSlider.needLowerThumb = false
        minDistance.text = "1" + " \(appDelegate.distanceUnit!)"
        maxDistance.text = "\(appDelegate.ondemandRadius!)" + " \(appDelegate.distanceUnit!)"
        

        pricingRangeSlider.lowerValue = 0.0
        pricingRangeSlider.upperValue = 1.0
        pricingRangeSlider.needLowerThumb = true
        minPrice.text = "\(appDelegate.currency!) " + "0"
        maxPrice.text = "\(appDelegate.currency!) " + String(format: "%d", Int(maxFare))
        
    }

    
    @IBAction func btnDismissPopupClicked(_ sender: UIButton) {
        if sender.tag == 100{
            self.didFinishApplyingFilters!(nil)
            
            self.dismiss(animated: true, completion: nil)
        }
        else if sender.tag == 101{
            var filterElements:[String:Any] = [String:Any]()
            filterElements["radius"] = "\(radius_Range)"
            filterElements["rating"] = "\(arr_selectedRatings.joined(separator: ","))"
            filterElements["price_range"] = "\(fare_Range)"
            
            self.didFinishApplyingFilters!(filterElements)
            self.dismiss(animated: true, completion: nil)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tapGestureAction(tapGesture:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func distanceSliderRangeChanges(_ sender: RangeSlider) {
        let lowerVal = sender.upperValue * appDelegate.ondemandRadius.toDouble()!
        
        radius_Range = "\(Int(lowerVal == 0 ? 1 : lowerVal))"
        minDistance.text = String(format: "%d", Int(lowerVal == 0 ? 1 : lowerVal )) + " \(appDelegate.distanceUnit!)"
    }
    
    @IBAction func priceSliderRangeChanges(_ sender: RangeSlider) {
        
        let lowerVal = sender.lowerValue * self.maxFare
        let upperVal = sender.upperValue * self.maxFare
        
        fare_Range = "\(Int(lowerVal == 0 ? 1 : lowerVal)),\(Int(upperVal))"
        
        minPrice.text = "\(appDelegate.currency!) " + String(format: "%d", Int(lowerVal == 0 ? 1 : lowerVal))
        maxPrice.text = "\(appDelegate.currency!) " + String(format: "%d", Int(upperVal))
        
    }
    
    @IBAction func btnStarRatingClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true{
            sender.isSelected = true
            if !arr_selectedRatings.contains("\(sender.tag - 99)"){
                arr_selectedRatings.append("\(sender.tag - 99)")
            }
        }
        else {
            sender.isSelected = false
            if arr_selectedRatings.contains("\(sender.tag - 99)"){
            arr_selectedRatings.remove("\(sender.tag - 99)")
            }
        }
    }
    
        // MARK: - UIGestureRecognizer
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool{
        if gestureRecognizer is UITapGestureRecognizer {
            if (String(describing: touch.view!.classForCoder) == "UITableViewCellContentView"){
                return false
            }
        }
        return true
    }
    
        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
