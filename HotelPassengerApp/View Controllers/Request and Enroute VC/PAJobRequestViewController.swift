//
//  PAActiveJobViewController.swift
//  PassangerApp
//
//  Created by Netquall on 1/15/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader
import GoogleMaps
class PAJobRequestViewController: UIViewController, BackButtonActionProtocol, RemoteNotificationTapProtocol {
    
    // MARK: ========Property Decalartion -=============
    @IBOutlet weak var mainRequestView: UIView!
    var etaLblShowTime : UILabel!
    var finalImage : UIImage!
    var image_MapPinCar : Data!
    @IBOutlet weak var lblDropOffAddress: UILabel!
    @IBOutlet weak var lblPickeUpAddress: UILabel!
    @IBOutlet weak var mapViewRequest: GMSMapView!
    
    
    @IBOutlet weak var viewPickupDropOff: UIView!
    
    var isRequestForCar : Bool!
    var arrOndemanCarsSelected : [OnDemandCarsAvailable]!
    var dicArrivalTimeOfVehicleType : [String:Any]!
    var CardID : String!
    var mapBounds : GMSCoordinateBounds!
    lazy var locationManagerObjct = LocationManager.sharedInstance
    
    @IBOutlet weak var constraintTopViewPickup: NSLayoutConstraint!
    
    @IBOutlet weak var constraintBottomViewPickUp: NSLayoutConstraint!
    // MARK: ========view Life Cycyle  -=============
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  self.navigationController?.navigationBar.items =
        self.lblPickeUpAddress.text! = self.locationManagerObjct.strCurrentLocation
        self.lblDropOffAddress.text! = locationManagerObjct.dicDestinationLocation!["address"] as! String
        // self.mapViewRequest.settings.myLocationButton = true
        self.mapViewRequest.isMyLocationEnabled = true
        self.mapViewRequest.settings.zoomGestures = true
        self.showDriverLocationOnMap()
        self.showPickUpLocationOnMap()
        self.mapBounds = GMSCoordinateBounds(coordinate: self.locationManagerObjct.pickUpLocation!.coordinate, coordinate: self.locationManagerObjct.pickUpLocation!.coordinate)
        
        if let objectCar = self.arrOndemanCarsSelected.first {
            let carLoc : CLLocation = CLLocation(latitude:CDouble(objectCar.latitude)!
                ,longitude: CDouble(objectCar.longitude)!)
            let carBounds = GMSCoordinateBounds(coordinate: carLoc.coordinate, coordinate: carLoc.coordinate)
            self.mapBounds = self.mapBounds.includingBounds(carBounds)
        }
        
       // let destinationBounds = GMSCoordinateBounds(coordinate: self.locationManagerObjct.destinationLocation!.coordinate, coordinate: self.locationManagerObjct.destinationLocation!.coordinate)
        
        //self.mapBounds = self.mapBounds.includingBounds(destinationBounds)
        weak var weakSelf = self
        UIView.animate(withDuration: 0.20, animations: { () -> Void in
            weakSelf!.mapViewRequest.animate(to: weakSelf!.mapViewRequest.camera(for: weakSelf!.mapBounds, insets: UIEdgeInsetsMake(30.0, 30.0, 200, 30.0))!)
        })
        if isRequestForCar == true {
            self.makeRequestToBookACarWithCarDetails(self.arrOndemanCarsSelected.first!)
            self.isRequestForCar = false
        }
        
        
        // Do any   additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
       // appDelegate.delegateJobStatusAction = self
       // appDelegate.delegateJobStatusAction = self
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
func showPickUpLocationOnMap(){
        
        let marker = GMSMarker()
        let latitude: Double = CDouble(locationManagerObjct.pickUpLocation!.coordinate.latitude)
        let longitude: Double = CDouble(locationManagerObjct.pickUpLocation!.coordinate.longitude)
        let postion = CLLocationCoordinate2DMake(latitude, longitude)
        marker.position = postion
        let carObject = self.arrOndemanCarsSelected.first!
        let imageCar = self.showEtaMethodsOnMapWith(self.dicArrivalTimeOfVehicleType[carObject.vehicalType_title] as! String, imageString: "blue.png")
        marker.icon = imageCar
        marker.userData = carObject
        marker.map = self.mapViewRequest
        
        
    }
func showDriverLocationOnMap (){
        
        if let carObject = self.arrOndemanCarsSelected.first {
            let marker = GMSMarker()
            let latitude: Double = CDouble(carObject.latitude)!
            let longitude: Double = CDouble(carObject.longitude)!
            let postion = CLLocationCoordinate2DMake(latitude, longitude)
            marker.position = postion
            let imageData : Data? = Globals.sharedInstance.getValueFromUserDefaultsForKey(carObject.vehicalType_title) as? Data
            if imageData != nil {
                self.image_MapPinCar = imageData!
            }else {
                self.image_MapPinCar = Data().vehicleImageForType(carObject.vehicle_type_id)
            }
           if let imageCar = UIImage(data: self.image_MapPinCar, scale: 1.5)
              {
             marker.icon = imageCar
           }else {
              marker.icon = UIImage(named: "sedan")
            }
            
            marker.userData = carObject
            marker.map = self.mapViewRequest
        }
        
    }
func showEtaMethodsOnMapWith(_ etaString:String, imageString:String) -> UIImage{
        
        let viewBack = UIView(frame: CGRect(x: 0,y: 0,width: 100,height: 100))
        viewBack.backgroundColor = UIColor.clear
        let pinImageView : UIImageView = UIImageView(image: UIImage(named: imageString))
        
        
        self.etaLblShowTime = UILabel(frame: CGRect(x: 33,y: 20,width: 40,height: 30))
        self.etaLblShowTime.textAlignment = .center
        self.etaLblShowTime.numberOfLines = 2
        self.etaLblShowTime.text = etaString
        // self.etaLblShowTime.backgroundColor = UIColor.brownColor()
        self.etaLblShowTime.textColor = UIColor.white
        self.etaLblShowTime.font = UIFont(name: "Oxygen", size: 10)
        viewBack.addSubview(pinImageView)
        viewBack.addSubview(self.etaLblShowTime)
        // viewBack.bringSubviewToFront(self.etaLblShowTime)
        if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
            UIGraphicsBeginImageContextWithOptions(viewBack.frame.size, false, UIScreen.main.scale)
        }else {
            UIGraphicsBeginImageContext(viewBack.frame.size)
        }
        viewBack.layer.render(in: UIGraphicsGetCurrentContext()!)
        self.finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return self.finalImage
    }
    // MARK: -=====Request Send to Book A Car
func makeRequestToBookACarWithCarDetails(_ carSelected:OnDemandCarsAvailable)
    {
        
        if carSelected.fleet_id != nil && carSelected.driver_id != nil {
            
            var dicParameters = [String:Any]()
            dicParameters["user_id"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            dicParameters["session"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            let arrCurentDate = Globals.sharedInstance.getCurrentDate().components(separatedBy: CharacterSet.whitespaces)
            dicParameters["pu_date"] = arrCurentDate[0] as Any?
            dicParameters["pu_time"] = arrCurentDate[1] as Any?
            
            dicParameters["vechile_type"] = carSelected.vehicle_type_id as Any?
            
            dicParameters["pickup_location"] = self.locationManagerObjct.strCurrentLocation as Any?
            dicParameters["pickup_latitude"] = String(locationManagerObjct.pickUpLocation!.coordinate.latitude) as Any?
            dicParameters["pickup_longitude"] = String(locationManagerObjct.pickUpLocation!.coordinate.longitude) as Any?
            dicParameters["dropoff_location"] = locationManagerObjct.dicDestinationLocation!["address"] as! String as Any?
            dicParameters["dropoff_latitude"] = String(locationManagerObjct.destinationLocation!.coordinate.latitude) as Any?
            dicParameters["dropoff_longitude"] = String(locationManagerObjct.destinationLocation!.coordinate.longitude) as Any?
            dicParameters["fleet_id"] = carSelected.fleet_id! as Any?
            dicParameters["driver_id"] = carSelected.driver_id! as Any?
            dicParameters["credit_card_id"] = self.CardID as Any?
            dicParameters["company_id"] = Constants.CompanyID as Any?
            weak var weakSelf = self
            ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.BookACar, dicParameters: dicParameters, successBlock: { (dicData) -> Void in
                }, failureBlock: { (error) -> () in
                    appDelegate.hideProgress()
                    let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
                    alert.show()
            })
            
            
        }
        
    }
    // MARK: ======job Accepted Notification Methods===============
    
func jobAccptedNotificationMethod(){
        
        weak var weakSelf = self
        UIView.animate(withDuration: 0.30, delay: 0.30, options: UIViewAnimationOptions(), animations: { () -> Void in
            //weakSelf!.constraintBottomViewPickUp.priority = UILayoutPriorityDefaultLow
            weakSelf?.viewPickupDropOff.translatesAutoresizingMaskIntoConstraints = true
            weakSelf!.viewPickupDropOff.frame = CGRect(x: 0,y: 0 , width: weakSelf!.view.frame.width, height: 55)
            //  weakSelf!.constraintTopViewPickup.priority = UILayoutPriority.abs(1000.0)
            
            }, completion: nil)
        
        UIView.animate(withDuration: 0.20, animations: { () -> Void in
            weakSelf!.mapViewRequest.animate(to: weakSelf!.mapViewRequest.camera(for: weakSelf!.mapBounds, insets: UIEdgeInsetsMake(150.0, 30.0, 50, 30.0))!)
        })
        
        
    }
func jobStatusNotificationMethod(_ dicNotification: [String : Any]) {
        self.saveDriverDetailsWhenJobAccepted(dicNotification)
        self.jobAccptedNotificationMethod()
    }
func RemoteNotificationTapAction() {
        self.jobAccptedNotificationMethod()
    }
func saveDriverDetailsWhenJobAccepted(_ dicJobDetails:[String:Any])
    {
        
        let driverObject = DriverDetails()
        if let cellNumber = dicJobDetails["cellular_phone"] {
            driverObject.cellular_phone = cellNumber as! String
        }
        if let first_name = dicJobDetails["first_name"] {
            driverObject.firstName = first_name as! String
        }
        if let last_name = dicJobDetails["last_name"] {
            driverObject.lastName = last_name as! String
        }
        if let image_url = dicJobDetails["image_url"] {
            driverObject.image_url = image_url as! String
        }
        if let fleet_name = dicJobDetails["fleet_name"] {
            driverObject.fleet_Name = fleet_name as! String
        }
        if let pickup = dicJobDetails["pickup"] {
            driverObject.pickUp_Loc = pickup as! String
        }
        if let dropOff = dicJobDetails["dropoff"] {
            let arrDropOff = (dropOff as! String).components(separatedBy: "place_id")
            if arrDropOff.contains("place_id"){
                driverObject.dropOff_Loc = arrDropOff[0]
            }else {
                driverObject.dropOff_Loc = dropOff as! String
            }
        }
        if let licenceNo = dicJobDetails["licenceno"] {
            driverObject.licence_number = licenceNo as! String
        }
        if let fleet_image = dicJobDetails["fleet_image"] {
            driverObject.fleet_image = fleet_image as! String
        }
        if let vehicle_type_title = dicJobDetails["vehicle_type_title"] {
            driverObject.vehicle_type_title = vehicle_type_title as! String
        }
        if let availableStatus = dicJobDetails["availablestatus"] {
            driverObject.response_Status = availableStatus as! String
        }
        if let status = dicJobDetails["status"] {
            driverObject.jobStatus = status as! String
        }
        if let driver_id = dicJobDetails["driver_id"] {
            driverObject.driver_ID = driver_id as! String
        }
        
        if let dicRates = dicJobDetails["rate"] as? [String:Any] {
            
            if let base_fare = dicRates["base_fare"] {
                driverObject.base_fare = base_fare as! String
            }
            if let mile_rate = dicRates["mile_rate"] {
                driverObject.mile_rate = mile_rate as! String
            }
            if let min_fare = dicRates["min_fare"] {
                driverObject.min_fare = min_fare as! String
            }
            if let min_rate = dicRates["min_rate"] {
                driverObject.min_rate = min_rate as! String
            }
        }
        Globals.sharedInstance.activeDriver = driverObject
        Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(dicJobDetails, key:Constants.activeJobNotification)
        
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    func backButtonActionMethod(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func btnCancelRequestAction(_ sender: UIButton) {
        
        
        
    }
}
