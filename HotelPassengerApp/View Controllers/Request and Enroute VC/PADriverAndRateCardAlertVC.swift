//
//  PADriverAndRateCardAlertVC.swift
//  PassangerApp
//
//  Created by Netquall on 2/9/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader
class PADriverAndRateCardAlertVC: UIViewController {

    // MARK:-============ Property decalaration ==========

    var isDriverProfile : Bool!
    var dicDetails : [String:Any]!
    @IBOutlet weak var viewAlertDetails: UIView!
    @IBOutlet weak var btnTopTitleOutlet: UIButton!
    
    @IBOutlet weak var lblDriverNameOrMinFare: UILabel!
    
    @IBOutlet weak var viewDriverProfile: UIView!
    @IBOutlet weak var lblVehicleOrMinRateTitle: UILabel!
    
    @IBOutlet weak var lblDriverNameOnly: UILabel!
    @IBOutlet weak var lblVehicleNameOrMinRate: UILabel!
    
    @IBOutlet weak var lblLicenceOrMileRateTitle: UILabel!
    
    @IBOutlet weak var lblLicenceNumberOrMileRate: UILabel!
    
     var imageDriver: DLImageView!
    @IBOutlet weak var imageDriverOutlet : UIImageView!
    @IBOutlet weak var txtPhoneNumberDriver: UITextField!
    var currency = ""
    // MARK: - ======== View LIfe Cycyle   ========
 
override func viewDidLoad() {
        super.viewDidLoad()
  
   self.lblDriverNameOrMinFare.numberOfLines = 3
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PADriverAndRateCardAlertVC.tapGestureAction(_:)))
       self.view.addGestureRecognizer(tapGesture)
        for viewSub in self.viewAlertDetails.subviews {
            if viewSub.isKind(of: UIView.self){
                if viewSub.tag == 40 || viewSub.tag == 41 {
                 viewSub.layer.borderWidth = 1.0
                viewSub.layer.borderColor = UIColor.appGrayColor().cgColor
                }
                continue
            }
        }
        if self.isDriverProfile == true {
            self.setupDriverDetails()
        }else {
            self.setupFareViewDetails()
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.bringSubview(toFront: self.viewAlertDetails)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - ======== view setup for driver details   ========

    
func setupDriverDetails(){
    self.viewAlertDetails.translatesAutoresizingMaskIntoConstraints = true
    
    Globals.layoutViewFor(self.viewAlertDetails, color: UIColor.appThemeColor(), width: 2, cornerRadius: 10)
    
    if Constants.iPadScreen == true {
     //   self.viewAlertDetails.frame.size.height = 343
     //   self.viewAlertDetails.frame.size.width = 400
      //  self.viewAlertDetails.center = self.view.center
    }else {
    self.viewAlertDetails.frame.size.height = 270
    self.viewAlertDetails.frame.size.width = self.view.frame.width - 32
    }
        self.lblVehicleOrMinRateTitle.text = "VEHICLE"
        self.lblLicenceOrMileRateTitle.text = "LICENCE"
        self.btnTopTitleOutlet.setTitle("Driver Profile", for: UIControlState())
        self.btnTopTitleOutlet.isHidden = true
       self.lblDriverNameOrMinFare.isHidden = true
    self.viewDriverProfile.isHidden = false
    self.btnTopTitleOutlet.setImage(UIImage(named: "userIcon"), for: UIControlState())
    self.lblDriverNameOnly.text = (self.dicDetails[Constants.DriverName] as! String)
    self.txtPhoneNumberDriver.text = (self.dicDetails[Constants.DriverPhone] as! String)
    self.lblLicenceNumberOrMileRate.text = self.dicDetails[Constants.DriverLicence] as? String
    self.lblVehicleNameOrMinRate.text = self.dicDetails[Constants.VehicleName] as? String
    //let request = NSURLRequest(URL: NSURL(string:self.dicDetails[Constants.DriverImage] as! String)!)
    
    self.txtPhoneNumberDriver.leftView = UIView(frame: CGRect(x: 0,y: 0,width: 60,height: 20))
    self.txtPhoneNumberDriver.leftViewMode = .always
    
    self.imageDriverOutlet.image = UIImage(named: "profile_placeholder")
    
    if let str = self.dicDetails[Constants.DriverImage] as? String{
        DLImageLoader.sharedInstance().image(fromUrl: str) { [weak self] (error, image) -> () in
            if image != nil
             {
                self?.imageDriverOutlet.image = image
            }
            else {
            }
        }
        }
    else {
         self.imageDriverOutlet.image = UIImage(named: "profile_placeholder")
       // driver_img
    }
    
    
    if Constants.iPadScreen == true {
       self.imageDriverOutlet.layer.cornerRadius = self.imageDriverOutlet.frame.height / 2.0
    }else {
        self.imageDriverOutlet.layer.cornerRadius = 35.0
    }
    
    self.imageDriverOutlet.layer.masksToBounds = true
   // self.imageDriverOutlet.layer.borderWidth = 1.0
    //self.imageDriverOutlet.layer.borderColor = UIColor.whiteColor().CGColor
}
    // MARK: - ======== View setup for fare details  ========

    func setupFareViewDetails(){
        
        self.viewAlertDetails.translatesAutoresizingMaskIntoConstraints = true
        
        if Constants.iPadScreen == true {
           // self.viewAlertDetails.frame.size.height = 270
           // self.viewAlertDetails.frame.size.width = 500
           // self.viewAlertDetails.center = self.view.center
        }else {
            self.viewAlertDetails.frame.size.height = 230
            self.viewAlertDetails.frame.size.width = self.view.frame.width - 32
        }
        self.btnTopTitleOutlet.isHidden = false
        self.lblDriverNameOrMinFare.isHidden = false
        
        if (appDelegate.distanceUnit == "KM"){
            self.lblLicenceOrMileRateTitle.text = "PER KM RATE"
        }
        else {
            self.lblLicenceOrMileRateTitle.text = "PER MILE RATE"
        }
        
        self.lblVehicleOrMinRateTitle.text = "PER MIN RATE"
        self.btnTopTitleOutlet.setTitle("Rate Card", for: UIControlState())
        self.viewDriverProfile.isHidden = true
        self.btnTopTitleOutlet.setImage(UIImage(named: "rate_card"), for: UIControlState())
        if self.dicDetails[Constants.MinFare] as! String  != "" {
            
            let strMinFare = " \(currency) \(self.dicDetails[Constants.MinFare] as! String ) Minimum Fare"
            let strBaseFare = " \(currency) \(self.dicDetails[Constants.BaseFare] as! String ) Base Fare"
            let strCompleteFare = "\(strMinFare)\n\n" + "\(strBaseFare)"
            let attributedText = NSMutableAttributedString(string:strCompleteFare)
            let attrsMinFare      = [NSFontAttributeName: Globals.defaultAppFontWithBold(16), NSForegroundColorAttributeName: UIColor.appBlackColor()]
            let rangeMInFare = NSString(string: strCompleteFare).range(of: strMinFare)
            attributedText.addAttributes(attrsMinFare, range: rangeMInFare)
            let attrsBaseFare = [NSFontAttributeName: Globals.defaultAppFontWithBold(15), NSForegroundColorAttributeName: UIColor.appBlackColor()]
            let rangeBaseFare = NSString(string: strCompleteFare).range(of: strBaseFare)
            attributedText.addAttributes(attrsBaseFare, range: rangeBaseFare)
            self.lblDriverNameOrMinFare.attributedText = attributedText
            
            
        }else {
            self.lblDriverNameOrMinFare.text = "N/A"
        }
            
            var strPerMileRate = ""
            if (appDelegate.distanceUnit == "KM"){
                strPerMileRate =   "\(currency)  \(self.dicDetails[Constants.PerKMRate] as! String)"
            }
            else {
                strPerMileRate =   "\(currency)  \(self.dicDetails[Constants.PerMileRate] as! String)"
            }
        
        self.lblLicenceNumberOrMileRate.text = strPerMileRate + "/\(appDelegate.distanceUnit!)"
        
     
        if self.dicDetails[Constants.PerMinRate] as! String  != "" {
            let strPerMileRate = "\(currency)  \(self.dicDetails[Constants.PerMinRate] as! String)"
            self.lblVehicleNameOrMinRate.text = strPerMileRate + "/min"
        }
        else {
            self.lblVehicleNameOrMinRate.text = "N/A"
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tapGestureAction(_ tapGesture:UITapGestureRecognizer){
        
        self.dismiss(animated: true, completion: nil)
    }
}
