//
//  ReceiptViewController.swift
//  PassangerApp
//
//  Created by Netquall on 2/25/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader
import GoogleMaps


class ReceiptViewController: BaseViewController, BackButtonActionProtocol {
    
    // MARK:-============ Property decalaration ==========

    @IBOutlet var viewReceiFareDisTimeDetails: UIView!
    
    @IBOutlet var viewReciPickUpDropOffDetails: UIView!
    
    @IBOutlet var viewRecpDateVehicleDetails: UIView!
    
    @IBOutlet var mapRecpViewDetails: UIView!
    
    @IBOutlet var viewRecpStopsDetails: UIView!
    
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var driverImageView: UIImageView!
    @IBOutlet weak var tblReservationDetails: UITableView!
    var mapBounds : GMSCoordinateBounds!
    lazy var dicDataSource = [String:Any]()
    lazy var arrStops = [String]()
    var driverID:String!
    var ipadPadding : CGFloat = 0.0
    var ipadMap : CGFloat = 0.0
    
    // MARK:-============ view life cycle ==========
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dicDataSource = self.dicDataSource.formatDictionaryForNullValues(self.dicDataSource)!
        if Constants.iPadScreen == true{
            self.ipadPadding = 30.0
            self.ipadMap = 200.0
        }
       
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Receipt", imageName: "backArrow")
        Globals.sharedInstance.delegateBack = self
       
        self.tblReservationDetails.contentInset = UIEdgeInsetsMake(5, 0, 0, 0)
        // Do any additional setup after loading the view.
        self.tblReservationDetails.reloadData()
        
      self.driverImageView.layer.borderColor = UIColor.white.cgColor
        self.driverImageView.layer.borderWidth =  2.0
        self.driverImageView.layer.cornerRadius = self.driverImageView.frame.height / 2.0
        self.driverImageView.layer.masksToBounds = true
        self.lblDriverName.text = "\(self.dicDataSource["first_name"]!) \(self.dicDataSource["last_name"]!)"
        
        
        if appDelegate.networkAvialable == true {
            if let keyForImage = self.dicDataSource["image"]{
                DLImageLoader.sharedInstance().image(fromUrl: keyForImage as! String, completed: {[weak self] (error, image) -> () in
                    if image != nil {
                        self?.driverImageView!.image = image
                    }else {
                        self?.driverImageView!.image = UIImage(named: "profileImage")
                    }
                })
            }
            else {
                self.driverImageView!.image = UIImage(named: "profileImage")
            }
            
        }

    }
    override func viewDidLayoutSubviews() {
            self.mapRecpViewDetails.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 128 + self.ipadMap)
            self.driverImageView.layer.cornerRadius = self.driverImageView.frame.height / 2.0
            self.driverImageView.layer.masksToBounds = true
    }
    override func viewWillAppear(_ animated: Bool) {
        Globals.sharedInstance.delegateBack = self
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        Globals.sharedInstance.delegateBack = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // var mapBounds : GMSCoordinateBounds!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK:-============ btn Back Action  ==========

    func backButtonActionMethod(_ sender: UIButton) {
        
        Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.activeJobNotification)
        Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.currentJobStaus)
        
//        dispatch_async(dispatch_get_main_queue(), { [weak self] () -> Void in
//            self?.performSegueWithIdentifier("backToMenuView", sender: self)
//        })
        
            DispatchQueue.main.async { [weak self]() -> Void in
                if self == nil {
                    return
                }
                for vc: UIViewController in (self?.navigationController?.viewControllers)! {
                    if (vc.isKind(of: SWRevealViewController.self)) {
                        self?.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }

    }
    
    // MARK:-============ Get Address from Latitude and Longitude ==========

    func getAddressLatLongiFromAddressString(_ strAddress: String) -> CLLocation {
        
        var latitude:Double = 0
        var longitude:Double = 0
        
        let esc_addr: String = strAddress.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let req = Globals.sharedInstance.getFullApiAddressForAddressString(esc_addr)
        var result : String!
        do {
            result = try String(contentsOf: URL(string: req)!, encoding: String.Encoding.utf8)
        }catch {
            print("json Error-::\(error)")
            
        }
        
        if result  != "" && result  != nil{
            let scanner: Scanner = Scanner(string: result)
            
            if scanner.scanUpTo("\"lat\" :", into: nil) && scanner.scanString("\"lat\" :", into: nil) {
                scanner.scanDouble(&latitude)
                if scanner.scanUpTo("\"lng\" :", into: nil) && scanner.scanString("\"lng\" :", into: nil) {
                    scanner.scanDouble(&longitude)
                }
            }
        }
        
        let location = CLLocation(latitude: latitude, longitude: longitude)
        return location
    }
    
    
    // MARK:-============table View Data Source  ==========

    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int
    {
        if  self.arrStops.count == 0{
            return 4
        }else {
            return 5
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if self.arrStops.count == 0{
            return 0
        }else {
            if section == 3 {
                return self.arrStops.count
            }else {
                return 0
            }
        }
        
        
    }
  
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        var cell : UITableViewCell?
        
        let  cellIndetifier = "cellRideLaterWithStops"
        //cellCarInfo
        cell  = self.tblReservationDetails.dequeueReusableCell(withIdentifier: cellIndetifier)!
        var lblText : UILabel!
        var viewOuter : UIView!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
            lblText = cell!.viewWithTag(63) as! UILabel
            viewOuter = cell!.viewWithTag(62)! as UIView
            
        }else {
            lblText = cell!.viewWithTag(63) as! UILabel
            viewOuter = cell!.viewWithTag(62)! as UIView
            
        }
        viewOuter.layer.borderColor = UIColor.lightGray.cgColor
        viewOuter.layer.borderWidth = 1.0
        //let dicStops = self.arrStops[indexPath.row]
        lblText.text = self.arrStops[indexPath.row]
        lblText.numberOfLines = 4
        lblText.font = Globals.defaultAppFont(14)
        lblText.textColor = UIColor.black
        return cell!
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch (section){
            
        case 0:
            
            return 150 + self.ipadMap
        case 1:
            return 155 + self.ipadPadding
        case 2:
            return 114 + self.ipadPadding
        case 3:
            if  self.arrStops.count == 0{
                return 70 + self.ipadPadding
            }else {
                return 30
            }
            
        case 4:
            return 70 + self.ipadPadding
            
        default :
            return 60 + self.ipadPadding
        }
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        switch(section){
        case 0:
            self.mapRecpViewDetails.translatesAutoresizingMaskIntoConstraints = true
            
              // self.driverImageView.translatesAutoresizingMaskIntoConstraints = true
            return self.mapRecpViewDetails
        case 1:
            let strDateTime = Globals.getDateFormatForDateString("\(self.dicDataSource["do_date"]!) \(self.dicDataSource["do_time"]!)")
            let lblDate = self.viewRecpDateVehicleDetails.viewWithTag(20) as! UILabel
            let lblTime = self.viewRecpDateVehicleDetails.viewWithTag(21) as! UILabel
            
            lblDate.text = (self.dicDataSource["do_date"] as! String)
            lblTime.text = (self.dicDataSource["do_time"]! as! String)

            let lblVehicleType = self.viewRecpDateVehicleDetails.viewWithTag(22) as! UILabel
            lblVehicleType.text = (self.dicDataSource["vehicle_type_title"]! as! String)
            let lblVehicleName = self.viewRecpDateVehicleDetails.viewWithTag(23) as! UILabel
            lblVehicleName.text = (self.dicDataSource["description"]! as! String)
            self.viewRecpDateVehicleDetails.translatesAutoresizingMaskIntoConstraints = true
            self.viewRecpDateVehicleDetails.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 155 + self.ipadPadding )
            return  self.viewRecpDateVehicleDetails
        case 2:
            let lblPickUp = self.viewReciPickUpDropOffDetails.viewWithTag(20) as! UILabel
            
            lblPickUp.text = (self.dicDataSource["pickup"] as! String)
            
            let lblDropOff = self.viewReciPickUpDropOffDetails.viewWithTag(21) as! UILabel ;
            
            if let strDrop = self.dicDataSource["dropoff"] as? String {
                lblDropOff.text = strDrop
            }else {
                lblDropOff.text = "N/A"
            }
            
            self.viewReciPickUpDropOffDetails.translatesAutoresizingMaskIntoConstraints = true
            self.viewReciPickUpDropOffDetails.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 114 + self.ipadPadding )
            
            return  self.viewReciPickUpDropOffDetails
        case 3:
            if self.arrStops.count == 0 {
                self.setupFareDisTimeViewDetais()
                return self.viewReceiFareDisTimeDetails
            }else {
                self.viewRecpStopsDetails.translatesAutoresizingMaskIntoConstraints = true
                self.viewRecpStopsDetails.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 30)
                return self.viewRecpStopsDetails
            }
            
        case 4:
            self.setupFareDisTimeViewDetais()
            return  self.viewReceiFareDisTimeDetails
        default :
            break
            
        }
        return nil
    }
    
    func setupFareDisTimeViewDetais(){
        self.viewReceiFareDisTimeDetails.layer.borderColor = UIColor.lightGray.cgColor
        self.viewReceiFareDisTimeDetails.layer.borderWidth = 1.0
        
        let lblFare = self.viewReceiFareDisTimeDetails.viewWithTag(20) as! UILabel
        if (self.dicDataSource["grand_total"] as! String).characters.count != 0 {
             let estFare = Float(self.dicDataSource["grand_total"] as! String)!
            lblFare.text =   String(format: "%@ %.2f", appDelegate.currency, estFare)
        }else {
            lblFare.text = "N/A"
        }
        
        
        let lblDistance = self.viewReceiFareDisTimeDetails.viewWithTag(21) as! UILabel
        if let dis = self.dicDataSource["distance"]{
            if "\(dis)".characters.count != 0 {
                lblDistance.text = "\(dis) \(appDelegate.distanceUnit!)"
            }else {
                lblDistance.text = "N/A"
            }
        }
        let lblTime = self.viewReceiFareDisTimeDetails.viewWithTag(22) as! UILabel
        
        if (self.dicDataSource["time"] as! String).characters.count != 0 {
            lblTime.text = Globals.sharedInstance.formatArrivalTimeStringWith((self.dicDataSource["time"]! as! String))
        }else {
            lblTime.text = "N/A"
        }
        
        self.viewReceiFareDisTimeDetails.translatesAutoresizingMaskIntoConstraints = true
        self.viewReceiFareDisTimeDetails.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 70 + self.ipadPadding)
    }
    
    // MARK:-============btn rate Your Ride action  ==========

    @IBAction func btnRateYourRideClicked(_ sender: Any) {
       // self.performSegueWithIdentifier("goToRatingView", sender: self)
    }
    
    // MARK: - ====Navigation========
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "goToRatingView"{
            
            let ratingVC = segue.destination as! PARateDriverVC
            ratingVC.reservationId = self.dicDataSource["reservation_id"] as! String
            ratingVC.driverID = self.driverID!
        }
    }
    
}
