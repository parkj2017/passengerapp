//
//  PAMyFavouriteDriverViewController.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 30/06/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader
class tableViewCellTypeFavDriver:UITableViewCell
{
    @IBOutlet var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet var imageDriver: UIImageView!
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var ratingView: CosmosView!
    @IBOutlet var lblPlaceHolder: UILabel!
    @IBOutlet var activityView: UIActivityIndicatorView!
    
    func setUpStarRating(_ rating:Double){
        ratingView.rating = rating
    }
    func setupDriverView(_ imageUrl:String)  {
        
        self.activityView.isHidden = false;
        self.activityView.startAnimating()
        if appDelegate.networkAvialable == true {
            DLImageLoader.sharedInstance().image(fromUrl: imageUrl, completed: { [weak self](error, image) -> () in
                DispatchQueue.main.async(execute: { () -> Void in
                    self?.activityView.stopAnimating()
                    self?.activityView.isHidden = true;
                    if image != nil {
                        self?.imageDriver.image = image
                    }else {
                        self?.imageDriver.image = UIImage(named: "profile_placeholder")
                    }
                })
            })
        }
        else {
            DispatchQueue.main.async(execute: { () -> Void in
                self.activityView.stopAnimating()
                self.activityView.isHidden = true;
                self.imageDriver.image = UIImage(named: "profile_placeholder")
            })
        }
        
    }
    
    override func  awakeFromNib() {
        if Constants.iPadScreen {
            self.imageWidthConstraint.constant = 86.0 * 1.5
            self.imageHeightConstraint.constant = 86.0 * 1.5
            self.imageDriver.layer.cornerRadius =  (86.0 * 1.5 ) / 2.0
            self.layoutIfNeeded()
            self.updateConstraintsIfNeeded()
        }
        self.imageDriver.layer.masksToBounds = true
        self.imageDriver.layer.borderColor = UIColor.appBlackColor().cgColor
        self.imageDriver.layer.borderWidth = 1.0
        self.lblDriverName.font = Globals.defaultAppFont(14)
        self.lblPlaceHolder.font = Globals.defaultAppFont(14)
        
    }
    
}

class PAMyFavouriteDriverViewController: UIViewController , BackButtonActionProtocol{
    
    var arrFavDrivers:[[String:Any]]!
    
    @IBOutlet var tblFavDrivers: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden =  false
        
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("My Favorite Drivers", imageName: "backArrow")
        self.getFavDriversList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
        self.navigationController?.isNavigationBarHidden = false
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Globals.sharedInstance.delegateBack = nil
        
    }
    
    func backButtonActionMethod(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- ======= table View Data Source =============
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if self.arrFavDrivers == nil{
            return 1 //to show loading message on cell
        }
        else  if self.arrFavDrivers.count == 0{
            return 1 //to show no driver found on cell
        }
        return self.arrFavDrivers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        var cell   = tblFavDrivers.dequeueReusableCell(withIdentifier: "tableViewCellFavDriversIdentifier") as? tableViewCellTypeFavDriver
        if cell == nil{
            cell = UITableViewCell(style: .default, reuseIdentifier: "tableViewCellFavDriversIdentifier")  as? tableViewCellTypeFavDriver
        }
        
        if indexPath.row % 2 == 0{
            cell?.backgroundColor = UIColor(red: 214.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0)
        }
        else {
            cell?.backgroundColor = UIColor.white
        }
        
        cell!.lblPlaceHolder.isHidden = false
        if self.arrFavDrivers == nil{
            cell!.lblPlaceHolder.text = "  Loading..."
        }
        else  if self.arrFavDrivers.count == 0{
            cell!.lblPlaceHolder.text = "  No Favorite Driver Found"
        }
        else {
            
            cell!.lblPlaceHolder.isHidden = true
            
            cell?.ratingView.totalStars = 5
            cell?.ratingView.isUserInteractionEnabled = false
            
            let dic = self.arrFavDrivers[indexPath.row]
            
            var driverName = ""
            if let fName = dic["first_name"] as? String{
                driverName = fName
            }
            if let lName = dic["last_name"] as? String{
                driverName =  driverName + " " + lName
            }
            
            cell?.setupDriverView(dic["image"] as! String)
            if let rating = dic["totalRating"] as? String{
                cell?.setUpStarRating(rating.toDouble()!)
            }
            else {
                cell?.setUpStarRating(0.0)
            }
            cell?.lblDriverName.text = driverName
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return Constants.iPadScreen == true ? 104.0 * 1.5 : 104.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return nil
    }
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
        if self.arrFavDrivers.count == 0{
            return
        }
        var dicParam = [String:Any]()
        
        let dic = self.arrFavDrivers[indexPath.row]
        
        dicParam["primary_driver"] = dic["id"] as! String as Any?
        dicParam["reservation_id"] = "" as Any?
        
        let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "PADriverDetailsViewController") as! PADriverDetailsViewController
        detailsVc.dicDriverInfo = dicParam
        detailsVc.isEditable = true
        detailsVc.didFinishDeletingFavDriver = {[weak self] () -> () in
            self?.getFavDriversList()
        }
        detailsVc.view.backgroundColor = UIColor.clear
        detailsVc.modalPresentationStyle = .overFullScreen
        detailsVc.providesPresentationContextTransitionStyle = true
        detailsVc.definesPresentationContext = true
        
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overFullScreen
        self.present(detailsVc, animated: true, completion: nil)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getFavDriversList(){
        if appDelegate.networkAvialable == true {
            var dicParameters = [String:Any]()
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            dicParameters["user_id"] = user_ID
            dicParameters["session"] = session
            
            appDelegate.showProgressWithText("Fetching Favorite drivers.....")
            
            DispatchQueue.main.async(execute: {[weak self] () -> Void in
                self?.tblFavDrivers.reloadData()
            })
            ServiceManager.sharedInstance.dataTaskWithPostRequest("favouriteList", dicParameters: dicParameters, successBlock: {[weak self] (dicData) -> Void in
                appDelegate.hideProgress()
                if self == nil {
                    return
                }
                if self?.arrFavDrivers == nil{
                    self?.arrFavDrivers = [[String:Any]]()
                }
                else {
                    self?.arrFavDrivers.removeAll()
                }
                DispatchQueue.main.async(execute: {[weak self] () -> Void in
                    self?.tblFavDrivers.reloadData()
                })
                
                if let status = dicData["status"] as? String{
                    if status == "success"{
                        if let arrDict = dicData["record"] as? [[String:Any]]{
                            self?.arrFavDrivers = arrDict
                        }
                    }
                    DispatchQueue.main.async(execute: {[weak self] () -> Void in
                        self?.tblFavDrivers.reloadData()
                    })
                }
                })
            { (error) -> () in
                let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
            }
            
        }else {
            appDelegate.hideProgress()
            
            let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
