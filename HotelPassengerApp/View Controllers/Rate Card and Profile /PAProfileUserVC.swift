//
//  PAProfileUserVC.swift
//  PassangerApp
//
//  Created by Netquall on 2/22/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader
class PAProfileUserVC: BaseViewController {
    
    // MARK:-============ Property decalaration ==========

    @IBOutlet weak var viewTextFeildS: UIView!
    
    @IBOutlet weak var imageUserProfile: UIImageView!
    // MARK:-====View Life cycle =======
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        
        let btnPayment = self.viewTextFeildS.viewWithTag(204) as! UIButton
        btnPayment.layer.borderColor = UIColor.appBlackColor().cgColor
        btnPayment.layer.borderWidth = 1.0
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
//        self.navigationController?.navigationBar.SetNavigationBarThemeColor()
       self.navigationItem.leftBarButtonItem =  self.leftBackButtonWithCustom()
        if Constants.iPadScreen == true {
            self.imageUserProfile.layer.cornerRadius = 125.0
        }else {
            self.imageUserProfile.layer.cornerRadius = 50.0
        }
        
        self.imageUserProfile.layer.masksToBounds = true
        let userDetails = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        var imageNil = true
        if let imageUrl = userDetails["image_url"] {
            if imageUrl as! String != "" {
                if appDelegate.networkAvialable == true {
                    DLImageLoader.sharedInstance().image(fromUrl: imageUrl as! String, completed: { [weak self](error, image) -> () in
                        if image != nil {
                            DispatchQueue.main.async(execute: { () -> Void in
                                self?.imageUserProfile.image = image
                                self?.imageUserProfile.layer.borderColor = UIColor.white.cgColor
                                self?.imageUserProfile.layer.borderWidth =  2.0
                                
                            })
                        }else {
                            self?.imageUserProfile.image = UIImage(named: "profileImage")
                        }
                        imageNil = false
                        })
                    
                }
            }
        }
        if imageNil == true {
            self.imageUserProfile.image = UIImage(named: "profileImage")
        }
        var lblWidth : CGFloat = 80
        var hightView : CGFloat = 50
        if Constants.iPadScreen == true {
            lblWidth = 130
            hightView = 70
        }
        
        let lblTextField = UILabel()
        lblTextField.font = Globals.defaultAppFont(13)
        lblTextField.text = "First Name :"
        lblWidth = lblTextField.intrinsicContentSize.width
        lblTextField.frame = CGRect(x: 10,y: 0,width: lblWidth,height: hightView)
        let txtFrstName = self.viewTextFeildS.viewWithTag(200) as! UITextField
        
        let view1 = UIView(frame: CGRect(x: 0,y: 0,width: lblWidth + 15 ,height: hightView))
        view1.addSubview(lblTextField)
        txtFrstName.leftView = view1
        txtFrstName.leftViewMode = .always
        txtFrstName.layer.borderColor = UIColor.lightGray.cgColor
        txtFrstName.layer.borderWidth = 1.0
        txtFrstName.text = (userDetails["first_name"] as! String).capitalized
        
        
        let lblTextField1 = UILabel()
        lblTextField1.font = Globals.defaultAppFont(13)
        lblTextField1.text = "Last Name :"
        lblWidth = lblTextField1.intrinsicContentSize.width
        lblTextField1.frame = CGRect(x: 10,y: 0,width: lblWidth,height: hightView)
        let view2 = UIView(frame: CGRect(x: 0,y: 0,width: lblWidth + 15,height: hightView))
        let txtLastName = self.viewTextFeildS.viewWithTag(201) as! UITextField
        view2.addSubview(lblTextField1)
        txtLastName.leftView = view2
        txtLastName.leftViewMode = .always
        txtLastName.layer.borderColor = UIColor.lightGray.cgColor
        txtLastName.layer.borderWidth = 1.0
        txtLastName.text = (userDetails["last_name"] as! String).capitalized
        
        
        let lblTextField2 = UILabel()
        lblTextField2.font = Globals.defaultAppFont(13)
        lblTextField2.text = "Phone Number :"
        lblWidth = lblTextField2.intrinsicContentSize.width
        lblTextField2.frame = CGRect(x: 10,y: 0,width: lblWidth,height: hightView)
        let view3 = UIView(frame: CGRect(x: 0,y: 0,width: lblWidth + 20,height: hightView))
        let txtPhone = self.viewTextFeildS.viewWithTag(202) as! UITextField
        view3.addSubview(lblTextField2)
        txtPhone.leftView = view3
        txtPhone.leftViewMode = .always
        txtPhone.layer.borderColor = UIColor.lightGray.cgColor
        txtPhone.layer.borderWidth = 1.0
        txtPhone.text = String(describing: userDetails["mobile"]!)
        
        
        let lblTextField3 = UILabel()
        lblTextField3.font = Globals.defaultAppFont(13)
        let txtEmail = self.viewTextFeildS.viewWithTag(203) as! UITextField
        lblTextField3.text = "Email :"
        lblWidth = lblTextField3.intrinsicContentSize.width
        lblTextField3.frame = CGRect(x: 10,y: 0,width: lblWidth,height: hightView)
        let view4 = UIView(frame: CGRect(x: 0,y: 0,width: lblWidth + 20,height: hightView))
        view4.addSubview(lblTextField3)
        txtEmail.leftView = view4
        txtEmail.leftViewMode = .always
        txtEmail.layer.borderColor = UIColor.lightGray.cgColor
        txtEmail.layer.borderWidth = 1.0
        txtEmail.text = String(describing: userDetails["email"]!)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// MARK:-============ Outlet Btn Action Methods ==========
    
    @IBAction func btnPaymentSelectionsAction(_ sender: UIButton) {
        
        let payment = self.storyboard?.instantiateViewController(withIdentifier: "PASelectPaymentVC") as! PASelectPaymentVC
        payment.profilePayment = true
        payment.pushToRequestView = { (dicCardDetails:[String:Any]?, cardID:String!) -> () in
        }
        self.navigationController?.pushViewController(payment, animated: true)
    }
    func leftBackButtonWithCustom() -> UIBarButtonItem{
        let navLeftBtn: UIButton = UIButton(type: .custom)
        navLeftBtn.addTarget(self, action: #selector(PAProfileUserVC.leftBarButtonClicked(_:)), for: .touchUpInside)
        navLeftBtn.setTitle("Profile", for: UIControlState())
        navLeftBtn.setTitleColor(UIColor.appGrayColor(), for: UIControlState())
        navLeftBtn.titleLabel!.font = UIFont(name: "Oxygen-bold", size: 16)
        navLeftBtn.setImage(UIImage(named: "whiteBackArrow"), for: UIControlState())
        navLeftBtn.contentMode = .scaleAspectFit
        navLeftBtn.contentHorizontalAlignment = .left
        navLeftBtn.frame = CGRect(x: 5, y: 0,width: 230, height: 40)
        navLeftBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -2, 0, 0)
        navLeftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -8, 0, 0)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: navLeftBtn)
        return leftBarButton
        
    }
    func leftBarButtonClicked(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
