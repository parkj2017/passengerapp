//
//  PARateCardsBtAddressesVC.swift
//  PassangerApp
//
//  Created by Netquall on 2/22/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



enum addressType : String{
    case pickUp
    case dropOff
}

enum PickerSelectionType : String{
    case vehicleType
    case serviceType
    case serviceTypeNone
}

class PARateCardsBtAddressesVC: BaseViewController, BackButtonActionProtocol, UIPickerViewDataSource, UIPickerViewDelegate {
    
    // MARK:-============ Property decalaration ==========
    @IBOutlet weak var txtVehicleTypeSelection: UITextField!
    @IBOutlet weak var txtServiceTypeSelection: UITextField!

    @IBOutlet weak var pickerViewCarName: UIPickerView!
    @IBOutlet var viewPickerView: UIView!
    @IBOutlet weak var txtEstimatedFare: UITextField!
    @IBOutlet var btnBookNow: UIButton!
    lazy var locationManagerObjct = LocationManager.sharedInstance
    
    lazy var dicAddresDetails = [String:Any]()
    lazy var arrDataSourceCarInfo = [[String:Any]]()
    lazy  var dicDisAndTime = [String:Any]()
    var strEstimateFare = ""
    var strVehicleType = ""
    var strServiceType = ""

    var pickerType:PickerSelectionType! = PickerSelectionType.serviceTypeNone

    lazy var arrDataServiceType = [Any]()

    
    // MARK:-====View Life cycle =======
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Quotation", imageName: "backArrow")
        self.getingVehlciTypesAvailable()
        self.dicAddresDetails[addressType.pickUp.rawValue] = "" as Any?
        self.dicAddresDetails[addressType.dropOff.rawValue] = "" as Any?
        
        self.txtVehicleTypeSelection.inputView = self.viewPickerView
        self.txtServiceTypeSelection.inputView = self.viewPickerView

        self.pickerViewCarName.delegate = self
        self.pickerViewCarName.dataSource = self
        self.viewPickerView.frame = CGRect(x: 0, y: self.view.frame.height - 216, width: self.view.frame.width , height: 260)
        
        self.viewPickerView.isHidden = true
        // Do any additional setup after loading the view.
        
        btnBookNow.addTarget(self, action: #selector(PARateCardsBtAddressesVC.bookACarNow), for: .touchUpInside)
        
        
        self.txtEstimatedFare.isHidden = true
        DispatchQueue.main.async {[weak self] () -> Void in
            for customView in (self?.view.subviews)!
            {
                if customView.isKind(of: UITextField.self)
                {
                    let text = customView as! UITextField
                    let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 0))
                    let paddingViewRight = UIView()
                    if Constants.iPadScreen == true {
                        paddingViewRight.frame = CGRect(x: self!.view.frame.width - 30, y: 0, width: 30, height: 0)
                        
                    }else
                    {
                        paddingViewRight.frame = CGRect(x: self!.view.frame.width - 20, y: 0, width: 20, height: 0)
                        
                    }
                    text.leftView = paddingView
                    text.leftViewMode = .always
                    text.rightView = paddingViewRight
                    text.rightViewMode = .always
                    text.layer.borderColor = UIColor.lightGray.cgColor
                    text.layer.borderWidth = 1.0
                    text.setValue(UIColor.black, forKeyPath: "_placeholderLabel.textColor")
                    text.tintColor = UIColor.appBlackColor()
                     text.textColor  = UIColor.appBlackColor()
                    
                }else if customView.isKind(of: UIButton.self){
                    let btn  = customView as! UIButton
                    if btn.tag != 1000{
                        btn.layer.borderColor = UIColor.lightGray.cgColor
                        btn.layer.borderWidth = 1.0
                        btn.titleLabel?.numberOfLines = 4
                        btn.titleLabel?.font = Globals.defaultAppFont(13)
                    }
                }
                
            }
            
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.btnBookNow.hidden = true
        Globals.sharedInstance.delegateBack = self
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK:-============ get available Vehicle From Server ==========
    
    func getingVehlciTypesAvailable()
    {
        
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }
        appDelegate.showProgressWithText("Getting available vehicle...")
        var param_Dictionary = [String : Any]()
        let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
        let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
        let  urlString = "get_vechile_type"
        param_Dictionary["user_id"] = user_ID
        param_Dictionary["session"] = session
        param_Dictionary["company_id"] = Constants.CompanyID as Any?
        
        ServiceManager.sharedInstance.dataTaskWithPostRequest(urlString, dicParameters: param_Dictionary, successBlock: { [weak self] (dicData) -> Void in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success" {
                
                if let arrSevice = dicData["service_type"] as? [[String:Any]]{
                    self?.arrDataServiceType = arrSevice as [Any]
                }

                if let arrVeicleInfo = dicData["records"] as? [[String:Any]]{
                    self?.arrDataSourceCarInfo = arrVeicleInfo
                }
            }else {
                appDelegate.hideProgress()
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
            }, failureBlock: { (error) -> () in
                appDelegate.hideProgress()
                Globals.ToastAlertWithString("Network Error!..")
        })
    }
    // MARK:-============Delegate Text Field ==========
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        
        if textField == txtVehicleTypeSelection{
            if self.arrDataSourceCarInfo.count > 0{
                self.viewPickerView.isHidden = false
            }
            self.pickerType = PickerSelectionType.vehicleType
        }
        else if textField == txtServiceTypeSelection {
            if self.arrDataServiceType.count > 0{
                self.viewPickerView.isHidden = false
            }
            self.pickerType = PickerSelectionType.serviceType
        }
        else{
            self.pickerType = PickerSelectionType.serviceTypeNone
        }
        self.pickerViewCarName.reloadAllComponents()
        return true
    }
    // MARK: - =======picker View Data Source and DSelegate ======
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if self.pickerType == PickerSelectionType.serviceType
        {
            return self.arrDataServiceType.count
        }
        else  if self.pickerType == PickerSelectionType.vehicleType
        {
            return self.arrDataSourceCarInfo.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       
        if self.pickerType == PickerSelectionType.serviceType
        {
            let dicCars = self.arrDataServiceType[row] as! [String:Any]
            return  dicCars["service_type"] as? String
        }
        else  if self.pickerType == PickerSelectionType.vehicleType{
        let dicStops = self.arrDataSourceCarInfo[row]
            return  dicStops["vehicle_type_title"] as? String
        }
        return ""

    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.pickerType == PickerSelectionType.serviceType
        {
            let dicCars = self.arrDataServiceType[row] as! [String:Any]
            self.txtServiceTypeSelection.text = dicCars["service_type"] as? String
            self.strServiceType = (dicCars["id"] as? String)!
                }
        else  if self.pickerType == PickerSelectionType.vehicleType{
            let dicCars = self.arrDataSourceCarInfo[row]
            self.txtVehicleTypeSelection.text = dicCars["vehicle_type_title"] as? String
            self.strVehicleType = (dicCars["id"] as? String)!
        }
    }
    @IBAction func btnDonePickerViewAction(_ sender: UIBarButtonItem) {
        
        if self.pickerType == PickerSelectionType.serviceType
        {
            if self.txtServiceTypeSelection.text == "" {
                let dicCars = self.arrDataServiceType.first as? [String:Any]
                self.txtServiceTypeSelection.text = dicCars!["service_type"] as? String
                self.strServiceType = (dicCars!["id"] as? String)!
            }
            self.txtServiceTypeSelection.resignFirstResponder()
            
            if self.txtVehicleTypeSelection.text?.characters.count > 0{
                self.calculateEstimateFare()
            }
        }
        else  if self.pickerType == PickerSelectionType.vehicleType{
        if self.txtVehicleTypeSelection.text == "" {
            let dicCars = self.arrDataSourceCarInfo.first
            self.txtVehicleTypeSelection.text = dicCars!["vehicle_type_title"] as? String
            self.strVehicleType = (dicCars!["id"] as? String)!
        }
        self.txtVehicleTypeSelection.resignFirstResponder()
        self.calculateEstimateFare()
        }
    }
    
    
    // MARK:-============ Outlet Btn Action Methods ==========
    
    @IBAction func btnPickUPAddressAction(_ sender: UIButton) {
        let selectAddrsVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesAddessViewController") as! SearchPlacesAddessViewController
        
        selectAddrsVC.viewFrom = "rideLater"
        selectAddrsVC.isPickUpAddress = true
        selectAddrsVC.strTypeOfAddress = "Pickup Address"
        selectAddrsVC.finishSelectingAddress = { [weak self](dicDropOff : [String:Any], tag:Int, isdone:Bool) -> () in
            if dicDropOff.count != 0 {
                self?.dicAddresDetails[addressType.pickUp.rawValue] = dicDropOff as Any?
                self?.locationManagerObjct.dicSourceCustomLocation = dicDropOff
                
                sender.setTitle((dicDropOff["address"] as! String), for: UIControlState())
                if let dropOff = self?.dicAddresDetails[addressType.dropOff.rawValue] {
                    if !(dropOff is String){
                        let dropOff = dropOff as! [String:Any]
                        let pickLoc = CLLocation(latitude: dicDropOff["latitude"] as! Double , longitude: dicDropOff["longitude"] as! Double)
                        let DroppOffLoc = CLLocation(latitude: dropOff["latitude"] as! Double , longitude: dropOff["longitude"] as! Double)
                        self?.locationManagerObjct.pickUpLocation = pickLoc
                        self?.locationManagerObjct.destinationLocation = DroppOffLoc
                        self?.getDistanceFromPickUpToDropOff(pickLoc, DropOffLoc: DroppOffLoc)
                    }
                }
            }
        }
        //selectAddrsVC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Pickup Address", imageName: "backArrow")
        self.navigationController?.pushViewController(selectAddrsVC, animated: true)
        
    }
    
    @IBAction func btnDropOffAddressAction(_ sender: UIButton)
    {
        let selectAddrsVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesAddessViewController") as! SearchPlacesAddessViewController
        selectAddrsVC.viewFrom = "rideLater"
        selectAddrsVC.isPickUpAddress = false
        selectAddrsVC.strTypeOfAddress = "Drop-off Address"
        
        selectAddrsVC.finishSelectingAddress = { [weak self](dicDropOff : [String:Any], tag:Int, isdone:Bool) -> () in
            if dicDropOff.count != 0 {
                self?.dicAddresDetails[addressType.dropOff.rawValue] = dicDropOff as Any?
                self?.locationManagerObjct.dicDestinationLocation = dicDropOff
                
                sender.setTitle((dicDropOff["address"] as! String), for: UIControlState())
                if let pickUp = self?.dicAddresDetails[addressType.pickUp.rawValue]{
                    if !(pickUp is String){
                        let pickUp = pickUp as! [String:Any]
                        let pickLoc = CLLocation(latitude: dicDropOff["latitude"] as! Double , longitude: dicDropOff["longitude"] as! Double)
                        let DroppOffLoc = CLLocation(latitude: pickUp["latitude"] as! Double , longitude: pickUp["longitude"] as! Double)
                        self?.locationManagerObjct.pickUpLocation = pickLoc
                        self?.locationManagerObjct.destinationLocation = DroppOffLoc
                        self?.getDistanceFromPickUpToDropOff(pickLoc, DropOffLoc: DroppOffLoc)
                    }
                }
            }
        }
        //    selectAddrsVC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Drop-off Address", imageName: "backArrow")
        self.navigationController?.pushViewController(selectAddrsVC, animated: true)
    }
    // MARK:- ====calculate distance between Pickp Drop Off =====
    func getDistanceFromPickUpToDropOff(_ PickUpLocation:CLLocation, DropOffLoc: CLLocation){
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async { [weak self]() -> Void in
            let urlStr = Globals.sharedInstance.getFullApiAddressForDistanceToDriverDistance(PickUpLocation, destinationLoc: DropOffLoc)
            
            if urlStr == "" {
                return
            }
            ServiceManager.sharedInstance.dataTaskWithGetRequest(urlStr, successBlock: { (dicData) -> Void in
                let arrRoutes = dicData["routes"] as! [[String : Any]]
                if arrRoutes.count > 0 {
                    let dicMainRoutes = arrRoutes.first!
                    let arrLegs = dicMainRoutes["legs"] as! [[String:Any]]
                    self?.dicDisAndTime["distance"] = arrLegs.first!["distance"]!
                    self?.dicDisAndTime["duration"] = arrLegs.first!["duration"]!
                    if self?.strVehicleType != "" {
                        self?.calculateEstimateFare()
                    }
                }
                }, failureBlock: { (error) -> () in
                    appDelegate.hideProgress()
                    print("Error \(error?.description)")
            })
        }
        
    }
    func bookACarNow()
    {
        
        if let _ = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.activeJobNotification)
        {
            let alert = UIAlertController(title: "Please wait", message: "There is one ongoing job.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else  if strVehicleType == "" {
                            return
            }
        else{
            
            if appDelegate.networkAvialable == true {
                
                if  let  newLocation =  self.dicAddresDetails[addressType.pickUp.rawValue] as? [String:Any]{
                    
                    locationManagerObjct.pickUpLocation = CLLocation(latitude: newLocation["latitude"] as! Double , longitude: newLocation["longitude"] as! Double)
                    locationManagerObjct.strCurrentLocation = newLocation["address"] as! String
                    
                    let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
                    let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
                    
                    var dicParameters = [String : Any]()
                    if self.txtVehicleTypeSelection.text != ""{
                        dicParameters["vehicle_type"] = self.txtVehicleTypeSelection.text as Any?
                    }
                    dicParameters["user_id"] = user_ID
                    dicParameters["session"] = session
                    dicParameters["latitude"] = "\(locationManagerObjct.pickUpLocation!.coordinate.latitude)" as Any?
                    dicParameters["longitude"] = "\(locationManagerObjct.pickUpLocation!.coordinate.longitude)" as Any?
                    dicParameters["on_demand_radius"] = appDelegate.ondemandRadius as Any?
                    dicParameters["pickup_date_time"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any? //"2016-05-05  19:49:12"
                    dicParameters["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?
                    dicParameters["company_id"] = Constants.CompanyID as Any?
                   
                    self.locationManagerObjct.strVehicleType = self.txtVehicleTypeSelection.text!

                    if  let  newLocation = self.locationManagerObjct.destinationLocation{
                        dicParameters["dropoff_latitude"] = "\(newLocation.coordinate.latitude)" as Any?
                        dicParameters["dropoff_longitude"] = "\(newLocation.coordinate.longitude)" as Any?
                    }
                    else {
                        dicParameters["dropoff_latitude"] = "" as Any?
                        dicParameters["dropoff_longitude"] = "" as Any?
                    }

                    
                    let url = Constants.baseUrl + Constants.getNearestCarsNew
                    let URL = Foundation.URL(string:url)!
                    let mutableURLRequest = NSMutableURLRequest(url: URL)
                    mutableURLRequest.httpMethod = "POST"
                    
                    
                    do {
                        mutableURLRequest.httpBody = try JSONSerialization.data(withJSONObject: dicParameters, options: JSONSerialization.WritingOptions())
                        mutableURLRequest.timeoutInterval = 45
                    } catch {
                        print("Error--- \(error)")
                        return
                    }
                    NSLog("%@, %@", url, dicParameters)
                    
                    mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    
                    appDelegate.showProgressWithText("Please wait... \n Looking for " + "\(self.txtVehicleTypeSelection.text!)" + "in nearby area")
                   Alamofire.request(URL, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (Response) in
                    
                        if Response.result.isSuccess{
                            if let  jsonDataRecieved = Response.data{
                                let jsonDicRecieved = JSON(data:jsonDataRecieved )
                                if let dic = jsonDicRecieved.dictionaryObject {
                                    if dic["status"] as! String == "session_expired"{
                                        let navigation = appDelegate.window?.rootViewController as? UINavigationController
                                        if let _ = navigation {
                                            navigation!.popToRootViewController(animated: true)
                                        }
                                        appDelegate.hideProgress()
                                        Globals.logout()
                                    }
                                    if dic["status"] as! String == "success" {
                                        let arrKeys = dic.keys
                                        if  !(arrKeys.contains("message"))  {
                                            if let carsDetails = dic.formatDictionaryForNullValues(dic as [String : Any]){
                                                self.showCarsOnMap(carsDetails)
                                            }
                                            else {
                                                Globals.ToastAlertWithString((dic["message"] as! String))
                                            }
                                        } else {
                                            Globals.ToastAlertWithString((dic["message"] as! String))
                                        }
                                    }else {
                                        Globals.ToastAlertWithString((dic["message"] as! String))
                                    }
                                }else {
                                    Globals.ToastAlertWithString("Network Error")
                                }
                            }else {
                                Globals.ToastAlertWithString("Network Error")
                            }
                            DispatchQueue.main.async(execute: { () -> Void in
                                appDelegate.hideProgress()
                                })
                        }else {
                            DispatchQueue.main.async(execute: { () -> Void in
                                appDelegate.hideProgress()
                                Globals.ToastAlertWithString("Network Error")
                                })
                            
                        }
                    }
                }
            }
        }

    }
    
    func showCarsOnMap(_ dicRecieved:[String: Any]){
        
        if let arrOnDemandCars = dicRecieved["records"] as? [String:Any]
        {
            if let onDemandCars = arrOnDemandCars["\(self.txtVehicleTypeSelection.text!)"] as? [[String:Any]]{
                let payment = self.storyboard?.instantiateViewController(withIdentifier: "PAPaymentForOnDemandVC") as! PAPaymentForOnDemandVC
                payment.arrOndemanCarsSelect = onDemandCars
                payment.isJobRequest = true
                
                self.navigationController?.pushViewController(payment, animated: true)
            }
            else{
                Globals.ToastAlertWithString("Sorry there are no cars in the area of type " + "\(self.txtVehicleTypeSelection.text!)")
            }
        }
        else {
            Globals.ToastAlertWithString("Sorry there are no cars in the area")
        }
        
    }
    
    // MARK:- ====calculate Estimate Fare =====
    func calculateEstimateFare() {
        
        // let estFare = estDistance * Float(CarObject.base_per_km)!
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }
        if  self.arrDataSourceCarInfo.count == 0 || self.dicAddresDetails[addressType.pickUp.rawValue] is String {
            return
        }
        var Time : Double! = 0.0
        var distance : Float! = 0.0
        if let arrKeeys : [String]? = [String](self.dicDisAndTime.keys)
        {
            if arrKeeys!.contains("duration"){
                let DicTime = self.dicDisAndTime["duration"]! as! [String:Any]
                Time = (DicTime["value"] as! Double) / 60 
                let Dicdistance = self.dicDisAndTime["distance"]! as! [String:Any]
                distance = (Dicdistance["value"]  as! Float) * appDelegate.distanceMultiplier
                //let estFare = estDistance * Float(CarObject.base_per_km)!
            }
        }
        appDelegate.showProgressWithText("Calculating Estimated Fare")
        var param_Dictionary: [String : Any] = [String : Any]()
        let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
        let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
        
        param_Dictionary["user_id"] = user_ID
        param_Dictionary["session"] = session
        
        
        if   self.arrDataSourceCarInfo.count != 0{
            param_Dictionary["vechile"] = self.strVehicleType as Any?
        }
        
        param_Dictionary["distance"] = distance as Any?
        param_Dictionary["time"] = Time as Any?
        param_Dictionary["distance_type"] = appDelegate.distanceType as Any?
        param_Dictionary["service_type"] = self.strServiceType as Any?

        // for pickup
        if let dicPickupAddress = self.dicAddresDetails[addressType.pickUp.rawValue] as? [String:Any] {
            param_Dictionary["pickup_location"] = dicPickupAddress["address"]
            param_Dictionary["zip_from"] = dicPickupAddress["postal_code"]
        }
        // for Drop off
        if let dicDropOffAddress = self.dicAddresDetails[addressType.dropOff.rawValue] as? [String:Any] {
            param_Dictionary["dropoff_location"] = dicDropOffAddress["address"]
            param_Dictionary["zip_to"] = dicDropOffAddress["postal_code"]
            param_Dictionary["template_type"] = "ptp" as Any?
        }
        else {
            param_Dictionary["dropoff_location"] = "" as Any?
            param_Dictionary["zip_to"] = "" as Any?
            param_Dictionary["template_type"] = "hourly" as Any?
        }
        
        ServiceManager.sharedInstance.dataTaskWithPostRequest("get_fare_quote", dicParameters: param_Dictionary, successBlock: {[weak self] (dicData) -> Void in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success"{
                
                if let totalAmt = dicData["totalamount"]
                {
                    if String(describing: totalAmt) == "0"
                    {
                        self?.strEstimateFare = "N/A"
                        let alert = self?.alertView("Alert", message:"No rates are available for this request, please contact your customer support.", delegate:nil, cancelButton:"Ok", otherButons:nil)
                        alert!.show()
                    }
                    else {
                        self?.strEstimateFare = "\(appDelegate.currency) \(totalAmt)"
                    }
                }
                let strCompleteFare = "Estimate Fare :  \( (self?.strEstimateFare)!)"
                let attributedText = NSMutableAttributedString(string:strCompleteFare)
                let attrsBaseFare = [NSFontAttributeName: Globals.defaultAppFontWithBold(15), NSForegroundColorAttributeName: UIColor.appBlackColor()]
                let rangeBaseFare = NSString(string: strCompleteFare).range(of: (self?.strEstimateFare)!)
                attributedText.addAttributes(attrsBaseFare, range: rangeBaseFare)
                DispatchQueue.main.async(execute: { () -> Void in
                    self?.txtEstimatedFare.attributedText = attributedText
                    self?.txtEstimatedFare.isHidden = false
                    
                })
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
            }, failureBlock: { (error) -> () in
                appDelegate.hideProgress()
                print(error)
                Globals.ToastAlertWithString("Netowrk Error")
        })
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func backButtonActionMethod(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
