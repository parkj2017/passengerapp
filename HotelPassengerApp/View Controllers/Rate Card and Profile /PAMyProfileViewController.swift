//
//  PAMyProfileViewController.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 23/06/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import AVFoundation
import DLImageLoader
class tableViewCellTypeCardNo:UITableViewCell
{
    @IBOutlet var btnBin: UIButton!
    @IBOutlet var lblCardNo: UILabel!
}
class tableViewCellFavouriteDrivers:UITableViewCell
{
    @IBOutlet var lblPlaceHolder: UILabel!
}
class tableViewCellTypeAddress:UITableViewCell
{
    @IBOutlet var lblHomeAddress: UIButton!
    @IBOutlet var lblWorkAddress: UIButton!
    
    override func  awakeFromNib() {
        lblHomeAddress.titleLabel?.numberOfLines = 2
        lblWorkAddress.titleLabel?.numberOfLines = 2
    }
}

class favDriverView: UIView {
    
    @IBOutlet var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imageDriver: UIImageView!
    @IBOutlet var activityView:UIActivityIndicatorView!
    @IBOutlet var btnForSelection: UIButton!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    override func awakeFromNib() {
    }
    override func layoutSubviews(){
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
    }
    
    func setupDriverView(_ name:String, imageUrl:String)  {
        
        self.lblName.text = name
        self.lblName.font = Globals.defaultAppFont(14)
        self.activityView.isHidden = false;
        self.activityView.tintColor = UIColor.appUbberBlueColor()
        self.activityView.color =  UIColor.appUbberBlueColor()
        self.activityView.backgroundColor = UIColor.clear
         self.imageDriver.layer.cornerRadius =  22.5
        if Constants.iPadScreen {
            self.imageWidthConstraint.constant = 45.0 * 1.5
             self.imageHeightConstraint.constant = 45.0 * 1.5
            self.imageDriver.layer.cornerRadius =  (45.0 * 1.5 ) / 2.0
            self.layoutIfNeeded()
            self.updateConstraintsIfNeeded()
        }
        self.imageDriver.layer.borderColor = UIColor.appBlackColor().cgColor
        self.imageDriver.layer.borderWidth = 1.0
        self.imageDriver.layer.masksToBounds = true
        
        if appDelegate.networkAvialable == true {
            DLImageLoader.sharedInstance().image(fromUrl: imageUrl, completed: { [weak self](error, image) -> () in
                self?.activityView.isHidden = true;
                if image != nil {
                    DispatchQueue.main.async(execute: { () -> Void in
                        self?.imageDriver.image = image
                    })
                }else {
                    self?.imageDriver.image = UIImage(named: "profile_placeholder")
                }
                })
        }
        else {
            self.activityView.isHidden = true;
            self.imageDriver.image = UIImage(named: "profile_placeholder")
        }
        
    }
    fileprivate func commonInit() {
        let bundle = Bundle(for:type(of: self))
        let nib = UINib(nibName: "favDriverView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.frame = self.bounds
        view.backgroundColor = UIColor.white
        self.addSubview(view)
    }
}

import MapKit
class PAMyProfileViewController: BaseViewController, BackButtonActionProtocol, UIActionSheetDelegate , UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet var tblDetails: UITableView!
    @IBOutlet var imageProfile: UIImageView!
    @IBOutlet var lblFullName: UILabel!
    
    var dicAddressDetails : [String:Any]!
    var arrFavDrivers: [[String:Any]]!
    var picker = UIImagePickerController()
    var arrCardDetails: [[String:Any]]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("My Profile", imageName: "backArrow")
        
        self.getUserProfile()
        self.imageProfile.layer.masksToBounds = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action:#selector(self.changeProfileImage(_:)))
        self.imageProfile.addGestureRecognizer(tapGesture)
        
        
        let userDetails = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        if let imageUrl = userDetails["image_url"] as? String {
            self.setProfileImage(imageUrl)
        }
        else {
            self.imageProfile.image = UIImage(named: "profileImage")
        }
        
        
        let name = Constants.isBookerLogin == true ? "\(String(describing: appDelegate.activeUserDetailsForBooker["first_name"]!))" : Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).first_name") as! String
        let lastName =  Constants.isBookerLogin == true ? "\(String(describing: appDelegate.activeUserDetailsForBooker["last_name"]!))" : Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).last_name") as! String
        

        lblFullName.text =  name.capitalized + " " + lastName.capitalized
    }
    
    override func viewDidLayoutSubviews() {
        self.imageProfile.layer.borderColor = UIColor.white.cgColor
        self.imageProfile.layer.borderWidth =  2.0
        
        if Constants.iPadScreen == true {
            self.imageProfile.layer.cornerRadius = 125.0
        }else {
            self.imageProfile.layer.cornerRadius = 50.0
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
        self.navigationController?.isNavigationBarHidden = false
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Globals.sharedInstance.delegateBack = nil
        
    }
    
    func backButtonActionMethod(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getUserProfile(){
        if appDelegate.networkAvialable == true {
            var dicParameters = [String:Any]()
            
            if Constants.isBookerLogin == true
            {
                dicParameters["user_id"] = "\(appDelegate.activeUserDetailsForBooker["id"]!)"
                dicParameters["is_booker"] = "yes"
            }
            else
            {
                let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
                let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
                dicParameters["user_id"] = user_ID
                dicParameters["session"] = session
                dicParameters["is_booker"] = "no"

            }
            weak var weakSelf = self
            appDelegate.showProgressWithText("Fetching profile data...")
            
            ServiceManager.sharedInstance.dataTaskWithPostRequest("userprofile", dicParameters: dicParameters, successBlock: { (dicData) -> Void in
                appDelegate.hideProgress()
                if weakSelf == nil {
                    return
                }
                DispatchQueue.main.async(execute: {[weak self] () -> Void in
                    
                    if let status = dicData["status"] as? String{
                        if status.compare("success") == .orderedSame
                        {
                            if let profileData = dicData["record"] as? [String:Any]{
                                self?.dicAddressDetails = [String:Any]()
                                if let fName = profileData["first_name"] as? String{
                                    if let lName = profileData["last_name"] as? String{
                                        self?.lblFullName.text =  (fName).capitalized + " " + (lName).capitalized
                                    }
                                    else {
                                        self?.lblFullName.text =  (fName).capitalized
                                    }
                                }
                                
                                if let arrDict = profileData["favroite_drivers"] as? [[String:Any]]{
                                    self?.arrFavDrivers = arrDict
                                }
                                else {
                                    self?.arrFavDrivers = [[String:Any]]()//no fav drivers, will show no fav driver found
                                }
                                if let arrDict = profileData["cards"] as? [[String:Any]]{
                                    self?.arrCardDetails = arrDict
                                }
                                else {
                                    self?.arrCardDetails = [[String:Any]]()//no fav drivers, will show no fav driver found
                                }
                                if let homeAdd = profileData["p_appartment"] as? String{
                                    self?.dicAddressDetails["home_address"] = (homeAdd.isEmpty == true) ? "Add home address" : homeAdd as Any?
                                    self?.dicAddressDetails["home_lat_long"] = "0.0,0.0"
                                }
                                
                                if let workAdd = profileData["work_apt"] as? String{
                                    self?.dicAddressDetails["work_address"] = (workAdd.isEmpty == true) ? "Add work address" : workAdd
                                    self?.dicAddressDetails["work_lat_long"] = "0.0,0.0"
                                }
                                //                                if let homeAdd = profileData["p_appartment"] as? String{
                                //                                    self?.dicAddressDetails["home_address"]  = homeAdd
                                //                                    self?.dicAddressDetails["home_lat_long"] = "0.0,0.0"
                                //                                }
                                //
                                //                                if let workAdd = profileData["work_apt"] as? String{
                                //                                    self?.dicAddressDetails["work_address"]  = workAdd
                                //                                    self?.dicAddressDetails["work_lat_long"] = "0.0,0.0"
                                //                                }
                                if let imageUrl = profileData["profile_image"] as? String{
                                    self?.setProfileImage(imageUrl)
                                }
                                
                            }
                        }
                        
                        DispatchQueue.main.async(execute: {
                            weakSelf?.tblDetails.reloadData()
                        })
                    }
                    else {
                        appDelegate.hideProgress()
                        let alert = UIAlertView(title: "Please try again later.", message: "Not able to fetch user details.", delegate: nil, cancelButtonTitle: "Ok")
                        alert.show()
                    }
                })
                
            })
            { (error) -> () in
                appDelegate.hideProgress()
                let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
            }
            
        }else {
            appDelegate.hideProgress()
            let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
        }
        
    }
    
    func setProfileImage(_ imageURL:String)
    {
        if appDelegate.networkAvialable == true {
            DLImageLoader.sharedInstance().image(fromUrl: imageURL, completed: { [weak self](error, image) -> () in
                if image != nil {
                    DispatchQueue.main.async(execute: { () -> Void in
                        self?.imageProfile.image = image
                    })
                }else {
                    self?.imageProfile.image = UIImage(named: "profileImage")
                }
                })
            
        }
        else{
            self.imageProfile.image = UIImage(named: "profileImage")
        }
    }
    func setHomeWorkAddress(_ address:[String:Any], isHomeAdd:Bool)  {
        if appDelegate.networkAvialable == true {
            var dicParameters = [String:Any]()
            
            if Constants.isBookerLogin == true
            {
                dicParameters["user_id"] = "\(appDelegate.activeUserDetailsForBooker["id"]!)"
                dicParameters["is_booker"] = "yes"
            }
            else
            {
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            dicParameters["user_id"] = user_ID
            dicParameters["session"] = session
            dicParameters["is_booker"] = "no"

            }
            dicParameters["status"] = "update"
            
            if isHomeAdd == true{
                
                dicParameters["home_address"] = address["address"] as! String
                dicParameters["home_lat_long"] = String(format: "%@,%@",  "\(address["latitude"]!)",  "\(address["longitude"]!)")
            }
            else {
                dicParameters["work_address"] = address["address"] as! String
                dicParameters["work_lat_long"] = String(format: "%@,%@",  "\(address["latitude"]!)",  "\(address["longitude"]!)")
            }
            weak var weakSelf = self
            appDelegate.showProgressWithText("Updating address.....")
            
            ServiceManager.sharedInstance.dataTaskWithPostRequest("getUpdateAccountAddress", dicParameters: dicParameters, successBlock: { (dicData) -> Void in
                appDelegate.hideProgress()
                if weakSelf == nil {
                    return
                }
                DispatchQueue.main.async(execute: {[weak self] () -> Void in
                    if let dict = dicData["record"] as? [String:Any]{
                        appDelegate.hideProgress()
                        
                        self?.dicAddressDetails = dict
                        if isHomeAdd == true{
                            self?.dicAddressDetails["home_address"]  = address["address"] as! String
                            self?.dicAddressDetails["home_lat_long"] = String(format: "%@,%@",  "\(address["latitude"]!)",  "\(address["longitude"]!)")
                        }
                        else {
                            self?.dicAddressDetails["work_address"]  = address["address"] as! String
                            self?.dicAddressDetails["work_lat_long"] = String(format: "%@,%@",  "\(address["latitude"]!)",  "\(address["longitude"]!)")
                        }
                    }
                    weakSelf?.tblDetails.reloadData()
                    })
                
                })
            { (error) -> () in
                let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
            }
            
        }else {
            let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
        }
    }
    
    
    //MARK:- ======= table View Data Source =============
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0 || section == 1{
            return 1
        }
        if section == 2 {
            if self.arrCardDetails != nil {
                return arrCardDetails.count == 0 ? 1 : arrCardDetails.count
            }
            else{
                return 1
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.section == 0{
            var cell   = tblDetails.dequeueReusableCell(withIdentifier: "tableViewCellTypeFavDriversIdentifier") as? tableViewCellFavouriteDrivers
            if cell == nil{
                cell = UITableViewCell(style: .default, reuseIdentifier: "tableViewCellTypeFavDriversIdentifier")  as? tableViewCellFavouriteDrivers
            }
            
            for view in (cell?.subviews)!{
                if view.isKind(of: favDriverView.self){
                    view.removeFromSuperview()
                }
            }
            if self.arrFavDrivers == nil{
                cell!.lblPlaceHolder.text = "Loading..."
            }
            else {
                if self.arrFavDrivers.count == 0{
                    cell!.lblPlaceHolder.text = "No Favorite Driver Found"
                }
                else {
                    
                    let scrollview:UIScrollView = UIScrollView(frame: cell!.bounds)
                    var contentSize:CGSize = CGSize.zero
                    if DeviceType.IS_IPAD {
                         contentSize = CGSize(width: CGFloat(Double(self.arrFavDrivers.count) * (125.0 * 1.5)), height: 82.0 * 1.5)
                    }
                    else if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                        contentSize = CGSize(width: CGFloat(self.arrFavDrivers.count*106), height: 82)
                    }
                    else {
                        contentSize = CGSize(width: CGFloat(self.arrFavDrivers.count*125), height: 82)
                    }
                    for i in 0 ..< self.arrFavDrivers.count  {
                        let dic = self.arrFavDrivers[i]
                        
                       
                        var view:favDriverView?
                        if DeviceType.IS_IPAD {
                            view = favDriverView(frame: CGRect(x: (Double(i) * (125 * 1.5 )) , y: 0, width: 125.0 * 1.5, height: 82.0 * 1.5))
                        }
                        else
                        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                            view = favDriverView(frame: CGRect(x: (Double(i) * 106.6) , y: 0, width: 106, height: 82))
                        }
                        else {
                            view = favDriverView(frame: CGRect(x: (i * 125) , y: 0, width: 125, height: 82))
                        }
                        var driverName = ""
                        if let fName = dic["first_name"] as? String{
                            driverName = fName
                        }
                        if let lName = dic["last_name"] as? String{
                            driverName =  driverName + " " + lName
                        }
                        
                       // cell?.addSubview(view!)
                        if let image = dic["image"] as? String{
                            view?.setupDriverView(driverName , imageUrl: image)
                        }
                        else {
                            view?.setupDriverView(driverName , imageUrl: "")
                        }
                        view?.btnForSelection.tag = 100 + i
                        view?.btnForSelection.addTarget(self, action: #selector(PAMyProfileViewController.btnShowDriverProfileClicked(_:)), for: .touchUpInside)
                        cell?.bringSubview(toFront: scrollview)
                        
                        scrollview.contentSize = contentSize
                        scrollview.addSubview(view!)
                        cell?.addSubview(scrollview)

                        if i == self.arrFavDrivers.count-1 {
                            cell!.lblPlaceHolder.text = ""
                        }
                    }
                }
            }
            return cell!
            
        } else if indexPath.section == 1{
            var cell = tblDetails.dequeueReusableCell(withIdentifier: "tableViewCellTypeAddressIdentifier") as? tableViewCellTypeAddress
            if cell == nil{
                cell = UITableViewCell(style: .default, reuseIdentifier: "tableViewCellTypeAddressIdentifier") as? tableViewCellTypeAddress
            }
            
            cell!.lblHomeAddress.addTarget(self, action: #selector(updateHomeAddress), for: .touchUpInside)
            cell!.lblWorkAddress.addTarget(self, action: #selector(updateWorkAddress), for: .touchUpInside)
            
            if self.dicAddressDetails != nil{
                if let workAdd = self.dicAddressDetails["work_address"] as? String{
                    cell!.lblWorkAddress.setTitle(workAdd, for: UIControlState())
                }
                else {
                    cell!.lblWorkAddress.setTitle("Add", for: UIControlState())
                }
                if let homeAdd = self.dicAddressDetails["home_address"] as? String{
                    cell!.lblHomeAddress.setTitle(homeAdd, for: UIControlState())
                }
                else{
                    cell!.lblHomeAddress.setTitle("Add", for: UIControlState())
                }
            }
            else {
                cell!.lblWorkAddress.setTitle("loading...", for: UIControlState())
                cell!.lblHomeAddress.setTitle("loading...", for: UIControlState())
            }
            return cell!
            
        }
        if indexPath.section == 2 {
            var cell = tblDetails.dequeueReusableCell(withIdentifier: "tableViewCellTypeCardNoIdentifier") as? tableViewCellTypeCardNo
            if cell == nil{
                cell = UITableViewCell(style: .default, reuseIdentifier: "tableViewCellTypeCardNoIdentifier") as? tableViewCellTypeCardNo
            }
            if self.arrCardDetails == nil{
                cell!.lblCardNo.text = "Loading..."
                cell!.btnBin.isHidden = true
            }
            else {
                if self.arrCardDetails.count == 0 {
                    cell!.lblCardNo.text = "No Preferred Card Available"
                    cell!.btnBin.isHidden = true
                }
                else {
                    if let dicCardDetails = arrCardDetails![indexPath.row] as? [String:Any]{
                        if let cardNo = dicCardDetails["cc_no"], let cc_type = dicCardDetails["cc_type"]{
                            cell!.lblCardNo.text = "\(cc_type) \(cardNo)"
                            cell!.btnBin.isHidden = false
                            cell!.btnBin.tag = indexPath.row
                            cell!.btnBin.addTarget(self, action: #selector(PAMyProfileViewController.btnDeleteClicked(_:)), for: .touchUpInside)
                        }
                    }
                }
                
            }
            return cell!
        }
        return UITableViewCell()
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return Constants.iPadScreen == true ? 82.0*1.5 : 82.0
        }else if indexPath.section == 1{
            return Constants.iPadScreen == true ? 114.0*1.5 : 114
        }
        if indexPath.section == 2 {
            return Constants.iPadScreen == true ? 44.0*1.5 : 44
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.iPadScreen == true ? 30*1.5 : 30
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0,y: 0, width: self.view.frame.width,height: Constants.iPadScreen == true ? 30*1.5 : 30))
        let lblPreferCards = UILabel(frame: CGRect(x: 10,y: 8, width: self.view.frame.width-20,height: Constants.iPadScreen == true ? 20*1.5 : 20))
        lblPreferCards.textAlignment = .left
        lblPreferCards.font = Globals.defaultAppFontWithBold(15)
        lblPreferCards.textColor = UIColor.appBlackColor()
        if section == 0 {
            lblPreferCards.text = "FAVORITE DRIVERS"
        }
        else if section == 1 {
            lblPreferCards.text = "HOME AND OFFICE ADDRESS"
        }
        else if section == 2 {
            lblPreferCards.text = "SAVED CARDS"
        }
        
        headerView.addSubview(lblPreferCards)
        return headerView
    }
    // MARK:- ====table view delegate Methods ==========
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        if indexPath.section == 2
        {
        }
    }
    
    
    //MARK --- Selectors ---------
    
    func btnShowDriverProfileClicked(_ selectedBtn:UIButton)  {
        var dicParam = [String:Any]()
        
        let dic = self.arrFavDrivers[selectedBtn.tag%100]
        dicParam["primary_driver"] = dic["id"] as! String
        dicParam["reservation_id"] = ""
        let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "PADriverDetailsViewController") as! PADriverDetailsViewController
        detailsVc.dicDriverInfo = dicParam
        detailsVc.isEditable = true
        detailsVc.didFinishDeletingFavDriver = {[weak self] () -> () in
            self?.getUserProfile()
        }
        detailsVc.view.backgroundColor = UIColor.clear
        detailsVc.modalPresentationStyle = .overFullScreen
        detailsVc.providesPresentationContextTransitionStyle = true
        detailsVc.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overFullScreen
        self.present(detailsVc, animated: true, completion: nil)
        
    }
    
    @IBAction func btnPaymentSelectionsAction(_ sender: UIButton) {
        
        let payment = self.storyboard?.instantiateViewController(withIdentifier: "PASelectPaymentVC") as! PASelectPaymentVC
        payment.profilePayment = true
        payment.pushToRequestView = { (dicCardDetails:[String:Any]?, cardID:String!) -> () in
        }
        self.navigationController?.pushViewController(payment, animated: true)
    }
    
    func btnDeleteClicked(_ clickedBtn:UIButton){
        
        var loginTextField: UITextField?
        var alertMesag = "Remove this card from your preferred list?"
        
        if let card_data = self.arrCardDetails[clickedBtn.tag] as? [String:Any]
        {
            if let card_no = card_data["cc_no"]{
                alertMesag = "Remove \(card_no) from your preferred list?"
            }
        }

        let alert = UIAlertController(title: "Confirmation", message:alertMesag , preferredStyle:.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
            
        }))
        
        alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default, handler:{[weak self] (ACTION :UIAlertAction!)in
            
            if appDelegate.networkAvialable == true {
                
                if alert.textFields![0].text?.count == 0
                {
                    return
                }
                
                let dicCardDetails = self?.arrCardDetails![clickedBtn.tag]
                
                var dicParameters = [String:Any]()
                let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
                let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
                
                dicParameters["user_id"] = user_ID
                dicParameters["payment_id"] = dicCardDetails!["id"]
                dicParameters["session"] = session
                dicParameters["password"] = alert.textFields![0].text
                dicParameters["status"] = "prefered" as Any?
                
                appDelegate.showProgressWithText("Removing as preferred card....")
                ServiceManager.sharedInstance.dataTaskWithPostRequest("removePreferedCard", dicParameters: dicParameters, successBlock: { [weak self](dicData) -> Void in
                    appDelegate.hideProgress()
                    
                    DispatchQueue.main.async(execute: {[weak self] () -> Void in
                        if let status = dicData["status"] as? String{
                            if status.compare("success") == .orderedSame
                            {
                                Globals.ToastAlertWithString("Preferred card removed successfully")
                                self?.arrCardDetails?.remove(at: clickedBtn.tag)
                                self?.tblDetails.reloadData()
                            }
                            else {
                                if let msg = dicData["message"] as? String
                                {
                                    Globals.ToastAlertWithString(msg)
                                }
                                else {
                                    Globals.ToastAlertWithString("Something went wrong, Please try again later")
                                }
                            }
                        }
                        })
                    
                    })
                { (error) -> () in
                    appDelegate.hideProgress()
                    let alert = UIAlertView(title: "Error !", message: "\(error?.localizedDescription)", delegate: nil, cancelButtonTitle: "Ok")
                    alert.show()
                }
                
            }else {
                let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
            }
            
            }))
        
        alert.addTextField { (textField) -> Void in
            loginTextField = textField
            loginTextField?.placeholder = "Enter Password"
            loginTextField?.isSecureTextEntry = true
            
        }
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    func updateHomeAddress(){
        let selectAddrsVC = self.storyboard?.instantiateViewController(withIdentifier: "PASelectAddressViewController") as! PASelectAddressViewController
        selectAddrsVC.isPickUpAddress = true
        selectAddrsVC.strTypeOfAddress = "Home Address"
        selectAddrsVC.finishSelectingDropOffAddress = { [weak self](dicAddress : [String:Any], tag:Int) -> () in
            if dicAddress.count != 0 {
                self?.setHomeWorkAddress(dicAddress, isHomeAdd: true)
            }
        }
        self.navigationController?.pushViewController(selectAddrsVC, animated: true)
    }
    
    func updateWorkAddress(){
        let selectAddrsVC = self.storyboard?.instantiateViewController(withIdentifier: "PASelectAddressViewController") as! PASelectAddressViewController
        selectAddrsVC.isPickUpAddress = true
        selectAddrsVC.strTypeOfAddress = "Work Address"
        selectAddrsVC.finishSelectingDropOffAddress = { [weak self](dicAddress : [String:Any], tag:Int) -> () in
            if dicAddress.count != 0 {
                self?.setHomeWorkAddress(dicAddress, isHomeAdd: false)
            }
        }
        self.navigationController?.pushViewController(selectAddrsVC, animated: true)
    }
    
    
    //MARK: - UPDATE USER PROFILE
    
    func changeProfileImage(_ sender: UITapGestureRecognizer) {
        let actionSheet = UIActionSheet(title: "Select", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Photo Gallery","Camera")
        actionSheet.show(in: self.view)
    }
    
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int){
        
        picker.allowsEditing = true //2
        picker.delegate = self
        switch buttonIndex{
        case 1 :
            picker.sourceType = .photoLibrary //3
            self.permission()

        case 2:
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            self.permission()

        default : break
            
        }
    }
    func openImagePickerWith(_ sourceType:UIImagePickerControllerSourceType) {
        picker.sourceType = sourceType //3
        picker.modalPresentationStyle = .fullScreen
        DispatchQueue.main.async(execute: {[weak self] () -> Void in
            self!.present((self?.picker)!, animated: true, completion: nil)
        })

    }
    func permission() {
        
        let authStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if authStatus == .authorized {
            self.openImagePickerWith(self.picker.sourceType)
        }
        else if authStatus == .notDetermined {
            NSLog("%@", "Camera access not determined. Ask for permission.")
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(granted: Bool) -> Void in
                if granted {
                    NSLog("Granted access to %@", AVMediaTypeVideo)
                    self.openImagePickerWith(self.picker.sourceType)
                }
                else {
                    NSLog("Not granted access to %@", AVMediaTypeVideo)
                    let alert = UIAlertView(title: "Camera Access Denied", message: "Please go to Settings -> Privacy -> Enable camera access for app.", delegate: nil, cancelButtonTitle: "Cancel")
                    alert.show()
                }
            })
        }
        else  {
            // My own Helper class is used here to pop a dialog in one simple line.
            let alert = UIAlertView(title: "Camera Access Denied", message: "Please go to Settings -> Privacy -> Enable camera access for app.", delegate: nil, cancelButtonTitle: "Cancel")
            alert.show()
        }
    }
    // MARK:- ====image picker delegate===
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
         if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                let dataImage = UIImageJPEGRepresentation(RBResizeImage(chosenImage, targetSize: CGSize(width: 500, height: 500)), 0.1)
                let strImageBase64 = dataImage!.base64EncodedString(options: .lineLength64Characters)
                self.imageProfile.image = UIImage(data:dataImage!)
                self.updateProfileOnServer(strImageBase64)
        }
                dismiss(animated: true, completion: nil) //5
    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : Any]?) {
//        let chosenImage = image
//        let dataImage = UIImageJPEGRepresentation(RBResizeImage(chosenImage, targetSize: CGSize(width: 500, height: 500)), 0.1)
//        let strImageBase64 = dataImage!.base64EncodedString(options: .lineLength64Characters)
//        self.imageProfile.image = UIImage(data:dataImage!)
//        self.updateProfileOnServer(strImageBase64)
//        dismiss(animated: true, completion: nil) //5
//    }
    
    

      func RBResizeImage(_ image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
      func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func updateProfileOnServer(_ base64Format:String){
        if appDelegate.networkAvialable == false {
            Globals.ToastAlertForNetworkConnection()
            return
        }
        var dicJobStatusParam : [String:Any]! = Dictionary()
        let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
        let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
        
        dicJobStatusParam["user_id"] = user_ID
        dicJobStatusParam["session"] = session
        dicJobStatusParam["profile_image"] = String(base64Format)
        
        appDelegate.showProgressWithText("Uploading profile picture...")
        ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.updateProfileImage, dicParameters: dicJobStatusParam, successBlock: {(dicData) in
            appDelegate.hideProgress()
            if dicData != nil
            {
                if dicData["status"] as! String == "success" {
                    Globals.ToastAlertWithString("Your profile successfully updated")
                }else {
                    Globals.ToastAlertWithString(dicData["message"] as! String)
                }
            }else {
                Globals.ToastAlertWithString("Network error!")
            }
            appDelegate.hideProgress()
        }) { (error) in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString(error!.description )
            print(error)
            return
        }
        
    }
    
}
