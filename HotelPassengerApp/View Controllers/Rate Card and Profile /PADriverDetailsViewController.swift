//
//  PADriverDetailsViewController.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 04/07/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import UIKit
import DLImageLoader
enum viewTags :Int {
    case lblDate = 20
    case lblTime = 21
    case lblVehicleType = 22
    case lblVehicleName = 23
    case imageDriver = 50
    case ratingView = 60
    case driverName = 51
}

import Alamofire
class PADriverDetailsViewController: UIViewController {
    
    // MARK: - ===property ===
    @IBOutlet var viewDriverImage: UIView!
    @IBOutlet var btnDeleteFromFavourite: UIButton!
    
    @IBOutlet var viewRating: UIView!
    @IBOutlet weak var tblViewDriverFeedback: UITableView!
    
    var didFinishDeletingFavDriver:(()->())?
    
    
    @IBOutlet var topConstarintForTable: NSLayoutConstraint!
    //var isPushedFromActiveJob:Bool! = false
    
    var dicJobDeatils:[String:Any]!
    var dicDriverInfo : [String:Any]!
    var isEditable:Bool! = false
    
    var driverImage:UIImage!
    // MARK: - ===view life cycle ===
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewRating.isUserInteractionEnabled = isEditable
        self.getDriverInfomation()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PADriverDetailsViewController.tapGestureAction(_:)))
        self.view.viewWithTag(96)!.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
        //print( dicJobDeatils)
        //print(dicDriverInfo)
        
    }
    override func viewDidLayoutSubviews() {
        if isEditable == true{
            topConstarintForTable.constant = 84
            btnDeleteFromFavourite.isHidden = false
        }
        else {
            topConstarintForTable.constant = 0
            btnDeleteFromFavourite.isHidden = true
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tapGestureAction(_ tapGesture:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDismissClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func reloadViewAgain()  {
        
        let imageDriver = self.viewDriverImage.viewWithTag(viewTags.imageDriver.rawValue) as! UIImageView
        
        imageDriver.image = UIImage(named: "profile_placeholder")
        
        if let str = self.dicJobDeatils["image"] as? String{
            DLImageLoader.sharedInstance().image(fromUrl: str) { [weak self](error, image) -> () in
                if image != nil
                {
                    self?.driverImage = image
                }
                else {
                    self?.driverImage = UIImage(named: "profile_placeholder")
                }
            }
        }
        else {
            self.driverImage = UIImage(named: "profile_placeholder")
        }
        
        DispatchQueue.main.async { [weak self] in
            self?.tblViewDriverFeedback.reloadData()
        }
    }
    // MARK: - ============table View Data Source  ==========
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int
    {
        if self.dicJobDeatils == nil {
            return 0
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 1 {
            if let arrComment = self.dicJobDeatils["comments"] as? [Any] {
                return arrComment.count
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        var cell : UITableViewCell?
        
        let  cellIndetifier = "cellFeedbackDriverComments"
        //cellCarInfo
        cell  = self.tblViewDriverFeedback.dequeueReusableCell(withIdentifier: cellIndetifier, for: indexPath)
        var lblPaxName : UILabel!
        var txtViewComments : UITextView!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
            lblPaxName = cell!.viewWithTag(62) as! UILabel
            txtViewComments = cell!.viewWithTag(63)! as! UITextView
        }else {
            lblPaxName = cell!.viewWithTag(62) as! UILabel
            txtViewComments = cell!.viewWithTag(63)! as! UITextView
        }
        if let arrComment = self.dicJobDeatils["comments"] as? [[String:Any]] {
            let dicComments = arrComment[indexPath.row]
            if let strPaxname = dicComments["passenger_name"] as? String{
                lblPaxName.text = strPaxname == "" ? "N/A" : strPaxname
            }
            else {
                lblPaxName.text =  "N/A"
            }
            
            if let strComments = dicComments["passenger_comment"] as? String{
                txtViewComments.text = strComments.isEmpty ?  "No comments found" : strComments
            }
            else {
                txtViewComments.text =  "No comments found"
            }
        }
        lblPaxName.numberOfLines = 1
        lblPaxName.font = Globals.defaultAppFontWithBold(13)
        lblPaxName.textColor = UIColor.black
        txtViewComments.font = Globals.defaultAppFontItalic(14)
        txtViewComments.textColor = UIColor.lightBlack()
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        if let arrComment = self.dicJobDeatils["comments"] as? [[String:Any]] {
            let constraint: CGSize = CGSize(width: tblViewDriverFeedback.frame.width, height: 20000.0)
            let dicComments = arrComment[indexPath.row]
            let strComments = dicComments["passenger_comment"] as? String
            var size: CGSize
            let boundingBox: CGSize = strComments!.boundingRect(with: constraint, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName:Globals.defaultAppFontItalic(13)], context: nil).size
            size = CGSize(width: ceil(boundingBox.width), height: ceil(boundingBox.height))
            return size.height > 70 ? size.height + 35 : 70
        }
        return Constants.iPadScreen ? 100 : 70.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch (section){
        case 0:
            return Constants.iPadScreen ? 200 : 160
            
        case 1:
            return 40
            
        default :
            return 70
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch(section){
        case 0:
            self.viewDriverImage.translatesAutoresizingMaskIntoConstraints = true
            self.viewDriverImage.frame = CGRect(x: 0, y: 0, width: self.tblViewDriverFeedback.frame.width, height: 160)
            self.viewDriverImage.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            
            let imageDriver = self.viewDriverImage.viewWithTag(viewTags.imageDriver.rawValue) as! UIImageView
            
            if Constants.iPadScreen == true {
                imageDriver.layer.cornerRadius = imageDriver.frame.height / 2.0
            }else {
                imageDriver.frame.size = CGSize(width: 90, height: 90)
                imageDriver.layer.cornerRadius = 45.0
                imageDriver.layer.borderColor = UIColor.appGrayColor().cgColor
                imageDriver.layer.borderWidth = 0.5
                
            }
            imageDriver.layer.masksToBounds = true
            
            
            if let str = self.dicJobDeatils["image"] as? String{
                if let activity = self.viewDriverImage.viewWithTag(52) as? UIActivityIndicatorView
                {
                    activity.isHidden = false
                    activity.startAnimating()
                }
                DLImageLoader.sharedInstance().image(fromUrl: str) { [weak self](error, image) -> () in
                    if image != nil
                    {
                        imageDriver.image = image
                    }
                    else {
                        imageDriver.image = UIImage(named: "profile_placeholder")
                    }
                    
                    DispatchQueue.main.async { [weak self] in
                        
                        if let activity = self?.viewDriverImage.viewWithTag(52) as? UIActivityIndicatorView
                        {
                            activity.isHidden = true
                            activity.stopAnimating()
                        }
                    }
                }
            }
            else {
                DispatchQueue.main.async { [weak self] in
                    
                    imageDriver.image = UIImage(named: "profile_placeholder")
                    if let activity = self?.viewDriverImage.viewWithTag(52) as? UIActivityIndicatorView
                    {
                        activity.isHidden = true
                        activity.stopAnimating()
                    }
                }
            }
            
            if let lblDriverName = self.viewDriverImage.viewWithTag(viewTags.driverName.rawValue) as? UILabel
            {
                lblDriverName.text = (self.dicJobDeatils["first_name"] as! String) + " " +  (self.dicJobDeatils["last_name"] as! String)
            }
            self.viewDriverImage.updateConstraints()
            
            return self.viewDriverImage
        case 1:
            let view:UIView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40.0))
            let textLabel:UILabel = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.width-20, height: 40.0))
            view.backgroundColor = UIColor.white
            textLabel.textColor = UIColor.appBlackColor()
            textLabel.font = Globals.defaultAppFontWithBold(14)
            textLabel.text = "Passenger comments on last three rides"
            textLabel.textAlignment = NSTextAlignment.center
            view.addSubview(textLabel)
            view.layer.borderWidth = 0.5
            textLabel.numberOfLines = 2
            view.layer.borderColor = UIColor.appGrayColor().cgColor
            return view
            
        default :
            break
        }
        return nil
    }
    func getDriverInfomation()  {
        if appDelegate.networkAvialable == false   {
            return
        }
        appDelegate.showProgressWithText("Please Wait...")
        let userInfo = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        var dicParameters = [String:Any]()
        dicParameters["driver_id"] = self.dicDriverInfo["primary_driver"]!
        dicParameters["user_id"] = userInfo["id"]!
        dicParameters["session"] = userInfo["session"]!
        if let reservation_id = self.dicDriverInfo["id"] as? String{
            dicParameters["reservation_id"] = reservation_id as Any?
        }
        ServiceManager.sharedInstance.dataTaskWithPostRequest("getDriverAllInfomation", dicParameters: dicParameters, successBlock: { [weak self](dicData:[String:Any]!) in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success"{
                self?.dicJobDeatils = dicData["record"] as! [String:Any]
                
                if let ratingStr = self?.dicJobDeatils["rating"] as? String{
                    
                    let floatingRatesStar = self?.viewRating.viewWithTag(viewTags.ratingView.rawValue) as! CosmosView
                    floatingRatesStar.rating = ratingStr.toDouble()!
                    floatingRatesStar.totalStars = 5
                    floatingRatesStar.isUserInteractionEnabled = false
                }
                self?.reloadViewAgain()
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString((error?.description)!)
        }
    }
    
    // MARK: - Delete driver form Favourite
    
    
    @IBAction func btnDeleteFromFavouriteClicked(_ sender: Any) {
        if appDelegate.networkAvialable == false   {
            return
        }
        appDelegate.showProgressWithText("Please Wait...")
        let userInfo = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
        
        var dicParameters = [String:Any]()
        dicParameters["driver_id"] = self.dicDriverInfo["primary_driver"]!
        dicParameters["user_id"] = userInfo["id"]!
        dicParameters["session"] = session
        
        ServiceManager.sharedInstance.dataTaskWithPostRequest("removeFromFavouriteList", dicParameters: dicParameters, successBlock: { [weak self](dicData:[String:Any]!) in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success"{
                Globals.ToastAlertWithString(dicData["message"] as! String)
                self?.didFinishDeletingFavDriver!()
                self?.dismiss(animated: true, completion: nil)
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString((error?.description)!)
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
