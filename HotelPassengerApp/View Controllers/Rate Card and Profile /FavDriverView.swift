//
//  FavDriverView.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 04/07/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import Foundation

class favDriverView: UIView {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imageDriver: UIImageView!
    @IBOutlet var activityView:UIActivityIndicatorView!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    override func awakeFromNib() {
    }
    override func layoutSubviews(){
        self.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.layer.borderWidth = 0.5
    }
    
    func setupDriverView(name:String, imageUrl:String)  {
        
        self.lblName.text = name
        self.activityView.hidden = false;
        self.imageDriver.layer.cornerRadius = 22.5
        self.imageDriver.layer.borderColor = UIColor.appBlackColor().CGColor
        self.imageDriver.layer.borderWidth = 1.0
        self.imageDriver.layer.masksToBounds = true
        
        if appDelegate.networkAvialable == true {
            DLImageLoader.sharedInstance.imageFromUrl(imageUrl, completed: { [weak self](error, image) -> () in
                self?.activityView.hidden = true;
                if image != nil {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self?.imageDriver.image = image
                    })
                }else {
                    self?.imageDriver.image = UIImage(named: "imageProfile")
                }
                })
        }
        else {
            self.activityView.hidden = true;
            self.imageDriver.image = UIImage(named: "imageProfile")
        }
        
    }
    private func commonInit() {
        let bundle = NSBundle(forClass:self.dynamicType)
        let nib = UINib(nibName: "favDriverView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        view.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        view.layer.borderColor = UIColor.lightGrayColor().CGColor
        view.frame = self.bounds
        view.backgroundColor = UIColor.whiteColor()
        self.addSubview(view)
    }
}
