//
//  PABookerMainMenuViewController.swift
//  HotelPassengerApp
//
//  Created by Netquall on 22/08/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit

class PABookerMainMenuViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = true
        
        self.title = "Home"
    // Do any additional setup after loading the view.
    }

    @IBAction func bookReservationClicked(_ sender: UIButton) {
        let detailsVc = UIStoryboard(name: "RideLater", bundle: nil).instantiateViewController(withIdentifier: "PAReservationsRideLaterVC") as! PAReservationsRideLaterVC
        detailsVc.isEditingMode = false
        self.navigationController?.pushViewController(detailsVc, animated: true)
    }
    @IBAction func upcomingReservationClicked(_ sender: UIButton) {
        let detailsVc = appDelegate.storybaord.instantiateViewController(withIdentifier: "UpcomingReservationsViewController") as! UpcomingReservationsViewController
        self.navigationController?.pushViewController(detailsVc, animated: true)
    }
    @IBAction func pastReservationClicked(_ sender: UIButton) {
        let detailsVc = appDelegate.storybaord.instantiateViewController(withIdentifier: "PastReservationsViewController") as! PastReservationsViewController
        self.navigationController?.pushViewController(detailsVc, animated: true)
    }
    @IBAction func paymentHistoryClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showPaymentHistoryVC", sender: self)
    }
    @IBAction func myProfileClicked(_ sender: UIButton) {
        let detailsVc = appDelegate.storybaord.instantiateViewController(withIdentifier: "PAMyProfileViewController") as! PAMyProfileViewController
        self.navigationController?.pushViewController(detailsVc, animated: true)
    }
    @IBAction func backToListingClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showPaymentHistoryVC"{
            
            let des_VC = segue.destination as? PAPaymentHistoryViewController
        }

        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
