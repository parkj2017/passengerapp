//
//  PARateDriverVC.swift
//  PassangerApp
//
//  Created by Netquall on 6/24/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit

class PARateDriverVC: BaseViewController, UITextViewDelegate, BackButtonActionProtocol {

    @IBOutlet var toolBarView: UIToolbar!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewRadioButtons: UIView!
    
    @IBOutlet weak var txtViewComments: UITextView!
    @IBOutlet weak var starRatingViewCosmo: CosmosView!
    var strPaxComments = ""
    var reservationId = ""
    var driverID = ""
    
    // MARK:- ====view life cycle ====
    override func viewDidLoad() {
        super.viewDidLoad()
       self.starRatingViewCosmo.updateOnTouch = true
        self.starRatingViewCosmo.rating = 0.0
        self.txtViewComments.text = "Add Comments"
         self.txtViewComments.textColor = UIColor.whiteColor()
        self.txtViewComments.inputAccessoryView = self.toolBarView
        self.txtViewComments.delegate = self
        Globals.layoutViewFor(self.txtViewComments, color: UIColor.lightTextColor(), width: 1.0, cornerRadius: 0.0)
        // Do any additional setup after loading the view.
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Feedback", imageName: "backArrow")
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         Globals.sharedInstance.delegateBack = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ============text view delegate  ==========
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        if self.txtViewComments == textView {
            if self.txtViewComments.text == "Add Comments" {
                self.txtViewComments.text = ""
            }
        }
        print(Constants.screenHeight)
        
        self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-100.0)
        
        if Constants.screenHeight <= 568.0 {
        if self.view.frame.origin.y == 64.0 {
        UIView.animateWithDuration(0.10) { [weak self] in
            self?.view.frame.origin.y -= 150.0
          }
         }
        }
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        if Constants.screenHeight <= 568.0 {
        UIView.animateWithDuration(0.10) { [weak self] in
            self?.view.frame.origin.y = 64.0
          }
        }
    }
    
    @IBAction func btnDoneOrCancelTextViewAction(sender: UIBarButtonItem) {
        if sender.tag == 19 {
            self.txtViewComments.text = "Add Comments"
        }
        self.view.endEditing(true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnRadioSatisfyOrNotAllSidesClickedAction(sender: UIButton) {
        
        self.strPaxComments = sender.titleForState(.Normal)!
        sender.selected = true
        for btnView in self.viewRadioButtons.subviews {
            if btnView.isKindOfClass(UIButton) {
            if btnView.tag != sender.tag {
                let btn = btnView as! UIButton
                btn.selected = false
             }
           }
        }
        
    }
    
    @IBAction func btnAddDriverAsFavouriteAction(sender: UIButton) {
        if appDelegate.networkAvialable == false   {
            return
        }
        appDelegate.showProgressWithText("Please Wait...")
        let userInfo = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:AnyObject]
        var dicParameters = [String:AnyObject]()
        dicParameters["driver_id"] = self.driverID
        dicParameters["user_id"] = userInfo["id"]!
        dicParameters["session"] = userInfo["session"]!
        dicParameters["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars()
        appDelegate.showProgressWithText("Please wait..")
        ServiceManager.sharedInstance.dataTaskWithPostRequest("addtoFavouriteList", dicParameters: dicParameters, successBlock: { (dicData:[String:AnyObject]!) in
            appDelegate.hideProgress()
            print(dicData)
            if dicData["status"] as! String == "success"{
                Globals.ToastAlertWithString(dicData["message"] as! String)
                
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString(error.description)
        }

    }
    
    @IBAction func btnRateYourRideClickedAction(sender: UIButton) {
        if appDelegate.networkAvialable == false   {
            return
        }
        appDelegate.showProgressWithText("Please Wait...")
        let userInfo = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:AnyObject]
        var dicParameters = [String:AnyObject]()
        dicParameters["driver_id"] = self.driverID
        dicParameters["user_id"] = userInfo["id"]!
        dicParameters["session"] = userInfo["session"]!
        dicParameters["reservation_id"] = self.reservationId
        dicParameters["rating"] = ""
        dicParameters["pass_feedback"] = self.strPaxComments
        dicParameters["avg_rating"] = self.starRatingViewCosmo.rating
        dicParameters["rating_type"] = "single"
        dicParameters["comment"] = self.txtViewComments.text
        dicParameters["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars()
        dicParameters["created_on"] = Globals.sharedInstance.getCurrentDateforOndemandCars()
        appDelegate.showProgressWithText("Please wait..")
        ServiceManager.sharedInstance.dataTaskWithPostRequest("customerfeedback", dicParameters: dicParameters, successBlock: { [weak self](dicData:[String:AnyObject]!) in
            appDelegate.hideProgress()
            print(dicData)
            if dicData["status"] as! String == "success"{
            Globals.ToastAlertWithString(dicData["message"] as! String)
            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.activeJobNotification)
            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.currentJobStaus)
                self?.backToOnDemandViewController()
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString(error.description)
        }

    }
    // MARK:---======back button -========
    func backButtonActionMethod(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
func backToOnDemandViewController(){
        dispatch_async(dispatch_get_main_queue()) { [weak self]() -> Void in
            if self == nil {
                return
            }
        for vc: UIViewController in (self?.navigationController?.viewControllers)! {
            print("view class ----\(vc.className)")
                    if (vc.isKindOfClass(SWRevealViewController)) {
                        self?.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }
        }
}
