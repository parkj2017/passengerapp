//
//  OnDemandViewController.swift
//  PassangerApp
//
//  Created by Netquall on 12/2/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader
import GoogleMaps
import Alamofire
import SwiftyJSON




class OnDemandViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, BackButtonActionProtocol {
    // MARK:- ===== Property Decalaration ========
    
    @IBOutlet weak var tableViewCars: UITableView!
    
    @IBOutlet weak var viewHeaderTableView: UIView!
    
    @IBOutlet var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewDefault: UIScrollView!
    
    @IBOutlet weak var btnSourceAddress: UIButton!
    @IBOutlet  var mapView: GMSMapView!
    @IBOutlet weak var mapCenterPinImage: UIImageView!
    
    
    @IBOutlet weak var btnDestinationAddress: UIButton!
    
    @IBOutlet var viewNearbyFavDrivers: UIView!
    
    var markerDestinationAddress = GMSMarker()
    lazy  var markerPickUpAddress : GMSMarker? = GMSMarker()
    var map_CentreCoordinate : CLLocationCoordinate2D?
    //var mapBounds : GMSCoordinateBounds!
    var carLocationNearest : CLLocation?
    let searchRadius: Double = 100
    var ZoomOut_In : Float = 16.0
    var onDemand_carInformationArray = [[String: AnyObject]]()
    lazy var dicOnDemandCarsAvailable = [String:[[String: AnyObject]]]()
    lazy var dicArrivalTimeOfCarType = [String: AnyObject]()
    var carType_Title = String()
    // images of cars nsdata objects
    lazy var image_DataSedan : NSData? = NSData()
    lazy var image_DataPremSedan = NSData()
    lazy var image_DataSuv = NSData()
    // few
    lazy var arrTotalCarsKeys = [String]()
    lazy var vehicleTypeID: String  = String()
    var indexPathCellSelected : NSIndexPath!
    lazy var locationManagerObjct = LocationManager.sharedInstance
    var arrVehicleInfo = [[String:AnyObject]]()
    lazy var checkForFocusByDestination =  Bool()
    
    var timerCarList : NSTimer!
    var isActiveJobNotificationTap = false
    
    var nearByFavDrivers:[[String:AnyObject]] = [[String:AnyObject]]()
    
    // MARK:- ===== View Life cycle ========
    
    let dicActiveUser = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as? [String:AnyObject]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.locationManagerObjct.dicDestinationLocation!.removeAll()
        self.locationManagerObjct.destinationLocation = nil
        
        
        self.btnSourceAddress.titleLabel?.numberOfLines = 2;
        self.btnDestinationAddress.titleLabel?.numberOfLines = 2;
        
        dispatch_async(dispatch_get_main_queue()) {[weak self] () -> Void in
            self?.viewHeightConstraint.constant = 0.0
            self?.view.layoutIfNeeded()
            self?.view.translatesAutoresizingMaskIntoConstraints = true
        }
        
        self.mapCenterPinImage.hidden = false
        
        // self.setNavigationBarItem()
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Book for now", imageName: "backArrow")
        //self.navigationController?.navigationBar.SetNavigationBarNormalColor()
        
        self.btnSourceAddress.setTitle(self.locationManagerObjct.strCurrentLocation, forState: .Normal)
        self.MapStopScrollingAndGetAddressOfCurrentLocation(locationManagerObjct.currentLocation!)
        // weak var weakSelf = self
        self.locationManagerObjct.pickUpLocation! = self.locationManagerObjct.currentLocation!
        
        let mapBounds:GMSCoordinateBounds = GMSCoordinateBounds(coordinate: self.locationManagerObjct.pickUpLocation!.coordinate, coordinate: self.locationManagerObjct.pickUpLocation!.coordinate)
        dispatch_async(dispatch_get_main_queue(), { [weak self]() -> Void in
            self?.mapView.clear()
            let camera = GMSCameraPosition.cameraWithLatitude((LocationManager.sharedInstance.currentLocation!.coordinate.latitude), longitude:(LocationManager.sharedInstance.currentLocation!.coordinate.longitude) , zoom: 16.0)
            self?.mapView.animateToCameraPosition(camera)
            })
        mapView.delegate = self
        mapView.settings.myLocationButton = true
        mapView.myLocationEnabled = true
        mapView.settings.zoomGestures = true
        self.checkForFocusByDestination = false
        self.locationManagerObjct.destinationLocation = nil
        // map center view button
        
        // self.btnMapViewCenter.titleLabel?.font = UIFont.systemFontOfSize(10.0)
        // self.btnMapViewCenter.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 22, 0)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {[weak self] () -> Void in
            self?.locationManagerObjct.pickUpLocation = self?.locationManagerObjct.currentLocation
            self?.refreshMapViewForCheckOnDemandCars()
        }
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        appDelegate.hideProgress()
        Globals.sharedInstance.delegateBack = self
        Globals.ToastAlertWithString("Loading nearby cars")
        self.refreshNearCarsList()
        
        self.mapView.bringSubviewToFront(self.mapCenterPinImage)
        if let btn = self.mapView.viewWithTag(30) {
            self.mapView.bringSubviewToFront(btn)
        }
        if let btn = self.mapView.viewWithTag(31) {
            self.mapView.bringSubviewToFront(btn)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopRefreshCarListTimer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK:- ===timer method to refresh near car list =====
    func refreshNearCarsList(){
        // self.refreshMapViewForCheckOnDemandCars()
        self.timerCarList = NSTimer.scheduledTimerWithTimeInterval(10.0, target:self, selector: #selector(OnDemandViewController.refreshMapViewForCheckOnDemandCars), userInfo:nil , repeats: true)
    }
    func stopRefreshCarListTimer(){
        if self.timerCarList !=  nil {
            if self.timerCarList.valid{
                self.timerCarList.invalidate()
                
            }
        }
    }
    // MARK: ===== Custom Button Methodsss ========
    @IBAction func btnZoomInMapViewACtion(sender: UIButton) {
        if ZoomOut_In < 0.0{
            ZoomOut_In = 0.0
        }
        if ZoomOut_In >= 0.0 {
            ZoomOut_In += 3.0
            self.mapView.animateToZoom(ZoomOut_In)
        }
    }
    
    @IBAction func btnZoomOutMapViewAction(sender: UIButton) {
        if ZoomOut_In < 0.0{
            ZoomOut_In = 0.0
        }
        if ZoomOut_In >= 0.0 {
            ZoomOut_In -= 3.0
            self.mapView.animateToZoom(ZoomOut_In)
        }
    }
    @IBAction func btnCarsPressedForGettingBaseRate(sender: UIButton) {
    }
    // MARK:- ===add custom pickup address button action =====
    
    @IBAction func btnAddSourceAddressAction(sender: UIButton) {
        self.checkForFocusByDestination = false
        // weak var weakSelf = self
        let searchVC = self.storyboard?.instantiateViewControllerWithIdentifier("SearchPlacesAddessViewController") as! SearchPlacesAddessViewController
        searchVC.strTitleOfView = "Source Address"
        searchVC.isPickUpAddress = true
        searchVC.viewFrom = "onDemand"
        searchVC.finishSelectingDropOffAddress = {[weak self] (dicDropOff : [String:AnyObject]) -> () in
            if dicDropOff.count != 0 {
                self?.locationManagerObjct.dicSourceCustomLocation = dicDropOff
                
                self?.locationManagerObjct.pickUpLocation! = CLLocation(latitude: (dicDropOff["latitude"]!).doubleValue, longitude: (dicDropOff["longitude"]!).doubleValue)
                self?.btnSourceAddress.setTitle(dicDropOff["address"] as? String, forState: .Normal)
                self?.locationManagerObjct.strCurrentLocation = (dicDropOff["address"] as? String)!
                self?.showPickUpAddressOnMap()
                self?.refreshMapViewForCheckOnDemandCars()
                if  self?.locationManagerObjct.destinationLocation != nil {
                    self?.calulateEstTotalFairBtSourceDestination()
                }
            }
        }
        let nvc = UINavigationController(rootViewController: searchVC)
        searchVC.strTypeOfAddress = "Pick Up Address"
        self.navigationController?.presentViewController(nvc, animated: true, completion: nil)
    }
    // MARK:- ===add custom Drop off address button action =====
    
    @IBAction func btnAddDestinationToGetAFareAction(sender: UIButton) {
        
        self.checkForFocusByDestination = false
        // weak var weakSelf = self
        let searchVC = self.storyboard?.instantiateViewControllerWithIdentifier("SearchPlacesAddessViewController") as! SearchPlacesAddessViewController
        searchVC.strTitleOfView = ""
        searchVC.isPickUpAddress = false
        searchVC.viewFrom = "onDemand"
        searchVC.finishSelectingDropOffAddress = { [weak self](dicDropOff : [String:AnyObject]) -> () in
            if dicDropOff.count != 0 {
                self?.locationManagerObjct.dicDestinationLocation = dicDropOff
                self?.locationManagerObjct.destinationLocation = CLLocation(latitude: (dicDropOff["latitude"]!).doubleValue, longitude: (dicDropOff["longitude"]!).doubleValue)
                self?.showDestinationAddressOnMap(dicDropOff)
                self?.refreshMapViewForCheckOnDemandCars()
                
                self?.calulateEstTotalFairBtSourceDestination()
            }
        }
        let nvc = UINavigationController(rootViewController: searchVC)
        //searchVC.title = "Destination Address"
        searchVC.strTypeOfAddress = "Destination Address"
        self.navigationController?.presentViewController(nvc, animated: true, completion: nil)
    }
    // MARK: ========Show Marker on Map of pickup and Destination Address ==================
    func showPickUpAddressOnMap(){
        self.checkForFocusByDestination = false
        self.markerPickUpAddress!.map = nil
        //let destinationLocation = CLLocation(latitude: dicDestination["latitude"] as! Double, longitude: dicDestination["longitude"] as! Double)
        self.markerPickUpAddress!.position = CLLocationCoordinate2DMake((self.locationManagerObjct.pickUpLocation?.coordinate.latitude)!, (self.locationManagerObjct.pickUpLocation?.coordinate.longitude)!)
        // self.markerPickUpAddress!.userData = dicDestination["address"] as? String
        dispatch_async(dispatch_get_main_queue(), { [weak self]() -> Void in
            self?.mapCenterPinImage.hidden = false
            // self?.markerPickUpAddress!.icon = UIImage(named: "mappin")
            // self?.markerPickUpAddress!.map = self?.mapView
            })
        let mapBounds:GMSCoordinateBounds = GMSCoordinateBounds(coordinate:self.locationManagerObjct.pickUpLocation!.coordinate , coordinate: self.locationManagerObjct.pickUpLocation!.coordinate)
        //weak var weakSelf = self
        UIView.animateWithDuration(0.20, animations: { [weak self]() -> Void in
            
            if self?.viewNearbyFavDrivers.hidden == false{
                self?.mapView.animateToCameraPosition((self?.mapView.cameraForBounds(mapBounds, insets:UIEdgeInsetsMake(90.0, 40.0, 40.0, 40.0)))!)
            }else{
                self?.mapView.animateToCameraPosition((self?.mapView.cameraForBounds(mapBounds, insets:UIEdgeInsetsMake(40.0, 40.0, 40.0, 40.0)))!)
            }
            })
    }
    func showDestinationAddressOnMap(dicDestination:[String:AnyObject]){
        
        print(dicDestination)
        self.checkForFocusByDestination = false
        self.markerDestinationAddress.map = nil
        self.btnDestinationAddress.setTitle(dicDestination["address"] as? String, forState: .Normal)
        let postion = CLLocationCoordinate2DMake((dicDestination["latitude"]!).doubleValue, (dicDestination["longitude"]!).doubleValue)
        self.markerDestinationAddress.position = postion
        self.markerDestinationAddress.userData = dicDestination["address"] as? String
        dispatch_async(dispatch_get_main_queue(), { [weak self]() -> Void in
            self?.markerDestinationAddress.icon = UIImage(named: "customer")
            self?.markerDestinationAddress.map = self?.mapView
            })
        if let _ = self.locationManagerObjct.pickUpLocation {
            self.markerPickUpAddress!.position = CLLocationCoordinate2DMake((self.locationManagerObjct.pickUpLocation?.coordinate.latitude)!, (self.locationManagerObjct.pickUpLocation?.coordinate.longitude)!)
            self.mapCenterPinImage.hidden = true
            // self.markerPickUpAddress!.userData = dicDestination["address"] as? String
            dispatch_async(dispatch_get_main_queue(), { [weak self]() -> Void in
                self?.markerPickUpAddress!.icon = UIImage(named: "mappin")
                self?.markerPickUpAddress!.map = self?.mapView
                })
        }
        dispatch_async(dispatch_get_main_queue(), { [weak self]() -> Void in
            self?.focusMapOnCurrentLocationWith((dicDestination["latitude"]!).doubleValue, longitude: (dicDestination["longitude"]!).doubleValue)
            })
    }
    
    // MARK:- ===== update Location and Zoom ========
    func focusMapOnCurrentLocationWith(latitude:Double, longitude:Double){
        //weak var weakSelf = self
        let locationFocus = CLLocation(latitude: Double(latitude), longitude: Double(longitude))
        // self.mapBounds = nil
        var mapBounds:GMSCoordinateBounds = GMSCoordinateBounds(coordinate:locationFocus.coordinate , coordinate: locationFocus.coordinate)
        let boundsSource = GMSCoordinateBounds(coordinate:locationManagerObjct.pickUpLocation!.coordinate, coordinate: locationManagerObjct.pickUpLocation!.coordinate)
        if let _ = self.carLocationNearest {
            if let boundsCar = GMSCoordinateBounds(coordinate:self.carLocationNearest!.coordinate, coordinate: self.carLocationNearest!.coordinate) as? GMSCoordinateBounds{
                mapBounds = mapBounds.includingBounds(boundsCar)
            }
        }
        mapBounds = mapBounds.includingBounds(boundsSource)
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            UIView.animateWithDuration(0.20, animations: { [weak self]() -> Void in
                if self?.viewNearbyFavDrivers.hidden == false{
                    self?.mapView.animateToCameraPosition((self?.mapView.cameraForBounds(mapBounds, insets:UIEdgeInsetsMake(90.0, 40.0, 40.0, 40.0)))!)
                }else{
                    self?.mapView.animateToCameraPosition((self?.mapView.cameraForBounds(mapBounds, insets:UIEdgeInsetsMake(40.0, 40.0, 40.0, 40.0)))!)
                }
                })
        }
    }
    // MARK: ===== Estimantion Fair Between source and Destination ========
    func calulateEstTotalFairBtSourceDestination(){
        //        if locationManagerObjct.destinationLocation == nil {
        //            return
        //        }
        for key in  (self.arrTotalCarsKeys) {
            let dicCarArray = (self.dicOnDemandCarsAvailable[key])! as [[String:AnyObject]]
            var amt:Float = 0.0
            for index in 0..<dicCarArray.count
            {
                let valuesDict = dicCarArray[index]
                print(valuesDict)
                if let strFareEstimated =  valuesDict["grandTotal"]{
                    let estFare =  String(strFareEstimated).toFloat()
                    if index == 0{
                        self.dicArrivalTimeOfCarType[key + "fare"] = String(format: "%.2f", estFare)
                    }
                    if estFare > amt{
                        amt = estFare
                    }
                }
            }
            self.dicArrivalTimeOfCarType[key + "preAuthAmt"] = String(format: "%.2f", amt)
        }
        print(self.dicArrivalTimeOfCarType)
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tableViewCars.reloadData()
        })
    }
    // MARK:- ===== Webcall to Check OnDemand cars Avaialble ========
    func refreshMapViewForCheckOnDemandCars()
    {
        if appDelegate.networkAvialable == true {
            if  let  newLocation = self.locationManagerObjct.pickUpLocation{
                
                // weak var weakSelf  = self
                if dicActiveUser == nil{
                    return
                }
                var dicParameters = [String : AnyObject]()
                if carType_Title != ""{
                    dicParameters["vehicle_type"] = carType_Title
                }
                dicParameters["user_id"] = dicActiveUser!["id"]
                dicParameters["session"] = dicActiveUser!["session"]
                dicParameters["latitude"] = "\(newLocation.coordinate.latitude)"
                dicParameters["longitude"] = "\(newLocation.coordinate.longitude)"
                dicParameters["on_demand_radius"] = appDelegate.ondemandRadius
                dicParameters["pickup_date_time"] = Globals.sharedInstance.getCurrentDateforOndemandCars() //"2016-05-05  19:49:12"
                dicParameters["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars()
                
                dicParameters["company_id"] = Constants.CompanyID
                
                if  let  newLocation = self.locationManagerObjct.destinationLocation{
                    
                    dicParameters["dropoff_latitude"] = "\(newLocation.coordinate.latitude)"
                    dicParameters["dropoff_longitude"] = "\(newLocation.coordinate.longitude)"
                }
                else {
                    dicParameters["dropoff_latitude"] = ""
                    dicParameters["dropoff_longitude"] = ""
                }
                
                print("refresh map view dic param -----\(dicParameters)")
                self.tableViewCars.userInteractionEnabled = false
                (self.viewHeaderTableView.viewWithTag(13) as! UIActivityIndicatorView).startAnimating()
                
                
                let url = Constants.baseUrl + Constants.getNearestCarsNew
                let URL = NSURL(string:url)!
                let mutableURLRequest = NSMutableURLRequest(URL: URL)
                mutableURLRequest.HTTPMethod = "POST"
                
                // let parameters = ["foo": "bar"]
                
                do {
                    mutableURLRequest.HTTPBody = try NSJSONSerialization.dataWithJSONObject(dicParameters, options: NSJSONWritingOptions())
                    mutableURLRequest.timeoutInterval = 45
                } catch {
                    print("Error--- \(error)")
                    return
                }
                NSLog("%@, %@", url, dicParameters)
                
                mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                //   Alamofire.request(mutableURLRequest).validate().responseJSON {[weak self] (Response) -> Void in
                Alamofire.request(.POST, URL, parameters:dicParameters , encoding: .JSON).responseJSON { [weak self](Response) -> Void in
                    if self == nil {
                        return
                    }
                    if Response.result.isSuccess{
                        if let  jsonDataRecieved = Response.data{
                            let jsonDicRecieved = JSON(data:jsonDataRecieved )
                            if let dic = jsonDicRecieved.dictionaryObject {
                                if dic["status"] as! String == "session_expired"{
                                    let navigation = appDelegate.window?.rootViewController as? UINavigationController
                                    if let _ = navigation {
                                        navigation!.popToRootViewControllerAnimated(true)
                                    }
                                    appDelegate.hideProgress()
                                    Globals.logout()
                                }
                                print("cars avaible near ---\(dic)")
                                if dic["status"] as! String == "success" {
                                    let arrKeys = dic.keys
                                    if  !(arrKeys.contains("message"))  {
                                        if let carsDetails = dic.formatDictionaryForNullValues(dic){
                                            self?.showCarsOnMap(carsDetails)
                                            self?.showFavouriteDriverIfAny()
                                            self?.calulateEstTotalFairBtSourceDestination()
                                        }
                                        else {
                                            self?.showErrorMessage(dic["message"] as! String)
                                        }
                                    } else {
                                        self?.showErrorMessage(dic["message"] as! String)
                                    }
                                }
                                else if dic["status"] as! String == "payment_pending" {
                                    /*  {
                                     "status": "payment_pending",
                                     "message": "Pending Payment",
                                     "records": [
                                     {
                                     "reservation_id": "6812",
                                     "company_id": "40",
                                     "grand_total": "70.00",
                                     "conf_id": "-10-",
                                     "pu_date": "2016-09-26",
                                     "pu_time": "14:39:00",
                                     "pickup": "Unnamed Road, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 140308, India",
                                     "dropoff": ""
                                     }
                                     ]
                                     }*/
                                    if let dicRecords = dic["records"] as? [[String : AnyObject]]
                                    {
                                        self?.stopRefreshCarListTimer()
                                        let detailsVc = self?.storyboard?.instantiateViewControllerWithIdentifier("PADuePaymentsViewController") as! PADuePaymentsViewController
                                        detailsVc.arrUnpaidReservations = dicRecords
                                         self?.navigationController?.pushViewController(detailsVc, animated: true)
                                    }
                                }
                            }
                            else {
                                self?.showErrorMessage("Error")
                            }
                        }else {
                            self?.showErrorMessage("Error")
                        }
                    }else {
                        let error = Response.result.error
                        print("error near by cars -- \(error)")
                        self?.showErrorMessage("Network Error")
                    }
                }
            }
        }
    }
    
    func showErrorMessage(message:String) {
        dispatch_async(dispatch_get_main_queue()) {
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)
            self.viewNearbyFavDrivers.hidden = true
            CATransaction.commit()
            self.noCarsAvailbleShowToTable(message)
        }
    }
    
    
    func showFavouriteDriverIfAny()  {
        
        nearByFavDrivers.removeAll()
        
        if self.arrVehicleInfo.count>0{
            for index in 0..<self.arrVehicleInfo.count
            {
                let dicVehicleType = self.arrVehicleInfo[index]
                
                if let arrOnDemandCars = self.dicOnDemandCarsAvailable[dicVehicleType["vehicle_type_title"]! as! String]
                {
                    let temp:[[String:AnyObject]]! = arrOnDemandCars.filter({ (car:[String:AnyObject]) -> Bool in
                        if let distace = car["fav_driver"] as? String
                        {
                            if distace == "1" {
                                return true
                            }
                            else {
                                return false
                            }
                        }
                        return false
                    })
                    nearByFavDrivers.appendContentsOf(temp)
                }
                print(nearByFavDrivers)
                
                if nearByFavDrivers.count > 0{
                    
                    (self.viewNearbyFavDrivers.viewWithTag(1000))!.alpha = 1.0
                    (self.viewNearbyFavDrivers.viewWithTag(1001))!.alpha = 1.0
                    
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.viewNearbyFavDrivers.hidden = false
                        
                        UIView.animateWithDuration (1.0, delay: 0.0, options:[.Autoreverse, .Repeat, .AllowUserInteraction] ,animations: {
                            (self.viewNearbyFavDrivers.viewWithTag(1000))?.alpha = 0.1
                            (self.viewNearbyFavDrivers.viewWithTag(1001))?.alpha = 0.1
                            
                            }, completion: { _ in
                        })
                    }
                    
                }
                else {
                    dispatch_async(dispatch_get_main_queue()) {
                        CATransaction.begin()
                        CATransaction.setAnimationDuration(1.0)
                        self.viewNearbyFavDrivers.hidden = true
                        CATransaction.commit()
                    }
                }
            }
        }
    }
    
    @IBAction func showMyFavDrivers(sender: AnyObject) {
        
        if nearByFavDrivers.count > 0 {
            let detailsVc = self.storyboard?.instantiateViewControllerWithIdentifier("PAOnDemandFavDriversViewController") as! PAOnDemandFavDriversViewController
            detailsVc.favDriversArray = nearByFavDrivers
            
            detailsVc.didFinishSelectingDrivers = {[weak self] (selectedDrivers:[[String:AnyObject]]) -> () in
                
                let payment = self?.storyboard?.instantiateViewControllerWithIdentifier("PAPaymentForOnDemandVC") as! PAPaymentForOnDemandVC
                payment.arrOndemanCarsSelect = selectedDrivers
                payment.isJobRequest = true
                
                //calculation of preauthrization amount for booking
                if self?.locationManagerObjct.destinationLocation != nil{
                    var amt:Float = 0.0
                    for index in 0..<selectedDrivers.count
                    {
                        let valuesDict = selectedDrivers[index]
                        
                        if let strFareEstimated =  valuesDict["grandTotal"]{
                            let estFare =  String(strFareEstimated).toFloat()
                            if estFare > amt{
                                amt = estFare
                            }
                        }
                    }
                    payment.strPreAuthAmountForJob =  "\(amt)"
                    
                }
                else {
                    payment.strPreAuthAmountForJob = ""
                }
                self?.navigationController?.pushViewController(payment, animated: true)
                
            }
            
            detailsVc.view.backgroundColor = UIColor.clearColor()
            detailsVc.modalPresentationStyle = .OverFullScreen
            detailsVc.providesPresentationContextTransitionStyle = true
            detailsVc.definesPresentationContext = true
            self.providesPresentationContextTransitionStyle = true
            self.definesPresentationContext = true
            self.modalTransitionStyle = .CrossDissolve
            self.modalPresentationStyle = .OverFullScreen
            self.presentViewController(detailsVc, animated: true) {}
        }
        else {
            Globals.ToastAlertWithString("Please wait...")
        }
    }
    // MARK:- ===update table and map when no cars available  =====
    
    func noCarsAvailbleShowToTable(strMessage:String){
        self.carLocationNearest = nil
        self.tableViewCars.userInteractionEnabled = true
        (self.viewHeaderTableView.viewWithTag(13) as! UIActivityIndicatorView).stopAnimating()
        for keyCar in self.arrTotalCarsKeys {
            self.dicArrivalTimeOfCarType[keyCar] = "Loading..."
        }
        
        self.mapView.clear();
        self.tableViewCars.reloadData()
        self.showTableViewForCars(false, withHeight: 0.0)
        Globals.ToastAlertWithString(strMessage)
    }
    // MARK:- ===update table and map when cars available  =====
    
    func showCarsOnMap(dicRecieved:[String: AnyObject]){
        
        print("showCarsOnMap(dicRecieved:[String: AnyObject]")
        var arrKeys = [String]()
        for keys in dicRecieved.keys{
            arrKeys.append(keys)
        }
        if arrKeys.contains("message"){
            (self.viewHeaderTableView.viewWithTag(13) as! UIActivityIndicatorView).stopAnimating()
            self.showTableViewForCars(false, withHeight: 0.0)
            self.tableViewCars.userInteractionEnabled = true
            JLToast.makeText("Sorry there are no cars in the area", duration: JLToastDelay.ShortDelay).show()
            return
        }
        self.mapView.clear()
        // show destination address on Map
        if locationManagerObjct.pickUpLocation != nil{
            // self.showPickUpAddressOnMap()
            self.mapCenterPinImage.hidden = false
        }
        if locationManagerObjct.dicDestinationLocation!.count != 0 {
            self.showDestinationAddressOnMap(self.locationManagerObjct.dicDestinationLocation!)
        }
        self.dicOnDemandCarsAvailable.removeAll()
        self.onDemand_carInformationArray.removeAll()
        self.arrTotalCarsKeys.removeAll()
        self.arrVehicleInfo.removeAll()
        self.arrVehicleInfo = dicRecieved["records"]!["vehicle_types"] as! [[String : AnyObject]]
        
        var firstLoop = 0
        for dic in self.arrVehicleInfo {
            self.arrTotalCarsKeys.append(dic["vehicle_type_title"] as! String)
            firstLoop += 1
        }
        if self.arrVehicleInfo.count != 0 {
            self.tableViewCars.reloadData()
            let heightTable = CGFloat(100) + CGFloat(60 * self.arrTotalCarsKeys.count)
            self.showTableViewForCars(true, withHeight: heightTable)
        }
        
        print("First Lop \(firstLoop)")
        let dicRecords = dicRecieved["records"] as! [String : AnyObject]
        var totalLoop = 0
        for key in self.arrTotalCarsKeys {
            totalLoop += 1
            var arrTypeCar = [[String : AnyObject]]()
            arrTypeCar = dicRecords[key] as! [[String : AnyObject]]
            for dicRecords  in arrTypeCar{
                totalLoop += 1
                let marker = GMSMarker()
                let latitude: Double =  CDouble(dicRecords["latitude"]! as! String)!
                let longitude: Double = CDouble(dicRecords["longitude"]! as! String)!
                let postion = CLLocationCoordinate2DMake(latitude, longitude)
                marker.position = postion
                
                DLImageLoader.sharedInstance.imageFromUrl(dicRecords["map_icon"]! as! String , completed: { (error, image) -> () in
                    if image != nil {
                        let size = CGSize(width: 30 , height: 30)
                        UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
                        image.drawInRect(CGRectMake(0, 0, size.width, size.height))
                        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
                        UIGraphicsEndImageContext()
                        marker.icon = newImage
                    }else {
                        marker.icon = UIImage(named: "sedan")
                    }
                })
                dispatch_async(dispatch_get_main_queue(), { [weak self] () -> Void in
                    marker.map = self?.mapView
                    })
                self.onDemand_carInformationArray.append(dicRecords)
            }
            arrTypeCar.sortInPlace({ (s1 : [String : AnyObject]!, s2: [String : AnyObject]!) -> Bool in
                CDouble(s1["distance"] as! String) <  CDouble(s2["distance"] as! String)
            })
            self.getNearestCarDistance(arrTypeCar.first!)
            self.dicOnDemandCarsAvailable[key] = arrTypeCar
            arrTypeCar.removeAll()
        }
        
        print("total loops-- \(totalLoop ) and all loop \(totalLoop + firstLoop)")
        self.onDemand_carInformationArray.sortInPlace({ (s1 : [String : AnyObject]!, s2: [String : AnyObject]!) -> Bool in
            CDouble(s1["distance"] as! String) <  CDouble(s2["distance"] as! String)
        })
        let dicNearCar = self.onDemand_carInformationArray.first
        self.carLocationNearest  = CLLocation(latitude: Double(dicNearCar!["latitude"] as! String!)!, longitude: Double(dicNearCar!["longitude"] as! String!)!)
        
    }
    
    func getNearestCarDistance(carsObject:[String:AnyObject]){
        
        if self.onDemand_carInformationArray.count != 0 {
            //weak var weakSelf = self
            //  self.vehicleTypeID = (carsObject.vehicle_type_id)
            let destiLoc : CLLocation = CLLocation(latitude:CDouble(carsObject["latitude"] as! String)!
                ,longitude: CDouble(carsObject["longitude"] as! String)!)
            
            let urlStr = Globals.sharedInstance.getFullApiAddressForDistanceToDriverDistance(self.locationManagerObjct.pickUpLocation, destinationLoc: destiLoc)
            
            if urlStr == "" {
                return
            }
            
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                ServiceManager.sharedInstance.dataTaskWithGetRequest(urlStr, successBlock: {[weak self] (dicData) -> Void in
                    
                    self?.getNearCarsArrivalTimeInfo(dicData as [String:AnyObject], carObject: carsObject)
                    }, failureBlock: { (error) -> () in
                        (UIApplication.sharedApplication().delegate! as! AppDelegate).hideProgress()
                        print("get near car distance error \(error.description)")
                })
            })
        }
        
    }
    func getNearCarsArrivalTimeInfo(dicCarsTimeInfo: [String:AnyObject], carObject: [String:AnyObject] ) {
        
        let arrRoutes = dicCarsTimeInfo["routes"] as! [[String : AnyObject]]
        if arrRoutes.count > 0 {
            
            let dicMainRoutes = arrRoutes.first!
            let arrLegs = dicMainRoutes["legs"] as! [[String:AnyObject]]
            let dicDuration = arrLegs.first!["duration"] as! [String:AnyObject]
            //   weak var weakSelf = self
            var strTimeArrival = dicDuration["text"] as! String
            
            if strTimeArrival.containsString("hours") {
                
                strTimeArrival = strTimeArrival.stringByReplacingOccurrencesOfString("hours", withString: "h")
                strTimeArrival = strTimeArrival.stringByReplacingOccurrencesOfString("mins", withString: "m")
                strTimeArrival = strTimeArrival.stringByReplacingOccurrencesOfString("min", withString: "m")
                
            }
            if strTimeArrival.containsString("hour") {
                strTimeArrival = strTimeArrival.stringByReplacingOccurrencesOfString("hour", withString: "h")
                strTimeArrival = strTimeArrival.stringByReplacingOccurrencesOfString("mins", withString: "m")
                strTimeArrival = strTimeArrival.stringByReplacingOccurrencesOfString("min", withString: "m")
            }
            (UIApplication.sharedApplication().delegate! as! AppDelegate).hideProgress()
            self.dicArrivalTimeOfCarType[carObject["vehicle_type_title"] as! String] = strTimeArrival
            self.tableViewCars.userInteractionEnabled = true
            (self.viewHeaderTableView.viewWithTag(13) as! UIActivityIndicatorView).stopAnimating()
            dispatch_async(dispatch_get_main_queue(), {[weak self]() -> Void in
                self?.tableViewCars.reloadData()
                self?.tableViewCars.selectRowAtIndexPath(self?.indexPathCellSelected, animated: false, scrollPosition:.None)
                })
        }
    }
    func showTableViewForCars(show:Bool, withHeight:CGFloat){
        
        dispatch_async(dispatch_get_main_queue()) {[weak self] () -> Void in
            UIView.animateWithDuration(1.0, animations: { () -> Void in
                if show == true {
                    self?.viewHeightConstraint.constant = withHeight > 230 ? 230 : withHeight
                }else {
                    self?.viewHeightConstraint.constant = 0.0
                }
                self?.view.layoutIfNeeded()
            })
        }
    }
    func showSelectdTypeOfCarOnMap(arrCarObjectS:[OnDemandCarsAvailable]){
        self.mapView.clear()
        // show destination address on Map
        if locationManagerObjct.dicDestinationLocation!.count != 0 {
            self.showDestinationAddressOnMap(locationManagerObjct.dicDestinationLocation!)
            
        }
        for carsObjct in arrCarObjectS{
            let marker = GMSMarker()
            let latitude: Double = CDouble(carsObjct.latitude)!
            let longitude: Double = CDouble(carsObjct.longitude)!
            let postion = CLLocationCoordinate2DMake(latitude, longitude)
            marker.position = postion
            let imageData : NSData? = Globals.sharedInstance.getValueFromUserDefaultsForKey(carsObjct.vehicle_type_id) as? NSData
            if imageData != nil {
                image_DataSedan = imageData!
            }else {
                if let  _ = NSData().vehicleImageForType(carsObjct.vehicle_type_id) {
                    self.image_DataSedan = NSData().vehicleImageForType(carsObjct.vehicle_type_id)!
                }
            }
            if let imageCar = UIImage(data: image_DataSedan!, scale: 1.5) {
                marker.icon = imageCar
            }else {
                marker.icon = UIImage(named: "sedan")
                
            }
            
            marker.userData = carsObjct
            marker.map = self.mapView
        }
    }
    
    
    
    
    // MARK:- ========table View data source and  Delegate-=============
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrVehicleInfo.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cellIndetifier = "cellAvailableCars"
        var cell : CarsAvailableCellOnDemand? = self.tableViewCars.dequeueReusableCellWithIdentifier(cellIndetifier, forIndexPath: indexPath) as? CarsAvailableCellOnDemand
        // var cell : CarsAvailableCellOnDemand? = self.tableViewCars.dequeueReusableCellWithIdentifier(cellIndetifier) as? CarsAvailableCellOnDemand
        if cell == nil {
            cell! = UITableViewCell(style: .Default, reuseIdentifier: cellIndetifier) as! CarsAvailableCellOnDemand
        }
        
        let dicVehicle = self.arrVehicleInfo[indexPath.row]
        let vehicleName = dicVehicle["vehicle_type_title"]! as! String
        
        if cell!.selected {
            
            if appDelegate.networkAvialable == true {
                if let keyForImage = dicVehicle["active_icon"]{
                    DLImageLoader.sharedInstance.imageFromUrl(keyForImage as! String, completed: { (error, image) -> () in
                        if image != nil {
                            cell!.imageCarIcon.image = image
                        }else {
                            cell!.imageCarIcon.image = UIImage(named: "Defaultcar_Selectec")
                        }
                    })
                }
                else {
                    cell!.imageCarIcon.image = UIImage(named: "Defaultcar_Selectec")
                }
                
            }
            
            // cell!.contentView.backgroundColor = UIColor.appRedColor()
            cell?.SetFontColorOfCellSubViews(cell!, color: UIColor.whiteColor())
            cell!.viewCarNamePrize.backgroundColor = UIColor.appRedColor()
            
            cell!.imageTimeClock.image = UIImage(named: "timeWhite")
        }
        else {
            if appDelegate.networkAvialable == true {
                if let keyForImage = dicVehicle["inactive_icon"]{
                    DLImageLoader.sharedInstance.imageFromUrl(keyForImage as! String, completed: { (error, image) -> () in
                        if image != nil {
                            cell!.imageCarIcon.image = image
                        }else {
                            cell!.imageCarIcon.image = UIImage(named: "Defaultcar")
                        }
                    })
                }
                else {
                    cell!.imageCarIcon.image = UIImage(named: "Defaultcar")
                }
                
            }
            
            cell?.SetFontColorOfCellSubViews(cell!, color: UIColor.blackColor())
            //cell!.contentView.backgroundColor = UIColor(red: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
            cell!.viewCarNamePrize.backgroundColor = UIColor.whiteColor()
            cell!.imageTimeClock.image = UIImage(named: "time")
        }
        if let strArivalTime = self.dicArrivalTimeOfCarType[vehicleName] as? String{
            cell!.activityIndicator.hidden = true
            cell!.activityIndicator.stopAnimating()
            cell!.lblArivalTime.text! = strArivalTime
            cell!.lblArivalTime.hidden = false
        }else {
            cell!.lblArivalTime.hidden = true
            cell!.activityIndicator.hidden = false
            cell!.activityIndicator.startAnimating()
        }
        
        print( self.dicOnDemandCarsAvailable)
        
        if dicActiveUser != nil{
            
            if let val = dicActiveUser!["showCompanyNameToPassanger"] as? String{
                if val.compare("Y") {
                    
                    if let arrOnDemandCars = self.dicOnDemandCarsAvailable[vehicleName]
                    {
                        let onDemandCarObjct = arrOnDemandCars.first!
                        cell?.lblCarName.text! = String(format: "%@\n (%@)", "\(vehicleName.uppercaseString)", "\(onDemandCarObjct["company_name"]!)" )
                        
                        if let seat_capacity = onDemandCarObjct["max_seat_available"]
                        {
                            cell!.lblMaxSeats.text = String(format: "%@ Pax", "\(String(seat_capacity))" )
                        }
                        if let strFareEstimated =  onDemandCarObjct["grandTotal"]{
                            let strFareEstimated =  String(strFareEstimated).toFloat()
                            cell!.lblCarPrice.text! =  String(format: "\(appDelegate.currency) %.2f", strFareEstimated)
                        }
                        else {
                            cell!.lblCarPrice.text! = ""
                        }
                    }
                }
                else {
                    if let arrOnDemandCars = self.dicOnDemandCarsAvailable[vehicleName]
                    {
                        let onDemandCarObjct = arrOnDemandCars.first!
                        cell?.lblCarName.text! = (vehicleName.uppercaseString)
                        if let seat_capacity = onDemandCarObjct["max_seat_available"]
                        {
                            cell!.lblMaxSeats.text = String(format: "%@ Pax", "\(String(seat_capacity))" )
                        }
                        if let strFareEstimated =  onDemandCarObjct["grandTotal"]{
                            let strFareEstimated =  String(strFareEstimated).toFloat()
                            cell!.lblCarPrice.text! =  String(format: "\(appDelegate.currency) %.2f", strFareEstimated)
                        }
                        else {
                            cell!.lblCarPrice.text! = ""
                        }
                    }
                }
                
            }
        }
        
        return cell!
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.viewHeaderTableView!
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        // self.tableViewCars.setEditing(true, animated: false)
        
        let dicVehicleType = self.arrVehicleInfo[indexPath.row]
        if let strArivalTime = self.dicArrivalTimeOfCarType[dicVehicleType["vehicle_type_title"]! as! String] as? String
        {
            if strArivalTime != Constants.strNoCars {
                let cell = self.tableViewCars.cellForRowAtIndexPath(indexPath) as! CarsAvailableCellOnDemand
                cell.setSelected(true, animated: false)
                self.tableViewCars.reloadData()
                self.tableViewCars.selectRowAtIndexPath(indexPath, animated: false, scrollPosition:.None)
                if let arrOnDemandCars = self.dicOnDemandCarsAvailable[dicVehicleType["vehicle_type_title"]! as! String]
                {
                    self.indexPathCellSelected = indexPath
                    let onDemandCarObjct = arrOnDemandCars.first!
                    // self.showSelectdTypeOfCarOnMap(arrOnDemandCars)
                    self.vehicleTypeID = String(onDemandCarObjct["id"])
                    
                    self.locationManagerObjct.strVehicleType = String(dicVehicleType["vehicle_type_title"]!)
                    
                    let payment = self.storyboard?.instantiateViewControllerWithIdentifier("PAPaymentForOnDemandVC") as! PAPaymentForOnDemandVC
                    payment.arrOndemanCarsSelect = arrOnDemandCars
                    payment.isJobRequest = true
                    
                    if let vehicleType = dicVehicleType["vehicle_type_title"] as? String
                    {
                        if let preAuthAmt =  self.dicArrivalTimeOfCarType[vehicleType + "preAuthAmt"] as? String{
                            payment.strPreAuthAmountForJob =  preAuthAmt
                        }
                    }
                    else {
                        payment.strPreAuthAmountForJob = ""
                    }
                    self.navigationController?.pushViewController(payment, animated: true)
                }
            }
        }
        else {
            self.dicArrivalTimeOfCarType[dicVehicleType["vehicle_type_title"]! as! String] = Constants.strNoCars
            self.tableViewCars.setEditing(false, animated: false)
            self.tableViewCars.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a  little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        if segue.identifier! == "toPaymentAlert" {
            
            _ = segue.destinationViewController as! PaymentAlertViewController
            
            
        }
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    func backButtonActionMethod(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
        self.locationManagerObjct.dicDestinationLocation!.removeAll()
        self.locationManagerObjct.destinationLocation = nil
    }
}

// class brackets
// MARK: - =======GMSMapViewDelegate================
extension OnDemandViewController: GMSMapViewDelegate {
    
    func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
        if  self.checkForFocusByDestination {
            self.mapCenterPinImage.hidden = false
            self.map_CentreCoordinate = self.mapView.camera.target
            let newLocation = CLLocation(latitude: (self.map_CentreCoordinate?.latitude)! , longitude: (self.map_CentreCoordinate?.longitude)!)
            if ((self.locationManagerObjct.pickUpLocation!.coordinate.latitude != newLocation.coordinate.latitude) && (self.locationManagerObjct.pickUpLocation!.coordinate.longitude != newLocation.coordinate.longitude)) {
                self.locationManagerObjct.pickUpLocation! = newLocation
                self.MapStopScrollingAndGetAddressOfCurrentLocation(newLocation)
                if self.locationManagerObjct.destinationLocation != nil {
                    self.calulateEstTotalFairBtSourceDestination()
                }
                
            }
        }
    }
    
    func mapView(mapView: GMSMapView!, willMove gesture: Bool) {
        // addressLabel.lock()
        if (gesture)
        {
            self.locationManagerObjct.dicSourceCustomLocation!.removeAll()
            self.markerPickUpAddress!.map = nil
            self.mapCenterPinImage.hidden = false
            self.checkForFocusByDestination = true
            self.tableViewCars.userInteractionEnabled = false
            (self.viewHeaderTableView.viewWithTag(13) as! UIActivityIndicatorView).startAnimating()
            mapView.selectedMarker = nil
        }
    }
    func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
        // mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    func didTapMyLocationButtonForMapView(mapView: GMSMapView!) -> Bool {
        
        if self.markerPickUpAddress!.map == nil {
            self.mapCenterPinImage.hidden = false
        }
        if let myLocation = mapView.myLocation {
            self.locationManagerObjct.pickUpLocation = myLocation
            self.showPickUpAddressOnMap()
            self.MapStopScrollingAndGetAddressOfCurrentLocation(myLocation)
            self.calulateEstTotalFairBtSourceDestination()
            
        }
        return true
    }
    // MARK: - ===get address from map location stops=======
    
    func MapStopScrollingAndGetAddressOfCurrentLocation(newLocation:CLLocation){
        
        //weak var weakSelf = self
        GoogleMapAPIServiceManager.sharedInstance.getLocationAddressUsingLatitude(newLocation.coordinate.latitude, longitude: newLocation.coordinate.longitude, withComplitionHandler: { (dicRecievedJSON) -> () in
            print(dicRecievedJSON)
            if dicRecievedJSON["status"] as! String != "ZERO_RESULTS"{
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { [weak self]() -> Void in
                    self?.refreshMapViewForCheckOnDemandCars()
                    })
                dispatch_async(dispatch_get_main_queue(), { [weak self]() -> Void in
                    self?.btnSourceAddress.setTitle(dicRecievedJSON["results"]![0]["formatted_address"] as? String, forState: .Normal)
                    self?.locationManagerObjct.strCurrentLocation = (dicRecievedJSON["results"]![0]["formatted_address"] as? String)!
                    if self?.btnSourceAddress.titleLabel!.text?.characters.count != 0 {
                    }
                    })
            }
            })
        { (error) -> () in
            print("map error \(error)")
            
        }
    }
    
    
}
