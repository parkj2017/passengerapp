//
//  PAAccoutListingViewController.swift
//  HotelPassengerApp
//
//  Created by Netquall on 16/08/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit


class PAAccoutListingViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, CustomSearchControllerDelegate, passengerListingaActionDelegate {

    @IBOutlet weak var tableAccountListing: UITableView!
    
    var arrDataSource:[[String:Any]] = [[String:Any]]()
     var resultSearchController = UISearchController()
    var customSearchController: CustomSearchController!
    var shouldShowSearchResults = false

    @IBOutlet weak var bookerName: UILabel!
    var pageNumber:Int = 1
    var loading = false
    var totalJobRowsForSearch:Int = 1

    
    @IBOutlet var viewTitle: UIView!
    var refreshControl = UIRefreshControl()

    var filteredTableData = [[String:Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Loading...")
        self.refreshControl.addTarget(self, action: #selector(OngoingJobsViewController.refreshReservation(_:)), for: UIControlEvents.valueChanged)
        self.tableAccountListing.addSubview(refreshControl)

        self.tableAccountListing.sendSubview(toBack: self.refreshControl)
        
        
        let userDetails = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        bookerName.text =  (userDetails["first_name"] as! String).capitalized + " " + (userDetails["last_name"] as! String).capitalized
        
        self.getAccoutListing()
        self.configureCustomSearchController()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true

    }
    
    func configureCustomSearchController() {
        self.customSearchController = CustomSearchController(searchResultsController: self, searchBarFrame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: 45.0), searchBarFont: UIFont(name: "Oxygen", size: 16.0)!, searchBarTextColor: UIColor.gray, searchBarTintColor: UIColor.white)
        self.customSearchController.customSearchBar.placeholder = "Search For..."

        self.tableAccountListing.tableHeaderView = customSearchController.customSearchBar
        customSearchController.customDelegate = self
    }

    
    
    func removeAccoutFromBooker(cellIndex:Int)
    {
        let alert = UIAlertController(title: "Confirmation", message: "Do you want to remove account?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:{ [weak self](ACTION :UIAlertAction!) in
            self?.removeAccoutFromBooker(cellIndex)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func removeAccoutFromBooker(_ cellIndex:Int) {
        
        var dataDict = [String:Any]()
        if shouldShowSearchResults && self.filteredTableData.count > 0{
            dataDict = self.filteredTableData[cellIndex]
        }
        else{
            dataDict = self.arrDataSource[cellIndex]
        }
        if (appDelegate.networkAvialable == true) {
            appDelegate.showProgressWithText("Please wait...")
            var dicParameters : [String:Any]! = Dictionary()
            
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let booker_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).booker_id")
            
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            
            dicParameters["booker_id"] = booker_ID
            dicParameters["session"] = session
            dicParameters["account_no"] = "\(dataDict["account_no"]!)"
            dicParameters["acc_id"] = "\(dataDict["acc_id"]!)"
            
            ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.removeAccount, dicParameters: dicParameters, successBlock:
                { [weak self](dicRecievedJSON) -> Void in
                    appDelegate.hideProgress()
                    
                    if dicRecievedJSON != nil
                    {
                        if dicRecievedJSON["status"] as! String == "success" {
                            if self?.shouldShowSearchResults == true {
                                self?.filteredTableData.remove(at: cellIndex)
                            }
                            else{
                                self?.arrDataSource.remove(at: cellIndex)
                            }
                            self?.tableAccountListing.reloadData()
                        }
                        else {
                            Globals.ToastAlertWithString("No account found")
                            
                        }
                    }
            }){[weak self] (error) -> ()  in
                appDelegate.hideProgress()
                Globals.ToastAlertForNetworkConnection()
                self?.loading = false
            }
            
        }else {
            self.loading = false
            let alert = self.alertView("Sorry", message:"Please check your network connection", delegate:nil, cancelButton:"Ok", otherButons:nil)
            alert?.show()
        }
    }
    func getAccoutListing()
    {
        if (appDelegate.networkAvialable == true) {
            appDelegate.showProgressWithText("Fetching Account Details...")
            var dicParameters : [String:Any]! = Dictionary()
            
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let booker_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).booker_id")

            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            
            dicParameters["booker_id"] = booker_ID
            dicParameters["user_id"] = user_ID
            dicParameters["session"] = session
            dicParameters["page"] = "\(pageNumber)"
            dicParameters["company_id"] = Constants.CompanyID as Any?
            ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.getAccoutListing, dicParameters: dicParameters, successBlock:
                { [weak self](dicRecievedJSON) -> Void in
                    appDelegate.hideProgress()

                    if dicRecievedJSON != nil
                    {
                        var dict_Response:[String:Any]!
                        if dicRecievedJSON["status"] as! String == "success" {
                            dict_Response = dicRecievedJSON.formatDictionaryForNullValues(dicRecievedJSON)
                            if let total_rows = dict_Response["total"] as? Int {
                                if total_rows > 0{
                                    self?.totalJobRowsForSearch = total_rows
                                    if let records = dict_Response["record"] as? [[String:Any]]{
                                    self?.arrDataSource = records
                                     self?.tableAccountListing.reloadData()
                                    }
                                }
                                else {
                                    Globals.ToastAlertWithString("No account found")

                                }
                            }
                        }
                    }
                }){[weak self] (error) -> ()  in
                     appDelegate.hideProgress()
                    let alert = UIAlertController(title: "Connection Error", message: "Do you want to retry?", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!) in
                        
                        if appDelegate.networkAvialable == true
                        {
                            self?.loading = true
                            self?.getAccoutListing()
                        }else
                        {
                        }
                    }))
                    self?.present(alert, animated: true, completion: nil)
                    
                    self?.loading = false
                }
                            
            }else {
                self.loading = false
                let alert = self.alertView("Sorry", message:"Please check your network connection", delegate:nil, cancelButton:"Ok", otherButons:nil)
                alert?.show()
            }
       }
    // MARK: - Refresh control of UITableView
    
    func refreshReservation(_ sender:UIRefreshControl){
        pageNumber = 1
        self.arrDataSource.removeAll()
        self.tableAccountListing.reloadData()
        
        self.refreshControl.endRefreshing()
        getAccoutListing()
    }
    @IBAction func btnLogoutAction(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Confirmation", message: "Do you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.ActiveUserInfo)
            self.navigationController!.popToRootViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
            
        }))
        self.present(alert, animated: true, completion: nil)
        //}
        
    }

    // MARK: - UITableViewDelegates
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        let headerView = self.tableAccountListing.dequeueReusableCell(withIdentifier: "userAccoutHeaderCell")
        headerView?.backgroundColor = UIColor.clear
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
      return 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return self.getHeightForCellAtIndexPath(indexPath)
    }
    
    var selectedAccoutDetails = [String:Any]()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if shouldShowSearchResults{
            appDelegate.activeUserDetailsForBooker = self.filteredTableData[indexPath.row]
        }
        else{
            appDelegate.activeUserDetailsForBooker = self.arrDataSource[indexPath.row]
        }
        
        self.performSegue(withIdentifier: "goToBookerMenu", sender: self)
   
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
         if shouldShowSearchResults{
            return self.filteredTableData.count
         }
         else{
        return self.arrDataSource.count
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
      
        if let cell = self.tableAccountListing.dequeueReusableCell(withIdentifier: "passengerDetailsCell", for: indexPath) as? PassengerListingTableViewCell{
            var dataDict = [String:Any]()
            if shouldShowSearchResults{
                dataDict = self.filteredTableData[indexPath.row]
            }
            else{
             dataDict = self.arrDataSource[indexPath.row]
            }
        let arrData:[[String:Any]] = [["Account#": "\(dataDict["id"]!)"], ["Name": "\(dataDict["first_name"]!)" + " \(dataDict["last_name"]!)"], ["Email": "\(dataDict["email"]!)"], ["Phone": "\(dataDict["mobile"]!)"]]
            cell.setContentForTextView(value: arrData)
            cell.k_listing_type = .K_TYPE_USER_LIST
            cell.AccountListingDelegate = self
            cell.btn_option1.tag = indexPath.row
                
            cell.btn_option2.tag = indexPath.row
            return cell
        }

        return UITableViewCell()
    }
    // MARK: - passengerListingaActionDelegate
    func btnDeleteActionPerformed(_ sender: UIButton){
        self.removeAccoutFromBooker(cellIndex:sender.tag)
    }
    func btnDashboardActionPerformed(_ sender: UIButton){
   
        if shouldShowSearchResults{
            appDelegate.activeUserDetailsForBooker = self.filteredTableData[sender.tag]
        }
        else{
            appDelegate.activeUserDetailsForBooker = self.arrDataSource[sender.tag]
        }

        self.performSegue(withIdentifier: "goToBookerMenu", sender: self)

    }
    func btnViewActionPerformed(_ sender: UIButton){
        
    }
    func btnEmailActionPerformed(_ sender: UIButton){
        
    }

    
    // MARK: - Calculate height for row at index
    
    func getHeightForCellAtIndexPath(_ indexPath:IndexPath) -> CGFloat
    {
        
        let textView = UITextView()
        textView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width - 100, height: 100)
        textView.font = Globals.defaultAppFont(16)
        
        var dataDict = [String:Any]()
        
        if shouldShowSearchResults && self.filteredTableData.count > 0{
            dataDict = self.filteredTableData[indexPath.row]
        }
        else{
            dataDict = self.arrDataSource[indexPath.row]
        }

        
        print(dataDict)
        let arrData:[[String:Any]] = [["Account#": "\(dataDict["id"]!)"], ["Name": "\(dataDict["first_name"]!)" + " \(dataDict["last_name"]!)"], ["Email": "\(dataDict["email"]!)"], ["Phone": "\(dataDict["mobile"]!)"]]

        let str_combine_attibuted: NSMutableAttributedString = NSMutableAttributedString()
        for  (index, val) in arrData.enumerated() {
            
            var key_star: String = ""
            var val_str: String = ""
            var fontColor:UIColor?
            
            if index == 0 {
                key_star = "\(val.keys.first!): ".trimmingCharacters(in:NSCharacterSet.whitespaces)
                val_str = " \(val.values.first!)\n".trimmingCharacters(in:NSCharacterSet.whitespaces)
                fontColor = UIColor.appThemeColor()
            }
            else {
                key_star = "\(val.keys.first!): ".trimmingCharacters(in:NSCharacterSet.whitespaces)
                val_str = " \(val.values.first!)\n".trimmingCharacters(in:NSCharacterSet.whitespaces)
                fontColor = UIColor.darkGray
            }
            
            let title_attri = [NSFontAttributeName: Globals.defaultAppFont(16), NSForegroundColorAttributeName: UIColor.black]
            
            let str_title: NSMutableAttributedString = NSMutableAttributedString(string: key_star, attributes: title_attri)
            str_combine_attibuted.append(str_title)
            
            
            let values_Attri = [NSFontAttributeName: Globals.defaultAppFont(16), NSForegroundColorAttributeName: fontColor!] as [String : Any]
            let str_value: NSMutableAttributedString = NSMutableAttributedString(string: val_str, attributes: values_Attri)
            
            str_combine_attibuted.append(str_value)
        }
        
        textView.attributedText = str_combine_attibuted
        print(str_combine_attibuted)
        return textView.contentSize.height
    }
    

    // MARK: - CustomSearchControllerDelegate functions
    func didStartSearching() {
        shouldShowSearchResults = true
        self.tableAccountListing.reloadData()
    }
    func didTapOnSearchButton() {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            self.tableAccountListing.reloadData()
        }
    }
    func didTapOnCancelButton() {
        shouldShowSearchResults = false
        self.tableAccountListing.reloadData()
    }
    func didChangeSearchText(_ searchText: String) {
        filteredTableData.removeAll(keepingCapacity: false)
        
        var searchPredicate = NSPredicate()
       if let arr_Comp = searchText.components(separatedBy: " ") as? [String]
       {
        if arr_Comp.count <= 1{
            searchPredicate = NSPredicate(format: "first_name CONTAINS[c] %@ OR last_name CONTAINS[c] %@",searchText, searchText)
        }
        else {
            searchPredicate = NSPredicate(format: "first_name CONTAINS[c] %@ OR last_name CONTAINS[c] %@",arr_Comp[0], arr_Comp[1])

        }
        }
        self.filteredTableData =  (self.arrDataSource as NSArray).filtered(using: searchPredicate) as! [[String : Any]]
        self.tableAccountListing.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK:- Pagination of table view rows
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.shouldShowSearchResults == false {
            if !self.loading {
                let endScrolling:CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height
                if endScrolling >= scrollView.contentSize.height {
                    self.loading = true
                    self.loadDataDelayed()
                }
            }
        }
    }
    
    func loadDataDelayed() {
        if self.shouldShowSearchResults == false {
            if totalJobRowsForSearch > arrDataSource.count {
                let page: Double = ceil(Double(arrDataSource.count)/8.0)
                pageNumber = Int(page) + 1
                getAccoutListing()
            }
        }
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goToBookerMenu"{
            
            let des_VC = segue.destination as? PABookerMainMenuViewController
        }
    }
   

}
