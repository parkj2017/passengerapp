//
//  PAActiveJobViewController.swift
//  PassangerApp
//
//  Created by Netquall on 1/15/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader
import GoogleMaps
import MessageUI
import MapKit


class PAOngoingJobDetails: BaseViewController, BackButtonActionProtocol, RemoteNotificationTapProtocol, JobActionProtocol, UIAlertViewDelegate, MFMessageComposeViewControllerDelegate {
    
    // MARK:- ========Property Decalartion -=============
    @IBOutlet weak var mainRequestView: UIView!
    var etaLblShowTime : UILabel!
    var finalImage : UIImage!
    var image_MapPinCar : NSData!
    var timerDriverLoc : NSTimer!
    var timerRequestToDriver : NSTimer!

    @IBOutlet weak var lblPickeUpAddress: UIButton!
    @IBOutlet weak var mapViewRequest: GMSMapView!
    
  //  var carRequestRadius:carRequestSteps = .kRadiusNull
    
    @IBOutlet weak var viewPickupDropOff: UIView!
    let pickupDropOffMarker = GMSMarker()
    
    var carMarker = GMSMarker()
    var carMarkerImage:UIImage!
    var isJobRequest : Bool!
    var jobAcceptedbyDriver : Bool! = false
    var strJobStatus:String!
    var activeDriver : DriverDetails! = DriverDetails()
    var dicOngoingJob : [String:AnyObject]!
    var locationPickOrDrop : CLLocation!
    var activeButton : UIButton!
    var arrOndemanCarsSelected : [[String:AnyObject]]!
    lazy var dicArrivalTimeOfVehicleType = [String:AnyObject]()
    var CardID : String!
    var mapBounds : GMSCoordinateBounds!
    var dicRequestCarParam : [String:AnyObject]!
    var driver_Last_Location: CLLocationCoordinate2D!
    var driver_Current_Location: CLLocationCoordinate2D!
    lazy var locationManagerObjct = LocationManager.sharedInstance
    var reservaiton_ID:String! = ""
    
    @IBOutlet weak var constraintTopViewPickup: NSLayoutConstraint!
    @IBOutlet weak var lblDropOffAddress: UIButton!
    @IBOutlet weak var constraintBottomViewPickUp: NSLayoutConstraint!
    // MARK:- ========view Life Cycyle  -=============
    override func viewDidLoad() {
        super.viewDidLoad()
        //  self.navigationController?.navigationBar.items =
        self.lblDropOffAddress.titleLabel?.numberOfLines = 2;
        driver_Current_Location = CLLocationCoordinate2DMake(0.0, 0.0)
        
        if self.etaLblShowTime == nil{
            self.etaLblShowTime = UILabel(frame: CGRectMake(33,20,40,30))
            self.etaLblShowTime.textAlignment = .Center
            self.etaLblShowTime.numberOfLines = 2
            // self.etaLblShowTime.backgroundColor = UIColor.brownColor()
            self.etaLblShowTime.textColor = UIColor.whiteColor()
            self.etaLblShowTime.font = UIFont(name: "Oxygen", size: 10)
        }
        
        if self.isJobRequest == true {
        //    print("----------------> \(self.arrOndemanCarsSelected.count)\n-------------------->\(self.arrOndemanCarsSelected)")
            
            appDelegate.requestedDriverIDs.removeAll()
            Globals.sharedInstance.removeValuetoUserDefaultsWithKey(Constants.allRequestedDrivers)
            
            self.setupViewForJobRequest()
        }else{
            self.setUpViewForJobStatusWith(self.dicOngoingJob["jobStatus"] as! String)
            self.getDriverLocation()
            self.refreshDriverLocation()
            
        }
        // Do any   additional setup after loading the view.
        
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
        appDelegate.delegateJobStatusAction = self
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        appDelegate.delegateJobStatusAction = nil
        Globals.sharedInstance.delegateBack = nil
        
    }
    func timerStartForRequestToDriver(){
        print("time Start")
        if self.timerRequestToDriver == nil{
            self.timerRequestToDriver =  NSTimer.scheduledTimerWithTimeInterval(10.0, target:self, selector: "resendRequestToAnotherDriverAfterTimeInterval", userInfo:nil , repeats: true )
        }
        //NSRunLoop.currentRunLoop().addTimer(self.timerDriverLoc, forMode: NSRunLoopCommonModes)
    }
    func stopTimerForRequestToDriver(){
        if self.timerRequestToDriver != nil{
            print("time stop methds")
            self.timerRequestToDriver.invalidate()
            self.timerRequestToDriver = nil  // new changes
            print("time stop caleled")
        }
    }
    // new changes for 10 sec time requets ...
    func resendRequestToAnotherDriverAfterTimeInterval(){
        print("resendRequestToAnotherDriverAfterTimeInterval")
        
        if self.jobAcceptedbyDriver == true || self.jobAcceptedbyDriver == nil   {
            return
        }
        self.jobAcceptedbyDriver = false
        
        if self.arrOndemanCarsSelected.count != 0 {
            self.isJobRequest = true
            self.makeRequestToBookACarWithCarDetails1()
            }else {
            self.stopTimerForRequestToDriver()
            self.backToOnDemandViewController()
        }
        
    }
    func backToOnDemandViewController(){
        dispatch_async(dispatch_get_main_queue()) { [weak self]() -> Void in
            appDelegate.hideProgress()

            appDelegate.delegateJobStatusAction = nil
            self?.navigationController?.popViewControllerAnimated(true)

//            if self == nil {
//                return
//            }
//            let activeView = self?.navigationController?.topViewController
//            if activeView == nil {
//                return
//            }
//            if activeView!.isKindOfClass(PAActiveJobViewController) {
//                for vc: UIViewController in (self?.navigationController?.viewControllers)! {
//                    if (vc.isKindOfClass(PAMainMenuViewController)) {
//                        appDelegate.delegateJobStatusAction = nil
//                        self?.navigationController?.popToViewController(vc, animated: true)
//                    }
//                }
//            }
        }
    }

    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        // self.stoprefreshDriverLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK:- =====show View Acording Demand =====
    func setupViewForJobRequest()
    {
           self.lblPickeUpAddress.setTitle( self.locationManagerObjct.strCurrentLocation, forState: .Normal)
      
        if let strAddress = locationManagerObjct.dicDestinationLocation!["address"] as? String {
            self.lblDropOffAddress.setTitle( strAddress, forState: .Normal)
        }else {
            self.lblDropOffAddress.setTitle( "N/A", forState: .Normal)
        }
        
        self.mapViewRequest.settings.zoomGestures = true
        if  let carObject = self.arrOndemanCarsSelected.first {
            let latitude: Double = CDouble(carObject["latitude"]! as! String)!
            let longitude: Double = CDouble(carObject["longitude"]! as! String)!
            let postion = CLLocationCoordinate2DMake(latitude, longitude)
            self.driver_Current_Location = postion
            self.activeDriver = DriverDetails()
            self.activeDriver.driver_ID = carObject["driver_id"]! as! String
            
            if(carObject["map_icon"] != nil ){
                DLImageLoader.sharedInstance.imageFromUrl((carObject["map_icon"]! as! String) , completed: { [weak self](error, image) -> () in
                    if self == nil {
                        return
                    }
                    if image != nil {
                        let size = CGSize(width: 30 , height: 30)
                        UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
                        image.drawInRect(CGRectMake(0, 0, size.width, size.height))
                        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
                        UIGraphicsEndImageContext()
                        self?.carMarkerImage = newImage
                        self?.carMarker.icon = self?.carMarkerImage
                        dispatch_async(dispatch_get_main_queue(), { [weak self]() -> Void in
                            self?.carMarker.map = self?.mapViewRequest
                            })
                    }else {
                        self?.carMarkerImage = UIImage(named: "sedan")
                    }
                    })
            }
            self.carMarkerImage = UIImage(named: "sedan")
            self.showDriverLocationOnMap(postion,withVehicalTitle: carObject["map_icon"]! as! String)
            
        }
        if let _ = locationManagerObjct.pickUpLocation {
            
            let latitude: Double = CDouble(locationManagerObjct.pickUpLocation!.coordinate.latitude)
            let longitude: Double = CDouble(locationManagerObjct.pickUpLocation!.coordinate.longitude)
            let postion = CLLocationCoordinate2DMake(latitude, longitude)
            let carObject = self.arrOndemanCarsSelected.first
            self.showPickUpLocationOnMap(postion, withVehicalTitle: carObject!["vehicle_type_title"]! as! String)
        }
        self.mapBounds = GMSCoordinateBounds(coordinate: self.locationManagerObjct.pickUpLocation!.coordinate, coordinate: self.locationManagerObjct.pickUpLocation!.coordinate)
        
        if let objectCar = self.arrOndemanCarsSelected.first {
            let carLoc : CLLocation = CLLocation(latitude:CDouble(objectCar["latitude"]! as! String)!
                ,longitude: CDouble(objectCar["longitude"]! as! String)!)
            let carBounds = GMSCoordinateBounds(coordinate: carLoc.coordinate, coordinate: carLoc.coordinate)
            self.mapBounds = self.mapBounds.includingBounds(carBounds)
        }
        if self.isJobRequest == true {
            self.showBottomViewsWithJobStatus(false, showCancel: true)
            self.view.sendSubviewToBack(self.viewPickupDropOff)
            self.jobAcceptedbyDriver = false
            self.timerStartForRequestToDriver()
            self.makeRequestToBookACarWithCarDetails1()
            
        }else {
            self.showBottomViewsWithJobStatus(true, showCancel: true)
           }
        
        if self.mapBounds != nil{
            dispatch_async(dispatch_get_main_queue()) {[weak self] () -> Void in
                CATransaction.begin()
                CATransaction.setAnimationDuration(1.0)
                self?.mapViewRequest.animateToCameraPosition((self?.mapViewRequest.cameraForBounds((self?.mapBounds)!, insets: UIEdgeInsetsMake(100.0, 50.0, 50, 50.0)))!)
                CATransaction.commit()
            }
        }
    }
    
    func setUpViewForJobStatusWith(JobStatus:String)
    {
        self.strJobStatus = JobStatus
        self.jobAcceptedbyDriver = true
        if let reservation_id = self.dicOngoingJob["reservation_id"] as? String
        {
            let driverObject = DriverDetails()
            driverObject.driver_ID = self.dicOngoingJob["driver_id"] as! String
            driverObject.reservation_id = reservation_id
            if let booking_id = self.dicOngoingJob["conf_id"] as? String
            {
                driverObject.booking_Id = booking_id.stringByReplacingOccurrencesOfString("-", withString: "")
            }
            self.activeDriver = driverObject
        }
        
        var  pickLocation : CLLocation!
        var postionPickup : CLLocationCoordinate2D!
        let StrVehicle = (self.dicOngoingJob["vehicle_type"])! as! String
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(JobStatus, imageName: "backArrow")
        
        print("dic Ongoing ==\( self.dicOngoingJob["latitude"] )  --- > \(self.dicOngoingJob)")
        var  driverLocation : CLLocation!
        if self.dicOngoingJob["latitude"] as! String != "" {
            driverLocation = CLLocation(latitude: (self.dicOngoingJob["latitude"] as! String).toDouble()!  , longitude: (self.dicOngoingJob["longitude"] as! String).toDouble()!)
            let driverLoc = CLLocationCoordinate2DMake((self.dicOngoingJob["latitude"] as! String).toDouble()!, (self.dicOngoingJob["longitude"] as! String).toDouble()!)
            if(self.dicOngoingJob["map_icon"] != nil ){
                DLImageLoader.sharedInstance.imageFromUrl((self.dicOngoingJob["map_icon"]! as? String)! , completed: { [weak self](error, image) -> () in
                    if self == nil {
                        return
                    }
                    if image != nil {
                        let size = CGSize(width: 30 , height: 30)
                        UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
                        image.drawInRect(CGRectMake(0, 0, size.width, size.height))
                        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
                        UIGraphicsEndImageContext()
                        self?.carMarkerImage = newImage
                        self?.carMarker.icon = self?.carMarkerImage
                        dispatch_async(dispatch_get_main_queue(), { [weak self]() -> Void in
                            self?.carMarker.map = self?.mapViewRequest
                            })
                    }else {
                        self?.carMarkerImage = UIImage(named: "sedan")
                    }
                    })
            }
            self.carMarkerImage = UIImage(named: "sedan")
            self.showDriverLocationOnMap(driverLoc, withVehicalTitle:(self.dicOngoingJob["map_icon"] as? String)!)
            self.driver_Current_Location = driverLoc
            // self.driver_Last_Location = driverLoc
        }
        if JobStatus.caseInsensitiveCompare("En Route") == NSComparisonResult.OrderedSame || JobStatus.caseInsensitiveCompare("On Location") == NSComparisonResult.OrderedSame || JobStatus.caseInsensitiveCompare("Circling") == NSComparisonResult.OrderedSame {
            if self.dicOngoingJob["pickup_latitude"] as! String != "" && self.dicOngoingJob["pickup_longitude"] as! String != ""
            {
                pickLocation = CLLocation(latitude:(self.dicOngoingJob["pickup_latitude"] as! String ).toDouble()! , longitude: (self.dicOngoingJob["pickup_longitude"] as! String).toDouble()!)
                postionPickup = CLLocationCoordinate2DMake(pickLocation.coordinate.latitude,pickLocation.coordinate.longitude)
            }else {
                pickLocation = self.getAddressLatLongiFromAddressString((self.dicOngoingJob["pickup"] as! String), isPickup: true)
                postionPickup = CLLocationCoordinate2DMake(pickLocation.coordinate.latitude,pickLocation.coordinate.longitude)
            }
            if JobStatus.caseInsensitiveCompare("En Route") == NSComparisonResult.OrderedSame {
                self.showBottomViewsWithJobStatus(true, showCancel: true)
                //self.getDistanceFromPickUpToDropOff(pickLocation, DropOffLoc: driverLocation,withJobStatus: JobStatus)
            }else {
                self.showBottomViewsWithJobStatus(true, showCancel: false)
                self.dicArrivalTimeOfVehicleType[StrVehicle] = "OL"
                self.locationPickOrDrop = pickLocation
                self.setupMapBoundsWithLocation(pickLocation)
               self.showPickUpLocationOnMap(postionPickup, withVehicalTitle:StrVehicle)
            }
        }
        else {
            if self.dicOngoingJob["dropoff"] as! String != "" {
                if self.dicOngoingJob["dropoff_latitude"] as! String != "" && self.dicOngoingJob["dropoff_longitude"] as! String != ""{
                    let dropLocation = CLLocation(latitude:(self.dicOngoingJob["dropoff_latitude"] as! String).toDouble()! , longitude: (self.dicOngoingJob["dropoff_longitude"] as! String).toDouble()!)
                    postionPickup = CLLocationCoordinate2DMake(dropLocation.coordinate.latitude,dropLocation.coordinate.longitude)
                    
                   // self.getDistanceFromPickUpToDropOff(driverLocation, DropOffLoc: dropLocation, withJobStatus: JobStatus)
                    self.locationPickOrDrop = dropLocation
                }else {
                    pickLocation = self.getAddressLatLongiFromAddressString((self.dicOngoingJob["dropoff"] as! String), isPickup: false )
                    postionPickup = CLLocationCoordinate2DMake(pickLocation.coordinate.latitude,pickLocation.coordinate.longitude)
                 //   self.getDistanceFromPickUpToDropOff(pickLocation, DropOffLoc: driverLocation,withJobStatus: JobStatus)
                }
            } else {
                if self.dicOngoingJob["pickup_latitude"] as! String != "" && self.dicOngoingJob["pickup_longitude"] as! String != ""
                {
                    postionPickup = CLLocationCoordinate2DMake((self.dicOngoingJob["pickup_latitude"] as! String ).toDouble()!,(self.dicOngoingJob["pickup_longitude"] as! String).toDouble()!)
                    
                }
                self.dicArrivalTimeOfVehicleType[StrVehicle] = "OL"
                self.showPickUpLocationOnMap(postionPickup, withVehicalTitle: StrVehicle)
            }
            self.locationPickOrDrop = CLLocation(latitude:postionPickup.latitude , longitude: postionPickup.longitude)
            self.setupMapBoundsWithLocation(self.locationPickOrDrop)
            self.showBottomViewsWithJobStatus(true, showCancel: false)
        }
        self.lblPickeUpAddress.setTitle(self.dicOngoingJob["pickup"] as? String, forState: .Normal)
        self.lblDropOffAddress.setTitle( (self.dicOngoingJob["dropoff"] as! String), forState: .Normal)
        
        self.mapViewRequest.settings.zoomGestures = true
        
    }
    // MARK: ====Get Dsitance From =====
    func getDistanceFromPickUpToDropOff(PickUpLocation:CLLocation?, DropOffLoc: CLLocation?, withJobStatus:String)
    {
        
        self.locationPickOrDrop = PickUpLocation
        let StrVehicle = (self.dicOngoingJob["vehicle_type"])! as! String
        if PickUpLocation != nil && DropOffLoc != nil {
            dispatch_async(dispatch_get_main_queue()) { [weak self]() -> Void in
                appDelegate.showProgressWithText("Please Wait...")
                let urlStr = Globals.sharedInstance.getFullApiAddressForDistanceToDriverDistance(PickUpLocation!, destinationLoc: DropOffLoc!)
                
                if urlStr == "" {
                    return
                }
                print(urlStr)
                ServiceManager.sharedInstance.dataTaskWithGetRequest(urlStr, successBlock: { (dicData) -> Void in
                    //print("Time Arrival ---\(dicData)")
                    let arrRoutes = dicData["routes"] as! [[String : AnyObject]]
                    if arrRoutes.count > 0 {
                        
                        let strTime = String().getNearCarsArrivalTimeInfo(dicData)
                        self?.dicArrivalTimeOfVehicleType[StrVehicle] = strTime
                        
                        if withJobStatus.caseInsensitiveCompare("En Route") == NSComparisonResult.OrderedSame || withJobStatus.caseInsensitiveCompare("On Location") == NSComparisonResult.OrderedSame || withJobStatus.caseInsensitiveCompare("Circling") == NSComparisonResult.OrderedSame {
                            let postion = CLLocationCoordinate2DMake((PickUpLocation?.coordinate.latitude)!, (PickUpLocation?.coordinate.longitude)!)
                            self?.showPickUpLocationOnMap(postion, withVehicalTitle:(self?.dicOngoingJob["vehicle_type"])! as! String )
                            self?.setupMapBoundsWithLocation(PickUpLocation!)
                        }
                        else {
                            let postion = CLLocationCoordinate2DMake((PickUpLocation?.coordinate.latitude)!, (PickUpLocation?.coordinate.longitude)!)
                            self?.showPickUpLocationOnMap(postion, withVehicalTitle: (self?.dicOngoingJob["vehicle_type"])! as! String)
                            self?.setupMapBoundsWithLocation(PickUpLocation!)
                        }
                        appDelegate.hideProgress()
                    }else {
                        appDelegate.hideProgress()
                        self?.dicArrivalTimeOfVehicleType[StrVehicle] = "loading"
                        self?.showPickUpLocationOnMap(CLLocationCoordinate2D(latitude: (self?.locationPickOrDrop.coordinate.latitude)!, longitude:  (self?.locationPickOrDrop.coordinate.longitude)!), withVehicalTitle: StrVehicle)
                        //  Globals.ToastAlertWithString("Network Error--")
                    }
                    }, failureBlock: { (error) -> () in
                        appDelegate.hideProgress()
                        // Globals.ToastAlertWithString("Network Error--")
                        print("Error \(error.description)")
                })
                
            }
        }else {
            let StrVehicle = (self.dicOngoingJob["vehicle_type"])! as! String
            self.dicArrivalTimeOfVehicleType[StrVehicle] = "Loading"
            let postion = CLLocationCoordinate2DMake((PickUpLocation?.coordinate.latitude)!, (PickUpLocation?.coordinate.longitude)!)
            self.showPickUpLocationOnMap(postion, withVehicalTitle:StrVehicle)
            self.setupMapBoundsWithLocation(PickUpLocation!)
        }
    }
    // MARK:- =====Show Eta And With Location  =====
    func showPickUpLocationOnMap(location:CLLocationCoordinate2D, withVehicalTitle:String){
        
        pickupDropOffMarker.position = location
        if self.isJobRequest == true {
            self.showEtaMethodsOnMapWith(self.dicArrivalTimeOfVehicleType[withVehicalTitle] as! String, imageString: "blue.png")
            pickupDropOffMarker.icon = self.finalImage
        }else {
            if (self.dicOngoingJob["jobStatus"] as! String).caseInsensitiveCompare("En Route") == NSComparisonResult.OrderedSame{
                self.showEtaMethodsOnMapWith(self.dicArrivalTimeOfVehicleType[withVehicalTitle] as! String, imageString: "blue.png")
                pickupDropOffMarker.icon = self.finalImage
            }else {
                self.showEtaMethodsOnMapWith(self.dicArrivalTimeOfVehicleType[withVehicalTitle] as! String, imageString: "green.png")
                pickupDropOffMarker.icon = self.finalImage
            }
        }
        pickupDropOffMarker.userData = withVehicalTitle
        pickupDropOffMarker.map = self.mapViewRequest
        
    }
    func setupMapBoundsWithLocation(location:CLLocation?){
        
        if let _ = location {
            self.mapBounds = GMSCoordinateBounds(coordinate: location!.coordinate, coordinate: location!.coordinate)
        }
        
        if let _ = self.driver_Current_Location {
            let driveloc = CLLocation(latitude:self.driver_Current_Location!.latitude , longitude: self.driver_Current_Location!.longitude)
            let carBounds = GMSCoordinateBounds(coordinate: driveloc.coordinate, coordinate: driveloc.coordinate)
            if let _ = self.mapBounds {
                self.mapBounds = self.mapBounds.includingBounds(carBounds)
            }else {
                self.mapBounds = GMSCoordinateBounds(coordinate: driveloc.coordinate, coordinate: driveloc.coordinate)
            }
        }
        dispatch_async(dispatch_get_main_queue()) {[weak self] () -> Void in
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)
            self?.mapViewRequest.animateToCameraPosition((self?.mapViewRequest.cameraForBounds((self?.mapBounds)!, insets: UIEdgeInsetsMake(100.0, 50.0, 50, 50.0)))!)
            CATransaction.commit()
        }
        
    }
    // MARK:- =====Show Driver LOcation on Map=====
    func showDriverLocationOnMap (location:CLLocationCoordinate2D, withVehicalTitle:String){
        
        carMarker.position = location
        self.carMarker.icon = carMarkerImage
        dispatch_async(dispatch_get_main_queue(), { [weak self]() -> Void in
            self?.carMarker.map = self?.mapViewRequest
            })
        
    }
    func showEtaMethodsOnMapWith(etaString:String, imageString:String){
        
        let viewBack = UIView(frame: CGRectMake(0,0,100,100))
        viewBack.backgroundColor = UIColor.clearColor()
        let pinImageView : UIImageView = UIImageView(image: UIImage(named: imageString))
        self.etaLblShowTime = UILabel(frame: CGRectMake(33,20,40,30))
        self.etaLblShowTime.textAlignment = .Center
        self.etaLblShowTime.numberOfLines = 2
        self.etaLblShowTime.text = etaString
        // self.etaLblShowTime.backgroundColor = UIColor.brownColor()
        self.etaLblShowTime.textColor = UIColor.whiteColor()
        self.etaLblShowTime.font = UIFont(name: "Oxygen", size: 10)
        viewBack.addSubview(pinImageView)
        viewBack.addSubview(self.etaLblShowTime)
        // viewBack.bringSubviewToFront(self.etaLblShowTime)
        if UIScreen.mainScreen().respondsToSelector("scale") {
            UIGraphicsBeginImageContextWithOptions(viewBack.frame.size, false, UIScreen.mainScreen().scale)
        }else {
            UIGraphicsBeginImageContext(viewBack.frame.size)
        }
        viewBack.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        self.finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        // return self.finalImage
    }
    // MARK:- =====web Request Send to Book A Car=====
    
  
    func makeRequestToBookACarWithCarDetails1()
    {
        appDelegate.showProgressWithText("Please Wait...")
//
//        if carRequestRadius == .kRadiusNull{
//            carRequestRadius = .kRadiusOneThird
//        }
//        var driverIDs:[String] = [String]()
//
//        if carRequestRadius == .kRadiusOneThird && driverIDs.count  == 0{
//            driverIDs = self.driverIDsForRequest(.kRadiusOneThird)
//            carRequestRadius = .kRadiusHalf
//        }
//        if carRequestRadius == .kRadiusHalf && driverIDs.count  == 0{
//            driverIDs = self.driverIDsForRequest(.kRadiusHalf)
//            carRequestRadius = .kRadiusToAll
//        }
//        if carRequestRadius == .kRadiusToAll && driverIDs.count  == 0{
//            driverIDs = self.driverIDsForRequest(.kRadiusToAll)
//            carRequestRadius = .kRadiusNull
//             self.stopTimerForRequestToDriver()
//           // JLToast.makeText("Request sent to all driver(s). Waiting for driver(s) to accept your job.", duration: 4.0).show()
//        }

        if self.jobAcceptedbyDriver == true || self.jobAcceptedbyDriver == nil   {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                appDelegate.hideProgress()
                self.stopTimerForRequestToDriver()
                })
            return
        }
//        if driverIDs.count == 0{
//            appDelegate.hideProgress()
//            return;
//        }

        if appDelegate.networkAvialable == false   {
            appDelegate.hideProgress()
            return
        }
        
        var dicParameters = [String:AnyObject]()
        dicParameters["user_id"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
        dicParameters["session"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
        let arrCurentDate = Globals.sharedInstance.getCurrentDate().componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        dicParameters["pu_date"] = arrCurentDate[0]
        dicParameters["pu_time"] = arrCurentDate[1]
        
        dicParameters["vechile_type"] = ""//carSelected["vehicle_type"]
        
        dicParameters["pickup_location"] = self.locationManagerObjct.strCurrentLocation
        dicParameters["pickup_latitude"] = String(locationManagerObjct.pickUpLocation!.coordinate.latitude)
        dicParameters["pickup_longitude"] = String(locationManagerObjct.pickUpLocation!.coordinate.longitude)
        if let _ = locationManagerObjct.dicDestinationLocation {
            if let _ = locationManagerObjct.dicDestinationLocation!["address"] as? String {
                dicParameters["dropoff_location"] = locationManagerObjct.dicDestinationLocation!["address"] as! String
                dicParameters["dropoff_latitude"] = String(locationManagerObjct.destinationLocation!.coordinate.latitude)
                dicParameters["dropoff_longitude"] = String(locationManagerObjct.destinationLocation!.coordinate.longitude)
            }else {
                dicParameters["dropoff_location"] = ""
                dicParameters["dropoff_latitude"] = ""
                dicParameters["dropoff_longitude"] = ""
            }
        }else
        {
            dicParameters["dropoff_location"] = ""
            dicParameters["dropoff_latitude"] = ""
            dicParameters["dropoff_longitude"] = ""
            
        }
        dicParameters["distance_type"] = appDelegate.distanceType //changes by prakash
        dicParameters["fleet_id"] = "0" //this will be override by diver id who will accept job
        
       // dicParameters["driver_id"] = driverIDs
        dicParameters["reservation_id"] = reservaiton_ID
        
        dicParameters["company_id"] = Constants.CompanyID
        dicParameters["distance_type"] = appDelegate.distanceType //changes by prakash
        if let arrCard = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveCardInfo).records") as? [[String:AnyObject]] {
            for dic in arrCard {
                let name = dic["payment_method"] as! Int
                if name == 11 {
                    dicParameters["credit_card_id"] = dic["id"]
                    break
                }
            }
        }
        
        dicParameters["payment_method"] = "11"
        dicParameters["passanger_name"] = self.dicRequestCarParam["passangerName"]
        dicParameters["room_no"] = self.dicRequestCarParam["roomNumber"]
        
        
        print(dicParameters)
        weak var weakSelf = self
        ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.BookACar, dicParameters: dicParameters, successBlock: { [weak self](dicData) -> Void in
            
          ///  JLToast.makeText("Driver Notified. Waiting for driver to accept job...", duration: 3.0).show()
            
            print("dic car Request----\(dicData)")
            appDelegate.hideProgress()
            if weakSelf == nil || self == nil{
                return
            }
            if dicData["status"] as! String == "success" {
                
                self?.activeDriver = DriverDetails()
                self?.activeDriver.reservation_id = String(dicData["reservation_id"]!)
                self?.activeDriver.booking_Id = String(dicData["booking_id"]!)
                //self?.activeDriver.driver_ID = String(carSelected["driver_id"]!)print
                self?.reservaiton_ID = String(dicData["reservation_id"]!)
                
                if self?.arrOndemanCarsSelected.count != 0 {
                    weakSelf?.arrOndemanCarsSelected.removeAtIndex(0)
                    self?.timerStartForRequestToDriver()
                    // new changes
                    dispatch_async(dispatch_get_main_queue(), { [weak self]() -> Void in
                       // self?.etaMapIconForStaus("Requesting")
                        self?.pickupDropOffMarker.position = (self?.locationManagerObjct.pickUpLocation!.coordinate)!
                        if self!.finalImage != nil {
                            self?.pickupDropOffMarker.icon = self!.finalImage
                        }
                        self?.pickupDropOffMarker.map = self?.mapViewRequest
                        })
                    //weakSelf?.getDriverLocation()
                }
            }else if dicData["status"] as! String == "session_expired" {
                appDelegate.hideProgress()
                JLToast.makeText( dicData["message"] as! String, duration: 3.0).show();
                Globals.sharedInstance.activeDriver = nil
                self?.stoprefreshDriverLocation()
                self?.backToOnDemandViewController()
            }else {
                appDelegate.hideProgress()
                JLToast.makeText( dicData["message"] as! String, duration: 3.0).show();
                self?.stoprefreshDriverLocation()
                self?.backToOnDemandViewController()
            }
            }, failureBlock: { [weak self](error) -> () in
                appDelegate.hideProgress()
//                 if self?.arrOndemanCarsSelected.first != nil {
//                    self?.makeRequestToBookACarWithCarDetails1()
//                }
            })
    }
    
    
    func makeRequestToBookACarWithCarDetails(carSelected:[String:AnyObject])
    {
        
        if carSelected["driver_id"]  != nil && carSelected["fleet_id"] != nil
        {
            
            appDelegate.showProgressWithText("Please Wait...")
            var dicParameters = [String:AnyObject]()
            dicParameters["user_id"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            dicParameters["session"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            let arrCurentDate = Globals.sharedInstance.getCurrentDate().componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            dicParameters["pu_date"] = arrCurentDate[0]
            dicParameters["pu_time"] = arrCurentDate[1]
            dicParameters["vechile_type"] = carSelected["vehicle_type"]
            
            dicParameters["pickup_location"] = self.locationManagerObjct.strCurrentLocation
            dicParameters["pickup_latitude"] = String(locationManagerObjct.pickUpLocation!.coordinate.latitude)
            dicParameters["pickup_longitude"] = String(locationManagerObjct.pickUpLocation!.coordinate.longitude)
            if let _ = locationManagerObjct.dicDestinationLocation {
                if let _ = locationManagerObjct.dicDestinationLocation!["address"] as? String {
                    dicParameters["dropoff_location"] = locationManagerObjct.dicDestinationLocation!["address"] as! String
                    dicParameters["dropoff_latitude"] = String(locationManagerObjct.destinationLocation!.coordinate.latitude)
                    dicParameters["dropoff_longitude"] = String(locationManagerObjct.destinationLocation!.coordinate.longitude)
                }else {
                    dicParameters["dropoff_location"] = ""
                    dicParameters["dropoff_latitude"] = ""
                    dicParameters["dropoff_longitude"] = ""
                }
            }else
            {
                dicParameters["dropoff_location"] = ""
                dicParameters["dropoff_latitude"] = ""
                dicParameters["dropoff_longitude"] = ""
                
            }
            
            dicParameters["distance_type"] = appDelegate.distanceType //changes by prakash
            dicParameters["fleet_id"] = carSelected["fleet_id"]!
            dicParameters["driver_id"] = carSelected["driver_id"]! as! String
            if let arrCard = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveCardInfo).records") as? [[String:AnyObject]] {
                for dic in arrCard {
                    let name = dic["payment_method"] as! Int
                    if name == 11 {
                        dicParameters["credit_card_id"] = dic["id"]
                        break
                    }
                }
            }
            
            dicParameters["payment_method"] = "11"
            dicParameters["passanger_name"] = self.dicRequestCarParam["passangerName"]
            dicParameters["room_no"] = self.dicRequestCarParam["roomNumber"]
            dicParameters["company_id"] = Constants.CompanyID
            
            print(dicParameters)
            weak var weakSelf = self
            ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.BookACar, dicParameters: dicParameters, successBlock: { [weak self](dicData) -> Void in
               // JLToast.makeText("Driver Notified. Waiting for driver to accept job...", duration: 3.0).show()
                
                print("dic car Request----\(dicData)")
                appDelegate.hideProgress()
                if dicData["status"] as! String == "success" {
                    
                    self?.activeDriver = DriverDetails()
                    self?.activeDriver.reservation_id = String(dicData["reservation_id"]!)
                    self?.activeDriver.booking_Id = String(dicData["booking_id"]!)
                    self?.activeDriver.driver_ID = String(carSelected["driver_id"]!)
                    if self?.arrOndemanCarsSelected.count != 0 {
                        self?.arrOndemanCarsSelected.removeAtIndex(0)
                    }
                }else if dicData["status"] as! String == "session_expired" {
                    appDelegate.hideProgress()
                    JLToast.makeText( dicData["message"] as! String, duration: 3.0).show();
                    Globals.sharedInstance.activeDriver = nil
                    self?.stoprefreshDriverLocation()
                    self?.backToOnDemandViewController()
                }else {
                    appDelegate.hideProgress()
                    JLToast.makeText( dicData["message"] as! String, duration: 3.0).show();
                    self?.stoprefreshDriverLocation()
                    self?.backToOnDemandViewController()
                }
                }, failureBlock: {[weak self] (error) -> () in
                    appDelegate.hideProgress()
                    print("Network Error ---->  dic car Request----\(error)")
                     if self?.arrOndemanCarsSelected.first != nil {
                        self?.makeRequestToBookACarWithCarDetails1()
                    }
                })
        }
    }
    
    
    
    //getting all driver id based on defined radius at portal
    func driverIDsForRequest(carRadius:carRequestSteps) -> [String] {
        print(appDelegate.ondemandRadius);
        
        var mindistance:Double = 0.0
        var maxDistance:Double = 0.0
        var arrDriverIDs:[String]! = [String]()
        if carRadius == .kRadiusOneThird{
            mindistance = 0.0
            maxDistance = appDelegate.ondemandRadius.toDouble()!/3
        }
        else if  carRadius == .kRadiusHalf{
            mindistance = appDelegate.ondemandRadius.toDouble()!/2
            maxDistance = appDelegate.ondemandRadius.toDouble()!/2
        }
        else if  carRadius == .kRadiusToAll{
            mindistance = appDelegate.ondemandRadius.toDouble()!/2
            maxDistance = appDelegate.ondemandRadius.toDouble()!
        }
        
        let filteredResults:[[String:AnyObject]]! = arrOndemanCarsSelected.filter({ (car:[String:AnyObject]) -> Bool in
            if let distace = car["distance"] as? String
            {
                if distace.toDouble() >= mindistance && distace.toDouble() < maxDistance{
                    return true
                }
                else {
                    return false
                }
            }
            return false
        })
        
        for car in filteredResults {
            arrDriverIDs.append(car["driver_id"] as! String)
            if (!appDelegate.requestedDriverIDs.contains(car["driver_id"] as! String)){
                appDelegate.requestedDriverIDs.append(car["driver_id"] as! String)
            }
            
        }
        Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(appDelegate.requestedDriverIDs!, key: Constants.allRequestedDrivers)
        return arrDriverIDs
    }
    
    // MARK:- ======show bottom view with Status of Job===============
    func showBottomViewsWithJobStatus(showBottomView:Bool, showCancel:Bool){
        
        weak var weakSelf = self
        if showBottomView {
            UIView.animateKeyframesWithDuration(0.10, delay:0.0, options: .OverrideInheritedOptions, animations: { () -> Void in
                if showCancel == true {
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        weakSelf!.viewPickupDropOff.translatesAutoresizingMaskIntoConstraints = true
                        weakSelf!.viewPickupDropOff.frame = CGRectMake(0, Constants.screenHeight - 320,CGRectGetWidth(weakSelf!.view.frame) , 198.0)
                        weakSelf!.view.sendSubviewToBack(weakSelf!.viewPickupDropOff)
                        weakSelf!.mainRequestView.viewWithTag(45)?.hidden = false
                        
                    })
                } else  {
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        weakSelf!.viewPickupDropOff.translatesAutoresizingMaskIntoConstraints = true
                        weakSelf!.viewPickupDropOff.frame = CGRectMake(0, Constants.screenHeight - 262,CGRectGetWidth(weakSelf!.view.frame) , 198.0)
                        weakSelf!.view.bringSubviewToFront(weakSelf!.viewPickupDropOff)
                        weakSelf!.mainRequestView.viewWithTag(45)?.hidden = true
                    })
                }
                }, completion: nil)
            
        }else {
            UIView.animateKeyframesWithDuration(0.10, delay:0.0, options: .OverrideInheritedOptions, animations: { () -> Void in
                if showCancel == true {
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        weakSelf!.viewPickupDropOff.translatesAutoresizingMaskIntoConstraints = true
                        weakSelf!.viewPickupDropOff.frame = CGRectMake(0, Constants.screenHeight - 228,CGRectGetWidth(weakSelf!.view.frame) , 198.0)
                        weakSelf!.view.bringSubviewToFront(weakSelf!.viewPickupDropOff)
                        weakSelf!.mainRequestView.viewWithTag(45)?.hidden = false
                        
                    })
                } else  {
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        weakSelf!.viewPickupDropOff.translatesAutoresizingMaskIntoConstraints = true
                        weakSelf!.viewPickupDropOff.frame = CGRectMake(0, Constants.screenHeight - 198,CGRectGetWidth(weakSelf!.view.frame) , 198.0)
                        weakSelf!.view.bringSubviewToFront(weakSelf!.viewPickupDropOff)
                        weakSelf!.mainRequestView.viewWithTag(45)?.hidden = true
                    })
                }
                }, completion: nil)
            
        }
    }
    func getAddressLatLongiFromAddressString(strAddress: String, isPickup:Bool) -> CLLocation {
        var latitude:Double = 0
        var longitude:Double = 0
        let esc_addr: String = strAddress.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let req = Globals.sharedInstance.getFullApiAddressForAddressString(esc_addr)
        var result : String!
        do {
            result = try String(contentsOfURL: NSURL(string: req)!, encoding: NSUTF8StringEncoding)
        }catch {
            print("json Error-::\(error)")
            
        }
        
        if result  != "" {
            let scanner: NSScanner = NSScanner(string: result)
            
            if scanner.scanUpToString("\"lat\" :", intoString: nil) && scanner.scanString("\"lat\" :", intoString: nil) {
                scanner.scanDouble(&latitude)
                if scanner.scanUpToString("\"lng\" :", intoString: nil) && scanner.scanString("\"lng\" :", intoString: nil) {
                    scanner.scanDouble(&longitude)
                }
            }
        }
        
        let location = CLLocation(latitude: latitude, longitude: longitude)
        if isPickup == true {
            self.dicOngoingJob["pickup_latitude"] = String(latitude)
            self.dicOngoingJob["pickup_longitude"] = String(longitude)
        }else {
            self.dicOngoingJob["dropoff_latitude"] = String(latitude)
            self.dicOngoingJob["dropoff_longitude"] = String(longitude)
            
        }
        
        return location
    }
    
    // // MARK:- ========Job Action Delegate =============
    
    func jobAccepted(dicNotification: [String : AnyObject]) {
           self.stopTimerForRequestToDriver()
        self.isJobRequest = false
             self.jobAcceptedbyDriver = true
        let  strFirstName =  dicNotification["first_name"] as! String
        let strlastName = dicNotification["last_name"] as! String
        let alertView = UIAlertView(title:"Job Accepted" , message: "Job accepted with driver name \(strFirstName) \(strlastName)", delegate: self, cancelButtonTitle: "Okay")
        alertView.show()
    }
    
    func jobRejected(dicNotification: [String : AnyObject]) {
        self.stopTimerForRequestToDriver()
        self.jobAcceptedbyDriver = false
        self.resendRequestToAnotherDriverAfterTimeInterval()
    }
    
    
    
    // MARK: jobStatusChanged
    func jobStatusChanged(strJobStatus:String)
    {
        appDelegate.hideProgress()
        self.activeDriver = Globals.sharedInstance.activeDriver!
        
        dispatch_async(dispatch_get_main_queue(), { [weak self]() -> Void in
            
            self?.backButtonTitile(strJobStatus)
            
            if  strJobStatus.compare("Customer in car")  || strJobStatus.compare("Drop off")  || strJobStatus.compare("Circling")  {
                self?.showBottomViewsWithJobStatus(true, showCancel: false)
            }
            if strJobStatus.compare("En Route")  || strJobStatus.compare("Accept") || strJobStatus.compare("On location")
            {
                self?.showBottomViewsWithJobStatus(false, showCancel: true)
            }
            else if strJobStatus.compare("Bail Out"){
                if self?.activeDriver.reservation_Source != "on_demand"
                {
                  //  self?.jobCancelled()
                }
                else {
                   // self?.cancelJobAndLookForOtherDrivers()
                }
            }
            self?.activeDriver.jobStatus = strJobStatus
            self?.setUpViewForJobStatusWith(strJobStatus)
            })
        //self.etaMapIconForStaus(strJobStatus)
        
    }
    func backButtonTitile(currentJobStaus:String){
        
        var title:String = ""
        if currentJobStaus.compare("En Route") {
            title = "Driver En Route"
        }
        else if currentJobStaus.compare("on location") {
            title = "Driver On Location"
        }
        else if currentJobStaus.compare("circling"){
            title = "Driver Circling"
        }
        else if currentJobStaus.compare("customer in car"){
            title = "Customer on board"
        }
        else if currentJobStaus.compare("drop off"){
            title = " Customer Drop off"
        }
        else if currentJobStaus.compare("done"){
            title = "Job Done"
        }
            
        else if currentJobStaus.compare("Accept"){
            title = "Job Accepted"
        }
        else {
            title = currentJobStaus
        }
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom(title.capitalizedString, imageName: "backArrow")
    }

    func JobReceiptRecieved(dicNotification: [String : AnyObject]) {
        
    }
    
    func RemoteNotificationTapAction() {
        
    }
    func saveDriverDetailsWhenJobAccepted(dicJobDetails:[String:AnyObject])
    {
        
        let driverObject = DriverDetails()
        if let cellNumber = dicJobDetails["cellular_phone"] {
            driverObject.cellular_phone = cellNumber as! String
        }
        if let first_name = dicJobDetails["first_name"] {
            driverObject.firstName = first_name as! String
        }
        if let last_name = dicJobDetails["last_name"] {
            driverObject.lastName = last_name as! String
        }
        if let image_url = dicJobDetails["image_url"] {
            driverObject.image_url = image_url as! String
        }
        if let fleet_name = dicJobDetails["fleet_name"] {
            driverObject.fleet_Name = fleet_name as! String
        }
        if let pickup = dicJobDetails["pickup"] {
            driverObject.pickUp_Loc = pickup as! String
        }
        if let dropOff = dicJobDetails["dropoff"] {
            let arrDropOff = (dropOff as! String).componentsSeparatedByString("place_id")
            if arrDropOff.contains("place_id"){
                driverObject.dropOff_Loc = arrDropOff[0]
            }else {
                driverObject.dropOff_Loc = dropOff as! String
            }
        }
        if let licenceNo = dicJobDetails["licenceno"] {
            driverObject.licence_number = licenceNo as! String
        }
        if let fleet_image = dicJobDetails["fleet_image"] {
            driverObject.fleet_image = fleet_image as! String
        }
        if let vehicle_type_title = dicJobDetails["vehicle_type_title"] {
            driverObject.vehicle_type_title = vehicle_type_title as! String
        }
        if let availableStatus = dicJobDetails["availablestatus"] {
            driverObject.response_Status = availableStatus as! String
        }
        if let status = dicJobDetails["status"] {
            driverObject.jobStatus = status as! String
        }
        if let driver_id = dicJobDetails["driver_id"] {
            driverObject.driver_ID = driver_id as! String
        }
        
        if let dicRates = dicJobDetails["rate"] {
            
            if let base_fare = dicRates["base_fare"] {
                driverObject.base_fare = base_fare as! String
            }
            if let mile_rate = dicRates["mile_rate"] {
                driverObject.mile_rate = mile_rate as! String
            }
            if let km_rate = dicRates["km_rate"] {
                driverObject.km_rate = km_rate as! String
            }
            if let min_fare = dicRates["min_fare"] {
                driverObject.min_fare = min_fare as! String
            }
            if let min_rate = dicRates["min_rate"] {
                driverObject.min_rate = min_rate as! String
            }
        }
        else {
            driverObject.base_fare = "0.0"
            driverObject.mile_rate = "0.0"
            driverObject.min_fare = "0.0"
            driverObject.min_rate = "0.0"
            driverObject.km_rate = "0.0"
        }
        if let dicCustom = dicJobDetails["custom"]{
            if let booking_id = dicCustom["booking_id"] {
                driverObject.booking_Id = String(booking_id)
                driverObject.reservation_id = driverObject.booking_Id
            }
        }
        self.jobAcceptedbyDriver = true
        Globals.sharedInstance.activeDriver = driverObject
         
    }
    // MARK:- ========== Custome button Actions =======
    @IBAction func btnCallAction(sender: UIButton) {
        
        if let strPhoneNumber = self.dicOngoingJob["driver_phone"] as? String{
            
            self.activeButton = sender
            
            if let url = NSURL(string: "tel://\(strPhoneNumber.stringByReplacingOccurrencesOfString(" ", withString: ""))") {
                if (UIApplication.sharedApplication().canOpenURL(url)) {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            else {
                Globals.ToastAlertWithString("Not a valid contact number")
            }
            
        }else {
            Globals.ToastAlertWithString("No Phone Number Found")
        }
    }
    
    @IBAction func btnMessageAction(sender: UIButton) {
        
        if let strPhoneNumber = self.dicOngoingJob["driver_phone"]{
            sender.selected = true
            self.activeButton = sender
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [strPhoneNumber as! String]
                controller.messageComposeDelegate = self
                self.presentViewController(controller, animated: true, completion: nil)
            }
        }else {
            Globals.ToastAlertWithString("No Phone Number Found")
        }
        
    }
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        //... handle sms screen actions
        self.activeButton.selected = false
        
        self.dismissViewControllerAnimated(true, completion: nil)
        self.activeButton = nil
    }
    @IBAction func btnShowDriverProfileAction(sender: UIButton) {
        
        sender.selected = true
        var dicParam = [String:AnyObject]()
        var driverName:String! = ""
        if let firstName = self.dicOngoingJob["driver_firstName"] as? String{
            driverName = driverName.stringByAppendingString(firstName)
        }
        if let lastName = self.dicOngoingJob["driver_lastName"] as? String{
            driverName = driverName.stringByAppendingString(" ".stringByAppendingString(lastName))
        }
        
        if driverName.characters.count == 1{
            driverName = "Driver Name: Not Assigned"
        }
        
        var vehicleName: String!
        if let nameVehcile = self.dicOngoingJob["carDescription"] as? String{
            vehicleName = nameVehcile
        }else {
            vehicleName = "VehicleName: N/A"
        }
        var driver_phoneS: String!
        if let driver_phone = self.dicOngoingJob["driver_phone"] as? String{
            driver_phoneS = driver_phone
        }else {
            driver_phoneS = " N/A"
        }
        var licence: String!
        if let licences = self.dicOngoingJob["driver_license"] as? String{
            licence = licences
        }else {
            licence = " N/A"
        }
        dicParam[Constants.DriverName] = driverName
        dicParam[Constants.VehicleName] = vehicleName
        dicParam[Constants.DriverPhone] = driver_phoneS
        dicParam[Constants.DriverLicence] = licence
        dicParam[Constants.DriverImage] = self.dicOngoingJob["driver_image"] as? String
        let detailsVc = self.storyboard?.instantiateViewControllerWithIdentifier("PADriverAndRateCardAlertVC") as! PADriverAndRateCardAlertVC
        detailsVc.isDriverProfile = true
        detailsVc.dicDetails = dicParam
        detailsVc.view.backgroundColor = UIColor.clearColor()
        detailsVc.modalPresentationStyle = .OverFullScreen
        detailsVc.providesPresentationContextTransitionStyle = true
        detailsVc.definesPresentationContext = true
        //detailsVc.modalPresentationStyle = .Custom
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        self.modalTransitionStyle = .CrossDissolve
        self.modalPresentationStyle = .OverFullScreen
        self.presentViewController(detailsVc, animated: true) { () -> Void in
            sender.selected = false
        }
        // self.presentViewController(detailsVc, animated: true, completion: nil)
        
    }
    @IBAction func btnRateCardForFareAction(sender: UIButton)
    {
        
        if let reservation_source = self.dicOngoingJob["reservation_source"] as? String {
            if reservation_source != "on_demand"
            {
                let alertView = UIAlertView(title:"Rates not applicable!" , message: "Rates are only available for ride now jobs.", delegate: nil, cancelButtonTitle: "Dismiss")
                alertView.show()
                return
            }
        }
        
        sender.selected = true
        if let rate = self.dicOngoingJob["rates"] {
            
            var dicParam = [String:AnyObject]()
            
            if let Mile = rate["mile_rate"] as? String{
                dicParam[Constants.PerMileRate] = Mile
            }else {
                dicParam[Constants.PerMileRate] = "0.0"
            }
            
            if let km = rate["km_rate"] as? String{
                dicParam[Constants.PerKMRate] = km
            }else {
                dicParam[Constants.PerKMRate] = "0.0"
            }
            
            if let minFare = rate["min_fare"] as? String{
                dicParam[Constants.MinFare] = minFare
            }else {
                dicParam[Constants.MinFare] = "0.0"
            }
            if let min_rate = rate["min_rate"] as? String{
                dicParam[Constants.PerMinRate] = min_rate
            }else {
                dicParam[Constants.PerMinRate] = "0.0"
            }
            if let base_fare = rate["base_fare"] as? String{
                dicParam[Constants.BaseFare] = base_fare
            }else {
                dicParam[Constants.BaseFare] = "0.0"
            }
            
            let detailsVc = self.storyboard?.instantiateViewControllerWithIdentifier("PADriverAndRateCardAlertVC") as! PADriverAndRateCardAlertVC
            detailsVc.isDriverProfile = false
            detailsVc.dicDetails = dicParam
            detailsVc.view.backgroundColor = UIColor.clearColor()
            detailsVc.modalPresentationStyle = .OverFullScreen
            detailsVc.providesPresentationContextTransitionStyle = true
            detailsVc.definesPresentationContext = true
            self.providesPresentationContextTransitionStyle = true
            self.definesPresentationContext = true
            self.modalTransitionStyle = .CrossDissolve
            self.modalPresentationStyle = .OverFullScreen
            self.presentViewController(detailsVc, animated: true) { () -> Void in
                sender.selected = false
            }
            // self.presentViewController(detailsVc, animated: true, completion: nil)
        }else {
            sender.selected = false
        }
    }
    @IBAction func btnDropoffClicked(sender: AnyObject) {
        if self.isJobRequest == true {
            return
        }
        let searchVC = self.storyboard?.instantiateViewControllerWithIdentifier("SearchPlacesAddessViewController") as! SearchPlacesAddessViewController
        searchVC.strTitleOfView = ""
        searchVC.finishSelectingDropOffAddress = { [weak self](dictDropOff : [String:AnyObject]) -> () in
            if dictDropOff.count != 0 {
                self?.updateDropoffAddress(dictDropOff)
               }
        }
        let nvc = UINavigationController(rootViewController: searchVC)
        
        searchVC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Dropoff Address", imageName: "backArrow")
        self.navigationController!.presentViewController(nvc, animated: true, completion: nil)
        
        
    }
    
    func updateDropoffAddress(dicDropOffAddress:[String:AnyObject]){
        
        var dicParameters = [String:AnyObject]()
        dicParameters["user_id"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
        dicParameters["session"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
        dicParameters["reservation_id"] = self.activeDriver.reservation_id!
        dicParameters["driverid"] = self.activeDriver.driver_ID!
        dicParameters["passenger_cordinates"] = ""
        dicParameters["passenger_eta"] = ""
        dicParameters["dropoff"] = "\(dicDropOffAddress["address"]!)"
        dicParameters["latitude"] = "\(dicDropOffAddress["latitude"]!)"
        dicParameters["longitude"] = "\(dicDropOffAddress["longitude"]!)"
        
        print(dicParameters)
        
         appDelegate.showProgressWithText("Updating dropoff locaiton")
        ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.updateDropoffAddress, dicParameters:dicParameters
            , successBlock: { [weak self](dicData) -> Void in
             
                print("dic car Request----\(dicData)")
                
                if dicData["status"] as! String == "getdriver" {
                        if let add = dicDropOffAddress["address"] as? String
                        {
                            self?.lblDropOffAddress.setTitle(add, forState: .Normal)
                        }
                        print("\(self?.dicOngoingJob) \(dicDropOffAddress)")
                        
                        if self?.strJobStatus != nil{
                                if self?.strJobStatus.caseInsensitiveCompare("Customer in car") == NSComparisonResult.OrderedSame {
                                    if self?.driver_Current_Location?.longitude != 0.0 && self?.driver_Current_Location?.latitude != 0.0 {
                                        
                                        dispatch_async(dispatch_get_main_queue(), {[weak self] () -> Void in
                                            self?.dicOngoingJob["dropoff"] = "\(dicDropOffAddress["address"]!)"
                                            self?.dicOngoingJob["dropoff_latitude"]  = "\(dicDropOffAddress["latitude"]!)"
                                            self?.dicOngoingJob["dropoff_longitude"] =  "\(dicDropOffAddress["longitude"]!)"
                                            
                                            let postion = CLLocation(latitude: dicDropOffAddress["latitude"] as! Double, longitude: dicDropOffAddress["longitude"] as! Double)
                                            self?.showDriverLocationOnMap((self?.driver_Current_Location)!, withVehicalTitle: "")
                                            self?.locationPickOrDrop = postion
                                            self?.etaLblShowTime.text = Globals.sharedInstance.formatArrivalTimeStringWith("")
                                            self?.setupMapBoundsWithLocation(postion)
                                            self?.etaMapIconForStaus("loading", stauts: (self?.strJobStatus)!)
                                            self?.carMarker.icon = self?.carMarkerImage
                                            self?.carMarker.map = self?.mapViewRequest
                                            self?.pickupDropOffMarker.position = (self?.locationPickOrDrop.coordinate)!
                                            self?.pickupDropOffMarker.icon = self?.finalImage
                                            self?.pickupDropOffMarker.map = self?.mapViewRequest
                                            
                                            })
                                    }
                                }
                                else {
                                    self?.dicOngoingJob["dropoff"] = "\(dicDropOffAddress["address"]!)"
                                    self?.dicOngoingJob["dropoff_latitude"]  = "\(dicDropOffAddress["latitude"]!)"
                                    self?.dicOngoingJob["dropoff_longitude"] =  "\(dicDropOffAddress["longitude"]!)"
                                    self?.carMarker.icon = self?.carMarkerImage
                                    self?.carMarker.map = self?.mapViewRequest
                                    self?.pickupDropOffMarker.icon = self?.finalImage
                                    self?.pickupDropOffMarker.map = self?.mapViewRequest

                                }
                        }
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        appDelegate.hideProgress()
                        })
                    }
                else {
                }
                
        }) { (error) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                appDelegate.hideProgress()
                   let alert = UIAlertView(title: "Somethig went wrong !", message: "Please try again", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()

            })
        }
        
        
    }
    // MARK:- ===Refresh Driver Loc with 6 sec delay =====
    func refreshDriverLocation(){
        
        if self.timerDriverLoc == nil{
            self.timerDriverLoc = NSTimer.scheduledTimerWithTimeInterval(6.0, target:self, selector: "getDriverLocation", userInfo:nil , repeats: true)
        }
        
    }
    func stoprefreshDriverLocation(){
        
        if self.timerDriverLoc != nil{
            if self.timerDriverLoc.valid{
                self.timerDriverLoc.invalidate()
            }
        }
    }
    // MARK:- ===web call to get driver location =====
    
    func getDriverLocation() {
        
        if appDelegate.networkAvialable == true {
            if let _ = self.activeDriver {
                if let _ = self.activeDriver.driver_ID
                {
                    var dicParameters = [String:AnyObject]()
                    dicParameters["driver_id"] = self.activeDriver!.driver_ID
                    dicParameters["user_id"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
                    dicParameters["session"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {[weak self] () -> Void in
                        ServiceManager.sharedInstance.driverStatusTrackRequestPost("get_driver_track", dicParameters: dicParameters, successBlock: { (dicData) -> Void in
                            print(dicData)
                            if let dic = dicData["message"] as? [String:AnyObject]{
                                self?.driver_Current_Location = CLLocationCoordinate2DMake((dic["latitude"] as! String).toDouble()!, (dic["longitude"] as! String).toDouble()!)
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self?.etaLblShowTime.text = Globals.sharedInstance.formatArrivalTimeStringWith(dic["eta"] as! String)
                                    self?.carMarker.position = (self?.driver_Current_Location!)!
                                    self?.carMarker.icon = self?.carMarkerImage
                                    self?.carMarker.map = self?.mapViewRequest
                                    self?.animateCarMovementonMapWithMarker()
                                    self?.setupMapBoundsWithLocation(self?.locationPickOrDrop)
                                    
                                    if let JobStatus = dic["job_status"] as? String{
            
                                        self?.strJobStatus = JobStatus
                                        var  pickLocation : CLLocation!
                                        var postionPickup : CLLocationCoordinate2D!
                                        
                                        if JobStatus.caseInsensitiveCompare("En Route") == NSComparisonResult.OrderedSame || JobStatus.caseInsensitiveCompare("On Location") == NSComparisonResult.OrderedSame || JobStatus.caseInsensitiveCompare("Circling") == NSComparisonResult.OrderedSame {
                                            if self?.dicOngoingJob["pickup_latitude"] as! String != "" && self?.dicOngoingJob["pickup_longitude"] as! String != ""
                                            {
                                                pickLocation = CLLocation(latitude:(self?.dicOngoingJob["pickup_latitude"] as! String ).toDouble()! , longitude: (self?.dicOngoingJob["pickup_longitude"] as! String).toDouble()!)
                                                postionPickup = CLLocationCoordinate2DMake(pickLocation.coordinate.latitude,pickLocation.coordinate.longitude)
                                            }else {
                                                pickLocation = self?.getAddressLatLongiFromAddressString((self?.dicOngoingJob["pickup"] as! String), isPickup: true)
                                                postionPickup = CLLocationCoordinate2DMake(pickLocation.coordinate.latitude,pickLocation.coordinate.longitude)
                                            }
                                            self?.locationPickOrDrop = CLLocation(latitude:postionPickup.latitude , longitude: postionPickup.longitude)
                                        }
                                        else {
                                            if self?.dicOngoingJob["dropoff"] as! String != "" {
                                                if self?.dicOngoingJob["dropoff_latitude"] as! String != "" && self?.dicOngoingJob["dropoff_longitude"] as! String != ""{
                                                    let dropLocation = CLLocation(latitude:(self?.dicOngoingJob["dropoff_latitude"] as! String).toDouble()! , longitude: (self?.dicOngoingJob["dropoff_longitude"] as! String).toDouble()!)
                                                    postionPickup = CLLocationCoordinate2DMake(dropLocation.coordinate.latitude,dropLocation.coordinate.longitude)
                                                    self?.locationPickOrDrop = dropLocation
                                                }else {
                                                    pickLocation = self?.getAddressLatLongiFromAddressString((self?.dicOngoingJob["dropoff"] as! String), isPickup: false )
                                                    postionPickup = CLLocationCoordinate2DMake(pickLocation.coordinate.latitude,pickLocation.coordinate.longitude)
                                                }
                                            } else {
                                                if self?.dicOngoingJob["pickup_latitude"] as! String != "" && self?.dicOngoingJob["pickup_longitude"] as! String != ""
                                                {
                                                    postionPickup = CLLocationCoordinate2DMake((self?.dicOngoingJob["pickup_latitude"] as! String ).toDouble()!,(self?.dicOngoingJob["pickup_longitude"] as! String).toDouble()!)
                                                }
                                                self?.etaLblShowTime.text = "OL"
                                            }
                                            self?.locationPickOrDrop = CLLocation(latitude:postionPickup.latitude , longitude: postionPickup.longitude)
                                        }
                                        
                                        self?.etaMapIconForStaus((self?.etaLblShowTime.text)!, stauts: dic["job_status"] as! String)
                                        
                                        if self?.locationPickOrDrop  != nil
                                        {
                                        self?.pickupDropOffMarker.position = (self?.locationPickOrDrop!.coordinate)!
                                        self?.pickupDropOffMarker.icon = self?.finalImage
                                        // self?.pickupDropOffMarker.userData = eta
                                        self?.pickupDropOffMarker.map = self?.mapViewRequest
                                        }
                                        
                                    }
                                    
                                })
                            }
                            }, failureBlock: { (error) -> () in
                        })
                        })
                }
            }
        }
    }
    // MARK:- ======Animate driver on map =====
    func etaMapIconForStaus(etaString:String, stauts:String){
        
        if self.isJobRequest == true {
            self.showEtaMethodsOnMapWith("Requesting", imageString: "blue.png")
        }else {
            
            if(stauts).caseInsensitiveCompare("En Route") == NSComparisonResult.OrderedSame{
                self.showEtaMethodsOnMapWith(etaString, imageString: "blue.png")
            }else if (stauts).caseInsensitiveCompare("On location") == NSComparisonResult.OrderedSame{
                self.showEtaMethodsOnMapWith("OL", imageString: "green.png")
            }else if (stauts).caseInsensitiveCompare("Circling") == NSComparisonResult.OrderedSame{
                self.showEtaMethodsOnMapWith("CL", imageString: "green.png")
            }
            else if (stauts).caseInsensitiveCompare("Customer in car") == NSComparisonResult.OrderedSame{
                self.showEtaMethodsOnMapWith(etaString, imageString: "green.png")
            }
            else if (stauts).caseInsensitiveCompare("Drop off") == NSComparisonResult.OrderedSame{
                self.showEtaMethodsOnMapWith("Drop off", imageString: "blue.png")
            }
            else {
                self.showEtaMethodsOnMapWith(etaString, imageString: "blue.png")
            }
        }
        
    }
    func animateCarMovementonMapWithMarker() {
        
        if  self.driver_Last_Location == nil || self.driver_Last_Location.latitude == 0.0 ||  self.driver_Last_Location.longitude == 0.0  {
            self.driver_Last_Location = self.driver_Current_Location
        }
            
        else if (self.driver_Current_Location.latitude == self.driver_Last_Location.latitude) &&  (self.driver_Current_Location.longitude == self.driver_Last_Location.longitude){
            return;
        }
        
        let bearing: Double = self.getHeadingForDirectionFromCoordinate(driver_Last_Location, toCoordinate: driver_Current_Location)
        let p1: MKMapPoint = MKMapPointForCoordinate(self.driver_Last_Location)
        let p2: MKMapPoint = MKMapPointForCoordinate(self.driver_Current_Location)
        let dist: CLLocationDistance = MKMetersBetweenMapPoints(p1, p2)
        if dist < 6.0 {
            return
        }
        CATransaction.begin()
        CATransaction.setAnimationDuration(6.0)
        self.carMarker.position = self.driver_Last_Location
        self.carMarker.position = self.driver_Current_Location
        self.carMarker.rotation = bearing
        CATransaction.commit()
        self.driver_Last_Location = self.driver_Current_Location
    }
    func getHeadingForDirectionFromCoordinate(fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Double {
        let fromLat: Double = degreesToRadians(fromLoc.latitude)
        let fromLng: Double = degreesToRadians(fromLoc.longitude)
        let toLat: Double = degreesToRadians(toLoc.latitude)
        let toLng: Double = degreesToRadians(toLoc.longitude)
        
        let x = sin(toLng - fromLng) * cos(toLat)
        let y = cos(fromLat) * sin(toLat) - sin(fromLat) * cos(toLat) *  cos(toLng - fromLng)
        
        let degree = radiandsToDegrees(atan2(x, y))
        if degree >= 0 {
            return degree
        }
        else {
            return 360 + degree
        }
    }
    func degreesToRadians(location:Double) -> Double{
        return M_PI * location / 180.0
    }
    func radiandsToDegrees(location:Double) -> Double{
        return  location * 180.0 / M_PI
    }
    // MARK: - ===Alert Delegate=====
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        appDelegate.hideProgress()

        if buttonIndex == 0 {
            self.backToOnDemandViewController()
        }
    }
    
    
    
    // MARK:- ==== button cancel Request Action======
    
    @IBAction func btnCancelRequestAction(sender: UIButton)
    {
        
        
        if self.activeDriver.reservation_id == nil || self.reservaiton_ID == "" {
            self.stoprefreshDriverLocation()
            self.stopTimerForRequestToDriver()
            self.backToOnDemandViewController()
            return
        }

        let alert = UIAlertController(title: "Confirmation", message: "Do you want to cancel booking?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{ (ACTION :UIAlertAction!)in
            
            if appDelegate.networkAvialable == true
            {
                
                if self.activeDriver == nil{
                    return
                }
                if self.activeDriver.reservation_id != nil && self.activeDriver.booking_Id != nil {
                    
                    appDelegate.showProgressWithText("Canceling Request...")
                    var dicParameters = [String:AnyObject]()
                    
                    dicParameters["booking_id"] = self.activeDriver!.booking_Id!
                    
                    dicParameters["driver_id"] = appDelegate.requestedDriverIDs
                    
                    dicParameters["reservation_id"] = self.activeDriver!.reservation_id!
                    dicParameters["user_id"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
                    dicParameters["session"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
                    ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.cancel_Booking, dicParameters: dicParameters, successBlock: { [weak self](dicData) -> Void in
                        appDelegate.hideProgress()
                        print("cancel Request --\(dicData)")
                        if dicData["status"] as! String == "success"{
                            
                            JLToast.makeText("Booking cancelled Successfully ", duration: 3.0).show();
                            Globals.sharedInstance.activeDriver = nil
                            self?.activeDriver = nil
                            self?.stoprefreshDriverLocation()
                            self?.stopTimerForRequestToDriver()
                            self?.locationManagerObjct.dicDestinationLocation?.removeAll()
                            self?.locationManagerObjct.destinationLocation = nil
                           self?.backToOnDemandViewController()
                            
                        }else  if dicData["status"] as! String == "session_expired"{
                            appDelegate.hideProgress()
                            let alert = UIAlertView(title: "Error !", message:  dicData["message"] as? String, delegate: nil, cancelButtonTitle: "Ok")
                            alert.show()
                            
                        }
                        
                        }, failureBlock: { (error) -> () in
                            appDelegate.hideProgress()
                            let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
                            // alert.show()
                    })
                }
            }else
            {
                let alert = UIAlertView(title: "Error !", message: "Please check Internet Connection", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
            }
            
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler:{ (ACTION :UIAlertAction!)in
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
 
    func backButtonActionMethod(sender:UIButton){
        
        if self.jobAcceptedbyDriver == true{
            self.stoprefreshDriverLocation()
            self.backToOnDemandViewController()
        }
        
    }
    
}// class brackets
