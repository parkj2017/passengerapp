//
//  OngoingJobsViewController.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 03/02/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
class OngoingJobsViewController: BaseViewController,BackButtonActionProtocol, JobActionProtocol {
    
    // MARK:-============ Property decalaration ==========
    
    @IBOutlet weak var tableReservations: UITableView!
    var arrReservationData:[Any]! = []
    var loading:Bool! = false
    var page_Number:Int! = 0
    var totalJobRows:Int! = 0
    var arrFilterDataSearch:[Any]! = []
    var shouldShowSearchResults = false
    var selectedIndexPath : NSIndexPath!
    var refreshControl = UIRefreshControl()
    // MARK:-=====view life cycle=====
    
    func jobAccepted(_ dicNotification:[String:Any]){
        var driverName = ""
        var conf_id = ""
        
        if let dicCustom = dicNotification["custom"] as? [AnyHashable:Any]{
            if let b_id = dicCustom["booking_id"]{
                conf_id = "\(b_id)"
            }
            if let f_name = dicNotification["first_name"], let  l_name = dicNotification["last_name"]{
                driverName = "\(f_name) \(l_name)"
            }
            
            let alertView = UIAlertView(title:"Job Accepted" , message: "Job with booking No #\(conf_id) accepted by \(driverName). \n Now you can track this job in \"ON-GOING RESERVATION\" section.", delegate: nil, cancelButtonTitle: "Okay")
            alertView.show()
        }
        
        self.refreshReservation(refreshControl)
    }
    func jobRejected(_ dicNotification:[String:Any]){
        self.refreshReservation(refreshControl)
    }
    func JobReceiptRecieved(_ dicNotification:[String:Any]){
        self.refreshReservation(refreshControl)
    }
    func jobStatusChanged(_ strStatus:String, dicNotification:[String:Any]){
        
        if strStatus.compare("Unassigned"){
            
                let alert = UIAlertView(title: "Booking Info", message:"Driver has been unassigned from this job. You can re-request a car from BOOK FOR NOW section.", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
        }
        if strStatus.compare("bail out"){
                  let alert = UIAlertView(title: "Bail Out", message:"Request has already been sent to all nearby chauffeurs. Please hang on, you will be notified once the job is accepted.", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
        }
        self.refreshReservation(refreshControl)
    }
    func getupdatedAddress(_ dicNotification:[String:Any]){
        self.refreshReservation(refreshControl)
    }
    func dropOffDoneByDriver(_ dicNotification:[String:Any]){
        self.refreshReservation(refreshControl)
    }
    func  jobCancelled(_ dicNotification:[String:Any]?){
        self.refreshReservation(refreshControl)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // self.title = "Ongoing Jobs"
        
        page_Number = 1
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Ongoing Reservations", imageName: "backArrow")
        self.tableReservations.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(OngoingJobsViewController.refreshReservation(_:)), for: UIControlEvents.valueChanged)
        self.tableReservations.addSubview(refreshControl)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
        self.arrReservationData.removeAll()
        self.arrFilterDataSearch.removeAll()
        getDriverActiveJobsFromDate()
        appDelegate.delegateJobStatusAction = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        appDelegate.delegateJobStatusAction = nil
        
    }
    // MARK:-====== UITableView Delegate and data Source ==========
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.shouldShowSearchResults == true {
            return arrFilterDataSearch.count
        }else {
            return arrReservationData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIndentifier = "OngoingJobsTableCellIdentifier"
        var cell : ReservationTableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath as IndexPath) as? ReservationTableViewCell
        if cell == nil {
            cell = UITableViewCell(style:.default, reuseIdentifier: cellIndentifier) as! ReservationTableViewCell
        }
        
        var reservation_DC: [String : Any]!
        if self.shouldShowSearchResults == true {
            reservation_DC = arrFilterDataSearch[indexPath.row] as! [String : Any]
        }else {
            reservation_DC = arrReservationData[indexPath.row] as! [String : Any]
        }
        let viewPickup = cell.viewWithTag(111)!
        let viewDropp = cell.viewWithTag(112)!

        Globals.layoutViewFor(viewPickup, color: UIColor.darkBlack(), width: 1.5, cornerRadius: 3)
        Globals.layoutViewFor(viewDropp, color: UIColor.appThemeColor(), width: 1.5, cornerRadius: 3)
        Globals.layoutViewFor( cell.lblStops, color: UIColor.appThemeColor(), width: 1.5, cornerRadius: cell.lblStops.frame.size.width / 2.0)

        
        let strbookingID = String(describing: reservation_DC["conf_id"]!).replacingOccurrences(of: "-", with: "")
        cell.lblBookingNumber.attributedText = strbookingID.setAttributedStringWithColorSize("Booking ID: ", secondStr: strbookingID, firstColor: UIColor.gray, secondColor: UIColor.darkGray, font1: Globals.defaultAppFont(14), font2:Globals.defaultAppFontWithBold(16))
        let date_Time: String! = "\(reservation_DC["pu_date"]!) \(reservation_DC["pu_time"]!)"
        cell.lblDateOfJob.text = Globals.getFormattedDateString(date_Time)

        if let jobStatus = reservation_DC["availablestatus"] as? String{
            cell.lblJobStatus.attributedText = strbookingID.setAttributedStringWithColorSize("Job Status: ", secondStr: self.JobStatusTitleCustom(jobStatus), firstColor: UIColor.gray, secondColor: UIColor.appThemeColor(), font1: Globals.defaultAppFont(14), font2:Globals.defaultAppFontWithBold(16))
        }
        
        if let stops = reservation_DC["stops"] as? [[String:Any]]
        {
            if stops.count>0{
                cell.lblStops.attributedText = "".setAttributedStringWithColorSize("\(stops.count) \n ", secondStr: "Stops", firstColor: UIColor.appBlackColor(), secondColor: UIColor.appBlackColor(), font1:Globals.defaultAppFontWithBold(16) ,font2:Globals.defaultAppFont(14))
            }
            else {
                cell.lblStops.attributedText = "".setAttributedStringWithColorSize("No \n ", secondStr: "Stops", firstColor: UIColor.appBlackColor(), secondColor: UIColor.appBlackColor(), font1:Globals.defaultAppFontWithBold(16) ,font2:Globals.defaultAppFont(14))
            }
        }
        else{
            cell.lblStops.attributedText = "".setAttributedStringWithColorSize("No \n ", secondStr: "Stops", firstColor: UIColor.appBlackColor(), secondColor: UIColor.appBlackColor(), font1:Globals.defaultAppFontWithBold(16) ,font2:Globals.defaultAppFont(14))
        }

        cell.lblPickupAddess.text = reservation_DC["pickup"] as? String
        cell.lblPickupAddess.font  = Globals.defaultAppFont(12)
        let dropStr = reservation_DC["dropoff"] as? String
        cell.lblDropAddress.text = dropStr == "" ? "N/A" : dropStr!
        cell.lblDropAddress.font  = Globals.defaultAppFont(12)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return Constants.iPadScreen == true ? 240 : 180.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if appDelegate.networkAvialable == true{
            self.selectedIndexPath = indexPath
            //showActiveJob // toOngoingDetails
            self.performSegue(withIdentifier: "showActiveJob", sender: indexPath)
            tableReservations.deselectRow(at: indexPath as IndexPath, animated: true)
        }
        else {
            Globals.ToastAlertForNetworkConnection()
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        if arrReservationData.count > 0{
            let view:UIView = UIView(frame: CGRect(x: 0, y: 0 , width: tableView.frame.size.width, height: 25))
            view.layer.borderColor = UIColor.appGrayColor().cgColor
            view.layer.borderWidth = 1.0
            view.backgroundColor = UIColor.appBlackColor()
            let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0 , width: tableView.frame.size.width, height: 25))
            label.text = "Pull To Refresh"
            label.font = Globals.defaultAppFontWithBold(14)
            label.textAlignment = .center
            label.textColor = UIColor.white
            view.addSubview(label)
            return view
        }
        else {
            return UIView()
        }
    }
    
    // MARK:-======= Pagination of table view rows==========
    func JobStatusTitleCustom(_ currentJobStaus:String) -> String{
        
        var title:String = ""
        if currentJobStaus.compare("En Route") {
            title = "Driver En Route"
        }
        else if currentJobStaus.compare("on location") {
            title = "Driver On Location"
        }
        else if currentJobStaus.compare("circling"){
            title = "Driver Circling"
        }
        else if currentJobStaus.compare("customer in car"){
            title = "Customer on board"
        }
        else if currentJobStaus.compare("drop off"){
            title = " Customer Dropped off"
        }
        else if currentJobStaus.compare("done"){
            title = "Job Done"
        }
            
        else if currentJobStaus.compare("Accept"){
            title = "Job Accepted"
        }
        else {
            title = currentJobStaus
        }
        return title
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if !self.loading {
            let endScrolling:CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height
            if endScrolling >= scrollView.contentSize.height {
                self.loading = true
                self.loadDataDelayed()
            }
        }
    }
    
    func loadDataDelayed() {
        if totalJobRows > arrReservationData.count {
            let page: Double = ceil(Double(arrReservationData.count)/8.0)
            getReservation(Int(page) + 1)
        }else {
            self.refreshControl.endRefreshing()
        }
    }
    
    func getReservation(_ page: Int) {
        page_Number = page
        getDriverActiveJobsFromDate()
    }
    
    // MARK:-========= Webservice Call to get ongoing reservartions ========
    func refreshReservation(_ sender:UIRefreshControl){
        // if !self.refreshControl.refreshing {
        page_Number = 1
        self.refreshControl.endRefreshing()
        getDriverActiveJobsFromDate()
        // }
    }
    func getDriverActiveJobsFromDate() {
        if (appDelegate.networkAvialable == true) {
            
            appDelegate.showProgressWithText("Fetching Reservations")
            
            var dicParameters : [String:Any]! = Dictionary()
            
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            
            dicParameters["user_id"] = "\(String(describing: user_ID!))"
            dicParameters["session"] = "\(String(describing: session!))"
            //            dicParameters["status"] = "inactive"
            dicParameters["page"] = "\(page_Number!)"
            //
            dicParameters["company_id"] = Constants.CompanyID
            weak var weakSelf = self
            ServiceManager.sharedInstance.dataTaskWithPostRequest("ongoingJobsHotelApp", dicParameters: dicParameters, successBlock:
                { (dicRecievedJSON) -> Void in
                    if dicRecievedJSON != nil
                    {
                        var dict_Response:[String:Any]!
                        
                        if dicRecievedJSON["status"] as! String == "success" {
                            dict_Response = dicRecievedJSON.formatDictionaryForNullValues(dicRecievedJSON)
                            if let totalRows = dict_Response["total_rows"]{
                                if "\(totalRows)".toInteger() > 0{
                                let id_String: String = dict_Response["record_ids"] as! String
                                var record_idsArray: [Any] = id_String.components(separatedBy: ",")
                                var dictValue: [NSObject : Any] = (dict_Response["records"] as! [NSObject : Any])
                                if dictValue.count > 0 {
                                    if weakSelf?.arrReservationData == nil {
                                        weakSelf?.arrReservationData = [Any]()
                                    }else {
                                        weakSelf?.arrReservationData.removeAll(keepingCapacity: true)
                                    }
                                    for i in 0 ..< record_idsArray.count {
                                        let reservation_DC: [NSObject : Any] = (dictValue[record_idsArray[i] as! NSObject] as! [AnyHashable : Any] as [NSObject : Any])
                                        weakSelf?.arrReservationData.append(reservation_DC)
                                    }
                                }
                                }
                                else {
                                    weakSelf?.arrReservationData.removeAll(keepingCapacity: true)
                                    Globals.ToastAlertWithString("No on going job found")
                                }
                            }
                            else {
                                weakSelf?.arrReservationData.removeAll(keepingCapacity: true)
                                Globals.ToastAlertWithString("No on going job found")
                            }
                            
                            if weakSelf?.arrReservationData.count == 0 {
                                Globals.ToastAlertWithString("No on going job found")
                            }
                            else {
                                weakSelf?.totalJobRows = dict_Response["total_rows"] as! Int
                            }
                        }
                        weakSelf?.loading = false
                        DispatchQueue.main.async(execute: {
                            weakSelf?.tableReservations.reloadData()
                        })
                    appDelegate.hideProgress()
                    }
            }) { (error) -> () in
                appDelegate.hideProgress()
                Globals.ToastAlertWithString("Network Error")
                print(error)
            }
            
        }else {
            Globals.ToastAlertForNetworkConnection()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - =====Navigation====
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //showActiveJob // toOngoingDetails
        if segue.identifier == "showActiveJob"{
            let activeVC = segue.destination as! PAActiveJobViewController
            let dicOngoingJob = self.arrReservationData[self.selectedIndexPath.row] as! [String:Any]
            let driverObject = DriverDetails()
            driverObject.setCurrentJobValues(dicOngoingJob)
            //activeVC.dicOngoingJob = dicOngoingJob
            activeVC.strJobStatus = "En route"
            activeVC.isJobRequest = false
            activeVC.navigateFromOnDemand = false
            activeVC.jobAcceptedbyDriver = true
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    func backButtonActionMethod(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
