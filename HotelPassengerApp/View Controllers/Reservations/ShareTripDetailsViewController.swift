//
//  ShareTripDetailsViewController.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 21/07/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit

class ShareTripDetailsViewController: UIViewController {
    
    @IBOutlet var txtView: UITextView!
    var strShareType:String! = ""
    var reservation_id:String! = ""
    var emailBillling:String! = ""

    @IBOutlet var inputAccView: UIView!
    @IBOutlet var lblPopupTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.txtView.layer.borderColor = UIColor.appGrayColor().cgColor
        self.txtView.layer.borderWidth = 1.0
        self.txtView.inputAccessoryView = inputAccView
        self.txtView.tintColor = UIColor.appBlackColor()
        if emailBillling != nil {
            self.txtView.text = emailBillling
        }
        if strShareType == "trip"
        {
            lblPopupTitle.text = "Share Trip Details"
        }
        else {
            lblPopupTitle.text = "Share Receipt"
        }
        
    }
    
    
    @IBAction func selftxtView(_ sender: Any) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- sharing popup button actions
    @IBAction func sharePopupCancelClicked(_ sender:UIButton)  {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sharePopupDoneClicked(_ sender:UIButton)  {
        if txtView.text == "Enter email(s) with , separated" || txtView.text.isEmpty
        {
            return;
        }
        if appDelegate.networkAvialable == true
        {
            self.view.endEditing(true)

            appDelegate.showProgressWithText("Sending mail...")
            var dicParameters = [String:Any]()
            
            dicParameters["email"] = txtView.text as Any?
            dicParameters["type"] = strShareType as Any?
            dicParameters["reservation_id"] = reservation_id as Any?
            
            if Constants.isBookerLogin == true
            {
                dicParameters["user_id"] = "\(appDelegate.activeUserDetailsForBooker["id"]!)"
                dicParameters["is_booker"] = "yes"
            }
            else
            {
                dicParameters["user_id"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
                dicParameters["session"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
                dicParameters["is_booker"] = "no"

            }
            ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.shareTripDetails, dicParameters: dicParameters, successBlock: { [weak self](dicData) -> Void in
                appDelegate.hideProgress()

                if dicData["status"] as! String == "success"{
                    
                    DispatchQueue.main.async(execute: {
                        Globals.ToastAlertWithString("Your mail sent successfully.")
                        self?.dismiss(animated: true, completion: nil)
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        Globals.ToastAlertWithString("Sorry! Invoice isn't generated yet.")
                        self?.dismiss(animated: true, completion: nil)
                    })
                }
                }, failureBlock: { (error) -> () in
                    appDelegate.hideProgress()
                  //  JLToast.makeText("Please check your network connection", duration: JLToastDelay.ShortDelay).show()
            })
        }
        else
        {
         //   JLToast.makeText("Please check your network connection", duration: JLToastDelay.ShortDelay).show()
        }
        
    }
    // MARK: - ============text view delegate  ==========
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == "Enter email(s) with , separated" {
                textView.text = ""
        }
        return true
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
