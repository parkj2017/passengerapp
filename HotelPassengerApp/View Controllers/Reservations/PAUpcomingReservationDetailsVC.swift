//
//  PAUpcomingReservationDetailsVC.swift
//  PassangerApp
//
//  Created by Netquall on 6/24/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit
import DLImageLoader
import GoogleMaps

class PAUpcomingReservationDetailsVC: BaseViewController , BackButtonActionProtocol{
    
    // MARK: - ============ Property decalaration ==========
    
    @IBOutlet var viewFareDisTimeDetails: UIView!
    
    @IBOutlet var viewPickUpDropOffDetails: UIView!
    
    @IBOutlet var mapViewDetails: GMSMapView!
    
    
    @IBOutlet var viewDriverBookingShareBtns: UIView!
    
    @IBOutlet weak var lblPopupTitle: UILabel!
    
    @IBOutlet weak var lblPopupDetailsText: UILabel!
    @IBOutlet weak var viewPopupDetails: UIView!
    
    @IBOutlet weak var viewEditStarRating: CosmosView!
    @IBOutlet weak var txtViewEditableComments: UITextView!
    
    @IBOutlet weak var tblReservationDetails: UITableView!
    
    var didFinishCancellingReservation:(()->())?
    var isPresented :Bool! = false
    var strJobTitle:String!
    
    
    var mapBounds : GMSCoordinateBounds!
    var isPastReservation = false
    lazy var dicDataSource = [String:Any]()
    var iPadScreen = false
    lazy var arrStops = [[String:Any]]()
    // MARK: - ====View Life cycle =======
    
    
    
    func btnDismissViewAction(_ sender:UIBarButtonItem)  {
        // self.finishAddingExtraFares(strFareDetails: "true")
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.isPresented == true {
            self.title = strJobTitle
            self.view.viewWithTag(32)?.isHidden = true
            self.view.viewWithTag(33)?.isHidden = true
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Dismiss", style: .plain, target: self, action: #selector(PAUpcomingReservationDetailsVC.btnDismissViewAction(_:)))
        }
        
        if let stops = self.dicDataSource["stops"] as? [[String:Any]] {
            self.arrStops = stops
        }
        // self.arrStops = self.dicDataSource["stops"] as! [String]
        self.tblReservationDetails.reloadData()
        //  mapViewDetails.settings.myLocationButton = true
        //  mapViewDetails.myLocationEnabled = true
        mapViewDetails.settings.zoomGestures = true
        self.tblReservationDetails.contentInset = UIEdgeInsetsMake(5, 0, 0, 0)
        // Do any additional setup after loading the view.
        self.tblReservationDetails.backgroundColor = UIColor.white
        self.viewPopupDetails.frame.origin.y = 5000000.0
        self.viewPopupDetails.isHidden = true
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(PAUpcomingReservationDetailsVC.moveToUpcomingList), name: NSNotification.Name(rawValue: Constants.kUpdateJobListNotification), object: nil)
        let strbookingID = String(describing: self.dicDataSource["conf_id"]!).replacingOccurrences(of: "-", with: "")
        
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Booking No: " + "\(strbookingID)", imageName: "backArrow")

        Globals.sharedInstance.delegateBack = self
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Globals.sharedInstance.delegateBack = nil
        
        Constants.reservationUpdated = true
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.kUpdateJobListNotification), object: nil)
    }
    
    func moveToUpcomingList() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // var mapBounds : GMSCoordinateBounds!
        if self.arrStops.count > 0 {
            for dicStops in self.arrStops{
                if let lati = dicStops["pickup_latitude"]  as? String {
                    var pickupLocation:CLLocation?
                    if lati.characters.count != 0 && lati != "0.0"{
                        let PickupLatitude = CDouble(dicStops["pickup_latitude"] as! String)
                        let PickupLongi = CDouble(dicStops["pickup_longitude"] as! String)
                        pickupLocation = CLLocation(latitude:PickupLatitude! , longitude: PickupLongi!)
                    }
                    
                    if pickupLocation != nil{
                        if self.mapBounds == nil{
                            self.mapBounds = GMSCoordinateBounds(coordinate: pickupLocation!.coordinate, coordinate: pickupLocation!.coordinate)
                        }else {
                            let boundsSource = GMSCoordinateBounds(coordinate: pickupLocation!.coordinate, coordinate: pickupLocation!.coordinate)
                            self.mapBounds = self.mapBounds.includingBounds(boundsSource)
                        }
                        let marker = GMSMarker()
                        marker.position = CLLocationCoordinate2DMake(pickupLocation!.coordinate.latitude, pickupLocation!.coordinate.longitude)
                        marker.icon = UIImage(named: "iconStops")
                        marker.map = self.mapViewDetails
                    }
                }
            }
            
        }
        var pickupLocation:CLLocation?

        if let lati = self.dicDataSource["pickup_latitude"]  as? String {
            
            if lati.characters.count != 0 && lati != "0.0"{
                let PickupLatitude = CDouble(self.dicDataSource["pickup_latitude"] as! String)
                let PickupLongi = CDouble(self.dicDataSource["pickup_longitude"] as! String)
                pickupLocation = CLLocation(latitude:PickupLatitude! , longitude: PickupLongi!)
            }
            else {
                if let address = self.dicDataSource["pickup"]  as? String {
                    pickupLocation = getAddressLatLongiFromAddressString(address, isPickup: true)
                }
            }
            
            if pickupLocation != nil{
                if self.mapBounds == nil{
                    self.mapBounds = GMSCoordinateBounds(coordinate: pickupLocation!.coordinate, coordinate: pickupLocation!.coordinate)
                }else {
                    let boundsSource = GMSCoordinateBounds(coordinate: pickupLocation!.coordinate, coordinate: pickupLocation!.coordinate)
                    self.mapBounds = self.mapBounds.includingBounds(boundsSource)
                }
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2DMake(pickupLocation!.coordinate.latitude, pickupLocation!.coordinate.longitude)
                marker.icon = UIImage(named: "pickupPin")
                marker.map = self.mapViewDetails
            }
        }
        var dropLocation:CLLocation?

        if let lati =  self.dicDataSource["dropoff_latitude"]  as? String{
            
            
            if lati.characters.count != 0  && lati != "0.0" {
                // String(format: "%@", locale: lati as! NSLocale)
                let dropLatitude = CDouble(self.dicDataSource["dropoff_latitude"] as! String)
                let dropLongi = CDouble(self.dicDataSource["dropoff_longitude"] as! String)
                dropLocation = CLLocation(latitude:dropLatitude! , longitude: dropLongi!)
            }
            else {
                if let address = self.dicDataSource["dropoff"]  as? String {
                    dropLocation =  getAddressLatLongiFromAddressString(address, isPickup: false)
                }
            }
            
            if dropLocation != nil{
                let boundsSource = GMSCoordinateBounds(coordinate:dropLocation!.coordinate, coordinate: dropLocation!.coordinate)
                self.mapBounds = self.mapBounds.includingBounds(boundsSource)
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2DMake(dropLocation!.coordinate.latitude, dropLocation!.coordinate.longitude)
                marker.icon = UIImage(named: "dropOffPin")
                marker.map = self.mapViewDetails
               
                self.addOverlayToMapView(pickupLoc: pickupLocation!, dropOff: dropLocation!)
            }

        }
        
        
        //   self.mapBounds = GMSCoordinateBounds(coordinate: LocationManager.sharedInstance.currentLocation!.coordinate, coordinate: LocationManager.sharedInstance.currentLocation!.coordinate)
        weak var weakSelf = self
        UIView.animate(withDuration: 0.50, animations: { () -> Void in
            DispatchQueue.main.async(execute: {  () -> Void in
                weakSelf?.mapViewDetails.animate(to: weakSelf!.mapViewDetails.camera(for: weakSelf!.mapBounds, insets: UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0))!)
            })
        })
        
    }
    func addPolyLineWithEncodedStringInMap(_ encodedString: String) {
        let path = GMSMutablePath(fromEncodedPath: encodedString)
        let polyLine = GMSPolyline(path: path)
        polyLine.strokeWidth = 1.5
        polyLine.geodesic = true
        polyLine.strokeColor = UIColor.appThemeColor()
        polyLine.isTappable = true
        polyLine.map = self.mapViewDetails
    }
    func addOverlayToMapView(pickupLoc origin:CLLocation, dropOff destination:CLLocation){
        let request = OCDirectionsRequest(originLocation: origin, andDestinationLocation: destination)
        request?.transitMode = .bus
        request?.trafficModel = .default
        
        request?.alternatives = true
        request?.travelMode = OCDirectionsRequestTravelMode.driving
        request?.waypointsOptimise = true
        
        let direction = OCDirectionsAPIClient(noKeyUseHttps: true)
        direction?.directions(request, response: { (response, error) in
            //print("response ---\(response?.dictionary)")
            if ((error) != nil) {
                return;
            }
            if (response?.status != OCDirectionsResponseStatus.OK) {
                return
            }
            DispatchQueue.main.async(execute: { [weak self] in
                self?.addPolyLineWithEncodedStringInMap(response?.route().overviewPolyline.dictionary["points"] as! String)
            })
            
            if let legs = response?.route().legs.first as? OCDirectionsLeg {
                // print("duration ---\(legs.duration.text)")
            }
            
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - ============text view delegate  ==========
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if self.txtViewEditableComments == textView {
            if self.txtViewEditableComments.text == "Add Comments" {
                self.txtViewEditableComments.text = ""
            }
        }
        return true
    }
    //MARK:- Cancel & Edit Reservation Action
    
    @IBAction func btnEditReservationClicked(_ sender: Any) {

           let detailsVc = UIStoryboard(name: "RideLater", bundle: nil).instantiateViewController(withIdentifier: "PAReservationsRideLaterVC") as! PAReservationsRideLaterVC
            detailsVc.isEditingMode = true
            detailsVc.reservationDetails = self.dicDataSource;
        
        
        self.navigationController?.pushViewController(detailsVc, animated: true)
//           let navControl:UINavigationController = UINavigationController(rootViewController: detailsVc)
//           navControl.navigationBar.isOpaque = true
//            self.show(navControl, sender: nil)
    }
    @IBAction func btnCancelReservationClicked(_ sender: Any) {
        
//        if let driverID = self.dicDataSource["primary_driver"] as? String{
//            if  driverID.characters.count > 0 && driverID != "0" {
        
                let alert = UIAlertController(title: "Confirmation", message: "Do you want to cancel this reservation?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:{[weak self] (ACTION :UIAlertAction!)in
                    if appDelegate.networkAvialable == true
                    {
                        appDelegate.showProgressWithText("Canceling Request...")
                        var dicParameters = [String:Any]()
                        if let strbookingID = self?.dicDataSource["conf_id"] as? String
                        {
                            dicParameters["booking_id"] = strbookingID.replacingOccurrences(of: "-", with: "") as Any?
                        }
                        dicParameters["driver_id"] = self?.dicDataSource["primary_driver"]
                        dicParameters["reservation_id"] = self?.dicDataSource["id"]!
                        dicParameters["user_id"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
                        dicParameters["session"] = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
                        
                        ServiceManager.sharedInstance.dataTaskWithPostRequest(Constants.cancel_ongoing_Reservation, dicParameters: dicParameters, successBlock: { [weak self](dicData) -> Void in
                            appDelegate.hideProgress()
                            if self == nil {
                                return
                            }
                            if dicData["status"] as! String == "success"{
                             //   JLToast.makeText("Reservation cancelled Successfully ", duration: 3.0).show();
                                self?.didFinishCancellingReservation!()
                                DispatchQueue.main.async(execute: { [weak self] in
                                    self?.navigationController?.popViewController(animated: true)
                                    })
                            }
                            }, failureBlock: { (error) -> () in
                                appDelegate.hideProgress()
                                let alert = UIAlertView(title: "Error !", message: "Network Error !", delegate: nil, cancelButtonTitle: "Ok")
                                alert.show()
                                
                        })
                    }
                    else
                    {
                     //   JLToast.makeText("Please check your network connection", duration: JLToastDelay.ShortDelay).show()
                    }
                    }))
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
                    
                }))
                self.present(alert, animated: true, completion: nil)
//            }
//        }
//        else {
//            Globals.ToastAlertWithString("Driver yet not assigned for job")
//        }
    
        
    }
    // MARK: - ============ Get Address from Latitude and Longitude ==========
    
    func getAddressLatLongiFromAddressString(_ strAddress: String, isPickup:Bool) -> CLLocation?
    {
        if strAddress.characters.count == 0 {
            return nil
        }
        //var latitude: UnsafeMutablePointer
        //var longitude: UnsafeMutablePointer
        var latitude:Double = 0
        var longitude:Double = 0/// Swift 8bit int
        // grab the data and cast it to a Swift Int8
        
        //let esc_addr: String = strAddress.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let esc_addr: String = strAddress.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
        
        let req = Globals.sharedInstance.getFullApiAddressForAddressString(esc_addr)
        var result : String!
        do {
            result = try String(contentsOf: URL(string: req)!, encoding: String.Encoding.utf8)
        }catch {
            print("json Error-::\(error)")
            
        }
        
        if result  != "" && result  != nil{
            let scanner: Scanner = Scanner(string: result)
            
            if scanner.scanUpTo("\"lat\" :", into: nil) && scanner.scanString("\"lat\" :", into: nil) {
                scanner.scanDouble(&latitude)
                if scanner.scanUpTo("\"lng\" :", into: nil) && scanner.scanString("\"lng\" :", into: nil) {
                    scanner.scanDouble(&longitude)
                }
            }
        }
        
        let location = CLLocation(latitude: latitude, longitude: longitude)
        
        return location
    }
    
    
    
    // MARK: - ============table View Data Source  ==========
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int
    {
        if  self.arrStops.count == 0{
            return 3
        }else {
            return 3
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.arrStops.count == 0{
            return 0
        }else {
            if section == 12 {
                return self.arrStops.count
            }else {
                return 0
            }
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        var cell : UITableViewCell?
        
        let  cellIndetifier = "cellRideLaterWithStops"
        //cellCarInfo
        cell  = self.tblReservationDetails.dequeueReusableCell(withIdentifier: cellIndetifier)!
        var lblText : UILabel!
        var viewOuter : UIView!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIndetifier)
            lblText = cell!.viewWithTag(63) as! UILabel
            viewOuter = cell!.viewWithTag(62)! as UIView
            
        }else {
            lblText = cell!.viewWithTag(63) as! UILabel
            viewOuter = cell!.viewWithTag(62)! as UIView
            
        }
        viewOuter.layer.borderColor = UIColor.lightGray.cgColor
        viewOuter.layer.borderWidth = 1.0
        //let dicStops = self.arrStops[indexPath.row]
        // lblText.text = self.arrStops[indexPath.row]
        lblText.numberOfLines = 4
        lblText.font = Globals.defaultAppFont(14)
        lblText.textColor = UIColor.black
        return cell!
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch (section){
            
        case 0:
            return DeviceType.IS_IPAD ? 100 : 80
        case 1:
            return DeviceType.IS_IPAD ? self.view.frame.size.height - 340 : DeviceType.IS_IPHONE_X ? self.view.frame.size.height - 285 : self.view.frame.size.height - 250
        case 2:
            return DeviceType.IS_IPAD ? 171 : 114
        default :
            return 70
        }
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        switch(section){
        case 0:
            self.viewDriverBookingShareBtns.translatesAutoresizingMaskIntoConstraints = true
            self.viewDriverBookingShareBtns.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 80 * 1.5)
            return self.viewDriverBookingShareBtns
        case 1:
            self.mapViewDetails.translatesAutoresizingMaskIntoConstraints = true
            var heightView : CGFloat!
            heightView =  self.view.frame.height - (DeviceType.IS_IPAD ? 340 : 250)
            self.mapViewDetails.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: heightView)
            return self.mapViewDetails
        case 2:
            let lblPickUp = self.viewPickUpDropOffDetails.viewWithTag(20) as! UILabel
            
            lblPickUp.text = (self.dicDataSource["pickup"] as! String)
            
            let lblDropOff = self.viewPickUpDropOffDetails.viewWithTag(21) as! UILabel ;
            
            if let strDrop = self.dicDataSource["dropoff"] as? String {
                lblDropOff.text = strDrop
            }else {
                lblDropOff.text = "N/A"
            }
            
            self.viewPickUpDropOffDetails.translatesAutoresizingMaskIntoConstraints = true
            self.viewPickUpDropOffDetails.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 114*1.5)
            
            return  self.viewPickUpDropOffDetails
        default :
            break
            
        }
        return nil
    }
    func setupFareDisTimeViewDetais(){
        //self.viewFareDisTimeDetails.layer.borderColor = UIColor.lightGrayColor().CGColor
        //self.viewFareDisTimeDetails.layer.borderWidth = 1.0
        
        let lblFare = self.viewFareDisTimeDetails.viewWithTag(20) as! UILabel
        
        if var fare = self.dicDataSource["totalfare"] as? String{
            if fare.characters.count > 0 {
                fare = fare.replacingOccurrences(of: ",", with: "")
                lblFare.text =  fare //String(format: "%@ %.2f", appDelegate.currency, fare.toFloat())
            }
            else {
                lblFare.text = "N/A"
            }
        }
        
        let lblDistance = self.viewFareDisTimeDetails.viewWithTag(21) as! UILabel
        lblDistance.text = "N/A"

        if appDelegate.distanceType.contains("km"){
            if let dis = self.dicDataSource["distance_kilometers"]{
                if "\(dis)".characters.count != 0 {
                    lblDistance.text = "\(dis) \(appDelegate.distanceUnit!)"
                }
            }
        }
        else {
            if let dis = self.dicDataSource["distance_miles"]{
                if "\(dis)".characters.count != 0 {
                    lblDistance.text = "\(dis) \(appDelegate.distanceUnit!)"
                }
            }
        }
        let lblTime = self.viewFareDisTimeDetails.viewWithTag(22) as! UILabel
        
        if let time = self.dicDataSource["time"] {
            if "\(time)".isEmpty == true{
                lblTime.text = "N/A"
            }
            else {
                lblTime.text = Globals.sharedInstance.formatArrivalTimeStringWith("\(time)")
            }
        }else {
            lblTime.text = "N/A"
        }
        
        self.viewFareDisTimeDetails.translatesAutoresizingMaskIntoConstraints = true
        self.viewFareDisTimeDetails.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 70)
    }
    
    
    @IBAction func btnDriverDetailsOrShareOrBookingStatusAction(_ sender: UIButton) {
        sender.isSelected = true
        weak var weakSender = sender
        
        if sender.tag == 150 {
            if let driverID = self.dicDataSource["primary_driver"] as? String{
                if  driverID.characters.count > 0 && driverID != "0" {
                    
                    let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "PADriverFeedbackPopupVC") as! PADriverFeedbackPopupVC
                    
                    detailsVc.dicDriverInfo = self.dicDataSource
                    detailsVc.isPresented = true
                    detailsVc.view.backgroundColor = UIColor.clear
                    detailsVc.modalPresentationStyle = .overFullScreen
                    detailsVc.providesPresentationContextTransitionStyle = true
                    detailsVc.definesPresentationContext = true
                    self.providesPresentationContextTransitionStyle = true
                    self.definesPresentationContext = true
                    self.modalTransitionStyle = .crossDissolve
                    self.modalPresentationStyle = .overFullScreen
                    self.present(detailsVc, animated: true) { () -> Void in
                        weakSender?.isSelected = false
                    }
                }
                else {
                    self.showBannerWithMessage("Driver yet not assigned for job")
                  //  Globals.ToastAlertWithString("Driver yet not assigned for job")
                    sender.isSelected = false
                }
            }
            else {
                  self.showBannerWithMessage("Driver yet not assigned for job")
            //    Globals.ToastAlertWithString("Driver yet not assigned for job")
                sender.isSelected = false
            }
        }else if sender.tag == 152 { // Share
            let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "ShareTripDetailsViewController") as! ShareTripDetailsViewController
            detailsVc.strShareType = "trip"
            detailsVc.reservation_id = self.dicDataSource["id"] as! String
            detailsVc.emailBillling = String(describing: self.dicDataSource["billingEmail"]!)
            detailsVc.view.backgroundColor = UIColor.clear
            detailsVc.modalPresentationStyle = .overFullScreen
            detailsVc.providesPresentationContextTransitionStyle = true
            detailsVc.definesPresentationContext = true
            self.providesPresentationContextTransitionStyle = true
            self.definesPresentationContext = true
            self.modalTransitionStyle = .crossDissolve
            self.modalPresentationStyle = .overFullScreen
            self.present(detailsVc, animated: true) { () -> Void in
                weakSender?.isSelected = false
            }
        }else if sender.tag == 151 { // Booking Status
            self.lblPopupTitle.text = "BOOKING STATUS"
            var strStatus = self.dicDataSource["status"] as? String
            if strStatus == nil {
                strStatus = ""
            }
            self.lblPopupDetailsText.text = strStatus == "" ? "N/A \n \n \n " : "\(strStatus!) \n \n \n \n "
            UIView.animate(withDuration: 0.10, delay: 0.10, options:.overrideInheritedCurve, animations: { [weak self] in
                self?.viewPopupDetails.frame.origin.y = 0.0
                self?.viewPopupDetails.isHidden = false
                self?.view.bringSubview(toFront: self!.viewPopupDetails)
                }, completion: { (bol : Bool) in
                    weakSender?.isSelected = false
            })
        }
    }
    
    
    
    @IBAction func btnEstinateFareAction(_ sender: UIButton) {
        
        if let fareDetails = self.dicDataSource["rates"] as? [String:AnyObject]{
            let detailsVc = appDelegate.storybaord.instantiateViewController(withIdentifier: "PAFareBreakDownPopupVC") as! PAFareBreakDownPopupVC
            
            detailsVc.dicFareDetails = fareDetails
            if let fare = self.dicDataSource["rates_arr"] as? [[String:Any]] {
                detailsVc.arrFareDetails = fare
            }
            detailsVc.showdisclaimer = true
            
            if let currency = self.dicDataSource["currency"] as? String {
                detailsVc.currency = currency
            }
            else{
                detailsVc.currency = appDelegate.currency
            }
            if let fare = dicDataSource["totalfare"] as? String{
                if fare.isEmpty == false {
                    //fare = fare.replacingOccurrences(of: ",", with: "")
                    detailsVc.strTotalFare = String(format: "%@",fare)
                }
                else {
                    detailsVc.strTotalFare = "0.0"
                }
            }
            detailsVc.view.backgroundColor = UIColor.clear
            detailsVc.modalPresentationStyle = .overFullScreen
            detailsVc.providesPresentationContextTransitionStyle = true
            detailsVc.definesPresentationContext = true
            self.providesPresentationContextTransitionStyle = true
            self.definesPresentationContext = true
            self.modalTransitionStyle = .crossDissolve
            self.modalPresentationStyle = .overFullScreen
            self.present(detailsVc, animated: true, completion: nil)
        }
        else {
            sender.isSelected = true

            self.lblPopupTitle.text = "ESTIMATED FARE"
            var strFare = self.dicDataSource["totalfare"] as? String
            
            if strFare == nil && strFare == ""{
                strFare = ""
                self.lblPopupDetailsText.text = "N/A \n \n \n "
            }else {
                self.lblPopupDetailsText.attributedText = "".setAttributedStringWithColorSize("\(strFare!) \n \n ", secondStr: "Estimate includes surcharges, fuel and airport fee. Wait time, tolls, parking and local taxes that cannot be calculated prior to service may apply and are excluded. Additional service may change rate.", firstColor: UIColor.appBlackColor(), secondColor: UIColor.darkBlack(), font1: Globals.defaultAppFontWithBold(22), font2: Globals.defaultAppFont(13))
            }
            UIView.animate(withDuration: 0.10, delay: 0.10, options:.overrideInheritedCurve, animations: { [weak self] in
                self?.viewPopupDetails.frame.origin.y = 0.0
                self?.viewPopupDetails.isHidden = false
                let viewWhite = self?.viewPopupDetails.viewWithTag(87)
                self?.viewPopupDetails.bringSubview(toFront: viewWhite!)
                let btnCross = viewWhite?.viewWithTag(88) as! UIButton
                viewWhite!.bringSubview(toFront: btnCross)
                self?.view.bringSubview(toFront: self!.viewPopupDetails)
                }, completion: { (bol : Bool) in
                    sender.isSelected = false
            })
        }
    }
    // MARK:- ===== Rating View Delegate ====
    @IBAction func btnCrossPopupAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.10, animations: { [weak self] in
            self?.viewPopupDetails.frame.origin.y = 5000000.0
            self?.viewPopupDetails.isHidden = true
        }) 
        
    }
    func floatRatingViewAction(_ rating: Double) {
        if appDelegate.networkAvialable == false   {
            return
        }
        appDelegate.showProgressWithText("Please Wait...")
        let userInfo = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
        var dicParameters = [String:Any]()
        dicParameters["driver_id"] = self.dicDataSource["primary_driver"]!
        dicParameters["user_id"] = userInfo["id"]!
        dicParameters["session"] = userInfo["session"]!
        dicParameters["reservation_id"] = self.dicDataSource["id"]!
        dicParameters["rating"] = "" as Any?
        dicParameters["pass_feedback"] = "" as Any?
        dicParameters["avg_rating"] = rating as Any?
        dicParameters["rating_type"] = "single" as Any?
        dicParameters["comment"] = self.txtViewEditableComments.text as Any?
        dicParameters["current"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?
        dicParameters["created_on"] = Globals.sharedInstance.getCurrentDateforOndemandCars() as Any?
        appDelegate.showProgressWithText("Please wait..")
        ServiceManager.sharedInstance.dataTaskWithPostRequest("customerfeedback", dicParameters: dicParameters, successBlock: { (dicData:[String:Any]!) in
            appDelegate.hideProgress()
            if dicData["status"] as! String == "success"{
                Globals.ToastAlertWithString(dicData["message"] as! String)
                
            }else {
                Globals.ToastAlertWithString(dicData["message"] as! String)
            }
        }) { (error) in
            appDelegate.hideProgress()
            Globals.ToastAlertWithString((error?.description)!)
        }
        
        
    }
    
    func backButtonActionMethod(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
