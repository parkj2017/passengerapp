//
//  ReservationTableViewCell.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 03/02/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit

class ReservationTableViewCell: UITableViewCell {
    // MARK:-============ Property decalaration ==========
    
    
    @IBOutlet var titleHeaderDropOffText: UILabel!
    @IBOutlet var lblVehicleType_Name: UILabel!
    @IBOutlet weak var lblBookingNumber: UILabel!
    
    @IBOutlet weak var lblDateOfJob: UILabel!
    
    @IBOutlet weak var lblPickupAddess: UILabel!
    
    @IBOutlet weak var lblJobStatus: UILabel!
    @IBOutlet weak var lblStops: UILabel!
    @IBOutlet weak var lblDropAddress: UILabel!
    
    var fontSizeView : Float = 14.0
    // @IBOutlet weak var lblJobStatus: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
          self.titleHeaderDropOffText.textColor = UIColor.appThemeColor()
        self.lblDateOfJob.font = Globals.defaultAppFont(fontSizeView)
         self.lblPickupAddess.font = Globals.defaultAppFont(fontSizeView)
        self.lblStops.font = Globals.defaultAppFont(fontSizeView)
        self.lblDropAddress.font = Globals.defaultAppFont(fontSizeView)

        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
