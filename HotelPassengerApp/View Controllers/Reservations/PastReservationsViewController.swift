//
//  PastReservationsViewController.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 03/02/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit

class PastReservationsViewController: BaseViewController,BackButtonActionProtocol, UISearchBarDelegate
{
// MARK:-============ Property decalaration ==========

    @IBOutlet weak var tableReservations: UITableView!
    var arrReservationData:[Any]! = []
    
    var arrFilterDataSearch:[Any]! = []
    var loading:Bool! = false
    var page_Number:Int! = 0
    var page_NumberSearch:Int! = 0
    var totalJobRows:Int! = 0
     var totalJobRowsForSearch:Int! = 0
    var selectedIndexPath : IndexPath!
    var shouldShowSearchResults = false
     var dateChanged = true
    var searchTextField : UITextField!
    var searchStrDate : String!
     @IBOutlet var viewDatePicker: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: - =====view life cycle=====
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //self.tableReservations.contentInset = UIEdgeInsetsMake(-5, 0, 0, 0)
        //  self.title = "Past Reservations"
        self.navigationController?.isNavigationBarHidden = false
        
        self.viewDatePicker.frame = CGRect(x: 0, y: self.view.frame.height - 216, width: self.view.frame.width , height: 216)
        self.viewDatePicker.isHidden = true
        (self.viewDatePicker.viewWithTag(100) as! UIDatePicker).maximumDate = Date()
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Past Reservations", imageName: "backArrow")
        getDriverActiveJobsFromDate(false, strDate: "",pageNumber: page_Number)
        self.searchBar.placeholder = "Search With Date"
        self.searchBar.delegate = self
        self.searchBar.tintColor = UIColor.white
        //self.searchBar.showsCancelButton = true
        self.searchBar.backgroundColor = UIColor.appThemeColor()
        self.searchBar.setImage(UIImage(named: "searchIcon"), for: .search, state: UIControlState())
        let viewSearchBar = self.searchBar.subviews[0]
        for searchSubViews in viewSearchBar.subviews {
            if searchSubViews.isKind(of: UITextField.self){
                let textField = searchSubViews as! UITextField
                textField.textColor = UIColor.white
                self.searchTextField = textField

                textField.inputView = self.viewDatePicker
                textField.font = Globals.defaultAppFont(15)
                textField.setValue(UIColor.white, forKeyPath: "_placeholderLabel.textColor")
                (((textField.value(forKey: "textInputTraits"))! as Any) as AnyObject).setValue(UIColor.white, forKeyPath: "insertionPointColor")
                //textField.setValue(UIColor.whiteColor(), forKeyPath: "insertionPointColor")
                textField.backgroundColor = UIColor.clear
                textField.tintColor = UIColor.white
                textField.frame.size.height = 44
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Globals.sharedInstance.delegateBack = self
        if  Constants.reservationUpdated == true {
            self.shouldShowSearchResults = false
            arrFilterDataSearch.removeAll()
            arrReservationData.removeAll()
            Constants.reservationUpdated = false
            getDriverActiveJobsFromDate(false, strDate: "",pageNumber: page_Number)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Globals.sharedInstance.delegateBack = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ====== UITableView Delegate and data Source ==========
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.shouldShowSearchResults == true {
            return arrFilterDataSearch.count
        }else {
            return arrReservationData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier = "PastReservationTableCellIdentifier"
        var cell : ReservationTableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as? ReservationTableViewCell
        if cell == nil {
            cell = UITableViewCell(style:.default, reuseIdentifier: cellIndentifier) as! ReservationTableViewCell
        }
        var reservation_DC: [String : Any]!
        if self.shouldShowSearchResults == true {
            reservation_DC = arrFilterDataSearch[indexPath.row] as! [String : Any]
        }else {
            reservation_DC = arrReservationData[indexPath.row] as! [String : Any]
        }
        let viewPickup = cell.viewWithTag(111)!
        let viewDropp = cell.viewWithTag(112)!
        Globals.layoutViewFor(viewPickup, color: UIColor.darkBlack(), width: 1.5, cornerRadius: 3)
        Globals.layoutViewFor(viewDropp, color: UIColor.appThemeColor(), width: 1.5, cornerRadius: 3)
        Globals.layoutViewFor( cell.lblStops, color: UIColor.appThemeColor(), width: 1.5, cornerRadius: cell.lblStops.frame.size.width / 2.0)
        let strbookingID = String(describing: reservation_DC["conf_id"]!).replacingOccurrences(of: "-", with: "")
        cell.lblBookingNumber.attributedText = strbookingID.setAttributedStringWithColorSize("Booking ID: ", secondStr: strbookingID, firstColor: UIColor.gray, secondColor: UIColor.darkGray, font1: Globals.defaultAppFont(14), font2:Globals.defaultAppFontWithBold(16))
        let date_Time: String! = "\(reservation_DC["pu_date"]!) \(reservation_DC["pu_time"]!)"
        cell.lblDateOfJob.text = Globals.getFormattedDateString(date_Time)

        if let stops = reservation_DC["stops"] as? [[String:Any]]
        {
            if stops.count>0{
                cell.lblStops.attributedText = "".setAttributedStringWithColorSize("\(stops.count) \n ", secondStr: "Stop(s)", firstColor: UIColor.appBlackColor(), secondColor: UIColor.appBlackColor(), font1:Globals.defaultAppFontWithBold(16) ,font2:Globals.defaultAppFont(12))
            }
            else {
                cell.lblStops.attributedText = "".setAttributedStringWithColorSize("No \n ", secondStr: "Stop", firstColor: UIColor.appBlackColor(), secondColor: UIColor.appBlackColor(), font1:Globals.defaultAppFontWithBold(14) ,font2:Globals.defaultAppFont(12))
            }
        }
        else{
            cell.lblStops.attributedText = "".setAttributedStringWithColorSize("No \n ", secondStr: "Stops", firstColor: UIColor.appBlackColor(), secondColor: UIColor.appBlackColor(), font1:Globals.defaultAppFontWithBold(16) ,font2:Globals.defaultAppFont(14))
        }

        cell.lblPickupAddess.text = reservation_DC["pickup"] as? String
        let dropStr = reservation_DC["dropoff"] as? String
        cell.lblDropAddress.text = dropStr == "" ? "N/A" : dropStr!
 
        cell.accessoryView?.backgroundColor = UIColor.appThemeColor()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return  Constants.iPadScreen == true ? 140.0 * 1.5 : 140.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
        if self.shouldShowSearchResults == true {
            if self.arrFilterDataSearch.count != 0 {
                self.performSegue(withIdentifier: "ReservationDetail", sender: self)
            }
        }
        else {
            if self.arrReservationData.count != 0 {
                self.performSegue(withIdentifier: "ReservationDetail", sender: self)
            }
        }
        tableReservations.deselectRow(at: indexPath, animated: true)
    }
    
// MARK: - ======= Pagination of table view rows==========
    
func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    
            if !self.loading {
                let endScrolling:CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height
                if endScrolling >= scrollView.contentSize.height {
                    self.loading = true
                    self.loadDataDelayed()
                }
            }
    
    }
    
func loadDataDelayed() {
       if self.shouldShowSearchResults == true {
            if totalJobRowsForSearch > arrFilterDataSearch.count {
                let page: Double = ceil(Double(arrFilterDataSearch.count)/8.0)
                page_NumberSearch = Int(page) + 1
                if dateChanged == true {
                     page_NumberSearch = 0
                    getDriverActiveJobsFromDate(true, strDate: self.searchStrDate, pageNumber: page_NumberSearch)
                }else {
                    getDriverActiveJobsFromDate(true, strDate: self.searchStrDate, pageNumber: page_NumberSearch)
                }
              
            }
        } else {
            if totalJobRows > arrReservationData.count {
                let page: Double = ceil(Double(arrReservationData.count)/8.0)
                getReservation(Int(page) + 1)
            }
        }
        
    }
    
    func getReservation(_ page: Int) {
        page_Number = page
        getDriverActiveJobsFromDate(false, strDate: "", pageNumber: page_Number)
    }
    
// MARK: - ========= Webservice Call to get past reservartions ========
    
func getDriverActiveJobsFromDate(_ flagSearch:Bool, strDate:String, pageNumber:Int)
{
    if (appDelegate.networkAvialable == true) {
        appDelegate.showProgressWithText("Fetching Reservations")
        //UIApplication.sharedApplication().delegate as! AppDelegate.showProgressWithText("Updating Profile...")
        var dicParameters : [String:Any]! = Dictionary()
        
        
        if Constants.isBookerLogin == true{
             dicParameters["user_id"] = "\(appDelegate.activeUserDetailsForBooker["id"]!)"
            dicParameters["is_booker"] = "yes"
            

        }else {
            let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
            let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
            
            dicParameters["user_id"] = user_ID
            dicParameters["session"] = session
            dicParameters["is_booker"] = "no"

        }
        dicParameters["status"] = "inactive" as Any?
        dicParameters["page"] = pageNumber as Any?
        
        if flagSearch == true{
            dicParameters["date_type"] = "single" as Any?
            dicParameters["from"] = strDate as Any?
            dicParameters["to"] = strDate as Any?
        }
        
        dicParameters["company_id"] = Constants.CompanyID as Any?
        weak var weakSelf = self
        ServiceManager.sharedInstance.dataTaskWithPostRequest("get_reservation", dicParameters: dicParameters, successBlock:
            { (dicRecievedJSON) -> Void in
                if dicRecievedJSON != nil
                {
                    var dict_Response:[String:Any]!
                    if dicRecievedJSON["status"] as! String == "success" {
                        dict_Response = dicRecievedJSON.formatDictionaryForNullValues(dicRecievedJSON)
                        if let totalRows = dict_Response["total_rows"]{
                            if "\(totalRows)".toInteger() > 0{
                                
                                let id_String: String = dict_Response["record_ids"] as! String
                            var record_idsArray: [Any] = id_String.components(separatedBy: ",")
                            var dictValue: [AnyHashable: Any] = (dict_Response["records"] as! [AnyHashable: Any])
                            if dictValue.count > 0 {
                                if weakSelf?.shouldShowSearchResults == true {
                                    if weakSelf?.arrFilterDataSearch == nil {
                                        weakSelf?.arrFilterDataSearch = [Any]()
                                    }
                                    else {
                                        if weakSelf?.dateChanged == true{
                                        weakSelf?.arrFilterDataSearch .removeAll()
                                        }
                                        
                                    }
                                    for i in 0 ..< record_idsArray.count {
                                        let reservation_DC: [AnyHashable: Any] = (dictValue[record_idsArray[i] as! NSObject] as! [AnyHashable: Any])
                                        weakSelf?.arrFilterDataSearch.append(reservation_DC)
                                        
                                    }
                                    weakSelf?.dateChanged = false
                                    weakSelf?.totalJobRowsForSearch = dict_Response["total_rows"] as! Int
                                    weakSelf?.loading = false
                                    weakSelf?.tableReservations.reloadData()
                                    weakSelf?.searchBar.text = ""
                                    weakSelf?.searchBar.resignFirstResponder()
                                    weakSelf?.viewDatePicker.isHidden = true
                                }else {
                                    if weakSelf?.arrReservationData == nil {
                                        weakSelf?.arrReservationData = [Any]()
                                    }
//                                    else {
//                                        weakSelf?.arrReservationData .removeAll()
//                                    }
                                    for i in 0 ..< record_idsArray.count {
                                        let reservation_DC: [AnyHashable: Any] = (dictValue[record_idsArray[i] as! NSObject] as! [AnyHashable: Any])
                                        weakSelf?.arrReservationData.append(reservation_DC)
                                        
                                    }
                                    if weakSelf?.arrReservationData.count == 0 {
                                        Globals.ToastAlertWithString("No past record found")
                                    }
                                    else {
                                       
                                        weakSelf?.totalJobRows = dict_Response["total_rows"] as! Int
                                        weakSelf?.loading = false
                                    DispatchQueue.main.async(execute: { () -> Void in
                                        weakSelf?.tableReservations.reloadData()

                                    })
                                  }
                                }
                            }
                        }
                        else {
                            if weakSelf?.shouldShowSearchResults == true {
                                Globals.ToastAlertWithString("No Records found")
                                weakSelf?.shouldShowSearchResults = true
                                
                            }else {
                                Globals.ToastAlertWithString("No past record found")
                            }
                            weakSelf?.tableReservations.reloadData()
                        }
                      }
                    }
                    else {
                        if weakSelf?.shouldShowSearchResults == true {
                            Globals.ToastAlertWithString("No Records found")
                            weakSelf?.shouldShowSearchResults = true
                        }else {
                            Globals.ToastAlertWithString("No past record found")
                        }
                            weakSelf?.tableReservations.reloadData()
                    }
                    
                    (UIApplication.shared.delegate! as! AppDelegate).hideProgress()
                }
            }) { (error) -> () in
                (UIApplication.shared.delegate! as! AppDelegate).hideProgress()
                Globals.ToastAlertForNetworkConnection()
                weakSelf?.loading = false
                print(error)
        }
        
    }else {
        self.loading = false
        let alert = self.alertView("Past Reservations", message:"Please check your network connection", delegate:nil, cancelButton:"Ok", otherButons:nil)
        alert?.show()
    }
    }
    

// MARK: - ======Search Bar Delegate =====
func  searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool{
        self.viewDatePicker.isHidden = false
        self.shouldShowSearchResults = true
      self.searchBar.showsCancelButton = true
     self.tableReservations.reloadData()
        return true
}
func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
{
        self.shouldShowSearchResults = true
        self.searchBar.resignFirstResponder()
        self.tableReservations.reloadData()
        
}
func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
{
        self.shouldShowSearchResults = false
        self.searchBar.resignFirstResponder()
        self.searchBar.text = ""
        self.tableReservations.reloadData()
        self.arrFilterDataSearch.removeAll(keepingCapacity: true)
        self.page_NumberSearch = 0
        self.loading = false
}
func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
{
//        self.arrFilterDataSearch.removeAll(keepCapacity: true)
//        let searchPredicate = NSPredicate(format: "conf_id CONTAINS[C]%@",searchText)
//        let arraySearch = (self.arrReservationData as NSArray).filteredArrayUsingPredicate(searchPredicate)
//        print("search Result ---\(arraySearch)")
//        self.arrFilterDataSearch = arraySearch
//        self.tableReservations.reloadData()
    
}
@IBAction func datePickerValueChangedAction(_ sender: UIDatePicker) {
    let  formatter: DateFormatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    self.searchTextField.text = formatter.string(from: sender.date)
    self.searchStrDate = formatter.string(from: sender.date)
}
@IBAction func btnDonePickUpDateAction(_ sender: UIBarButtonItem) {
    
  //  self.searchBar.resignFirstResponder()
   //  self.viewDatePicker.hidden = true
    if sender.tag == 20{
    if self.arrFilterDataSearch != nil {
       self.arrFilterDataSearch.removeAll(keepingCapacity: true)
    }
    if let _ = self.searchStrDate {
        self.getDriverActiveJobsFromDate(true, strDate: self.searchStrDate,pageNumber: 0)
    }else {
        let  formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
      self.searchStrDate = formatter.string(from: Date())
    self.getDriverActiveJobsFromDate(true, strDate: self.searchStrDate,pageNumber: 0)
    }
  
    // self.viewDatePicker.hidden = true
    self.dateChanged = true
    }
    else {
        self.shouldShowSearchResults = false
        self.tableReservations.reloadData()
        self.view.endEditing(true)
    }
}
    
// MARK:- === Navigation==========
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ReservationDetail" {
            let reserDetailsVC = segue.destination as! PAReservationsDetailsVC
            if self.shouldShowSearchResults == true {
                reserDetailsVC.dicDataSource = self.arrFilterDataSearch[self.selectedIndexPath.row] as! [String:Any]
            }else {
                reserDetailsVC.dicDataSource = self.arrReservationData[self.selectedIndexPath.row] as! [String:Any]
            }
            reserDetailsVC.isPastReservation = true
            
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    // MARK:---======bak button======
func backButtonActionMethod(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
