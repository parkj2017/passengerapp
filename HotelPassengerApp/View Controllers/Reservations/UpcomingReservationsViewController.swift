//
//  UpcomingReservationsViewController.swift
//  PassangerApp
//
//  Created by Gulraj Grewal on 03/02/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import UIKit

enum kFilterType : Int {
    case today
    case tomorrow
    case week
    case all
    case search
}

class UpcomingReservationsViewController: BaseViewController,BackButtonActionProtocol, UISearchBarDelegate{

// MARK:-============ Property decalaration ==========
    
    @IBOutlet weak var viewFilters: UIView!
    @IBOutlet weak var tableReservations: UITableView!
    //var customSearchController: CustomSearchController!
    
    @IBOutlet weak var searchBar: UISearchBar!
    var arrReservationData:[Any]! = []
     var arrFilterDataSearch:[Any]! = []
     var selectedIndexPath : IndexPath!
    var page_Number:Int! = 0
    var totalJobRows:Int! = 0
    var loading:Bool! = false
    var page_NumberSearch:Int! = 0
    // var page_NumberSearch:Int! = 0
    var shouldShowSearchResults = false
     var dateChanged = true
    var filterType:kFilterType!
    var pickerDate : Date!
     var searchTextField : UITextField!
    var totalJobRowsForSearch:Int! = 0
    var searchStrDate : String!
    @IBOutlet var viewDatePicker: UIView!
    // MARK: - =====view life cycle=====
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.viewDatePicker.frame = CGRect(x: 0, y: self.view.frame.height - 216, width: self.view.frame.width , height: 216)
        self.viewDatePicker.isHidden = true
        (self.viewDatePicker.viewWithTag(100) as! UIDatePicker).minimumDate = Date()
        page_Number = 0
       // self.title = "Upcoming Reservations"
        self.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Upcoming Reservations", imageName: "backArrow")
        for view in viewFilters.subviews as! [UIButton] {
            
            view.isSelected = false
            view.backgroundColor = UIColor.lightBlack()

//            if view.tag == 1000 {
//                view.isSelected = true
//                view.backgroundColor = UIColor.darkBlack()
//              }else {
//                         }
        }
        Constants.reservationUpdated = false
        filterType = .all
        filterResultsForType(filterType)
        self.searchBar.placeholder = "Search with date"
        self.searchBar.setImage(UIImage(named: "searchIcon"), for: .search, state: UIControlState())
        self.searchBar.delegate = self
        self.searchBar.tintColor = UIColor.white
        self.searchBar.backgroundColor = UIColor.appThemeColor()
        self.searchBar.showsCancelButton = true
        let viewSearchBar = self.searchBar.subviews[0]
        for searchSubViews in viewSearchBar.subviews {
            if searchSubViews.isKind(of: UITextField.self){
                let textField = searchSubViews as! UITextField
                textField.textColor = UIColor.white
                 self.searchTextField = textField
                textField.inputView = self.viewDatePicker
                  textField.font = Globals.defaultAppFont(15)
                textField.setValue(UIColor.white, forKeyPath: "_placeholderLabel.textColor")
                (((textField.value(forKey: "textInputTraits"))! as Any) as AnyObject).setValue(UIColor.white, forKeyPath: "insertionPointColor")
                //textField.setValue(UIColor.whiteColor(), forKeyPath: "insertionPointColor")
                textField.backgroundColor = UIColor.clear
                textField.tintColor = UIColor.white
                textField.frame.size.height = 44
            }
        }
        

}
    
    func reloadJobs()  {
        filterType = .today
        filterResultsForType(filterType)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpcomingReservationsViewController.reloadJobs), name: NSNotification.Name(rawValue: Constants.kUpdateJobListNotification), object: nil)

        Globals.sharedInstance.delegateBack = self
        if  Constants.reservationUpdated == true {
            Constants.reservationUpdated = false
            arrFilterDataSearch.removeAll()
            arrReservationData.removeAll()
            //filterType = .today
            filterResultsForType(filterType)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.kUpdateJobListNotification), object: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //    func configureCustomSearchController() {
    //
    //        self.customSearchController = CustomSearchController(searchResultsController: self, searchBarFrame: CGRectMake(0.0, 67.0, self.view.frame.size.width, 50.0), searchBarFont: UIFont(name: "Oxygen", size: 18.0)!, searchBarTextColor: UIColor.DefaultGreenColor(), searchBarTintColor: UIColor.whiteColor())
    //        self.customSearchController.customSearchBar.placeholder = "Search"
    //        self.tableReservations.tableHeaderView = customSearchController.customSearchBar
    //        customSearchController.customDelegate = self
    //
    //    }
    
    
    
    
    
// MARK:-============ Reset date formats ==========
    
func resetFromDate(_ currentDate: Date) -> Date {
        let cal: Calendar = Calendar.current
        var comps:DateComponents = (cal as NSCalendar).components([.year, .month, .day, .hour, .minute, .second], from: currentDate)
        comps.hour = 0
        comps.minute = 0
        comps.second = 0
        let today: Date = cal.date(from: comps)!
        return today
    }
  
// MARK:-============ button action filter webcalls for today tommorow ==========
func filterResultsForType(_ filterType: kFilterType) {
    
//        self.arrFilterDataSearch.removeAll()
//        self.arrReservationData.removeAll()
        self.tableReservations.reloadData()
    
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd "
        var currentDate: Date? = nil
        var nextDate: Date? = nil
        switch filterType {
        case .today:
            currentDate = self.resetFromDate(Date())
            self.getDriverActiveJobsFromDate(formatter.string(from: currentDate!), toDate: formatter.string(from: currentDate!), date_type: "single")
            
        case .tomorrow:
            currentDate = self.resetFromDate(Date()).addingTimeInterval(24 * 60 * 60)
            self.getDriverActiveJobsFromDate(formatter.string(from: currentDate!), toDate: formatter.string(from: currentDate!), date_type: "single")
            
        case .week:
            currentDate = self.resetFromDate(Date())
            nextDate = self.resetFromDate(Date()).addingTimeInterval(7 * 24 * 60 * 60)
            self.getDriverActiveJobsFromDate(formatter.string(from: currentDate!), toDate: formatter.string(from: nextDate!), date_type: "week")
            
        case .all:
            currentDate = self.resetFromDate(Date())
            self.getDriverActiveJobsFromDate(formatter.string(from: currentDate!), toDate: "", date_type: "")
            
        case .search:
                self.arrFilterDataSearch.removeAll(keepingCapacity: true)
            if let _ = self.pickerDate {
              currentDate = self.resetFromDate(pickerDate)
            }else {
                 currentDate = self.resetFromDate(Date())
            }
                self.getDriverActiveJobsFromDate(formatter.string(from: currentDate!), toDate: formatter.string(from: currentDate!), date_type: "single")
        }
   
    }
    
@IBAction func filterButtonPressed(_ sender: UIButton) {
    
    DispatchQueue.main.async(execute: { [weak self]() -> Void in
        for view in self?.viewFilters.subviews as! [UIButton] {
            if view.tag == sender.tag {
                view.isSelected = true
                view.backgroundColor = UIColor.darkBlack()
            }
            else {
                view.isSelected = false
                  view.backgroundColor = UIColor.lightBlack()
            }
        }
        })
        if self.arrReservationData == nil {
            self.arrReservationData = [Any]()
        }
        else {
            self.arrReservationData.removeAll()
        }
    self.shouldShowSearchResults = false
    self.searchBar.resignFirstResponder()
    self.searchBar.text = ""
    self.loading = false
        switch sender.tag {
        case 1000:
            filterType = .today
            filterResultsForType(filterType)
            
        case 1001:
            filterType = .tomorrow
            filterResultsForType(filterType)
            
        case 1002:
            filterType = .week
            filterResultsForType(filterType)
            
        default:
            break
        }
}
    
    // MARK:-====== UITableView Delegate and data Source ==========
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.shouldShowSearchResults == true {
            return arrFilterDataSearch.count
        }else {
            return arrReservationData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier = "ReservationTableCellIdentifier"
        var cell : ReservationTableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as? ReservationTableViewCell
        if cell == nil {
            cell = UITableViewCell(style:.default, reuseIdentifier: cellIndentifier) as! ReservationTableViewCell
        }
        var reservation_DC: [String : Any]!
        if self.shouldShowSearchResults == true {
            reservation_DC = arrFilterDataSearch[indexPath.row] as! [String : Any]
        }else {
            reservation_DC = arrReservationData[indexPath.row] as! [String : Any]
        }
        let viewPickup = cell.viewWithTag(111)!
        let viewDropp = cell.viewWithTag(112)!
        Globals.layoutViewFor(viewPickup, color: UIColor.darkBlack(), width: 1.5, cornerRadius: 3)
        Globals.layoutViewFor(viewDropp, color: UIColor.appThemeColor(), width: 1.5, cornerRadius: 3)
         Globals.layoutViewFor( cell.lblStops, color: UIColor.appThemeColor(), width: 1.5, cornerRadius: cell.lblStops.frame.size.width / 2.0)
        let strbookingID = String(describing: reservation_DC["conf_id"]!).replacingOccurrences(of: "-", with: "")
        cell.lblBookingNumber.attributedText = strbookingID.setAttributedStringWithColorSize("Booking ID: ", secondStr: strbookingID, firstColor: UIColor.gray, secondColor: UIColor.darkGray, font1: Globals.defaultAppFont(14), font2:Globals.defaultAppFontWithBold(16))
       
        
        let date_Time: String! = "\(reservation_DC["pu_date"]!) \(reservation_DC["pu_time"]!)"
        cell.lblDateOfJob.text = Globals.getFormattedDateString(date_Time)
        
        
        if let stops = reservation_DC["stops"] as? [[String:Any]]
        {
            if stops.count>0{
                cell.lblStops.attributedText = "".setAttributedStringWithColorSize("\(stops.count) \n ", secondStr: "Stops", firstColor: UIColor.appBlackColor(), secondColor: UIColor.appBlackColor(), font1:Globals.defaultAppFontWithBold(16) ,font2:Globals.defaultAppFont(14))
            }
            else {
             cell.lblStops.attributedText = "".setAttributedStringWithColorSize("No \n ", secondStr: "Stops", firstColor: UIColor.appBlackColor(), secondColor: UIColor.appBlackColor(), font1:Globals.defaultAppFontWithBold(16) ,font2:Globals.defaultAppFont(14))
            }
        }
        else{
            cell.lblStops.attributedText = "".setAttributedStringWithColorSize("No \n ", secondStr: "Stops", firstColor: UIColor.appBlackColor(), secondColor: UIColor.appBlackColor(), font1:Globals.defaultAppFontWithBold(16) ,font2:Globals.defaultAppFont(14))
        }
        cell.lblPickupAddess.text = reservation_DC["pickup"] as? String
        let dropStr = reservation_DC["dropoff"] as? String
        cell.lblDropAddress.text = dropStr == "" ? "N/A" : dropStr!
        if let vehicleName = reservation_DC["vehicle_type_title"] as? String {
            cell.lblVehicleType_Name.text = "Vehicle: \(vehicleName)"
        }
/*
        cell.reservationID.text = "Booking ID: ".stringByAppendingString(reservation_DC["conf_id"]!.stringByReplacingOccurrencesOfString("-", withString: ""))
        
        let date_Time: String! = "\(reservation_DC["pu_date"]!) \(reservation_DC["pu_time"]!)"
        
        var arrDateComp: [Any] = Globals.sharedInstance.getdateComponentsforDate(date_Time)
        cell.date.text = "\(arrDateComp[0])"
        cell.month.text = "\(arrDateComp[1])"
        cell.time.text = "\(arrDateComp[2])"
        
        if let stops = reservation_DC["stops"] as? [String]
        {
            if stops.count>0{
                cell.lblStops.text = "\(stops.count)"
            }
            else {
                cell.lblStops.text = "0"
            }
        }
        
        cell.pickUp.text = reservation_DC["pickup"] as? String
        cell.dropOff.text = reservation_DC["dropoff"] as? String
        
        if let childSeat:String = "\(reservation_DC["child_seat_count"]!)" {
            cell.lblBaby.text = (childSeat == "0" ) ? "0" : childSeat
        }else {
            cell.lblBaby.text = "0"
        }
        
        if var childSeat = reservation_DC["no_of_pax"] as? String {
            childSeat = childSeat == "" ? "0" : childSeat
            cell.lblPassenger.text = childSeat
        }else {
            cell.lblPassenger.text = "0"
        }
        var driverName:String! = ""
        if let firstName = reservation_DC["first_name"] as? String{
            driverName = driverName.stringByAppendingString(firstName)
        }
        if let lastName = reservation_DC["last_name"] as? String{
            driverName = driverName.stringByAppendingString(" ".stringByAppendingString(lastName))
        }
        
        
        if driverName.characters.count>1{
            cell.lblPassName.text = "Driver Name: ".stringByAppendingString(driverName)
        }
        else {
            cell.lblPassName.text = "Driver Name: Not Assigned"
        }
        */
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return Constants.iPadScreen == true ? 240 : 180.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
         self.selectedIndexPath = indexPath
        if self.shouldShowSearchResults == true {
            if self.arrFilterDataSearch.count != 0 {
                 self.performSegue(withIdentifier: "ReservationDetail", sender: self)
            }
        }
        else {
            if self.arrReservationData.count != 0 {
                self.performSegue(withIdentifier: "ReservationDetail", sender: self)
            }
        }
        tableReservations.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK:-======= Pagination of table view rows==========
    
func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.shouldShowSearchResults == false {
        if !self.loading {
            let endScrolling:CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height
            if endScrolling >= scrollView.contentSize.height {
                self.loading = true
                self.loadDataDelayed()
            }
          }
        }
    }
    
    func loadDataDelayed() {
        if self.shouldShowSearchResults == true {
            if totalJobRowsForSearch > arrFilterDataSearch.count {
                let page: Double = ceil(Double(arrFilterDataSearch.count)/8.0)
                page_NumberSearch = Int(page) + 1
                if dateChanged == true {
                    page_NumberSearch = 0
                     getReservation(Int(page) + 1)
                }else {
                     getReservation(Int(page) + 1)
                }
                
            }
        } else {
            if totalJobRows > arrReservationData.count {
                let page: Double = ceil(Double(arrReservationData.count)/8.0)
                getReservation(Int(page) + 1)
            }
        }
    }
    
    func getReservation(_ page: Int) {
        page_Number = page
        filterResultsForType(filterType)
    }
    
    // MARK: - ========= Webservice Call to get past reservartions ========
    
    func getDriverActiveJobsFromDate(_ dateType: String, toDate: String, date_type:String) {
        if (appDelegate.networkAvialable == true) {
            appDelegate.showProgressWithText("Fetching Reservations")
            //UIApplication.sharedApplication().delegate as! AppDelegate.showProgressWithText("Updating Profile...")
            var dicParameters : [String:Any]! = Dictionary()
            
            
            if Constants.isBookerLogin == true
            {
                dicParameters["user_id"] = "\(appDelegate.activeUserDetailsForBooker["id"]!)"
                dicParameters["is_booker"] = "yes"
            }
            else
            {
                let user_ID  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).id")
                let session  = Globals.sharedInstance.getValueFromUserDefaultsForKey_Path("\(Constants.ActiveUserInfo).session")
                dicParameters["user_id"] = user_ID
                dicParameters["session"] = session
                dicParameters["is_booker"] = "no"

            }
            
            dicParameters["status"] = "active" as Any?
            if shouldShowSearchResults == true {
                dicParameters["page"] = page_NumberSearch as Any?
            }else {
              dicParameters["page"] = page_Number as Any?
            }
           
            if (toDate == dateType) {
                dicParameters["date_type"] = date_type//"single" as Any?
             }
            else {
                dicParameters["date_type"] = date_type//"week" as Any?
             }
            
            dicParameters["from"] = dateType as Any?
            dicParameters["to"] = toDate as Any?

            dicParameters["company_id"] = Constants.CompanyID as Any?
            weak var weakSelf = self
            if page_Number == 0{
            self.arrFilterDataSearch.removeAll()
            self.arrReservationData.removeAll()
            self.tableReservations.reloadData()
            }
            ServiceManager.sharedInstance.dataTaskWithPostRequest("get_reservation", dicParameters: dicParameters, successBlock:
                { (dicRecievedJSON) -> Void in
                    if dicRecievedJSON != nil
                    {
                        var dict_Response:[String:Any]!
                        
                        if dicRecievedJSON["status"] as! String == "success" {
                            dict_Response = dicRecievedJSON.formatDictionaryForNullValues(dicRecievedJSON)
                            if let totalRows = dict_Response["total_rows"]{
                                if "\(totalRows)".toInteger() > 0{
                                let id_String: String = dict_Response["record_ids"] as! String
                                var record_idsArray: [Any] = id_String.components(separatedBy: ",")
                                var dictValue: [AnyHashable: Any] = (dict_Response["records"] as! [AnyHashable: Any])
                                if dictValue.count > 0 {
                                    if weakSelf?.shouldShowSearchResults == true {
                                        if weakSelf?.arrFilterDataSearch == nil {
                                            weakSelf?.arrFilterDataSearch = [Any]()
                                        }
                                        if weakSelf?.dateChanged == true{
                                            weakSelf?.arrFilterDataSearch .removeAll()
                                        }
                                        for i in 0 ..< record_idsArray.count {
                                            let reservation_DC: [AnyHashable: Any] = (dictValue[record_idsArray[i] as! NSObject] as! [AnyHashable: Any])
                                            weakSelf?.arrFilterDataSearch.append(reservation_DC)
                                            
                                        }
                                         weakSelf?.dateChanged = false
                                        weakSelf?.searchBar.text = ""
                                        weakSelf?.searchBar.resignFirstResponder()
                                        weakSelf?.viewDatePicker.isHidden = true
                                    }else {
                                    if weakSelf?.arrReservationData == nil {
                                        weakSelf?.arrReservationData = [Any]()
                                    }
                                    for i in 0 ..< record_idsArray.count {
                                        let reservation_DC: [AnyHashable: Any] = (dictValue[record_idsArray[i] as! NSObject] as! [AnyHashable: Any])
                                        weakSelf?.arrReservationData.append(reservation_DC)
                                        
                                    }
                                }
                            }
                            }else {
                                Globals.ToastAlertWithString("No future Trip found")
                                }
                                
                          }
                        else {
                            Globals.ToastAlertWithString("No future Trip found")
                            }
                            
                            weakSelf?.totalJobRows = dict_Response["total_rows"] as! Int
                            weakSelf?.loading = false
                            weakSelf?.tableReservations.reloadData()
                            
                        }
                        (UIApplication.shared.delegate! as! AppDelegate).hideProgress()
                    }
                }) { (error) -> () in
                    (UIApplication.shared.delegate! as! AppDelegate).hideProgress()
                    print(error)
            }
            
        }else {
            let alert = self.alertView("Upcoming Reservations", message:"Please check your network connection", delegate:nil, cancelButton:"Ok", otherButons:nil)
            alert?.show()
        }
    }
    

  
// MARK: - ======Search Bar Delegate =====
    
func  searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool{
        self.shouldShowSearchResults = true
    self.viewDatePicker.isHidden = false
      self.tableReservations.reloadData()
        return true
}
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.shouldShowSearchResults = true
        self.searchBar.resignFirstResponder()
        self.tableReservations.reloadData()
        
    }
func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
{
        self.shouldShowSearchResults = false
        self.searchBar.resignFirstResponder()
        self.searchBar.text = ""
        self.tableReservations.reloadData()
        self.arrFilterDataSearch.removeAll(keepingCapacity: true)
        self.loading = false
}
func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
{
//        self.arrFilterDataSearch.removeAll(keepCapacity: true)
//        let searchPredicate = NSPredicate(format: "conf_id CONTAINS[C]%@",searchText)
//        let arraySearch = (self.arrReservationData as NSArray).filteredArrayUsingPredicate(searchPredicate)
//        print("search Result ---\(arraySearch)")
//        self.arrFilterDataSearch = arraySearch
//        self.tableReservations.reloadData()
    
}
@IBAction func datePickerValueChangedAction(_ sender: UIDatePicker) {
        let  formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        self.searchTextField.text = formatter.string(from: sender.date)
      self.searchStrDate = formatter.string(from: sender.date)
        self.pickerDate = sender.date
}
    @IBAction func btnCancelPickupDateAction(_ sender: UIBarButtonItem) {
        self.dateChanged = false
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        self.viewDatePicker.isHidden = true

    }
@IBAction func btnDonePickUpDateAction(_ sender: UIBarButtonItem) {
    
    if let _ = self.searchStrDate {
        
    }else {
        let  formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        self.searchStrDate = formatter.string(from: Date())
     }
       
        self.dateChanged = true
         filterType = .search
         filterResultsForType(filterType)
}
  
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
        let reserDetailsVC = segue.destination as! PAUpcomingReservationDetailsVC
       if self.shouldShowSearchResults == true {
        if self.arrFilterDataSearch.count != 0 {
        reserDetailsVC.dicDataSource = self.arrFilterDataSearch[self.selectedIndexPath.row] as! [String:Any]
        }
       }
       else {
        if self.arrReservationData.count != 0 {
             reserDetailsVC.dicDataSource = self.arrReservationData[self.selectedIndexPath.row] as! [String:Any]
        }
        }
    
    reserDetailsVC.didFinishCancellingReservation = {[weak self] () -> () in
  
        }
       reserDetailsVC.navigationItem.leftBarButtonItem = Globals.sharedInstance.leftBackButtonWithCustom("Upcoming Reservations", imageName: "backArrow")
    }
    // MARK:---======back button -========
    func backButtonActionMethod(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
