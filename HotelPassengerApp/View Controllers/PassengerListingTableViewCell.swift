//
//  PassengerListingTableViewCell.swift
//  HotelPassengerApp
//
//  Created by Netquall on 16/08/17.
//  Copyright © 2017 Netquall. All rights reserved.
//

import UIKit

protocol passengerListingaActionDelegate {
    
    func btnDeleteActionPerformed(_ sender: UIButton)
    func btnDashboardActionPerformed(_ sender: UIButton)
    func btnViewActionPerformed(_ sender: UIButton)
    func btnEmailActionPerformed(_ sender: UIButton)
}

enum listType {
    case K_TYPE_USER_LIST
    case K_TYPE_PAID_UNPAID_LIST
}

class PassengerListingTableViewCell: UITableViewCell {
    @IBOutlet weak var txtViewdetails: UITextView!

    var k_listing_type:listType = .K_TYPE_USER_LIST
    
    @IBOutlet weak var btn_option1: UIButton!
    @IBOutlet weak var btn_option2: UIButton!
    var AccountListingDelegate:passengerListingaActionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
    }

    func setUpcellWithCellData(cellData:[[String:Any]]) {
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        
        // Configure the view for the selected state
    }
    
    func setContentForTextView(value:[[String:Any]]){
        
        let str_combine_attibuted: NSMutableAttributedString = NSMutableAttributedString()
        for  (index, val) in value.enumerated() {
            
            var key_star: String = ""
            var val_str: String = ""
            var fontColor:UIColor?
            
            if index == 0 {
                key_star = "\(val.keys.first!): ".trimmingCharacters(in:NSCharacterSet.whitespaces)
                val_str = " \(val.values.first!)\n".trimmingCharacters(in:NSCharacterSet.whitespaces)
                fontColor = UIColor.appThemeColor()
            }
            else {
                key_star = "\(val.keys.first!): ".trimmingCharacters(in:NSCharacterSet.whitespaces)
                val_str = " \(val.values.first!)\n".trimmingCharacters(in:NSCharacterSet.whitespaces)
                fontColor = UIColor.darkGray
            }
            
            let title_attri = [NSFontAttributeName: Globals.defaultAppFont(16), NSForegroundColorAttributeName: UIColor.black]
            
            let str_title: NSMutableAttributedString = NSMutableAttributedString(string: key_star, attributes: title_attri)
            str_combine_attibuted.append(str_title)
            
            
            let values_Attri = [NSFontAttributeName: Globals.defaultAppFont(16), NSForegroundColorAttributeName: fontColor!] as [String : Any]
            let str_value: NSMutableAttributedString = NSMutableAttributedString(string: val_str, attributes: values_Attri)
            
            str_combine_attibuted.append(str_value)
            
        }
       self.txtViewdetails.attributedText = str_combine_attibuted
        self.txtViewdetails.setContentOffset(CGPoint.zero, animated: false)
        switch k_listing_type {
        case .K_TYPE_USER_LIST:
            self.btn_option1.setImage(UIImage(named: "Delete_ac"), for: .normal)
            self.btn_option2.setImage(UIImage(named: "Dashboard"), for: .normal)
            break
            
        case .K_TYPE_PAID_UNPAID_LIST:
            self.btn_option1.setImage(UIImage(named: "View"), for: .normal)
            self.btn_option2.setImage(UIImage(named: "Email"), for: .normal)
            break
            
        default: break
            
        }
        

        self.updateConstraintsIfNeeded()
        self.superview?.updateConstraintsIfNeeded()
    }

    @IBAction func btn_one_action(_ sender: UIButton) {
        switch k_listing_type {
        case .K_TYPE_USER_LIST:
            self.AccountListingDelegate?.btnDeleteActionPerformed(sender)
            break

        case .K_TYPE_PAID_UNPAID_LIST:
            self.AccountListingDelegate?.btnViewActionPerformed(sender)
            break
            
        default: break
            
        }
    }
    @IBAction func btn_two_action(_ sender: UIButton) {
        switch k_listing_type {
        case .K_TYPE_USER_LIST:
            self.AccountListingDelegate?.btnDashboardActionPerformed(sender)
            break
            
        case .K_TYPE_PAID_UNPAID_LIST:
             self.AccountListingDelegate?.btnEmailActionPerformed(sender)
            break
            
         default: break
            
        }

    }
}
