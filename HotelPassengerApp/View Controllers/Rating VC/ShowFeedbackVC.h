//
//  ShowFeedbackVC.h
//  RideApplication
//
//  Created by Gulraj Grewal on 2/21/15.
//  Copyright (c) 2015 BlackBirdWorldWide. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLStarRatingControl.h"

@interface ShowFeedbackVC :UIViewController
{
    IBOutlet UITextView *txt_View;
    IBOutlet UIScrollView *scroll_View;
    
}
@property (nonatomic, strong)  NSString *reservationId;
@property (nonatomic, strong)  NSString *driverID;


@end
