//
//  ShowFeedbackVC.m
//  RideApplication
//
//  Created by Gulraj Grewal on 2/21/15.
//  Copyright (c) 2015 BlackBirdWorldWide. All rights reserved.
//

#import "ShowFeedbackVC.h"

//#import <JLToast/JLToast.h>
#import "HotelPassengerApp-Swift.h"

@interface ShowFeedbackVC ()
{
    NSString *arrivalRating;
    NSString *drivingRating;
    NSString *serviceRating;
    NSString *tripRating;
    NSString *carQualityRating;
    NSString *otherRating;
    int tag_Value;
    
    }
@end

@implementation ShowFeedbackVC


#pragma mark- View Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    txt_View.layer.borderWidth = 1.0f;
    txt_View.layer.borderColor = [[UIColor grayColor] CGColor];
    
    [self makeRequest];
    //[self showRatingView];
}
- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}
- (void)keyboardWasShown:(NSNotification *)notification {
    
    NSDictionary* info = [notification userInfo];
    
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGPoint buttonOrigin = txt_View.superview.frame.origin;
    CGFloat buttonHeight = txt_View.superview.frame.size.height;
    CGRect visibleRect = self.view.frame;
    visibleRect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
        [scroll_View setContentOffset:scrollPoint animated:YES];
    }
    
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    [scroll_View setContentOffset:CGPointZero animated:YES];
    
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}


- (void)viewWillDisappear:(BOOL)animated {
    
    [self deregisterFromKeyboardNotifications];
    
    [super viewWillDisappear:animated];
    
}
#pragma mark- Other Methods
- (void)showRatingView{
    
    
    
    float viewWidth = CGRectGetWidth(self.view.frame);
    float DLstarHeight;
    float lblHeight;
    float fontSize;
    CGSize imageSize;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        imageSize = CGSizeMake(60, 30);
        DLstarHeight = 72.0;
        lblHeight = 60.0;
        fontSize = 18.0;
    }else {
        imageSize = CGSizeMake(30, 15);
        DLstarHeight = 42.0;
        lblHeight = 30.0;
        fontSize = 12.0;
    }
    
    DLStarRatingControl *customNumberOfStars1 = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, 0, viewWidth,DLstarHeight) andStars:5 isFractional:YES];
    customNumberOfStars1.backgroundColor = [UIColor whiteColor];
	customNumberOfStars1.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
	customNumberOfStars1.rating = [arrivalRating floatValue];
    customNumberOfStars1.enabled = NO;
	[scroll_View addSubview:customNumberOfStars1];
    
    ///add image and label //
    UIImageView  *arrival_Image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 12, imageSize.width, imageSize.height)];
    arrival_Image.contentMode = UIViewContentModeScaleAspectFit;
    arrival_Image.image = [UIImage imageNamed:@"ArrivalTime.png"];
    [customNumberOfStars1 addSubview:arrival_Image];
    
    UILabel *arrivalLbl = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 100, lblHeight)];
    arrivalLbl.text = @"ARRIVAL TIME";
    arrivalLbl.font = [UIFont fontWithName:@"Arial" size:12];
    [customNumberOfStars1 addSubview:arrivalLbl];
    
    DLStarRatingControl *customNumberOfStars2 = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(customNumberOfStars1.frame)+ 3, viewWidth, DLstarHeight) andStars:5 isFractional:YES];
    customNumberOfStars2.backgroundColor = [UIColor whiteColor];
	customNumberOfStars2.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
	customNumberOfStars2.rating = [drivingRating floatValue];
    customNumberOfStars2.enabled = NO;
	[scroll_View addSubview:customNumberOfStars2];
    
    ///add image and label //
    UIImageView  *driving_Image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 12, imageSize.width, imageSize.height)];
    driving_Image.contentMode = UIViewContentModeScaleAspectFit;
    driving_Image.image = [UIImage imageNamed:@"driving.png"];
    [customNumberOfStars2 addSubview:driving_Image];
    
    UILabel *drivingLbl = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 100, lblHeight)];
    drivingLbl.text = @"DRIVING";
    drivingLbl.font = [UIFont fontWithName:@"Arial" size:12];
    [customNumberOfStars2 addSubview:drivingLbl];
    
    
    DLStarRatingControl *customNumberOfStars3 = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(customNumberOfStars2.frame)+ 3, viewWidth, DLstarHeight) andStars:5 isFractional:YES];
    customNumberOfStars3.backgroundColor = [UIColor whiteColor];
	customNumberOfStars3.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
	customNumberOfStars3.rating = [serviceRating floatValue];
    customNumberOfStars3.enabled = NO;
	[scroll_View addSubview:customNumberOfStars3];
    
    ///add image and label //
    UIImageView  *services_Image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 12, imageSize.width, imageSize.height)];
    services_Image.contentMode = UIViewContentModeScaleAspectFit;
    services_Image.image = [UIImage imageNamed:@"service.png"];
    [customNumberOfStars3 addSubview:services_Image];
    
    UILabel *servicesLbl = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 100, lblHeight)];
    servicesLbl.text = @"SERVICE";
    servicesLbl.font = [UIFont fontWithName:@"Arial" size:12];
    [customNumberOfStars3 addSubview:servicesLbl];
    
    DLStarRatingControl *customNumberOfStars4 = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(customNumberOfStars3.frame)+ 3, viewWidth, DLstarHeight) andStars:5 isFractional:YES];
    customNumberOfStars4.backgroundColor = [UIColor whiteColor];
	customNumberOfStars4.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
	customNumberOfStars4.rating = [tripRating floatValue];
    customNumberOfStars4.enabled = NO;
	[scroll_View addSubview:customNumberOfStars4];
    
    ///add image and label //
    UIImageView  *tripImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 12, imageSize.width, imageSize.height)];
    tripImage.contentMode = UIViewContentModeScaleAspectFit;
    tripImage.image = [UIImage imageNamed:@"triproute.png"];
    [customNumberOfStars4 addSubview:tripImage];
    
    UILabel *tripLbl = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 100, lblHeight)];
    tripLbl.text = @"TRIP ROUTE";
    tripLbl.font = [UIFont fontWithName:@"Arial" size:12];
    [customNumberOfStars4 addSubview:tripLbl];
    
    
    DLStarRatingControl *customNumberOfStars5 = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(customNumberOfStars4.frame)+ 3, viewWidth,DLstarHeight) andStars:5 isFractional:YES];
    customNumberOfStars5.backgroundColor = [UIColor whiteColor];
	customNumberOfStars5.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
	customNumberOfStars5.rating = [carQualityRating floatValue];
    customNumberOfStars5.enabled = NO;
	[scroll_View addSubview:customNumberOfStars5];
    
    ///add image and label //
    UIImageView  *carQuality_Image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 12, imageSize.width, imageSize.height)];
    carQuality_Image.contentMode = UIViewContentModeScaleAspectFit;
    carQuality_Image.image = [UIImage imageNamed:@"carquality.png"];
    [customNumberOfStars5 addSubview:carQuality_Image];
    
    UILabel *carQualityLbl = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 100, lblHeight)];
    carQualityLbl.text = @"CAR QUALITY";
    carQualityLbl.font = [UIFont fontWithName:@"Arial" size:12];
    [customNumberOfStars5 addSubview:carQualityLbl];
    
    
    DLStarRatingControl *customNumberOfStars6 = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(customNumberOfStars5.frame)+ 3, viewWidth, DLstarHeight) andStars:5 isFractional:YES];
    customNumberOfStars6.backgroundColor = [UIColor whiteColor];
	customNumberOfStars6.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
	customNumberOfStars6.rating = [otherRating floatValue];
    customNumberOfStars6.enabled = NO;
	[scroll_View addSubview:customNumberOfStars6];
   // UILabel *lblComment = [[UILabel alloc]initWithFrame:CGRectMake(0, 318, , <#CGFloat height#>)
    
    ///add image and label //
    UIImageView  *other_Image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 12, imageSize.width, imageSize.height)];
    other_Image.contentMode = UIViewContentModeScaleAspectFit;
    other_Image.image = [UIImage imageNamed:@"other.png"];
    [customNumberOfStars6 addSubview:other_Image];
    
    UILabel *otherLbl = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 100, lblHeight)];
    otherLbl.text = @"OTHER";
    otherLbl.font = [UIFont fontWithName:@"Arial" size:12];
    [customNumberOfStars6 addSubview:otherLbl];
}

#pragma mark- Request Methods
//////************ MAKE REQUEST  ***********///////
- (void)makeRequest {
    
    NSString *urlString = @"get_feedback";
    
    NSMutableDictionary *param_Dictionary = [[NSMutableDictionary alloc] init];
    
    [param_Dictionary setObject:@"2573" forKey:@"reservation_id"];
    
    //// ******** convert dictionary values in json format ******///////
    NSError *error;
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param_Dictionary
                                                       options:NSJSONWritingPrettyPrinted //Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    }
    else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"response data %@", jsonString);
    }
    __block typeof(self) weakSelf = self;
    ServiceManager *ObjtServcManager = [ServiceManager sharedInstance];
    [ObjtServcManager dataTaskWithPostRequest:urlString dicParameters:param_Dictionary successBlock:^(NSDictionary<NSString *,id> * dicReponse) {
        [weakSelf hideActivityIndicator];
        [weakSelf responseStringValue:dicReponse];
    } failureBlock:^(NSError *error) {
        [weakSelf hideActivityIndicator];
     //   [[JLToast makeText:@"Network Error " delay:0.0 duration:3.0] show];
    } ];
    
}
-(void)showActivityIndicator{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [app showProgressWithText:@"Please Wait..."];
    
}
-(void)hideActivityIndicator{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [app hideProgress];
    
}
/// GET RESPONSE FROM WEB SERVICE ////
////////////*********** PROTOCOL METHOD IMPLEMENTATION *********////////////////
- (void)responseStringValue:(NSDictionary *)dict_Value
{
	NSString *status_String = [dict_Value objectForKey:@"status"];
	if ([status_String isEqualToString:@"success"]) {
		
        NSArray *array = [dict_Value valueForKey:@"records"];
        for (NSDictionary *dict in array) {
            
            if ([[dict valueForKey:@"rating_type"] isEqualToString:@"driving"]) {
                
                drivingRating = [dict valueForKey:@"rating"];
                
            }
            if ([[dict valueForKey:@"rating_type"] isEqualToString:@"arrival_time"]) {
                
                arrivalRating = [dict valueForKey:@"rating"];
                
            }
            if ([[dict valueForKey:@"rating_type"] isEqualToString:@"other"]) {
                
                otherRating = [dict valueForKey:@"rating"];
                
            }
            if ([[dict valueForKey:@"rating_type"] isEqualToString:@"car_quality"]) {
                
                carQualityRating = [dict valueForKey:@"rating"];
                
            }
            if ([[dict valueForKey:@"rating_type"] isEqualToString:@"service"]) {
                
                serviceRating = [dict valueForKey:@"rating"];
                
            }
            if ([[dict valueForKey:@"rating_type"] isEqualToString:@"trip_route"]) {
                
                tripRating = [dict valueForKey:@"rating"];
                txt_View.text = [dict valueForKey:@"comment"];
                
            }
            
        }
        [self showRatingView];
        
	}
	else if ([status_String isEqualToString:@"error"]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error !" message:[dict_Value objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
        
	}
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
