//
//  FeedbackSubView.h
//  RideApplication
//
//  Created by Gulraj Grewal on 2/13/15.
//  Copyright (c) 2015 BlackBirdWorldWide. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLStarRatingControl.h"

@interface FeedbackSubView : UIViewController<DLStarRatingDelegate, UITextFieldDelegate>
{
    IBOutlet UITextView *txt_View;
    IBOutlet UIScrollView *scroll_View;
    __weak IBOutlet UIButton *btnSubmit;
    
}
@property (nonatomic, strong)  NSString *reservationId;
@property (nonatomic, strong)  NSString *driverID;

- (IBAction)submitClick:(id)sender;
@end
