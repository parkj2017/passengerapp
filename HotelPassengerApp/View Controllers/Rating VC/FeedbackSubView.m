//
//  FeedbackSubView.m
//  RideApplication
//
//  Created by Gulraj Grewal on 2/13/15.
//  Copyright (c) 2015 BlackBirdWorldWide. All rights reserved.
//

#import "FeedbackSubView.h"
//#import <JLToast/JLToast.h>
#import "HotelPassengerApp-Swift.h"
#import "HotelPassengerApp-BridgingHeader.h"
@interface FeedbackSubView ()< BackButtonActionProtocol>
{
    NSString *arrivalRating;
    NSString *drivingRating;
    NSString *serviceRating;
    NSString *tripRating;
    NSString *carQualityRating;
    NSString *otherRating;
    int tag_Value;
    
    
    IBOutlet UITextField *txtGratuity_money;
    IBOutlet UITextField *txtGratuity_Percentage;
}
@end

@implementation FeedbackSubView


#pragma mark- View Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    txt_View.layer.borderWidth = 1.0f;
    txt_View.layer.borderColor = [[UIColor grayColor] CGColor];
    self .navigationItem.leftBarButtonItem = [[Globals sharedInstance] leftBackButtonWithCustom:@"Chauffeur Feedback" imageName:@"backArrow"];
    [self showRatingView];
    
    CGRect leftview1 = CGRectZero;
    CGRect leftview2 = CGRectZero;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        leftview1 = CGRectMake(0, 0, 30, 30);
        leftview2 = CGRectMake(0, 0, 30, 30);
    }
    else
    {
        leftview1 = CGRectMake(0, 0, 20, 20);
        leftview2 = CGRectMake(0, 0, 20, 20);
    }
    
    AppDelegate *appdel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    UILabel *moneyLeftView = [[UILabel alloc]initWithFrame:leftview1];
    moneyLeftView.backgroundColor = [UIColor clearColor];
    moneyLeftView.textAlignment = NSTextAlignmentCenter;
    moneyLeftView.textColor = [UIColor grayColor];
    moneyLeftView.text = [NSString stringWithFormat:@"%@", appdel.currency];
    moneyLeftView.font = [UIFont fontWithName:@"Oxygen-Bold" size:15];

    txtGratuity_money.leftView = moneyLeftView;
    txtGratuity_money.leftViewMode = UITextFieldViewModeAlways;
    txtGratuity_money.delegate = self;

    UILabel *percentLeftview = [[UILabel alloc]initWithFrame:leftview2];
    percentLeftview.backgroundColor = [UIColor clearColor];
    percentLeftview.textColor = [UIColor grayColor];
    percentLeftview.textAlignment = NSTextAlignmentCenter;
    percentLeftview.text = @"%";
    percentLeftview.font = [UIFont fontWithName:@"Oxygen-Bold" size:15];

    txtGratuity_Percentage.delegate = self;
    txtGratuity_Percentage.leftView = percentLeftview;
    txtGratuity_Percentage.leftViewMode = UITextFieldViewModeAlways;

    txtGratuity_money.layer.borderWidth = 5;
    txtGratuity_money.layer.borderWidth = 1.0;
    txtGratuity_money.layer.borderColor = [UIColor grayColor].CGColor;
    
    txtGratuity_Percentage.layer.borderWidth = 5;
    txtGratuity_Percentage.layer.borderWidth = 1.0;
    txtGratuity_Percentage.layer.borderColor = [UIColor grayColor].CGColor;
    
    txtGratuity_money.keyboardType = UIKeyboardTypeDecimalPad;
    txtGratuity_Percentage.keyboardType = UIKeyboardTypeDecimalPad;
    
    
    txt_View.tintColor = [UIColor colorWithRed: 29.0/255.0 green: 127.0/255.0 blue: 62.0/255.0  alpha: 1.0];
    txtGratuity_money.tintColor = [UIColor colorWithRed: 29.0/255.0 green: 127.0/255.0 blue: 62.0/255.0  alpha: 1.0];
    txtGratuity_Percentage.tintColor = [UIColor colorWithRed: 29.0/255.0 green: 127.0/255.0 blue: 62.0/255.0  alpha: 1.0];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [Globals sharedInstance].delegateBack = self;
    [self registerForKeyboardNotifications];
}
#pragma mark- Other Methods
- (void)showRatingView{
    [scroll_View setContentInset:UIEdgeInsetsMake(5, 0, 0, 0)];
    
    float viewWidth = CGRectGetWidth(self.view.frame);
   
    float DLstarHeight;
    float lblHeight;
    float fontSize;
    float lblWitdh;
    float lblYValue;
    CGSize imageSize;
    CGRect imageRect;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        imageRect = CGRectMake(10, 16, 50, 50);
         imageSize = CGSizeMake(65, 65);
        DLstarHeight = 82.0;
        lblHeight = 60.0;
        lblYValue = 16;
        fontSize = 20.0;
        lblWitdh = 300;
    }else {
         imageRect = CGRectMake(10, 11, 30, 30);
        imageSize = CGSizeMake(45, 45);
        DLstarHeight = 52.0;
        lblHeight = 30.0;
        lblYValue = 11;
        fontSize = 15.0;
        lblWitdh = 130;
    }
    

   DLStarRatingControl *customNumberOfStars1 = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, 0, viewWidth,DLstarHeight) andStars:5 isFractional:YES];
    customNumberOfStars1.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:122.0/255.0 blue:120.0/255.0 alpha:1.0];
    customNumberOfStars1.delegate = self;
    customNumberOfStars1.rating = 0;
    customNumberOfStars1.tag = 101;
    [scroll_View addSubview:customNumberOfStars1];
    
    ///add image and label //
    UIImageView  *arrival_Image = [[UIImageView alloc] initWithFrame:imageRect];
    arrival_Image.contentMode = UIViewContentModeScaleAspectFit;
    arrival_Image.image = [UIImage imageNamed:@"ArrivalTime.png"];
    [customNumberOfStars1 addSubview:arrival_Image];
    
    UILabel *arrivalLbl = [[UILabel alloc]initWithFrame:CGRectMake(imageSize.width + 10, lblYValue, lblWitdh, lblHeight)];
    arrivalLbl.backgroundColor = [UIColor clearColor];
    arrivalLbl.textColor = [UIColor whiteColor];
    arrivalLbl.text = @"ARRIVAL TIME";
    arrivalLbl.font = [UIFont fontWithName:@"Oxygen-Bold" size:fontSize];
    [customNumberOfStars1 addSubview:arrivalLbl];
    UILabel *lblLine = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(customNumberOfStars1.frame)-1, viewWidth, 1)];
    lblLine.backgroundColor = [UIColor lightGrayColor];
    //[customNumberOfStars1 addSubview:lblLine];
    
    
  DLStarRatingControl *customNumberOfStars2 = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(customNumberOfStars1.frame), viewWidth, DLstarHeight) andStars:5 isFractional:YES];
    customNumberOfStars2.backgroundColor = [UIColor colorWithRed:251.0/255.0 green:181.0/255.0 blue:123.0/255.0 alpha:1.0];

    customNumberOfStars2.delegate = self;
    customNumberOfStars2.rating = 0;
    customNumberOfStars2.tag = 102;
    [scroll_View addSubview:customNumberOfStars2];
    
    ///add image and label //
    UIImageView  *driving_Image = [[UIImageView alloc] initWithFrame:imageRect];
    driving_Image.contentMode = UIViewContentModeScaleAspectFit;
    driving_Image.image = [UIImage imageNamed:@"driving.png"];
    [customNumberOfStars2 addSubview:driving_Image];
    
   UILabel *drivingLbl = [[UILabel alloc]initWithFrame:CGRectMake(imageSize.width + 10, lblYValue, lblWitdh, lblHeight)];
    drivingLbl.backgroundColor = [UIColor clearColor];
    drivingLbl.textColor = [UIColor whiteColor];
    drivingLbl.text = @"DRIVING";
    drivingLbl.font = [UIFont fontWithName:@"Oxygen-Bold" size:fontSize];
    [customNumberOfStars2 addSubview:drivingLbl];
    UILabel *lblLine1 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(customNumberOfStars2.frame)-1, viewWidth, 1)];
    lblLine1.backgroundColor = [UIColor lightGrayColor];
    //[customNumberOfStars2 addSubview:lblLine1];
    //[customNumberOfStars2 bringSubviewToFront:lblLine1];
    
    DLStarRatingControl *customNumberOfStars3 = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(customNumberOfStars2.frame), viewWidth, DLstarHeight) andStars:5 isFractional:YES];
    customNumberOfStars3.backgroundColor = [UIColor colorWithRed:92.0/255.0 green:158.0/255.0 blue:165.0/255.0 alpha:1.0];

    customNumberOfStars3.delegate = self;
    customNumberOfStars3.rating = 0;
    customNumberOfStars3.tag = 103;
    [scroll_View addSubview:customNumberOfStars3];
    
    ///add image and label //
   UIImageView  *services_Image = [[UIImageView alloc] initWithFrame:imageRect];
    services_Image.contentMode = UIViewContentModeScaleAspectFit;
    services_Image.image = [UIImage imageNamed:@"service.png"];
    [customNumberOfStars3 addSubview:services_Image];
    
 UILabel *servicesLbl = [[UILabel alloc]initWithFrame:CGRectMake(imageSize.width + 10, lblYValue, lblWitdh, lblHeight)];
    servicesLbl.backgroundColor = [UIColor clearColor];
    servicesLbl.textColor = [UIColor whiteColor];
    servicesLbl.text = @"SERVICE";
    servicesLbl.font = [UIFont fontWithName:@"Oxygen-Bold" size:fontSize];
    [customNumberOfStars3 addSubview:servicesLbl];
    UILabel *lblLine2 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(customNumberOfStars3.frame)-1, viewWidth, 1)];
    lblLine2.backgroundColor = [UIColor lightGrayColor];
    //[customNumberOfStars3 addSubview:lblLine2];
    //[customNumberOfStars3 bringSubviewToFront:lblLine2];
    
    DLStarRatingControl *customNumberOfStars4 = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(customNumberOfStars3.frame), viewWidth, DLstarHeight) andStars:5 isFractional:YES];
    customNumberOfStars4.backgroundColor =[UIColor colorWithRed:173.0/255.0 green:139.0/255.0 blue:197.0/255.0 alpha:1.0];

    customNumberOfStars4.delegate = self;
    customNumberOfStars4.rating = 0;
    customNumberOfStars4.tag = 104;
    [scroll_View addSubview:customNumberOfStars4];
    
    ///add image and label //
     UIImageView  *tripImage = [[UIImageView alloc] initWithFrame:imageRect];
    tripImage.contentMode = UIViewContentModeScaleAspectFit;
    tripImage.image = [UIImage imageNamed:@"triproute.png"];
    [customNumberOfStars4 addSubview:tripImage];
    
   UILabel *tripLbl = [[UILabel alloc]initWithFrame:CGRectMake(imageSize.width + 10, lblYValue, lblWitdh, lblHeight)];
    tripLbl.backgroundColor = [UIColor clearColor];
    tripLbl.textColor = [UIColor whiteColor];
    tripLbl.text = @"TRIP ROUTE";
    tripLbl.font = [UIFont fontWithName:@"Oxygen-Bold" size:fontSize];
    [customNumberOfStars4 addSubview:tripLbl];
    UILabel *lblLine3 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(customNumberOfStars4.frame)-1, viewWidth, 1)];
    lblLine3.backgroundColor = [UIColor lightGrayColor];
    //[customNumberOfStars4 addSubview:lblLine3];
    
     DLStarRatingControl *customNumberOfStars5 = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(customNumberOfStars4.frame), viewWidth,DLstarHeight) andStars:5 isFractional:YES];
    customNumberOfStars5.backgroundColor = [UIColor colorWithRed:165.0/255.0 green:75.0/255.0 blue:105.0/255.0 alpha:1.0];

    customNumberOfStars5.delegate = self;
    customNumberOfStars5.rating = 0;
    customNumberOfStars5.tag = 105;
    [scroll_View addSubview:customNumberOfStars5];
    
    ///add image and label //
     UIImageView  *carQuality_Image = [[UIImageView alloc] initWithFrame:imageRect];
    carQuality_Image.contentMode = UIViewContentModeScaleAspectFit;
    carQuality_Image.image = [UIImage imageNamed:@"carquality.png"];
    [customNumberOfStars5 addSubview:carQuality_Image];
    
   UILabel *carQualityLbl = [[UILabel alloc]initWithFrame:CGRectMake(imageSize.width + 10, lblYValue, lblWitdh, lblHeight)];
    carQualityLbl.backgroundColor = [UIColor clearColor];
    carQualityLbl.textColor = [UIColor whiteColor];
    carQualityLbl.text = @"CAR QUALITY";
    carQualityLbl.font = [UIFont fontWithName:@"Oxygen-Bold" size:fontSize];
    [customNumberOfStars5 addSubview:carQualityLbl];
    
    UILabel *lblLine4 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(customNumberOfStars5.frame)-1, viewWidth, 1)];
    lblLine4.backgroundColor = [UIColor lightGrayColor];
    //[customNumberOfStars5 addSubview:lblLine4];
    
    DLStarRatingControl *customNumberOfStars6 = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(customNumberOfStars5.frame), viewWidth, DLstarHeight) andStars:5 isFractional:YES];
    customNumberOfStars6.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:164.0/255.0 blue:205.0/255.0 alpha:1.0];

    customNumberOfStars6.delegate = self;
    customNumberOfStars6.rating = 0;
    customNumberOfStars6.tag = 106;
    [scroll_View addSubview:customNumberOfStars6];
    ///add image and label //
     UIImageView  *other_Image = [[UIImageView alloc] initWithFrame:imageRect];
    other_Image.contentMode = UIViewContentModeScaleAspectFit;
    other_Image.image = [UIImage imageNamed:@"other.png"];
    [customNumberOfStars6 addSubview:other_Image];
    
    UILabel *otherLbl = [[UILabel alloc]initWithFrame:CGRectMake(imageSize.width + 10, lblYValue, lblWitdh, lblHeight)];
    otherLbl.backgroundColor = [UIColor clearColor];
    otherLbl.textColor = [UIColor whiteColor];
    otherLbl.text = @"OTHER";
    otherLbl.font = [UIFont fontWithName:@"Oxygen-Bold" size:fontSize];
    [customNumberOfStars6 addSubview:otherLbl];
    
    UILabel *lblLine5 = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(customNumberOfStars6.frame)-1, viewWidth, 1)];
    lblLine5.backgroundColor = [UIColor lightGrayColor];
    //[customNumberOfStars6 addSubview:lblLine5];
    
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolbar.barTintColor = [UIColor colorWithRed:31 / 255.0 green:121 / 255.0 blue:232 / 255.0 alpha:1.0];
    toolbar.translucent = NO;
    
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"DONE", nil)  style:UIBarButtonItemStyleBordered target:self action:@selector(donePressed)];
    [doneButton setTintColor:[UIColor whiteColor]];
    [doneButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Oxygen-Bold" size:18.0], NSFontAttributeName, nil]
                              forState:UIControlStateNormal];
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
   // fixedItem.width = 120; // or whatever you want
    
    [toolbar setItems:[NSArray arrayWithObjects:fixedItem,doneButton, fixedItem,nil]];
    txt_View.inputAccessoryView = toolbar;
    txtGratuity_Percentage.inputAccessoryView = toolbar;
    txtGratuity_money.inputAccessoryView = toolbar;
    
}

-(void)donePressed
{
    [self.view endEditing:YES];
}


- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}


- (void)viewWillDisappear:(BOOL)animated {
    
    [self deregisterFromKeyboardNotifications];
    
    [super viewWillDisappear:animated];
    
}

- (void)keyboardWasShown:(NSNotification *)notification {
    
    NSDictionary* info = [notification userInfo];
    
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGPoint buttonOrigin = btnSubmit.frame.origin;
    //CGFloat buttonHeight = btnSubmit.frame.size.height;
    CGRect visibleRect = self.view.frame;
    visibleRect.size.height -= keyboardSize.height+20;
    
    if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height);
        [scroll_View setContentOffset:scrollPoint animated:YES];
    }
    
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    [scroll_View setContentOffset:CGPointZero animated:YES];
    
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return true;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField == txtGratuity_Percentage) {
        txtGratuity_money.text = @"";
    }
    if (textField == txtGratuity_money) {
        txtGratuity_Percentage.text = @"";
    }
    return YES;
}
#pragma mark- Other Methods
-(void)backButtonActionMethod:(UIButton *)sender{
    
    [[Globals sharedInstance] removeValuetoUserDefaultsWithKey:@"activeJobNotification"];
    [[Globals sharedInstance] removeValuetoUserDefaultsWithKey:@"currentJobStaus"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"backToMainView" sender:self];
    });
}
- (IBAction)backClicked:(id)sender {
    
    
    
}

-(void)newRating:(DLStarRatingControl *)control :(float)rating {
    
    //DLStarRatingControl *customNumberOfStars6 = (DLStarRatingControl *)control;
    NSInteger intValue = control.tag;
    if (intValue == 101) {
        
        arrivalRating  = [NSString stringWithFormat:@"%0.1f",rating];
    }
    else if (intValue == 102)
    {
        drivingRating  = [NSString stringWithFormat:@"%0.1f",rating];
        
    }
    else if (intValue == 103)
    {
        serviceRating  = [NSString stringWithFormat:@"%0.1f",rating];
        
    }
    else if (intValue == 104)
    {
        tripRating  = [NSString stringWithFormat:@"%0.1f",rating];
        
    }
    else if (intValue == 105)
    {
        carQualityRating  = [NSString stringWithFormat:@"%0.1f",rating];
        
    }
    else if (intValue == 106)
    {
        otherRating  = [NSString stringWithFormat:@"%0.1f",rating];
        
    }
}
- (IBAction)submitClick:(id)sender;
{
    
    [self showActivityIndicator];
    [self makeRequest];
}
#pragma mark- Request Methods
//////************ MAKE REQUEST  ***********///////
- (void)makeRequest {
    
    NSString *urlString = @"customerfeedback";
    
    NSMutableDictionary *param_Dictionary = [[NSMutableDictionary alloc] init];
    
    NSString *user_ID = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ActiveUserInfo.id"];
     NSString *session = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ActiveUserInfo.session"];
    [param_Dictionary setObject:user_ID forKey:@"user_id"];
    [param_Dictionary setObject:session forKey:@"session"];
    [param_Dictionary setObject:_reservationId forKey:@"reservation_id"];
    
    if (!arrivalRating) {
        arrivalRating = @"0.0";
    }
    if (!drivingRating) {
        drivingRating = @"0.0";
    }
    if (!serviceRating) {
        serviceRating = @"0.0";
    }
    if (!tripRating) {
        tripRating = @"0.0";
    }
    if (!carQualityRating) {
        carQualityRating = @"0.0";
    }
    if (!otherRating) {
        otherRating = @"0.0";
    }
    
    NSDictionary *dictValue = [NSDictionary dictionaryWithObjectsAndKeys:
                               arrivalRating, @"arrival_time",
                               drivingRating, @"driving",
                               serviceRating, @"service",
                               tripRating, @"trip_route",
                               carQualityRating, @"car_quality",otherRating, @"other",nil];
    
    
    NSMutableArray *fieldValues = [[NSMutableArray alloc] init];
    [fieldValues addObject:dictValue];
    
    
    [param_Dictionary setValue:fieldValues forKey:@"rating"];
    if (txt_View.text == nil) {
        txt_View.text = @"";
    }
    else{
        [param_Dictionary setObject:txt_View.text forKey:@"comment"];
    }
    
    [param_Dictionary setObject:_driverID forKey:@"driver_id"];
    
    [param_Dictionary setObject:@"card" forKey:@"payment_mode"];
    if (txtGratuity_money.text.length >0){
    [param_Dictionary setObject:@"F" forKey:@"gratuity_type"];
        [param_Dictionary setObject:txtGratuity_money.text forKey:@"gratuity"];
    }
    else{
        [param_Dictionary setObject:@"P" forKey:@"gratuity_type"];
    [param_Dictionary setObject:txtGratuity_Percentage.text forKey:@"gratuity"];
    }

    //// ******** convert dictionary values in json format ******///////
    NSError *error;
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param_Dictionary
                                                       options:NSJSONWritingPrettyPrinted //Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    }
    else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"response data %@", jsonString);
    }
    
//    return;
    __block typeof(self) weakSelf = self;
    ServiceManager *ObjtServcManager = [ServiceManager sharedInstance];
    [ObjtServcManager dataTaskWithPostRequest:urlString dicParameters:param_Dictionary successBlock:^(NSDictionary<NSString *,id> * dicReponse) {
        [weakSelf hideActivityIndicator];
        [weakSelf responseStringValue:dicReponse];
    } failureBlock:^(NSError *error) {
        [weakSelf hideActivityIndicator];
     //   [[JLToast makeText:@"Network Error " delay:0.0 duration:3.0] show];
    } ];
}
-(void)showActivityIndicator{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [app showProgressWithText:@"Please Wait..."];
    
}
-(void)hideActivityIndicator{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [app hideProgress];
    
}
/// GET RESPONSE FROM WEB SERVICE ////
////////////*********** PROTOCOL METHOD IMPLEMENTATION *********////////////////
- (void)responseStringValue:(NSDictionary *)dict_Value
{
    
    [self hideActivityIndicator];
    
    NSString *status_String = [dict_Value objectForKey:@"status"];
    if ([status_String isEqualToString:@"success"]) {
        
        
        
    }
    else if ([status_String isEqualToString:@"error"]) {
      //  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error !" message:[dict_Value objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
     //   [alert show];
        //[self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    //[self.navigationController popToRootViewControllerAnimated:YES];
    [[Globals sharedInstance] removeValuetoUserDefaultsWithKey:@"activeJobNotification"];
    [[Globals sharedInstance] removeValuetoUserDefaultsWithKey:@"currentJobStaus"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[[UIAlertView alloc] initWithTitle:@"Thanks for your feedback" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];

        [self performSegueWithIdentifier:@"backToMainView" sender:self];
    });
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
