//
//  RangeSlider.swift
//  CustomSliderExample
//
//  Created by William Archimede on 04/09/2014.
//  Copyright (c) 2014 HoodBrains. All rights reserved.
//

import UIKit
import QuartzCore

class RangeSliderTrackLayer: CALayer {
    weak var rangeSlider: RangeSlider?
    
    override func draw(in ctx: CGContext) {
        guard let slider = rangeSlider else {
            return
        }
        
        // Clip
        let cornerRadius = bounds.height * slider.curvaceousness / 2.0
        let path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        ctx.addPath(path.cgPath)
        
        // Fill the track
        ctx.setFillColor(slider.trackTintColor.cgColor)
        ctx.addPath(path.cgPath)
        ctx.fillPath()
        
        // Fill the highlighted range
        ctx.setFillColor(slider.trackHighlightTintColor.cgColor)
        let lowerValuePosition = CGFloat(slider.positionForValue(value: slider.lowerValue))
        let upperValuePosition = CGFloat(slider.positionForValue(value: slider.upperValue))
        let rect = CGRect(x: lowerValuePosition, y: 0.0, width: upperValuePosition - lowerValuePosition, height: bounds.height)
        ctx.fill(rect)
        
        }
}

class RangeSliderThumbLayer: CALayer {
    
    var highlighted: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    weak var rangeSlider: RangeSlider?
    
    var strokeColor: UIColor = UIColor.lightGray {
        didSet {
            setNeedsDisplay()
        }
    }
    var lineWidth: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(in ctx: CGContext) {
        guard let slider = rangeSlider else {
            return
        }
        
        let thumbFrame = bounds.insetBy(dx: 2.0, dy: 2.0)
        let cornerRadius = thumbFrame.height * slider.curvaceousness / 2.0
        let thumbPath = UIBezierPath(roundedRect: thumbFrame, cornerRadius: cornerRadius)
        
        // Fill
        ctx.setFillColor(slider.thumbTintColor.cgColor)
        ctx.setShadow(offset: CGSize (width: -1, height:1) , blur: 3, color: UIColor.gray.cgColor)
        ctx.addPath(thumbPath.cgPath)
        ctx.fillPath()
        
        // Center fill
        let thumb_center = UIBezierPath(roundedRect: bounds.insetBy(dx: 13, dy: 13), cornerRadius: 3)
        ctx.setFillColor(UIColor.lightGray.cgColor)
        ctx.setShadow(offset:  CGSize (width: -1, height:1), blur: 3, color: nil)
        ctx.addPath(thumb_center.cgPath)
        ctx.fillPath()

        // Outline
        ctx.setStrokeColor(UIColor.lightGray.cgColor)
        ctx.setLineWidth(0.5)
        ctx.addPath(thumbPath.cgPath)
        ctx.strokePath()
        
        if highlighted {
            ctx.setFillColor(UIColor(white: 0.0, alpha: 0.1).cgColor)
            ctx.addPath(thumbPath.cgPath)
            ctx.fillPath()
        }
    }
}

@IBDesignable
class RangeSlider: UIControl {
    @IBInspectable var minimumValue: Double = 0.0 {
        willSet(newValue) {
            assert(newValue < maximumValue, "RangeSlider: minimumValue should be lower than maximumValue")
        }
        didSet {
            updateLayerFrames()
        }
    }
    
    @IBInspectable var maximumValue: Double = 1.0 {
        willSet(newValue) {
            assert(newValue > minimumValue, "RangeSlider: maximumValue should be greater than minimumValue")
        }
        didSet {
            updateLayerFrames()
        }
    }
    
    @IBInspectable var lowerValue: Double = 0.0 {
        didSet {
            if lowerValue < minimumValue {
                lowerValue = minimumValue
            }
            updateLayerFrames()
        }
    }
    
    @IBInspectable var upperValue: Double = 1.0 {
        didSet {
            if upperValue > maximumValue {
                upperValue = maximumValue
            }
            updateLayerFrames()
        }
    }
    @IBInspectable var needLowerThumb: Bool = true
    
    
    var gapBetweenThumbs: Double {
        if needLowerThumb == true{
        return Double(thumbWidth)*(maximumValue - minimumValue) / Double(bounds.width)
        }
        else {
            return 0.0
        }
    }
    
    @IBInspectable var trackTintColor: UIColor = UIColor(red: 234.0/255.0, green: 232.0/255.0, blue: 233.0/255.0, alpha: 1.0) {
        didSet {
            trackLayer.setNeedsDisplay()
        }
    }
    
    @IBInspectable var trackHighlightTintColor: UIColor = UIColor.appThemeColor() {
        didSet {
            trackLayer.setNeedsDisplay()
        }
    }
    
    @IBInspectable var thumbTintColor: UIColor = UIColor.white {
        didSet {
            if needLowerThumb == true{
                lowerThumbLayer.setNeedsDisplay()
            }
            upperThumbLayer.setNeedsDisplay()
        }
    }
    
    @IBInspectable var thumbBorderColor: UIColor = UIColor.gray {
    didSet {
            if needLowerThumb == true{
                lowerThumbLayer.strokeColor = thumbBorderColor
            }
            upperThumbLayer.strokeColor = thumbBorderColor
        }
    }
    
    @IBInspectable var thumbBorderWidth: CGFloat = 0.5 {
        didSet {
            if needLowerThumb == true{
            lowerThumbLayer.lineWidth = thumbBorderWidth
            }
            upperThumbLayer.lineWidth = thumbBorderWidth
        }
    }
    
    @IBInspectable var curvaceousness: CGFloat = 1.0 {
        didSet {
            if curvaceousness < 0.0 {
                curvaceousness = 0.0
            }
            
            if curvaceousness > 1.0 {
                curvaceousness = 1.0
            }
            
            trackLayer.setNeedsDisplay()
            if needLowerThumb == true{
                lowerThumbLayer.setNeedsDisplay()
            }
            upperThumbLayer.setNeedsDisplay()
        }
    }
    
    private var previouslocation = CGPoint()
    
    private let trackLayer = RangeSliderTrackLayer()
    private let lowerThumbLayer = RangeSliderThumbLayer()
    private let upperThumbLayer = RangeSliderThumbLayer()
    
    private var thumbWidth: CGFloat {
        return CGFloat(bounds.height)
    }
    
    override var frame: CGRect {
        didSet {
            updateLayerFrames()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeLayers()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initializeLayers()
    }
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        updateLayerFrames()
    }
  
    
    private func initializeLayers() {
        layer.backgroundColor = UIColor.clear.cgColor
        
        trackLayer.rangeSlider = self
        trackLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(trackLayer)
        
        if needLowerThumb == true{
            lowerThumbLayer.rangeSlider = self
            lowerThumbLayer.contentsScale = UIScreen.main.scale
            layer.addSublayer(lowerThumbLayer)
        }
        upperThumbLayer.rangeSlider = self
        upperThumbLayer.contentsScale = UIScreen.main.scale//mainScre().scale
        layer.addSublayer(upperThumbLayer)
    }
    
    func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        trackLayer.frame = bounds.insetBy(dx: 0.0, dy: bounds.height/3)
        trackLayer.setNeedsDisplay()
        
        if needLowerThumb == true{
            let lowerThumbCenter = CGFloat(positionForValue(value: lowerValue))
            lowerThumbLayer.frame = CGRect(x: lowerThumbCenter - thumbWidth/2.0, y: 0.0, width: thumbWidth, height: thumbWidth)
            lowerThumbLayer.setNeedsDisplay()
        }
        else {
            let lowerThumbCenter = CGFloat(0.0)
            lowerThumbLayer.frame = CGRect() //CGRectZero
            lowerThumbLayer.setNeedsDisplay()
        }
        
        let upperThumbCenter = CGFloat(positionForValue(value: upperValue))
        upperThumbLayer.frame = CGRect(x: upperThumbCenter - thumbWidth/2.0, y: 0.0, width: thumbWidth, height: thumbWidth)
        upperThumbLayer.setNeedsDisplay()
        
        CATransaction.commit()
    }
    
    func positionForValue(value: Double) -> Double {
        return Double(bounds.width - thumbWidth) * (value - minimumValue) /
            (maximumValue - minimumValue) + Double(thumbWidth/2.0)
    }
    
    func boundValue(value: Double, toLowerValue lowerValue: Double, upperValue: Double) -> Double {
        return min(max(value, lowerValue), upperValue)
    }
    
    
    // MARK: - Touches
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        previouslocation = touch.location(in: self)
        
        // Hit test the thumb layers
        if needLowerThumb == true{
            if lowerThumbLayer.frame.contains(previouslocation) {
                lowerThumbLayer.highlighted = true
            }
        }
        else {
            lowerThumbLayer.highlighted = false
        }
        
        if upperThumbLayer.frame.contains(previouslocation) {
            upperThumbLayer.highlighted = true
        }
        return upperThumbLayer.highlighted || lowerThumbLayer.highlighted
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let location = touch.location(in: self)
        
        // Determine by how much the user has dragged
        let deltaLocation = Double(location.x - previouslocation.x)
        let deltaValue = (maximumValue - minimumValue) * deltaLocation / Double(bounds.width - bounds.height)
        
        previouslocation = location
        
        // Update the values
        if needLowerThumb == true{
            if lowerThumbLayer.highlighted {
                lowerValue = boundValue(value: lowerValue + deltaValue, toLowerValue: minimumValue, upperValue: upperValue - gapBetweenThumbs)
            }
        }
        else {
            lowerValue = 0.0
        }
        if upperThumbLayer.highlighted {
            upperValue = boundValue(value: upperValue + deltaValue, toLowerValue: lowerValue + gapBetweenThumbs, upperValue: maximumValue)
        }
        
        sendActions(for: .valueChanged)
        
        return true
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        if needLowerThumb == true{
        lowerThumbLayer.highlighted = false
        }
        upperThumbLayer.highlighted = false
    }
}
