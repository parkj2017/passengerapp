//
//  PAFareBreakDownPopupVC.swift
//  HotelPassengerApp
//
//  Created by Netquall on 10/6/16.
//  Copyright © 2016 Netquall. All rights reserved.
//
import UIKit
class PAFareBreakDownPopupVC: UIViewController {
    @IBOutlet weak var tblPopupDetails: UITableView!
    var dicFareDetails : [String:Any]!
    var arrFareDetails : [[String:Any]] = [[String:Any]]()
    var arrKeys : [String]!
    var strTotalFare:String?
    var currency:String?
    var showdisclaimer:Bool = false

    @IBOutlet weak var lblDisclaimerHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if dicFareDetails != nil {
            self.arrKeys = [String](self.dicFareDetails.keys)
            self.tblPopupDetails.reloadData()
        }
        if let viewBlack = self.view.viewWithTag(555) {
            viewBlack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PAFareBreakDownPopupVC.didTapDismissView)))
        }
        if let viewWhite = self.view.viewWithTag(888) {
            viewWhite.backgroundColor = UIColor.appThemeColor()
            if let lblTotalKey = viewWhite.viewWithTag(66) as? UILabel {
                lblTotalKey.textColor = UIColor.white
                if showdisclaimer == false{
                    lblTotalKey.text = "Total Fare"
                }
                else {
                    lblTotalKey.text = "Estimated Fare"
                }
            }
            if let viewBlack = viewWhite.viewWithTag(67) as? UILabel{
                viewBlack.textColor = UIColor.white
                viewBlack.text = strTotalFare!.isEmpty ? "0.0" : "\(strTotalFare!)"
            }
        }
        
        if showdisclaimer == true{
            lblDisclaimerHeightConstraint.constant = (DeviceType.IS_IPAD) ? 73.0 * 1.5 : 73.0
        }
        else {
            lblDisclaimerHeightConstraint.constant = 0.0
        }
        self.view.updateConstraintsIfNeeded()
        
        self.tblPopupDetails.tableFooterView = UIView()
        
        
        // Do any additional setup after loading the view.
    }
    
    func didTapDismissView()  {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension PAFareBreakDownPopupVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrKeys != nil {
            return arrKeys.count
        }
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier : String!  = "cellFareBreakDown"
        var cell : UITableViewCell!
        var lblFareKey : UILabel!
        var lblFareValue : UILabel!
        cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath)
        if cell == nil {
            cell = UITableViewCell(style:.default, reuseIdentifier: cellIndentifier)
        }else {
            lblFareKey = cell.viewWithTag( 66) as? UILabel
            lblFareValue = cell.viewWithTag( 67) as? UILabel
            lblFareValue.numberOfLines = 5
            lblFareValue.textColor = UIColor.darkGray
            lblFareValue.textAlignment = .right
            lblFareValue.backgroundColor = UIColor.white
            lblFareKey.numberOfLines = 5
            lblFareKey.textColor = UIColor.darkGray
            lblFareKey.textAlignment = .left
            lblFareKey.backgroundColor = UIColor.white
            lblFareKey.font = Globals.defaultAppFont(15.0)
            lblFareValue.font = Globals.defaultAppFont(15.0)
        }
        
        if self.arrKeys != nil {
            
            if let dicValues = arrFareDetails[indexPath.row] as? [String:Any]{
                lblFareValue.isHidden = false
                lblFareKey.isHidden = false
                let fareKey = dicValues.keys.first//self.arrKeys[indexPath.row]
                let fareValue = String(describing: "\(dicValues.values.first!)")//self.dicFareDetails[fareKey]!
                lblFareKey.text = fareKey
                lblFareValue.text = " \(currency!) \(fareValue)"
            }
        }
        else {
            cell.textLabel?.text = "Fare breakdown not available"
            lblFareValue.isHidden = true
            lblFareKey.isHidden = true
        }
        
//        if self.arrKeys != nil {
//            lblFareValue.isHidden = false
//            lblFareKey.isHidden = false
//            let fareKey = self.arrKeys[indexPath.row]
//            let fareValue = String(describing: self.dicFareDetails[fareKey]!)
//            lblFareKey.text = fareKey
//            lblFareValue.text = " \(currency!) \(fareValue)"
//        }else {
//            cell.textLabel?.text = "Fare breakdown not available"
//            lblFareValue.isHidden = true
//            lblFareKey.isHidden = true
//        }

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = UIView()
        headerView.backgroundColor = UIColor.appThemeColor()
        headerView.frame = CGRect(x: 0, y: 0, width: self.tblPopupDetails.frame.width, height: 44)
        let lblTitle: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tblPopupDetails.frame.width, height: 44))
 
        lblTitle.font = Globals.defaultAppFontWithBold(16.0)
        lblTitle.textAlignment = .center
        lblTitle.backgroundColor = UIColor.clear
        lblTitle.textColor = UIColor.white
        headerView.addSubview(lblTitle)
        if showdisclaimer == true{
            lblTitle.text = "Rate Card"
        }
        else{
            lblTitle.text = "Fare Breakdown"
        }
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
}
