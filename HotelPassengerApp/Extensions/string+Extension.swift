//
//  string+Extension.swift
//  PassangerApp
//
//  Created by Netquall on 12/16/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import Foundation
import UIKit
extension String {
    
//func firstCharacterUpperCase() -> String {
//        let lowercaseString = self.lowercased()
//        
//        return lowercaseString.replacingCharacters(in: lowercaseString.startIndex...lowercaseString.startIndex, with: String(lowercaseString[lowercaseString.startIndex]).uppercased())
//}
    func numericString(_ str:String) -> String{
        return str.replacingOccurrences(of: "[^0-9.]", with: "", options: NSString.CompareOptions.regularExpression, range:nil).trimmingCharacters(in: CharacterSet.whitespaces)
    }
func urlencode() -> String {
        let urlEncoded = self.replacingOccurrences(of: " ", with: "+")
        return urlEncoded.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }
    
func hmacsha1(_ key: String) -> String {
    
    let encoding:GTMStringEncoding = GTMStringEncoding.rfc4648Base64WebsafeStringEncoding() as! GTMStringEncoding //rfc4648Base64WebsafeStringEncoding()
    
    let dataToDigest = self.data(using: String.Encoding.ascii)
    let secretKey =  encoding.decode(key)//encoding.init().decode(key)//encoding.decode(key)
    let digestLength = Int(CC_SHA1_DIGEST_LENGTH)
    let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLength)
    CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA1), (secretKey! as NSData).bytes, secretKey!.count, (dataToDigest! as NSData).bytes, dataToDigest!.count, result)
    let binarySignData = Data(bytes: UnsafePointer<UInt8>(result), count: digestLength)
    let strSignature = encoding.encode(binarySignData)
    return strSignature!
    }
///===== convert string to bool =====
func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return nil
        }
    }
    
func getNearCarsArrivalTimeInfo(_ dicCarsTimeInfo: [String:Any]) -> String{
    let arrRoutes = dicCarsTimeInfo["routes"] as! [[String : Any]]
    var strTimeReturn : String!
    if arrRoutes.count > 0 {
        let dicMainRoutes = arrRoutes.first!
        let arrLegs = dicMainRoutes["legs"] as! [[String:Any]]
        let dicDuration = arrLegs.first!["duration"] as! [String:Any]
        var strTimeArrival = dicDuration["text"] as! String
        if strTimeArrival.contains("hours") {
            strTimeArrival = strTimeArrival.replacingOccurrences(of: "hours", with: "h")
            strTimeArrival = strTimeArrival.replacingOccurrences(of: "mins", with: "m")
            strTimeArrival = strTimeArrival.replacingOccurrences(of: "min", with: "m")
        }
        if strTimeArrival.contains("hour") {
            strTimeArrival = strTimeArrival.replacingOccurrences(of: "hour", with: "h")
            strTimeArrival = strTimeArrival.replacingOccurrences(of: "mins", with: "m")
            strTimeArrival = strTimeArrival.replacingOccurrences(of: "min", with: "m")
        }
        strTimeReturn = strTimeArrival
    }
    return strTimeReturn
  }
    func compare(_ str:String) -> Bool {
        return self.caseInsensitiveCompare(str) == .orderedSame
    }
    // few changes
    func toDouble() -> Double? {
        if !self.isEmpty{
            return Double(self);
        }
        else{
            return 0.0
        }
    }
    func toInteger() -> Int {
        if !self.isEmpty{
            return Int(self)!
        }
        else{
            return 0
        }
    }
    func toFloat() -> Float {
        if !self.isEmpty{
            return Float(self)!
        }
        else{
            return 0.0
        }
    }
    func setAttributedStringWithColorSize(_ firstStr:String ,secondStr:String, firstColor:UIColor, secondColor:UIColor, font1:UIFont, font2:UIFont) -> NSAttributedString {
        let combineStr = firstStr + secondStr
    let attributedText = NSMutableAttributedString(string:combineStr)
    let attrsFirstStr      = [NSFontAttributeName: font1, NSForegroundColorAttributeName:firstColor]
    let attrsSecondStr      = [NSFontAttributeName: font2, NSForegroundColorAttributeName: secondColor]
    let rangeFirst = NSString(string: combineStr).range(of: firstStr)
    attributedText.addAttributes(attrsFirstStr, range: rangeFirst)
    let rangeSecond = NSString(string: combineStr).range(of: secondStr)
    attributedText.addAttributes(attrsSecondStr, range:rangeSecond)
    return attributedText
    }
}
