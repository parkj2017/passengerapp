//
//  NSData+Extension.swift
//  PassangerApp
//
//  Created by Netquall on 12/16/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import Foundation
import UIKit
import DLImageLoader

extension Data {

func vehicleImageForType(_ vehicleType:String)-> Data?{
    
    var vehicleImage_Data : Data? = nil
    let dicActiveUser = Globals.sharedInstance.getValueFromUserDefaultsForKey(Constants.ActiveUserInfo) as! [String:Any]
    let arrVehicleTypes = dicActiveUser["vehicle_types"] as! [AnyObject]
    for dicVehicleInfo in arrVehicleTypes{
       
    if ((dicVehicleInfo["id"] as! String).compare(vehicleType) ) {
//            let cars_Icon: NSURL = NSURL(string: dicVehicleInfo["map_icon"]!!.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!
//            if let data =  NSData(contentsOfURL: cars_Icon) {
//            vehicleImage_Data = data
//            Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(vehicleImage_Data!, key: vehicleType)
//             }
        let url = (dicVehicleInfo["map_icon"] as! String).addingPercentEscapes(using: .utf8)

        let urlReqst = URLRequest(url: URL(string: url!)!)
        DLImageLoader.sharedInstance().image(from: urlReqst, completed: { (error, image) -> () in
            if image != nil {
                let size = CGSize(width: 25 , height: 25)
                UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
                image?.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
                let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
                UIGraphicsEndImageContext()
                
                if let data = UIImagePNGRepresentation(newImage)
                {
                    vehicleImage_Data = data
                    Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(vehicleImage_Data as Any?, key: vehicleType)
                }
            }
        })
//        DLImageLoader.sharedInstance().imageFromUrl(((dicVehicleInfo["map_icon"]!! as Any).addingPercentEscapes(using: String.Encoding.utf8)!), completed: { (error, image) -> () in
//            if image != nil {
//                let size = CGSize(width: 25 , height: 25)
//                UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
//                image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
//                let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
//                UIGraphicsEndImageContext()
//                
//                if let data = UIImagePNGRepresentation(newImage)
//                {
//                    vehicleImage_Data = data
//                    Globals.sharedInstance.saveValuetoUserDefaultsWithKeyandValue(vehicleImage_Data, key: vehicleType)
//                }
//            }
//        })

            break
        }
     }
    if let _ = vehicleImage_Data {
          return vehicleImage_Data!
    }else {
         return nil
    }
  }
}
