//
//  Dictionary+Extension.swift
//  PassangerApp
//
//  Created by Netquall on 1/12/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import Foundation
import UIKit
var banningDictionary: [String : String] = [
    "<null>" : "\"\"",
    "null" : "\"\"",
]
extension Dictionary {
    
    // Remove Null values from Dictionary Values 
    
func formatDictionaryForNullValues(_ dicValues:[String:Any])-> [String:Any]? {
    
    var jsonString:String!
    var jsonData : Data! = Data()
    do {
        jsonData = try JSONSerialization.data(withJSONObject: dicValues, options: JSONSerialization.WritingOptions.prettyPrinted)
        jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
       // print("response data \(jsonString)")
    }catch{
        print("Error--- \(error)")
        return nil
    }
    if jsonString.contains("<null>") == true {
         jsonString = jsonString.replacingOccurrences(of: "<null>", with: "\"\"")
    }
    if jsonString.contains("null"){
        jsonString = jsonString.replacingOccurrences(of: "null", with: "\"\"")
    }
    
    
 
    let bannedWords = banningDictionary.keys
    let nullValues = bannedWords.joined(separator: "|")//joinWithSeparator("|")
    let pattern = "\\b(?:\(nullValues))\\b"
    let regex = try! MyRegularExpression(pattern: pattern, options: [])

    jsonString = regex.stringByReplacingMatches(in: jsonString, options: [], range: NSRange(0..<jsonString.utf16.count), withTemplate: "")
   let ObjectData = jsonString.data(using: String.Encoding.utf8)
    var dicReturn : [String:Any]!
    do {
        dicReturn = try JSONSerialization.jsonObject(with: ObjectData!, options: .mutableContainers) as! [String:Any]
        return dicReturn!
    }catch {
        print("json Error-::\(error)")
        return nil
       }
    
   }
   
    
}
class MyRegularExpression: NSRegularExpression {
    override func replacementString(for result: NSTextCheckingResult, in string: String, offset: Int, template templ: String) -> String {
        let matchingRange = result.range
        let matchingWord = (string as NSString).substring(with: matchingRange)
        if let replacedWord = banningDictionary[matchingWord] {
            return replacedWord
        } else {
            //This may never be executed.
            return "*"
        }
        
    }
}
extension NSDictionary {
    
    // Remove Null values from Dictionary Values
    
    func formatDictionaryForNullValues(_ dicValues:NSMutableDictionary)-> NSMutableDictionary? {
        
        var jsonString:String!
        var jsonData : Data! = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dicValues, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            // print("response data \(jsonString)")
        }catch{
            print("Error--- \(error)")
            return nil
        }
        if jsonString.contains("<null>") == true {
            jsonString = jsonString.replacingOccurrences(of: "<null>", with: "\"\"")
        }
        if jsonString.contains("null"){
            jsonString = jsonString.replacingOccurrences(of: "null", with: "\"\"")
        }
        
        
        
        let bannedWords = banningDictionary.keys
        let nullValues = bannedWords.joined(separator: "|")//joinWithSeparator("|")
        let pattern = "\\b(?:\(nullValues))\\b"
        let regex = try! MyRegularExpression(pattern: pattern, options: [])
        
        jsonString = regex.stringByReplacingMatches(in: jsonString, options: [], range: NSRange(0..<jsonString.utf16.count), withTemplate: "")
        let ObjectData = jsonString.data(using: String.Encoding.utf8)
        var dicReturn : [String:Any]!
        do {
            dicReturn = try JSONSerialization.jsonObject(with: ObjectData!, options: .mutableContainers) as! [String:Any]
            return dicReturn as! NSMutableDictionary
        }catch {
            print("json Error-::\(error)")
            return nil
        }
        
    }
    
    
}
