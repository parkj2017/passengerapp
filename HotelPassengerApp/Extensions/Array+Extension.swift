//
//  Array+Extension.swift
//  PassangerApp
//
//  Created by Netquall on 12/16/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import Foundation
import UIKit

extension Array {
    func contains<T>(_ obj: T) -> Bool where T : Equatable {
        return self.filter({$0 as? T == obj}).count > 0
    }
}
extension Array where Element: Equatable {
    mutating func remove(_ object: Element) {
        if let index = index(where: { $0 == object }) {
            self.remove(at: index)
        }
    }
}
