//
//  UIStatusBar+MSStatusBarContent.swift
//  MSExplorer
//
//  Created by Netquall on 10/31/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar
{
    
    func SetNavigationBarThemeColor() 
    {
        self.tintColor = UIColor.darkBlack()
        self.barTintColor = UIColor.appLighGrayColor()
        self.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.darkBlack() ];
        UIApplication.shared.setStatusBarStyle(.default, animated: false)
        guard  let statusBar = ((UIApplication.shared.value(forKey: "statusBarWindow") as Any) as AnyObject).value(forKey: "statusBar") as? UIView else {
            return
        }
        statusBar.backgroundColor = UIColor.appLighGrayColor()

    }
    func SetNavigationBarNormalColor()
    {
        self.backgroundColor = UIColor.black
        self.tintColor = UIColor.black
        self.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.darkBlack() ];
        self.barTintColor = UIColor.appLighGrayColor()
        UIApplication.shared.setStatusBarStyle(.default, animated: false)
    }
    
}
