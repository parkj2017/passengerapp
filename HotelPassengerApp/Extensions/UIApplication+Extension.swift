//
//  UIApplication+Extension.swift
//  PassangerApp
//
//  Created by Netquall on 12/2/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import UIKit

extension UIApplication {
    
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.topViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        
        return viewController
    }
    
    class func topViewControllerForPush () -> UIViewController?  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let navigationController = storyboard.instantiateViewController(withIdentifier: "LoginViewControllerNavigation") as?UINavigationController{
            return navigationController.topViewController
        }
        else {
            return UIViewController()
        }
    }
}
