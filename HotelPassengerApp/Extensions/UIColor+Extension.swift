//
//  UIColor+Extension.swift
//  PassangerApp
//
//  Created by Netquall on 1/15/16.
//  Copyright © 2016 Netquall. All rights reserved.
//	

import Foundation
import UIKit
extension UIColor{
   class  func appBlackColor() -> UIColor{
    return UIColor(red: 37.0/255.0, green: 37.0/255.0, blue: 37.0/255.0, alpha: 1.0)
    }
    class  func appLighGrayColor() -> UIColor{
        return UIColor(red: 232.0/255.0, green: 232.0/255.0, blue: 232.0/255.0, alpha: 1.0)
    }
    class  func appRedColor() -> UIColor{
        return UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
    }
    class  func appLightBlueColor() -> UIColor{
        return UIColor(red: 21.0/255.0, green: 225.0/255.0, blue: 223.0/255.0, alpha: 1.0)
    }
    class  func appUbberBlueColor() -> UIColor{
        return UIColor(red: 43.0/255.0, green: 182.0/255.0, blue: 203.0/255.0, alpha: 1.0)
    }
    class  func appGrayColor() -> UIColor{
        return UIColor(red: 124.0/255.0, green: 124.0/255.0, blue: 124.0/255.0, alpha: 1.0)
    }
    //upcoming reservation
    class  func darkBlack() -> UIColor{
        return UIColor(red: 19.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1.0)
    }
    class  func lightBlack() -> UIColor{
        return UIColor(red: 47.0/255.0, green: 47.0/255.0, blue: 47.0/255.0, alpha: 1.0)
    }
    // MARK:- =========== Ride later section colors  ===========
    class  func appRideLater_RedColor() -> UIColor{
        return UIColor(red: 232.0/255.0, green: 111.0/255.0, blue: 90.0/255.0, alpha: 1.0)
    }
    class  func appThemeColor() -> UIColor{
        return UIColor(red: 231.0/255.0, green: 112.0/255.0, blue: 88.0/255.0, alpha: 1.0)
    }
    class  func appRideLater_GreenColor() -> UIColor{
        return UIColor(red: 75.0/255.0, green: 162.0/255.0, blue: 22.0/255.0, alpha: 1.0)
    }
    class  func appRideLater_UbberBlueColor() -> UIColor{
        return UIColor(red: 43.0/255.0, green: 182.0/255.0, blue: 203.0/255.0, alpha: 1.0)
    }
    class  func appRideLater_GrayColor() -> UIColor{
        return UIColor(red: 124.0/255.0, green: 124.0/255.0, blue: 124.0/255.0, alpha: 1.0)
    }
    class  func appRideLater_cellBackground() -> UIColor{
        return UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
    }
    class  func appRideLater_TextColor() -> UIColor{
        return UIColor(red: 72.0/255.0, green: 72.0/255.0, blue: 72.0/255.0, alpha: 1.0)
    }
    
    class  func appLogo_bgColor() -> UIColor{
        return UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1.0)
            //UIColor(red: 47.0/255.0, green: 47.0/255.0, blue: 47.0/255.0, alpha: 1.0)
    }
}
