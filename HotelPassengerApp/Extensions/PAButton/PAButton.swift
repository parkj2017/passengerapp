//
//  XTLabel.swift
//  XTag
//
//  Created by Dad's Gift on 08/05/16.
//  Copyright © 2016 Dad's Gift. All rights reserved.
//

import UIKit

class PAButton: UIButton {
    
  
    @IBInspectable var backGround: UIColor = UIColor.appThemeColor() {
        didSet {
           self.setNeedsDisplay()
            self.backgroundColor = backGround
        }
        
    }
//    @IBInspectable var backgroundColor: UIColor = UIColor.appThemeColor() {
//        didSet {
//         
//        }
//    }
    
  
   
    override func draw(_ rect: CGRect) {
        super.draw(rect)
         self.titleLabel?.font = Globals.defaultAppFontWithBold(16)
          self.backgroundColor = self.backGround
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.titleLabel?.font = Globals.defaultAppFontWithBold(16)
        self.backgroundColor = self.backGround
    }
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        self.backgroundColor = self.backGround
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }

}
