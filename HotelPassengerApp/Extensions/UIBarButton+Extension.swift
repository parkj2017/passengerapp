//
//  UIBarButton+Extension.swift
//  PassangerApp
//
//  Created by Netquall on 1/15/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import Foundation
import UIKit


extension UIBarButtonItem {
   
   
   class func leftBackButtonWithCustom(_ title:String, imageName : String) -> UIBarButtonItem{
        let navLeftBtn: UIButton = UIButton(type: .custom)
           navLeftBtn.setTitle(title, for: UIControlState())
        navLeftBtn.titleLabel!.textColor = UIColor.appBlackColor()
        navLeftBtn.titleLabel!.font = UIFont(name: "Oxygen", size: 18)
        navLeftBtn.setImage(UIImage(named: imageName), for: UIControlState())
        navLeftBtn.contentMode = .scaleAspectFit
        navLeftBtn.contentHorizontalAlignment = .left
        navLeftBtn.frame = CGRect(x: 40, y: 0,width: 150, height: 40)
       // navLeftBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -7, 0, 0)
        //navLeftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: navLeftBtn)
        return leftBarButton
        
    }
    
}
extension UIButton {
    override open var intrinsicContentSize: CGSize {
        get {
            let labelSize = titleLabel?.sizeThatFits(CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude)) ?? CGSize.zero
            let desiredButtonSize = CGSize(width: labelSize.width + titleEdgeInsets.left + titleEdgeInsets.right, height: labelSize.height + titleEdgeInsets.top + titleEdgeInsets.bottom)
            
            return desiredButtonSize
        }
    }
}
