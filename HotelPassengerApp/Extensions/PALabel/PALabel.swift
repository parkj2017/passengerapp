//
//  XTLabel.swift
//  XTag
//
//  Created by Dad's Gift on 08/05/16.
//  Copyright © 2016 Dad's Gift. All rights reserved.
//

import UIKit

class PALabel: UILabel {
    
    @IBInspectable var leftTextPedding: Int = 15 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    override func draw(_ rect: CGRect) {
        let insets : UIEdgeInsets = UIEdgeInsets (top: 0, left: CGFloat(leftTextPedding), bottom: 0, right: 10)
         
        return super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
}
