//
//  UIViewController+Extension.swift
//  PassangerApp
//
//  Created by Netquall on 12/2/15.
//  Copyright © 2015 Netquall. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
    
    }
    //MARK: -- Show Banner With Error Message
    func showBannerWithMessage (_ message : String){
        CSNotificationView.show(in: self, style: .error, font: Globals.defaultAppFont(14), message: message) //show(in: self, style: .error, message: message)
        
    }
    func showInfoBannerWithMessage (_ message : String){
        CSNotificationView.show(in: self, style: .success, font: Globals.defaultAppFont(14), message: message) //show(in: self, style: .error, message: message)
    }
    //MARK: -- Show Banner With Success Message
    func showSuccessBannerWithMessage (_ message : String){
          CSNotificationView.show(in: self, style: .success, font: Globals.defaultAppFont(14), message: message)
    }
func alertView(_ titleAlert:String!, message: String!, delegate: Any?, cancelButton: String!, otherButons:[String]?) -> UIAlertView!
    {
        if let otherButonsTitles = otherButons{
            let alertView = UIAlertView(title: titleAlert, message: message, delegate: delegate as? UIAlertViewDelegate, cancelButtonTitle: cancelButton, otherButtonTitles: otherButonsTitles[0])
            return alertView
        }else{
           let alertView = UIAlertView(title: titleAlert, message: message, delegate:nil, cancelButtonTitle: cancelButton)
            return alertView
        }
}
class func createMenuViewController() {
        
        // create viewController code...
       // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
       // let mainViewController = storyboard.instantiateViewControllerWithIdentifier("OnDemandViewController") as! OnDemandViewController
        
        
      //  let leftViewController = storyboard.instantiateViewControllerWithIdentifier("MenuLeftViewController") as! MenuLeftViewController
        
        
      //  let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        //UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
       // leftViewController.OnDemandVC_Home = nvc
       // let slideMenuController = ExSlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
       // slideMenuController.automaticallyAdjustsScrollViewInsets = true
       // UIApplication.sharedApplication().keyWindow!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
       // UIApplication.sharedApplication().keyWindow!.rootViewController = slideMenuController
       // UIApplication.sharedApplication().keyWindow!.makeKeyAndVisible()
    }
   
    
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!
    }
    
}
